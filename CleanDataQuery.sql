TRuncate table [AthleticPreferencesTransaction]
TRUNCATE table [CompetitorNumbersTransaction]
TRUNCATE table RelayPreferencesTransaction
TRUNCATE table AthleticPreferenceTemplate
DELETE [AthleteRelayPreferencesTransaction]
TRuncate table CompetitorNumbersTransaction
TRuncate table [DualMeetAssignmentTransaction]
TRuncate table DualMeetTransaction
TRuncate table MeetSetup
TRuncate table [MeetSetupAthletePreferences]
TRuncate table  [MeetSetupCompetitorNumber]
TRuncate table [MeetSetupDualMeet]
TRuncate table MeetSetupDualMeetAssignment
TRuncate table [MeetSetupRelayPreferences]
TRuncate table  [MeetSetupRendomizationRule]
TRuncate table [MeetSetupSeedingRules]
TRuncate table [MeetSetupStandardLanePrefrences]
TRuncate table MeetSetupWaterfallStart
TRuncate table MeetStandardAlleyPrefrences
--TRuncate table MenuInRole
TRuncate table  [RelayPreferencesTransaction]
TRuncate table [RendomizationRuleTransaction]
TRuncate table ReportColumnInDataSetTable
TRuncate table ReportContentPropertyDetail
TRuncate table ReportDataSetInTable
TRUNCATE TABLE SeedPreferenceTemplate
DELETE SeedingPreferencesTransaction
TRuncate table SeedingRulesTransaction

TRuncate table StandardAlleyPrefrencesTransaction
TRuncate table StandardLanePrefrencesTransaction
TRuncate table WaterfallStartPreferenceTransaction
--TRuncate table UserInRole
TRuncate table ReportGenerationDetail
TRuncate table ReportGenerationContentType
TRuncate table ReportDetailInfoItemProperty
TRuncate table ReportDataSetMappedDetail
DELETE  ReportDataSetMappedMaster
DELETE  ReportDataSetInfo
DELETE  ReportDetailInfoItem
DELETE  ReportDetail
DELETE  ReportDetailInfo

--TRuncate table [Role]
--DELETE  [User]
DELETE  ReportDataSetTableColumn
DELETE  ReportDataSetTable

DELETE  ReportGenerationInfo
DELETE  ReportGeneratorContent





--select * from [AthleteRelayPreferencesTransaction]