﻿using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.SetupMeet.Model
{
    public class ScoringMeetModel : BindableBase
    {
        private int _Place;
        public int Place
        {
            get { return _Place; }
            set
            {
                SetProperty(ref _Place, value);
            }
        }
        private int _IndividualPints;
        public int IndividualPints
        {
            get { return _IndividualPints; }
            set
            {
                SetProperty(ref _IndividualPints, value);
            }
        }

        private int _RelayPoints;
        public int RelayPoints
        {
            get { return _RelayPoints; }
            set
            {
                SetProperty(ref _RelayPoints, value);
            }
        }
        private int _CombEvtPoints;
        public int CombEvtPoints
        {
            get { return _CombEvtPoints; }
            set
            {
                SetProperty(ref _CombEvtPoints, value);
            }
        }
    }
}
