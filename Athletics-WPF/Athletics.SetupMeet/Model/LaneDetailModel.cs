﻿using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.SetupMeet.Model
{
    public class LaneDetailModel : BindableBase
    {
        private int _LaneID;
        public int LaneID
        {
            get { return _LaneID; }
            set
            {
                SetProperty(ref _LaneID, value);
            }
        }

        private int _SchoolID;
        public int SchoolID
        {
            get { return _SchoolID; }
            set
            {
                SetProperty(ref _SchoolID, value);
            }
        }
        private string _SchoolName;
        public string SchoolName
        {
            get { return _SchoolName; }
            set
            {
                SetProperty(ref _SchoolName, value);
            }
        }
    }
}
