﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Athletics.SetupMeet.Model
{
    public class MeetMenuItemViewModel
    {
        private ICommand _command;
        public MeetMenuItemViewModel()
        {
            MenuItems = new ObservableCollection<MeetMenuItemViewModel>();
        }

        public string Header { get; set; }
        public int MenuID { get; set; }
        public ObservableCollection<MeetMenuItemViewModel> MenuItems { get; set; }

        public ICommand Command
        {
            get
            {
                return _command;
            }
            set
            {
                _command = value;
            }
        }
    }
    public class MeetCommandViewModel : ICommand
    {
        private readonly Action _action;

        public MeetCommandViewModel(Action action)
        {
            _action = action;
        }

        public void Execute(object o)
        {
            _action();
        }

        public bool CanExecute(object o)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { }
            remove { }
        }
    }
}
