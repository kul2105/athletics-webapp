﻿using Athletics.MeetSetup.ViewModels;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.SetupMeet.Model
{
    public class AthleteRelayPrefrencesGroup : BindableBase
    {
        private string _GroupName;
        public string GroupName
        {
            get { return _GroupName; }
            set
            {
                _GroupName = value;
                SetProperty(ref _GroupName, value);
            }
        }
        private ObservableCollection<Option> _AthletePreferencesList = new ObservableCollection<Option>();
        public ObservableCollection<Option> AthletePreferencesList
        {
            get { return _AthletePreferencesList; }
            set
            {
                SetProperty(ref _AthletePreferencesList, value);
            }
        }

        private ObservableCollection<Option> _CompetitorNumbersList = new ObservableCollection<Option>();
        public ObservableCollection<Option> CompetitorNumbersList
        {
            get { return _CompetitorNumbersList; }
            set
            {
                SetProperty(ref _CompetitorNumbersList, value);
            }
        }

        private ObservableCollection<Option> _RelayPreferencesList = new ObservableCollection<Option>();
        public ObservableCollection<Option> RelayPreferencesList
        {
            get { return _RelayPreferencesList; }
            set
            {
                SetProperty(ref _RelayPreferencesList, value);
            }
        }
    }
}
