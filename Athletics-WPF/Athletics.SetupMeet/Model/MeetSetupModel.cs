﻿using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.SetupMeet.Model
{
    public class MeetSetupModel : BindableBase
    {
        private int _MeetID;
        public int MeetID
        {
            get { return _MeetID; }
            set
            {
                SetProperty(ref _MeetID, value);
            }
        }
        private string _MeetName;
        public string MeetName
        {
            get { return _MeetName; }
            set
            {
                SetProperty(ref _MeetName, value);
            }
        }
        private string _MeetName2;
        public string MeetName2
        {
            get { return _MeetName2; }
            set
            {
                SetProperty(ref _MeetName2, value);
            }
        }
        private bool? _IsUserDivisionBirthDate = false;
        public bool? IsUserDivisionBirthDate
        {
            get { return _IsUserDivisionBirthDate; }
            set
            {
                SetProperty(ref _IsUserDivisionBirthDate, value);
            }
        }

        private bool? _IsLinkTeamToDivision = false;
        public bool? IsLinkTeamToDivision
        {
            get { return _IsLinkTeamToDivision; }
            set
            {
                SetProperty(ref _IsLinkTeamToDivision, value);
            }
        }

        private string _BaseCounty;
        public string BaseCounty
        {
            get { return _BaseCounty; }
            set
            {
                SetProperty(ref _BaseCounty, value);
            }
        }
        private DateTime? _StartDate = DateTime.Now;
        public DateTime? StartDate
        {
            get { return _StartDate; }
            set
            {
                SetProperty(ref _StartDate, value);
            }
        }

        private DateTime? _EndDate = DateTime.Now;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set
            {
                SetProperty(ref _EndDate, value);
            }
        }
        private DateTime? _AgeUpDate = DateTime.Now;
        public DateTime? AgeUpDate
        {
            get { return _AgeUpDate; }
            set
            {
                SetProperty(ref _AgeUpDate, value);
            }
        }


        private string _KindOfMeetOption;
        public string KindOfMeetOption
        {
            get { return _KindOfMeetOption; }
            set
            {
                SetProperty(ref _KindOfMeetOption, value);
            }
        }

        private string _MeetClassOption;
        public string MeetClassOption
        {
            get { return _MeetClassOption; }
            set
            {
                SetProperty(ref _MeetClassOption, value);
            }
        }


        private string _MeetTypeOption;
        public string MeetTypeOption
        {
            get { return _MeetTypeOption; }
            set
            {
                SetProperty(ref _MeetTypeOption, value);
            }
        }


        private string _MeetStyleOption;
        public string MeetStyleOption
        {
            get { return _MeetStyleOption; }
            set
            {
                SetProperty(ref _MeetStyleOption, value);
            }
        }

        private string _MeetArenaOption;
        public string MeetArenaOption
        {
            get { return _MeetArenaOption; }
            set
            {
                SetProperty(ref _MeetArenaOption, value);
            }
        }
        private string _MeetTypeDivisionOption;
        public string MeetTypeDivisionOption
        {
            get { return _MeetTypeDivisionOption; }
            set
            {
                SetProperty(ref _MeetTypeDivisionOption, value);
            }
        }

        private SeedingPrefrencesGroup _SeedingPrefrencesGroup;
        public SeedingPrefrencesGroup SeedingPrefrencesGroup
        {
            get { return _SeedingPrefrencesGroup; }
            set
            {
                SetProperty(ref _SeedingPrefrencesGroup, value);
            }
        }

        private AthleteRelayPrefrencesGroup _AthleteRelayPrefrencesGroup;
        public AthleteRelayPrefrencesGroup AthleteRelayPrefrencesGroup
        {
            get { return _AthleteRelayPrefrencesGroup; }
            set
            {
                SetProperty(ref _AthleteRelayPrefrencesGroup, value);
            }
        }

    }
}
