﻿using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.SetupMeet.Model
{
   
    public class DualMeetSchoolTransaction: BindableBase
    {
        private int? _LaneNumber;
        public int? LaneNumber
        {
            get { return _LaneNumber; }
            set
            {
                SetProperty(ref _LaneNumber, value);
            }
        }

        private string _SchoolAbb;
        public string SchoolAbb
        {
            get { return _SchoolAbb; }
            set
            {
                SetProperty(ref _SchoolAbb, value);
            }
        }

        private string _SchoolName;
        public string SchoolName
        {
            get { return _SchoolName; }
            set
            {
                SetProperty(ref _SchoolName, value);
            }
        }
    }
}
