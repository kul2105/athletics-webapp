﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.ViewModels;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.SetupMeet.Model
{
    public class SeedingPrefrencesGroup : BindableBase
    {
        private string _GroupName;
        public string GroupName
        {
            get { return _GroupName; }
            set
            {
                _GroupName = value;
                SetProperty(ref _GroupName, value);
            }
        }
        private ObservableCollection<Entities.Models.WaterfallStartPreference> _WaterfallStartPreferencesList = new ObservableCollection<Entities.Models.WaterfallStartPreference>();
        public ObservableCollection<Entities.Models.WaterfallStartPreference> WaterfallStartPreferencesList
        {
            get { return _WaterfallStartPreferencesList; }
            set
            {
                SetProperty(ref _WaterfallStartPreferencesList, value);
            }
        }

        private ObservableCollection<Entities.Models.StandardLanePrefrence> _StandardLanePrefrencesList = new ObservableCollection<Entities.Models.StandardLanePrefrence>();
        public ObservableCollection<Entities.Models.StandardLanePrefrence> StandardLanePrefrencesList
        {
            get { return _StandardLanePrefrencesList; }
            set
            {
                SetProperty(ref _StandardLanePrefrencesList, value);
            }
        }

        private ObservableCollection<Entities.Models.StandardAlleyPrefrence> _StandardAlleyPrefrencesList = new ObservableCollection<Entities.Models.StandardAlleyPrefrence>();
        public ObservableCollection<Entities.Models.StandardAlleyPrefrence> StandardAlleyPrefrencesList
        {
            get { return _StandardAlleyPrefrencesList; }
            set
            {
                SetProperty(ref _StandardAlleyPrefrencesList, value);
            }
        }

        private ObservableCollection<DualMeetSchoolTransaction> _DualMeetList = new ObservableCollection<DualMeetSchoolTransaction>();
        public ObservableCollection<DualMeetSchoolTransaction> DualMeetList
        {
            get { return _DualMeetList; }
            set
            {
                SetProperty(ref _DualMeetList, value);
            }
        }

        private Entities.Models.MeetSetupSeedingRule _SeedingRules = new Entities.Models.MeetSetupSeedingRule();
        public Entities.Models.MeetSetupSeedingRule SeedingRules
        {
            get { return _SeedingRules; }
            set
            {
                SetProperty(ref _SeedingRules, value);
            }
        }

        private Entities.Models.DualMeetAssignmentTransaction _MeetAssignment;
        public Entities.Models.DualMeetAssignmentTransaction MeetAssignment
        {
            get { return _MeetAssignment; }
            set
            {
                SetProperty(ref _MeetAssignment, value);
            }
        }

        private Entities.Models.RendomizationRuleTransaction _RendomizationRule = new Entities.Models.RendomizationRuleTransaction();
        public Entities.Models.RendomizationRuleTransaction RendomizationRule
        {
            get { return _RendomizationRule; }
            set
            {
                SetProperty(ref _RendomizationRule, value);
            }
        }

        private ObservableCollection<SchoolDetail> _SchoolDetailList = new ObservableCollection<SchoolDetail>();
        public ObservableCollection<SchoolDetail> SchoolDetailList
        {
            get { return _SchoolDetailList; }
            set
            {
                SetProperty(ref _SchoolDetailList, value);
            }
        }
    }
}
