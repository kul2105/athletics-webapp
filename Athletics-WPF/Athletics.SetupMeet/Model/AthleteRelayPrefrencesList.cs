﻿
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.ViewModels;
using Athletics.ModuleCommon;
using Athletics.SetupMeet.Common;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Athletics.SetupMeet.Model
{
    public class AthleteRelayPrefrencesList: BindableBase
    {
        private ObservableCollection<AthleteRelayPrefrencesGroup> _AthleteRelayPreferencesGroupList = new ObservableCollection<AthleteRelayPrefrencesGroup>();
        public ObservableCollection<AthleteRelayPrefrencesGroup> AthleteRelayPreferencesGroupList
        {
            get { return _AthleteRelayPreferencesGroupList; }
            set
            {
                SetProperty(ref _AthleteRelayPreferencesGroupList, value);
            }
        }

        public AthleteRelayPrefrencesList()
        {
            IMeetSetupManage meetSetupManage= GlobalEvent.MeetSetupManage;
           
            foreach (var item in meetSetupManage.GetAllRelayPrefrencesGroupData().Result.Select(p => p.RelayGroupID).Distinct())
            {
                AthleteRelayPrefrencesGroup opt = new AthleteRelayPrefrencesGroup();
                opt.GroupName = item;
                opt.AthletePreferencesList = new ObservableCollection<Option>();
                opt.RelayPreferencesList = new ObservableCollection<Option>();
                opt.CompetitorNumbersList = new ObservableCollection<Option>();
                foreach (var AthletePreference in meetSetupManage.GetAllRelayPrefrencesGroupData().Result.Where(p => p.RelayGroupID == item).Select(p => p.AthletePreferencesName).Distinct())
                {
                    Option optRelayPrefrences = new Option();
                    optRelayPrefrences.Display = AthletePreference;
                    opt.AthletePreferencesList.Add(optRelayPrefrences);
                }

                foreach (var RelayPreference in meetSetupManage.GetAllRelayPrefrencesGroupData().Result.Where(p => p.RelayGroupID == item).Select(p => p.RelayPreferencesName).Distinct())
                {
                    Option optRelayPrefrences = new Option();
                    optRelayPrefrences.Display = RelayPreference;
                    opt.RelayPreferencesList.Add(optRelayPrefrences);
                }

                foreach (var CompetitorNumber in meetSetupManage.GetAllRelayPrefrencesGroupData().Result.Where(p => p.RelayGroupID == item).Select(p => p.CompetitorNumbersName).Distinct())
                {
                    Option optRelayPrefrences = new Option();
                    optRelayPrefrences.Display = CompetitorNumber;
                    opt.CompetitorNumbersList.Add(optRelayPrefrences);
                }
                AthleteRelayPreferencesGroupList.Add(opt);
            }
           
        }
    }
}
