﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace Athletics.SetupMeet.ValidationRules
{
    public class GridStandardLanePrefrenceDuplicate : ValidationRule
    {
        public override ValidationResult Validate(object value,
            System.Globalization.CultureInfo cultureInfo)
        {
            StandardLanePrefrence currentLane = (value as BindingGroup).Items[0] as StandardLanePrefrence;
            if (currentLane != null)
            {

                List<int> TotalLIneValue = new List<int>();
                if (currentLane.FirstLane != null)
                {
                    TotalLIneValue.Add(currentLane.FirstLane.Value);
                }
                if (currentLane.SecondLine != null)
                {
                    TotalLIneValue.Add(currentLane.SecondLine.Value);
                }

                if (currentLane.ThirdLine != null)
                {
                    TotalLIneValue.Add(currentLane.ThirdLine.Value);
                }

                if (currentLane.ForthLine != null)
                {
                    TotalLIneValue.Add(currentLane.ForthLine.Value);
                }
                if (currentLane.FifthLine != null)
                {
                    TotalLIneValue.Add(currentLane.FifthLine.Value);
                }
                if (currentLane.SixthLine != null)
                {
                    TotalLIneValue.Add(currentLane.SixthLine.Value);
                }
                if (currentLane.SeventhLine != null)
                {
                    TotalLIneValue.Add(currentLane.SeventhLine.Value);
                }
                if (currentLane.EightsLine != null)
                {
                    TotalLIneValue.Add(currentLane.EightsLine.Value);
                }
                if (currentLane.NinthLine != null)
                {
                    TotalLIneValue.Add(currentLane.NinthLine.Value);
                }
                if (currentLane.TenthLine != null)
                {
                    TotalLIneValue.Add(currentLane.TenthLine.Value);
                }

                if (TotalLIneValue.Count != TotalLIneValue.Distinct().Count())
                {
                    return new ValidationResult(false, "Duplicate value not allowed in Lane");
                }
            }
            return ValidationResult.ValidResult;
        }
    }

    public class GridStandardAlleyPrefrenceDuplicate : ValidationRule
    {
        public override ValidationResult Validate(object value,
            System.Globalization.CultureInfo cultureInfo)
        {
            StandardAlleyPrefrence currentLane = (value as BindingGroup).Items[0] as StandardAlleyPrefrence;
            if (currentLane != null)
            {

                List<int> TotalLIneValue = new List<int>();
                if (currentLane.FirstLane != null)
                {
                    TotalLIneValue.Add(currentLane.FirstLane.Value);
                }
                if (currentLane.SecondLine != null)
                {
                    TotalLIneValue.Add(currentLane.SecondLine.Value);
                }

                if (currentLane.ThirdLine != null)
                {
                    TotalLIneValue.Add(currentLane.ThirdLine.Value);
                }

                if (currentLane.ForthLine != null)
                {
                    TotalLIneValue.Add(currentLane.ForthLine.Value);
                }
                if (currentLane.FifthLine != null)
                {
                    TotalLIneValue.Add(currentLane.FifthLine.Value);
                }
                if (currentLane.SixthLine != null)
                {
                    TotalLIneValue.Add(currentLane.SixthLine.Value);
                }
                if (currentLane.SeventhLine != null)
                {
                    TotalLIneValue.Add(currentLane.SeventhLine.Value);
                }
                if (currentLane.EightsLine != null)
                {
                    TotalLIneValue.Add(currentLane.EightsLine.Value);
                }
                if (currentLane.NinthLine != null)
                {
                    TotalLIneValue.Add(currentLane.NinthLine.Value);
                }
                if (currentLane.TenthLine != null)
                {
                    TotalLIneValue.Add(currentLane.TenthLine.Value);
                }

                if (TotalLIneValue.Count != TotalLIneValue.Distinct().Count())
                {
                    return new ValidationResult(false, "Duplicate value not allowed in Lane");
                }
            }
            return ValidationResult.ValidResult;
        }
    }

    public class CellMaxValueLimitValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value != null)
            {
                int ParseValue = -1;
                if (int.TryParse(value.ToString(), out ParseValue))
                {
                    if (ParseValue > 10 || ParseValue <= 0)
                    {
                        return new ValidationResult(false, "Value must be between 1 to 10");
                        //check for unique
                    }
                }
            }
            return new ValidationResult(true, null);
        }
    }

    public class CellMaxValueLimitHundredSixthValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value != null)
            {
                int ParseValue = -1;
                if (int.TryParse(value.ToString(), out ParseValue))
                {
                    if (ParseValue > 160)
                    {
                        return new ValidationResult(false, "Value must be less  than  160.");
                        //check for unique
                    }
                }
            }
            return new ValidationResult(true, null);
        }
    }
}
