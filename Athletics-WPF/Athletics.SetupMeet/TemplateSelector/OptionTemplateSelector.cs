﻿using Athletics.MeetSetup.ViewModels;
using Athletics.ModuleCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Athletics.SetupMeet.TemplateSelector
{
    public class OptionTemplateSelector : DataTemplateSelector
    {
        public override System.Windows.DataTemplate SelectTemplate(object item,
                        System.Windows.DependencyObject container)
        {
            Option athletOption = item as Option;
            if (athletOption != null)
            {
                if (Boolean.Parse(athletOption.Tag) == false)
                {
                    DataTemplate datatemplateCheckBox = Application.Current.FindResource("CheckBoxStyle") as DataTemplate;
                    return datatemplateCheckBox;
                }
                else
                {
                    DataTemplate datatemplateRadiobutton = Application.Current.FindResource("RadioButtonStyle") as DataTemplate;
                    return datatemplateRadiobutton;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
