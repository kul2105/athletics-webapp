﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Athletics.ControlLibrary.DialogWindow;
using Microsoft.Practices.Unity;
using Athletics.MeetSetup.Views.SeedPreferences;
using Athletics.MeetSetup.ViewModels.SeedPreferences;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Command;
using Athletics.MeetSetup.Behaviors;
using Athletics.ModuleCommon;
using Athletics.SetupMeet.View.AssigPrefrences;
using System.Windows.Media;

namespace Athletics.MeetSetup.ViewModels
{

    public class AssigPrefrencesViewModel : BindableBase, IAssigPrefrencesViewModel, IGenericInteractionView<MeetSetupModel>
    {
        private IAssigPrefrencesView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        SeedPreferencesViewModel seedVm = null;
        AthleteRelayPreferencesViewModel AthleteRelayPrefrences = null;
        public AssigPrefrencesViewModel(IUnityContainer unity, IAssigPrefrencesView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;
            unityContainer = unity;
            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            seedVm = unityContainer.Resolve<ISeedPreferencesViewModel>() as SeedPreferencesViewModel;
            AthleteRelayPrefrences = unityContainer.Resolve<IAthleteRelayPreferencesViewModel>() as AthleteRelayPreferencesViewModel;
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Task.Run(async () =>
                           {
                               if (AthleteRelayPreferencesId == 0)
                               {

                                   if (seedVm == null && AthleteRelayPrefrences == null)
                                   {
                                       MessageBox.Show("Please select atlest one Prefrences to Assigned", "No Prefrences Selected");
                                       return;
                                   }
                                   ServiceRequest_AssignPreferences serviceObj = new ServiceRequest_AssignPreferences();
                                   serviceObj.AthleteRelayPreferencesViewModel = AthleteRelayPrefrences;
                                   serviceObj.SeedPreferencesViewModel = seedVm;
                                   serviceObj.MeetSetupID = MeetSetupList.FirstOrDefault().MeetID;

                                   AuthorizedResponse<AssignPreferencesResult> Response = await meetSetupManage.AssignPreferences(serviceObj);
                                   this.AthleteRelayPreferencesId = Response.Result.AssignPreferencesID;
                               }
                               else
                               {
                               }
                               await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {
                                    Window window = view as Window;
                                    if (window != null)
                                    {
                                        seedVm = null;
                                        AthleteRelayPrefrences = null;
                                        window.Visibility = Visibility.Hidden;
                                    }
                                }));
                           });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {

                    Window window = view as Window;
                    if (window != null)
                    {
                        seedVm = null;
                        AthleteRelayPrefrences = null;
                        window.Visibility = Visibility.Hidden;
                    }
                }));

            });

            _AddAthletPrefrencesCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new Window();
                    AthleteRelayPreferencesView uc = new AthleteRelayPreferencesView();
                    window = new Window();
                    window.Content = uc;
                    window.WindowStyle = WindowStyle.None;
                    window.Name = "AthletsWindow";
                    window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    Window win = view as Window;
                    if (win != null)
                        window.Style = win.FindResource("DialogStyleInner") as Style;
                    window.ResizeMode = ResizeMode.CanResizeWithGrip;
                    //window.SizeToContent = SizeToContent.WidthAndHeight;
                    window.MouseLeftButtonDown += Window_MouseLeftButtonDown;

                    window.Title = "Athlete Relay Preferences";

                    AthleteRelayPreferencesViewModel AthleteRelayPrefrencesModel = new AthleteRelayPreferencesViewModel(uc, meetSetupManage, meetSetupController);
                    if (AthleteRelayPrefrencesModel != null)
                    {
                        AthleteRelayPrefrencesModel.ShowCommand = Visibility.Visible;
                        AthleteRelayPrefrencesModel.CallingFromMeet = true;
                        dialog.BindView(window);
                        dialog.BindViewModel(AthleteRelayPrefrencesModel);
                    }
                    dialog.ShowDialog();
                    this.AthleteRelayPrefrences = AthleteRelayPrefrencesModel;
                    AthletList = new AthleteRelayPrefrencesList();
                }));

            });

            _AddSeedPrefrencesCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {

                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new Window();
                    SeedPreferencesView ucSeed = new SeedPreferencesView();
                    window = new Window();
                    window.Content = ucSeed;
                    window.WindowStyle = WindowStyle.None;
                    window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    Window win = view as Window;
                    if (win != null)
                        window.Style = win.FindResource("DialogStyleInner") as Style;
                    window.ResizeMode = ResizeMode.CanResizeWithGrip;
                    //window.SizeToContent = SizeToContent.WidthAndHeight;
                    window.Name = "SeedPreferences";
                    window.Title = "Seeding Preferences";
                    window.MouseLeftButtonDown += Window_MouseLeftButtonDown;
                    SeedPreferencesViewModel seedprefrences = new SeedPreferencesViewModel(unity, ucSeed, meetSetupManage, meetSetupController);
                    if (seedprefrences != null)
                    {
                        seedprefrences.ShowCommand = Visibility.Visible;
                        seedprefrences.CalledFromAssign = true;
                        seedprefrences.GetEntity();
                        dialog.BindView(window);
                        dialog.BindViewModel(seedprefrences);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    seedprefrences.SetEntity(null);
                    SeedList = new SeedingPrefrencesList();
                    this.seedVm = seedprefrences;
                }));

            });

            ItemsDragDropCommand = new RelayCommand<DataGridDragDropEventArgs>((args) => DragDropItem(args), (args) => args != null && args.TargetObject != null && args.DroppedObject != null && args.Effects != System.Windows.DragDropEffects.None);
            IGenericInteractionView<SeedPreferencesViewModel> seedprefrences1 = unityContainer.Resolve<ISeedPreferencesViewModel>() as IGenericInteractionView<SeedPreferencesViewModel>;
            if (seedprefrences1 != null)
            {
                seedprefrences1.SetEntity(null);
            }

            IGenericInteractionView<AthleteRelayPreferencesViewModel> Athletprefrences = unityContainer.Resolve<IAthleteRelayPreferencesViewModel>() as IGenericInteractionView<AthleteRelayPreferencesViewModel>;
            if (Athletprefrences != null)
            {
                Athletprefrences.SetEntity(null);
            }
            this.GetAllScoringPreferencesList();
            _AddTemplateCommand = new DelegateCommand(AddTemplateCommandHandler);
            _UpdateTemplateCommand = new DelegateCommand(UpdateTemplateCommandHandler);
            _DeleteTemplateCommand = new DelegateCommand(DeleteTemplateCommandHandler);

            _AddScoringSetupCommand = new DelegateCommand(CreateScoringCommandHandler);
            _UpdateScoringCommand = new DelegateCommand(UpdateScoringCommandHandler);
            _DeleteScoringCommand = new DelegateCommand(DeleteScoringCommandHandler);
            GetAllScoringSetupTemplateList();
            View.DataContext = this;
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            Window win = sender as Window;
            win.Visibility = Visibility.Hidden;
        }

        private void GetAllScoringSetupTemplateList()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                ScoringSetupList = new ObservableCollection<ServiceRequest_ScoringSetup>();
                List<ServiceRequest_ScoringSetup> Response = await MeetSetupManage.GetAllScoringSetupTemplate();
                foreach (var item in Response)
                {
                    ScoringSetupList.Add(item);
                }
            })).Wait();
        }
        private void DeleteScoringCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedScoringSetup != null)
                {
                    AuthorizedResponse<ScoringSetupResult> Response = await MeetSetupManage.DeletScoringSetupTemplate(SelectedScoringSetup.TemplateOrMeetSetupID);
                    this.ScoringSetupList.Remove(SelectedScoringSetup);
                }
            })).Wait();
        }

        private void UpdateScoringCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedScoringSetup == null) return;
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<IScoringSetupView>() as Window;
                IGenericInteractionView<ServiceRequest_ScoringSetup> createDesignDataSetViewModel = unityContainer.Resolve<IScoringSetupViewModel>() as IGenericInteractionView<ServiceRequest_ScoringSetup>;
                if (this.SelectedScoringSetup != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedScoringSetup);
                    ScoringSetupViewModel setupVM = createDesignDataSetViewModel as ScoringSetupViewModel;
                    if (setupVM != null)
                    {
                        setupVM.IsReadonly = true;
                    }
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                dialog.Close();
                //SelectedScoringSetup = createDesignDataSetViewModel.GetEntity();
                //if (SelectedScoringSetup != null)
                //{
                //    AuthorizedResponse<ScoringSetupResult> Response = await MeetSetupManage.CreateScoringMeetTemplate(SelectedScoringSetup);
                //    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                //    {
                //        MessageBox.Show(Response.Message);
                //    }
                //}
            })).Wait();
        }

        private void CreateScoringCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<IScoringSetupView>() as Window;
                IGenericInteractionView<ServiceRequest_ScoringSetup> createDesignDataSetViewModel = unityContainer.Resolve<IScoringSetupViewModel>() as IGenericInteractionView<ServiceRequest_ScoringSetup>;
                if (createDesignDataSetViewModel != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedScoringSetup);
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                dialog.Close();
                SelectedScoringSetup = createDesignDataSetViewModel.GetEntity();
                ScoringSetupView scoringsetup = window as ScoringSetupView;
                if (scoringsetup != null)
                {
                    ScoringSetupViewModel vm = scoringsetup.DataContext as ScoringSetupViewModel;
                    if (vm != null)
                        if (vm.IsInCloseMode) return;
                }
                TemplateNameView templateView = new TemplateNameView();
                templateView.Height = 200;
                templateView.Width = 300;
                templateView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                templateView.ShowDialog();
                if (string.IsNullOrEmpty(templateView.TemplateName)) return;
                SelectedScoringSetup.TemplateName = templateView.TemplateName;
                MessageBoxResult result = MessageBox.Show("Do you wants this Template as Default Template", "Default Template", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    SelectedScoringSetup.IsDefault = true;
                }
                else
                {
                    SelectedScoringSetup.IsDefault = false;
                }
                if (SelectedScoringSetup != null)
                {
                    this.ScoringSetupList.Add(SelectedScoringSetup);
                }
                if (SelectedScoringSetup != null)
                {
                    AuthorizedResponse<ScoringSetupResult> Response = await MeetSetupManage.CreateScoringMeetTemplate(SelectedScoringSetup);
                    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show(Response.Message);
                    }
                }
            })).Wait();
        }

        private void DeleteTemplateCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedScoringPreference != null)
                {
                    AuthorizedResponse<ScoringPreferencesResult> Response = await MeetSetupManage.DeletScoringPreferencesTemplate(SelectedScoringPreference.ScroringPreferencesTemplatID);
                    this.ScoringPreferencesList.Remove(SelectedScoringPreference);
                }
            })).Wait();
        }

        private void UpdateTemplateCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedScoringPreference == null) return;
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<IEntryScoringPreferencesView>() as Window;
                if (this.SelectedScoringPreference != null)
                {
                    this.SelectedScoringPreference.Editable = false;
                    dialog.BindView(window);
                    dialog.BindViewModel(this.SelectedScoringPreference);
                }
                dialog.ShowDialog();
                dialog.Close();
            })).Wait();

        }

        private void AddTemplateCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<IEntryScoringPreferencesView>() as Window;
                IGenericInteractionView<EntryScoringPreferencesViewModel> createDesignDataSetViewModel = unityContainer.Resolve<IEntryScoringPreferencesViewModel>() as IGenericInteractionView<EntryScoringPreferencesViewModel>;
                if (createDesignDataSetViewModel != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedScoringPreference);
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                dialog.Close();
                SelectedScoringPreference = createDesignDataSetViewModel.GetEntity();
                if (SelectedScoringPreference.IsInCloseMode) return;
                TemplateNameView templateView = new TemplateNameView();
                templateView.Height = 200;
                templateView.Width = 300;
                templateView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                templateView.ShowDialog();
                SelectedScoringPreference.ScroringPreferencesTemplateName = templateView.TemplateName;
                if (SelectedScoringPreference != null)
                {
                    this.ScoringPreferencesList.Add(SelectedScoringPreference);
                }
                if (SelectedScoringPreference != null)
                {
                    ServiceRequest_ScoringPreferences scoringPreferences = new ServiceRequest_ScoringPreferences();
                    scoringPreferences.ScoringPreferencesViewModel = SelectedScoringPreference;
                    AuthorizedResponse<ScoringPreferencesResult> Response = await MeetSetupManage.CreateScoringPreferencesTemplate(scoringPreferences);
                    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show(Response.Message);
                    }
                }
            })).Wait();
        }

        private async void DragDropItem(DataGridDragDropEventArgs args)
        {
            ListViewItem listViewItem = args.DroppedObject as ListViewItem;
            if (listViewItem != null)
            {
                AssignedSeedVisibility = Visibility.Visible;
                AssignedRelayPreferencesVisibility = Visibility.Visible;
                AthleteRelayPrefrencesGroup group = listViewItem.DataContext as AthleteRelayPrefrencesGroup;
                BindAthletGroup(group);
                SeedingPrefrencesGroup seedgroup = listViewItem.DataContext as SeedingPrefrencesGroup;
                BindSeedingPrefrencesGroup(seedgroup);
            }
            else if (args.DroppedObject.GetType() == typeof(EntryScoringPreferencesViewModel))
            {
                EntryScoringPreferencesViewModel entryScoring = args.DroppedObject as EntryScoringPreferencesViewModel;
                if (entryScoring != null)
                {
                    ServiceRequest_ScoringPreferences scoringPreferences = new ServiceRequest_ScoringPreferences();
                    scoringPreferences.ScoringPreferencesViewModel = entryScoring;
                    MeetSetupModel meetsetup = this.MeetSetupList.FirstOrDefault();
                    if (meetsetup != null)
                    {
                        scoringPreferences.TemplateOrMeetSetupID = meetsetup.MeetID;
                        AuthorizedResponse<ScoringPreferencesResult> Response = await MeetSetupManage.AssignScoringPreferencesMeet(scoringPreferences);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show(Response.Message);
                        }
                    }

                }
            }
            else if (args.DroppedObject.GetType() == typeof(ServiceRequest_ScoringSetup))
            {
                ServiceRequest_ScoringSetup entryScoring = args.DroppedObject as ServiceRequest_ScoringSetup;
                if (entryScoring != null)
                {
                    ServiceRequest_ScoringSetup scoringPreferences = new ServiceRequest_ScoringSetup();
                    scoringPreferences.ScoringSetupTemplateList = entryScoring.ScoringSetupTemplateList;
                    MeetSetupModel meetsetup = this.MeetSetupList.FirstOrDefault();
                    if (meetsetup != null)
                    {
                        scoringPreferences.TemplateOrMeetSetupID = meetsetup.MeetID;
                        scoringPreferences.TemplateOrMeetSetupID = meetsetup.MeetID;
                        AuthorizedResponse<ScoringSetupResult> Response = await MeetSetupManage.AssignScoringMeetTemplate(scoringPreferences);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show(Response.Message);
                        }
                    }

                }
            }

        }
        private void BindAthletGroup(AthleteRelayPrefrencesGroup group)
        {
            AthleteRelayPrefrences = unityContainer.Resolve<IAthleteRelayPreferencesViewModel>() as AthleteRelayPreferencesViewModel;
            if (group != null)
            {
                AthleteRelayPrefrences.ShowCommand = Visibility.Collapsed;
                this.BindAthletRelayGroup(group.AthletePreferencesList);
                this.BindCompetitorNumbersList(group.CompetitorNumbersList);
                this.BindRelayPreferencesList(group.RelayPreferencesList);
            }
            else
            {
                AthleteRelayPreferencesViewModel AthleteRelayPrefrences = unityContainer.Resolve<IAthleteRelayPreferencesViewModel>() as AthleteRelayPreferencesViewModel;
                if (AthleteRelayPrefrences != null)
                {
                    AthleteRelayPrefrences.ShowCommand = Visibility.Collapsed;
                    foreach (var item in AthleteRelayPrefrences.AthletePreferencesOptions)
                    {
                        item.SetIsSelected(false);
                    }
                    foreach (var item in AthleteRelayPrefrences.CompetitorNumbersOptions)
                    {
                        item.SetIsSelected(false);
                    }
                    foreach (var item in AthleteRelayPrefrences.RelayPreferencesOptions)
                    {
                        item.SetIsSelected(false);
                    }
                }
            }
        }

        private void BindSeedingPrefrencesGroup(SeedingPrefrencesGroup seedgroup)
        {
            seedVm = unityContainer.Resolve<ISeedPreferencesViewModel>() as SeedPreferencesViewModel;
            if (seedgroup != null)
            {
                seedVm.ShowCommand = Visibility.Collapsed;
                this.BindDualMeetList(seedgroup.DualMeetList, seedgroup.SchoolDetailList);
                this.BindRendomizationRule(seedgroup.RendomizationRule);
                this.BindSeedingRules(seedgroup.SeedingRules);
                this.BindStandardAlleyPrefrencesList(seedgroup.StandardAlleyPrefrencesList);
                this.BindStandardLanePrefrencesList(seedgroup.StandardLanePrefrencesList);
                this.BindWaterfallStartPreferencesList(seedgroup.WaterfallStartPreferencesList);
                this.BindMeetAssignment(seedgroup.MeetAssignment);
            }
            else
            {
                SeedPreferencesViewModel seedprefrences1 = unityContainer.Resolve<ISeedPreferencesViewModel>() as SeedPreferencesViewModel;
                if (seedprefrences1 != null)
                {
                    seedprefrences1.ShowCommand = Visibility.Collapsed;
                    seedprefrences1.LaneDetailList = new ObservableCollection<LaneDetailModel>();
                    seedprefrences1.CloseGap = false;
                    seedprefrences1.RoundFirstMultipleRound = string.Empty;
                    seedprefrences1.RoundSecondThirdAndForth = string.Empty;
                    seedprefrences1.TimedFinalEvents = string.Empty;
                    seedprefrences1.AllowExhibitionAthletesInFinal = false;
                    seedprefrences1.AllowForeignAthletesInFinal = false;
                    seedprefrences1.ApplyNCAARule = false;
                    seedprefrences1.SeedExhibitionAthletesLast = false;
                    seedprefrences1.UseSpecialRandomSelectMethod = false;
                    if (seedprefrences1.StandardAlleyPrefrences != null)
                    {
                        seedprefrences1.StandardAlleyPrefrences.StandardAlleyPrefrenceList = new ObservableCollection<StandardAlleyPrefrence>();
                    }
                    if (seedprefrences1.StandardLanePrefrences != null)
                    {
                        seedprefrences1.StandardLanePrefrences.StandardLanePrefrenceList = new ObservableCollection<StandardLanePrefrence>();
                    }
                    if (seedprefrences1.WaterFallPreferences != null)
                    {
                        seedprefrences1.WaterFallPreferences.WaterfallStartList = new ObservableCollection<WaterfallStartPreference>();
                    }
                    seedprefrences1.AlternamtUserOfUnAssignedLane = false;
                    seedprefrences1.StrictAssignmentAllHeats = false;
                    seedprefrences1.StrictAssignmentFastestHeatOnly = false;
                    seedprefrences1.UseLaneAssignmentsForInLaneRacesOnly = false;
                    seedprefrences1.UserLaneOrPositionAssignmentsAbove = false;
                }
            }
        }
        private void BindMeetAssignment(DualMeetAssignmentTransaction meetAssignment)
        {
            seedVm.AlternamtUserOfUnAssignedLane = meetAssignment.AlternamtUserOfUnAssignedLane == null ? false : meetAssignment.AlternamtUserOfUnAssignedLane.Value;
            seedVm.StrictAssignmentAllHeats = meetAssignment.StrictAssignmentAllHeats == null ? false : meetAssignment.StrictAssignmentAllHeats.Value;
            seedVm.StrictAssignmentFastestHeatOnly = meetAssignment.StrictAssignmentFastestHeatOnly == null ? false : meetAssignment.StrictAssignmentFastestHeatOnly.Value;
            seedVm.UseLaneAssignmentsForInLaneRacesOnly = meetAssignment.UseLaneAssignmentsForInLaneRacesOnly == null ? false : meetAssignment.UseLaneAssignmentsForInLaneRacesOnly.Value;
            seedVm.UserLaneOrPositionAssignmentsAbove = meetAssignment.UserLaneOrPositionAssignmentsAbove == null ? false : meetAssignment.UserLaneOrPositionAssignmentsAbove.Value;
        }

        private void BindWaterfallStartPreferencesList(ObservableCollection<WaterfallStartPreference> waterfallStartPreferencesList)
        {
            seedVm.WaterFallPreferences = new WaterfallStartViewModel(null, MeetSetupManage, MeetSetupStateController);
            foreach (var item in waterfallStartPreferencesList)
            {
                seedVm.WaterFallPreferences.WaterfallStartList.Where(p => p.Rank == item.Rank).ToList().ForEach(p => p.Position = item.Position);
            }
        }

        private void BindStandardLanePrefrencesList(ObservableCollection<StandardLanePrefrence> standardLanePrefrencesList)
        {
            seedVm.StandardLanePrefrences = new StandardLanePrefrencesViewModel(null, MeetSetupManage, MeetSetupStateController);
            foreach (var item in standardLanePrefrencesList)
            {
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.EightsLine = item.EightsLine);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.FifthLine = item.FifthLine);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.FirstLane = item.FirstLane);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.ForthLine = item.ForthLine);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.NinthLine = item.NinthLine);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.SecondLine = item.SecondLine);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.SeventhLine = item.SeventhLine);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.SixthLine = item.SixthLine);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.TenthLine = item.TenthLine);
                seedVm.StandardLanePrefrences.StandardLanePrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.ThirdLine = item.ThirdLine);
            }
        }

        private void BindStandardAlleyPrefrencesList(ObservableCollection<StandardAlleyPrefrence> standardAlleyPrefrencesList)
        {
            seedVm.StandardAlleyPrefrences = new StandardAlleyPrefrencesViewModel(null, MeetSetupManage, MeetSetupStateController);
            foreach (var item in standardAlleyPrefrencesList)
            {
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.EightsLine = item.EightsLine);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.FifthLine = item.FifthLine);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.FirstLane = item.FirstLane);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.ForthLine = item.ForthLine);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.NinthLine = item.NinthLine);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.SecondLine = item.SecondLine);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.SeventhLine = item.SeventhLine);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.SixthLine = item.SixthLine);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.TenthLine = item.TenthLine);
                seedVm.StandardAlleyPrefrences.StandardAlleyPrefrenceList.Where(p => p.PreferencesName.Trim() == item.PreferencesName.Trim()).ToList().ForEach(p => p.ThirdLine = item.ThirdLine);
            }
        }

        private void BindSeedingRules(MeetSetupSeedingRule seedingRules)
        {
            if (seedingRules != null)
            {
                seedVm.AllowExhibitionAthletesInFinal = seedingRules.AllowExhibitionAthletesInFinal == null ? false : seedingRules.AllowExhibitionAthletesInFinal.Value;
                seedVm.AllowForeignAthletesInFinal = seedingRules.AllowForeignAthletesInFinal == null ? false : seedingRules.AllowForeignAthletesInFinal.Value;
                seedVm.ApplyNCAARule = seedingRules.ApplyNCAARule == null ? false : seedingRules.ApplyNCAARule.Value;
                seedVm.SeedExhibitionAthletesLast = seedingRules.SeedExhibitionAthletesLast == null ? false : seedingRules.SeedExhibitionAthletesLast.Value;
                seedVm.UseSpecialRandomSelectMethod = seedingRules.UseSpecialRandomSelectMethod == null ? false : seedingRules.UseSpecialRandomSelectMethod.Value;
            }
        }

        private void BindRendomizationRule(RendomizationRuleTransaction rendomizationRule)
        {
            if (rendomizationRule != null)
            {
                seedVm.CloseGap = rendomizationRule.CloseGap == null ? false : rendomizationRule.CloseGap.Value;
                seedVm.RoundFirstMultipleRound = rendomizationRule.RoundFirstMultipleRound;
                seedVm.RoundSecondThirdAndForth = rendomizationRule.RoundSecondThirdAndForth;
                seedVm.TimedFinalEvents = rendomizationRule.TimedFinalEvents;
            }
        }

        private void BindDualMeetList(ObservableCollection<DualMeetSchoolTransaction> dualMeetList, ObservableCollection<SchoolDetail> schoolDetailList)
        {
            seedVm.LaneDetailList = new ObservableCollection<LaneDetailModel>();
            foreach (var item in dualMeetList)
            {
                LaneDetailModel lineDetail = new LaneDetailModel();
                lineDetail.LaneID = item.LaneNumber.Value;
                lineDetail.SchoolName = item.SchoolName;
                SchoolDetail school = schoolDetailList.Where(p => p.SchoolName == item.SchoolName).FirstOrDefault();
                if (school != null)
                {
                    lineDetail.SchoolID = school.SchoolID;
                }
                else
                {
                    MessageBox.Show("School does not found");
                }
                seedVm.LaneDetailList.Add(lineDetail);

            }
        }

        private void BindRelayPreferencesList(ObservableCollection<Option> relayPreferencesList)
        {
            if (AthleteRelayPrefrences != null)
            {
                AthleteRelayPrefrences.RelayPreferencesOptions.ToList().ForEach(p => p.IsSelected = false);
                foreach (var item in relayPreferencesList)
                {
                    AthleteRelayPrefrences.RelayPreferencesOptions.Where(p => p.Display == item.Display).ToList().ForEach(p => p.IsSelected = true);
                }
            }
        }

        private void BindCompetitorNumbersList(ObservableCollection<Option> competitorNumbersList)
        {
            if (AthleteRelayPrefrences != null)
            {
                AthleteRelayPrefrences.CompetitorNumbersOptions.ToList().ForEach(p => p.IsSelected = false);
                foreach (var item in competitorNumbersList)
                {
                    AthleteRelayPrefrences.CompetitorNumbersOptions.Where(p => p.Display == item.Display).ToList().ForEach(p => p.IsSelected = true);
                }
            }
        }

        private void BindAthletRelayGroup(ObservableCollection<Option> PreferencesList)
        {
            if (AthleteRelayPrefrences != null)
            {
                AthleteRelayPrefrences.AthletePreferencesOptions.ToList().ForEach(p => p.IsSelected = false);
                foreach (var item in PreferencesList)
                {
                    AthleteRelayPrefrences.AthletePreferencesOptions.Where(p => p.Display == item.Display).ToList().ForEach(p => p.IsSelected = true);
                }
            }
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender);
            if (window != null)
            {
                if (e.ButtonState == MouseButtonState.Pressed)
                    window.DragMove();
            }
        }

        private void FillAthletsRelayPrefrences()
        {

        }

        private bool ValidateInputs()
        {
            return true;

        }
        private void GetAllScoringPreferencesList()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<EntryScoringPreferencesViewModel> Response = await MeetSetupManage.GetAllScoringPreferencesTemplate();
                foreach (var item in Response)
                {
                    ScoringPreferencesList.Add(item);
                }
            })).Wait();
        }
        public void SetEntity(MeetSetupModel entity)
        {
            MeetSetupList.Clear();
            if (entity != null)
            {
                MeetSetupList.Add(entity);
            }
            if (entity.SeedingPrefrencesGroup != null)
            {
                SeedGroupVisibility = Visibility.Collapsed;
                AssignedSeedVisibility = Visibility.Visible;
            }
            else
            {
                SeedGroupVisibility = Visibility.Visible;
                AssignedSeedVisibility = Visibility.Collapsed;
            }
            if (entity.AthleteRelayPrefrencesGroup != null)
            {
                AthleteGroupVisibility = Visibility.Collapsed;
                AssignedRelayPreferencesVisibility = Visibility.Visible;
            }
            else
            {
                AssignedRelayPreferencesVisibility = Visibility.Collapsed;
                AthleteGroupVisibility = Visibility.Visible;
            }
            if (entity.AthleteRelayPrefrencesGroup != null && entity.SeedingPrefrencesGroup != null)
            {
                AssignGroupVisibility = Visibility.Collapsed;
            }
            else
            {
                AssignGroupVisibility = Visibility.Visible;
            }
            BindAthletGroup(entity.AthleteRelayPrefrencesGroup);
            BindSeedingPrefrencesGroup(entity.SeedingPrefrencesGroup);
        }

        private EntryScoringPreferencesViewModel _SelectedScoringPreference;
        public EntryScoringPreferencesViewModel SelectedScoringPreference
        {
            get { return _SelectedScoringPreference; }
            set
            {
                _SelectedScoringPreference = value;
                base.OnPropertyChanged("SelectedScoringPreference");
            }
        }

        private ObservableCollection<EntryScoringPreferencesViewModel> _ScoringPreferencesList = new ObservableCollection<EntryScoringPreferencesViewModel>();
        public ObservableCollection<EntryScoringPreferencesViewModel> ScoringPreferencesList
        {
            get { return _ScoringPreferencesList; }
            set
            {
                _ScoringPreferencesList = value;
                base.OnPropertyChanged("ScoringPreferencesList");
            }
        }

        private Visibility _AthleteGroup = Visibility.Visible;
        public Visibility AthleteGroupVisibility
        {
            get { return _AthleteGroup; }
            set
            {
                SetProperty(ref _AthleteGroup, value);
            }
        }

        private Visibility _AssignedRelayPreferencesVisibility = Visibility.Visible;
        public Visibility AssignedRelayPreferencesVisibility
        {
            get { return _AssignedRelayPreferencesVisibility; }
            set
            {
                SetProperty(ref _AssignedRelayPreferencesVisibility, value);
            }
        }

        private Visibility _AssignedSeedVisibility = Visibility.Visible;
        public Visibility AssignedSeedVisibility
        {
            get { return _AssignedSeedVisibility; }
            set
            {
                SetProperty(ref _AssignedSeedVisibility, value);
            }
        }

        private Visibility _SeedGroup = Visibility.Visible;
        public Visibility SeedGroupVisibility
        {
            get { return _SeedGroup; }
            set
            {
                SetProperty(ref _SeedGroup, value);
            }
        }

        private Visibility _AssignGroupVisibility = Visibility.Visible;
        public Visibility AssignGroupVisibility
        {
            get { return _AssignGroupVisibility; }
            set
            {
                SetProperty(ref _AssignGroupVisibility, value);
            }
        }

        private ObservableCollection<MeetSetupModel> _MeetSetupList = new ObservableCollection<MeetSetupModel>();
        public ObservableCollection<MeetSetupModel> MeetSetupList
        {
            get { return _MeetSetupList; }
            set
            {
                SetProperty(ref _MeetSetupList, value);
            }
        }
        private AthleteRelayPrefrencesList _athletList = new AthleteRelayPrefrencesList();
        public AthleteRelayPrefrencesList AthletList
        {
            get { return _athletList; }
            set
            {
                SetProperty(ref _athletList, value);
            }
        }

        private SeedingPrefrencesList _seedList = new SeedingPrefrencesList();
        public SeedingPrefrencesList SeedList
        {
            get { return _seedList; }
            set
            {
                SetProperty(ref _seedList, value);
            }
        }



        private bool _VisibleAssignRegion = false;
        public bool VisibleAssignRegion
        {
            get { return _VisibleAssignRegion; }
            set
            {
                SetProperty(ref _VisibleAssignRegion, value);
            }
        }

        private ObservableCollection<ServiceRequest_ScoringSetup> _ScoringSetupList = new ObservableCollection<ServiceRequest_ScoringSetup>();
        public ObservableCollection<ServiceRequest_ScoringSetup> ScoringSetupList
        {
            get { return _ScoringSetupList; }
            set
            {
                SetProperty(ref _ScoringSetupList, value);
            }
        }

        private ServiceRequest_ScoringSetup _SelectedScoringSetup = new ServiceRequest_ScoringSetup();
        public ServiceRequest_ScoringSetup SelectedScoringSetup
        {
            get { return _SelectedScoringSetup; }
            set
            {
                SetProperty(ref _SelectedScoringSetup, value);
            }
        }

        public MeetSetupModel GetEntity()
        {

            MeetSetupModel entity = new MeetSetupModel();
            return entity;
        }

        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }
        private ICommand _AddAthletPrefrencesCommand;
        public ICommand AddAthletPrefrencesCommand
        {
            get { return _AddAthletPrefrencesCommand; }
        }


        private ICommand _AddSeedPrefrencesCommand;
        public ICommand AddSeedPrefrencesCommand
        {
            get { return _AddSeedPrefrencesCommand; }
        }
        public RelayCommand<DataGridDragDropEventArgs> ItemsDragDropCommand { get; private set; }

        private ICommand _AddTemplateCommand;
        public ICommand AddTemplateCommand
        {
            get
            {
                return _AddTemplateCommand;
            }
        }

        private ICommand _UpdateTemplateCommand;
        public ICommand UpdateTemplateCommand
        {
            get
            {
                return _UpdateTemplateCommand;
            }
        }

        private ICommand _DeleteTemplateCommand;
        public ICommand DeleteTemplateCommand
        {
            get
            {
                return _DeleteTemplateCommand;
            }
        }

        private ICommand _AddScoringSetupCommand;
        public ICommand AddScoringSetupCommand
        {
            get
            {
                return _AddScoringSetupCommand;
            }
        }

        private ICommand _UpdateScoringCommand;
        public ICommand UpdateScoringCommand
        {
            get
            {
                return _UpdateScoringCommand;
            }
        }

        private ICommand _DeleteScoringCommand;
        public ICommand DeleteScoringCommand
        {
            get
            {
                return _DeleteScoringCommand;
            }
        }

        public bool Response { get; private set; }
    }
}

