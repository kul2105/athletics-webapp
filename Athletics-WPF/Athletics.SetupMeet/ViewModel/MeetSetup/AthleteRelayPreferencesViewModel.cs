﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Athletics.ModuleCommon;
using System.Windows.Controls;
using Athletics.SetupMeet.View.AssigPrefrences;

namespace Athletics.MeetSetup.ViewModels
{

    public class AthleteRelayPreferencesViewModel : BindableBase, IAthleteRelayPreferencesViewModel, IGenericInteractionView<AthleteRelayPreferencesViewModel>
    {
        private IAthleteRelayPreferencesView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        public AthleteRelayPreferencesViewModel(IAthleteRelayPreferencesView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;
            _PART_CLOSE = new DelegateCommand(PART_CLOSE_Click);
            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            //  _RadioButtonChecked = new DelegateCommand<RadioButton>(RadioButton_Checked);
            // _CheckBoxChecked = new DelegateCommand<CheckBox>(CheckBox_Checked);
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Task.Run(async () =>
                           {
                               if (AthleteRelayPreferencesId == 0)
                               {
                                   ServiceRequest_AthleteRelayPrefrences athrelayPrefServiceObj = new ServiceRequest_AthleteRelayPrefrences();
                                   athrelayPrefServiceObj.AthletePreferencesList = new List<int>();
                                   foreach (var item in AthletePreferencesOptions.Where(p => p.IsSelected == true))
                                   {
                                       athrelayPrefServiceObj.AthletePreferencesList.Add(item.DisplayID);
                                   }

                                   athrelayPrefServiceObj.CompetitorNumberList = new List<int>();
                                   foreach (var item in CompetitorNumbersOptions.Where(p => p.IsSelected == true))
                                   {
                                       athrelayPrefServiceObj.CompetitorNumberList.Add(item.DisplayID);
                                   }

                                   athrelayPrefServiceObj.RelayPreferencesList = new List<int>();
                                   foreach (var item in RelayPreferencesOptions.Where(p => p.IsSelected == true))
                                   {
                                       athrelayPrefServiceObj.RelayPreferencesList.Add(item.DisplayID);
                                   }

                                   if (athrelayPrefServiceObj.AthletePreferencesList.Count <= 0 || athrelayPrefServiceObj.CompetitorNumberList.Count <= 0 || athrelayPrefServiceObj.RelayPreferencesList.Count <= 0)
                                   {
                                       MessageBox.Show("Please select atlest one Prefrences to add", "No Prefrences Selected");
                                       return;
                                   }

                                   AuthorizedResponse<AthleteRelayPrefrencesResult> Response = await meetSetupManage.CreateNewAthletePreference(athrelayPrefServiceObj);
                                   this.AthleteRelayPreferencesId = Response.Result.AthleteRelayPrefrencesID;
                                   if (this.CallingFromMeet)
                                   {
                                       MessageBoxResult msgResult = MessageBox.Show("Do you want(s) Meet to Save as Template", "Save as Template", MessageBoxButton.YesNo);
                                       if (msgResult == MessageBoxResult.Yes)
                                       {
                                           await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                            {
                                                TemplateNameView templateNameView = new TemplateNameView();
                                                templateNameView.Height = 200;
                                                templateNameView.Width = 300;
                                                templateNameView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                                                templateNameView.ShowDialog();
                                                string templateName = templateNameView.TemplateName;
                                                ServiceRequest_TemplateName request = new ServiceRequest_TemplateName();
                                                request.Preferenceid = this.AthleteRelayPreferencesId;
                                                request.TemplateName = templateName;
                                                await meetSetupManage.CreateRelayPreferenceTemplate(request);
                                                templateNameView.Close();
                                            }));
                                       }
                                   }
                                   else
                                   {
                                       await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                        {
                                            TemplateNameView templateNameView = new TemplateNameView();
                                            templateNameView.Height = 200;
                                            templateNameView.Width = 300;
                                            templateNameView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                                            templateNameView.ShowDialog();
                                            string templateName = templateNameView.TemplateName;
                                            ServiceRequest_TemplateName request = new ServiceRequest_TemplateName();
                                            request.Preferenceid = this.AthleteRelayPreferencesId;
                                            request.TemplateName = templateName;
                                            await meetSetupManage.CreateRelayPreferenceTemplate(request);
                                            templateNameView.Close();
                                        }));
                                   }
                               }
                               else
                               {
                                   // AuthorizedResponse<MeetSetupResult> Response = await meetSetupManage.UpdateMeet(newAPIServiceRequest_MeetSetup);
                               }
                               await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {

                                    PART_CLOSE_Click();
                                }));
                           });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {

                    PART_CLOSE_Click();
                }));

            });

            foreach (var item in meetSetupManage.GetAllMeetAthletePreference().Result)
            {
                Option opt = new Option();
                opt.Display = item.AthletePreferencesName;
                opt.DisplayID = item.AthletePreferencesID;
                opt.GroupName = "MeetAthletePreference";
                opt.OptionChanged += Opt_OptionChangedRadio;
                AthletePreferencesOptions.Add(opt);
            }

            foreach (var item in meetSetupManage.GetAllMeetCompetitorNumber().Result)
            {
                Option opt = new Option();
                opt.Display = item.CompetitorNumbersName;
                opt.DisplayID = item.CompetitorNumberID;
                opt.Tag = item.IsSingleSelectionGroup.ToString();
                opt.GroupName = "MeetCompetitorNumber";
                opt.OptionChanged += Opt_OptionChanged;
                CompetitorNumbersOptions.Add(opt);
            }

            foreach (var item in meetSetupManage.GetAllMeetRelayPreference().Result)
            {
                Option opt = new Option();
                opt.Display = item.RelayPreferencesName;
                opt.DisplayID = item.RelayPreferencesID;
                opt.GroupName = "MeetRelayPreference";
                opt.OptionChanged += Opt_OptionChangedRadio;
                RelayPreferencesOptions.Add(opt);
            }


            View.DataContext = this;
        }

        private void Opt_OptionChangedRadio(Option option, bool IsSelected)
        {
            RadioButton_Checked(option);
        }

        private void Opt_OptionChanged(Option option, bool IsSelected)
        {
            bool isCheckBox = false;
            if (!string.IsNullOrEmpty(option.Tag))
            {
                bool.TryParse(option.Tag, out isCheckBox);
            }
            if (!isCheckBox)
            {
                CheckBox_Checked(option);
            }
            else
            {
                RadioButton_Checked(option);
            }
        }

        private readonly ICommand _PART_CLOSE;

        private bool ValidateInputs()
        {
            return true;
            //if (_RoleNameValid == true && _RoleIDValid == true)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        public void SetEntity(AthleteRelayPreferencesViewModel entity)
        {
            if (entity != null)
            {
                // this.SelectedMeetArenaOption = this.MeetArenaOptions.Where(p => p.Display == entity.MeetArenaOption).FirstOrDefault();
                //  this.SelectedMeetTypeDivisionOption = this.MeetTypeDivisionOptions.Where(p => p.Display == entity.MeetTypeDivisionOption).FirstOrDefault();
                // AthleteRelayPreferencesId = entity.MeetID;
            }
        }

        public AthleteRelayPreferencesViewModel GetEntity()
        {
            //if (IsInCloseMode == true)
            //{
            //    IsInCloseMode = false;
            //    return null;

            //}
            AthleteRelayPreferencesViewModel entity = new AthleteRelayPreferencesViewModel(View, MeetSetupManage, MeetSetupStateController);
            //entity.MeetStyleOption = this.SelectedMeetStyleOption.Display;
            //entity.MeetTypeDivisionOption = this.SelectedMeetTypeDivisionOption.Display;
            //entity.MeetID = this.AthleteRelayPreferencesId;
            return entity;
        }

        private ObservableCollection<Option> _AthletePreferencesOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> AthletePreferencesOptions
        {
            get { return _AthletePreferencesOptions; }
            set
            {
                SetProperty(ref _AthletePreferencesOptions, value);
            }
        }

        private Option _SelectedMeetStyleOption;
        public Option SelectedMeetStyleOption
        {
            get { return _SelectedMeetStyleOption; }
            set
            {
                SetProperty(ref _SelectedMeetStyleOption, value);
            }
        }
        private ObservableCollection<Option> _CompetitorNumbersOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> CompetitorNumbersOptions
        {
            get { return _CompetitorNumbersOptions; }
            set
            {
                SetProperty(ref _CompetitorNumbersOptions, value);
            }
        }

        private Option _SelectedMeetArenaOption;
        public Option SelectedMeetArenaOption
        {
            get { return _SelectedMeetArenaOption; }
            set
            {
                SetProperty(ref _SelectedMeetArenaOption, value);
            }
        }

        private ObservableCollection<Option> _RelayPreferencesOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> RelayPreferencesOptions
        {
            get { return _RelayPreferencesOptions; }
            set
            {
                SetProperty(ref _RelayPreferencesOptions, value);
            }
        }

        private Option _SelectedMeetTypeDivisionOption;
        public Option SelectedMeetTypeDivisionOption
        {
            get { return _SelectedMeetTypeDivisionOption; }
            set
            {
                SetProperty(ref _SelectedMeetTypeDivisionOption, value);
            }
        }

        private bool _CallingFromMeet = false;
        public bool CallingFromMeet
        {
            get { return _CallingFromMeet; }
            set
            {
                SetProperty(ref _CallingFromMeet, value);
            }
        }

        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        public ICommand PART_CLOSE
        {
            get
            {
                return _PART_CLOSE;
            }
        }
        //private ICommand _RadioButtonChecked;
        //public ICommand RadioButtonChecked
        //{
        //    get
        //    {
        //        return _RadioButtonChecked;
        //    }
        //}

        //private ICommand _CheckBoxChecked;
        //public ICommand CheckBoxChecked
        //{
        //    get
        //    {
        //        return _CheckBoxChecked;
        //    }
        //}
        private Visibility _ShowCommand = Visibility.Visible;
        public Visibility ShowCommand
        {
            get { return _ShowCommand; }
            set
            {
                SetProperty(ref _ShowCommand, value);
            }
        }

        private void PART_CLOSE_Click()
        {

            Window window = null;
            foreach (Window item in Application.Current.Windows)
            {
                if (item.Name == "AthletsWindow")
                {
                    window = item;
                }
            }
            if (window != null)
            {
                window.Close();
            }
        }

        private void CheckBox_Checked(Option checkBox)
        {
            //CheckBox checkBox = sender as CheckBox;
            //AthleteRelayPreferencesViewModel vm = DataContext as AthleteRelayPreferencesViewModel;
            switch (checkBox.Tag)
            {
                case "MeetAthletePreference":
                    AthletePreferencesOptions.Where(p => p.Display == checkBox.Display.ToString()).ToList().ForEach(p => p.SetIsSelected(true));
                    break;

                case "MeetCompetitorNumber":
                    CompetitorNumbersOptions.Where(p => p.Display == checkBox.Display.ToString()).ToList().ForEach(p => p.SetIsSelected(true));
                    break;

                case "MeetRelayPreference":
                    RelayPreferencesOptions.Where(p => p.Display == checkBox.Display.ToString()).ToList().ForEach(p => p.SetIsSelected(true));
                    break;

                default:
                    break;
            }
            AthletePreferencesOptions.Where(p => p.GroupName == checkBox.Tag).ToList().ForEach(p => p.RaiseEvent = true);
            CompetitorNumbersOptions.Where(p => p.GroupName == checkBox.Tag).ToList().ForEach(p => p.RaiseEvent = true);
            RelayPreferencesOptions.Where(p => p.GroupName == checkBox.Tag).ToList().ForEach(p => p.RaiseEvent = true);
        }

        private void RadioButton_Checked(Option radioButton)
        {
            //  RadioButton radioButton = sender as RadioButton;
            switch (radioButton.GroupName)
            {
                case "MeetAthletePreference":
                    AthletePreferencesOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.SetIsSelected(false));
                    AthletePreferencesOptions.Where(p => p.Display == radioButton.Display.ToString()).ToList().ForEach(p => p.SetIsSelected(true));
                    break;

                case "MeetCompetitorNumber":
                    CompetitorNumbersOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.SetIsSelected(false));
                    CompetitorNumbersOptions.Where(p => p.Display == radioButton.Display.ToString()).ToList().ForEach(p => p.SetIsSelected(true));
                    break;

                case "MeetRelayPreference":
                    RelayPreferencesOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.SetIsSelected(false));
                    RelayPreferencesOptions.Where(p => p.Display == radioButton.Display.ToString()).ToList().ForEach(p => p.SetIsSelected(true));
                    break;

                default:
                    break;
            }
            AthletePreferencesOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.RaiseEvent = true);
            CompetitorNumbersOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.RaiseEvent = true);
            RelayPreferencesOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.RaiseEvent = true);
        }


    }
}

