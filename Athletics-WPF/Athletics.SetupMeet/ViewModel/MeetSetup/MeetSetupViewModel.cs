﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Athletics.ModuleCommon;

namespace Athletics.MeetSetup.ViewModels
{
    public class MeetSetupViewModel : BindableBase, IMeetSetupViewModel, IGenericInteractionView<MeetSetupModel>
    {
        private IMeetSetupView View = null;
        private int MeetSetupId = 0;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        public MeetSetupViewModel(IMeetSetupView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;

            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            _CreateMeetCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            APIServiceRequest_MeetSetup newAPIServiceRequest_MeetSetup = new APIServiceRequest_MeetSetup();
                            if (string.IsNullOrEmpty(this.MeetName))
                            {
                                MessageBox.Show("Please enter Meet Name");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetSetupName = this.MeetName;
                            if (string.IsNullOrEmpty(this.MeetName2))
                            {
                                MessageBox.Show("Please enter Meet Name2");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetSetupName2 = this.MeetName2;
                            if (this.SelectedStartDate == null)
                            {
                                MessageBox.Show("Please select Start Date");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetSetupStartDate = this.SelectedStartDate;
                            if (this.SelectedEndDate == null)
                            {
                                MessageBox.Show("Please select End Date");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetSetupEndDate = this.SelectedEndDate;
                            if (this.SelectedAgeUpDate == null)
                            {
                                MessageBox.Show("Please select Age-up Date");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetSetupAgeUpDate = this.SelectedAgeUpDate;

                            newAPIServiceRequest_MeetSetup.UseDivisionBirthDateRange = this.IsUserDivisionBirthDate;
                            newAPIServiceRequest_MeetSetup.LinkTermToDivision = this.IsLinkTeamToDivision;
                            if (this.SelectedMeetTypeOption == null)
                            {
                                MessageBox.Show("Please check any Meet Type");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetTypeID = this.SelectedMeetTypeOption.DisplayID;
                            if (this.SelectedMeetStyleOption == null)
                            {
                                MessageBox.Show("Please check any Meet Style");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetStyleID = this.SelectedMeetStyleOption.DisplayID;
                            if (this.SelectedKindOfMeetOption == null)
                            {
                                MessageBox.Show("Please check any Meet Kind");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetKindID = this.SelectedKindOfMeetOption.DisplayID;
                            if (this.SelectedBaseCounty == null)
                            {
                                MessageBox.Show("Please select Base County");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetCountyID = this.SelectedBaseCounty.BaseCountyID;
                            if (this.SelectedMeetClassOption == null)
                            {
                                MessageBox.Show("Please check any Meet Class");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetClassID = this.SelectedMeetClassOption.DisplayID;
                            if (this.SelectedMeetArenaOption == null)
                            {
                                MessageBox.Show("Please check any Meet Arena");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetArenaID = this.SelectedMeetArenaOption.DisplayID;
                            if (this.SelectedMeetTypeDivisionOption == null)
                            {
                                MessageBox.Show("Please check any Meet Division Option");
                                return;
                            }
                            newAPIServiceRequest_MeetSetup.MeetTypeOptionID = this.SelectedMeetTypeDivisionOption.DisplayID;
                            MeetSetupStateController.MeetState = e_MEET_STATE.MEET_SETUP;

                            await Task.Run(async () =>
                            {
                                if (MeetSetupId == 0)
                                {
                                    AuthorizedResponse<MeetSetupResult> Response = await meetSetupManage.CreateNewMeet(newAPIServiceRequest_MeetSetup);
                                    this.MeetSetupId = Response.Result.MeetSetupID;

                                    //Application.Current.Dispatcher.Invoke(new Action(() =>
                                    //{
                                    //    if (Response.Status == eMEET_REQUEST_RESPONSE.SUCCESS)
                                    //    {
                                    //        ClearScreen();
                                    //    }
                                    //    else
                                    //    {
                                    //        ClearScreen();
                                    //    }
                                    //}));
                                }
                                else
                                {
                                    newAPIServiceRequest_MeetSetup.MeetSetupID = MeetSetupId;
                                    AuthorizedResponse<MeetSetupResult> Response = await meetSetupManage.UpdateMeet(newAPIServiceRequest_MeetSetup);
                                }
                                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                 {

                                     Window win = view as Window;
                                     if (win != null)
                                     {
                                         win.Visibility = Visibility.Collapsed;
                                     }
                                 }));
                            });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });


            _CancelMeetCommand = new DelegateCommand(async () =>
            {
                ClearScreen();
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    IsInCloseMode = true;
                    Window win = view as Window;
                    if (win != null)
                    {
                        win.Visibility = Visibility.Collapsed;
                    }
                }));

            });

            _AgeUPCommand = new DelegateCommand(async () =>
            {
                MeetSetupStateController.MeetState = e_MEET_STATE.MEET_SETUPCOMPLETED;
            });


            BaseCountyList = meetSetupManage.GetAllBaseCounty().Result;
            foreach (var item in meetSetupManage.GetAllMeetKind().Result)
            {
                Option opt = new Option();
                opt.Display = item.MeetKindName;
                opt.DisplayID = item.MeetKindID;
                KindOfMeetOptions.Add(opt);
            }

            foreach (var item in meetSetupManage.GetAllMeetClass().Result)
            {
                Option opt = new Option();
                opt.Display = item.MeetClassName;
                opt.DisplayID = item.MeetClassID;
                MeetClassOptions.Add(opt);
            }
            foreach (var item in meetSetupManage.GetAllMeetType().Result)
            {
                Option opt = new Option();
                opt.Display = item.MeetTypeName;
                opt.DisplayID = item.MeetTypeID;
                MeetTypeOptions.Add(opt);
            }
            foreach (var item in meetSetupManage.GetAllMeetStyle().Result)
            {
                Option opt = new Option();
                opt.Display = item.MeetStyleName;
                opt.DisplayID = item.MeetStyleID;
                MeetStyleOptions.Add(opt);
            }

            foreach (var item in meetSetupManage.GetAllMeetArena().Result)
            {
                Option opt = new Option();
                opt.Display = item.MeetArenaName;
                opt.DisplayID = item.MeetArenaID;
                MeetArenaOptions.Add(opt);
            }

            foreach (var item in meetSetupManage.GetAllMeetTypeDivision().Result)
            {
                Option opt = new Option();
                opt.Display = item.MeetTypeDivisionName;
                opt.DisplayID = item.MeetTypeDivisionID;
                MeetTypeDivisionOptions.Add(opt);
            }


            View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            return true;
            //if (_RoleNameValid == true && _RoleIDValid == true)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
        private void ClearScreen()
        {
            this.MeetName = string.Empty;
            this.MeetName2 = string.Empty;
            this.IsLinkTeamToDivision = false;
            this.SelectedMeetTypeOption = null;
            this.SelectedMeetStyleOption = null;
            this.SelectedKindOfMeetOption = null;
            this.SelectedBaseCounty = null;
            this.SelectedMeetClassOption = null;
            this.SelectedMeetArenaOption = null;
            this.SelectedMeetTypeOption = null;
        }

        public void SetEntity(MeetSetupModel entity)
        {
            if (entity != null)
            {
                this.MeetName = entity.MeetName;
                this.MeetName2 = entity.MeetName2;
                this.IsLinkTeamToDivision = entity.IsLinkTeamToDivision;
                this.IsUserDivisionBirthDate = entity.IsUserDivisionBirthDate;
                this.SelectedMeetArenaOption = this.MeetArenaOptions.Where(p => p.Display == entity.MeetArenaOption).FirstOrDefault();
                this.SelectedKindOfMeetOption = this.KindOfMeetOptions.Where(p => p.Display == entity.KindOfMeetOption).FirstOrDefault();
                this.SelectedMeetClassOption = this.MeetClassOptions.Where(p => p.Display == entity.MeetClassOption).FirstOrDefault();
                this.SelectedMeetStyleOption = this.MeetStyleOptions.Where(p => p.Display == entity.MeetStyleOption).FirstOrDefault();
                this.SelectedMeetTypeOption = this.MeetTypeOptions.Where(p => p.Display == entity.MeetTypeOption).FirstOrDefault();
                this.SelectedAgeUpDate = entity.AgeUpDate;
                this.SelectedStartDate = entity.StartDate;
                this.SelectedEndDate = entity.EndDate;
                this.SelectedBaseCounty = this.BaseCountyList.Where(p => p.BaseCountyName == entity.BaseCounty).FirstOrDefault();
                this.SelectedMeetTypeDivisionOption = this.MeetTypeDivisionOptions.Where(p => p.Display == entity.MeetTypeDivisionOption).FirstOrDefault();
                MeetSetupId = entity.MeetID;
            }
        }

        public MeetSetupModel GetEntity()
        {
            if (IsInCloseMode == true)
            {
                IsInCloseMode = false;
                return null;

            }
            MeetSetupModel entity = new MeetSetupModel();
            entity.AgeUpDate = this.SelectedAgeUpDate;
            if (this.SelectedBaseCounty != null)
                entity.BaseCounty = this.SelectedBaseCounty.BaseCountyName;
            entity.EndDate = this.SelectedEndDate;
            entity.IsLinkTeamToDivision = this.IsLinkTeamToDivision;
            entity.IsUserDivisionBirthDate = this.IsUserDivisionBirthDate;
            entity.KindOfMeetOption = this.SelectedKindOfMeetOption.Display;
            entity.MeetArenaOption = this.SelectedMeetArenaOption.Display;
            entity.MeetClassOption = this.SelectedMeetClassOption.Display;
            entity.MeetName = this.MeetName;
            entity.MeetName2 = this.MeetName2;
            entity.MeetStyleOption = this.SelectedMeetStyleOption.Display;
            entity.MeetTypeDivisionOption = this.SelectedMeetTypeDivisionOption.Display;
            entity.MeetTypeOption = this.SelectedMeetTypeOption.Display;
            entity.StartDate = this.SelectedStartDate;
            entity.MeetID = this.MeetSetupId;
            return entity;
        }

        private List<BaseCounty> _BaseCountyList;
        public List<BaseCounty> BaseCountyList
        {
            get { return _BaseCountyList; }
            set
            {
                SetProperty(ref _BaseCountyList, value);
            }
        }
        private BaseCounty _SelectedBaseCounty;
        public BaseCounty SelectedBaseCounty
        {
            get { return _SelectedBaseCounty; }
            set
            {
                SetProperty(ref _SelectedBaseCounty, value);
                ((DelegateCommand)CreateMeetCommand).RaiseCanExecuteChanged();
            }
        }
        private DateTime? _SelectedStartDate = DateTime.Now;
        public DateTime? SelectedStartDate
        {
            get { return _SelectedStartDate; }
            set
            {
                SetProperty(ref _SelectedStartDate, value);
                ((DelegateCommand)CreateMeetCommand).RaiseCanExecuteChanged();
            }
        }

        private DateTime? _SelectedEndDate = DateTime.Now;
        public DateTime? SelectedEndDate
        {
            get { return _SelectedEndDate; }
            set
            {
                SetProperty(ref _SelectedEndDate, value);
                ((DelegateCommand)CreateMeetCommand).RaiseCanExecuteChanged();
            }
        }
        private DateTime? _SelectedAgeUpDate = DateTime.Now;
        public DateTime? SelectedAgeUpDate
        {
            get { return _SelectedAgeUpDate; }
            set
            {
                SetProperty(ref _SelectedAgeUpDate, value);
                ((DelegateCommand)CreateMeetCommand).RaiseCanExecuteChanged();
            }
        }

        private string _MeetName;
        public string MeetName
        {
            get { return _MeetName; }
            set
            {
                SetProperty(ref _MeetName, value);
                ((DelegateCommand)CreateMeetCommand).RaiseCanExecuteChanged();
            }
        }
        private string _MeetName2;
        public string MeetName2
        {
            get { return _MeetName2; }
            set
            {
                SetProperty(ref _MeetName2, value);
                ((DelegateCommand)CreateMeetCommand).RaiseCanExecuteChanged();
            }
        }
        private bool? _IsUserDivisionBirthDate = false;
        public bool? IsUserDivisionBirthDate
        {
            get { return _IsUserDivisionBirthDate; }
            set
            {
                SetProperty(ref _IsUserDivisionBirthDate, value);
            }
        }

        private bool? _IsLinkTeamToDivision = false;
        public bool? IsLinkTeamToDivision
        {
            get { return _IsLinkTeamToDivision; }
            set
            {
                SetProperty(ref _IsLinkTeamToDivision, value);
            }
        }

        private ObservableCollection<Option> _KindOfMeetOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> KindOfMeetOptions
        {
            get { return _KindOfMeetOptions; }
            set
            {
                SetProperty(ref _KindOfMeetOptions, value);
            }
        }

        private Option _SelectedKindOfMeetOption;
        public Option SelectedKindOfMeetOption
        {
            get { return _SelectedKindOfMeetOption; }
            set
            {
                SetProperty(ref _SelectedKindOfMeetOption, value);
            }
        }
        private ObservableCollection<Option> _MeetClassOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> MeetClassOptions
        {
            get { return _MeetClassOptions; }
            set
            {
                SetProperty(ref _MeetClassOptions, value);
            }
        }

        private Option _SelectedMeetClassOption;
        public Option SelectedMeetClassOption
        {
            get { return _SelectedMeetClassOption; }
            set
            {
                SetProperty(ref _SelectedMeetClassOption, value);
            }
        }

        private ObservableCollection<Option> _MeetTypeOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> MeetTypeOptions
        {
            get { return _MeetTypeOptions; }
            set
            {
                SetProperty(ref _MeetTypeOptions, value);
            }
        }

        private Option _SelectedMeetTypeOption;
        public Option SelectedMeetTypeOption
        {
            get { return _SelectedMeetTypeOption; }
            set
            {
                SetProperty(ref _SelectedMeetTypeOption, value);
            }
        }

        private ObservableCollection<Option> _MeetStyleOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> MeetStyleOptions
        {
            get { return _MeetStyleOptions; }
            set
            {
                SetProperty(ref _MeetStyleOptions, value);
            }
        }

        private Option _SelectedMeetStyleOption;
        public Option SelectedMeetStyleOption
        {
            get { return _SelectedMeetStyleOption; }
            set
            {
                SetProperty(ref _SelectedMeetStyleOption, value);
            }
        }
        private ObservableCollection<Option> _MeetArenaOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> MeetArenaOptions
        {
            get { return _MeetArenaOptions; }
            set
            {
                SetProperty(ref _MeetArenaOptions, value);
            }
        }

        private Option _SelectedMeetArenaOption;
        public Option SelectedMeetArenaOption
        {
            get { return _SelectedMeetArenaOption; }
            set
            {
                SetProperty(ref _SelectedMeetArenaOption, value);
            }
        }

        private ObservableCollection<Option> _MeetTypeDivisionOptions = new ObservableCollection<Option>();
        public ObservableCollection<Option> MeetTypeDivisionOptions
        {
            get { return _MeetTypeDivisionOptions; }
            set
            {
                SetProperty(ref _MeetTypeDivisionOptions, value);
            }
        }

        private Option _SelectedMeetTypeDivisionOption;
        public Option SelectedMeetTypeDivisionOption
        {
            get { return _SelectedMeetTypeDivisionOption; }
            set
            {
                SetProperty(ref _SelectedMeetTypeDivisionOption, value);
            }
        }


        private ICommand _CreateMeetCommand;
        public ICommand CreateMeetCommand
        {
            get { return _CreateMeetCommand; }
        }

        private ICommand _CancelMeetCommand;
        public ICommand CancelMeetCommand
        {
            get { return _CancelMeetCommand; }
        }

        private ICommand _AgeUPCommand;
        public ICommand AgeUPCommand
        {
            get { return _AgeUPCommand; }
        }

    }
}
