﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.ModuleCommon;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.MeetSetup.ViewModels
{

    public class DivisionRegionNamesViewModel : BindableBase, IDivisionRegionNamesViewModel, IGenericInteractionView<DivisionRegionNamesViewModel>
    {
        private IDivisionRegionNamesView View = null;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        //IModalDialog dialog = null;
        //System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public DivisionRegionNamesViewModel()
        {

        }
        public DivisionRegionNamesViewModel(IUnityContainer unity, IDivisionRegionNamesView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;
            unityContainer = unity;
            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Task.Run(async () =>
                           {


                               if (DivisionRegionNameList != null)
                               {

                                   ServiceRequest_DivisionRegionNames serviceObj = new ServiceRequest_DivisionRegionNames();
                                   serviceObj.DivisionRegionNameList = this.DivisionRegionNameList.ToList();
                                   AuthorizedResponse<DivisionRegionNamesResult> Response = await meetSetupManage.UpdateDivisionRegionName(serviceObj);
                                   if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                                   {
                                       MessageBox.Show(Response.Message, "Error In Adding Division Region Names");
                                       return;
                                   }
                               }
                               await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if(item.Name== "DivisionRegionNamesView")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }));
                           });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });

            _CancelCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {

                    foreach (Window item in Application.Current.Windows)
                    {
                        if (item.Name == "DivisionRegionNamesView")
                        {
                            item.Close();
                            break;
                        }
                    }
                }));

            });


            _AddNextRowCommand = new DelegateCommand(async () =>
           {
               DivisionRegionName scoringDataSet = new DivisionRegionName();
               DivisionRegionNameList.Add(scoringDataSet);
               this.SelectedDivisionRegionName = DivisionRegionNameList[DivisionRegionNameList.Count - 1];
           });
            GetAllDivisionRegionNameList();
            OperatorList = new ObservableCollection<string>();
            OperatorList.Add(">");
            OperatorList.Add(">=");
            OperatorList.Add("<");
            OperatorList.Add("<=");

            View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            return true;

        }
        private void GetAllDivisionRegionNameList()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                DivisionRegionNameList = new ObservableCollection<DivisionRegionName>();
                List<DivisionRegionName> Response = await MeetSetupManage.GetAllDivisionRegionName();
                foreach (var item in Response)
                {
                    DivisionRegionNameList.Add(item);
                }
            })).Wait();
        }
        public void SetEntity(DivisionRegionNamesViewModel entity)
        {
            if (entity == null) return;
            this.DivisionRegionNameList.Clear();
            this.DivisionRegionNameList = entity.DivisionRegionNameList;
        }


        public DivisionRegionNamesViewModel GetEntity()
        {
            return this;
        }

        private ObservableCollection<DivisionRegionName> _DivisionRegionNameList = new ObservableCollection<DivisionRegionName>();
        public ObservableCollection<DivisionRegionName> DivisionRegionNameList
        {
            get { return _DivisionRegionNameList; }
            set
            {
                _DivisionRegionNameList = value;
                base.OnPropertyChanged("DivisionRegionNameList");
            }
        }

        public ObservableCollection<string> _OperatorList;
        public ObservableCollection<string> OperatorList
        {
            get { return _OperatorList; }
            set
            {
                _OperatorList = value;
                base.OnPropertyChanged("OperatorList");
            }
        }
        private DivisionRegionName _SelectedDivisionRegionName = new DivisionRegionName();
        public DivisionRegionName SelectedDivisionRegionName
        {
            get { return _SelectedDivisionRegionName; }
            set
            {
                _SelectedDivisionRegionName = value;
                base.OnPropertyChanged("SelectedDivisionRegionName");
            }
        }


        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        private ICommand _AddNextRowCommand;
        public ICommand AddNextRowCommand
        {
            get
            {
                return _AddNextRowCommand;
            }
        }


    }
}

