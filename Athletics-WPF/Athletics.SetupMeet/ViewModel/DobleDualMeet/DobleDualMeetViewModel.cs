﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.ModuleCommon;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.MeetSetup.ViewModels
{

    public class DobleDualMeetViewModel : BindableBase, IDobleDualMeetViewModel, IGenericInteractionView<DobleDualMeetViewModel>
    {
        private IDobleDualMeetView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public DobleDualMeetViewModel(IUnityContainer unity, IDobleDualMeetView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;
            unityContainer = unity;
            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            View.DataContext = this;
            MenuItems = new ObservableCollection<MeetMenuItemViewModel>();
            MeetMenuItemViewModel meetMenu = new MeetMenuItemViewModel();
            meetMenu.Header = "Save";
            _SaveCommand = new MeetCommandViewModel(SaveCommandHandler);
            meetMenu.Command = SaveCommand;
            MenuItems.Add(meetMenu);
            meetMenu = new MeetMenuItemViewModel();
            meetMenu.Header = "Select All Female";
            _SelectAllFemaleCommand = new MeetCommandViewModel(SelectAllFemaleCommandHandler);
            meetMenu.Command = SelectAllFemaleCommand;
            MenuItems.Add(meetMenu);

            meetMenu = new MeetMenuItemViewModel();
            meetMenu.Header = "Select All Male";
            _SelectAllMaleCommand = new MeetCommandViewModel(SelectAllMaleCommandHandler);
            meetMenu.Command = SelectAllMaleCommand;
            MenuItems.Add(meetMenu);

            meetMenu = new MeetMenuItemViewModel();
            meetMenu.Header = "Select All Combined";
            _SelectAllCombinedCommand = new MeetCommandViewModel(SelectAllCombinedCommandHandler);
            meetMenu.Command = SelectAllCombinedCommand;
            MenuItems.Add(meetMenu);

            meetMenu = new MeetMenuItemViewModel();
            meetMenu.Header = "De-Select All";
            _DeSelectAllCommand = new MeetCommandViewModel(DeSelectAllCommandHandler);
            meetMenu.Command = DeSelectAllCommand;
            MenuItems.Add(meetMenu);
        }

        private void DeSelectAllCommandHandler()
        {
            MessageBox.Show("De-Select All");
        }

        private void SelectAllCombinedCommandHandler()
        {
            MessageBox.Show("Select All Combined ");
        }

        private void SelectAllMaleCommandHandler()
        {
            MessageBox.Show("Select All Male");
        }

        private void SelectAllFemaleCommandHandler()
        {
            MessageBox.Show("Select All FeMale");
        }

        private void SaveCommandHandler()
        {
            MessageBox.Show("Saved");
        }

        private ObservableCollection<DualMeetPairing> _DualMeetPairingList;
        public ObservableCollection<DualMeetPairing> DualMeetPairingList
        {
            get { return _DualMeetPairingList; }
            set
            {
                SetProperty(ref _DualMeetPairingList, value);
            }
        }

        private ICommand _SaveCommand;
        public ICommand SaveCommand
        {
            get
            {
                return _SaveCommand;
            }
        }

        private ICommand _SelectAllFemaleCommand;
        public ICommand SelectAllFemaleCommand
        {
            get
            {
                return _SelectAllFemaleCommand;
            }
        }

        private ICommand _SelectAllMaleCommand;
        public ICommand SelectAllMaleCommand
        {
            get
            {
                return _SelectAllMaleCommand;
            }
        }
        private ICommand _SelectAllCombinedCommand;
        public ICommand SelectAllCombinedCommand
        {
            get
            {
                return _SelectAllCombinedCommand;
            }
        }

        private ICommand _DeSelectAllCommand;
        public ICommand DeSelectAllCommand
        {
            get
            {
                return _DeSelectAllCommand;
            }
        }
        private bool ValidateInputs()
        {
            return true;

        }

        public void SetEntity(DobleDualMeetViewModel entity)
        {

        }
        private ObservableCollection<MeetMenuItemViewModel> _MenuItems;
        public ObservableCollection<MeetMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }
        public DobleDualMeetViewModel GetEntity()
        {
            DobleDualMeetViewModel entity = new DobleDualMeetViewModel(unityContainer, View, MeetSetupManage, MeetSetupStateController);
            return entity;
        }

    }
    public class DualMeetPairing
    {
        public bool IsSelected { get; set; }
        public string Gender { get; set; }
        public string SchoolFirst { get; set; }
        public string SchoolSecond { get; set; }
    }
}

