﻿using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Views;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{
    public class MeetSetupShellViewModel : BindableBase, IMeetSetupShellViewModel
    {
        private IMeetSetupShellView View;
        private IMeetSetupStateController _MeetStateController;

        public MeetSetupShellViewModel(IMeetSetupShellView View, IMeetSetupStateController MeetStateController)
        {
            this.View = View;
            View.DataContext = this;
            this._MeetStateController = MeetStateController;
            _MeetStateController.MeetStateChanged += MeetStateControllerStateChanged;
            OnPropertyChanged(null);

        }

        private void DisableAllViews()
        {
            MeetSetupViewVisible = false;
            AthleteRelayPreferencesSViewVisible = false;
            SeedingPreferencesViewVisible = false;

        }

        private void MeetStateControllerStateChanged(e_MEET_STATE NewMeetState)
        {
            DisableAllViews();
            switch (NewMeetState)
            {
                case e_MEET_STATE.MEET_SETUP:
                    MeetSetupViewVisible = true;
                    break;
                case e_MEET_STATE.ATHLETE_RELAY_PREFERENCES:
                    AthleteRelayPreferencesSViewVisible = true;
                    break;
                case e_MEET_STATE.SEEDING_PREFERENCES:
                    SeedingPreferencesViewVisible = true;
                    break;
            }
        }

        private bool _MeetSetupViewVisible = true;
        public bool MeetSetupViewVisible
        {
            get
            {
                return _MeetSetupViewVisible;
            }
            protected set { SetProperty(ref _MeetSetupViewVisible, value); }
        }

        private bool _AthleteRelayPreferencesSViewVisible = false;
        public bool AthleteRelayPreferencesSViewVisible
        {
            get
            {
                return _AthleteRelayPreferencesSViewVisible;
            }
            protected set { SetProperty(ref _AthleteRelayPreferencesSViewVisible, value); }
        }

        private bool _SeedingPreferencesViewVisible = false;
        public bool SeedingPreferencesViewVisible
        {
            get
            {
                return _SeedingPreferencesViewVisible;
            }
            protected set { SetProperty(ref _SeedingPreferencesViewVisible, value); }
        }

    }
}
