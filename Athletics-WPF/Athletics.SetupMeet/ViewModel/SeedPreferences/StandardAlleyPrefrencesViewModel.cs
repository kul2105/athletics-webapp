﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Athletics.MeetSetup.Views.SeedPreferences;
using Athletics.ModuleCommon;

namespace Athletics.MeetSetup.ViewModels.SeedPreferences
{

    public class StandardAlleyPrefrencesViewModel : BindableBase, IStandardAlleyPrefrencesViewModel, IGenericInteractionView<ServiceRequest_StandardAlleyPrefrences>
    {
        private IStandardAlleyPrefrencesView View = null;
        private string StandardAlleyPrefrencesId = string.Empty;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        ServiceRequest_StandardAlleyPrefrences service = new ServiceRequest_StandardAlleyPrefrences();
        public StandardAlleyPrefrencesViewModel(IStandardAlleyPrefrencesView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;

            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Task.Run(async () =>
                           {
                               if (StandardAlleyPrefrencesId == string.Empty)
                               {
                                   service.StandardAlleyPrefrencesList = this.StandardAlleyPrefrenceList.ToList();
                                   //AuthorizedResponse<StandardAlleyPrefrenceResult> Response = await meetSetupManage.CreateNewStandardAlleyPrefrences(service);
                               }
                               else
                               {
                                   // AuthorizedResponse<MeetSetupResult> Response = await meetSetupManage.UpdateMeet(newAPIServiceRequest_MeetSetup);
                               }
                               await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {

                                    this.View.CloseView();
                                }));
                           });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    this.View.CloseView();
                }));

            });

            foreach (var item in meetSetupManage.GetAllStandardAlleyPrefrence().Result)
            {
                StandardAlleyPrefrenceList.Add(item);
            }
            if (View != null)
                View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            return true;
            //if (_RoleNameValid == true && _RoleIDValid == true)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        public void SetEntity(ServiceRequest_StandardAlleyPrefrences entity)
        {
            if (entity != null)
            {
                // this.SelectedMeetArenaOption = this.MeetArenaOptions.Where(p => p.Display == entity.MeetArenaOption).FirstOrDefault();
                //  this.SelectedMeetTypeDivisionOption = this.MeetTypeDivisionOptions.Where(p => p.Display == entity.MeetTypeDivisionOption).FirstOrDefault();
                // AthleteRelayPreferencesId = entity.MeetID;
            }
        }

        public ServiceRequest_StandardAlleyPrefrences GetEntity()
        {
            //if (IsInCloseMode == true)
            //{
            //    IsInCloseMode = false;
            //    return null;

            //}
            // StandardAlleyPrefrencesViewModel entity = new StandardAlleyPrefrencesViewModel(View, MeetSetupManage, MeetSetupStateController);
            //entity.MeetStyleOption = this.SelectedMeetStyleOption.Display;
            //entity.MeetTypeDivisionOption = this.SelectedMeetTypeDivisionOption.Display;
            //entity.MeetID = this.AthleteRelayPreferencesId;
            return this.service;
        }

        private ObservableCollection<StandardAlleyPrefrence> _StandardAlleyPrefrenceList = new ObservableCollection<StandardAlleyPrefrence>();
        public ObservableCollection<StandardAlleyPrefrence> StandardAlleyPrefrenceList
        {
            get { return _StandardAlleyPrefrenceList; }
            set
            {
                SetProperty(ref _StandardAlleyPrefrenceList, value);
            }
        }

        private StandardAlleyPrefrence _StandardAlleyPrefrence;
        public StandardAlleyPrefrence StandardAlleyPrefrence
        {
            get { return _StandardAlleyPrefrence; }
            set
            {
                SetProperty(ref _StandardAlleyPrefrence, value);
            }
        }


        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

    }
}

