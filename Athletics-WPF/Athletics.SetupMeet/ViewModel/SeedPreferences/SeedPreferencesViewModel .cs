﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Behaviors;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.MeetSetup.Views.SeedPreferences;
using Athletics.SetupMeet.Model;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Linq;
using Athletics.ModuleCommon;
using Athletics.SetupMeet.View.AssigPrefrences;

namespace Athletics.MeetSetup.ViewModels.SeedPreferences
{

    public class SeedPreferencesViewModel : BindableBase, ISeedPreferencesViewModel, IGenericInteractionView<SeedPreferencesViewModel>
    {
        private ISeedPreferencesView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        private readonly IUnityContainer unityContainer;
        private ServiceRequest_StandardAlleyPrefrences serviceStandardAlleyPrefrences = new ServiceRequest_StandardAlleyPrefrences();
        private ServiceRequest_StandardLanePrefrences serviceStandardLanePrefrences = new ServiceRequest_StandardLanePrefrences();
        private ServiceRequest_WaterfallStartPreference serviceWaterfallStartPreference = new ServiceRequest_WaterfallStartPreference();
        public bool CallingFromMeet { get; set; }
        IModalDialog dialog = null;
        private readonly ICommand _PART_CLOSE;
        System.Windows.Window window = null;
        public SeedPreferencesViewModel(IUnityContainer unity, ISeedPreferencesView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;
            unityContainer = unity;
            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            _PART_CLOSE = new DelegateCommand(PART_CLOSE_Click);
            _StandardLanePreferencesCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            IGenericInteractionView<ServiceRequest_StandardLanePrefrences> seedprefrences = null;
                            dialog = ServiceProvider.Instance.Get<IModalDialog>();
                            window = unityContainer.Resolve<IStandardLanePrefrencesView>() as System.Windows.Window;
                            if (StandardLanePrefrences == null)
                            {
                                seedprefrences = unityContainer.Resolve<IStandardLanePrefrencesViewModel>() as IGenericInteractionView<ServiceRequest_StandardLanePrefrences>;
                                if (seedprefrences != null)
                                {
                                    dialog.BindView(window);
                                    dialog.BindViewModel(seedprefrences);
                                }
                            }
                            else
                            {
                                dialog.BindView(window);
                                dialog.BindViewModel(StandardLanePrefrences);

                            }
                            dialog.ShowDialog();
                            if (seedprefrences != null)
                            {
                                serviceStandardLanePrefrences = seedprefrences.GetEntity();
                            }
                            dialog.Close();

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });

            _StandardAlleyPreferencesCommand = new DelegateCommand(async () =>
             {
                 if (!ValidateInputs())
                 {

                 }
                 else
                 {
                     await Task.Run(async () =>
                     {
                         Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             dialog = ServiceProvider.Instance.Get<IModalDialog>();
                             IGenericInteractionView<ServiceRequest_StandardAlleyPrefrences> seedprefrences = null;
                             window = unityContainer.Resolve<IStandardAlleyPrefrencesView>() as System.Windows.Window;
                             if (StandardAlleyPrefrences == null)
                             {
                                 seedprefrences = unityContainer.Resolve<IStandardAlleyPrefrencesViewModel>() as IGenericInteractionView<ServiceRequest_StandardAlleyPrefrences>;
                                 if (seedprefrences != null)
                                 {
                                     dialog.BindView(window);
                                     dialog.BindViewModel(seedprefrences);
                                 }
                             }
                             else
                             {
                                 dialog.BindView(window);
                                 dialog.BindViewModel(StandardAlleyPrefrences);
                             }
                             dialog.ShowDialog();
                             if (seedprefrences != null)
                             {
                                 serviceStandardAlleyPrefrences = seedprefrences.GetEntity();
                             }
                             dialog.Close();

                         })).Wait();
                     });
                 }
             }, () =>
             {
                 return ValidateInputs();
             });

            _CancelCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    PART_CLOSE_Click();
                }));

            });

            _WaterfallPreferencesCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            dialog = ServiceProvider.Instance.Get<IModalDialog>();
                            IGenericInteractionView<ServiceRequest_WaterfallStartPreference> seedprefrences = null;
                            window = unityContainer.Resolve<IWaterfallStartView>() as System.Windows.Window;
                            if (WaterFallPreferences == null)
                            {
                                seedprefrences = unityContainer.Resolve<IWaterfallStartViewModel>() as IGenericInteractionView<ServiceRequest_WaterfallStartPreference>;
                                if (seedprefrences != null)
                                {
                                    dialog.BindView(window);
                                    dialog.BindViewModel(seedprefrences);
                                }
                            }
                            else
                            {
                                dialog.BindView(window);
                                dialog.BindViewModel(WaterFallPreferences);

                            }

                            dialog.ShowDialog();
                            if (seedprefrences != null)
                            {
                                serviceWaterfallStartPreference = seedprefrences.GetEntity();
                            }
                            dialog.Close();

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });

            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Task.Run(async () =>
                            {
                                AuthorizedResponse<StandardAlleyPrefrenceResult> ResponseAlleyPrefrence = await meetSetupManage.CreateNewStandardAlleyPrefrences(serviceStandardAlleyPrefrences);
                                AuthorizedResponse<StandardLanePrefrencesResult> ResponseStandardLanePrefrences = await meetSetupManage.CreateNewStandardLanePrefrences(serviceStandardLanePrefrences);
                                AuthorizedResponse<WaterfallStartPreferenceResult> ResponseWaterfallStartPreference = await meetSetupManage.CreateNewWaterfallStartPreference(serviceWaterfallStartPreference);
                                ServiceRequest_DualMeet serviceDualMeet = new ServiceRequest_DualMeet();
                                serviceDualMeet.LaneLaneDetailModelList = new System.Collections.Generic.List<LaneDetailModel>();
                                foreach (var item in this.LaneDetailList)
                                {
                                    if (item.SchoolID > 0)
                                    {
                                        serviceDualMeet.LaneLaneDetailModelList.Add(item);
                                    }
                                }
                                AuthorizedResponse<DualMeetResult> ResponseDualMeetResult = await meetSetupManage.CreateNewDualMeet(serviceDualMeet);
                                if (ResponseDualMeetResult.Status == eMEET_REQUEST_RESPONSE.SUCCESS)
                                {
                                    ServiceRequest_DualMeetAssignment assignment = new ServiceRequest_DualMeetAssignment();
                                    assignment.AlternamtUserOfUnAssignedLane = this.AlternamtUserOfUnAssignedLane;
                                    assignment.DualMeetGroupID = ResponseDualMeetResult.Result.DualMeetGroupID;
                                    assignment.StrictAssignmentAllHeats = this.StrictAssignmentAllHeats;
                                    assignment.StrictAssignmentFastestHeatOnly = this.StrictAssignmentFastestHeatOnly;
                                    assignment.UseLaneAssignmentsForInLaneRacesOnly = this.UseLaneAssignmentsForInLaneRacesOnly;
                                    assignment.UserLaneOrPositionAssignmentsAbove = this.UserLaneOrPositionAssignmentsAbove;
                                    AuthorizedResponse<DualMeetAssignmentResult> ResponseDualMeetAssignmentResult = await meetSetupManage.CreateNewDualMeetAssignment(assignment);
                                }
                                else
                                {
                                    MessageBox.Show("There is an error in processing  Dual Meet");
                                    return;
                                }


                                ServiceRequest_SeedingRules seedingRules = new ServiceRequest_SeedingRules();
                                seedingRules.AllowExhibitionAthletesInFinal = this.AllowExhibitionAthletesInFinal;
                                seedingRules.AllowForeignAthletesInFinal = this.AllowForeignAthletesInFinal;
                                seedingRules.ApplyNCAARule = this.ApplyNCAARule;
                                seedingRules.SeedExhibitionAthletesLast = this.SeedExhibitionAthletesLast;
                                seedingRules.UseSpecialRandomSelectMethod = this.UseSpecialRandomSelectMethod;
                                AuthorizedResponse<SeedingRulesResult> ResponseSeedingRules = await meetSetupManage.CreateNewSeedingRules(seedingRules);

                                ServiceRequest_RendomizationRule rendomRules = new ServiceRequest_RendomizationRule();
                                rendomRules.RoundFirstMultipleRound = this.RoundFirstMultipleRound;
                                rendomRules.RoundSecondThirdAndForth = this.RoundSecondThirdAndForth;
                                rendomRules.TimedFinalEvents = this.TimedFinalEvents;
                                rendomRules.CloseGap = this.CloseGap;
                                AuthorizedResponse<RendomizationRuleResult> ResponseRendomizationRule = await meetSetupManage.CreateNewRendomizationRule(rendomRules);
                                ServiceRequest_SeedingPreferences seedingPrefrences = new ServiceRequest_SeedingPreferences();
                                if (ResponseRendomizationRule.Status == eMEET_REQUEST_RESPONSE.SUCCESS)
                                {
                                    seedingPrefrences.RendomizationRuleGroupID = ResponseRendomizationRule.Result.RendomizationRuleGroupID;
                                }
                                else
                                {
                                    MessageBox.Show("There is an error in processing  Rendomization Rule");
                                    return;
                                }

                                if (ResponseDualMeetResult.Status == eMEET_REQUEST_RESPONSE.SUCCESS)
                                {
                                    seedingPrefrences.DualMeetGroupID = ResponseDualMeetResult.Result.DualMeetGroupID;
                                }
                                else
                                {
                                    MessageBox.Show("There is an error in processing  Dual Meet");
                                    return;
                                }

                                if (ResponseSeedingRules.Status == eMEET_REQUEST_RESPONSE.SUCCESS)
                                {
                                    seedingPrefrences.SeedingRulesGroupID = ResponseSeedingRules.Result.SeedingRulesGroupID;
                                }
                                else
                                {
                                    MessageBox.Show("There is an error in processing  Seeding Rules");
                                    return;
                                }

                                if (ResponseAlleyPrefrence.Status == eMEET_REQUEST_RESPONSE.SUCCESS)
                                {
                                    seedingPrefrences.StandardAlleyPrefrencesGroupID = ResponseAlleyPrefrence.Result.StandardAlleyPrefrenceGroupID;
                                }
                                else
                                {
                                    MessageBox.Show("There is an error in processing  Alley Prefrence");
                                    return;
                                }

                                if (ResponseStandardLanePrefrences.Status == eMEET_REQUEST_RESPONSE.SUCCESS)
                                {
                                    seedingPrefrences.StandardLanePrefrencesGroupID = ResponseStandardLanePrefrences.Result.StandardLanePrefrencesGroupID;
                                }
                                else
                                {
                                    MessageBox.Show("There is an error in processing  Lane Prefrence");
                                    return;
                                }

                                if (ResponseWaterfallStartPreference.Status == eMEET_REQUEST_RESPONSE.SUCCESS)
                                {
                                    seedingPrefrences.WaterfallStartPreferencesGroupID = ResponseWaterfallStartPreference.Result.WaterfallStartPreferenceGroupID;
                                }
                                else
                                {
                                    MessageBox.Show("There is an error in processing  Waterfall Start Preferences");
                                    return;
                                }
                                AuthorizedResponse<SeedingPreferencesResult> ResponseSeedingPreferences = await meetSetupManage.CreateNewSeedingPreferences(seedingPrefrences);
                               int seedPreferenceID= ResponseSeedingPreferences.Result.SeedingPreferencesTranID;
                                if (this.CallingFromMeet)
                                {
                                    MessageBoxResult msgResult = MessageBox.Show("Do you want(s) Meet to Save as Template", "Save as Template", MessageBoxButton.YesNo);
                                    if (msgResult == MessageBoxResult.Yes)
                                    {
                                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                         {
                                             TemplateNameView templateNameView = new TemplateNameView();
                                             templateNameView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                                             templateNameView.Height = 200;
                                             templateNameView.Width = 300;
                                             templateNameView.ShowDialog();
                                             string templateName = templateNameView.TemplateName;
                                             ServiceRequest_TemplateName request = new ServiceRequest_TemplateName();
                                             request.Preferenceid = seedPreferenceID;
                                             request.TemplateName = templateName;
                                             await meetSetupManage.CreateSeedPreferenceTemplate(request);
                                             templateNameView.Close();
                                         }));
                                    }
                                }
                                else
                                {
                                    await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                     {
                                         TemplateNameView templateNameView = new TemplateNameView();
                                         templateNameView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                                         templateNameView.Height = 200;
                                         templateNameView.Width = 300;
                                         templateNameView.ShowDialog();
                                         string templateName = templateNameView.TemplateName;
                                         ServiceRequest_TemplateName request = new ServiceRequest_TemplateName();
                                         request.Preferenceid = seedPreferenceID;
                                         request.TemplateName = templateName;
                                         await meetSetupManage.CreateSeedPreferenceTemplate(request);
                                         templateNameView.Close();
                                     }));
                                }

                                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {
                                    PART_CLOSE_Click();
                                }));
                            });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });


            foreach (var item in meetSetupManage.GetAllLaneDetail().Result)
            {
                LaneDetailModel laneModel = new LaneDetailModel();
                laneModel.LaneID = item.LaneID;
                LaneDetailList.Add(laneModel);
            }

            foreach (var item in meetSetupManage.GetAllSchoolDetail().Result)
            {
                SchoolDetailList.Add(item);
            }


            ItemsDragDropCommand = new RelayCommand<DataGridDragDropEventArgs>((args) => DragDropItem(args), (args) => args != null && args.TargetObject != null && args.DroppedObject != null && args.Effects != System.Windows.DragDropEffects.None);
            View.DataContext = this;
        }

        private void DragDropItem(DataGridDragDropEventArgs args)
        {
            LaneDetailModel laneDetail = args.TargetObject as LaneDetailModel;
            if (laneDetail != null)
            {

                SchoolDetail schoolDetail = args.DroppedObject as SchoolDetail;

                if (schoolDetail != null)
                {
                    LaneDetailModel ExistedSchool = LaneDetailList.Where(p => p.SchoolID == schoolDetail.SchoolID).FirstOrDefault();
                    if (ExistedSchool == null)
                    {
                        laneDetail.SchoolID = schoolDetail.SchoolID;
                        laneDetail.SchoolName = schoolDetail.SchoolName;
                    }
                    else
                    {
                        MessageBox.Show("School Already added");
                    }
                }
            }

        }

        private bool ValidateInputs()
        {
            return true;
            //if (_RoleNameValid == true && _RoleIDValid == true)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        public void SetEntity(SeedPreferencesViewModel entity)
        {
            CallingFromMeet = true;
            if (entity != null)
            {
                // this.SelectedMeetArenaOption = this.MeetArenaOptions.Where(p => p.Display == entity.MeetArenaOption).FirstOrDefault();
                //  this.SelectedMeetTypeDivisionOption = this.MeetTypeDivisionOptions.Where(p => p.Display == entity.MeetTypeDivisionOption).FirstOrDefault();
                // AthleteRelayPreferencesId = entity.MeetID;
            }
        }

        public SeedPreferencesViewModel GetEntity()
        {

            //if (IsInCloseMode == true)
            //{
            //    IsInCloseMode = false;
            //    return null;

            //}
            SeedPreferencesViewModel entity = new SeedPreferencesViewModel(unityContainer, View, MeetSetupManage, MeetSetupStateController);
            //entity.MeetStyleOption = this.SelectedMeetStyleOption.Display;
            //entity.MeetTypeDivisionOption = this.SelectedMeetTypeDivisionOption.Display;
            //entity.MeetID = this.AthleteRelayPreferencesId;
            return entity;
        }
        private Visibility _ShowCommand = Visibility.Visible;
        public Visibility ShowCommand
        {
            get { return _ShowCommand; }
            set
            {
                SetProperty(ref _ShowCommand, value);
            }
        }
        
        private WaterfallStartViewModel _WaterFallPreferences = null;
        public WaterfallStartViewModel WaterFallPreferences
        {
            get { return _WaterFallPreferences; }
            set
            {
                SetProperty(ref _WaterFallPreferences, value);
            }
        }

        private StandardAlleyPrefrencesViewModel _StandardAlleyPrefrences = null;
        public StandardAlleyPrefrencesViewModel StandardAlleyPrefrences
        {
            get { return _StandardAlleyPrefrences; }
            set
            {
                SetProperty(ref _StandardAlleyPrefrences, value);
            }
        }

        private StandardLanePrefrencesViewModel _StandardLanePrefrences = null;
        public StandardLanePrefrencesViewModel StandardLanePrefrences
        {
            get { return _StandardLanePrefrences; }
            set
            {
                SetProperty(ref _StandardLanePrefrences, value);
            }
        }

        private bool _CalledFromAssign = false;
        public bool CalledFromAssign
        {
            get { return _CalledFromAssign; }
            set
            {
                SetProperty(ref _CalledFromAssign, value);
            }
        }
        private bool _AlternamtUserOfUnAssignedLane;
        public bool AlternamtUserOfUnAssignedLane
        {
            get { return _AlternamtUserOfUnAssignedLane; }
            set
            {
                SetProperty(ref _AlternamtUserOfUnAssignedLane, value);
            }
        }

        private bool _StrictAssignmentAllHeats;
        public bool StrictAssignmentAllHeats
        {
            get { return _StrictAssignmentAllHeats; }
            set
            {
                SetProperty(ref _StrictAssignmentAllHeats, value);
            }
        }
        private bool _StrictAssignmentFastestHeatOnly;
        public bool StrictAssignmentFastestHeatOnly
        {
            get { return _StrictAssignmentFastestHeatOnly; }
            set
            {
                SetProperty(ref _StrictAssignmentFastestHeatOnly, value);
            }
        }

        private bool _UseLaneAssignmentsForInLaneRacesOnly;
        public bool UseLaneAssignmentsForInLaneRacesOnly
        {
            get { return _UseLaneAssignmentsForInLaneRacesOnly; }
            set
            {
                SetProperty(ref _UseLaneAssignmentsForInLaneRacesOnly, value);
            }
        }

        private bool _UserLaneOrPositionAssignmentsAbove;
        public bool UserLaneOrPositionAssignmentsAbove
        {
            get { return _UserLaneOrPositionAssignmentsAbove; }
            set
            {
                SetProperty(ref _UserLaneOrPositionAssignmentsAbove, value);
            }
        }

        private bool _AllowExhibitionAthletesInFinal;
        public bool AllowExhibitionAthletesInFinal
        {
            get { return _AllowExhibitionAthletesInFinal; }
            set
            {
                SetProperty(ref _AllowExhibitionAthletesInFinal, value);
            }
        }

        private bool _AllowForeignAthletesInFinal;
        public bool AllowForeignAthletesInFinal
        {
            get { return _AllowForeignAthletesInFinal; }
            set
            {
                SetProperty(ref _AllowForeignAthletesInFinal, value);
            }
        }
        private bool _ApplyNCAARule;
        public bool ApplyNCAARule
        {
            get { return _ApplyNCAARule; }
            set
            {
                SetProperty(ref _ApplyNCAARule, value);
            }
        }
        private bool _SeedExhibitionAthletesLast;
        public bool SeedExhibitionAthletesLast
        {
            get { return _SeedExhibitionAthletesLast; }
            set
            {
                SetProperty(ref _SeedExhibitionAthletesLast, value);
            }
        }

        private bool _UseSpecialRandomSelectMethod;
        public bool UseSpecialRandomSelectMethod
        {
            get { return _UseSpecialRandomSelectMethod; }
            set
            {
                SetProperty(ref _UseSpecialRandomSelectMethod, value);
            }
        }

        private string _RoundFirstMultipleRound;
        public string RoundFirstMultipleRound
        {
            get { return _RoundFirstMultipleRound; }
            set
            {
                SetProperty(ref _RoundFirstMultipleRound, value);
            }
        }
        private string _RoundSecondThirdAndForth;
        public string RoundSecondThirdAndForth
        {
            get { return _RoundSecondThirdAndForth; }
            set
            {
                SetProperty(ref _RoundSecondThirdAndForth, value);
            }
        }

        private string _TimedFinalEvents;
        public string TimedFinalEvents
        {
            get { return _TimedFinalEvents; }
            set
            {
                SetProperty(ref _TimedFinalEvents, value);
            }
        }

        private bool _CloseGap;
        public bool CloseGap
        {
            get { return _CloseGap; }
            set
            {
                SetProperty(ref _CloseGap, value);
            }
        }

        private ObservableCollection<LaneDetailModel> _LaneDetailList = new ObservableCollection<LaneDetailModel>();
        public ObservableCollection<LaneDetailModel> LaneDetailList
        {
            get { return _LaneDetailList; }
            set
            {
                SetProperty(ref _LaneDetailList, value);
            }
        }

        private Option _SelectedMeetStyleOption;
        public Option SelectedMeetStyleOption
        {
            get { return _SelectedMeetStyleOption; }
            set
            {
                SetProperty(ref _SelectedMeetStyleOption, value);
            }
        }
        private ObservableCollection<SchoolDetail> _SchoolDetailList = new ObservableCollection<SchoolDetail>();
        public ObservableCollection<SchoolDetail> SchoolDetailList
        {
            get { return _SchoolDetailList; }
            set
            {
                SetProperty(ref _SchoolDetailList, value);
            }
        }


        private ICommand _StandardAlleyPreferencesCommand;
        public ICommand StandardAlleyPreferencesCommand
        {
            get { return _StandardAlleyPreferencesCommand; }
        }

        private ICommand _StandardLanePreferencesCommand;
        public ICommand StandardLanePreferencesCommand
        {
            get { return _StandardLanePreferencesCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        private ICommand _WaterfallPreferencesCommand;
        public ICommand WaterfallPreferencesCommand
        {
            get { return _WaterfallPreferencesCommand; }
        }
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        public ICommand PART_CLOSE
        {
            get
            {
                return _PART_CLOSE;
            }
        }

        private void PART_CLOSE_Click()
        {
            Window window = null;
            foreach (Window item in Application.Current.Windows)
            {
                if (item.Name == "SeedPreferences")
                {
                    window = item;
                }
            }
            if (window != null)
            {
                window.Close();
            }
        }

        public RelayCommand<DataGridDragDropEventArgs> ItemsDragDropCommand { get; private set; }
    }
}

