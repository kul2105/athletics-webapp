﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Athletics.MeetSetup.Views.SeedPreferences;
using Athletics.ModuleCommon;

namespace Athletics.MeetSetup.ViewModels.SeedPreferences
{

    public class WaterfallStartViewModel : BindableBase, IWaterfallStartViewModel, IGenericInteractionView<ServiceRequest_WaterfallStartPreference>
    {
        private IWaterfallStartView View = null;
        private string StandardWaterfallPrefrencesId = string.Empty;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        ServiceRequest_WaterfallStartPreference service = new ServiceRequest_WaterfallStartPreference();
        public WaterfallStartViewModel(IWaterfallStartView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;

            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Task.Run(async () =>
                           {
                               if (StandardWaterfallPrefrencesId == string.Empty)
                               {

                                   service.WaterfallStartPreferenceList = this.WaterfallStartList.ToList();
                               }
                               else
                               {
                                   // AuthorizedResponse<MeetSetupResult> Response = await meetSetupManage.UpdateMeet(newAPIServiceRequest_MeetSetup);
                               }
                               await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {

                                    this.View.CloseView();

                                }));
                           });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {

                    Window window = view as Window;
                    window.Visibility = Visibility.Hidden;
                }));

            });

            foreach (var item in meetSetupManage.GetAllWaterfallStartPreference().Result)
            {
                WaterfallStartList.Add(item);
            }
            if (View != null)
                View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            return true;
            //if (_RoleNameValid == true && _RoleIDValid == true)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        public void SetEntity(ServiceRequest_WaterfallStartPreference entity)
        {
            if (entity != null)
            {
                // this.SelectedMeetArenaOption = this.MeetArenaOptions.Where(p => p.Display == entity.MeetArenaOption).FirstOrDefault();
                //  this.SelectedMeetTypeDivisionOption = this.MeetTypeDivisionOptions.Where(p => p.Display == entity.MeetTypeDivisionOption).FirstOrDefault();
                // AthleteRelayPreferencesId = entity.MeetID;
            }
        }

        public ServiceRequest_WaterfallStartPreference GetEntity()
        {
            //if (IsInCloseMode == true)
            //{
            //    IsInCloseMode = false;
            //    return null;

            //}
            // WaterfallStartViewModel entity = new WaterfallStartViewModel(View, MeetSetupManage, MeetSetupStateController);
            //entity.MeetStyleOption = this.SelectedMeetStyleOption.Display;
            //entity.MeetTypeDivisionOption = this.SelectedMeetTypeDivisionOption.Display;
            //entity.MeetID = this.AthleteRelayPreferencesId;
            return this.service;
        }

        private ObservableCollection<WaterfallStartPreference> _WaterfallStartList = new ObservableCollection<WaterfallStartPreference>();
        public ObservableCollection<WaterfallStartPreference> WaterfallStartList
        {
            get { return _WaterfallStartList; }
            set
            {
                SetProperty(ref _WaterfallStartList, value);
            }
        }

        private WaterfallStartPreference _WaterfallStartPreference;
        public WaterfallStartPreference WaterfallStartPreference
        {
            get { return _WaterfallStartPreference; }
            set
            {
                SetProperty(ref _WaterfallStartPreference, value);
            }
        }


        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

    }
}

