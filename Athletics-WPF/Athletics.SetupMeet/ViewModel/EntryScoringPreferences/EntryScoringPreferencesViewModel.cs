﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.MeetSetup.ViewModels
{

    public class EntryScoringPreferencesViewModel : BindableBase, IEntryScoringPreferencesViewModel, IGenericInteractionView<EntryScoringPreferencesViewModel>
    {
        private IEntryScoringPreferencesView View = null;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public EntryScoringPreferencesViewModel()
        {

        }
        public EntryScoringPreferencesViewModel(IUnityContainer unity, IEntryScoringPreferencesView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;
            unityContainer = unity;
            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Task.Run(async () =>
                           {
                               //if (AthleteRelayPreferencesId == 0)
                               //{

                               //    //if (seedVm == null && AthleteRelayPrefrences == null)
                               //    //{
                               //    //    MessageBox.Show("Please select atlest one Prefrences to Assigned", "No Prefrences Selected");
                               //    //    return;
                               //    //}
                               //    //ServiceRequest_AssignPreferences serviceObj = new ServiceRequest_AssignPreferences();
                               //    //serviceObj.AthleteRelayPreferencesViewModel = AthleteRelayPrefrences;
                               //    //serviceObj.SeedPreferencesViewModel = seedVm;
                               //    //serviceObj.MeetSetupID = MeetSetupList.FirstOrDefault().MeetID;

                               //    //AuthorizedResponse<AssignPreferencesResult> Response = await meetSetupManage.AssignPreferences(serviceObj);
                               //    //this.AthleteRelayPreferencesId = Response.Result.AssignPreferencesID;
                               //}
                               //else
                               //{
                               //}
                               await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {
                                    Window window = view as Window;
                                    if (window != null)
                                    {
                                        window.Visibility = Visibility.Hidden;
                                    }
                                }));
                           });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {

                    Window window = view as Window;
                    if (window != null)
                    {
                        window.Visibility = Visibility.Hidden;
                    }
                }));

            });

            _SelectTeamCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    Window window = unityContainer.Resolve<IDobleDualMeetView>() as Window;
                    IGenericInteractionView<DobleDualMeetViewModel> createDesignDataSetViewModel = unityContainer.Resolve<IDobleDualMeetViewModel>() as IGenericInteractionView<DobleDualMeetViewModel>;
                    if (createDesignDataSetViewModel != null)
                    {
                        createDesignDataSetViewModel.SetEntity(null);
                        dialog.BindView(window);
                        dialog.BindViewModel(createDesignDataSetViewModel);
                    }
                    dialog.ShowDialog();
                    DobleDualMeetViewModel model = createDesignDataSetViewModel.GetEntity();
                }));

            });
            View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            return true;

        }

        public void SetEntity(EntryScoringPreferencesViewModel entity)
        {
            if (entity == null) return;
            this.AllowForeignAthletesPointScoreToCountTowordTeamScore = entity.AllowForeignAthletesPointScoreToCountTowordTeamScore;
            this.ConvertHandEntryTimesToFAT = entity.ConvertHandEntryTimesToFAT;
            this.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6 = entity.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6;
            this.DiffrentPointSystemForEach = entity.DiffrentPointSystemForEach;
            this.DiffrentPointSystemForMaleAndFemale = entity.DiffrentPointSystemForMaleAndFemale;
            this.DynamicMultipleTeamScoring = entity.DynamicMultipleTeamScoring;
            this.EntryNotesWithEachEntry = entity.EntryNotesWithEachEntry;
            this.ExceedOrEqualQualifingMarks = entity.ExceedOrEqualQualifingMarks;
            this.ExhibitionMarksReceiveFinalsRanks = entity.ExhibitionMarksReceiveFinalsRanks;
            this.FieldRelayCanBeScoreWithUnderSizeTeam = entity.FieldRelayCanBeScoreWithUnderSizeTeam;
            this.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult = entity.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult;
            this.IncludeRelayPointsWithIndivHighPointScore = entity.IncludeRelayPointsWithIndivHighPointScore;
            this.IsInCloseMode = entity.IsInCloseMode;
            this.MarksWillBeAddedForReamRanking = entity.MarksWillBeAddedForReamRanking;
            this.MaximumDisplacerPerTeam = entity.MaximumDisplacerPerTeam;
            this.MaximumEntriesPerAthleteIncludingRelays = entity.MaximumEntriesPerAthleteIncludingRelays;
            this.MaximumFieldEventEntriesPerAthletes = entity.MaximumFieldEventEntriesPerAthletes;
            this.MaximumPerTeamFinalFromPreLimsIndividual = entity.MaximumPerTeamFinalFromPreLimsIndividual;
            this.MaximumPerTeamFinalFromPreLimsRelay = entity.MaximumPerTeamFinalFromPreLimsRelay;
            this.MaximumRunnerToQualifyAsATeam = entity.MaximumRunnerToQualifyAsATeam;
            this.MaximumScorePerTeamPerEventIndividual = entity.MaximumScorePerTeamPerEventIndividual;
            this.MaximumScorePerTeamPerEventRelay = entity.MaximumScorePerTeamPerEventRelay;
            this.MaximumTrackEventEntriesPerAthleteIncludingRelay = entity.MaximumTrackEventEntriesPerAthleteIncludingRelay;
            this.NoneTeamRunnerDisplacedOtherRunner = entity.NoneTeamRunnerDisplacedOtherRunner;
            this.NumberOfRunnersScoreFromEachTeam = entity.NumberOfRunnersScoreFromEachTeam;
            this.PanaltyPointAddedForShortTeamLast1Last2Etc = entity.PanaltyPointAddedForShortTeamLast1Last2Etc;
            this.PanaltyPointsAddedForShortTeams = entity.PanaltyPointsAddedForShortTeams;
            this.RankResultByAgeGradedMarks = entity.RankResultByAgeGradedMarks;
            this.RecordWindReading = entity.RecordWindReading;
            this.RoundDownTheNearestEventCentimerer = entity.RoundDownTheNearestEventCentimerer;
            this.RoundUpResultToTenthofReport = entity.RoundUpResultToTenthofReport;
            this.ScoreEqualRelayOnly = entity.ScoreEqualRelayOnly;
            this.ScoreFastestHeadOnlyRegardLessOverAllPlace = entity.ScoreFastestHeadOnlyRegardLessOverAllPlace;
            this.ScroringPreferencesTemplateName = entity.ScroringPreferencesTemplateName;
            this.ScroringPreferencesTemplatID = entity.ScroringPreferencesTemplatID;
            this.ShowFinishedPlaceWithEachEntry = entity.ShowFinishedPlaceWithEachEntry;
            this.TimesWillBeAddedForTeamRanking = entity.TimesWillBeAddedForTeamRanking;
            this.TopHowManyForAwardLabelIndividual = entity.TopHowManyForAwardLabelIndividual;
            this.TopHowManyForAwardLabelRelay = entity.TopHowManyForAwardLabelRelay;
            this.TreateForeignAthletesAsExhibition = entity.TreateForeignAthletesAsExhibition;
            this.UseEntryDeclarationMethod = entity.UseEntryDeclarationMethod;
            this.UseFiveYearAgingTableMulti = entity.UseFiveYearAgingTableMulti;
            this.UseFiveYearAgingTableStandared = entity.UseFiveYearAgingTableStandared;
            this.UseOneYearAgingTableMulti = entity.UseOneYearAgingTableMulti;
            this.UseOneYearAgingTableStandared = entity.UseOneYearAgingTableStandared;
            this.WarnIfEntryLimitsExceeds = entity.WarnIfEntryLimitsExceeds;
            this.WarnIfTimesMarksAreOutofRange = entity.WarnIfTimesMarksAreOutofRange;
        }

        public EntryScoringPreferencesViewModel GetEntity()
        {
            return this;
        }
        private string _ScroringPreferencesTemplateName = string.Empty;
        public string ScroringPreferencesTemplateName
        {
            get { return _ScroringPreferencesTemplateName; }
            set
            {
                SetProperty(ref _ScroringPreferencesTemplateName, value);
            }
        }
        private int _ScroringPreferencesTemplatID = -1;
        public int ScroringPreferencesTemplatID
        {
            get { return _ScroringPreferencesTemplatID; }
            set
            {
                SetProperty(ref _ScroringPreferencesTemplatID, value);
            }
        }
        private bool _Editable = true;
        public bool Editable
        {
            get { return _Editable; }
            set
            {
                SetProperty(ref _Editable, value);
            }
        }
        #region Scoring Award

        private bool _DiffrentPointSystemForMaleAndFemale = false;
        public bool DiffrentPointSystemForMaleAndFemale
        {
            get { return _DiffrentPointSystemForMaleAndFemale; }
            set
            {
                SetProperty(ref _DiffrentPointSystemForMaleAndFemale, value);
            }
        }

        private bool _DiffrentPointSystemForEach = false;
        public bool DiffrentPointSystemForEach
        {
            get { return _DiffrentPointSystemForEach; }
            set
            {
                SetProperty(ref _DiffrentPointSystemForEach, value);
            }
        }

        private bool _ExceedOrEqualQualifingMarks = false;
        public bool ExceedOrEqualQualifingMarks
        {
            get { return _ExceedOrEqualQualifingMarks; }
            set
            {
                SetProperty(ref _ExceedOrEqualQualifingMarks, value);
            }
        }

        private bool _ScoreFastestHeadOnlyRegardLessOverAllPlace = false;
        public bool ScoreFastestHeadOnlyRegardLessOverAllPlace
        {
            get { return _ScoreFastestHeadOnlyRegardLessOverAllPlace; }
            set
            {
                SetProperty(ref _ScoreFastestHeadOnlyRegardLessOverAllPlace, value);
            }
        }
        private bool _ScoreEqualRelayOnly = false;
        public bool ScoreEqualRelayOnly
        {
            get { return _ScoreEqualRelayOnly; }
            set
            {
                SetProperty(ref _ScoreEqualRelayOnly, value);
            }
        }
        private bool _ExhibitionMarksReceiveFinalsRanks = false;
        public bool ExhibitionMarksReceiveFinalsRanks
        {
            get { return _ExhibitionMarksReceiveFinalsRanks; }
            set
            {
                SetProperty(ref _ExhibitionMarksReceiveFinalsRanks, value);
            }
        }
        private bool _TreateForeignAthletesAsExhibition = false;
        public bool TreateForeignAthletesAsExhibition
        {
            get { return _TreateForeignAthletesAsExhibition; }
            set
            {
                SetProperty(ref _TreateForeignAthletesAsExhibition, value);
            }
        }

        private bool _AllowForeignAthletesPointScoreToCountTowordTeamScore = false;
        public bool AllowForeignAthletesPointScoreToCountTowordTeamScore
        {
            get { return _AllowForeignAthletesPointScoreToCountTowordTeamScore; }
            set
            {
                SetProperty(ref _AllowForeignAthletesPointScoreToCountTowordTeamScore, value);
            }
        }

        private int _MaximumScorePerTeamPerEventIndividual;
        public int MaximumScorePerTeamPerEventIndividual
        {
            get { return _MaximumScorePerTeamPerEventIndividual; }
            set
            {
                SetProperty(ref _MaximumScorePerTeamPerEventIndividual, value);
            }
        }

        private int _MaximumScorePerTeamPerEventRelay;
        public int MaximumScorePerTeamPerEventRelay
        {
            get { return _MaximumScorePerTeamPerEventRelay; }
            set
            {
                SetProperty(ref _MaximumScorePerTeamPerEventRelay, value);
            }
        }

        private int _TopHowManyForAwardLabelIndividual;
        public int TopHowManyForAwardLabelIndividual
        {
            get { return _TopHowManyForAwardLabelIndividual; }
            set
            {
                SetProperty(ref _TopHowManyForAwardLabelIndividual, value);
            }
        }

        private int _TopHowManyForAwardLabelRelay;
        public int TopHowManyForAwardLabelRelay
        {
            get { return _TopHowManyForAwardLabelRelay; }
            set
            {
                SetProperty(ref _TopHowManyForAwardLabelRelay, value);
            }
        }
        private int _MaximumPerTeamFinalFromPreLimsIndividual;
        public int MaximumPerTeamFinalFromPreLimsIndividual
        {
            get { return _MaximumPerTeamFinalFromPreLimsIndividual; }
            set
            {
                SetProperty(ref _MaximumPerTeamFinalFromPreLimsIndividual, value);
            }
        }
        private int _MaximumPerTeamFinalFromPreLimsRelay;
        public int MaximumPerTeamFinalFromPreLimsRelay
        {
            get { return _MaximumPerTeamFinalFromPreLimsRelay; }
            set
            {
                SetProperty(ref _MaximumPerTeamFinalFromPreLimsRelay, value);
            }
        }

        private bool _IncludeRelayPointsWithIndivHighPointScore = false;
        public bool IncludeRelayPointsWithIndivHighPointScore
        {
            get { return _IncludeRelayPointsWithIndivHighPointScore; }
            set
            {
                SetProperty(ref _IncludeRelayPointsWithIndivHighPointScore, value);
            }
        }
        #endregion

        #region EntryLimits
        private int _MaximumEntriesPerAthleteIncludingRelays;
        public int MaximumEntriesPerAthleteIncludingRelays
        {
            get { return _MaximumEntriesPerAthleteIncludingRelays; }
            set
            {
                SetProperty(ref _MaximumEntriesPerAthleteIncludingRelays, value);
            }
        }

        private int _MaximumTrackEventEntriesPerAthleteIncludingRelay;
        public int MaximumTrackEventEntriesPerAthleteIncludingRelay
        {
            get { return _MaximumTrackEventEntriesPerAthleteIncludingRelay; }
            set
            {
                SetProperty(ref _MaximumTrackEventEntriesPerAthleteIncludingRelay, value);
            }
        }

        private int _MaximumFieldEventEntriesPerAthletes;
        public int MaximumFieldEventEntriesPerAthletes
        {
            get { return _MaximumFieldEventEntriesPerAthletes; }
            set
            {
                SetProperty(ref _MaximumFieldEventEntriesPerAthletes, value);
            }
        }
        private bool _WarnIfEntryLimitsExceeds = false;
        public bool WarnIfEntryLimitsExceeds
        {
            get { return _WarnIfEntryLimitsExceeds; }
            set
            {
                SetProperty(ref _WarnIfEntryLimitsExceeds, value);
            }
        }
        #endregion

        #region Entry or Result
        private bool _UseEntryDeclarationMethod = false;
        public bool UseEntryDeclarationMethod
        {
            get { return _UseEntryDeclarationMethod; }
            set
            {
                SetProperty(ref _UseEntryDeclarationMethod, value);
            }
        }

        private bool _EntryNotesWithEachEntry = false;
        public bool EntryNotesWithEachEntry
        {
            get { return _EntryNotesWithEachEntry; }
            set
            {
                SetProperty(ref _EntryNotesWithEachEntry, value);
            }
        }

        private bool _ConvertHandEntryTimesToFAT = false;
        public bool ConvertHandEntryTimesToFAT
        {
            get { return _ConvertHandEntryTimesToFAT; }
            set
            {
                SetProperty(ref _ConvertHandEntryTimesToFAT, value);
            }
        }

        private bool _ShowFinishedPlaceWithEachEntry = false;
        public bool ShowFinishedPlaceWithEachEntry
        {
            get { return _ShowFinishedPlaceWithEachEntry; }
            set
            {
                SetProperty(ref _ShowFinishedPlaceWithEachEntry, value);
            }
        }
        private bool _RecordWindReading = false;
        public bool RecordWindReading
        {
            get { return _RecordWindReading; }
            set
            {
                SetProperty(ref _RecordWindReading, value);
            }
        }

        private bool _RoundUpResultToTenthofReport = false;
        public bool RoundUpResultToTenthofReport
        {
            get { return _RoundUpResultToTenthofReport; }
            set
            {
                SetProperty(ref _RoundUpResultToTenthofReport, value);
            }
        }
        private bool _RankResultByAgeGradedMarks = false;
        public bool RankResultByAgeGradedMarks
        {
            get { return _RankResultByAgeGradedMarks; }
            set
            {
                SetProperty(ref _RankResultByAgeGradedMarks, value);
            }
        }
        private bool _WarnIfTimesMarksAreOutofRange = false;
        public bool WarnIfTimesMarksAreOutofRange
        {
            get { return _WarnIfTimesMarksAreOutofRange; }
            set
            {
                SetProperty(ref _WarnIfTimesMarksAreOutofRange, value);
            }
        }
        private bool _RoundDownTheNearestEventCentimerer = false;
        public bool RoundDownTheNearestEventCentimerer
        {
            get { return _RoundDownTheNearestEventCentimerer; }
            set
            {
                SetProperty(ref _RoundDownTheNearestEventCentimerer, value);
            }
        }
        #endregion

        #region 23DualMeet
        private ICommand _SelectTeamCommand;
        public ICommand SelectTeamCommand
        {
            get { return _SelectTeamCommand; }
        }

        #endregion

        #region Age Grading
        private bool _UseOneYearAgingTableStandared = false;
        public bool UseOneYearAgingTableStandared
        {
            get { return _UseOneYearAgingTableStandared; }
            set
            {
                SetProperty(ref _UseOneYearAgingTableStandared, value);
            }
        }
        private bool _UseFiveYearAgingTableStandared = false;
        public bool UseFiveYearAgingTableStandared
        {
            get { return _UseFiveYearAgingTableStandared; }
            set
            {
                SetProperty(ref _UseFiveYearAgingTableStandared, value);
            }
        }

        private bool _UseOneYearAgingTableMulti = false;
        public bool UseOneYearAgingTableMulti
        {
            get { return _UseOneYearAgingTableMulti; }
            set
            {
                SetProperty(ref _UseOneYearAgingTableMulti, value);
            }
        }
        private bool _UseFiveYearAgingTableMulti = false;
        public bool UseFiveYearAgingTableMulti
        {
            get { return _UseFiveYearAgingTableMulti; }
            set
            {
                SetProperty(ref _UseFiveYearAgingTableMulti, value);
            }
        }

        #endregion

        #region CC/RR
        private int _MaximumRunnerToQualifyAsATeam;
        public int MaximumRunnerToQualifyAsATeam
        {
            get { return _MaximumRunnerToQualifyAsATeam; }
            set
            {
                SetProperty(ref _MaximumRunnerToQualifyAsATeam, value);
            }
        }

        private int _NumberOfRunnersScoreFromEachTeam;
        public int NumberOfRunnersScoreFromEachTeam
        {
            get { return _NumberOfRunnersScoreFromEachTeam; }
            set
            {
                SetProperty(ref _NumberOfRunnersScoreFromEachTeam, value);
            }
        }

        private int _MaximumDisplacerPerTeam;
        public int MaximumDisplacerPerTeam
        {
            get { return _MaximumDisplacerPerTeam; }
            set
            {
                SetProperty(ref _MaximumDisplacerPerTeam, value);
            }
        }

        private bool _NoneTeamRunnerDisplacedOtherRunner;
        public bool NoneTeamRunnerDisplacedOtherRunner
        {
            get { return _NoneTeamRunnerDisplacedOtherRunner; }
            set
            {
                SetProperty(ref _NoneTeamRunnerDisplacedOtherRunner, value);
            }
        }

        private bool _DynamicMultipleTeamScoring;
        public bool DynamicMultipleTeamScoring
        {
            get { return _DynamicMultipleTeamScoring; }
            set
            {
                SetProperty(ref _DynamicMultipleTeamScoring, value);
            }
        }

        private int _PanaltyPointsAddedForShortTeams;
        public int PanaltyPointsAddedForShortTeams
        {
            get { return _PanaltyPointsAddedForShortTeams; }
            set
            {
                SetProperty(ref _PanaltyPointsAddedForShortTeams, value);
            }
        }
        private bool _PanaltyPointAddedForShortTeamLast1Last2Etc;
        public bool PanaltyPointAddedForShortTeamLast1Last2Etc
        {
            get { return _PanaltyPointAddedForShortTeamLast1Last2Etc; }
            set
            {
                SetProperty(ref _PanaltyPointAddedForShortTeamLast1Last2Etc, value);
            }
        }
        private int _DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6;
        public int DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6
        {
            get { return _DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6; }
            set
            {
                SetProperty(ref _DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6, value);
            }
        }
        private int _HowManuTopOverAllRunnersToExcludeFromAgeGroupResult;
        public int HowManuTopOverAllRunnersToExcludeFromAgeGroupResult
        {
            get { return _HowManuTopOverAllRunnersToExcludeFromAgeGroupResult; }
            set
            {
                SetProperty(ref _HowManuTopOverAllRunnersToExcludeFromAgeGroupResult, value);
            }
        }


        #endregion

        #region FieldRelay
        private int _MarksWillBeAddedForReamRanking;
        public int MarksWillBeAddedForReamRanking
        {
            get { return _MarksWillBeAddedForReamRanking; }
            set
            {
                SetProperty(ref _MarksWillBeAddedForReamRanking, value);
            }
        }

        private bool _FieldRelayCanBeScoreWithUnderSizeTeam;
        public bool FieldRelayCanBeScoreWithUnderSizeTeam
        {
            get { return _FieldRelayCanBeScoreWithUnderSizeTeam; }
            set
            {
                SetProperty(ref _FieldRelayCanBeScoreWithUnderSizeTeam, value);
            }
        }
        private int _TimesWillBeAddedForTeamRanking;
        public int TimesWillBeAddedForTeamRanking
        {
            get { return _TimesWillBeAddedForTeamRanking; }
            set
            {
                SetProperty(ref _TimesWillBeAddedForTeamRanking, value);
            }
        }

        #endregion


        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

    }
}

