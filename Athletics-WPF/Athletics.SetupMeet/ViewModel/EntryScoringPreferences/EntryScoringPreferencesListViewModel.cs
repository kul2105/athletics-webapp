﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Collections.Generic;
using Athletics.SetupMeet.View.AssigPrefrences;

namespace Athletics.MeetSetup.ViewModels
{
    public class EntryScoringPreferencesListViewModel : BindableBase, IEntryScoringPreferencesListViewModel, IGenericInteractionView<EntryScoringPreferencesListViewModel>
    {
        public IEntryScoringPreferencesListView _view;
        IUnityContainer unityContainer;
        private IMeetSetupManage MeetSetupManage;
        public EntryScoringPreferencesListViewModel(IEntryScoringPreferencesListView view, IUnityContainer unity, IMeetSetupManage MeetMng)
        {


            MeetSetupManage = MeetMng;
            unityContainer = unity;
            GetAllScoringPreferencesList();
            _AddTemplateCommand = new DelegateCommand(AddTemplateCommandHandler);
            _UpdateTemplateCommand = new DelegateCommand(UpdateTemplateCommandHandler);
            _DeleteTemplateCommand = new DelegateCommand(DeleteTemplateCommandHandler);
            view.DataContext = this;
            _view = view;

        }

        #region Methods
        public EntryScoringPreferencesListViewModel GetEntity()
        {
            GetAllScoringPreferencesList();
            return new EntryScoringPreferencesListViewModel(_view, unityContainer, MeetSetupManage);
        }

        public void SetEntity(EntryScoringPreferencesListViewModel entity)
        {
        }
        private void DeleteTemplateCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedScoringPreference != null)
                {
                    AuthorizedResponse<ScoringPreferencesResult> Response = await MeetSetupManage.DeletScoringPreferencesTemplate(SelectedScoringPreference.ScroringPreferencesTemplatID);
                    this.ScoringPreferencesList.Remove(SelectedScoringPreference);
                }
            })).Wait();
        }

        private void UpdateTemplateCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedScoringPreference == null) return;
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<IEntryScoringPreferencesView>() as Window;
                if (this.SelectedScoringPreference != null)
                {
                    this.SelectedScoringPreference.Editable = false;
                    dialog.BindView(window);
                    dialog.BindViewModel(this.SelectedScoringPreference);
                }
                dialog.ShowDialog();
                dialog.Close();
            })).Wait();

        }

        private void GetAllScoringPreferencesList()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<EntryScoringPreferencesViewModel> Response = await MeetSetupManage.GetAllScoringPreferencesTemplate();
                foreach (var item in Response)
                {
                    ScoringPreferencesList.Add(item);
                }
            })).Wait();
        }
        private void AddTemplateCommandHandler()
        {

            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<IEntryScoringPreferencesView>() as Window;
                IGenericInteractionView<EntryScoringPreferencesViewModel> createDesignDataSetViewModel = unityContainer.Resolve<IEntryScoringPreferencesViewModel>() as IGenericInteractionView<EntryScoringPreferencesViewModel>;
                if (createDesignDataSetViewModel != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedScoringPreference);
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                dialog.Close();
                SelectedScoringPreference = createDesignDataSetViewModel.GetEntity();
                TemplateNameView templateView = new TemplateNameView();
                templateView.Height = 200;
                templateView.Width = 300;
                templateView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                templateView.ShowDialog();
                SelectedScoringPreference.ScroringPreferencesTemplateName = templateView.TemplateName;
                if (SelectedScoringPreference != null)
                {
                    this.ScoringPreferencesList.Add(SelectedScoringPreference);
                }
                if (SelectedScoringPreference != null)
                {
                    ServiceRequest_ScoringPreferences scoringPreferences = new ServiceRequest_ScoringPreferences();
                    scoringPreferences.ScoringPreferencesViewModel = SelectedScoringPreference;
                    AuthorizedResponse<ScoringPreferencesResult> Response = await MeetSetupManage.CreateScoringPreferencesTemplate(scoringPreferences);
                    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show(Response.Message);
                    }
                }
            })).Wait();

        }
        #endregion

        #region Properties
        private EntryScoringPreferencesViewModel _SelectedScoringPreference;
        public EntryScoringPreferencesViewModel SelectedScoringPreference
        {
            get { return _SelectedScoringPreference; }
            set
            {
                _SelectedScoringPreference = value;
                base.OnPropertyChanged("SelectedScoringPreference");
            }
        }

        private ObservableCollection<EntryScoringPreferencesViewModel> _ScoringPreferencesList = new ObservableCollection<EntryScoringPreferencesViewModel>();
        public ObservableCollection<EntryScoringPreferencesViewModel> ScoringPreferencesList
        {
            get { return _ScoringPreferencesList; }
            set
            {
                _ScoringPreferencesList = value;
                base.OnPropertyChanged("ScoringPreferencesList");
            }
        }

        #endregion

        #region Commands
        private ICommand _AddTemplateCommand;
        public ICommand AddTemplateCommand
        {
            get
            {
                return _AddTemplateCommand;
            }
        }

        private ICommand _UpdateTemplateCommand;
        public ICommand UpdateTemplateCommand
        {
            get
            {
                return _UpdateTemplateCommand;
            }
        }

        private ICommand _DeleteTemplateCommand;
        public ICommand DeleteTemplateCommand
        {
            get
            {
                return _DeleteTemplateCommand;
            }
        }
        #endregion
    }

}

