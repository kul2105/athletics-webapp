﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.ModuleCommon;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.MeetSetup.ViewModels
{

    public class ScoringSetupViewModel : BindableBase, IScoringSetupViewModel, IGenericInteractionView<ServiceRequest_ScoringSetup>
    {
        private IScoringSetupView View = null;
        internal bool IsInCloseMode = false;
        private IMeetSetupStateController MeetSetupStateController;
        private SetupMeetManager.IMeetSetupManage MeetSetupManage;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        private ServiceRequest_ScoringSetup scoringSetup;
        public ScoringSetupViewModel()
        {

        }
        public ScoringSetupViewModel(IUnityContainer unity, IScoringSetupView view, IMeetSetupManage meetSetupManage, IMeetSetupStateController meetSetupController)
        {
            View = view;
            unityContainer = unity;
            MeetSetupStateController = meetSetupController;
            MeetSetupManage = meetSetupManage;
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Task.Run(async () =>
                           {


                               if (ScoringSetupTemplateDetailList != null)
                               {

                                   //ServiceRequest_ScoringSetup serviceObj = new ServiceRequest_ScoringSetup();
                                   //serviceObj.ScoringSetupTemplateList = this.ScoringSetupTemplateDetailList.ToList();
                                   //AuthorizedResponse<ScoringSetupResult> Response = await meetSetupManage.CreateScoringMeetTemplate(serviceObj);
                                   // this.AthleteRelayPreferencesId = Response.Result.ScoringSetupID;
                               }
                               await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {
                                    Window window = view as Window;
                                    if (window != null)
                                    {
                                        window.Visibility = Visibility.Hidden;
                                    }
                                }));
                           });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });

            _CancelCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {

                    Window window = view as Window;
                    if (window != null)
                    {
                        window.Visibility = Visibility.Hidden;
                    }
                }));

            });

            _DefaultCommand = new DelegateCommand(async () =>
            {
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                 {
                     await Task.Run(async () =>
                     {
                         await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             scoringSetup = await meetSetupManage.GetScoringSetupDefault();
                             scoringSetup.TemplateOrMeetSetupID = -1;
                             if (scoringSetup == null)
                             {
                                 MessageBox.Show("No Template is set as Default", "Default Template");
                                 scoringSetup = new ServiceRequest_ScoringSetup();
                             }
                             this.ScoringSetupTemplateDetailList.ToList().Clear();
                             foreach (var item in scoringSetup.ScoringSetupTemplateList)
                             {
                                 ScoringMeetModel model = new ScoringMeetModel();
                                 model.CombEvtPoints = item.CombEvtPoints == null ? 0 : item.CombEvtPoints.Value;
                                 model.IndividualPints = item.IndividualPints == null ? 0 : item.IndividualPints.Value;
                                 model.Place = item.Place == null ? 0 : item.Place.Value;
                                 model.RelayPoints = item.RelayPoints == null ? 0 : item.RelayPoints.Value;
                                 this.ScoringSetupTemplateDetailList.Add(model);
                             }
                         }));

                     });

                 }));
            });

            _AddNextRowCommand = new DelegateCommand(async () =>
           {
               ScoringMeetModel scoringDataSet = new ScoringMeetModel();
               ScoringSetupTemplateDetailList.Add(scoringDataSet);
               this.SelectedScoringSetupTemplateDetail = ScoringSetupTemplateDetailList[ScoringSetupTemplateDetailList.Count - 1];
           });
            View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            return true;

        }

        public void SetEntity(ServiceRequest_ScoringSetup entity)
        {
            if (entity == null) return;
            scoringSetup = entity;
            this.ScoringSetupTemplateDetailList.Clear();
            if (scoringSetup.ScoringSetupTemplateList == null) return;
            foreach (var item in scoringSetup.ScoringSetupTemplateList)
            {
                ScoringMeetModel model = new ScoringMeetModel();
                model.CombEvtPoints = item.CombEvtPoints == null ? 0 : item.CombEvtPoints.Value;
                model.IndividualPints = item.IndividualPints == null ? 0 : item.IndividualPints.Value;
                model.Place = item.Place == null ? 0 : item.Place.Value;
                model.RelayPoints = item.RelayPoints == null ? 0 : item.RelayPoints.Value;
                this.ScoringSetupTemplateDetailList.Add(model);
            }
        }


        public ServiceRequest_ScoringSetup GetEntity()
        {
            scoringSetup.ScoringSetupTemplateList = new System.Collections.Generic.List<ScoringSetupTemplateDetail>();
            foreach (var item in ScoringSetupTemplateDetailList)
            {
                ScoringSetupTemplateDetail scoringDetail = new ScoringSetupTemplateDetail();
                scoringDetail.CombEvtPoints = item.CombEvtPoints;
                scoringDetail.IndividualPints = item.IndividualPints;
                scoringDetail.Place = item.Place;
                scoringDetail.RelayPoints = item.RelayPoints;
                scoringSetup.ScoringSetupTemplateList.Add(scoringDetail);
            }
            return scoringSetup;
        }

        private ObservableCollection<ScoringMeetModel> _ScoringSetupTemplateDetailList = new ObservableCollection<ScoringMeetModel>();
        public ObservableCollection<ScoringMeetModel> ScoringSetupTemplateDetailList
        {
            get { return _ScoringSetupTemplateDetailList; }
            set
            {
                _ScoringSetupTemplateDetailList = value;
                base.OnPropertyChanged("ScoringSetupTemplateDetailList");
            }
        }

        private ScoringMeetModel _SelectedScoringSetupTemplateDetail = new ScoringMeetModel();
        public ScoringMeetModel SelectedScoringSetupTemplateDetail
        {
            get { return _SelectedScoringSetupTemplateDetail; }
            set
            {
                _SelectedScoringSetupTemplateDetail = value;
                base.OnPropertyChanged("SelectedScoringSetupTemplateDetail");
            }
        }

        private bool _IsReadonly = false;
        public bool IsReadonly
        {
            get { return _IsReadonly; }
            set
            {
                _IsReadonly = value;
                base.OnPropertyChanged("IsReadonly");
            }
        }
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        private ICommand _DefaultCommand;
        public ICommand DefaultCommand
        {
            get
            {
                return _DefaultCommand;
            }
        }
        private ICommand _AddNextRowCommand;
        public ICommand AddNextRowCommand
        {
            get
            {
                return _AddNextRowCommand;
            }
        }


    }
}

