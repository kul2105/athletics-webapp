﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.ModuleCommon;
using Athletics.SetupMeet.View.AssigPrefrences;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.MeetSetup.ViewModels
{
    public class ScoringSetupListViewModel : BindableBase, IScoringSetupListViewModel, IGenericInteractionView<ScoringSetupListViewModel>
    {
        public IScoringSetupListView _view;
        IUnityContainer unityContainer;
        private IMeetSetupManage MeetSetupManage;
        public ScoringSetupListViewModel(IScoringSetupListView view, IUnityContainer unity, IMeetSetupManage MeetMng)
        {

            MeetSetupManage = MeetMng;
            unityContainer = unity;
            GetAllScoringSetupTemplateList();
            view.DataContext = this;
            _view = view;
            _CreateScoringCommand = new DelegateCommand(CreateScoringCommandHandler);
            _UpdateScoringCommand = new DelegateCommand(UpdateScoringCommandHandler);
            _DeleteScoringCommand = new DelegateCommand(DeleteScoringCommandHandler);
        }

        private void DeleteScoringCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedScoringSetupTemplate != null)
                {
                    AuthorizedResponse<ScoringSetupResult> Response = await MeetSetupManage.DeletScoringSetupTemplate(SelectedScoringSetupTemplate.TemplateOrMeetSetupID);
                    this.ScoringSetupTemplateList.Remove(SelectedScoringSetupTemplate);
                }
            })).Wait();
        }

        private void UpdateScoringCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedScoringSetupTemplate == null) return;
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<IScoringSetupView>() as Window;
                IGenericInteractionView<ServiceRequest_ScoringSetup> createDesignDataSetViewModel = unityContainer.Resolve<IScoringSetupViewModel>() as IGenericInteractionView<ServiceRequest_ScoringSetup>;
                if (this.SelectedScoringSetupTemplate != null)
                {
                   
                    createDesignDataSetViewModel.SetEntity(this.SelectedScoringSetupTemplate);
                    ScoringSetupViewModel setupVM = createDesignDataSetViewModel as ScoringSetupViewModel;
                    if (setupVM != null)
                    {
                        setupVM.IsReadonly = false;
                    }
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                dialog.Close();
                SelectedScoringSetupTemplate = createDesignDataSetViewModel.GetEntity();
                if (SelectedScoringSetupTemplate != null)
                {
                    AuthorizedResponse<ScoringSetupResult> Response = await MeetSetupManage.CreateScoringMeetTemplate(SelectedScoringSetupTemplate);
                    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show(Response.Message);
                    }
                }
            })).Wait();
        }

        private void CreateScoringCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<IScoringSetupView>() as Window;
                IGenericInteractionView<ServiceRequest_ScoringSetup> createDesignDataSetViewModel = unityContainer.Resolve<IScoringSetupViewModel>() as IGenericInteractionView<ServiceRequest_ScoringSetup>;
                if (createDesignDataSetViewModel != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedScoringSetupTemplate);
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                dialog.Close();
                SelectedScoringSetupTemplate = createDesignDataSetViewModel.GetEntity();
                TemplateNameView templateView = new TemplateNameView();
                templateView.Height = 200;
                templateView.Width = 300;
                templateView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                templateView.ShowDialog();
                if (string.IsNullOrEmpty(templateView.TemplateName)) return;
                SelectedScoringSetupTemplate.TemplateName = templateView.TemplateName;
                MessageBoxResult result = MessageBox.Show("Do you wants this Template as Default Template", "Default Template", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    SelectedScoringSetupTemplate.IsDefault = true;
                }
                else
                {
                    SelectedScoringSetupTemplate.IsDefault = false;
                }
                if (SelectedScoringSetupTemplate != null)
                {
                    this.ScoringSetupTemplateList.Add(SelectedScoringSetupTemplate);
                }
                if (SelectedScoringSetupTemplate != null)
                {
                    AuthorizedResponse<ScoringSetupResult> Response = await MeetSetupManage.CreateScoringMeetTemplate(SelectedScoringSetupTemplate);
                    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show(Response.Message);
                    }
                }
            })).Wait();
        }

        #region Methods
        public ScoringSetupListViewModel GetEntity()
        {
            GetAllScoringSetupTemplateList();
            return new ScoringSetupListViewModel(_view, unityContainer, MeetSetupManage);
        }
        private bool ValidateInputs()
        {
            return true;

        }
        public void SetEntity(ScoringSetupListViewModel entity)
        {
        }

        private void GetAllScoringSetupTemplateList()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                ScoringSetupTemplateList = new ObservableCollection<ServiceRequest_ScoringSetup>();
                List<ServiceRequest_ScoringSetup> Response = await MeetSetupManage.GetAllScoringSetupTemplate();
                foreach (var item in Response)
                {
                    ScoringSetupTemplateList.Add(item);
                }
            })).Wait();
        }
        #endregion

        #region Properties

        private ObservableCollection<ServiceRequest_ScoringSetup> _ScoringSetupTemplateList = new ObservableCollection<ServiceRequest_ScoringSetup>();
        public ObservableCollection<ServiceRequest_ScoringSetup> ScoringSetupTemplateList
        {
            get { return _ScoringSetupTemplateList; }
            set
            {
                _ScoringSetupTemplateList = value;
                base.OnPropertyChanged("ScoringSetupTemplateList");
            }
        }

        private ServiceRequest_ScoringSetup _SelectedScoringSetupTemplate = new ServiceRequest_ScoringSetup();
        public ServiceRequest_ScoringSetup SelectedScoringSetupTemplate
        {
            get { return _SelectedScoringSetupTemplate; }
            set
            {
                _SelectedScoringSetupTemplate = value;
                base.OnPropertyChanged("SelectedScoringSetupTemplate");
            }
        }
        #endregion

        #region Commands

        private ICommand _CreateScoringCommand;
        public ICommand CreateScoringCommand
        {
            get { return _CreateScoringCommand; }
        }
        private ICommand _UpdateScoringCommand;
        public ICommand UpdateScoringCommand
        {
            get { return _UpdateScoringCommand; }
        }
        private ICommand _DeleteScoringCommand;
        public ICommand DeleteScoringCommand
        {
            get { return _DeleteScoringCommand; }
        }

        #endregion
    }

}

