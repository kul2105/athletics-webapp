﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_TemplateName
    {
        public int Preferenceid { get; set; }
        public string TemplateName { get; set; }
    }


}
