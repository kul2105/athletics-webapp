﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.ViewModels;
using Athletics.MeetSetup.ViewModels.SeedPreferences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_DivisionRegionNames
    {
        public List<DivisionRegionName> DivisionRegionNameList { get; set; }
    }
}
