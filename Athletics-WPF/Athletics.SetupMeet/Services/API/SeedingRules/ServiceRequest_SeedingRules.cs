﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_SeedingRules
    {
        public bool AllowForeignAthletesInFinal { get; set; }
        public bool AllowExhibitionAthletesInFinal { get; set; }
        public bool SeedExhibitionAthletesLast { get; set; }
        public bool ApplyNCAARule { get; set; }
        public bool UseSpecialRandomSelectMethod { get; set; }


    }
}
