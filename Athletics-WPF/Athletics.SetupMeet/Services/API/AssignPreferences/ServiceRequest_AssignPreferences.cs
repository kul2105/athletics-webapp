﻿using Athletics.MeetSetup.ViewModels;
using Athletics.MeetSetup.ViewModels.SeedPreferences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_AssignPreferences
    {
        public int MeetSetupID { get; set; }
        public AthleteRelayPreferencesViewModel AthleteRelayPreferencesViewModel { get; set; }
        public SeedPreferencesViewModel SeedPreferencesViewModel { get; set; }
    }
}
