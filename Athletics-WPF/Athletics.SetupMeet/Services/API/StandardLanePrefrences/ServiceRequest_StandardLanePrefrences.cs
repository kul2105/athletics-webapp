﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_StandardLanePrefrences : IServiceRequest_StandardLanePrefrences
    {
        public int StandardLanePrefrencesID { get; set; }
        public List<StandardLanePrefrence> StandardLanePrefrencesList { get; set; }
    }

    public interface IServiceRequest_StandardLanePrefrences
    {

    }
}
