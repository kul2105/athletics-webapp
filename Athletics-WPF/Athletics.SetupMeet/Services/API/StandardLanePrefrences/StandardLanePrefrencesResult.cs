﻿using System;
using System.Collections.Generic;

namespace Athletics.MeetSetup.Services.API
{
    public class StandardLanePrefrencesResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
        public string StandardLanePrefrencesGroupID { get; set; }

    }

}
