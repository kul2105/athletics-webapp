﻿using Athletics.Entities.Models;
using Athletics.SetupMeet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_DualMeet
    {
        public List<LaneDetailModel> LaneLaneDetailModelList { get; set; }
    }
       
}
