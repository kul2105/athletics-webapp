﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_DualMeetAssignment
    {
        public bool StrictAssignmentAllHeats { get; set; }
        public bool StrictAssignmentFastestHeatOnly { get; set; }
        public bool UseLaneAssignmentsForInLaneRacesOnly { get; set; }
        public bool UserLaneOrPositionAssignmentsAbove { get; set; }
        public bool AlternamtUserOfUnAssignedLane { get; set; }
        public string DualMeetGroupID { get; set; }
    }
}
