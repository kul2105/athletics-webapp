﻿using System;
using System.Collections.Generic;

namespace Athletics.MeetSetup.Services.API
{
    public class DualMeetAssignmentResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
        public int DualMeetAssignmentTranID { get; set; }

    }

}
