﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_RendomizationRule
    {
        public string TimedFinalEvents { get; set; }
        public string RoundFirstMultipleRound { get; set; }
        public string RoundSecondThirdAndForth { get; set; }
        public bool CloseGap { get; set; }
    }
}
