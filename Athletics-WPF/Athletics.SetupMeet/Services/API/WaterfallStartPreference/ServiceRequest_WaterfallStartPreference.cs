﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_WaterfallStartPreference : IServiceRequest_WaterfallStartPreference
    {
        public int WaterfallStartPreferenceID { get; set; }
        public List<WaterfallStartPreference> WaterfallStartPreferenceList { get; set; }
    }

    public interface IServiceRequest_WaterfallStartPreference
    {

    }
}
