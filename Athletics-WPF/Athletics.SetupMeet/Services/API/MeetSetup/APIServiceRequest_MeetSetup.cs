﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class APIServiceRequest_MeetSetup
    {
        public int MeetSetupID { get; set; }
        public string MeetSetupName { get; set; }
        public string MeetSetupName2 { get; set; }
        public DateTime? MeetSetupStartDate { get; set; }
        public DateTime? MeetSetupEndDate { get; set; }
        public DateTime? MeetSetupAgeUpDate { get; set; }
        public int MeetKindID { get; set; }
        public int MeetClassID { get; set; }
        public int MeetTypeID { get; set; }
        public int MeetCountyID { get; set; }
        public int MeetArenaID { get; set; }
        public int MeetStyleID { get; set; }
        public int MeetTypeOptionID { get; set; }
        public bool? UseDivisionBirthDateRange { get; set; }
        public bool? LinkTermToDivision { get; set; }

    }
}
