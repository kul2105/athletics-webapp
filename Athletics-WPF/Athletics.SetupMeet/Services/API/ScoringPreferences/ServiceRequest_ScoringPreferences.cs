﻿using Athletics.MeetSetup.ViewModels;
using Athletics.MeetSetup.ViewModels.SeedPreferences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_ScoringPreferences
    {
        public int TemplateOrMeetSetupID { get; set; }
        public EntryScoringPreferencesViewModel ScoringPreferencesViewModel { get; set; }
    }
}
