﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_SeedingPreferences
    {
        public int SeedingPreferencesID { get; set; }
        public string WaterfallStartPreferencesGroupID { get; set; }
        public string StandardLanePrefrencesGroupID { get; set; }
        public string StandardAlleyPrefrencesGroupID { get; set; }
        public string DualMeetGroupID { get; set; }
        public string SeedingRulesGroupID { get; set; }
        public string RendomizationRuleGroupID { get; set; }

    }
}
