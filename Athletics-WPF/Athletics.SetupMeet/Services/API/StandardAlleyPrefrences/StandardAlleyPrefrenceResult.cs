﻿using System;
using System.Collections.Generic;

namespace Athletics.MeetSetup.Services.API
{
    public class StandardAlleyPrefrenceResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
        public string StandardAlleyPrefrenceGroupID { get; set; }

    }

}
