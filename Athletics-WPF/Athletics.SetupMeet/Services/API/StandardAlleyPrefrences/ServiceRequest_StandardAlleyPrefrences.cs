﻿using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_StandardAlleyPrefrences : IServiceRequest_StandardAlleyPrefrences
    {
        public int AthleteRelayPrefrencesID { get; set; }
        public List<StandardAlleyPrefrence> StandardAlleyPrefrencesList { get; set; }
    }

    public interface IServiceRequest_StandardAlleyPrefrences
    {

    }
}
