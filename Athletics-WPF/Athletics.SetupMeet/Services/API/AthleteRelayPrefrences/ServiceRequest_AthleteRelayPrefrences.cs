﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_AthleteRelayPrefrences
    {
        public int AthleteRelayPrefrencesID { get; set; }
        public List<int> AthletePreferencesList { get; set; }
        public List<int> RelayPreferencesList { get; set; }
        public List<int> CompetitorNumberList { get; set; }

    }
}
