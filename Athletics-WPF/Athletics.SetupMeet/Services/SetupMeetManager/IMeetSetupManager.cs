﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Athletics.MeetSetup.SetupMeetManager
{
    public enum eMEET_REQUEST_RESPONSE
    {
        DEFAULT_ERROR,
        SUCCESS,
    }

    public class AuthorizedResponse<T> where T : class
    {
        public AuthorizedResponse()
        {
            this.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
            Message = "";
            Result = null;
        }

        public eMEET_REQUEST_RESPONSE Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }


    /// <summary>
    /// Anything having to do Meet Setup 
    /// inside this service
    /// </summary>
    public interface IMeetSetupManage
    {
        Task<AuthorizedResponse<MeetSetupResult>> CreateNewMeet(APIServiceRequest_MeetSetup CreateMeetRequest);
        Task<AuthorizedResponse<MeetSetupResult>> UpdateMeet(APIServiceRequest_MeetSetup CreateMeetRequest);
        Task<AuthorizedResponse<MeetSetupResult>> DeleteMeet(int MeetSetupID);
        Task<List<Entities.Models.MeetSetup>> GetAllMeetSetup();
        Task<List<Entities.Models.BaseCounty>> GetAllBaseCounty();
        Task<List<Entities.Models.MeetArena>> GetAllMeetArena();
        Task<List<Entities.Models.MeetClass>> GetAllMeetClass();
        Task<List<Entities.Models.MeetKind>> GetAllMeetKind();
        Task<List<Entities.Models.MeetStyle>> GetAllMeetStyle();
        Task<List<Entities.Models.MeetType>> GetAllMeetType();
        Task<List<Entities.Models.MeetTypeDivision>> GetAllMeetTypeDivision();
        Task<List<Entities.Models.AthletePreference>> GetAllMeetAthletePreference();
        Task<List<Entities.Models.RelayPreference>> GetAllMeetRelayPreference();
        Task<List<Entities.Models.CompetitorNumber>> GetAllMeetCompetitorNumber();
        Task<List<Entities.Models.LaneDetail>> GetAllLaneDetail();
        Task<List<Entities.Models.SchoolDetail>> GetAllSchoolDetail();
        Task<List<Entities.Models.StandardLanePrefrence>> GetAllStandardLanePrefrence();
        Task<List<Entities.Models.StandardAlleyPrefrence>> GetAllStandardAlleyPrefrence();
        Task<List<Entities.Models.WaterfallStartPreference>> GetAllWaterfallStartPreference();
        Task<AuthorizedResponse<AthleteRelayPrefrencesResult>> CreateNewAthletePreference(ServiceRequest_AthleteRelayPrefrences ServiceRequestAthleteRelayPrefrences);
        Task<AuthorizedResponse<StandardAlleyPrefrenceResult>> CreateNewStandardAlleyPrefrences(ServiceRequest_StandardAlleyPrefrences ServiceRequestStandardAlleyPrefrence);
        Task<AuthorizedResponse<StandardLanePrefrencesResult>> CreateNewStandardLanePrefrences(ServiceRequest_StandardLanePrefrences ServiceRequestStandardLanePrefrences);
        Task<AuthorizedResponse<WaterfallStartPreferenceResult>> CreateNewWaterfallStartPreference(ServiceRequest_WaterfallStartPreference ServiceRequestWaterfallStartPreference);
        Task<AuthorizedResponse<DualMeetResult>> CreateNewDualMeet(ServiceRequest_DualMeet ServiceRequest_DualMeet);
        Task<AuthorizedResponse<SeedingRulesResult>> CreateNewSeedingRules(ServiceRequest_SeedingRules ServiceRequest_SeedingRules);
        Task<AuthorizedResponse<DualMeetAssignmentResult>> CreateNewDualMeetAssignment(ServiceRequest_DualMeetAssignment ServiceRequest_DualMeetAssignment);
        Task<AuthorizedResponse<RendomizationRuleResult>> CreateNewRendomizationRule(ServiceRequest_RendomizationRule ServiceRequest_RendomizationRule);
        Task<AuthorizedResponse<SeedingPreferencesResult>> CreateNewSeedingPreferences(ServiceRequest_SeedingPreferences ServiceRequest_SeedingPreferences);
        Task<List<GetRelayPrefrencesGroupData_Result>> GetAllRelayPrefrencesGroupData();
        Task<List<GetSeedingPreferencesGroupDetail_Result>> GetAllSeedingPreferencesGroup();
        Task<AuthorizedResponse<AssignPreferencesResult>> AssignPreferences(ServiceRequest_AssignPreferences prefrenceToAssign);
        Task<List<GetSeedingPreferencesMeetData_Result>> GetSeedingPreferencesMeetData(int meetSetupID);
        Task<List<GetRelayPrefrencesMeetData_Result>> GetRelayPrefrencesMeetData(int meetSetupID);
        Task<AuthorizedResponse<TemplateNameResult>> CreateRelayPreferenceTemplate(ServiceRequest_TemplateName ReplayPreferenceTemplateInfo);
        Task<AuthorizedResponse<TemplateNameResult>> CreateSeedPreferenceTemplate(ServiceRequest_TemplateName ReplayPreferenceTemplateInfo);

        Task<AuthorizedResponse<ScoringPreferencesResult>> CreateScoringPreferencesTemplate(ServiceRequest_ScoringPreferences scoringPreference);
        Task<AuthorizedResponse<ScoringPreferencesResult>> AssignScoringPreferencesMeet(ServiceRequest_ScoringPreferences scoringPreference);
        Task<List<EntryScoringPreferencesViewModel>> GetAllScoringPreferencesTemplate();
        Task<List<EntryScoringPreferencesViewModel>> GetScoringPreferencesByMeetID(int MeetSetupID);

        Task<AuthorizedResponse<ScoringPreferencesResult>> UpdateAssignedScoringPreferencesMeet(ServiceRequest_ScoringPreferences scoringPreference);
        Task<AuthorizedResponse<ScoringPreferencesResult>> DeleteAssignedScoringPreferencesMeet(int MeetSetupID);
        Task<AuthorizedResponse<ScoringPreferencesResult>> DeletScoringPreferencesTemplate(int TemplateID);

        Task<List<ServiceRequest_ScoringSetup>> GetAllScoringSetupTemplate();
        Task<List<ScoringSetupTemplateDetail>> GetScoringSetupByMeetID(int MeetSetupID);
        Task<ServiceRequest_ScoringSetup> GetScoringSetupDefault();
        Task<AuthorizedResponse<ScoringSetupResult>> DeleteAssignedScoringSetupMeet(int MeetSetupID);
        Task<AuthorizedResponse<ScoringSetupResult>> DeletScoringSetupTemplate(int TemplateID);
        Task<AuthorizedResponse<ScoringSetupResult>> UpdateAssignedScoringSetupMeet(ServiceRequest_ScoringSetup scoringSetup);
        Task<AuthorizedResponse<ScoringSetupResult>> CreateScoringMeetTemplate(ServiceRequest_ScoringSetup scoringSetup);
        Task<AuthorizedResponse<ScoringSetupResult>> AssignScoringMeetTemplate(ServiceRequest_ScoringSetup scoringSetup);
        Task<List<DivisionRegionName>> GetAllDivisionRegionName();
        Task<AuthorizedResponse<DivisionRegionNamesResult>> UpdateDivisionRegionName(ServiceRequest_DivisionRegionNames divisionRegionNames);
    }
}
