﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.ViewModels;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Athletics.MeetSetup.SetupMeetManager
{
    public class MeetSetupManage : BindableBase, IMeetSetupManage
    {
        public async Task<AuthorizedResponse<AssignPreferencesResult>> AssignPreferences(ServiceRequest_AssignPreferences prefrenceToAssign)
        {
            // assign Athletic preferences
            int InsertedId = 0;
            string athletPrefGroupID = Guid.NewGuid().ToString();
            string athletRelayGroupID = Guid.NewGuid().ToString();
            string athletCompNameGroupID = Guid.NewGuid().ToString();
            AuthorizedResponse<AssignPreferencesResult> Result = new AuthorizedResponse<AssignPreferencesResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (prefrenceToAssign.AthleteRelayPreferencesViewModel != null)
                        {
                            ctx.MeetSetupAthletePreferences.RemoveRange(ctx.MeetSetupAthletePreferences.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                            foreach (var item in prefrenceToAssign.AthleteRelayPreferencesViewModel.AthletePreferencesOptions)
                            {
                                AthletePreference prefernces = ctx.AthletePreferences.Where(p => p.AthletePreferencesName == item.Display).FirstOrDefault();
                                if (prefernces != null)
                                {
                                    MeetSetupAthletePreference athletPrefTransaction = new MeetSetupAthletePreference();
                                    athletPrefTransaction.AthletePreferencesID = prefernces.AthletePreferencesID;
                                    athletPrefTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                                    ctx.MeetSetupAthletePreferences.Add(athletPrefTransaction);
                                }
                            }

                            foreach (var item in prefrenceToAssign.AthleteRelayPreferencesViewModel.RelayPreferencesOptions)
                            {
                                ctx.MeetSetupRelayPreferences.RemoveRange(ctx.MeetSetupRelayPreferences.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                                ctx.SaveChanges();
                                RelayPreference prefernces = ctx.RelayPreferences.Where(p => p.RelayPreferencesName == item.Display).FirstOrDefault();
                                if (prefernces != null)
                                {
                                    MeetSetupRelayPreference athletPrefTransaction = new MeetSetupRelayPreference();
                                    athletPrefTransaction.RelayPreferencesID = prefernces.RelayPreferencesID;
                                    athletPrefTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                                    ctx.MeetSetupRelayPreferences.Add(athletPrefTransaction);
                                }
                            }

                            foreach (var item in prefrenceToAssign.AthleteRelayPreferencesViewModel.CompetitorNumbersOptions)
                            {
                                ctx.MeetSetupCompetitorNumbers.RemoveRange(ctx.MeetSetupCompetitorNumbers.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                                ctx.SaveChanges();
                                CompetitorNumber prefernces = ctx.CompetitorNumbers.Where(p => p.CompetitorNumbersName == item.Display).FirstOrDefault();
                                if (prefernces != null)
                                {
                                    MeetSetupCompetitorNumber athletPrefTransaction = new MeetSetupCompetitorNumber();
                                    athletPrefTransaction.CompetitorNumberID = prefernces.CompetitorNumberID;
                                    athletPrefTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                                    ctx.MeetSetupCompetitorNumbers.Add(athletPrefTransaction);
                                }
                            }
                            ctx.SaveChanges();
                        }
                        if (prefrenceToAssign.SeedPreferencesViewModel != null)
                        {
                            ctx.MeetSetupSeedingRules.RemoveRange(ctx.MeetSetupSeedingRules.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                            ctx.SaveChanges();
                            MeetSetupSeedingRule seedingRulesTransaction = new MeetSetupSeedingRule();
                            seedingRulesTransaction.AllowExhibitionAthletesInFinal = prefrenceToAssign.SeedPreferencesViewModel.AllowExhibitionAthletesInFinal;
                            seedingRulesTransaction.AllowForeignAthletesInFinal = prefrenceToAssign.SeedPreferencesViewModel.AllowForeignAthletesInFinal;
                            seedingRulesTransaction.AllowForeignAthletesInFinal = prefrenceToAssign.SeedPreferencesViewModel.AllowExhibitionAthletesInFinal;
                            seedingRulesTransaction.ApplyNCAARule = prefrenceToAssign.SeedPreferencesViewModel.ApplyNCAARule;
                            seedingRulesTransaction.SeedExhibitionAthletesLast = prefrenceToAssign.SeedPreferencesViewModel.SeedExhibitionAthletesLast;
                            seedingRulesTransaction.UseSpecialRandomSelectMethod = prefrenceToAssign.SeedPreferencesViewModel.UseSpecialRandomSelectMethod;
                            seedingRulesTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                            ctx.MeetSetupSeedingRules.Add(seedingRulesTransaction);
                            ctx.SaveChanges();

                            ctx.MeetSetupRendomizationRules.RemoveRange(ctx.MeetSetupRendomizationRules.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                            ctx.SaveChanges();

                            MeetSetupRendomizationRule rendomizationRuleTransactionTransaction = new MeetSetupRendomizationRule();
                            rendomizationRuleTransactionTransaction.CloseGap = prefrenceToAssign.SeedPreferencesViewModel.CloseGap;
                            rendomizationRuleTransactionTransaction.RoundFirstMultipleRound = prefrenceToAssign.SeedPreferencesViewModel.RoundFirstMultipleRound;
                            rendomizationRuleTransactionTransaction.RoundSecondThirdAndForth = prefrenceToAssign.SeedPreferencesViewModel.RoundSecondThirdAndForth;
                            rendomizationRuleTransactionTransaction.TimedFinalEvents = prefrenceToAssign.SeedPreferencesViewModel.TimedFinalEvents;
                            rendomizationRuleTransactionTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                            ctx.MeetSetupRendomizationRules.Add(rendomizationRuleTransactionTransaction);
                            ctx.SaveChanges();

                            MeetStandardAlleyPrefrence standardAlleyPrefrencesTransaction = new MeetStandardAlleyPrefrence();
                            int itemCount = 1;
                            foreach (var item in prefrenceToAssign.SeedPreferencesViewModel.StandardAlleyPrefrences.StandardAlleyPrefrenceList)
                            {
                                if (itemCount == 1)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowZeroColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 2)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowOneColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 3)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSecondColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 4)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowThirdColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowThridColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 5)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowForthColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 6)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowFifthColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 7)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSixthColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 8)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowSeventhColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 9)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowEighthColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 10)
                                {
                                    if (item.FirstLane != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardAlleyPrefrencesTransaction.RowNineColumnTenthValue = (short)item.TenthLine.Value;

                                }
                                itemCount++;
                            }
                            standardAlleyPrefrencesTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                            ctx.MeetStandardAlleyPrefrences.RemoveRange(ctx.MeetStandardAlleyPrefrences.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                            ctx.SaveChanges();

                            ctx.MeetStandardAlleyPrefrences.Add(standardAlleyPrefrencesTransaction);
                            ctx.SaveChanges();

                            itemCount = 1;
                            MeetSetupStandardLanePrefrence standardLanePrefrencesTransaction = new MeetSetupStandardLanePrefrence();
                            foreach (var item in prefrenceToAssign.SeedPreferencesViewModel.StandardLanePrefrences.StandardLanePrefrenceList)
                            {
                                if (itemCount == 1)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowZeroColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 2)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowOneColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowOneColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 3)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowSecondColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 4)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowThirdColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowThridColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 5)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowForthColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowForthColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 6)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowFifthColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 7)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowSixthColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 8)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowSeventhColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 9)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowEighthColumnTenthValue = (short)item.TenthLine.Value;
                                }
                                else if (itemCount == 10)
                                {
                                    if (item.FirstLane != null)
                                        standardLanePrefrencesTransaction.RowNineColumnFirstValue = (short)item.FirstLane.Value;
                                    if (item.SecondLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnSecondValue = (short)item.SecondLine.Value;
                                    if (item.ThirdLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnThirdValue = (short)item.ThirdLine.Value;
                                    if (item.ForthLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnForthValue = (short)item.ForthLine.Value;
                                    if (item.FifthLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnFifthValue = (short)item.FifthLine.Value;
                                    if (item.SixthLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnSixthValue = (short)item.SixthLine.Value;
                                    if (item.SeventhLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnSeventhValue = (short)item.SeventhLine.Value;
                                    if (item.EightsLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnEightthValue = (short)item.EightsLine.Value;
                                    if (item.NinthLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnNinthValue = (short)item.NinthLine.Value;
                                    if (item.TenthLine != null)
                                        standardLanePrefrencesTransaction.RowNineColumnTenthValue = (short)item.TenthLine.Value;

                                }
                                itemCount++;
                            }

                            ctx.MeetSetupStandardLanePrefrences.RemoveRange(ctx.MeetSetupStandardLanePrefrences.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                            ctx.SaveChanges();


                            standardLanePrefrencesTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                            ctx.MeetSetupStandardLanePrefrences.Add(standardLanePrefrencesTransaction);
                            ctx.SaveChanges();

                            ctx.MeetSetupWaterfallStarts.RemoveRange(ctx.MeetSetupWaterfallStarts.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                            ctx.SaveChanges();

                            MeetSetupWaterfallStart waterfallStartPreferenceTransaction = new MeetSetupWaterfallStart();
                            foreach (var item in prefrenceToAssign.SeedPreferencesViewModel.WaterFallPreferences.WaterfallStartList)
                            {
                                waterfallStartPreferenceTransaction.Rank = item.Rank;
                                waterfallStartPreferenceTransaction.Position = item.Position;
                                waterfallStartPreferenceTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                                ctx.MeetSetupWaterfallStarts.Add(waterfallStartPreferenceTransaction);
                                ctx.SaveChanges();

                            }
                            ctx.MeetSetupDualMeets.RemoveRange(ctx.MeetSetupDualMeets.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                            ctx.SaveChanges();

                            foreach (var item in prefrenceToAssign.SeedPreferencesViewModel.LaneDetailList)
                            {
                                MeetSetupDualMeet dualMeetTransaction = new MeetSetupDualMeet();
                                dualMeetTransaction.LaneNumber = item.LaneID;
                                dualMeetTransaction.MeetSetupID = prefrenceToAssign.MeetSetupID;
                                dualMeetTransaction.SchoolID = item.SchoolID;
                                ctx.MeetSetupDualMeets.Add(dualMeetTransaction);
                            }
                            ctx.SaveChanges();

                            ctx.MeetSetupDualMeetAssignments.RemoveRange(ctx.MeetSetupDualMeetAssignments.Where(p => p.MeetSetupID == prefrenceToAssign.MeetSetupID).ToList());
                            ctx.SaveChanges();
                            MeetSetupDualMeetAssignment dualMeetAssignment = new MeetSetupDualMeetAssignment();
                            dualMeetAssignment.AlternamtUserOfUnAssignedLane = prefrenceToAssign.SeedPreferencesViewModel.AlternamtUserOfUnAssignedLane;
                            dualMeetAssignment.StrictAssignmentAllHeats = prefrenceToAssign.SeedPreferencesViewModel.StrictAssignmentAllHeats;
                            dualMeetAssignment.StrictAssignmentFastestHeatOnly = prefrenceToAssign.SeedPreferencesViewModel.StrictAssignmentFastestHeatOnly;
                            dualMeetAssignment.UseLaneAssignmentsForInLaneRacesOnly = prefrenceToAssign.SeedPreferencesViewModel.UseLaneAssignmentsForInLaneRacesOnly;
                            dualMeetAssignment.UserLaneOrPositionAssignmentsAbove = prefrenceToAssign.SeedPreferencesViewModel.UserLaneOrPositionAssignmentsAbove;
                            dualMeetAssignment.MeetSetupID = prefrenceToAssign.MeetSetupID;
                            ctx.MeetSetupDualMeetAssignments.Add(dualMeetAssignment);
                            ctx.SaveChanges();

                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<AssignPreferencesResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new AssignPreferencesResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }


                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new AssignPreferencesResult();
                Result.Result.AssignPreferencesID = prefrenceToAssign.MeetSetupID;
                Result.Result.AuthToken = null;
                return Result;


            }
        }

        public async Task<AuthorizedResponse<ScoringSetupResult>> AssignScoringMeetTemplate(ServiceRequest_ScoringSetup scoringSetup)
        {
            int InsertedId = 0;
            AuthorizedResponse<ScoringSetupResult> Result = new AuthorizedResponse<ScoringSetupResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (scoringSetup != null)
                        {
                            ctx.MeetScoringSetups.RemoveRange(ctx.MeetScoringSetups.Where(p => p.MeetSetupID == scoringSetup.TemplateOrMeetSetupID).ToList());
                            foreach (var item in scoringSetup.ScoringSetupTemplateList)
                            {
                                MeetScoringSetup meetScoringSetup = new MeetScoringSetup();
                                meetScoringSetup.CombEvtPoints = item.CombEvtPoints;
                                meetScoringSetup.IndividualPints = item.IndividualPints;
                                meetScoringSetup.MeetSetupID = scoringSetup.TemplateOrMeetSetupID;
                                meetScoringSetup.Place = item.Place;
                                meetScoringSetup.RelayPoints = item.RelayPoints;
                                ctx.MeetScoringSetups.Add(meetScoringSetup);
                            }
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringSetupResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringSetupResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                    Result.Message = "";
                    Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                    Result.Result = new ScoringSetupResult();
                    Result.Result.ScoringSetupID = scoringSetup.TemplateOrMeetSetupID;
                    Result.Result.AuthToken = null;
                    return Result;
                }
            }
        }

        public async Task<AuthorizedResponse<ScoringPreferencesResult>> AssignScoringPreferencesMeet(ServiceRequest_ScoringPreferences scoringPreference)
        {
            AuthorizedResponse<ScoringPreferencesResult> Result = new AuthorizedResponse<ScoringPreferencesResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (scoringPreference.ScoringPreferencesViewModel != null)
                        {
                            ctx.MeetSetupScroringPreferencesScoringAwards.RemoveRange(ctx.MeetSetupScroringPreferencesScoringAwards.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            MeetSetupScroringPreferencesScoringAward meetSetupScoringAward = new MeetSetupScroringPreferencesScoringAward();
                            meetSetupScoringAward.AllowForeignAthletesPointScoreToCountTowordTeamScore = scoringPreference.ScoringPreferencesViewModel.AllowForeignAthletesPointScoreToCountTowordTeamScore;
                            meetSetupScoringAward.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            meetSetupScoringAward.DiffrentPointSystemForEach = scoringPreference.ScoringPreferencesViewModel.DiffrentPointSystemForEach;
                            meetSetupScoringAward.DiffrentPointSystemForMaleAndFemale = scoringPreference.ScoringPreferencesViewModel.DiffrentPointSystemForMaleAndFemale;
                            meetSetupScoringAward.ExceedOrEqualQualifingMarks = scoringPreference.ScoringPreferencesViewModel.ExceedOrEqualQualifingMarks;
                            meetSetupScoringAward.ExhibitionMarksReceiveFinalsRanks = scoringPreference.ScoringPreferencesViewModel.ExhibitionMarksReceiveFinalsRanks;
                            meetSetupScoringAward.IncludeRelayPointsWithIndivHighPointScore = scoringPreference.ScoringPreferencesViewModel.IncludeRelayPointsWithIndivHighPointScore;
                            meetSetupScoringAward.MaximumPerTeamFinalFromPreLimsIndividual = scoringPreference.ScoringPreferencesViewModel.MaximumPerTeamFinalFromPreLimsIndividual;
                            meetSetupScoringAward.MaximumPerTeamFinalFromPreLimsRelay = scoringPreference.ScoringPreferencesViewModel.MaximumPerTeamFinalFromPreLimsRelay;
                            meetSetupScoringAward.MaximumScorePerTeamPerEventIndividual = scoringPreference.ScoringPreferencesViewModel.MaximumScorePerTeamPerEventIndividual;
                            meetSetupScoringAward.MaximumScorePerTeamPerEventRelay = scoringPreference.ScoringPreferencesViewModel.MaximumScorePerTeamPerEventRelay;
                            meetSetupScoringAward.ScoreEqualRelayOnly = scoringPreference.ScoringPreferencesViewModel.ScoreEqualRelayOnly;
                            meetSetupScoringAward.ScoreFastestHeadOnlyRegardLessOverAllPlace = scoringPreference.ScoringPreferencesViewModel.ScoreFastestHeadOnlyRegardLessOverAllPlace;
                            meetSetupScoringAward.TopHowManyForAwardLabelIndividual = scoringPreference.ScoringPreferencesViewModel.TopHowManyForAwardLabelIndividual;
                            meetSetupScoringAward.TopHowManyForAwardLabelRelay = scoringPreference.ScoringPreferencesViewModel.TopHowManyForAwardLabelRelay;
                            meetSetupScoringAward.TreateForeignAthletesAsExhibition = scoringPreference.ScoringPreferencesViewModel.TreateForeignAthletesAsExhibition;
                            ctx.MeetSetupScroringPreferencesScoringAwards.Add(meetSetupScoringAward);

                            ctx.MeetSetupScroringPreferencesAgeGradings.RemoveRange(ctx.MeetSetupScroringPreferencesAgeGradings.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            MeetSetupScroringPreferencesAgeGrading meetSetupScrollingPreferencesGrading = new MeetSetupScroringPreferencesAgeGrading();
                            meetSetupScrollingPreferencesGrading.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            meetSetupScrollingPreferencesGrading.UseFiveYearAgingTableMulti = scoringPreference.ScoringPreferencesViewModel.UseFiveYearAgingTableMulti;
                            meetSetupScrollingPreferencesGrading.UseFiveYearAgingTableStandared = scoringPreference.ScoringPreferencesViewModel.UseFiveYearAgingTableStandared;
                            meetSetupScrollingPreferencesGrading.UseOneYearAgingTableMulti = scoringPreference.ScoringPreferencesViewModel.UseOneYearAgingTableMulti;
                            meetSetupScrollingPreferencesGrading.UseOneYearAgingTableStandared = scoringPreference.ScoringPreferencesViewModel.UseOneYearAgingTableStandared;
                            ctx.MeetSetupScroringPreferencesAgeGradings.Add(meetSetupScrollingPreferencesGrading);

                            ctx.MeetSetupScroringPreferencesCCRRs.RemoveRange(ctx.MeetSetupScroringPreferencesCCRRs.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            MeetSetupScroringPreferencesCCRR ccRR = new MeetSetupScroringPreferencesCCRR();
                            ccRR.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            ccRR.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6 = scoringPreference.ScoringPreferencesViewModel.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6;
                            ccRR.DynamicMultipleTeamScoring = scoringPreference.ScoringPreferencesViewModel.DynamicMultipleTeamScoring;
                            ccRR.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult = scoringPreference.ScoringPreferencesViewModel.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult;
                            ccRR.MaximumDisplacerPerTeam = scoringPreference.ScoringPreferencesViewModel.MaximumDisplacerPerTeam;
                            ccRR.MaximumRunnerToQualifyAsATeam = scoringPreference.ScoringPreferencesViewModel.MaximumRunnerToQualifyAsATeam;
                            ccRR.NoneTeamRunnerDisplacedOtherRunner = scoringPreference.ScoringPreferencesViewModel.NoneTeamRunnerDisplacedOtherRunner;
                            ccRR.NumberOfRunnersScoreFromEachTeam = scoringPreference.ScoringPreferencesViewModel.NumberOfRunnersScoreFromEachTeam;
                            ccRR.PanaltyPointAddedForShortTeamLast1Last2Etc = scoringPreference.ScoringPreferencesViewModel.PanaltyPointAddedForShortTeamLast1Last2Etc;
                            ccRR.PanaltyPointsAddedForShortTeams = scoringPreference.ScoringPreferencesViewModel.PanaltyPointsAddedForShortTeams;

                            ctx.MeetSetupScroringPreferencesCCRRs.Add(ccRR);

                            ctx.MeetSetupScroringPreferencesEntryLimits.RemoveRange(ctx.MeetSetupScroringPreferencesEntryLimits.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            MeetSetupScroringPreferencesEntryLimit entryLimit = new MeetSetupScroringPreferencesEntryLimit();
                            entryLimit.MaximumEntriesPerAthleteIncludingRelays = scoringPreference.ScoringPreferencesViewModel.MaximumEntriesPerAthleteIncludingRelays;
                            entryLimit.MaximumFieldEventEntriesPerAthletes = scoringPreference.ScoringPreferencesViewModel.MaximumFieldEventEntriesPerAthletes;
                            entryLimit.MaximumTrackEventEntriesPerAthleteIncludingRelay = scoringPreference.ScoringPreferencesViewModel.MaximumTrackEventEntriesPerAthleteIncludingRelay;
                            entryLimit.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            entryLimit.WarnIfEntryLimitsExceeds = scoringPreference.ScoringPreferencesViewModel.WarnIfEntryLimitsExceeds;
                            entryLimit.MaximumFieldEventEntriesPerAthletes = scoringPreference.ScoringPreferencesViewModel.MaximumFieldEventEntriesPerAthletes;
                            ctx.MeetSetupScroringPreferencesEntryLimits.Add(entryLimit);

                            ctx.MeetSetupScroringPreferencesEntriesResults.RemoveRange(ctx.MeetSetupScroringPreferencesEntriesResults.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            MeetSetupScroringPreferencesEntriesResult scoringEntryresult = new MeetSetupScroringPreferencesEntriesResult();
                            scoringEntryresult.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            scoringEntryresult.ConvertHandEntryTimesToFAT = scoringPreference.ScoringPreferencesViewModel.ConvertHandEntryTimesToFAT;
                            scoringEntryresult.EntryNotesWithEachEntry = scoringPreference.ScoringPreferencesViewModel.EntryNotesWithEachEntry;
                            scoringEntryresult.RankResultByAgeGradedMarks = scoringPreference.ScoringPreferencesViewModel.RankResultByAgeGradedMarks;
                            scoringEntryresult.RecordWindReading = scoringPreference.ScoringPreferencesViewModel.RecordWindReading;
                            scoringEntryresult.RoundDownTheNearestEventCentimerer = scoringPreference.ScoringPreferencesViewModel.RoundDownTheNearestEventCentimerer;
                            scoringEntryresult.RoundUpResultToTenthofReport = scoringPreference.ScoringPreferencesViewModel.RoundUpResultToTenthofReport;
                            scoringEntryresult.ShowFinishedPlaceWithEachEntry = scoringPreference.ScoringPreferencesViewModel.ShowFinishedPlaceWithEachEntry;
                            scoringEntryresult.UseEntryDeclarationMethod = scoringPreference.ScoringPreferencesViewModel.UseEntryDeclarationMethod;
                            scoringEntryresult.WarnIfTimesMarksAreOutofRange = scoringPreference.ScoringPreferencesViewModel.WarnIfTimesMarksAreOutofRange;
                            ctx.MeetSetupScroringPreferencesEntriesResults.Add(scoringEntryresult);

                            ctx.MeetScroringPreferencesFieldRelays.RemoveRange(ctx.MeetScroringPreferencesFieldRelays.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            MeetScroringPreferencesFieldRelay fieldRelay = new MeetScroringPreferencesFieldRelay();
                            fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam = scoringPreference.ScoringPreferencesViewModel.FieldRelayCanBeScoreWithUnderSizeTeam;
                            fieldRelay.MarksWillBeAddedForReamRanking = scoringPreference.ScoringPreferencesViewModel.MarksWillBeAddedForReamRanking;
                            fieldRelay.TimesWillBeAddedForTeamRanking = scoringPreference.ScoringPreferencesViewModel.TimesWillBeAddedForTeamRanking;
                            fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam = scoringPreference.ScoringPreferencesViewModel.FieldRelayCanBeScoreWithUnderSizeTeam;
                            ctx.MeetScroringPreferencesFieldRelays.Add(fieldRelay);

                            ctx.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringPreferencesResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringPreferencesResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new ScoringPreferencesResult();
                Result.Result.ScoringPreferencesID = scoringPreference.TemplateOrMeetSetupID;
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<AthleteRelayPrefrencesResult>> CreateNewAthletePreference(ServiceRequest_AthleteRelayPrefrences CreateMeetRequest)
        {
            try
            {
                int InsertedId = 0;
                string athletPrefGroupID = Guid.NewGuid().ToString();
                string athletRelayGroupID = Guid.NewGuid().ToString();
                string athletCompNameGroupID = Guid.NewGuid().ToString();
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    foreach (var item in CreateMeetRequest.AthletePreferencesList)
                    {
                        AthleticPreferencesTransaction athletPrefTransaction = new AthleticPreferencesTransaction();
                        athletPrefTransaction.AthleticPreferencesGroupID = athletPrefGroupID;
                        athletPrefTransaction.AthleticPreferencesID = item;
                        ctx.AthleticPreferencesTransactions.Add(athletPrefTransaction);
                    }

                    foreach (var item in CreateMeetRequest.RelayPreferencesList)
                    {
                        RelayPreferencesTransaction athletRelayTransaction = new RelayPreferencesTransaction();
                        athletRelayTransaction.RelayPreferencesGroupID = athletRelayGroupID;
                        athletRelayTransaction.RelayPreferencesID = item;
                        ctx.RelayPreferencesTransactions.Add(athletRelayTransaction);
                    }

                    foreach (var item in CreateMeetRequest.CompetitorNumberList)
                    {
                        CompetitorNumbersTransaction competNumTransaction = new CompetitorNumbersTransaction();
                        competNumTransaction.CompetitorNumbersGroupID = athletCompNameGroupID;
                        competNumTransaction.CompetitorNumbersID = item;
                        ctx.CompetitorNumbersTransactions.Add(competNumTransaction);
                    }

                    AthleteRelayPreferencesTransaction relayPrefTrnsaction = new AthleteRelayPreferencesTransaction();
                    relayPrefTrnsaction.CompetitorNumberTransactionGroupID = athletCompNameGroupID;
                    relayPrefTrnsaction.PreferencesTransactionGroupID = athletPrefGroupID;
                    relayPrefTrnsaction.RelayPreferencesTransactionGroupID = athletRelayGroupID;

                    ctx.AthleteRelayPreferencesTransactions.Add(relayPrefTrnsaction);
                    ctx.SaveChanges();
                    InsertedId = relayPrefTrnsaction.AthleteRelayPreferencesID;

                }

                AuthorizedResponse<AthleteRelayPrefrencesResult> Result = new AuthorizedResponse<AthleteRelayPrefrencesResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new AthleteRelayPrefrencesResult();
                Result.Result.AthleteRelayPrefrencesID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<AthleteRelayPrefrencesResult> Result = new AuthorizedResponse<AthleteRelayPrefrencesResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new AthleteRelayPrefrencesResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<DualMeetResult>> CreateNewDualMeet(ServiceRequest_DualMeet ServiceRequest_DualMeet)
        {
            try
            {
                string InsertedId = string.Empty;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    string groupID = Guid.NewGuid().ToString();
                    foreach (var item in ServiceRequest_DualMeet.LaneLaneDetailModelList)
                    {
                        DualMeetTransaction dualMeetTransaction = new DualMeetTransaction();
                        dualMeetTransaction.LaneNumber = item.LaneID;
                        dualMeetTransaction.DualMeetGroupID = groupID;
                        dualMeetTransaction.SchoolID = item.SchoolID;
                        ctx.DualMeetTransactions.Add(dualMeetTransaction);
                    }
                    ctx.SaveChanges();
                    InsertedId = groupID;
                }
                AuthorizedResponse<DualMeetResult> Result = new AuthorizedResponse<DualMeetResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new DualMeetResult();
                Result.Result.DualMeetGroupID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<DualMeetResult> Result = new AuthorizedResponse<DualMeetResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new DualMeetResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<DualMeetAssignmentResult>> CreateNewDualMeetAssignment(ServiceRequest_DualMeetAssignment ServiceRequest_DualMeetAssignment)
        {
            try
            {
                int InsertedId = 0;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    DualMeetAssignmentTransaction dualMeetTransaction = new DualMeetAssignmentTransaction();
                    dualMeetTransaction.AlternamtUserOfUnAssignedLane = ServiceRequest_DualMeetAssignment.AlternamtUserOfUnAssignedLane;
                    dualMeetTransaction.DualMeetGroupID = ServiceRequest_DualMeetAssignment.DualMeetGroupID;
                    dualMeetTransaction.StrictAssignmentAllHeats = ServiceRequest_DualMeetAssignment.StrictAssignmentAllHeats;
                    dualMeetTransaction.StrictAssignmentFastestHeatOnly = ServiceRequest_DualMeetAssignment.StrictAssignmentFastestHeatOnly;
                    dualMeetTransaction.UseLaneAssignmentsForInLaneRacesOnly = ServiceRequest_DualMeetAssignment.UseLaneAssignmentsForInLaneRacesOnly;
                    dualMeetTransaction.UserLaneOrPositionAssignmentsAbove = ServiceRequest_DualMeetAssignment.UserLaneOrPositionAssignmentsAbove;
                    ctx.DualMeetAssignmentTransactions.Add(dualMeetTransaction);
                    ctx.SaveChanges();
                    InsertedId = dualMeetTransaction.DualMeetAssignmentTransactionID;
                }
                AuthorizedResponse<DualMeetAssignmentResult> Result = new AuthorizedResponse<DualMeetAssignmentResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new DualMeetAssignmentResult();
                Result.Result.DualMeetAssignmentTranID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<DualMeetAssignmentResult> Result = new AuthorizedResponse<DualMeetAssignmentResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new DualMeetAssignmentResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<MeetSetupResult>> CreateNewMeet(APIServiceRequest_MeetSetup CreateMeetRequest)
        {
            try
            {
                int InsertedId = 0;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    Entities.Models.MeetSetup meetSetup = new Entities.Models.MeetSetup
                    {
                        MeetSetupName = CreateMeetRequest.MeetSetupName,
                        MeetSetupName2 = CreateMeetRequest.MeetSetupName2,
                        MeetSetupAgeUpDate = CreateMeetRequest.MeetSetupAgeUpDate,
                        MeetSetupStartDate = CreateMeetRequest.MeetSetupStartDate,
                        MeetSetupEndDate = CreateMeetRequest.MeetSetupEndDate,
                        MeetTypeID = CreateMeetRequest.MeetTypeID,
                        MeetArenaID = CreateMeetRequest.MeetArenaID,
                        MeetClassID = CreateMeetRequest.MeetClassID,
                        MeetKindID = CreateMeetRequest.MeetKindID,
                        BaseCountyID = CreateMeetRequest.MeetCountyID,
                        MeetStyleID = CreateMeetRequest.MeetStyleID,
                        UseDivisionBirthdateRange = CreateMeetRequest.UseDivisionBirthDateRange,
                        MeetTypeDivisionID = CreateMeetRequest.MeetTypeOptionID,
                        LinkTermToDivision = CreateMeetRequest.LinkTermToDivision
                    };

                    ctx.MeetSetups.Add(meetSetup);
                    ctx.SaveChanges();
                    InsertedId = meetSetup.MeetSetupID;

                }

                AuthorizedResponse<MeetSetupResult> Result = new AuthorizedResponse<MeetSetupResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new MeetSetupResult();
                Result.Result.MeetSetupID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<MeetSetupResult> Result = new AuthorizedResponse<MeetSetupResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new MeetSetupResult();
                Result.Result.AuthToken = null;
                return Result;
            }

        }

        public async Task<AuthorizedResponse<RendomizationRuleResult>> CreateNewRendomizationRule(ServiceRequest_RendomizationRule ServiceRequest_RendomizationRule)
        {
            try
            {
                string groupID = Guid.NewGuid().ToString();
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    RendomizationRuleTransaction rendomizationRuleTransactionTransaction = new RendomizationRuleTransaction();
                    rendomizationRuleTransactionTransaction.RendomizationRuleGroupID = groupID;
                    rendomizationRuleTransactionTransaction.RoundFirstMultipleRound = ServiceRequest_RendomizationRule.RoundFirstMultipleRound;
                    rendomizationRuleTransactionTransaction.RoundSecondThirdAndForth = ServiceRequest_RendomizationRule.RoundSecondThirdAndForth;
                    rendomizationRuleTransactionTransaction.TimedFinalEvents = ServiceRequest_RendomizationRule.TimedFinalEvents;
                    rendomizationRuleTransactionTransaction.CloseGap = ServiceRequest_RendomizationRule.CloseGap;
                    ctx.RendomizationRuleTransactions.Add(rendomizationRuleTransactionTransaction);
                    ctx.SaveChanges();
                    groupID = rendomizationRuleTransactionTransaction.RendomizationRuleGroupID;
                }
                AuthorizedResponse<RendomizationRuleResult> Result = new AuthorizedResponse<RendomizationRuleResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new RendomizationRuleResult();
                Result.Result.RendomizationRuleGroupID = groupID;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<RendomizationRuleResult> Result = new AuthorizedResponse<RendomizationRuleResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new RendomizationRuleResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<SeedingPreferencesResult>> CreateNewSeedingPreferences(ServiceRequest_SeedingPreferences ServiceRequest_SeedingPreferences)
        {
            try
            {
                int InsertedId = 0;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    SeedingPreferencesTransaction seedingPreferencesTransaction = new SeedingPreferencesTransaction
                    {
                        DualMeetGroupID = ServiceRequest_SeedingPreferences.DualMeetGroupID,
                        SeedingRulesGroupID = ServiceRequest_SeedingPreferences.SeedingRulesGroupID,
                        StandardAlleyPrefrencesGroupID = ServiceRequest_SeedingPreferences.StandardAlleyPrefrencesGroupID,
                        StandardLanePrefrencesGroupID = ServiceRequest_SeedingPreferences.StandardLanePrefrencesGroupID,
                        WaterfallStartPreferencesGroupID = ServiceRequest_SeedingPreferences.WaterfallStartPreferencesGroupID,
                        RendomizationRuleGroupID = ServiceRequest_SeedingPreferences.RendomizationRuleGroupID
                    };

                    ctx.SeedingPreferencesTransactions.Add(seedingPreferencesTransaction);
                    ctx.SaveChanges();
                    InsertedId = seedingPreferencesTransaction.SeedingPrefrencesID;

                }

                AuthorizedResponse<SeedingPreferencesResult> Result = new AuthorizedResponse<SeedingPreferencesResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new SeedingPreferencesResult();
                Result.Result.SeedingPreferencesTranID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<SeedingPreferencesResult> Result = new AuthorizedResponse<SeedingPreferencesResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new SeedingPreferencesResult();
                Result.Result.AuthToken = null;
                return Result;
            }

        }

        public async Task<AuthorizedResponse<SeedingRulesResult>> CreateNewSeedingRules(ServiceRequest_SeedingRules ServiceRequest_SeedingRules)
        {
            try
            {
                string InsertedId = string.Empty;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    SeedingRulesTransaction seedingRulesTransaction = new SeedingRulesTransaction();
                    seedingRulesTransaction.SeedingRulesGroupID = Guid.NewGuid().ToString();
                    seedingRulesTransaction.AllowExhibitionAthletesInFinal = ServiceRequest_SeedingRules.AllowExhibitionAthletesInFinal;
                    seedingRulesTransaction.AllowForeignAthletesInFinal = ServiceRequest_SeedingRules.AllowForeignAthletesInFinal;
                    seedingRulesTransaction.ApplyNCAARule = ServiceRequest_SeedingRules.ApplyNCAARule;
                    seedingRulesTransaction.SeedExhibitionAthletesLast = ServiceRequest_SeedingRules.SeedExhibitionAthletesLast;
                    seedingRulesTransaction.UseSpecialRandomSelectMethod = ServiceRequest_SeedingRules.UseSpecialRandomSelectMethod;
                    ctx.SeedingRulesTransactions.Add(seedingRulesTransaction);
                    ctx.SaveChanges();

                    InsertedId = seedingRulesTransaction.SeedingRulesGroupID;
                }
                AuthorizedResponse<SeedingRulesResult> Result = new AuthorizedResponse<SeedingRulesResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new SeedingRulesResult();
                Result.Result.SeedingRulesGroupID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<SeedingRulesResult> Result = new AuthorizedResponse<SeedingRulesResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new SeedingRulesResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<StandardAlleyPrefrenceResult>> CreateNewStandardAlleyPrefrences(ServiceRequest_StandardAlleyPrefrences ServiceRequestStandardAlleyPrefrence)
        {
            try
            {
                string InsertedId = string.Empty;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    int itemCount = 1;
                    StandardAlleyPrefrencesTransaction standardAlleyPrefrencesTransaction = new StandardAlleyPrefrencesTransaction();
                    standardAlleyPrefrencesTransaction.StandardAlleyPrefrencesGroupID = Guid.NewGuid().ToString();
                    if (ServiceRequestStandardAlleyPrefrence.StandardAlleyPrefrencesList != null)
                    {
                        foreach (var item in ServiceRequestStandardAlleyPrefrence.StandardAlleyPrefrencesList)
                        {
                            if (itemCount == 1)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 2)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 3)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 4)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThridColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 5)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 6)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 7)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 8)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 9)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 10)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnTenthValue = (short)item.TenthLine.Value;

                            }
                            itemCount++;
                        }

                    }
                    ctx.StandardAlleyPrefrencesTransactions.Add(standardAlleyPrefrencesTransaction);
                    ctx.SaveChanges();
                    InsertedId = standardAlleyPrefrencesTransaction.StandardAlleyPrefrencesGroupID;

                }

                AuthorizedResponse<StandardAlleyPrefrenceResult> Result = new AuthorizedResponse<StandardAlleyPrefrenceResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new StandardAlleyPrefrenceResult();
                Result.Result.StandardAlleyPrefrenceGroupID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<StandardAlleyPrefrenceResult> Result = new AuthorizedResponse<StandardAlleyPrefrenceResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new StandardAlleyPrefrenceResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<StandardLanePrefrencesResult>> CreateNewStandardLanePrefrences(ServiceRequest_StandardLanePrefrences ServiceRequestStandardLanePrefrences)
        {
            try
            {
                string InsertedId = string.Empty;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    int itemCount = 1;
                    StandardLanePrefrencesTransaction standardAlleyPrefrencesTransaction = new StandardLanePrefrencesTransaction();
                    standardAlleyPrefrencesTransaction.StandardLanePrefrencesGroupID = Guid.NewGuid().ToString();
                    if (ServiceRequestStandardLanePrefrences.StandardLanePrefrencesList != null)
                    {
                        foreach (var item in ServiceRequestStandardLanePrefrences.StandardLanePrefrencesList)
                        {
                            if (itemCount == 1)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowZeroColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 2)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowOneColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 3)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSecondColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 4)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThirdColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowThridColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 5)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowForthColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 6)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowFifthColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 7)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSixthColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 8)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowSeventhColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 9)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowEighthColumnTenthValue = (short)item.TenthLine.Value;
                            }
                            else if (itemCount == 10)
                            {
                                if (item.FirstLane != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnFirstValue = (short)item.FirstLane.Value;
                                if (item.SecondLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnSecondValue = (short)item.SecondLine.Value;
                                if (item.ThirdLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnThirdValue = (short)item.ThirdLine.Value;
                                if (item.ForthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnForthValue = (short)item.ForthLine.Value;
                                if (item.FifthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnFifthValue = (short)item.FifthLine.Value;
                                if (item.SixthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnSixthValue = (short)item.SixthLine.Value;
                                if (item.SeventhLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnSeventhValue = (short)item.SeventhLine.Value;
                                if (item.EightsLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnEightthValue = (short)item.EightsLine.Value;
                                if (item.NinthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnNinthValue = (short)item.NinthLine.Value;
                                if (item.TenthLine != null)
                                    standardAlleyPrefrencesTransaction.RowNineColumnTenthValue = (short)item.TenthLine.Value;

                            }
                            itemCount++;
                        }
                    }

                    ctx.StandardLanePrefrencesTransactions.Add(standardAlleyPrefrencesTransaction);
                    ctx.SaveChanges();
                    InsertedId = standardAlleyPrefrencesTransaction.StandardLanePrefrencesGroupID;

                }

                AuthorizedResponse<StandardLanePrefrencesResult> Result = new AuthorizedResponse<StandardLanePrefrencesResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new StandardLanePrefrencesResult();
                Result.Result.StandardLanePrefrencesGroupID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<StandardLanePrefrencesResult> Result = new AuthorizedResponse<StandardLanePrefrencesResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new StandardLanePrefrencesResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<WaterfallStartPreferenceResult>> CreateNewWaterfallStartPreference(ServiceRequest_WaterfallStartPreference ServiceRequestWaterfallStartPreference)
        {
            try
            {
                string InsertedId = string.Empty;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    WaterfallStartPreferenceTransaction waterfallStartPreferenceTransaction = new WaterfallStartPreferenceTransaction();
                    waterfallStartPreferenceTransaction.WaterfallStartPreferencesGroupID = Guid.NewGuid().ToString();
                    if (ServiceRequestWaterfallStartPreference.WaterfallStartPreferenceList != null)
                    {
                        foreach (var item in ServiceRequestWaterfallStartPreference.WaterfallStartPreferenceList)
                        {
                            waterfallStartPreferenceTransaction.Rank = item.Rank;
                            waterfallStartPreferenceTransaction.Position = item.Position;
                            ctx.WaterfallStartPreferenceTransactions.Add(waterfallStartPreferenceTransaction);
                            ctx.SaveChanges();

                        }
                        InsertedId = waterfallStartPreferenceTransaction.WaterfallStartPreferencesGroupID;
                    }
                }
                AuthorizedResponse<WaterfallStartPreferenceResult> Result = new AuthorizedResponse<WaterfallStartPreferenceResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new WaterfallStartPreferenceResult();
                Result.Result.WaterfallStartPreferenceGroupID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<WaterfallStartPreferenceResult> Result = new AuthorizedResponse<WaterfallStartPreferenceResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new WaterfallStartPreferenceResult();
                Result.Result.AuthToken = null;
                return Result;
            }


        }

        public async Task<AuthorizedResponse<TemplateNameResult>> CreateRelayPreferenceTemplate(ServiceRequest_TemplateName ReplayPreferenceTemplateInfo)
        {
            try
            {
                int InsertedId = 0;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    AthleticPreferenceTemplate athleticPreferenceTemplate = new AthleticPreferenceTemplate
                    {
                        AthleticRelayPreferenceID = ReplayPreferenceTemplateInfo.Preferenceid,
                        AthleticPreferenceTemplateName = ReplayPreferenceTemplateInfo.TemplateName
                    };

                    ctx.AthleticPreferenceTemplates.Add(athleticPreferenceTemplate);
                    ctx.SaveChanges();
                    InsertedId = athleticPreferenceTemplate.AthleticPreferenceTemplateID;

                }

                AuthorizedResponse<TemplateNameResult> Result = new AuthorizedResponse<TemplateNameResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new TemplateNameResult();
                Result.Result.TemplateNameID = InsertedId.ToString();
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<TemplateNameResult> Result = new AuthorizedResponse<TemplateNameResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new TemplateNameResult();
                Result.Result.AuthToken = null;
                return Result;
            }

        }

        public async Task<AuthorizedResponse<ScoringSetupResult>> CreateScoringMeetTemplate(ServiceRequest_ScoringSetup scoringSetup)
        {
            try
            {
                int InsertedId = 0;
                using (Entities.Models.Entities ctx = new Entities.Models.Entities())
                {
                    if (scoringSetup.IsDefault == true)
                    {
                        ctx.ScoringSetupTemplates.ToList().ForEach(p => p.IsDefault = false);
                    }
                    ScoringSetupTemplate setupTemplate = ctx.ScoringSetupTemplates.Where(p => p.ScoringSetupTemplateID == scoringSetup.TemplateOrMeetSetupID).FirstOrDefault();
                    if (setupTemplate == null)
                    {
                        setupTemplate = new ScoringSetupTemplate();
                    }
                    else
                    {
                        ctx.ScoringSetupTemplateDetails.RemoveRange(ctx.ScoringSetupTemplateDetails.Where(p => p.ScoringSetupTemplateID == setupTemplate.ScoringSetupTemplateID));
                    }
                    setupTemplate.TemplateName = scoringSetup.TemplateName;
                    setupTemplate.IsDefault = scoringSetup.IsDefault;
                    setupTemplate.ScoringSetupTemplateDetails = new List<ScoringSetupTemplateDetail>();
                    foreach (var template in scoringSetup.ScoringSetupTemplateList)
                    {
                        ScoringSetupTemplateDetail templateDetail = new ScoringSetupTemplateDetail();
                        templateDetail.IndividualPints = template.IndividualPints;
                        templateDetail.Place = template.Place;
                        templateDetail.CombEvtPoints = template.CombEvtPoints;
                        templateDetail.RelayPoints = template.RelayPoints;
                        setupTemplate.ScoringSetupTemplateDetails.Add(templateDetail);
                    }
                    if (setupTemplate.ScoringSetupTemplateID <= 0)
                    {
                        ctx.ScoringSetupTemplates.Add(setupTemplate);
                    }
                    ctx.SaveChanges();
                    InsertedId = setupTemplate.ScoringSetupTemplateID;
                    AuthorizedResponse<ScoringSetupResult> Result = new AuthorizedResponse<ScoringSetupResult>();
                    Result.Message = "";
                    Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                    Result.Result = new ScoringSetupResult();
                    Result.Result.ScoringSetupID = InsertedId;
                    Result.Result.AuthToken = null;
                    return Result;
                }
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<ScoringSetupResult> Result = new AuthorizedResponse<ScoringSetupResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new ScoringSetupResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<ScoringPreferencesResult>> CreateScoringPreferencesTemplate(ServiceRequest_ScoringPreferences scoringPreference)
        {
            AuthorizedResponse<ScoringPreferencesResult> Result = new AuthorizedResponse<ScoringPreferencesResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (scoringPreference.ScoringPreferencesViewModel != null)
                        {
                            ctx.ScroringPreferencesTemplates.RemoveRange(ctx.ScroringPreferencesTemplates.Where(p => p.ScroringPreferencesTemplateID == scoringPreference.TemplateOrMeetSetupID).ToList());

                            ScroringPreferencesTemplate template = new ScroringPreferencesTemplate();
                            template.ScroringPreferencesTemplateName = scoringPreference.ScoringPreferencesViewModel.ScroringPreferencesTemplateName;

                            ctx.ScroringPreferencesTemplateScoringAwards.RemoveRange(ctx.ScroringPreferencesTemplateScoringAwards.Where(p => p.ScroringPreferencesTemplateScoringAwardID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            ScroringPreferencesTemplateScoringAward ScoringAward = new ScroringPreferencesTemplateScoringAward();
                            ScoringAward.AllowForeignAthletesPointScoreToCountTowordTeamScore = scoringPreference.ScoringPreferencesViewModel.AllowForeignAthletesPointScoreToCountTowordTeamScore;
                            ScoringAward.DiffrentPointSystemForEach = scoringPreference.ScoringPreferencesViewModel.DiffrentPointSystemForEach;
                            ScoringAward.DiffrentPointSystemForMaleAndFemale = scoringPreference.ScoringPreferencesViewModel.DiffrentPointSystemForMaleAndFemale;
                            ScoringAward.ExceedOrEqualQualifingMarks = scoringPreference.ScoringPreferencesViewModel.ExceedOrEqualQualifingMarks;
                            ScoringAward.ExhibitionMarksReceiveFinalsRanks = scoringPreference.ScoringPreferencesViewModel.ExhibitionMarksReceiveFinalsRanks;
                            ScoringAward.IncludeRelayPointsWithIndivHighPointScore = scoringPreference.ScoringPreferencesViewModel.IncludeRelayPointsWithIndivHighPointScore;
                            ScoringAward.MaximumPerTeamFinalFromPreLimsIndividual = scoringPreference.ScoringPreferencesViewModel.MaximumPerTeamFinalFromPreLimsIndividual;
                            ScoringAward.MaximumPerTeamFinalFromPreLimsRelay = scoringPreference.ScoringPreferencesViewModel.MaximumPerTeamFinalFromPreLimsRelay;
                            ScoringAward.MaximumScorePerTeamPerEventIndividual = scoringPreference.ScoringPreferencesViewModel.MaximumScorePerTeamPerEventIndividual;
                            ScoringAward.MaximumScorePerTeamPerEventRelay = scoringPreference.ScoringPreferencesViewModel.MaximumScorePerTeamPerEventRelay;
                            ScoringAward.ScoreEqualRelayOnly = scoringPreference.ScoringPreferencesViewModel.ScoreEqualRelayOnly;
                            ScoringAward.ScoreFastestHeadOnlyRegardLessOverAllPlace = scoringPreference.ScoringPreferencesViewModel.ScoreFastestHeadOnlyRegardLessOverAllPlace;
                            ScoringAward.TopHowManyForAwardLabelIndividual = scoringPreference.ScoringPreferencesViewModel.TopHowManyForAwardLabelIndividual;
                            ScoringAward.TopHowManyForAwardLabelRelay = scoringPreference.ScoringPreferencesViewModel.TopHowManyForAwardLabelRelay;
                            ScoringAward.TreateForeignAthletesAsExhibition = scoringPreference.ScoringPreferencesViewModel.TreateForeignAthletesAsExhibition;
                            ctx.ScroringPreferencesTemplateScoringAwards.Add(ScoringAward);
                            template.ScroringPreferencesTemplateScoringAward = ScoringAward;

                            ctx.ScroringPreferencesTemplateAgeGradings.RemoveRange(ctx.ScroringPreferencesTemplateAgeGradings.Where(p => p.ScroringPreferencesTemplateAgeGradingID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            ScroringPreferencesTemplateAgeGrading ScrollingPreferencesGrading = new ScroringPreferencesTemplateAgeGrading();
                            ScrollingPreferencesGrading.UseFiveYearAgingTableMulti = scoringPreference.ScoringPreferencesViewModel.UseFiveYearAgingTableMulti;
                            ScrollingPreferencesGrading.UseFiveYearAgingTableStandared = scoringPreference.ScoringPreferencesViewModel.UseFiveYearAgingTableStandared;
                            ScrollingPreferencesGrading.UseOneYearAgingTableMulti = scoringPreference.ScoringPreferencesViewModel.UseOneYearAgingTableMulti;
                            ScrollingPreferencesGrading.UseOneYearAgingTableStandared = scoringPreference.ScoringPreferencesViewModel.UseOneYearAgingTableStandared;
                            ctx.ScroringPreferencesTemplateAgeGradings.Add(ScrollingPreferencesGrading);
                            template.ScroringPreferencesTemplateAgeGrading = ScrollingPreferencesGrading;

                            ctx.ScroringPreferencesTemplateCCRRs.RemoveRange(ctx.ScroringPreferencesTemplateCCRRs.Where(p => p.ScroringPreferencesTemplateCCRRID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            ScroringPreferencesTemplateCCRR ccRR = new ScroringPreferencesTemplateCCRR();
                            ccRR.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6 = scoringPreference.ScoringPreferencesViewModel.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6;
                            ccRR.DynamicMultipleTeamScoring = scoringPreference.ScoringPreferencesViewModel.DynamicMultipleTeamScoring;
                            ccRR.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult = scoringPreference.ScoringPreferencesViewModel.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult;
                            ccRR.MaximumDisplacerPerTeam = scoringPreference.ScoringPreferencesViewModel.MaximumDisplacerPerTeam;
                            ccRR.MaximumRunnerToQualifyAsATeam = scoringPreference.ScoringPreferencesViewModel.MaximumRunnerToQualifyAsATeam;
                            ccRR.NoneTeamRunnerDisplacedOtherRunner = scoringPreference.ScoringPreferencesViewModel.NoneTeamRunnerDisplacedOtherRunner;
                            ccRR.NumberOfRunnersScoreFromEachTeam = scoringPreference.ScoringPreferencesViewModel.NumberOfRunnersScoreFromEachTeam;
                            ccRR.PanaltyPointAddedForShortTeamLast1Last2Etc = scoringPreference.ScoringPreferencesViewModel.PanaltyPointAddedForShortTeamLast1Last2Etc;
                            ccRR.PanaltyPointsAddedForShortTeams = scoringPreference.ScoringPreferencesViewModel.PanaltyPointsAddedForShortTeams;
                            ctx.ScroringPreferencesTemplateCCRRs.Add(ccRR);
                            template.ScroringPreferencesTemplateCCRR = ccRR;

                            ctx.ScroringPreferencesTemplateEntryLimits.RemoveRange(ctx.ScroringPreferencesTemplateEntryLimits.Where(p => p.ScroringPreferencesTemplateEntryLimitID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            ScroringPreferencesTemplateEntryLimit entryLimit = new ScroringPreferencesTemplateEntryLimit();
                            entryLimit.MaximumEntriesPerAthleteIncludingRelays = scoringPreference.ScoringPreferencesViewModel.MaximumEntriesPerAthleteIncludingRelays;
                            entryLimit.MaximumFieldEventEntriesPerAthletes = scoringPreference.ScoringPreferencesViewModel.MaximumFieldEventEntriesPerAthletes;
                            entryLimit.MaximumTrackEventEntriesPerAthleteIncludingRelay = scoringPreference.ScoringPreferencesViewModel.MaximumTrackEventEntriesPerAthleteIncludingRelay;
                            entryLimit.WarnIfEntryLimitsExceeds = scoringPreference.ScoringPreferencesViewModel.WarnIfEntryLimitsExceeds;
                            entryLimit.MaximumFieldEventEntriesPerAthletes = scoringPreference.ScoringPreferencesViewModel.MaximumFieldEventEntriesPerAthletes;
                            ctx.ScroringPreferencesTemplateEntryLimits.Add(entryLimit);
                            template.ScroringPreferencesTemplateEntryLimit = entryLimit;

                            ctx.ScroringPreferencesTemplateEntriesResults.RemoveRange(ctx.ScroringPreferencesTemplateEntriesResults.Where(p => p.ScroringPreferencesTemplateEntriesResultID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            ScroringPreferencesTemplateEntriesResult scoringEntryresult = new ScroringPreferencesTemplateEntriesResult();
                            scoringEntryresult.ConvertHandEntryTimesToFAT = scoringPreference.ScoringPreferencesViewModel.ConvertHandEntryTimesToFAT;
                            scoringEntryresult.EntryNotesWithEachEntry = scoringPreference.ScoringPreferencesViewModel.EntryNotesWithEachEntry;
                            scoringEntryresult.RankResultByAgeGradedMarks = scoringPreference.ScoringPreferencesViewModel.RankResultByAgeGradedMarks;
                            scoringEntryresult.RecordWindReading = scoringPreference.ScoringPreferencesViewModel.RecordWindReading;
                            scoringEntryresult.RoundDownTheNearestEventCentimerer = scoringPreference.ScoringPreferencesViewModel.RoundDownTheNearestEventCentimerer;
                            scoringEntryresult.RoundUpResultToTenthofReport = scoringPreference.ScoringPreferencesViewModel.RoundUpResultToTenthofReport;
                            scoringEntryresult.ShowFinishedPlaceWithEachEntry = scoringPreference.ScoringPreferencesViewModel.ShowFinishedPlaceWithEachEntry;
                            scoringEntryresult.UseEntryDeclarationMethod = scoringPreference.ScoringPreferencesViewModel.UseEntryDeclarationMethod;
                            scoringEntryresult.WarnIfTimesMarksAreOutofRange = scoringPreference.ScoringPreferencesViewModel.WarnIfTimesMarksAreOutofRange;
                            ctx.ScroringPreferencesTemplateEntriesResults.Add(scoringEntryresult);
                            template.ScroringPreferencesTemplateEntriesResult = scoringEntryresult;

                            ctx.ScroringPreferencesTemplateFieldRelays.RemoveRange(ctx.ScroringPreferencesTemplateFieldRelays.Where(p => p.ScroringPreferencesTemplateFieldRelayID == scoringPreference.TemplateOrMeetSetupID).ToList());
                            ScroringPreferencesTemplateFieldRelay fieldRelay = new ScroringPreferencesTemplateFieldRelay();
                            fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam = scoringPreference.ScoringPreferencesViewModel.FieldRelayCanBeScoreWithUnderSizeTeam;
                            fieldRelay.MarksWillBeAddedForReamRanking = scoringPreference.ScoringPreferencesViewModel.MarksWillBeAddedForReamRanking;
                            fieldRelay.TimesWillBeAddedForTeamRanking = scoringPreference.ScoringPreferencesViewModel.TimesWillBeAddedForTeamRanking;
                            fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam = scoringPreference.ScoringPreferencesViewModel.FieldRelayCanBeScoreWithUnderSizeTeam;
                            ctx.ScroringPreferencesTemplateFieldRelays.Add(fieldRelay);
                            template.ScroringPreferencesTemplateFieldRelay = fieldRelay;
                            ctx.ScroringPreferencesTemplates.Add(template);
                            ctx.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringPreferencesResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringPreferencesResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }


                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new ScoringPreferencesResult();
                Result.Result.ScoringPreferencesID = scoringPreference.TemplateOrMeetSetupID;
                Result.Result.AuthToken = null;
                return Result;


            }
        }

        public async Task<AuthorizedResponse<TemplateNameResult>> CreateSeedPreferenceTemplate(ServiceRequest_TemplateName ReplayPreferenceTemplateInfo)
        {
            try
            {
                int InsertedId = 0;
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    SeedPreferenceTemplate athleticPreferenceTemplate = new SeedPreferenceTemplate
                    {
                        SeedPreferenceID = ReplayPreferenceTemplateInfo.Preferenceid,
                        SeedPreferenceTemplateName = ReplayPreferenceTemplateInfo.TemplateName
                    };

                    ctx.SeedPreferenceTemplates.Add(athleticPreferenceTemplate);
                    ctx.SaveChanges();
                    InsertedId = athleticPreferenceTemplate.SeedPreferenceTemplateID;

                }

                AuthorizedResponse<TemplateNameResult> Result = new AuthorizedResponse<TemplateNameResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new TemplateNameResult();
                Result.Result.TemplateNameID = InsertedId.ToString();
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<TemplateNameResult> Result = new AuthorizedResponse<TemplateNameResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new TemplateNameResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<ScoringPreferencesResult>> DeleteAssignedScoringPreferencesMeet(int MeetSetupID)
        {
            AuthorizedResponse<ScoringPreferencesResult> Result = new AuthorizedResponse<ScoringPreferencesResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        ctx.MeetSetupScroringPreferencesScoringAwards.RemoveRange(ctx.MeetSetupScroringPreferencesScoringAwards.Where(p => p.MeetSetupID == MeetSetupID));
                        ctx.MeetSetupScroringPreferencesAgeGradings.RemoveRange(ctx.MeetSetupScroringPreferencesAgeGradings.Where(p => p.MeetSetupID == MeetSetupID));
                        ctx.MeetSetupScroringPreferencesCCRRs.RemoveRange(ctx.MeetSetupScroringPreferencesCCRRs.Where(p => p.MeetSetupID == MeetSetupID));
                        ctx.MeetSetupScroringPreferencesEntryLimits.RemoveRange(ctx.MeetSetupScroringPreferencesEntryLimits.Where(p => p.MeetSetupID == MeetSetupID));
                        ctx.MeetSetupScroringPreferencesEntriesResults.RemoveRange(ctx.MeetSetupScroringPreferencesEntriesResults.Where(p => p.MeetSetupID == MeetSetupID));
                        ctx.MeetScroringPreferencesFieldRelays.RemoveRange(ctx.MeetScroringPreferencesFieldRelays.Where(p => p.MeetSetupID == MeetSetupID));
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringPreferencesResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringPreferencesResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new ScoringPreferencesResult();
            Result.Result.ScoringPreferencesID = MeetSetupID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<ScoringSetupResult>> DeleteAssignedScoringSetupMeet(int MeetSetupID)
        {
            AuthorizedResponse<ScoringSetupResult> Result = new AuthorizedResponse<ScoringSetupResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        ctx.MeetScoringSetups.RemoveRange(ctx.MeetScoringSetups.Where(p => p.MeetSetupID == MeetSetupID));
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringSetupResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringSetupResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new ScoringSetupResult();
            Result.Result.ScoringSetupID = MeetSetupID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<MeetSetupResult>> DeleteMeet(int MeetSetupID)
        {
            AuthorizedResponse<MeetSetupResult> Result = new AuthorizedResponse<MeetSetupResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<MeetSetupAthletePreference> meetSetupAthletePreference = ctx.MeetSetupAthletePreferences.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupAthletePreference != null)
                        {
                            ctx.MeetSetupAthletePreferences.RemoveRange(meetSetupAthletePreference);
                        }
                        ctx.SaveChanges();

                        List<MeetSetupRelayPreference> meetSetupRelayPreferences = ctx.MeetSetupRelayPreferences.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupRelayPreferences != null)
                        {
                            ctx.MeetSetupRelayPreferences.RemoveRange(meetSetupRelayPreferences);
                        }
                        ctx.SaveChanges();


                        List<MeetSetupCompetitorNumber> meetSetupCompetitorNumbers = ctx.MeetSetupCompetitorNumbers.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupCompetitorNumbers != null)
                        {
                            ctx.MeetSetupCompetitorNumbers.RemoveRange(meetSetupCompetitorNumbers);
                        }
                        ctx.SaveChanges();

                        List<MeetSetupSeedingRule> meetSetupSeedingRules = ctx.MeetSetupSeedingRules.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupSeedingRules != null)
                        {
                            ctx.MeetSetupSeedingRules.RemoveRange(meetSetupSeedingRules);
                        }
                        ctx.SaveChanges();
                        List<MeetSetupRendomizationRule> meetSetupRendomizationRules = ctx.MeetSetupRendomizationRules.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupRendomizationRules != null)
                        {
                            ctx.MeetSetupRendomizationRules.RemoveRange(meetSetupRendomizationRules);
                        }
                        ctx.SaveChanges();

                        List<MeetStandardAlleyPrefrence> meetStandardAlleyPrefrences = ctx.MeetStandardAlleyPrefrences.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetStandardAlleyPrefrences != null)
                        {
                            ctx.MeetStandardAlleyPrefrences.RemoveRange(meetStandardAlleyPrefrences);
                        }
                        ctx.SaveChanges();

                        List<MeetSetupStandardLanePrefrence> meetSetupStandardLanePrefrences = ctx.MeetSetupStandardLanePrefrences.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupStandardLanePrefrences != null)
                        {
                            ctx.MeetSetupStandardLanePrefrences.RemoveRange(meetSetupStandardLanePrefrences);
                        }
                        ctx.SaveChanges();

                        List<MeetSetupWaterfallStart> meetSetupWaterfallStarts = ctx.MeetSetupWaterfallStarts.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupWaterfallStarts != null)
                        {
                            ctx.MeetSetupWaterfallStarts.RemoveRange(meetSetupWaterfallStarts);
                        }
                        ctx.SaveChanges();

                        List<MeetSetupDualMeet> meetSetupDualMeets = ctx.MeetSetupDualMeets.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupDualMeets != null)
                        {
                            ctx.MeetSetupDualMeets.RemoveRange(meetSetupDualMeets);
                        }

                        ctx.SaveChanges();

                        List<MeetSetupDualMeetAssignment> meetSetupDualMeetAssignments = ctx.MeetSetupDualMeetAssignments.Where(p => p.MeetSetupID == MeetSetupID).ToList();
                        if (meetSetupDualMeetAssignments != null)
                        {
                            ctx.MeetSetupDualMeetAssignments.RemoveRange(meetSetupDualMeetAssignments);
                        }

                        ctx.SaveChanges();

                        Entities.Models.MeetSetup meetSetup = ctx.MeetSetups.Where(p => p.MeetSetupID == MeetSetupID).FirstOrDefault();
                        if (meetSetup != null)
                        {
                            ctx.MeetSetups.Remove(meetSetup);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<MeetSetupResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new MeetSetupResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result = new AuthorizedResponse<MeetSetupResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new MeetSetupResult();
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<ScoringPreferencesResult>> DeletScoringPreferencesTemplate(int TemplateID)
        {
            AuthorizedResponse<ScoringPreferencesResult> Result = new AuthorizedResponse<ScoringPreferencesResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        ScroringPreferencesTemplate template = ctx.ScroringPreferencesTemplates.Where(p => p.ScroringPreferencesTemplateID == TemplateID).FirstOrDefault();
                        if (template != null)
                        {
                            ctx.ScroringPreferencesTemplateScoringAwards.RemoveRange(ctx.ScroringPreferencesTemplateScoringAwards.Where(p => p.ScroringPreferencesTemplateScoringAwardID == template.ScroringPreferencesTemplateScoringAwardID));
                            ctx.ScroringPreferencesTemplateAgeGradings.RemoveRange(ctx.ScroringPreferencesTemplateAgeGradings.Where(p => p.ScroringPreferencesTemplateAgeGradingID == template.ScroringPreferencesTemplateAgeGradingID));
                            ctx.ScroringPreferencesTemplateCCRRs.RemoveRange(ctx.ScroringPreferencesTemplateCCRRs.Where(p => p.ScroringPreferencesTemplateCCRRID == template.ScroringPreferencesTemplateCCRRID));
                            ctx.ScroringPreferencesTemplateEntryLimits.RemoveRange(ctx.ScroringPreferencesTemplateEntryLimits.Where(p => p.ScroringPreferencesTemplateEntryLimitID == template.ScroringPreferencesTemplateEntryLimitID));
                            ctx.ScroringPreferencesTemplateEntriesResults.RemoveRange(ctx.ScroringPreferencesTemplateEntriesResults.Where(p => p.ScroringPreferencesTemplateEntriesResultID == template.ScroringPreferencesTemplateEntriesResultID));
                            ctx.ScroringPreferencesTemplateFieldRelays.RemoveRange(ctx.ScroringPreferencesTemplateFieldRelays.Where(p => p.ScroringPreferencesTemplateFieldRelayID == template.ScroringPreferencesTemplateFieldRelayID));
                            ctx.ScroringPreferencesTemplates.RemoveRange(ctx.ScroringPreferencesTemplates.Where(p => p.ScroringPreferencesTemplateID == TemplateID));
                            ctx.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringPreferencesResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringPreferencesResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new ScoringPreferencesResult();
            Result.Result.ScoringPreferencesID = TemplateID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<ScoringSetupResult>> DeletScoringSetupTemplate(int TemplateID)
        {
            AuthorizedResponse<ScoringSetupResult> Result = new AuthorizedResponse<ScoringSetupResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        ctx.ScoringSetupTemplateDetails.RemoveRange(ctx.ScoringSetupTemplateDetails.Where(p => p.ScoringSetupTemplateID == TemplateID));
                        ctx.ScoringSetupTemplates.RemoveRange(ctx.ScoringSetupTemplates.Where(p => p.ScoringSetupTemplateID == TemplateID));
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringSetupResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringSetupResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new ScoringSetupResult();
            Result.Result.ScoringSetupID = TemplateID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<List<BaseCounty>> GetAllBaseCounty()
        {
            List<BaseCounty> baseCountyList = new List<BaseCounty>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (BaseCounty baseCounty in ctx.BaseCounties)
                {
                    baseCountyList.Add(baseCounty);
                }
            }
            return baseCountyList;
        }

        public async Task<List<DivisionRegionName>> GetAllDivisionRegionName()
        {
            List<DivisionRegionName> DivisionRegionNameList = new List<DivisionRegionName>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                foreach (DivisionRegionName baseCounty in ctx.DivisionRegionNames)
                {
                    DivisionRegionNameList.Add(baseCounty);
                }
            }
            return DivisionRegionNameList;
        }

        public async Task<List<LaneDetail>> GetAllLaneDetail()
        {
            List<LaneDetail> LaneDetailList = new List<LaneDetail>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (LaneDetail laneDetail in ctx.LaneDetails)
                {
                    LaneDetailList.Add(laneDetail);
                }
            }
            return LaneDetailList;
        }

        public async Task<List<MeetArena>> GetAllMeetArena()
        {
            List<MeetArena> baseMeetClass = new List<MeetArena>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (MeetArena meetArena in ctx.MeetArenas)
                {
                    baseMeetClass.Add(meetArena);
                }
            }
            return baseMeetClass;
        }

        public async Task<List<AthletePreference>> GetAllMeetAthletePreference()
        {
            List<AthletePreference> athletePreferenceList = new List<AthletePreference>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (AthletePreference athletePreference in ctx.AthletePreferences)
                {
                    athletePreferenceList.Add(athletePreference);
                }
            }
            return athletePreferenceList;
        }

        public async Task<List<MeetClass>> GetAllMeetClass()
        {
            List<MeetClass> baseMeetClass = new List<MeetClass>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (MeetClass meetArena in ctx.MeetClasses)
                {
                    baseMeetClass.Add(meetArena);
                }
            }
            return baseMeetClass;
        }

        public async Task<List<CompetitorNumber>> GetAllMeetCompetitorNumber()
        {
            List<CompetitorNumber> CompetitorNumberList = new List<CompetitorNumber>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (CompetitorNumber competitorNumber in ctx.CompetitorNumbers)
                {
                    CompetitorNumberList.Add(competitorNumber);
                }
            }
            return CompetitorNumberList;
        }

        public async Task<List<MeetKind>> GetAllMeetKind()
        {
            List<MeetKind> MeetKindList = new List<MeetKind>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (MeetKind meetKind in ctx.MeetKinds)
                {
                    MeetKindList.Add(meetKind);
                }
            }
            return MeetKindList;
        }

        public async Task<List<RelayPreference>> GetAllMeetRelayPreference()
        {
            List<RelayPreference> RelayPreferenceList = new List<RelayPreference>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (RelayPreference relayPreference in ctx.RelayPreferences)
                {
                    RelayPreferenceList.Add(relayPreference);
                }
            }
            return RelayPreferenceList;
        }

        public async Task<List<Entities.Models.MeetSetup>> GetAllMeetSetup()
        {
            List<Entities.Models.MeetSetup> MeetSetupList = new List<Entities.Models.MeetSetup>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (Entities.Models.MeetSetup meetKind in ctx.MeetSetups)
                {
                    MeetSetupList.Add(meetKind);
                }
            }
            return MeetSetupList;
        }

        public async Task<List<MeetStyle>> GetAllMeetStyle()
        {
            List<MeetStyle> MeetStyleList = new List<MeetStyle>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (MeetStyle meetStyle in ctx.MeetStyles)
                {
                    MeetStyleList.Add(meetStyle);
                }
            }
            return MeetStyleList;
        }

        public async Task<List<MeetType>> GetAllMeetType()
        {
            List<MeetType> MeetTypeList = new List<MeetType>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (MeetType meetType in ctx.MeetTypes)
                {
                    MeetTypeList.Add(meetType);
                }
            }
            return MeetTypeList;
        }

        public async Task<List<MeetTypeDivision>> GetAllMeetTypeDivision()
        {
            List<MeetTypeDivision> MeetTypeDivisionList = new List<MeetTypeDivision>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (MeetTypeDivision meetType in ctx.MeetTypeDivisions)
                {
                    MeetTypeDivisionList.Add(meetType);
                }
            }
            return MeetTypeDivisionList;
        }

        public async Task<List<GetRelayPrefrencesGroupData_Result>> GetAllRelayPrefrencesGroupData()
        {
            List<GetRelayPrefrencesGroupData_Result> StandardLanePrefrenceList = new List<GetRelayPrefrencesGroupData_Result>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (GetRelayPrefrencesGroupData_Result standardLanePrefrencs in ctx.GetRelayPrefrencesGroupData())
                {
                    StandardLanePrefrenceList.Add(standardLanePrefrencs);
                }
            }
            return StandardLanePrefrenceList;
        }

        public async Task<List<SchoolDetail>> GetAllSchoolDetail()
        {
            List<SchoolDetail> SchoolList = new List<SchoolDetail>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (SchoolDetail school in ctx.SchoolDetails)
                {
                    SchoolList.Add(school);
                }
            }
            return SchoolList;
        }

        public async Task<List<EntryScoringPreferencesViewModel>> GetAllScoringPreferencesTemplate()
        {
            List<EntryScoringPreferencesViewModel> EntryScoringPreferencesViewModelList = new List<EntryScoringPreferencesViewModel>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                EntryScoringPreferencesViewModel entryScoring = new EntryScoringPreferencesViewModel();
                foreach (ScroringPreferencesTemplate template in ctx.ScroringPreferencesTemplates)
                {
                    entryScoring.ScroringPreferencesTemplateName = template.ScroringPreferencesTemplateName;
                    entryScoring.ScroringPreferencesTemplatID = template.ScroringPreferencesTemplateID;
                    ScroringPreferencesTemplateScoringAward ScoringAward = ctx.ScroringPreferencesTemplateScoringAwards.Where(p => p.ScroringPreferencesTemplateScoringAwardID == template.ScroringPreferencesTemplateScoringAwardID).FirstOrDefault();
                    if (ScoringAward != null)
                    {
                        entryScoring.AllowForeignAthletesPointScoreToCountTowordTeamScore = ScoringAward.AllowForeignAthletesPointScoreToCountTowordTeamScore == null ? false : ScoringAward.AllowForeignAthletesPointScoreToCountTowordTeamScore.Value;
                        entryScoring.DiffrentPointSystemForEach = ScoringAward.DiffrentPointSystemForEach == null ? false : ScoringAward.DiffrentPointSystemForEach.Value;
                        entryScoring.DiffrentPointSystemForMaleAndFemale = ScoringAward.DiffrentPointSystemForMaleAndFemale == null ? false : ScoringAward.DiffrentPointSystemForMaleAndFemale.Value;
                        entryScoring.ExceedOrEqualQualifingMarks = ScoringAward.ExceedOrEqualQualifingMarks == null ? false : ScoringAward.ExceedOrEqualQualifingMarks.Value;
                        entryScoring.ExhibitionMarksReceiveFinalsRanks = ScoringAward.ExhibitionMarksReceiveFinalsRanks == null ? false : ScoringAward.ExhibitionMarksReceiveFinalsRanks.Value;
                        entryScoring.IncludeRelayPointsWithIndivHighPointScore = ScoringAward.IncludeRelayPointsWithIndivHighPointScore == null ? false : ScoringAward.IncludeRelayPointsWithIndivHighPointScore.Value;
                        entryScoring.MaximumPerTeamFinalFromPreLimsIndividual = ScoringAward.MaximumPerTeamFinalFromPreLimsIndividual == null ? 0 : ScoringAward.MaximumPerTeamFinalFromPreLimsIndividual.Value;
                        entryScoring.MaximumPerTeamFinalFromPreLimsRelay = ScoringAward.MaximumPerTeamFinalFromPreLimsRelay == null ? 0 : ScoringAward.MaximumPerTeamFinalFromPreLimsRelay.Value;
                        entryScoring.MaximumScorePerTeamPerEventIndividual = ScoringAward.MaximumScorePerTeamPerEventIndividual == null ? 0 : ScoringAward.MaximumScorePerTeamPerEventIndividual.Value;
                        entryScoring.MaximumScorePerTeamPerEventRelay = ScoringAward.MaximumScorePerTeamPerEventRelay == null ? 0 : ScoringAward.MaximumScorePerTeamPerEventRelay.Value;
                        entryScoring.ScoreEqualRelayOnly = ScoringAward.ScoreEqualRelayOnly == null ? false : ScoringAward.ScoreEqualRelayOnly.Value;
                        entryScoring.ScoreFastestHeadOnlyRegardLessOverAllPlace = ScoringAward.ScoreFastestHeadOnlyRegardLessOverAllPlace == null ? false : ScoringAward.ScoreFastestHeadOnlyRegardLessOverAllPlace.Value;
                        entryScoring.TopHowManyForAwardLabelIndividual = ScoringAward.TopHowManyForAwardLabelIndividual == null ? 0 : ScoringAward.TopHowManyForAwardLabelIndividual.Value;
                        entryScoring.TopHowManyForAwardLabelRelay = ScoringAward.TopHowManyForAwardLabelRelay == null ? 0 : ScoringAward.TopHowManyForAwardLabelRelay.Value;
                        entryScoring.TreateForeignAthletesAsExhibition = ScoringAward.TreateForeignAthletesAsExhibition == null ? false : ScoringAward.TreateForeignAthletesAsExhibition.Value;
                    }

                    ScroringPreferencesTemplateAgeGrading ScrollingPreferencesGrading = ctx.ScroringPreferencesTemplateAgeGradings.Where(p => p.ScroringPreferencesTemplateAgeGradingID == template.ScroringPreferencesTemplateAgeGradingID).FirstOrDefault();
                    if (ScrollingPreferencesGrading != null)
                    {
                        entryScoring.UseFiveYearAgingTableMulti = ScrollingPreferencesGrading.UseFiveYearAgingTableMulti == null ? false : ScrollingPreferencesGrading.UseFiveYearAgingTableMulti.Value;
                        entryScoring.UseFiveYearAgingTableStandared = ScrollingPreferencesGrading.UseFiveYearAgingTableStandared == null ? false : ScrollingPreferencesGrading.UseFiveYearAgingTableStandared.Value;
                        entryScoring.UseOneYearAgingTableMulti = ScrollingPreferencesGrading.UseOneYearAgingTableMulti == null ? false : ScrollingPreferencesGrading.UseOneYearAgingTableMulti.Value;
                        entryScoring.UseOneYearAgingTableStandared = ScrollingPreferencesGrading.UseOneYearAgingTableStandared == null ? false : ScrollingPreferencesGrading.UseOneYearAgingTableStandared.Value;
                    }

                    ScroringPreferencesTemplateCCRR ccRR = ctx.ScroringPreferencesTemplateCCRRs.Where(p => p.ScroringPreferencesTemplateCCRRID == template.ScroringPreferencesTemplateCCRRID).FirstOrDefault();
                    if (ccRR != null)
                    {
                        entryScoring.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6 = ccRR.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6 == null ? 0 : ccRR.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6.Value;
                        entryScoring.DynamicMultipleTeamScoring = ccRR.DynamicMultipleTeamScoring == null ? false : ccRR.DynamicMultipleTeamScoring.Value;
                        entryScoring.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult = ccRR.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult == null ? 0 : ccRR.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult.Value;
                        entryScoring.MaximumDisplacerPerTeam = ccRR.MaximumDisplacerPerTeam == null ? 0 : ccRR.MaximumDisplacerPerTeam.Value;
                        entryScoring.MaximumRunnerToQualifyAsATeam = ccRR.MaximumRunnerToQualifyAsATeam == null ? 0 : ccRR.MaximumRunnerToQualifyAsATeam.Value;
                        entryScoring.NoneTeamRunnerDisplacedOtherRunner = ccRR.NoneTeamRunnerDisplacedOtherRunner == null ? false : ccRR.NoneTeamRunnerDisplacedOtherRunner.Value;
                        entryScoring.NumberOfRunnersScoreFromEachTeam = ccRR.NumberOfRunnersScoreFromEachTeam == null ? 0 : ccRR.NumberOfRunnersScoreFromEachTeam.Value;
                        entryScoring.PanaltyPointAddedForShortTeamLast1Last2Etc = ccRR.PanaltyPointAddedForShortTeamLast1Last2Etc == null ? false : ccRR.PanaltyPointAddedForShortTeamLast1Last2Etc.Value;
                        entryScoring.PanaltyPointsAddedForShortTeams = ccRR.PanaltyPointsAddedForShortTeams == null ? 0 : ccRR.PanaltyPointsAddedForShortTeams.Value;
                    }

                    ScroringPreferencesTemplateEntryLimit entryLimit = ctx.ScroringPreferencesTemplateEntryLimits.Where(p => p.ScroringPreferencesTemplateEntryLimitID == template.ScroringPreferencesTemplateEntryLimitID).FirstOrDefault();
                    if (entryLimit != null)
                    {
                        entryScoring.MaximumEntriesPerAthleteIncludingRelays = entryLimit.MaximumEntriesPerAthleteIncludingRelays == null ? 0 : entryLimit.MaximumEntriesPerAthleteIncludingRelays.Value;
                        entryScoring.MaximumFieldEventEntriesPerAthletes = entryLimit.MaximumFieldEventEntriesPerAthletes == null ? 0 : entryLimit.MaximumFieldEventEntriesPerAthletes.Value;
                        entryScoring.MaximumTrackEventEntriesPerAthleteIncludingRelay = entryLimit.MaximumTrackEventEntriesPerAthleteIncludingRelay == null ? 0 : entryLimit.MaximumTrackEventEntriesPerAthleteIncludingRelay.Value;
                        entryScoring.WarnIfEntryLimitsExceeds = entryLimit.WarnIfEntryLimitsExceeds == null ? false : entryLimit.WarnIfEntryLimitsExceeds.Value;
                        entryScoring.MaximumFieldEventEntriesPerAthletes = entryLimit.MaximumFieldEventEntriesPerAthletes == null ? 0 : entryLimit.MaximumFieldEventEntriesPerAthletes.Value;
                    }

                    ScroringPreferencesTemplateEntriesResult scoringEntryresult = ctx.ScroringPreferencesTemplateEntriesResults.Where(p => p.ScroringPreferencesTemplateEntriesResultID == template.ScroringPreferencesTemplateEntriesResultID).FirstOrDefault();
                    if (scoringEntryresult != null)
                    {
                        entryScoring.ConvertHandEntryTimesToFAT = scoringEntryresult.ConvertHandEntryTimesToFAT == null ? false : scoringEntryresult.ConvertHandEntryTimesToFAT.Value;
                        entryScoring.EntryNotesWithEachEntry = scoringEntryresult.EntryNotesWithEachEntry == null ? false : scoringEntryresult.EntryNotesWithEachEntry.Value;
                        entryScoring.RankResultByAgeGradedMarks = scoringEntryresult.RankResultByAgeGradedMarks == null ? false : scoringEntryresult.RankResultByAgeGradedMarks.Value;
                        entryScoring.RecordWindReading = scoringEntryresult.RecordWindReading == null ? false : scoringEntryresult.RecordWindReading.Value;
                        entryScoring.RoundDownTheNearestEventCentimerer = scoringEntryresult.RoundDownTheNearestEventCentimerer == null ? false : scoringEntryresult.RoundDownTheNearestEventCentimerer.Value;
                        entryScoring.RoundUpResultToTenthofReport = scoringEntryresult.RoundUpResultToTenthofReport == null ? false : scoringEntryresult.RoundUpResultToTenthofReport.Value;
                        entryScoring.ShowFinishedPlaceWithEachEntry = scoringEntryresult.ShowFinishedPlaceWithEachEntry == null ? false : scoringEntryresult.ShowFinishedPlaceWithEachEntry.Value;
                        entryScoring.UseEntryDeclarationMethod = scoringEntryresult.UseEntryDeclarationMethod == null ? false : scoringEntryresult.UseEntryDeclarationMethod.Value;
                        entryScoring.WarnIfTimesMarksAreOutofRange = scoringEntryresult.WarnIfTimesMarksAreOutofRange == null ? false : scoringEntryresult.WarnIfTimesMarksAreOutofRange.Value;
                    }

                    ScroringPreferencesTemplateFieldRelay fieldRelay = ctx.ScroringPreferencesTemplateFieldRelays.Where(p => p.ScroringPreferencesTemplateFieldRelayID == template.ScroringPreferencesTemplateFieldRelayID).FirstOrDefault();
                    if (fieldRelay != null)
                    {
                        entryScoring.FieldRelayCanBeScoreWithUnderSizeTeam = fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam == null ? false : fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam.Value;
                        entryScoring.MarksWillBeAddedForReamRanking = fieldRelay.MarksWillBeAddedForReamRanking == null ? 0 : fieldRelay.MarksWillBeAddedForReamRanking.Value;
                        entryScoring.TimesWillBeAddedForTeamRanking = fieldRelay.TimesWillBeAddedForTeamRanking == null ? 0 : fieldRelay.TimesWillBeAddedForTeamRanking.Value;
                        entryScoring.FieldRelayCanBeScoreWithUnderSizeTeam = fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam == null ? false : fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam.Value;
                    }
                    EntryScoringPreferencesViewModelList.Add(entryScoring);

                }
            }
            return EntryScoringPreferencesViewModelList;
        }

        public async Task<List<ServiceRequest_ScoringSetup>> GetAllScoringSetupTemplate()
        {
            List<ServiceRequest_ScoringSetup> ScoringSetupTemplateList = new List<ServiceRequest_ScoringSetup>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                foreach (var item in ctx.ScoringSetupTemplates)
                {
                    ServiceRequest_ScoringSetup scoringSetupTemplate = new ServiceRequest_ScoringSetup();
                    scoringSetupTemplate.TemplateName = item.TemplateName;
                    scoringSetupTemplate.IsDefault = item.IsDefault == null ? false : item.IsDefault.Value;
                    scoringSetupTemplate.TemplateOrMeetSetupID = item.ScoringSetupTemplateID;
                    scoringSetupTemplate.ScoringSetupTemplateList = new List<ScoringSetupTemplateDetail>();
                    foreach (var templateDetail in ctx.ScoringSetupTemplateDetails.Where(p => p.ScoringSetupTemplateID == item.ScoringSetupTemplateID))
                    {
                        ScoringSetupTemplateDetail scoringTemplateDetail = new ScoringSetupTemplateDetail();
                        scoringTemplateDetail.CombEvtPoints = templateDetail.CombEvtPoints;
                        scoringTemplateDetail.IndividualPints = templateDetail.IndividualPints;
                        scoringTemplateDetail.Place = templateDetail.Place;
                        scoringTemplateDetail.RelayPoints = templateDetail.RelayPoints;
                        scoringTemplateDetail.ScoringSetupTemplateDetailID = templateDetail.ScoringSetupTemplateDetailID;
                        scoringSetupTemplate.ScoringSetupTemplateList.Add(scoringTemplateDetail);
                    }
                    ScoringSetupTemplateList.Add(scoringSetupTemplate);
                }
            }
            return ScoringSetupTemplateList;
        }

        public async Task<List<GetSeedingPreferencesGroupDetail_Result>> GetAllSeedingPreferencesGroup()
        {
            List<GetSeedingPreferencesGroupDetail_Result> StandardLanePrefrenceList = new List<GetSeedingPreferencesGroupDetail_Result>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (GetSeedingPreferencesGroupDetail_Result standardLanePrefrencs in ctx.GetSeedingPreferencesGroupDetail())
                {
                    StandardLanePrefrenceList.Add(standardLanePrefrencs);
                }
            }
            return StandardLanePrefrenceList;
        }

        public async Task<List<StandardAlleyPrefrence>> GetAllStandardAlleyPrefrence()
        {
            List<StandardAlleyPrefrence> StandardLanePrefrenceList = new List<StandardAlleyPrefrence>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (StandardAlleyPrefrence standardLanePrefrencs in ctx.StandardAlleyPrefrences)
                {
                    StandardLanePrefrenceList.Add(standardLanePrefrencs);
                }
            }
            return StandardLanePrefrenceList;
        }

        public async Task<List<StandardLanePrefrence>> GetAllStandardLanePrefrence()
        {
            List<StandardLanePrefrence> StandardLanePrefrenceList = new List<StandardLanePrefrence>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (StandardLanePrefrence standardLanePrefrencs in ctx.StandardLanePrefrences)
                {
                    StandardLanePrefrenceList.Add(standardLanePrefrencs);
                }
            }
            return StandardLanePrefrenceList;
        }

        public async Task<List<WaterfallStartPreference>> GetAllWaterfallStartPreference()
        {
            List<WaterfallStartPreference> StandardLanePrefrenceList = new List<WaterfallStartPreference>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (WaterfallStartPreference standardLanePrefrencs in ctx.WaterfallStartPreferences)
                {
                    StandardLanePrefrenceList.Add(standardLanePrefrencs);
                }
            }
            return StandardLanePrefrenceList;
        }

        public async Task<List<GetRelayPrefrencesMeetData_Result>> GetRelayPrefrencesMeetData(int meetSetupID)
        {
            List<GetRelayPrefrencesMeetData_Result> StandardLanePrefrenceList = new List<GetRelayPrefrencesMeetData_Result>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (GetRelayPrefrencesMeetData_Result standardLanePrefrencs in ctx.GetRelayPrefrencesMeetData(meetSetupID))
                {
                    StandardLanePrefrenceList.Add(standardLanePrefrencs);
                }
            }
            return StandardLanePrefrenceList;
        }

        public async Task<List<EntryScoringPreferencesViewModel>> GetScoringPreferencesByMeetID(int MeetSetupID)
        {
            List<EntryScoringPreferencesViewModel> EntryScoringPreferencesViewModelList = new List<EntryScoringPreferencesViewModel>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                EntryScoringPreferencesViewModel entryScoring = new EntryScoringPreferencesViewModel();
                MeetSetupScroringPreferencesScoringAward ScoringAward = ctx.MeetSetupScroringPreferencesScoringAwards.Where(p => p.MeetSetupID == MeetSetupID).FirstOrDefault();
                if (ScoringAward != null)
                {
                    entryScoring.AllowForeignAthletesPointScoreToCountTowordTeamScore = ScoringAward.AllowForeignAthletesPointScoreToCountTowordTeamScore == null ? false : ScoringAward.AllowForeignAthletesPointScoreToCountTowordTeamScore.Value;
                    entryScoring.DiffrentPointSystemForEach = ScoringAward.DiffrentPointSystemForEach == null ? false : ScoringAward.DiffrentPointSystemForEach.Value;
                    entryScoring.DiffrentPointSystemForMaleAndFemale = ScoringAward.DiffrentPointSystemForMaleAndFemale == null ? false : ScoringAward.DiffrentPointSystemForMaleAndFemale.Value;
                    entryScoring.ExceedOrEqualQualifingMarks = ScoringAward.ExceedOrEqualQualifingMarks == null ? false : ScoringAward.ExceedOrEqualQualifingMarks.Value;
                    entryScoring.ExhibitionMarksReceiveFinalsRanks = ScoringAward.ExhibitionMarksReceiveFinalsRanks == null ? false : ScoringAward.ExhibitionMarksReceiveFinalsRanks.Value;
                    entryScoring.IncludeRelayPointsWithIndivHighPointScore = ScoringAward.IncludeRelayPointsWithIndivHighPointScore == null ? false : ScoringAward.IncludeRelayPointsWithIndivHighPointScore.Value;
                    entryScoring.MaximumPerTeamFinalFromPreLimsIndividual = ScoringAward.MaximumPerTeamFinalFromPreLimsIndividual == null ? 0 : ScoringAward.MaximumPerTeamFinalFromPreLimsIndividual.Value;
                    entryScoring.MaximumPerTeamFinalFromPreLimsRelay = ScoringAward.MaximumPerTeamFinalFromPreLimsRelay == null ? 0 : ScoringAward.MaximumPerTeamFinalFromPreLimsRelay.Value;
                    entryScoring.MaximumScorePerTeamPerEventIndividual = ScoringAward.MaximumScorePerTeamPerEventIndividual == null ? 0 : ScoringAward.MaximumScorePerTeamPerEventIndividual.Value;
                    entryScoring.MaximumScorePerTeamPerEventRelay = ScoringAward.MaximumScorePerTeamPerEventRelay == null ? 0 : ScoringAward.MaximumScorePerTeamPerEventRelay.Value;
                    entryScoring.ScoreEqualRelayOnly = ScoringAward.ScoreEqualRelayOnly == null ? false : ScoringAward.ScoreEqualRelayOnly.Value;
                    entryScoring.ScoreFastestHeadOnlyRegardLessOverAllPlace = ScoringAward.ScoreFastestHeadOnlyRegardLessOverAllPlace == null ? false : ScoringAward.ScoreFastestHeadOnlyRegardLessOverAllPlace.Value;
                    entryScoring.TopHowManyForAwardLabelIndividual = ScoringAward.TopHowManyForAwardLabelIndividual == null ? 0 : ScoringAward.TopHowManyForAwardLabelIndividual.Value;
                    entryScoring.TopHowManyForAwardLabelRelay = ScoringAward.TopHowManyForAwardLabelRelay == null ? 0 : ScoringAward.TopHowManyForAwardLabelRelay.Value;
                    entryScoring.TreateForeignAthletesAsExhibition = ScoringAward.TreateForeignAthletesAsExhibition == null ? false : ScoringAward.TreateForeignAthletesAsExhibition.Value;
                }

                MeetSetupScroringPreferencesAgeGrading ScrollingPreferencesGrading = ctx.MeetSetupScroringPreferencesAgeGradings.Where(p => p.MeetSetupID == MeetSetupID).FirstOrDefault();
                if (ScrollingPreferencesGrading != null)
                {
                    entryScoring.UseFiveYearAgingTableMulti = ScrollingPreferencesGrading.UseFiveYearAgingTableMulti == null ? false : ScrollingPreferencesGrading.UseFiveYearAgingTableMulti.Value;
                    entryScoring.UseFiveYearAgingTableStandared = ScrollingPreferencesGrading.UseFiveYearAgingTableStandared == null ? false : ScrollingPreferencesGrading.UseFiveYearAgingTableStandared.Value;
                    entryScoring.UseOneYearAgingTableMulti = ScrollingPreferencesGrading.UseOneYearAgingTableMulti == null ? false : ScrollingPreferencesGrading.UseOneYearAgingTableMulti.Value;
                    entryScoring.UseOneYearAgingTableStandared = ScrollingPreferencesGrading.UseOneYearAgingTableStandared == null ? false : ScrollingPreferencesGrading.UseOneYearAgingTableStandared.Value;
                }

                MeetSetupScroringPreferencesCCRR ccRR = ctx.MeetSetupScroringPreferencesCCRRs.Where(p => p.MeetSetupID == MeetSetupID).FirstOrDefault();
                if (ccRR != null)
                {
                    entryScoring.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6 = ccRR.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6 == null ? 0 : ccRR.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6.Value;
                    entryScoring.DynamicMultipleTeamScoring = ccRR.DynamicMultipleTeamScoring == null ? false : ccRR.DynamicMultipleTeamScoring.Value;
                    entryScoring.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult = ccRR.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult == null ? 0 : ccRR.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult.Value;
                    entryScoring.MaximumDisplacerPerTeam = ccRR.MaximumDisplacerPerTeam == null ? 0 : ccRR.MaximumDisplacerPerTeam.Value;
                    entryScoring.MaximumRunnerToQualifyAsATeam = ccRR.MaximumRunnerToQualifyAsATeam == null ? 0 : ccRR.MaximumRunnerToQualifyAsATeam.Value;
                    entryScoring.NoneTeamRunnerDisplacedOtherRunner = ccRR.NoneTeamRunnerDisplacedOtherRunner == null ? false : ccRR.NoneTeamRunnerDisplacedOtherRunner.Value;
                    entryScoring.NumberOfRunnersScoreFromEachTeam = ccRR.NumberOfRunnersScoreFromEachTeam == null ? 0 : ccRR.NumberOfRunnersScoreFromEachTeam.Value;
                    entryScoring.PanaltyPointAddedForShortTeamLast1Last2Etc = ccRR.PanaltyPointAddedForShortTeamLast1Last2Etc == null ? false : ccRR.PanaltyPointAddedForShortTeamLast1Last2Etc.Value;
                    entryScoring.PanaltyPointsAddedForShortTeams = ccRR.PanaltyPointsAddedForShortTeams == null ? 0 : ccRR.PanaltyPointsAddedForShortTeams.Value;
                }

                MeetSetupScroringPreferencesEntryLimit entryLimit = ctx.MeetSetupScroringPreferencesEntryLimits.Where(p => p.MeetSetupID == MeetSetupID).FirstOrDefault();
                if (entryLimit != null)
                {
                    entryScoring.MaximumEntriesPerAthleteIncludingRelays = entryLimit.MaximumEntriesPerAthleteIncludingRelays == null ? 0 : entryLimit.MaximumEntriesPerAthleteIncludingRelays.Value;
                    entryScoring.MaximumFieldEventEntriesPerAthletes = entryLimit.MaximumFieldEventEntriesPerAthletes == null ? 0 : entryLimit.MaximumFieldEventEntriesPerAthletes.Value;
                    entryScoring.MaximumTrackEventEntriesPerAthleteIncludingRelay = entryLimit.MaximumTrackEventEntriesPerAthleteIncludingRelay == null ? 0 : entryLimit.MaximumTrackEventEntriesPerAthleteIncludingRelay.Value;
                    entryScoring.WarnIfEntryLimitsExceeds = entryLimit.WarnIfEntryLimitsExceeds == null ? false : entryLimit.WarnIfEntryLimitsExceeds.Value;
                    entryScoring.MaximumFieldEventEntriesPerAthletes = entryLimit.MaximumFieldEventEntriesPerAthletes == null ? 0 : entryLimit.MaximumFieldEventEntriesPerAthletes.Value;
                }

                MeetSetupScroringPreferencesEntriesResult scoringEntryresult = ctx.MeetSetupScroringPreferencesEntriesResults.Where(p => p.MeetSetupID == MeetSetupID).FirstOrDefault();
                if (scoringEntryresult != null)
                {
                    entryScoring.ConvertHandEntryTimesToFAT = scoringEntryresult.ConvertHandEntryTimesToFAT == null ? false : scoringEntryresult.ConvertHandEntryTimesToFAT.Value;
                    entryScoring.EntryNotesWithEachEntry = scoringEntryresult.EntryNotesWithEachEntry == null ? false : scoringEntryresult.EntryNotesWithEachEntry.Value;
                    entryScoring.RankResultByAgeGradedMarks = scoringEntryresult.RankResultByAgeGradedMarks == null ? false : scoringEntryresult.RankResultByAgeGradedMarks.Value;
                    entryScoring.RecordWindReading = scoringEntryresult.RecordWindReading == null ? false : scoringEntryresult.RecordWindReading.Value;
                    entryScoring.RoundDownTheNearestEventCentimerer = scoringEntryresult.RoundDownTheNearestEventCentimerer == null ? false : scoringEntryresult.RoundDownTheNearestEventCentimerer.Value;
                    entryScoring.RoundUpResultToTenthofReport = scoringEntryresult.RoundUpResultToTenthofReport == null ? false : scoringEntryresult.RoundUpResultToTenthofReport.Value;
                    entryScoring.ShowFinishedPlaceWithEachEntry = scoringEntryresult.ShowFinishedPlaceWithEachEntry == null ? false : scoringEntryresult.ShowFinishedPlaceWithEachEntry.Value;
                    entryScoring.UseEntryDeclarationMethod = scoringEntryresult.UseEntryDeclarationMethod == null ? false : scoringEntryresult.UseEntryDeclarationMethod.Value;
                    entryScoring.WarnIfTimesMarksAreOutofRange = scoringEntryresult.WarnIfTimesMarksAreOutofRange == null ? false : scoringEntryresult.WarnIfTimesMarksAreOutofRange.Value;
                }

                MeetScroringPreferencesFieldRelay fieldRelay = ctx.MeetScroringPreferencesFieldRelays.Where(p => p.MeetSetupID == MeetSetupID).FirstOrDefault();
                if (fieldRelay != null)
                {
                    entryScoring.FieldRelayCanBeScoreWithUnderSizeTeam = fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam == null ? false : fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam.Value;
                    entryScoring.MarksWillBeAddedForReamRanking = fieldRelay.MarksWillBeAddedForReamRanking == null ? 0 : fieldRelay.MarksWillBeAddedForReamRanking.Value;
                    entryScoring.TimesWillBeAddedForTeamRanking = fieldRelay.TimesWillBeAddedForTeamRanking == null ? 0 : fieldRelay.TimesWillBeAddedForTeamRanking.Value;
                    entryScoring.FieldRelayCanBeScoreWithUnderSizeTeam = fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam == null ? false : fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam.Value;
                }
                EntryScoringPreferencesViewModelList.Add(entryScoring);

            }
            return EntryScoringPreferencesViewModelList;
        }

        public async Task<List<ScoringSetupTemplateDetail>> GetScoringSetupByMeetID(int MeetSetupID)
        {
            List<ScoringSetupTemplateDetail> ScoringSetupTemplateList = new List<ScoringSetupTemplateDetail>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                MeetScoringSetup meetScoringSetup = ctx.MeetScoringSetups.Where(p => p.MeetSetupID == MeetSetupID).FirstOrDefault();
                foreach (var item in ctx.MeetScoringSetups.Where(p => p.MeetSetupID == MeetSetupID))
                {
                    ScoringSetupTemplateDetail ScoringSetupTemplate = new ScoringSetupTemplateDetail();
                    ScoringSetupTemplate.CombEvtPoints = item.CombEvtPoints;
                    ScoringSetupTemplate.IndividualPints = item.IndividualPints;
                    ScoringSetupTemplate.Place = item.Place;
                    ScoringSetupTemplate.RelayPoints = item.RelayPoints;
                    ScoringSetupTemplateList.Add(ScoringSetupTemplate);
                }
            }
            return ScoringSetupTemplateList;
        }

        public async Task<ServiceRequest_ScoringSetup> GetScoringSetupDefault()
        {
            ServiceRequest_ScoringSetup ScoringSetupTemplate = null;
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                var item = ctx.ScoringSetupTemplates.Where(p => p.IsDefault == true).FirstOrDefault();
                if (item != null)
                {
                    ScoringSetupTemplate = new ServiceRequest_ScoringSetup();
                    ScoringSetupTemplate.TemplateName = item.TemplateName;
                    ScoringSetupTemplate.IsDefault = item.IsDefault == null ? false : item.IsDefault.Value;
                    ScoringSetupTemplate.TemplateOrMeetSetupID = item.ScoringSetupTemplateID;
                    ScoringSetupTemplate.ScoringSetupTemplateList = new List<ScoringSetupTemplateDetail>();
                    foreach (var templateDetail in ctx.ScoringSetupTemplateDetails.Where(p => p.ScoringSetupTemplateID == item.ScoringSetupTemplateID))
                    {
                        ScoringSetupTemplateDetail scoringTemplateDetail = new ScoringSetupTemplateDetail();
                        scoringTemplateDetail.CombEvtPoints = templateDetail.CombEvtPoints;
                        scoringTemplateDetail.IndividualPints = templateDetail.IndividualPints;
                        scoringTemplateDetail.Place = templateDetail.Place;
                        scoringTemplateDetail.RelayPoints = templateDetail.RelayPoints;
                        scoringTemplateDetail.ScoringSetupTemplateDetailID = templateDetail.ScoringSetupTemplateDetailID;
                        ScoringSetupTemplate.ScoringSetupTemplateList.Add(scoringTemplateDetail);

                    }
                }
            }
            return ScoringSetupTemplate;
        }

        public async Task<List<GetSeedingPreferencesMeetData_Result>> GetSeedingPreferencesMeetData(int meetSetupID)
        {
            List<GetSeedingPreferencesMeetData_Result> StandardLanePrefrenceList = new List<GetSeedingPreferencesMeetData_Result>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (GetSeedingPreferencesMeetData_Result standardLanePrefrencs in ctx.GetSeedingPreferencesMeetData(meetSetupID))
                {
                    StandardLanePrefrenceList.Add(standardLanePrefrencs);
                }
            }
            return StandardLanePrefrenceList;
        }

        public async Task<AuthorizedResponse<ScoringPreferencesResult>> UpdateAssignedScoringPreferencesMeet(ServiceRequest_ScoringPreferences scoringPreference)
        {
            AuthorizedResponse<ScoringPreferencesResult> Result = new AuthorizedResponse<ScoringPreferencesResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (scoringPreference.ScoringPreferencesViewModel != null)
                        {
                            MeetSetupScroringPreferencesScoringAward meetSetupScoringAward = ctx.MeetSetupScroringPreferencesScoringAwards.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).FirstOrDefault();
                            if (meetSetupScoringAward == null)
                            {
                                meetSetupScoringAward = new MeetSetupScroringPreferencesScoringAward();
                            }
                            meetSetupScoringAward.AllowForeignAthletesPointScoreToCountTowordTeamScore = scoringPreference.ScoringPreferencesViewModel.AllowForeignAthletesPointScoreToCountTowordTeamScore;
                            meetSetupScoringAward.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            meetSetupScoringAward.DiffrentPointSystemForEach = scoringPreference.ScoringPreferencesViewModel.DiffrentPointSystemForEach;
                            meetSetupScoringAward.DiffrentPointSystemForMaleAndFemale = scoringPreference.ScoringPreferencesViewModel.DiffrentPointSystemForMaleAndFemale;
                            meetSetupScoringAward.ExceedOrEqualQualifingMarks = scoringPreference.ScoringPreferencesViewModel.ExceedOrEqualQualifingMarks;
                            meetSetupScoringAward.ExhibitionMarksReceiveFinalsRanks = scoringPreference.ScoringPreferencesViewModel.ExhibitionMarksReceiveFinalsRanks;
                            meetSetupScoringAward.IncludeRelayPointsWithIndivHighPointScore = scoringPreference.ScoringPreferencesViewModel.IncludeRelayPointsWithIndivHighPointScore;
                            meetSetupScoringAward.MaximumPerTeamFinalFromPreLimsIndividual = scoringPreference.ScoringPreferencesViewModel.MaximumPerTeamFinalFromPreLimsIndividual;
                            meetSetupScoringAward.MaximumPerTeamFinalFromPreLimsRelay = scoringPreference.ScoringPreferencesViewModel.MaximumPerTeamFinalFromPreLimsRelay;
                            meetSetupScoringAward.MaximumScorePerTeamPerEventIndividual = scoringPreference.ScoringPreferencesViewModel.MaximumScorePerTeamPerEventIndividual;
                            meetSetupScoringAward.MaximumScorePerTeamPerEventRelay = scoringPreference.ScoringPreferencesViewModel.MaximumScorePerTeamPerEventRelay;
                            meetSetupScoringAward.ScoreEqualRelayOnly = scoringPreference.ScoringPreferencesViewModel.ScoreEqualRelayOnly;
                            meetSetupScoringAward.ScoreFastestHeadOnlyRegardLessOverAllPlace = scoringPreference.ScoringPreferencesViewModel.ScoreFastestHeadOnlyRegardLessOverAllPlace;
                            meetSetupScoringAward.TopHowManyForAwardLabelIndividual = scoringPreference.ScoringPreferencesViewModel.TopHowManyForAwardLabelIndividual;
                            meetSetupScoringAward.TopHowManyForAwardLabelRelay = scoringPreference.ScoringPreferencesViewModel.TopHowManyForAwardLabelRelay;
                            meetSetupScoringAward.TreateForeignAthletesAsExhibition = scoringPreference.ScoringPreferencesViewModel.TreateForeignAthletesAsExhibition;
                            if (meetSetupScoringAward.ScroringPreferencesScoringAwardID <= 0)
                            {
                                ctx.MeetSetupScroringPreferencesScoringAwards.Add(meetSetupScoringAward);
                            }

                            MeetSetupScroringPreferencesAgeGrading meetSetupScrollingPreferencesGrading = ctx.MeetSetupScroringPreferencesAgeGradings.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).FirstOrDefault();
                            if (meetSetupScrollingPreferencesGrading == null)
                            {
                                meetSetupScrollingPreferencesGrading = new MeetSetupScroringPreferencesAgeGrading();
                            }
                            meetSetupScrollingPreferencesGrading.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            meetSetupScrollingPreferencesGrading.UseFiveYearAgingTableMulti = scoringPreference.ScoringPreferencesViewModel.UseFiveYearAgingTableMulti;
                            meetSetupScrollingPreferencesGrading.UseFiveYearAgingTableStandared = scoringPreference.ScoringPreferencesViewModel.UseFiveYearAgingTableStandared;
                            meetSetupScrollingPreferencesGrading.UseOneYearAgingTableMulti = scoringPreference.ScoringPreferencesViewModel.UseOneYearAgingTableMulti;
                            meetSetupScrollingPreferencesGrading.UseOneYearAgingTableStandared = scoringPreference.ScoringPreferencesViewModel.UseOneYearAgingTableStandared;
                            if (meetSetupScrollingPreferencesGrading.ScroringPreferencesAgeGradingID <= 0)
                            {
                                ctx.MeetSetupScroringPreferencesAgeGradings.Add(meetSetupScrollingPreferencesGrading);
                            }

                            MeetSetupScroringPreferencesCCRR ccRR = ctx.MeetSetupScroringPreferencesCCRRs.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).FirstOrDefault();
                            if (ccRR == null)
                            {
                                ccRR = new MeetSetupScroringPreferencesCCRR();
                            }
                            ccRR.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            ccRR.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6 = scoringPreference.ScoringPreferencesViewModel.DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6;
                            ccRR.DynamicMultipleTeamScoring = scoringPreference.ScoringPreferencesViewModel.DynamicMultipleTeamScoring;
                            ccRR.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult = scoringPreference.ScoringPreferencesViewModel.HowManuTopOverAllRunnersToExcludeFromAgeGroupResult;
                            ccRR.MaximumDisplacerPerTeam = scoringPreference.ScoringPreferencesViewModel.MaximumDisplacerPerTeam;
                            ccRR.MaximumRunnerToQualifyAsATeam = scoringPreference.ScoringPreferencesViewModel.MaximumRunnerToQualifyAsATeam;
                            ccRR.NoneTeamRunnerDisplacedOtherRunner = scoringPreference.ScoringPreferencesViewModel.NoneTeamRunnerDisplacedOtherRunner;
                            ccRR.NumberOfRunnersScoreFromEachTeam = scoringPreference.ScoringPreferencesViewModel.NumberOfRunnersScoreFromEachTeam;
                            ccRR.PanaltyPointAddedForShortTeamLast1Last2Etc = scoringPreference.ScoringPreferencesViewModel.PanaltyPointAddedForShortTeamLast1Last2Etc;
                            ccRR.PanaltyPointsAddedForShortTeams = scoringPreference.ScoringPreferencesViewModel.PanaltyPointsAddedForShortTeams;
                            if (ccRR.ScroringPreferencesCCRRID <= 0)
                            {
                                ctx.MeetSetupScroringPreferencesCCRRs.Add(ccRR);
                            }

                            MeetSetupScroringPreferencesEntryLimit entryLimit = ctx.MeetSetupScroringPreferencesEntryLimits.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).FirstOrDefault();
                            if (entryLimit == null)
                            {
                                entryLimit = new MeetSetupScroringPreferencesEntryLimit();
                            }
                            entryLimit.MaximumEntriesPerAthleteIncludingRelays = scoringPreference.ScoringPreferencesViewModel.MaximumEntriesPerAthleteIncludingRelays;
                            entryLimit.MaximumFieldEventEntriesPerAthletes = scoringPreference.ScoringPreferencesViewModel.MaximumFieldEventEntriesPerAthletes;
                            entryLimit.MaximumTrackEventEntriesPerAthleteIncludingRelay = scoringPreference.ScoringPreferencesViewModel.MaximumTrackEventEntriesPerAthleteIncludingRelay;
                            entryLimit.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            entryLimit.WarnIfEntryLimitsExceeds = scoringPreference.ScoringPreferencesViewModel.WarnIfEntryLimitsExceeds;
                            entryLimit.MaximumFieldEventEntriesPerAthletes = scoringPreference.ScoringPreferencesViewModel.MaximumFieldEventEntriesPerAthletes;
                            if (entryLimit.ScroringPreferencesEntryLimitID <= 0)
                            {
                                ctx.MeetSetupScroringPreferencesEntryLimits.Add(entryLimit);
                            }

                            MeetSetupScroringPreferencesEntriesResult scoringEntryresult = ctx.MeetSetupScroringPreferencesEntriesResults.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).FirstOrDefault();
                            if (scoringEntryresult == null)
                            {
                                scoringEntryresult = new MeetSetupScroringPreferencesEntriesResult();
                            }
                            scoringEntryresult.MeetSetupID = scoringPreference.TemplateOrMeetSetupID;
                            scoringEntryresult.ConvertHandEntryTimesToFAT = scoringPreference.ScoringPreferencesViewModel.ConvertHandEntryTimesToFAT;
                            scoringEntryresult.EntryNotesWithEachEntry = scoringPreference.ScoringPreferencesViewModel.EntryNotesWithEachEntry;
                            scoringEntryresult.RankResultByAgeGradedMarks = scoringPreference.ScoringPreferencesViewModel.RankResultByAgeGradedMarks;
                            scoringEntryresult.RecordWindReading = scoringPreference.ScoringPreferencesViewModel.RecordWindReading;
                            scoringEntryresult.RoundDownTheNearestEventCentimerer = scoringPreference.ScoringPreferencesViewModel.RoundDownTheNearestEventCentimerer;
                            scoringEntryresult.RoundUpResultToTenthofReport = scoringPreference.ScoringPreferencesViewModel.RoundUpResultToTenthofReport;
                            scoringEntryresult.ShowFinishedPlaceWithEachEntry = scoringPreference.ScoringPreferencesViewModel.ShowFinishedPlaceWithEachEntry;
                            scoringEntryresult.UseEntryDeclarationMethod = scoringPreference.ScoringPreferencesViewModel.UseEntryDeclarationMethod;
                            scoringEntryresult.WarnIfTimesMarksAreOutofRange = scoringPreference.ScoringPreferencesViewModel.WarnIfTimesMarksAreOutofRange;
                            if (scoringEntryresult.ScroringPreferencesEntriesResultID <= 0)
                            {
                                ctx.MeetSetupScroringPreferencesEntriesResults.Add(scoringEntryresult);
                            }

                            MeetScroringPreferencesFieldRelay fieldRelay = ctx.MeetScroringPreferencesFieldRelays.Where(p => p.MeetSetupID == scoringPreference.TemplateOrMeetSetupID).FirstOrDefault();
                            if (fieldRelay == null)
                            {
                                fieldRelay = new MeetScroringPreferencesFieldRelay();
                            }
                            fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam = scoringPreference.ScoringPreferencesViewModel.FieldRelayCanBeScoreWithUnderSizeTeam;
                            fieldRelay.MarksWillBeAddedForReamRanking = scoringPreference.ScoringPreferencesViewModel.MarksWillBeAddedForReamRanking;
                            fieldRelay.TimesWillBeAddedForTeamRanking = scoringPreference.ScoringPreferencesViewModel.TimesWillBeAddedForTeamRanking;
                            fieldRelay.FieldRelayCanBeScoreWithUnderSizeTeam = scoringPreference.ScoringPreferencesViewModel.FieldRelayCanBeScoreWithUnderSizeTeam;
                            if (fieldRelay.ScroringPreferencesFieldRelayID <= 0)
                            {
                                ctx.MeetScroringPreferencesFieldRelays.Add(fieldRelay);
                            }

                            ctx.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringPreferencesResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringPreferencesResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }


                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new ScoringPreferencesResult();
                Result.Result.ScoringPreferencesID = scoringPreference.TemplateOrMeetSetupID;
                Result.Result.AuthToken = null;
                return Result;


            }
        }

        public async Task<AuthorizedResponse<ScoringSetupResult>> UpdateAssignedScoringSetupMeet(ServiceRequest_ScoringSetup scoringSetup)
        {
            AuthorizedResponse<ScoringSetupResult> Result = new AuthorizedResponse<ScoringSetupResult>();
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        ctx.MeetScoringSetups.RemoveRange(ctx.MeetScoringSetups.Where(p => p.MeetSetupID == scoringSetup.TemplateOrMeetSetupID));
                        Result = await this.AssignScoringMeetTemplate(scoringSetup);

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new AuthorizedResponse<ScoringSetupResult>();
                        Result.Message = ex.Message;
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ScoringSetupResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }

                return Result;
            }
        }

        public async Task<AuthorizedResponse<DivisionRegionNamesResult>> UpdateDivisionRegionName(ServiceRequest_DivisionRegionNames divisionRegionNames)
        {
            using (Entities.Models.Entities ctx = new Entities.Models.Entities())
            {
                foreach (var divRegion in divisionRegionNames.DivisionRegionNameList)
                {
                    DivisionRegionName divisionRegionName = ctx.DivisionRegionNames.Where(p => p.DivisionRegionNameID == divRegion.DivisionRegionNameID).FirstOrDefault();
                    if (divisionRegionName == null)
                    {
                        divisionRegionName = new DivisionRegionName();
                    }
                    divisionRegionName.Code = divRegion.Code;
                    divisionRegionName.Calc = divRegion.Calc;
                    divisionRegionName.Division = divRegion.Division;
                    divisionRegionName.DivisionName = divRegion.DivisionName;
                    divisionRegionName.EndDate = divRegion.EndDate;
                    divisionRegionName.Operator = divRegion.Operator;
                    divisionRegionName.Range = divRegion.Range;
                    divisionRegionName.StartDate = divRegion.StartDate;
                    divisionRegionName.Entry = divRegion.Entry;
                    if (divisionRegionName.DivisionRegionNameID <= 0)
                    {
                        ctx.DivisionRegionNames.Add(divisionRegionName);
                    }
                }

                ctx.SaveChanges();
            }

            AuthorizedResponse<DivisionRegionNamesResult> Result = new AuthorizedResponse<DivisionRegionNamesResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new DivisionRegionNamesResult();
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<MeetSetupResult>> UpdateMeet(APIServiceRequest_MeetSetup CreateMeetRequest)
        {
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                Entities.Models.MeetSetup meetSetup = ctx.MeetSetups.Where(p => p.MeetSetupID == CreateMeetRequest.MeetSetupID).FirstOrDefault();
                if (meetSetup != null)
                {
                    meetSetup.MeetSetupName = CreateMeetRequest.MeetSetupName;
                    meetSetup.MeetSetupName2 = CreateMeetRequest.MeetSetupName2;
                    meetSetup.MeetSetupAgeUpDate = CreateMeetRequest.MeetSetupAgeUpDate;
                    meetSetup.MeetSetupStartDate = CreateMeetRequest.MeetSetupStartDate;
                    meetSetup.MeetSetupEndDate = CreateMeetRequest.MeetSetupEndDate;
                    meetSetup.MeetTypeID = CreateMeetRequest.MeetTypeID;
                    meetSetup.MeetArenaID = CreateMeetRequest.MeetArenaID;
                    meetSetup.MeetClassID = CreateMeetRequest.MeetClassID;
                    meetSetup.MeetKindID = CreateMeetRequest.MeetKindID;
                    meetSetup.BaseCountyID = CreateMeetRequest.MeetCountyID;
                    meetSetup.MeetStyleID = CreateMeetRequest.MeetStyleID;
                    meetSetup.UseDivisionBirthdateRange = CreateMeetRequest.UseDivisionBirthDateRange;
                    meetSetup.MeetTypeDivisionID = CreateMeetRequest.MeetTypeOptionID;
                    meetSetup.LinkTermToDivision = CreateMeetRequest.LinkTermToDivision;
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<MeetSetupResult> Result = new AuthorizedResponse<MeetSetupResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new MeetSetupResult();
            Result.Result.AuthToken = null;
            return Result;
        }
    }
}
