﻿using Athletics.Client.ViewModels;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.ViewModels;
using Athletics.MeetSetup.ViewModels.SeedPreferences;
using Athletics.MeetSetup.Views;
using Athletics.MeetSetup.Views.SeedPreferences;
using Athletics.SetupMeet.View.MeetSetup;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

namespace Athletics.MeetSetup
{
    public class MeetSetupModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public MeetSetupModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
            //this.container.TryResolve<IAssigPrefrencesViewModel>();
        }

        public void Initialize()
        {
            this.container.RegisterType<IMeetSetupStateController, MeetSetupStateController>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IMeetSetupView, MeetSetupView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IMeetSetupViewModel, MeetSetupViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IMeetSetupManage, MeetSetupManage>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IMeetSetupShellView, MeetSetupShellView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IMeetSetupShellViewModel, MeetSetupShellViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageMeetSetupView, ManageMeetSetupView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageMeetSetupViewModel, ManageMeetSetupViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IAthleteRelayPreferencesView, AthleteRelayPreferencesView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAthleteRelayPreferencesViewModel, AthleteRelayPreferencesViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<ISeedPreferencesView, SeedPreferencesView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISeedPreferencesViewModel, SeedPreferencesViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IStandardLanePrefrencesView, StandardLanePrefrencesView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IStandardLanePrefrencesViewModel, StandardLanePrefrencesViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IStandardAlleyPrefrencesView, StandardAlleyPrefrencesView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IStandardAlleyPrefrencesViewModel, StandardAlleyPrefrencesViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IWaterfallStartView, WaterfallStartView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IWaterfallStartViewModel, WaterfallStartViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAssigPrefrencesView, AssigPrefrencesView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAssigPrefrencesViewModel, AssigPrefrencesViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IEntryScoringPreferencesView, EntryScoringPreferencesView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IEntryScoringPreferencesViewModel, EntryScoringPreferencesViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IEntryScoringPreferencesListView, EntryScoringPreferencesListView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IEntryScoringPreferencesListViewModel, EntryScoringPreferencesListViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IDobleDualMeetView, DobleDualMeetView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IDobleDualMeetViewModel, DobleDualMeetViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IScoringSetupListView, ScoringSetupListView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IScoringSetupView, ScoringSetupView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IScoringSetupListViewModel, ScoringSetupListViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IScoringSetupViewModel, ScoringSetupViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IDivisionRegionNamesViewModel, DivisionRegionNamesViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IDivisionRegionNamesView, DivisionRegionNamesView>(new ContainerControlledLifetimeManager());

            this.regionManager.RegisterViewWithRegion("MeetSetupRegion", typeof(ManageMeetSetupView));
            this.regionManager.RegisterViewWithRegion("MeetSetupIntrectionRegion", typeof(MeetSetupView));
            this.regionManager.RegisterViewWithRegion("AthletePreferencesRegion", typeof(AthleteRelayPreferencesView));
            this.regionManager.RegisterViewWithRegion("SeedRegion", typeof(SeedPreferencesView));

        }
    }
}
