﻿using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Athletics.SetupMeet.View.AssigPrefrences
{
    /// <summary>
    /// Interaction logic for TemplateNameView.xaml
    /// </summary>
    public partial class TemplateNameView : Window, INotifyPropertyChanged
    {
        public TemplateNameView()
        {
            InitializeComponent();
            this.DataContext = this;
            _PART_CLOSE = new DelegateCommand(PART_CLOSE_Click);
        }
        private string _TemplateName = string.Empty;
        public string TemplateName
        {
            get { return _TemplateName; }
            set
            {
                _TemplateName = value;
                OnPropertyChanged("TemplateName");
            }
        }
        private void PART_CLOSE_Click()
        {

            PART_CLOSE_Click(this, null);
        }
        private readonly ICommand _PART_CLOSE;
        public ICommand PART_CLOSE
        {
            get
            {
                return _PART_CLOSE;
            }
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.DragMove();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);
        }
    }
}
