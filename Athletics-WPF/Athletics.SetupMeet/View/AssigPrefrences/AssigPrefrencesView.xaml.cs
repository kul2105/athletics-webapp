﻿using Athletics.MeetSetup.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Linq;
using Athletics.SetupMeet.Common;

namespace Athletics.MeetSetup.Views
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class AssigPrefrencesView : Window, IAssigPrefrencesView
    {
        public AssigPrefrencesView()
        {
            InitializeComponent();
            //   Loaded += (sender, e) => { MeetNameTextBox.Focus(); };
        }

        public void ResetFocus()
        {
            //  MeetNameTextBox.Focus();
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            Window window = ((Button)sender).TemplatedParent as Window;
            if (window != null)
                window.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);

        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }

        private void ListView_Drop(object sender, DragEventArgs e)
        {
            if (e.OriginalSource.GetType() == typeof(TextBlock))
            {
                SetupMeet.Model.MeetSetupModel meetSetup = ((TextBlock)e.OriginalSource).DataContext as SetupMeet.Model.MeetSetupModel;
                if (meetSetup != null)
                {
                    GlobalEvent.RaiseTargetControlLoaded(meetSetup);
                }
            }


        }
    }
}
