﻿namespace Athletics.MeetSetup.Views
{
    public interface IDobleDualMeetView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
