﻿namespace Athletics.MeetSetup.Views.SeedPreferences
{
    public interface IStandardAlleyPrefrencesView
    {
        object DataContext { get; set; }
        void ResetFocus();
        void CloseView();


    }

}
