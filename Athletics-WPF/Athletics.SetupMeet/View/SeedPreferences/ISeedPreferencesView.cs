﻿namespace Athletics.MeetSetup.Views.SeedPreferences
{
    public interface ISeedPreferencesView
    {
        object DataContext { get; set; }
        void ResetFocus();
        void CloseView();

    }

}
