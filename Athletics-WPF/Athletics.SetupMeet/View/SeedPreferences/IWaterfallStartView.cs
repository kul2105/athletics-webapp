﻿namespace Athletics.MeetSetup.Views.SeedPreferences
{
    public interface IWaterfallStartView
    {
        object DataContext { get; set; }
        void ResetFocus();
        void CloseView();

    }

}
