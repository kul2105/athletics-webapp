﻿namespace Athletics.MeetSetup.Views.SeedPreferences
{
    public interface IStandardLanePrefrencesView
    {
        object DataContext { get; set; }
        void ResetFocus();
        void CloseView();

    }

}
