﻿using Athletics.Entities.Models;
using Athletics.MeetSetup.ViewModels;
using Athletics.SetupMeet.Common;
using Athletics.SetupMeet.Model;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.MeetSetup.Views.SeedPreferences
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>

    public partial class SeedPreferencesView : UserControl, ISeedPreferencesView
    {

        public SeedPreferencesView()
        {
            InitializeComponent();
            //Loaded += (sender, e) => { if(TargetControlLoaded!=null) TargetControlLoaded(this.dataGrid2); };
        }

        public void CloseView()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                this.Visibility = Visibility.Collapsed;
            }));
        }

        public void ResetFocus()
        {
            //  MeetNameTextBox.Focus();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);

        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }
        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            MeetSetupViewModel vm = DataContext as MeetSetupViewModel;
            if (vm != null)
            {
                vm.IsInCloseMode = true;
            }
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.DragMove();

        }

        private void DataGrid2_Drop(object sender, DragEventArgs e)
        {
            if (e.OriginalSource.GetType() == typeof(TextBlock))
            {
                LaneDetailModel lane = ((TextBlock)e.OriginalSource).DataContext as LaneDetailModel;
                if (lane != null)
                {
                    GlobalEvent.RaiseTargetControlLoaded(lane);
                }
            }
        }
    }
}
