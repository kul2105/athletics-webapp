﻿using Athletics.MeetSetup.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Linq;

namespace Athletics.MeetSetup.Views
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class AthleteRelayPreferencesView : UserControl, IAthleteRelayPreferencesView
    {
        public AthleteRelayPreferencesView()
        {
            InitializeComponent();
            //   Loaded += (sender, e) => { MeetNameTextBox.Focus(); };
        }

        public void ResetFocus()
        {
            //  MeetNameTextBox.Focus();
        }

        private void UserControl_Drop(object sender, DragEventArgs e)
        {

        }
        //private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Visibility = Visibility.Hidden;
        //}
        //private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{

        //    if (e.ClickCount == 2)
        //    {
        //        WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
        //    }
        //    else
        //    {
        //        var window = (Window)((FrameworkElement)sender).TemplatedParent;
        //        window.DragMove();
        //    }

        //}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);

        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }
        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            MeetSetupViewModel vm = DataContext as MeetSetupViewModel;
            if (vm != null)
            {
                vm.IsInCloseMode = true;
            }
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            // e.Cancel = true;
            window.Visibility = Visibility.Hidden;
            // window.Close();
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.DragMove();

        }

        //private void CheckBox_Checked(object sender, RoutedEventArgs e)
        //{
        //    CheckBox checkBox = sender as CheckBox;
        //    AthleteRelayPreferencesViewModel vm = DataContext as AthleteRelayPreferencesViewModel;
        //    switch (checkBox.Tag)
        //    {
        //        case "MeetAthletePreference":
        //            vm.AthletePreferencesOptions.Where(p => p.Display == checkBox.Content.ToString()).ToList().ForEach(p => p.IsSelected = true);
        //            break;

        //        case "MeetCompetitorNumber":
        //            vm.CompetitorNumbersOptions.Where(p => p.Display == checkBox.Content.ToString()).ToList().ForEach(p => p.IsSelected = true);
        //            break;

        //        case "MeetRelayPreference":
        //            vm.RelayPreferencesOptions.Where(p => p.Display == checkBox.Content.ToString()).ToList().ForEach(p => p.IsSelected = true);
        //            break;

        //        default:
        //            break;
        //    }
        //}

        //private void RadioButton_Checked(object sender, RoutedEventArgs e)
        //{
        //    RadioButton radioButton = sender as RadioButton;
        //    AthleteRelayPreferencesViewModel vm = DataContext as AthleteRelayPreferencesViewModel;
        //    switch (radioButton.GroupName)
        //    {
        //        case "MeetAthletePreference":
        //            vm.AthletePreferencesOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.IsSelected = false);
        //            vm.AthletePreferencesOptions.Where(p => p.Display == radioButton.Content.ToString()).ToList().ForEach(p => p.IsSelected = true);
        //            break;

        //        case "MeetCompetitorNumber":
        //            vm.CompetitorNumbersOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.IsSelected = false);
        //            vm.CompetitorNumbersOptions.Where(p => p.Display == radioButton.Content.ToString()).ToList().ForEach(p => p.IsSelected = true);
        //            break;

        //        case "MeetRelayPreference":
        //            vm.RelayPreferencesOptions.Where(p => p.GroupName == radioButton.GroupName).ToList().ForEach(p => p.IsSelected = false);
        //            vm.RelayPreferencesOptions.Where(p => p.Display == radioButton.Content.ToString()).ToList().ForEach(p => p.IsSelected = true);
        //            break;

        //        default:
        //            break;
        //    }
        //}
    }
}
