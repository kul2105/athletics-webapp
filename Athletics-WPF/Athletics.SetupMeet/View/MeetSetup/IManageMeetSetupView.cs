﻿namespace Athletics.SetupMeet.View.MeetSetup
{
    public interface IManageMeetSetupView
    {
        object DataContext { get; set; }
        void ResetFocus();

    }
}
