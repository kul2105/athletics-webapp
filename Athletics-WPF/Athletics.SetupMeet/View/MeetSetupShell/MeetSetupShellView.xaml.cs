﻿using System.Windows.Controls;

namespace Athletics.MeetSetup.Views
{
    /// <summary>
    /// Interaction logic for MeetSetupShellView.xaml
    /// </summary>
    public partial class MeetSetupShellView : UserControl, IMeetSetupShellView
    {
        public MeetSetupShellView()
        {
            InitializeComponent();
        }
    }
}
