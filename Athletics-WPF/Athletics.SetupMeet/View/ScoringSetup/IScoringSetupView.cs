﻿using Athletics.ControlLibrary.DataGrid;

namespace Athletics.MeetSetup.Views
{
    public interface IScoringSetupView
    {
        object DataContext { get; set; }
        void ResetFocus();
        AthleticsGrid RecordGrid { get; set; }

    }

}
