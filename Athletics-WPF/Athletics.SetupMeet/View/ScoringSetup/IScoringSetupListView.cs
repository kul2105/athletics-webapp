﻿namespace Athletics.MeetSetup.Views
{
    public interface IScoringSetupListView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
