﻿using Athletics.MeetSetup.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Linq;
using Athletics.SetupMeet.Common;
using Athletics.ControlLibrary.DataGrid;

namespace Athletics.MeetSetup.Views
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class ScoringSetupView : Window, IScoringSetupView
    {
        public ScoringSetupView()
        {
            InitializeComponent();
            //   Loaded += (sender, e) => { MeetNameTextBox.Focus(); };
        }

        public AthleticsGrid RecordGrid { get => this.dataGrid2; set => this.dataGrid2 = value; }
        public void ResetFocus()
        {
            //  MeetNameTextBox.Focus();
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ScoringSetupViewModel vm = this.DataContext as ScoringSetupViewModel;
            if(vm!=null)
            {
                vm.IsInCloseMode = true;
            }
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);

        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }

    }
}
