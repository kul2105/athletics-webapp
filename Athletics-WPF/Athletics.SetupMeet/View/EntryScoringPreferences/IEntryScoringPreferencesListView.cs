﻿namespace Athletics.MeetSetup.Views
{
    public interface IEntryScoringPreferencesListView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
