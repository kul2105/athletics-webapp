﻿using Athletics.ControlLibrary.DataGrid;

namespace Athletics.MeetSetup.Views
{
    public interface IDivisionRegionNamesView
    {
        object DataContext { get; set; }
        void ResetFocus();
        AthleticsGrid RecordGrid { get; set; }

    }

}
