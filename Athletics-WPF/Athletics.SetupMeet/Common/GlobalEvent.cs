﻿using Athletics.MeetSetup.SetupMeetManager;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.SetupMeet.Common
{
    public class GlobalEvent
    {
        public delegate void TargetControlLoadedDelegate(object targetObject);
        public static event TargetControlLoadedDelegate TargetControlLoaded;
        public static void RaiseTargetControlLoaded(object targetObject)
        {
            if (TargetControlLoaded != null)
                TargetControlLoaded(targetObject);
        }

        public static IMeetSetupManage MeetSetupManage { get; set; }
    }
}
