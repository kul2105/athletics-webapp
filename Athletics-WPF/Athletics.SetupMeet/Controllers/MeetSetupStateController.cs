﻿using Microsoft.Practices.Prism.Mvvm;
using System;

namespace Athletics.MeetSetup.Controllers
{
    public class MeetSetupStateController : BindableBase, IMeetSetupStateController
    {
        public MeetSetupStateController()
        {
        }


        private e_MEET_STATE _MeetState = e_MEET_STATE.MEET_SETUP;
        public e_MEET_STATE MeetState
        {
            get
            {
                return _MeetState;
            }
            set
            {
                if (_MeetState != value)
                {
                    _MeetState = value;
                    OnPropertyChanged("MeetState");
                    switch (value)
                    {
                        case e_MEET_STATE.MEET_SETUP:
                            _sMeetState = "MEET_SETUP";
                            break;
                        case e_MEET_STATE.ATHLETE_RELAY_PREFERENCES:
                            _sMeetState = "ATHLETE_RELAY_PREFERENCES";
                            break;
                        case e_MEET_STATE.SEEDING_PREFERENCES:
                            _sMeetState = "ATHLETE_RELAY_PREFERENCES";
                            break;
                    }
                    OnPropertyChanged("sAuthenticationState");
                    Action<e_MEET_STATE> pAuthorizationStateChanged = MeetStateChanged;
                    if (pAuthorizationStateChanged != null)
                    {
                        pAuthorizationStateChanged(value);
                    }
                }
            }
        }


        private string _sMeetState;
        public string sMeetState
        {
            get { return _sMeetState; }
        }

        public event Action<e_MEET_STATE> MeetStateChanged;
    }
}
