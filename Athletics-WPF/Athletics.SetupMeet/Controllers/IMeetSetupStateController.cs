﻿using System;

namespace Athletics.MeetSetup.Controllers
{
    public enum e_MEET_STATE
    {
        MEET_SETUP,
        MEET_SETUPCOMPLETED,
        ATHLETE_RELAY_PREFERENCES,
        SEEDING_PREFERENCES,
        NONE
    }

    public interface IMeetSetupStateController
    {
        // For use by the view models
        e_MEET_STATE MeetState { get; set; }

        // For use by the views
        string sMeetState { get; }

        event Action<e_MEET_STATE> MeetStateChanged;
    }
}
