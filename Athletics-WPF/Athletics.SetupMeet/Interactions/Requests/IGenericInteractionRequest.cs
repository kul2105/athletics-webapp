﻿using System;

namespace Athletics.MeetSetup.Requests
{
    public interface IGenericInteractionRequest<T>
    {
        event EventHandler<GenericInteractionRequestEventArgs<T>> Raised;
    }
}
