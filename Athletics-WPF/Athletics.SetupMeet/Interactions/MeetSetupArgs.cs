﻿using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;

namespace Athletics.MeetSetup.Interactions
{
    public class MeetSetupArgs : Confirmation
    {
        public MeetSetupArgs()
        {
        }

        public MeetSetupModel SelectedMeetSetup { get; set; }
    }
}
