﻿using Athletics.MeetSetup.Requests;
using Athletics.SetupMeet.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Unity;
using System;
using System.Windows.Input;
using Athletics.MeetSetup.Views;
using System.Windows;
using Athletics.MeetSetup.ViewModels;
using Athletics.ModuleCommon;

namespace Athletics.MeetSetup.Interactions
{
    public class MeetSetupInteractionViewModel
    {
        private IUnityContainer Container;
        public MeetSetupInteractionViewModel()
        {
            _RaiseMeetSetupInteractionCommand = new DelegateCommand<MeetSetupModel>((MeetSetup) =>
            {
                RaiseManageUsersInteraction(MeetSetup);
            });
            Container = (UnityContainer)Application.Current.Resources["IoC"]; ;
        }
        private InteractionRequest<MeetSetupArgs> _ManageMeetSetupRequest = new InteractionRequest<MeetSetupArgs>();
        public InteractionRequest<MeetSetupArgs> ManageMeetSetupRequest
        {
            get { return _ManageMeetSetupRequest; }
        }

        MeetSetupArgs ManageUsersArgs = new MeetSetupArgs();
        private void RaiseManageUsersInteraction(MeetSetupModel meetSetup)
        {
            ManageUsersArgs.SelectedMeetSetup = meetSetup;
            IGenericInteractionView<MeetSetupModel> view = Container.Resolve<IMeetSetupViewModel>() as IGenericInteractionView<MeetSetupModel>;
            if (view != null)
            {
                view.SetEntity(meetSetup);
            }
            if (meetSetup == null)
            {
                ManageUsersArgs.Title = "Create Meet-Setup";
            }
            else
            {
                ManageUsersArgs.Title = "Update Meet-Setup";
            }
            _ManageMeetSetupRequest.Raise(ManageUsersArgs, (x) =>
            {
                if (x.Confirmed)
                {

                    ManageUsersArgs.SelectedMeetSetup = view.GetEntity();
                }
            });
        }

        private void CancelCallback()
        {
            throw new NotImplementedException();
        }

        private void GetMeetCallback(MeetSetupModel obj)
        {
            throw new NotImplementedException();
        }

        private ICommand _RaiseMeetSetupInteractionCommand;
        public ICommand RaiseMeetSetupInteractionCommand
        {
            get { return _RaiseMeetSetupInteractionCommand; }
        }
    }
}
