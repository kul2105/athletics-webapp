﻿//Author: Gerard Castelló Viader
//Date: 10/16/2011


namespace Athletics.MeetSetup.Generic
{
    public class GenericAdapter<T> : IGenericAdapter<T>
    {
        private readonly IGenericViewModel<T> viewModel;

        public GenericAdapter()
        {
            this.viewModel = new GenericViewModel<T>();
        }

        public IGenericViewModel<T> ViewModel
        {
            get { return this.viewModel; }
        }

        public void SetEntity(T entity)
        {
            this.ViewModel.SetEntity(entity);
        }

        public T GetEntity()
        {
            return this.ViewModel.GetEntity();
        }
    }
}
