﻿
using Athletics.MeetSetup.Views;
using Athletics.ModuleCommon;
using System.ComponentModel;
namespace Athletics.MeetSetup.Generic
{
    public interface IGenericViewModel<T> : IGenericInteractionView<T>, INotifyPropertyChanged
    {
        T Entity { get; set; }
    }
}
