﻿
namespace Athletics.MeetSetup.Generic
{
    public interface IGenericAdapter<T>
    {
        IGenericViewModel<T> ViewModel { get; }
    }
}
