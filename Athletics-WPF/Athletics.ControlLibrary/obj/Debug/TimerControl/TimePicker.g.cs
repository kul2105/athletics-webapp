﻿#pragma checksum "..\..\..\TimerControl\TimePicker.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "B5AC85618756075D4B8C733B1E5E1C779E552891"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Athletics.ControlLibrary;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Athletics.ControlLibrary {
    
    
    /// <summary>
    /// TimePicker
    /// </summary>
    public partial class TimePicker : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 21 "..\..\..\TimerControl\TimePicker.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock AddHoursTextBox;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\TimerControl\TimePicker.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button HourUpButton;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\TimerControl\TimePicker.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button HourDownButton;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\TimerControl\TimePicker.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock AddMinutesTextBox;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\TimerControl\TimePicker.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MinutesUpButton;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\TimerControl\TimePicker.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MinutesDownButton;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\TimerControl\TimePicker.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AmPmComboBox;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Athletics.ControlLibrary;component/timercontrol/timepicker.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\TimerControl\TimePicker.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.AddHoursTextBox = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.HourUpButton = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\..\TimerControl\TimePicker.xaml"
            this.HourUpButton.Click += new System.Windows.RoutedEventHandler(this.HourUpButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 3:
            this.HourDownButton = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\TimerControl\TimePicker.xaml"
            this.HourDownButton.Click += new System.Windows.RoutedEventHandler(this.HourDownButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 4:
            this.AddMinutesTextBox = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.MinutesUpButton = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\..\TimerControl\TimePicker.xaml"
            this.MinutesUpButton.Click += new System.Windows.RoutedEventHandler(this.MinutesUpButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 6:
            this.MinutesDownButton = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\TimerControl\TimePicker.xaml"
            this.MinutesDownButton.Click += new System.Windows.RoutedEventHandler(this.MinutesDownButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 7:
            this.AmPmComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

