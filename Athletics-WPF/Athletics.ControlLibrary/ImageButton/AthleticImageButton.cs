﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Athletics.ControlLibrary.AthleticImageButton
{
    public class AthleticImageButton : Button
    {
        static AthleticImageButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AthleticImageButton), new FrameworkPropertyMetadata(typeof(AthleticImageButton)));
        }

        #region Dependency Properties

        //public double ImageSize
        //{
        //    get { return (double)GetValue(ImageSizeProperty); }
        //    set { SetValue(ImageSizeProperty, value); }
        //}

        //public static readonly DependencyProperty ImageSizeProperty =
        //    DependencyProperty.Register("ImageSize", typeof(double), typeof(AthleticImageButton),
        //    new FrameworkPropertyMetadata(30.0, FrameworkPropertyMetadataOptions.AffectsRender));

        public string NormalImage
        {
            get { return (string)GetValue(NormalImageProperty); }
            set { SetValue(NormalImageProperty, value); }
        }

        public static readonly DependencyProperty NormalImageProperty =
            DependencyProperty.Register("NormalImage", typeof(ImageSource), typeof(AthleticImageButton),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        //public string HoverImage
        //{
        //    get { return (string)GetValue(HoverImageProperty); }
        //    set { SetValue(HoverImageProperty, value); }
        //}

        //public static readonly DependencyProperty HoverImageProperty =
        //    DependencyProperty.Register("HoverImage", typeof(ImageSource), typeof(AthleticImageButton),
        //    new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender, ImageSourceChanged));

        //public string PressedImage
        //{
        //    get { return (string)GetValue(PressedImageProperty); }
        //    set { SetValue(PressedImageProperty, value); }
        //}

        //public static readonly DependencyProperty PressedImageProperty =
        //    DependencyProperty.Register("PressedImage", typeof(ImageSource), typeof(AthleticImageButton),
        //    new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender, ImageSourceChanged));

        //public string DisabledImage
        //{
        //    get { return (string)GetValue(DisabledImageProperty); }
        //    set { SetValue(DisabledImageProperty, value); }
        //}

        //public static readonly DependencyProperty DisabledImageProperty =
        //    DependencyProperty.Register("DisabledImage", typeof(ImageSource), typeof(AthleticImageButton),
        //    new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender, ImageSourceChanged));

        //private static void ImageSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        //{
        //    //Application.GetResourceStream(new Uri("pack://application:,,," + (string)e.NewValue));
        //}

        #endregion
    }
}
