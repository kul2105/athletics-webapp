﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Athletics.ControlLibrary.TabControl
{
    public class AthleticsTab :System.Windows.Controls.TabControl
    {
        private DispatcherTimer timer;

        // this event will be fired when the user clicks to
        // change the index of the tab control
        public static readonly RoutedEvent SelectionChangingEvent = EventManager.RegisterRoutedEvent(
            "SelectionChanging", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(AthleticsTab));

        public event RoutedEventHandler SelectionChanging
        {
            add { AddHandler(SelectionChangingEvent, value); }
            remove { RemoveHandler(SelectionChangingEvent, value); }
        }

        public AthleticsTab()
        {
            DefaultStyleKey = typeof(AthleticsTab);
        }

        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            // I've put this code here to check if the user has
            // selected a patient before trying to change the index
            // for the case that the user has not choose any patients from the list
            // the Dispatcher will not invoke because we need the Patient
            // info for the other tabs to record !!
            //Patient p = ApplicationHelper.GetProperty<Patient>("SelectedPatient");
            //if (p == null)
            //    return;

            this.Dispatcher.BeginInvoke(
                (Action)delegate
                {
                    this.RaiseSelectionChangingEvent();

                    this.StopTimer();

                    this.timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 500) };

                    EventHandler handler = null;
                    handler = (sender, args) =>
                    {
                        this.StopTimer();
                        base.OnSelectionChanged(e);
                    };
                    this.timer.Tick += handler;
                    this.timer.Start();
                });
        }

        // This method raises the event to change the tab items
        private void RaiseSelectionChangingEvent()
        {
            var args = new RoutedEventArgs(SelectionChangingEvent);
            RaiseEvent(args);
        }

        private void StopTimer()
        {
            if (this.timer != null)
            {
                this.timer.Stop();
                this.timer = null;
            }
        }
    }
}
