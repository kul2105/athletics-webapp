﻿using System.Windows;


namespace Athletics.ControlLibrary.DialogWindow
{
    public interface IModalDialog
    {
        void BindViewModel<TViewModel>(TViewModel viewModel); //bind to viewModel
        void BindView(Window userControl); //bind to viewModel

        void ShowDialog();  //show the modal window 

        void Close();  //close the dialog   
    }
}
