﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.ControlLibrary
{
    public class CollectionItemsChangedEventArgs : EventArgs
    {
        /// <summary>
        /// The collection of items that changed.
        /// </summary>
        private ICollection items = null;

        public CollectionItemsChangedEventArgs(ICollection items)
        {
            this.items = items;
        }

        /// <summary>
        /// The collection of items that changed.
        /// </summary>
        public ICollection Items
        {
            get
            {
                return items;
            }
        }
    }
}
