USE [master]
GO
/****** Object:  Database [AthleticsDB]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE DATABASE [AthleticsDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AthleticsDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\AthleticsDB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'AthleticsDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\AthleticsDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [AthleticsDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AthleticsDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AthleticsDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AthleticsDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AthleticsDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AthleticsDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AthleticsDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [AthleticsDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [AthleticsDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AthleticsDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AthleticsDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AthleticsDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AthleticsDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AthleticsDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AthleticsDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AthleticsDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AthleticsDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AthleticsDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AthleticsDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AthleticsDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AthleticsDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AthleticsDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AthleticsDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AthleticsDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AthleticsDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [AthleticsDB] SET  MULTI_USER 
GO
ALTER DATABASE [AthleticsDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AthleticsDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AthleticsDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AthleticsDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [AthleticsDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [AthleticsDB]
GO
/****** Object:  Table [dbo].[ApplicationMenu]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationMenu](
	[MenuID] [int] NOT NULL,
	[MenuName] [nvarchar](50) NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [PK_ApplicationMenus] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AthletePreferences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AthletePreferences](
	[AthletePreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[AthletePreferencesName] [nvarchar](250) NULL,
 CONSTRAINT [PK_AthletePreferences] PRIMARY KEY CLUSTERED 
(
	[AthletePreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AthleteRelayPreferencesTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AthleteRelayPreferencesTransaction](
	[AthleteRelayPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[PreferencesTransactionGroupID] [nvarchar](250) NULL,
	[CompetitorNumberTransactionGroupID] [nvarchar](250) NULL,
	[RelayPreferencesTransactionGroupID] [nvarchar](250) NULL,
 CONSTRAINT [PK_AthleteRelayPreferencesTransaction_1] PRIMARY KEY CLUSTERED 
(
	[AthleteRelayPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AthleticPreferencesTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AthleticPreferencesTransaction](
	[AthleticPreferencesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[AthleticPreferencesGroupID] [nvarchar](250) NOT NULL,
	[AthleticPreferencesID] [int] NULL,
 CONSTRAINT [PK_AthleticPreferencesTransaction_1] PRIMARY KEY CLUSTERED 
(
	[AthleticPreferencesTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BaseCounty]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BaseCounty](
	[BaseCountyID] [int] IDENTITY(1,1) NOT NULL,
	[BaseCountyName] [nvarchar](250) NULL,
 CONSTRAINT [PK_BaseCounties] PRIMARY KEY CLUSTERED 
(
	[BaseCountyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompetitorNumbers]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompetitorNumbers](
	[CompetitorNumberID] [int] IDENTITY(1,1) NOT NULL,
	[CompetitorNumbersName] [nvarchar](250) NULL,
	[IsSingleSelectionGroup] [bit] NOT NULL,
 CONSTRAINT [PK_CompetitorNumbers] PRIMARY KEY CLUSTERED 
(
	[CompetitorNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompetitorNumbersTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompetitorNumbersTransaction](
	[CompetitorNumbersTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[CompetitorNumbersGroupID] [nvarchar](250) NOT NULL,
	[CompetitorNumbersID] [int] NULL,
 CONSTRAINT [PK_CompetitorNumbersTransaction_1] PRIMARY KEY CLUSTERED 
(
	[CompetitorNumbersTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DualMeetAssignmentTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DualMeetAssignmentTransaction](
	[DualMeetAssignmentTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[StrictAssignmentAllHeats] [bit] NULL,
	[StrictAssignmentFastestHeatOnly] [bit] NULL,
	[UseLaneAssignmentsForInLaneRacesOnly] [bit] NULL,
	[UserLaneOrPositionAssignmentsAbove] [bit] NULL,
	[AlternamtUserOfUnAssignedLane] [bit] NULL,
	[DualMeetGroupID] [nvarchar](250) NULL,
 CONSTRAINT [PK_DualMeetAssignmentTransaction] PRIMARY KEY CLUSTERED 
(
	[DualMeetAssignmentTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DualMeetTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DualMeetTransaction](
	[DualMeetTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[DualMeetGroupID] [nvarchar](250) NULL,
	[LaneNumber] [int] NULL,
	[SchoolID] [int] NULL,
 CONSTRAINT [PK_DualMeetTransaction] PRIMARY KEY CLUSTERED 
(
	[DualMeetTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LaneDetail]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LaneDetail](
	[LaneID] [int] IDENTITY(1,1) NOT NULL,
	[LaneNumber] [int] NULL,
	[SchoolID] [int] NULL,
 CONSTRAINT [PK_LaneDetails] PRIMARY KEY CLUSTERED 
(
	[LaneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetArena]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetArena](
	[MeetArenaID] [int] IDENTITY(1,1) NOT NULL,
	[MeetArenaName] [nvarchar](250) NULL,
 CONSTRAINT [PK_MeetArenas] PRIMARY KEY CLUSTERED 
(
	[MeetArenaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetClass]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetClass](
	[MeetClassID] [int] IDENTITY(1,1) NOT NULL,
	[MeetClassName] [nvarchar](250) NULL,
 CONSTRAINT [PK_MeetClasses] PRIMARY KEY CLUSTERED 
(
	[MeetClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetKind]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetKind](
	[MeetKindID] [int] IDENTITY(1,1) NOT NULL,
	[MeetKindName] [nvarchar](50) NULL,
 CONSTRAINT [PK_MeetKinds] PRIMARY KEY CLUSTERED 
(
	[MeetKindID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetup]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetup](
	[MeetSetupID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupName] [nvarchar](250) NULL,
	[MeetSetupName2] [nvarchar](250) NULL,
	[MeetSetupStartDate] [date] NULL,
	[MeetSetupEndDate] [date] NULL,
	[MeetSetupAgeUpDate] [date] NULL,
	[MeetKindID] [int] NULL,
	[MeetClassID] [int] NULL,
	[MeetTypeID] [int] NULL,
	[BaseCountyID] [int] NULL,
	[MeetArenaID] [int] NULL,
	[MeetStyleID] [int] NULL,
	[UseDivisionBirthdateRange] [bit] NULL,
	[LinkTermToDivision] [bit] NULL,
	[MeetTypeDivisionID] [int] NULL,
 CONSTRAINT [PK_MeetSetup] PRIMARY KEY CLUSTERED 
(
	[MeetSetupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupAthletePreferences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupAthletePreferences](
	[MeetSetupAthletePreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[AthletePreferencesID] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupAthletePreferences] PRIMARY KEY CLUSTERED 
(
	[MeetSetupAthletePreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupCompetitorNumber]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupCompetitorNumber](
	[MeetCompetitorNumberID] [int] IDENTITY(1,1) NOT NULL,
	[CompetitorNumberID] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetCompetitorNumber] PRIMARY KEY CLUSTERED 
(
	[MeetCompetitorNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupDualMeet]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupDualMeet](
	[MeetSetupDualMeetID] [int] IDENTITY(1,1) NOT NULL,
	[LaneNumber] [int] NULL,
	[SchoolID] [int] NULL,
	[MeetSetupID] [int] NULL,
	[StrictAssignmentAllHeats] [bit] NULL,
	[StrictAssignmentFastestHeatOnly] [bit] NULL,
	[UseLaneAssignmentsForInLaneRacesOnly] [bit] NULL,
	[UserLaneOrPositionAssignmentsAbove] [bit] NULL,
	[AlternamtUserOfUnAssignedLane] [bit] NULL,
 CONSTRAINT [PK_MeetSetupDualMeet] PRIMARY KEY CLUSTERED 
(
	[MeetSetupDualMeetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupRelayPreferences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupRelayPreferences](
	[MeetSetupRelayPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[RelayPreferencesID] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupRelayPreferences] PRIMARY KEY CLUSTERED 
(
	[MeetSetupRelayPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupRendomizationRule]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupRendomizationRule](
	[MeetSetupRendomizationRuleID] [int] IDENTITY(1,1) NOT NULL,
	[TimedFinalEvents] [nvarchar](50) NULL,
	[RoundFirstMultipleRound] [nvarchar](50) NULL,
	[RoundSecondThirdAndForth] [nvarchar](50) NULL,
	[CloseGap] [bit] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupRendomizationRule] PRIMARY KEY CLUSTERED 
(
	[MeetSetupRendomizationRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetups]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetups](
	[MeetSetupID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupName] [nvarchar](250) NULL,
	[MeetSetupName2] [nvarchar](250) NULL,
	[MeetSetupStartDate] [datetime] NULL,
	[MeetSetupEndDate] [datetime] NULL,
	[MeetSetupAgeUpDate] [datetime] NULL,
	[MeetKindID] [int] NULL,
	[MeetClassID] [int] NULL,
	[MeetTypeID] [int] NULL,
	[BaseCountyID] [int] NULL,
	[MeetArenaID] [int] NULL,
	[MeetStyleID] [int] NULL,
	[UseDivisionBirthdateRange] [bit] NULL,
	[LinkTermToDivision] [bit] NULL,
	[MeetTypeDivisionID] [int] NULL,
 CONSTRAINT [PK_MeetSetups] PRIMARY KEY CLUSTERED 
(
	[MeetSetupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupSeedingRules]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupSeedingRules](
	[MeetSetupSeedingRulesID] [int] IDENTITY(1,1) NOT NULL,
	[AllowForeignAthletesInFinal] [bit] NULL,
	[AllowExhibitionAthletesInFinal] [bit] NULL,
	[SeedExhibitionAthletesLast] [bit] NULL,
	[ApplyNCAARule] [bit] NULL,
	[UseSpecialRandomSelectMethod] [bit] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupSeedingRules] PRIMARY KEY CLUSTERED 
(
	[MeetSetupSeedingRulesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupStandardLanePrefrences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupStandardLanePrefrences](
	[MeetSetupStandardLanePrefrencesID] [int] IDENTITY(1,1) NOT NULL,
	[RowZeroColumnFirstValue] [smallint] NULL,
	[RowZeroColumnSecondValue] [smallint] NULL,
	[RowZeroColumnThirdValue] [smallint] NULL,
	[RowZeroColumnForthValue] [smallint] NULL,
	[RowZeroColumnFifthValue] [smallint] NULL,
	[RowZeroColumnSixthValue] [smallint] NULL,
	[RowZeroColumnSeventhValue] [smallint] NULL,
	[RowZeroColumnEightthValue] [smallint] NULL,
	[RowZeroColumnNinthValue] [smallint] NULL,
	[RowZeroColumnTenthValue] [smallint] NULL,
	[RowOneColumnFirstValue] [smallint] NULL,
	[RowOneColumnSecondValue] [smallint] NULL,
	[RowOneColumnThirdValue] [smallint] NULL,
	[RowOneColumnForthValue] [smallint] NULL,
	[RowOneColumnFifthValue] [smallint] NULL,
	[RowOneColumnSixthValue] [smallint] NULL,
	[RowOneColumnSeventhValue] [smallint] NULL,
	[RowOneColumnEightthValue] [smallint] NULL,
	[RowOneColumnNinthValue] [smallint] NULL,
	[RowOneColumnTenthValue] [smallint] NULL,
	[RowSecondColumnFirstValue] [smallint] NULL,
	[RowSecondColumnSecondValue] [smallint] NULL,
	[RowSecondColumnThirdValue] [smallint] NULL,
	[RowSecondColumnForthValue] [smallint] NULL,
	[RowSecondColumnFifthValue] [smallint] NULL,
	[RowSecondColumnSixthValue] [smallint] NULL,
	[RowSecondColumnSeventhValue] [smallint] NULL,
	[RowSecondColumnEightthValue] [smallint] NULL,
	[RowSecondColumnNinthValue] [smallint] NULL,
	[RowSecondColumnTenthValue] [smallint] NULL,
	[RowThirdColumnFirstValue] [smallint] NULL,
	[RowThirdColumnSecondValue] [smallint] NULL,
	[RowThirdColumnThirdValue] [smallint] NULL,
	[RowThirdColumnForthValue] [smallint] NULL,
	[RowThirdColumnFifthValue] [smallint] NULL,
	[RowThirdColumnSixthValue] [smallint] NULL,
	[RowThirdColumnSeventhValue] [smallint] NULL,
	[RowThirdColumnEightthValue] [smallint] NULL,
	[RowThirdColumnNinthValue] [smallint] NULL,
	[RowThridColumnTenthValue] [smallint] NULL,
	[RowForthColumnFirstValue] [smallint] NULL,
	[RowForthColumnSecondValue] [smallint] NULL,
	[RowForthColumnThirdValue] [smallint] NULL,
	[RowForthColumnForthValue] [smallint] NULL,
	[RowForthColumnFifthValue] [smallint] NULL,
	[RowForthColumnSixthValue] [smallint] NULL,
	[RowForthColumnSeventhValue] [smallint] NULL,
	[RowForthColumnEightthValue] [smallint] NULL,
	[RowForthColumnNinthValue] [smallint] NULL,
	[RowForthColumnTenthValue] [smallint] NULL,
	[RowFifthColumnFirstValue] [smallint] NULL,
	[RowFifthColumnSecondValue] [smallint] NULL,
	[RowFifthColumnThirdValue] [smallint] NULL,
	[RowFifthColumnForthValue] [smallint] NULL,
	[RowFifthColumnFifthValue] [smallint] NULL,
	[RowFifthColumnSixthValue] [smallint] NULL,
	[RowFifthColumnSeventhValue] [smallint] NULL,
	[RowFifthColumnEightthValue] [smallint] NULL,
	[RowFifthColumnNinthValue] [smallint] NULL,
	[RowFifthColumnTenthValue] [smallint] NULL,
	[RowSixthColumnFirstValue] [smallint] NULL,
	[RowSixthColumnSecondValue] [smallint] NULL,
	[RowSixthColumnThirdValue] [smallint] NULL,
	[RowSixthColumnForthValue] [smallint] NULL,
	[RowSixthColumnFifthValue] [smallint] NULL,
	[RowSixthColumnSixthValue] [smallint] NULL,
	[RowSixthColumnSeventhValue] [smallint] NULL,
	[RowSixthColumnEightthValue] [smallint] NULL,
	[RowSixthColumnNinthValue] [smallint] NULL,
	[RowSixthColumnTenthValue] [smallint] NULL,
	[RowSeventhColumnFirstValue] [smallint] NULL,
	[RowSeventhColumnSecondValue] [smallint] NULL,
	[RowSeventhColumnThirdValue] [smallint] NULL,
	[RowSeventhColumnForthValue] [smallint] NULL,
	[RowSeventhColumnFifthValue] [smallint] NULL,
	[RowSeventhColumnSixthValue] [smallint] NULL,
	[RowSeventhColumnSeventhValue] [smallint] NULL,
	[RowSeventhColumnEightthValue] [smallint] NULL,
	[RowSeventhColumnNinthValue] [smallint] NULL,
	[RowSeventhColumnTenthValue] [smallint] NULL,
	[RowEighthColumnFirstValue] [smallint] NULL,
	[RowEighthColumnSecondValue] [smallint] NULL,
	[RowEighthColumnThirdValue] [smallint] NULL,
	[RowEighthColumnForthValue] [smallint] NULL,
	[RowEighthColumnFifthValue] [smallint] NULL,
	[RowEighthColumnSixthValue] [smallint] NULL,
	[RowEighthColumnSeventhValue] [smallint] NULL,
	[RowEighthColumnEightthValue] [smallint] NULL,
	[RowEighthColumnNinthValue] [smallint] NULL,
	[RowEighthColumnTenthValue] [smallint] NULL,
	[RowNineColumnFirstValue] [smallint] NULL,
	[RowNineColumnSecondValue] [smallint] NULL,
	[RowNineColumnThirdValue] [smallint] NULL,
	[RowNineColumnForthValue] [smallint] NULL,
	[RowNineColumnFifthValue] [smallint] NULL,
	[RowNineColumnSixthValue] [smallint] NULL,
	[RowNineColumnSeventhValue] [smallint] NULL,
	[RowNineColumnEightthValue] [smallint] NULL,
	[RowNineColumnNinthValue] [smallint] NULL,
	[RowNineColumnTenthValue] [smallint] NULL,
 CONSTRAINT [PK_MeetSetupStandardLanePrefrences] PRIMARY KEY CLUSTERED 
(
	[MeetSetupStandardLanePrefrencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupWaterfallStart]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupWaterfallStart](
	[MeetSetupWaterfallStartPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[Position] [int] NULL,
	[Rank] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupWaterfallStart] PRIMARY KEY CLUSTERED 
(
	[MeetSetupWaterfallStartPreferenceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetStandardAlleyPrefrences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetStandardAlleyPrefrences](
	[MeetStandardAlleyPrefrencesID] [int] IDENTITY(1,1) NOT NULL,
	[RowZeroColumnFirstValue] [smallint] NULL,
	[RowZeroColumnSecondValue] [smallint] NULL,
	[RowZeroColumnThirdValue] [smallint] NULL,
	[RowZeroColumnForthValue] [smallint] NULL,
	[RowZeroColumnFifthValue] [smallint] NULL,
	[RowZeroColumnSixthValue] [smallint] NULL,
	[RowZeroColumnSeventhValue] [smallint] NULL,
	[RowZeroColumnEightthValue] [smallint] NULL,
	[RowZeroColumnNinthValue] [smallint] NULL,
	[RowZeroColumnTenthValue] [smallint] NULL,
	[RowOneColumnFirstValue] [smallint] NULL,
	[RowOneColumnSecondValue] [smallint] NULL,
	[RowOneColumnThirdValue] [smallint] NULL,
	[RowOneColumnForthValue] [smallint] NULL,
	[RowOneColumnFifthValue] [smallint] NULL,
	[RowOneColumnSixthValue] [smallint] NULL,
	[RowOneColumnSeventhValue] [smallint] NULL,
	[RowOneColumnEightthValue] [smallint] NULL,
	[RowOneColumnNinthValue] [smallint] NULL,
	[RowOneColumnTenthValue] [smallint] NULL,
	[RowSecondColumnFirstValue] [smallint] NULL,
	[RowSecondColumnSecondValue] [smallint] NULL,
	[RowSecondColumnThirdValue] [smallint] NULL,
	[RowSecondColumnForthValue] [smallint] NULL,
	[RowSecondColumnFifthValue] [smallint] NULL,
	[RowSecondColumnSixthValue] [smallint] NULL,
	[RowSecondColumnSeventhValue] [smallint] NULL,
	[RowSecondColumnEightthValue] [smallint] NULL,
	[RowSecondColumnNinthValue] [smallint] NULL,
	[RowSecondColumnTenthValue] [smallint] NULL,
	[RowThirdColumnFirstValue] [smallint] NULL,
	[RowThirdColumnSecondValue] [smallint] NULL,
	[RowThirdColumnThirdValue] [smallint] NULL,
	[RowThirdColumnForthValue] [smallint] NULL,
	[RowThirdColumnFifthValue] [smallint] NULL,
	[RowThirdColumnSixthValue] [smallint] NULL,
	[RowThirdColumnSeventhValue] [smallint] NULL,
	[RowThirdColumnEightthValue] [smallint] NULL,
	[RowThirdColumnNinthValue] [smallint] NULL,
	[RowThridColumnTenthValue] [smallint] NULL,
	[RowForthColumnFirstValue] [smallint] NULL,
	[RowForthColumnSecondValue] [smallint] NULL,
	[RowForthColumnThirdValue] [smallint] NULL,
	[RowForthColumnForthValue] [smallint] NULL,
	[RowForthColumnFifthValue] [smallint] NULL,
	[RowForthColumnSixthValue] [smallint] NULL,
	[RowForthColumnSeventhValue] [smallint] NULL,
	[RowForthColumnEightthValue] [smallint] NULL,
	[RowForthColumnNinthValue] [smallint] NULL,
	[RowForthColumnTenthValue] [smallint] NULL,
	[RowFifthColumnFirstValue] [smallint] NULL,
	[RowFifthColumnSecondValue] [smallint] NULL,
	[RowFifthColumnThirdValue] [smallint] NULL,
	[RowFifthColumnForthValue] [smallint] NULL,
	[RowFifthColumnFifthValue] [smallint] NULL,
	[RowFifthColumnSixthValue] [smallint] NULL,
	[RowFifthColumnSeventhValue] [smallint] NULL,
	[RowFifthColumnEightthValue] [smallint] NULL,
	[RowFifthColumnNinthValue] [smallint] NULL,
	[RowFifthColumnTenthValue] [smallint] NULL,
	[RowSixthColumnFirstValue] [smallint] NULL,
	[RowSixthColumnSecondValue] [smallint] NULL,
	[RowSixthColumnThirdValue] [smallint] NULL,
	[RowSixthColumnForthValue] [smallint] NULL,
	[RowSixthColumnFifthValue] [smallint] NULL,
	[RowSixthColumnSixthValue] [smallint] NULL,
	[RowSixthColumnSeventhValue] [smallint] NULL,
	[RowSixthColumnEightthValue] [smallint] NULL,
	[RowSixthColumnNinthValue] [smallint] NULL,
	[RowSixthColumnTenthValue] [smallint] NULL,
	[RowSeventhColumnFirstValue] [smallint] NULL,
	[RowSeventhColumnSecondValue] [smallint] NULL,
	[RowSeventhColumnThirdValue] [smallint] NULL,
	[RowSeventhColumnForthValue] [smallint] NULL,
	[RowSeventhColumnFifthValue] [smallint] NULL,
	[RowSeventhColumnSixthValue] [smallint] NULL,
	[RowSeventhColumnSeventhValue] [smallint] NULL,
	[RowSeventhColumnEightthValue] [smallint] NULL,
	[RowSeventhColumnNinthValue] [smallint] NULL,
	[RowSeventhColumnTenthValue] [smallint] NULL,
	[RowEighthColumnFirstValue] [smallint] NULL,
	[RowEighthColumnSecondValue] [smallint] NULL,
	[RowEighthColumnThirdValue] [smallint] NULL,
	[RowEighthColumnForthValue] [smallint] NULL,
	[RowEighthColumnFifthValue] [smallint] NULL,
	[RowEighthColumnSixthValue] [smallint] NULL,
	[RowEighthColumnSeventhValue] [smallint] NULL,
	[RowEighthColumnEightthValue] [smallint] NULL,
	[RowEighthColumnNinthValue] [smallint] NULL,
	[RowEighthColumnTenthValue] [smallint] NULL,
	[RowNineColumnFirstValue] [smallint] NULL,
	[RowNineColumnSecondValue] [smallint] NULL,
	[RowNineColumnThirdValue] [smallint] NULL,
	[RowNineColumnForthValue] [smallint] NULL,
	[RowNineColumnFifthValue] [smallint] NULL,
	[RowNineColumnSixthValue] [smallint] NULL,
	[RowNineColumnSeventhValue] [smallint] NULL,
	[RowNineColumnEightthValue] [smallint] NULL,
	[RowNineColumnNinthValue] [smallint] NULL,
	[RowNineColumnTenthValue] [smallint] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetStandardAlleyPrefrences] PRIMARY KEY CLUSTERED 
(
	[MeetStandardAlleyPrefrencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetStyle]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetStyle](
	[MeetStyleID] [int] IDENTITY(1,1) NOT NULL,
	[MeetStyleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_MeetStyles] PRIMARY KEY CLUSTERED 
(
	[MeetStyleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetType]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetType](
	[MeetTypeID] [int] IDENTITY(1,1) NOT NULL,
	[MeetTypeName] [nvarchar](150) NULL,
 CONSTRAINT [PK_MeetTypes] PRIMARY KEY CLUSTERED 
(
	[MeetTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetTypeDivision]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetTypeDivision](
	[MeetTypeDivisionID] [int] IDENTITY(1,1) NOT NULL,
	[MeetTypeDivisionName] [nvarchar](250) NULL,
	[MeetTypeID] [int] NULL,
 CONSTRAINT [PK_MeetTypeDivisions] PRIMARY KEY CLUSTERED 
(
	[MeetTypeDivisionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MenuInRole]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuInRole](
	[MenuInRoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [nvarchar](50) NULL,
	[MenuID] [int] NULL,
 CONSTRAINT [PK_MenuInRoles] PRIMARY KEY CLUSTERED 
(
	[MenuInRoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RelayPreferences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelayPreferences](
	[RelayPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[RelayPreferencesName] [nvarchar](250) NULL,
 CONSTRAINT [PK_RelayPreferences] PRIMARY KEY CLUSTERED 
(
	[RelayPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RelayPreferencesTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelayPreferencesTransaction](
	[RelayPreferencesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[RelayPreferencesGroupID] [nvarchar](250) NOT NULL,
	[RelayPreferencesID] [int] NULL,
 CONSTRAINT [PK_RelayPreferencesTransaction] PRIMARY KEY CLUSTERED 
(
	[RelayPreferencesTransactionID] ASC,
	[RelayPreferencesGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RendomizationRuleTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RendomizationRuleTransaction](
	[RendomizationRuleTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[RendomizationRuleGroupID] [nvarchar](250) NULL,
	[TimedFinalEvents] [nvarchar](50) NULL,
	[RoundFirstMultipleRound] [nvarchar](50) NULL,
	[RoundSecondThirdAndForth] [nvarchar](50) NULL,
	[CloseGap] [bit] NULL,
 CONSTRAINT [PK_RendomizationRuleTransaction] PRIMARY KEY CLUSTERED 
(
	[RendomizationRuleTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleGuid] [nvarchar](50) NOT NULL,
	[RoleID] [nvarchar](50) NULL,
	[RoleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SchoolDetail]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolDetail](
	[SchoolID] [int] IDENTITY(1,1) NOT NULL,
	[Abbr] [nvarchar](50) NULL,
	[SchoolName] [nvarchar](500) NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_SchoolDetails] PRIMARY KEY CLUSTERED 
(
	[SchoolID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SeedingPreferencesTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SeedingPreferencesTransaction](
	[SeedingPrefrencesID] [int] IDENTITY(1,1) NOT NULL,
	[WaterfallStartPreferencesGroupID] [nvarchar](250) NULL,
	[StandardLanePrefrencesGroupID] [nvarchar](250) NULL,
	[StandardAlleyPrefrencesGroupID] [nvarchar](250) NULL,
	[DualMeetGroupID] [nvarchar](250) NULL,
	[SeedingRulesGroupID] [nvarchar](250) NULL,
	[RendomizationRuleGroupID] [nvarchar](250) NULL,
 CONSTRAINT [PK_SeedingPreferences] PRIMARY KEY CLUSTERED 
(
	[SeedingPrefrencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SeedingRulesTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SeedingRulesTransaction](
	[SeedingRulesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[SeedingRulesGroupID] [nvarchar](250) NULL,
	[AllowForeignAthletesInFinal] [bit] NULL,
	[AllowExhibitionAthletesInFinal] [bit] NULL,
	[SeedExhibitionAthletesLast] [bit] NULL,
	[ApplyNCAARule] [bit] NULL,
	[UseSpecialRandomSelectMethod] [bit] NULL,
 CONSTRAINT [PK_SeedingRulesTransaction] PRIMARY KEY CLUSTERED 
(
	[SeedingRulesTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StandardAlleyPrefrences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardAlleyPrefrences](
	[StandardAlleyPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[PreferencesName] [nvarchar](250) NULL,
	[FirstLane] [int] NULL,
	[SecondLine] [int] NULL,
	[ThirdLine] [int] NULL,
	[ForthLine] [int] NULL,
	[FifthLine] [int] NULL,
	[SixthLine] [int] NULL,
	[SeventhLine] [int] NULL,
	[EightsLine] [int] NULL,
	[NinthLine] [int] NULL,
	[TenthLine] [int] NULL,
 CONSTRAINT [PK_StandardAlleyPrefrence] PRIMARY KEY CLUSTERED 
(
	[StandardAlleyPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StandardAlleyPrefrencesTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardAlleyPrefrencesTransaction](
	[StandardAlleyPrefrencesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[StandardAlleyPrefrencesGroupID] [nvarchar](250) NULL,
	[RowZeroColumnFirstValue] [smallint] NULL,
	[RowZeroColumnSecondValue] [smallint] NULL,
	[RowZeroColumnThirdValue] [smallint] NULL,
	[RowZeroColumnForthValue] [smallint] NULL,
	[RowZeroColumnFifthValue] [smallint] NULL,
	[RowZeroColumnSixthValue] [smallint] NULL,
	[RowZeroColumnSeventhValue] [smallint] NULL,
	[RowZeroColumnEightthValue] [smallint] NULL,
	[RowZeroColumnNinthValue] [smallint] NULL,
	[RowZeroColumnTenthValue] [smallint] NULL,
	[RowOneColumnFirstValue] [smallint] NULL,
	[RowOneColumnSecondValue] [smallint] NULL,
	[RowOneColumnThirdValue] [smallint] NULL,
	[RowOneColumnForthValue] [smallint] NULL,
	[RowOneColumnFifthValue] [smallint] NULL,
	[RowOneColumnSixthValue] [smallint] NULL,
	[RowOneColumnSeventhValue] [smallint] NULL,
	[RowOneColumnEightthValue] [smallint] NULL,
	[RowOneColumnNinthValue] [smallint] NULL,
	[RowOneColumnTenthValue] [smallint] NULL,
	[RowSecondColumnFirstValue] [smallint] NULL,
	[RowSecondColumnSecondValue] [smallint] NULL,
	[RowSecondColumnThirdValue] [smallint] NULL,
	[RowSecondColumnForthValue] [smallint] NULL,
	[RowSecondColumnFifthValue] [smallint] NULL,
	[RowSecondColumnSixthValue] [smallint] NULL,
	[RowSecondColumnSeventhValue] [smallint] NULL,
	[RowSecondColumnEightthValue] [smallint] NULL,
	[RowSecondColumnNinthValue] [smallint] NULL,
	[RowSecondColumnTenthValue] [smallint] NULL,
	[RowThirdColumnFirstValue] [smallint] NULL,
	[RowThirdColumnSecondValue] [smallint] NULL,
	[RowThirdColumnThirdValue] [smallint] NULL,
	[RowThirdColumnForthValue] [smallint] NULL,
	[RowThirdColumnFifthValue] [smallint] NULL,
	[RowThirdColumnSixthValue] [smallint] NULL,
	[RowThirdColumnSeventhValue] [smallint] NULL,
	[RowThirdColumnEightthValue] [smallint] NULL,
	[RowThirdColumnNinthValue] [smallint] NULL,
	[RowThridColumnTenthValue] [smallint] NULL,
	[RowForthColumnFirstValue] [smallint] NULL,
	[RowForthColumnSecondValue] [smallint] NULL,
	[RowForthColumnThirdValue] [smallint] NULL,
	[RowForthColumnForthValue] [smallint] NULL,
	[RowForthColumnFifthValue] [smallint] NULL,
	[RowForthColumnSixthValue] [smallint] NULL,
	[RowForthColumnSeventhValue] [smallint] NULL,
	[RowForthColumnEightthValue] [smallint] NULL,
	[RowForthColumnNinthValue] [smallint] NULL,
	[RowForthColumnTenthValue] [smallint] NULL,
	[RowFifthColumnFirstValue] [smallint] NULL,
	[RowFifthColumnSecondValue] [smallint] NULL,
	[RowFifthColumnThirdValue] [smallint] NULL,
	[RowFifthColumnForthValue] [smallint] NULL,
	[RowFifthColumnFifthValue] [smallint] NULL,
	[RowFifthColumnSixthValue] [smallint] NULL,
	[RowFifthColumnSeventhValue] [smallint] NULL,
	[RowFifthColumnEightthValue] [smallint] NULL,
	[RowFifthColumnNinthValue] [smallint] NULL,
	[RowFifthColumnTenthValue] [smallint] NULL,
	[RowSixthColumnFirstValue] [smallint] NULL,
	[RowSixthColumnSecondValue] [smallint] NULL,
	[RowSixthColumnThirdValue] [smallint] NULL,
	[RowSixthColumnForthValue] [smallint] NULL,
	[RowSixthColumnFifthValue] [smallint] NULL,
	[RowSixthColumnSixthValue] [smallint] NULL,
	[RowSixthColumnSeventhValue] [smallint] NULL,
	[RowSixthColumnEightthValue] [smallint] NULL,
	[RowSixthColumnNinthValue] [smallint] NULL,
	[RowSixthColumnTenthValue] [smallint] NULL,
	[RowSeventhColumnFirstValue] [smallint] NULL,
	[RowSeventhColumnSecondValue] [smallint] NULL,
	[RowSeventhColumnThirdValue] [smallint] NULL,
	[RowSeventhColumnForthValue] [smallint] NULL,
	[RowSeventhColumnFifthValue] [smallint] NULL,
	[RowSeventhColumnSixthValue] [smallint] NULL,
	[RowSeventhColumnSeventhValue] [smallint] NULL,
	[RowSeventhColumnEightthValue] [smallint] NULL,
	[RowSeventhColumnNinthValue] [smallint] NULL,
	[RowSeventhColumnTenthValue] [smallint] NULL,
	[RowEighthColumnFirstValue] [smallint] NULL,
	[RowEighthColumnSecondValue] [smallint] NULL,
	[RowEighthColumnThirdValue] [smallint] NULL,
	[RowEighthColumnForthValue] [smallint] NULL,
	[RowEighthColumnFifthValue] [smallint] NULL,
	[RowEighthColumnSixthValue] [smallint] NULL,
	[RowEighthColumnSeventhValue] [smallint] NULL,
	[RowEighthColumnEightthValue] [smallint] NULL,
	[RowEighthColumnNinthValue] [smallint] NULL,
	[RowEighthColumnTenthValue] [smallint] NULL,
	[RowNineColumnFirstValue] [smallint] NULL,
	[RowNineColumnSecondValue] [smallint] NULL,
	[RowNineColumnThirdValue] [smallint] NULL,
	[RowNineColumnForthValue] [smallint] NULL,
	[RowNineColumnFifthValue] [smallint] NULL,
	[RowNineColumnSixthValue] [smallint] NULL,
	[RowNineColumnSeventhValue] [smallint] NULL,
	[RowNineColumnEightthValue] [smallint] NULL,
	[RowNineColumnNinthValue] [smallint] NULL,
	[RowNineColumnTenthValue] [smallint] NULL,
 CONSTRAINT [PK_StandardAlleyPrefrencesTransaction] PRIMARY KEY CLUSTERED 
(
	[StandardAlleyPrefrencesTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StandardLanePrefrences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardLanePrefrences](
	[StandardLanePreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[PreferencesName] [nvarchar](250) NULL,
	[FirstLane] [int] NULL,
	[SecondLine] [int] NULL,
	[ThirdLine] [int] NULL,
	[ForthLine] [int] NULL,
	[FifthLine] [int] NULL,
	[SixthLine] [int] NULL,
	[SeventhLine] [int] NULL,
	[EightsLine] [int] NULL,
	[NinthLine] [int] NULL,
	[TenthLine] [int] NULL,
 CONSTRAINT [PK_StandardLanePrefrence] PRIMARY KEY CLUSTERED 
(
	[StandardLanePreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StandardLanePrefrencesTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardLanePrefrencesTransaction](
	[StandardLanePrefrencesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[StandardLanePrefrencesGroupID] [nvarchar](250) NULL,
	[RowZeroColumnFirstValue] [smallint] NULL,
	[RowZeroColumnSecondValue] [smallint] NULL,
	[RowZeroColumnThirdValue] [smallint] NULL,
	[RowZeroColumnForthValue] [smallint] NULL,
	[RowZeroColumnFifthValue] [smallint] NULL,
	[RowZeroColumnSixthValue] [smallint] NULL,
	[RowZeroColumnSeventhValue] [smallint] NULL,
	[RowZeroColumnEightthValue] [smallint] NULL,
	[RowZeroColumnNinthValue] [smallint] NULL,
	[RowZeroColumnTenthValue] [smallint] NULL,
	[RowOneColumnFirstValue] [smallint] NULL,
	[RowOneColumnSecondValue] [smallint] NULL,
	[RowOneColumnThirdValue] [smallint] NULL,
	[RowOneColumnForthValue] [smallint] NULL,
	[RowOneColumnFifthValue] [smallint] NULL,
	[RowOneColumnSixthValue] [smallint] NULL,
	[RowOneColumnSeventhValue] [smallint] NULL,
	[RowOneColumnEightthValue] [smallint] NULL,
	[RowOneColumnNinthValue] [smallint] NULL,
	[RowOneColumnTenthValue] [smallint] NULL,
	[RowSecondColumnFirstValue] [smallint] NULL,
	[RowSecondColumnSecondValue] [smallint] NULL,
	[RowSecondColumnThirdValue] [smallint] NULL,
	[RowSecondColumnForthValue] [smallint] NULL,
	[RowSecondColumnFifthValue] [smallint] NULL,
	[RowSecondColumnSixthValue] [smallint] NULL,
	[RowSecondColumnSeventhValue] [smallint] NULL,
	[RowSecondColumnEightthValue] [smallint] NULL,
	[RowSecondColumnNinthValue] [smallint] NULL,
	[RowSecondColumnTenthValue] [smallint] NULL,
	[RowThirdColumnFirstValue] [smallint] NULL,
	[RowThirdColumnSecondValue] [smallint] NULL,
	[RowThirdColumnThirdValue] [smallint] NULL,
	[RowThirdColumnForthValue] [smallint] NULL,
	[RowThirdColumnFifthValue] [smallint] NULL,
	[RowThirdColumnSixthValue] [smallint] NULL,
	[RowThirdColumnSeventhValue] [smallint] NULL,
	[RowThirdColumnEightthValue] [smallint] NULL,
	[RowThirdColumnNinthValue] [smallint] NULL,
	[RowThridColumnTenthValue] [smallint] NULL,
	[RowForthColumnFirstValue] [smallint] NULL,
	[RowForthColumnSecondValue] [smallint] NULL,
	[RowForthColumnThirdValue] [smallint] NULL,
	[RowForthColumnForthValue] [smallint] NULL,
	[RowForthColumnFifthValue] [smallint] NULL,
	[RowForthColumnSixthValue] [smallint] NULL,
	[RowForthColumnSeventhValue] [smallint] NULL,
	[RowForthColumnEightthValue] [smallint] NULL,
	[RowForthColumnNinthValue] [smallint] NULL,
	[RowForthColumnTenthValue] [smallint] NULL,
	[RowFifthColumnFirstValue] [smallint] NULL,
	[RowFifthColumnSecondValue] [smallint] NULL,
	[RowFifthColumnThirdValue] [smallint] NULL,
	[RowFifthColumnForthValue] [smallint] NULL,
	[RowFifthColumnFifthValue] [smallint] NULL,
	[RowFifthColumnSixthValue] [smallint] NULL,
	[RowFifthColumnSeventhValue] [smallint] NULL,
	[RowFifthColumnEightthValue] [smallint] NULL,
	[RowFifthColumnNinthValue] [smallint] NULL,
	[RowFifthColumnTenthValue] [smallint] NULL,
	[RowSixthColumnFirstValue] [smallint] NULL,
	[RowSixthColumnSecondValue] [smallint] NULL,
	[RowSixthColumnThirdValue] [smallint] NULL,
	[RowSixthColumnForthValue] [smallint] NULL,
	[RowSixthColumnFifthValue] [smallint] NULL,
	[RowSixthColumnSixthValue] [smallint] NULL,
	[RowSixthColumnSeventhValue] [smallint] NULL,
	[RowSixthColumnEightthValue] [smallint] NULL,
	[RowSixthColumnNinthValue] [smallint] NULL,
	[RowSixthColumnTenthValue] [smallint] NULL,
	[RowSeventhColumnFirstValue] [smallint] NULL,
	[RowSeventhColumnSecondValue] [smallint] NULL,
	[RowSeventhColumnThirdValue] [smallint] NULL,
	[RowSeventhColumnForthValue] [smallint] NULL,
	[RowSeventhColumnFifthValue] [smallint] NULL,
	[RowSeventhColumnSixthValue] [smallint] NULL,
	[RowSeventhColumnSeventhValue] [smallint] NULL,
	[RowSeventhColumnEightthValue] [smallint] NULL,
	[RowSeventhColumnNinthValue] [smallint] NULL,
	[RowSeventhColumnTenthValue] [smallint] NULL,
	[RowEighthColumnFirstValue] [smallint] NULL,
	[RowEighthColumnSecondValue] [smallint] NULL,
	[RowEighthColumnThirdValue] [smallint] NULL,
	[RowEighthColumnForthValue] [smallint] NULL,
	[RowEighthColumnFifthValue] [smallint] NULL,
	[RowEighthColumnSixthValue] [smallint] NULL,
	[RowEighthColumnSeventhValue] [smallint] NULL,
	[RowEighthColumnEightthValue] [smallint] NULL,
	[RowEighthColumnNinthValue] [smallint] NULL,
	[RowEighthColumnTenthValue] [smallint] NULL,
	[RowNineColumnFirstValue] [smallint] NULL,
	[RowNineColumnSecondValue] [smallint] NULL,
	[RowNineColumnThirdValue] [smallint] NULL,
	[RowNineColumnForthValue] [smallint] NULL,
	[RowNineColumnFifthValue] [smallint] NULL,
	[RowNineColumnSixthValue] [smallint] NULL,
	[RowNineColumnSeventhValue] [smallint] NULL,
	[RowNineColumnEightthValue] [smallint] NULL,
	[RowNineColumnNinthValue] [smallint] NULL,
	[RowNineColumnTenthValue] [smallint] NULL,
 CONSTRAINT [PK_StandardLanePrefrencesTransaction] PRIMARY KEY CLUSTERED 
(
	[StandardLanePrefrencesTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserGuid] [nvarchar](50) NOT NULL,
	[EmailID] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[UserPhoto] [varbinary](max) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[UserName] [nvarchar](max) NULL,
	[IsAdmin] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserInRole]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInRole](
	[UserGuid] [nvarchar](50) NULL,
	[RoleGuid] [nvarchar](50) NULL,
	[UserInRoleID] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserInRoles] PRIMARY KEY CLUSTERED 
(
	[UserInRoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WaterfallStartPreferences]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WaterfallStartPreferences](
	[WaterfallStartPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[Position] [int] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_WaterfallStartPreference] PRIMARY KEY CLUSTERED 
(
	[WaterfallStartPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WaterfallStartPreferenceTransaction]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WaterfallStartPreferenceTransaction](
	[WaterfallStartPreferenceTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[WaterfallStartPreferencesGroupID] [nvarchar](250) NULL,
	[Position] [int] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_WaterfallStartPreferenceTransaction] PRIMARY KEY CLUSTERED 
(
	[WaterfallStartPreferenceTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (1, N'File', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (2, N'Set-up', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (3, N'Events', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (4, N'Athletes', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (5, N'Relays', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (6, N'Schools', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (7, N'Seeding', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (8, N'Run', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (9, N'Labels', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (10, N'Check for Updates', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (11, N'Help', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (12, N'Meet Set-up', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (13, N'Athlete/Relay Preferences', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (14, N'Seeding Preferences', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (15, N'Admin', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (16, N'Create User', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (17, N'Add User Role', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (18, N'Assign Role', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (19, N'Add Module', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (20, N'Assign Module', 15)
SET IDENTITY_INSERT [dbo].[AthletePreferences] ON 

INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (1, N'Enter ages')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (2, N'Enter birth dates')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (3, N'Enter middle intial')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (4, N'Enter school year')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (5, N'Enter Registration Numbers')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (6, N'Enter Athlete Status')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (7, N'Enter ''Citizen of''')
SET IDENTITY_INSERT [dbo].[AthletePreferences] OFF
SET IDENTITY_INSERT [dbo].[AthleteRelayPreferencesTransaction] ON 

INSERT [dbo].[AthleteRelayPreferencesTransaction] ([AthleteRelayPreferencesID], [PreferencesTransactionGroupID], [CompetitorNumberTransactionGroupID], [RelayPreferencesTransactionGroupID]) VALUES (1, N'023a303b-feeb-4339-a0a2-f7077a0972d6', N'6eb043bd-807c-40d7-87b0-17f9708e437c', N'893a546f-ee9d-40d4-8ded-95428a8957da')
INSERT [dbo].[AthleteRelayPreferencesTransaction] ([AthleteRelayPreferencesID], [PreferencesTransactionGroupID], [CompetitorNumberTransactionGroupID], [RelayPreferencesTransactionGroupID]) VALUES (2, N'edd669f9-bc2e-457c-9857-e0de88af2a0b', N'a5e1f375-5b41-47c5-96dd-70c2112de161', N'92cdca44-eec0-4f0f-b39f-77767e5a008d')
INSERT [dbo].[AthleteRelayPreferencesTransaction] ([AthleteRelayPreferencesID], [PreferencesTransactionGroupID], [CompetitorNumberTransactionGroupID], [RelayPreferencesTransactionGroupID]) VALUES (3, N'2ee14873-479b-4b6e-b142-887b4fd69416', N'406d06cb-a261-4fce-ba8c-92088ee4331e', N'e60f4886-6fc8-41e3-a6df-12f2f3a14ea3')
INSERT [dbo].[AthleteRelayPreferencesTransaction] ([AthleteRelayPreferencesID], [PreferencesTransactionGroupID], [CompetitorNumberTransactionGroupID], [RelayPreferencesTransactionGroupID]) VALUES (4, N'316534b6-ed6d-4431-88ff-02546553ad12', N'489bc181-4b5f-4d90-a508-961d1ad42514', N'2771fbf8-c275-4805-8540-ed8fbe73f875')
SET IDENTITY_INSERT [dbo].[AthleteRelayPreferencesTransaction] OFF
SET IDENTITY_INSERT [dbo].[AthleticPreferencesTransaction] ON 

INSERT [dbo].[AthleticPreferencesTransaction] ([AthleticPreferencesTransactionID], [AthleticPreferencesGroupID], [AthleticPreferencesID]) VALUES (4, N'023a303b-feeb-4339-a0a2-f7077a0972d6', 5)
INSERT [dbo].[AthleticPreferencesTransaction] ([AthleticPreferencesTransactionID], [AthleticPreferencesGroupID], [AthleticPreferencesID]) VALUES (5, N'edd669f9-bc2e-457c-9857-e0de88af2a0b', 4)
INSERT [dbo].[AthleticPreferencesTransaction] ([AthleticPreferencesTransactionID], [AthleticPreferencesGroupID], [AthleticPreferencesID]) VALUES (6, N'2ee14873-479b-4b6e-b142-887b4fd69416', 3)
INSERT [dbo].[AthleticPreferencesTransaction] ([AthleticPreferencesTransactionID], [AthleticPreferencesGroupID], [AthleticPreferencesID]) VALUES (7, N'316534b6-ed6d-4431-88ff-02546553ad12', 5)
SET IDENTITY_INSERT [dbo].[AthleticPreferencesTransaction] OFF
SET IDENTITY_INSERT [dbo].[BaseCounty] ON 

INSERT [dbo].[BaseCounty] ([BaseCountyID], [BaseCountyName]) VALUES (1, N'USA')
INSERT [dbo].[BaseCounty] ([BaseCountyID], [BaseCountyName]) VALUES (2, N'CAN')
INSERT [dbo].[BaseCounty] ([BaseCountyID], [BaseCountyName]) VALUES (3, N'AUS')
INSERT [dbo].[BaseCounty] ([BaseCountyID], [BaseCountyName]) VALUES (4, N'OTH')
SET IDENTITY_INSERT [dbo].[BaseCounty] OFF
SET IDENTITY_INSERT [dbo].[CompetitorNumbers] ON 

INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (1, N'Enter competitor numbers', 0)
INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (2, N'Unique', 1)
INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (3, N'Male/Femail Separately Unique', 1)
INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (4, N'Disable Unqueness', 1)
INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (7, N'Auto increment competitor numbers', 0)
SET IDENTITY_INSERT [dbo].[CompetitorNumbers] OFF
SET IDENTITY_INSERT [dbo].[CompetitorNumbersTransaction] ON 

INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (1, N'6eb043bd-807c-40d7-87b0-17f9708e437c', 1)
INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (2, N'6eb043bd-807c-40d7-87b0-17f9708e437c', 2)
INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (3, N'6eb043bd-807c-40d7-87b0-17f9708e437c', 7)
INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (4, N'a5e1f375-5b41-47c5-96dd-70c2112de161', 4)
INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (5, N'a5e1f375-5b41-47c5-96dd-70c2112de161', 7)
INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (6, N'406d06cb-a261-4fce-ba8c-92088ee4331e', 2)
INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (7, N'489bc181-4b5f-4d90-a508-961d1ad42514', 1)
INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (8, N'489bc181-4b5f-4d90-a508-961d1ad42514', 2)
INSERT [dbo].[CompetitorNumbersTransaction] ([CompetitorNumbersTransactionID], [CompetitorNumbersGroupID], [CompetitorNumbersID]) VALUES (9, N'489bc181-4b5f-4d90-a508-961d1ad42514', 7)
SET IDENTITY_INSERT [dbo].[CompetitorNumbersTransaction] OFF
SET IDENTITY_INSERT [dbo].[DualMeetAssignmentTransaction] ON 

INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1, 1, 1, 1, 1, 1, N'e4782cf2-582a-4818-8008-99f5d10b7ac4')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (2, 1, 1, 1, 1, 1, N'61680991-ae4a-44dc-83ea-cd90ce94499c')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1002, 1, 1, 1, 1, 1, N'd4d14247-e962-4ec6-ba6b-c27df316cca8')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1003, 1, 1, 1, 1, 1, N'3026daff-a467-484d-b29e-1b756b42061b')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1004, 1, 1, 1, 1, 1, N'13776379-c3e5-4b36-adc4-bf16a2f6cfbe')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1005, 0, 0, 0, 0, 0, N'ef58c79e-2db4-4754-aad9-651bc8ece205')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1006, 0, 0, 0, 0, 0, N'aed9151e-fd25-4319-98be-745815219223')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1007, 0, 0, 0, 0, 0, N'1aa5a87a-7e65-40c3-b908-ef7e34664ac7')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1008, 0, 0, 0, 0, 0, N'c1e2af0e-bfb0-4b11-aee6-25b8ed74ab64')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1009, 1, 1, 0, 0, 0, N'c897f843-7adf-4a06-99a1-691d97902546')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1010, 1, 1, 0, 0, 0, N'cd0f5929-dcae-4c50-99b9-5f2786f95d72')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1011, 1, 1, 0, 0, 0, N'255b9886-d325-4171-b5ca-622ed4a226f4')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1012, 1, 1, 0, 0, 0, N'9246d4bc-cac4-4a30-b48d-afcb7bf58301')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1013, 1, 1, 0, 0, 0, N'64af4fab-ad3d-43a9-89e7-5abdfbcde1eb')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1014, 1, 1, 0, 0, 0, N'91c2ad8b-78f2-4289-a9de-8626ba510c04')
INSERT [dbo].[DualMeetAssignmentTransaction] ([DualMeetAssignmentTransactionID], [StrictAssignmentAllHeats], [StrictAssignmentFastestHeatOnly], [UseLaneAssignmentsForInLaneRacesOnly], [UserLaneOrPositionAssignmentsAbove], [AlternamtUserOfUnAssignedLane], [DualMeetGroupID]) VALUES (1015, 1, 1, 0, 0, 0, N'50ba2b7e-81d7-4fb3-a2e9-b280f6523c41')
SET IDENTITY_INSERT [dbo].[DualMeetAssignmentTransaction] OFF
SET IDENTITY_INSERT [dbo].[DualMeetTransaction] ON 

INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (8, N'e4782cf2-582a-4818-8008-99f5d10b7ac4', 1, 1)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (9, N'e4782cf2-582a-4818-8008-99f5d10b7ac4', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (10, N'e4782cf2-582a-4818-8008-99f5d10b7ac4', 3, 6)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (11, N'e4782cf2-582a-4818-8008-99f5d10b7ac4', 4, 7)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (12, N'e4782cf2-582a-4818-8008-99f5d10b7ac4', 5, 2)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (13, N'e4782cf2-582a-4818-8008-99f5d10b7ac4', 6, 5)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (14, N'e4782cf2-582a-4818-8008-99f5d10b7ac4', 7, 4)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (15, N'e4782cf2-582a-4818-8008-99f5d10b7ac4', 9, 8)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (16, N'61680991-ae4a-44dc-83ea-cd90ce94499c', 1, 1)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (17, N'61680991-ae4a-44dc-83ea-cd90ce94499c', 2, 2)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (18, N'61680991-ae4a-44dc-83ea-cd90ce94499c', 3, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (19, N'61680991-ae4a-44dc-83ea-cd90ce94499c', 4, 5)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (20, N'61680991-ae4a-44dc-83ea-cd90ce94499c', 5, 7)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (21, N'61680991-ae4a-44dc-83ea-cd90ce94499c', 6, 6)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (22, N'61680991-ae4a-44dc-83ea-cd90ce94499c', 7, 4)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (23, N'61680991-ae4a-44dc-83ea-cd90ce94499c', 10, 8)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1016, N'd4d14247-e962-4ec6-ba6b-c27df316cca8', 1, 1)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1017, N'd4d14247-e962-4ec6-ba6b-c27df316cca8', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1018, N'd4d14247-e962-4ec6-ba6b-c27df316cca8', 3, 2)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1019, N'd4d14247-e962-4ec6-ba6b-c27df316cca8', 4, 7)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1020, N'd4d14247-e962-4ec6-ba6b-c27df316cca8', 6, 6)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1021, N'd4d14247-e962-4ec6-ba6b-c27df316cca8', 8, 5)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1022, N'd4d14247-e962-4ec6-ba6b-c27df316cca8', 9, 4)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1023, N'd4d14247-e962-4ec6-ba6b-c27df316cca8', 10, 8)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1024, N'3026daff-a467-484d-b29e-1b756b42061b', 1, 1)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1025, N'3026daff-a467-484d-b29e-1b756b42061b', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1026, N'3026daff-a467-484d-b29e-1b756b42061b', 3, 2)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1027, N'3026daff-a467-484d-b29e-1b756b42061b', 4, 7)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1028, N'3026daff-a467-484d-b29e-1b756b42061b', 6, 6)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1029, N'3026daff-a467-484d-b29e-1b756b42061b', 8, 5)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1030, N'3026daff-a467-484d-b29e-1b756b42061b', 9, 4)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1031, N'3026daff-a467-484d-b29e-1b756b42061b', 10, 8)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1032, N'13776379-c3e5-4b36-adc4-bf16a2f6cfbe', 2, 2)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1033, N'c897f843-7adf-4a06-99a1-691d97902546', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1034, N'cd0f5929-dcae-4c50-99b9-5f2786f95d72', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1035, N'255b9886-d325-4171-b5ca-622ed4a226f4', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1036, N'9246d4bc-cac4-4a30-b48d-afcb7bf58301', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1037, N'64af4fab-ad3d-43a9-89e7-5abdfbcde1eb', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1038, N'91c2ad8b-78f2-4289-a9de-8626ba510c04', 2, 3)
INSERT [dbo].[DualMeetTransaction] ([DualMeetTransactionID], [DualMeetGroupID], [LaneNumber], [SchoolID]) VALUES (1039, N'50ba2b7e-81d7-4fb3-a2e9-b280f6523c41', 2, 3)
SET IDENTITY_INSERT [dbo].[DualMeetTransaction] OFF
SET IDENTITY_INSERT [dbo].[LaneDetail] ON 

INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (1, 1, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (2, 2, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (3, 3, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (4, 4, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (5, 5, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (6, 6, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (7, 7, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (8, 8, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (9, 9, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (10, 10, NULL)
SET IDENTITY_INSERT [dbo].[LaneDetail] OFF
SET IDENTITY_INSERT [dbo].[MeetArena] ON 

INSERT [dbo].[MeetArena] ([MeetArenaID], [MeetArenaName]) VALUES (1, N'Outdoor')
INSERT [dbo].[MeetArena] ([MeetArenaID], [MeetArenaName]) VALUES (2, N'Indoor')
SET IDENTITY_INSERT [dbo].[MeetArena] OFF
SET IDENTITY_INSERT [dbo].[MeetClass] ON 

INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (1, N'High School')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (2, N'College')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (3, N'USATF')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (4, N'IAAF')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (5, N'AAU')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (6, N'Master/Veterans')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (7, N'Others')
SET IDENTITY_INSERT [dbo].[MeetClass] OFF
SET IDENTITY_INSERT [dbo].[MeetKind] ON 

INSERT [dbo].[MeetKind] ([MeetKindID], [MeetKindName]) VALUES (1, N'Track and Field/CC')
SET IDENTITY_INSERT [dbo].[MeetKind] OFF
SET IDENTITY_INSERT [dbo].[MeetSetup] ON 

INSERT [dbo].[MeetSetup] ([MeetSetupID], [MeetSetupName], [MeetSetupName2], [MeetSetupStartDate], [MeetSetupEndDate], [MeetSetupAgeUpDate], [MeetKindID], [MeetClassID], [MeetTypeID], [BaseCountyID], [MeetArenaID], [MeetStyleID], [UseDivisionBirthdateRange], [LinkTermToDivision], [MeetTypeDivisionID]) VALUES (2, N'Meet2', N'Meetname2', CAST(N'2018-12-07' AS Date), CAST(N'2018-12-07' AS Date), CAST(N'2018-12-07' AS Date), 1, 3, 1, 1, 2, 2, 1, 1, 2)
INSERT [dbo].[MeetSetup] ([MeetSetupID], [MeetSetupName], [MeetSetupName2], [MeetSetupStartDate], [MeetSetupEndDate], [MeetSetupAgeUpDate], [MeetKindID], [MeetClassID], [MeetTypeID], [BaseCountyID], [MeetArenaID], [MeetStyleID], [UseDivisionBirthdateRange], [LinkTermToDivision], [MeetTypeDivisionID]) VALUES (4, N'Test3', N'Test4', CAST(N'2018-12-08' AS Date), CAST(N'2018-12-08' AS Date), CAST(N'2018-12-08' AS Date), 1, 1, 1, 2, 2, 1, 1, 0, 1)
INSERT [dbo].[MeetSetup] ([MeetSetupID], [MeetSetupName], [MeetSetupName2], [MeetSetupStartDate], [MeetSetupEndDate], [MeetSetupAgeUpDate], [MeetKindID], [MeetClassID], [MeetTypeID], [BaseCountyID], [MeetArenaID], [MeetStyleID], [UseDivisionBirthdateRange], [LinkTermToDivision], [MeetTypeDivisionID]) VALUES (5, N'Test4', N'Meet4', CAST(N'2018-12-08' AS Date), CAST(N'2018-12-08' AS Date), CAST(N'2018-12-08' AS Date), 1, 2, 1, 2, 2, 3, 0, 1, 3)
INSERT [dbo].[MeetSetup] ([MeetSetupID], [MeetSetupName], [MeetSetupName2], [MeetSetupStartDate], [MeetSetupEndDate], [MeetSetupAgeUpDate], [MeetKindID], [MeetClassID], [MeetTypeID], [BaseCountyID], [MeetArenaID], [MeetStyleID], [UseDivisionBirthdateRange], [LinkTermToDivision], [MeetTypeDivisionID]) VALUES (14, N'Meet Name 12', N'Meet Name 2 of 12', CAST(N'2018-12-09' AS Date), CAST(N'2018-12-09' AS Date), CAST(N'2018-12-09' AS Date), 1, 1, 1, 1, 2, 2, 1, 1, 1)
INSERT [dbo].[MeetSetup] ([MeetSetupID], [MeetSetupName], [MeetSetupName2], [MeetSetupStartDate], [MeetSetupEndDate], [MeetSetupAgeUpDate], [MeetKindID], [MeetClassID], [MeetTypeID], [BaseCountyID], [MeetArenaID], [MeetStyleID], [UseDivisionBirthdateRange], [LinkTermToDivision], [MeetTypeDivisionID]) VALUES (15, N'Meet 13', N'Meeet Tydfs', CAST(N'2018-12-22' AS Date), CAST(N'2018-12-22' AS Date), CAST(N'2018-12-22' AS Date), 1, 2, 1, 2, 2, 1, 1, 0, 1)
SET IDENTITY_INSERT [dbo].[MeetSetup] OFF
SET IDENTITY_INSERT [dbo].[MeetStyle] ON 

INSERT [dbo].[MeetStyle] ([MeetStyleID], [MeetStyleName]) VALUES (1, N'Standard')
INSERT [dbo].[MeetStyle] ([MeetStyleID], [MeetStyleName]) VALUES (2, N'2 Team Dual')
INSERT [dbo].[MeetStyle] ([MeetStyleID], [MeetStyleName]) VALUES (3, N'3 + Team Dbi Dual')
SET IDENTITY_INSERT [dbo].[MeetStyle] OFF
SET IDENTITY_INSERT [dbo].[MeetType] ON 

INSERT [dbo].[MeetType] ([MeetTypeID], [MeetTypeName]) VALUES (1, N'Standard')
SET IDENTITY_INSERT [dbo].[MeetType] OFF
SET IDENTITY_INSERT [dbo].[MeetTypeDivision] ON 

INSERT [dbo].[MeetTypeDivision] ([MeetTypeDivisionID], [MeetTypeDivisionName], [MeetTypeID]) VALUES (1, N'By Event', 1)
INSERT [dbo].[MeetTypeDivision] ([MeetTypeDivisionID], [MeetTypeDivisionName], [MeetTypeID]) VALUES (2, N'By Team', 1)
INSERT [dbo].[MeetTypeDivision] ([MeetTypeDivisionID], [MeetTypeDivisionName], [MeetTypeID]) VALUES (3, N'By Entry', 1)
SET IDENTITY_INSERT [dbo].[MeetTypeDivision] OFF
SET IDENTITY_INSERT [dbo].[MenuInRole] ON 

INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (1, N'1', 1)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (2, N'1', 2)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (3, N'1', 3)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (4, N'1', 4)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (5, N'1', 5)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (6, N'1', 6)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (7, N'1', 7)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (8, N'1', 8)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (9, N'1', 9)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (10, N'1', 10)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (11, N'1', 11)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (12, N'1', 12)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (13, N'1', 13)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (14, N'1', 14)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (15, N'1', 15)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (16, N'1', 16)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (17, N'1', 17)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (18, N'1', 18)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (19, N'1', 19)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (20, N'1', 20)
SET IDENTITY_INSERT [dbo].[MenuInRole] OFF
SET IDENTITY_INSERT [dbo].[RelayPreferences] ON 

INSERT [dbo].[RelayPreferences] ([RelayPreferencesID], [RelayPreferencesName]) VALUES (1, N'Allow "A" Relays only')
INSERT [dbo].[RelayPreferences] ([RelayPreferencesID], [RelayPreferencesName]) VALUES (2, N'Allow anyone from any team on a relay')
INSERT [dbo].[RelayPreferences] ([RelayPreferencesID], [RelayPreferencesName]) VALUES (3, N'Allow athlete on 2+ relays in same event')
SET IDENTITY_INSERT [dbo].[RelayPreferences] OFF
SET IDENTITY_INSERT [dbo].[RelayPreferencesTransaction] ON 

INSERT [dbo].[RelayPreferencesTransaction] ([RelayPreferencesTransactionID], [RelayPreferencesGroupID], [RelayPreferencesID]) VALUES (1, N'893a546f-ee9d-40d4-8ded-95428a8957da', 1)
INSERT [dbo].[RelayPreferencesTransaction] ([RelayPreferencesTransactionID], [RelayPreferencesGroupID], [RelayPreferencesID]) VALUES (2, N'92cdca44-eec0-4f0f-b39f-77767e5a008d', 2)
INSERT [dbo].[RelayPreferencesTransaction] ([RelayPreferencesTransactionID], [RelayPreferencesGroupID], [RelayPreferencesID]) VALUES (3, N'e60f4886-6fc8-41e3-a6df-12f2f3a14ea3', 1)
INSERT [dbo].[RelayPreferencesTransaction] ([RelayPreferencesTransactionID], [RelayPreferencesGroupID], [RelayPreferencesID]) VALUES (4, N'2771fbf8-c275-4805-8540-ed8fbe73f875', 1)
SET IDENTITY_INSERT [dbo].[RelayPreferencesTransaction] OFF
SET IDENTITY_INSERT [dbo].[RendomizationRuleTransaction] ON 

INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1, N'17ceb0f3-d18e-479b-a657-2f30a6c2cb4b', N'A', N'A', N'A', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (2, N'958f5511-d7c8-4589-9276-316e082ff942', N'A', N'B', N'C', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1002, N'02a9dfd9-91f9-437d-9272-30b948536034', N'A', N'B', N'C', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1003, N'6e9a76d4-b849-4534-8b9b-e1ec7009ce1d', N'A', N'B', N'C', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1004, N'7c88aa41-fcb7-407f-8208-048199cc80c8', N'A', N'A', N'A', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1005, N'74d9326f-170c-43b7-894c-a749173c44cb', NULL, NULL, NULL, 0)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1006, N'1d2672ca-714b-4a6e-b5ba-923e50dc8ebf', NULL, NULL, NULL, 0)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1007, N'ebbf62ae-3fa9-41ce-b846-06dc2f2060fc', NULL, NULL, NULL, 0)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1008, N'4b402e9c-d89c-46b7-bf0e-9006fa2142bc', NULL, NULL, NULL, 0)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1009, N'b2c202d6-ec86-4001-8086-7f8840b85d81', N'A', N'A', N'A', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1010, N'510776ee-f9f6-41b6-b56a-0fe87fc7b2fc', N'A', N'A', N'A', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1011, N'0d58ad40-3717-4129-a36a-ebf0e9571bb4', N'A', N'A', N'A', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1012, N'fb3b1dd9-a8e4-47ba-be7b-969d7f59ad6b', N'A', N'A', N'A', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1013, N'7a86d667-eb92-4ea0-9240-fd9577424e68', N'A', N'A', N'A', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1014, N'ea1d2e59-4009-40ed-9826-12a10d257d03', N'A', N'A', N'A', 1)
INSERT [dbo].[RendomizationRuleTransaction] ([RendomizationRuleTransactionID], [RendomizationRuleGroupID], [TimedFinalEvents], [RoundFirstMultipleRound], [RoundSecondThirdAndForth], [CloseGap]) VALUES (1015, N'75d578f1-0e30-4b30-a6b2-bcd800081a2c', N'A', N'A', N'A', 1)
SET IDENTITY_INSERT [dbo].[RendomizationRuleTransaction] OFF
INSERT [dbo].[Role] ([RoleGuid], [RoleID], [RoleName]) VALUES (N'1', N'1', N'Admin')
SET IDENTITY_INSERT [dbo].[SchoolDetail] ON 

INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (1, N'AGOU', N'Agoura HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (2, N'CALB', N'Calabasas HAS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (3, N'MOOR', N'Moorpark HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (4, N'NEPA', N'Newbury Park HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (5, N'ROYL', N'Royal HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (6, N'SLVA', N'Simi valley HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (7, N'THOA', N'Thousand Oaks HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (8, N'WELA', N'Westlake HS', NULL)
SET IDENTITY_INSERT [dbo].[SchoolDetail] OFF
SET IDENTITY_INSERT [dbo].[SeedingPreferencesTransaction] ON 

INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (2, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', N'8b46ec3b-3aaa-4e4d-9cea-9f428d8b74b2', N'9f1d8082-dc62-43fe-9b8d-ff740f7ba405', N'e4782cf2-582a-4818-8008-99f5d10b7ac4', N'ae40715e-083f-4386-afe5-9f9341a71dd7', N'17ceb0f3-d18e-479b-a657-2f30a6c2cb4b')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (3, N'243395e5-d4ed-476d-844d-28de84a5be9f', N'38b884e1-6c27-497c-8d08-7ce96de7a0a7', N'87b9c6c8-4940-487d-a2eb-1a7c7fb9ca3d', N'61680991-ae4a-44dc-83ea-cd90ce94499c', N'7c599a89-419d-447c-8454-be39b5366ab1', N'958f5511-d7c8-4589-9276-316e082ff942')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1003, N'0748298a-375a-4634-ac84-be9df2818254', N'c301c1cd-a0ff-4302-baab-b767fe63413e', N'71c63437-b84f-47de-871a-4094b8b56e29', N'd4d14247-e962-4ec6-ba6b-c27df316cca8', N'b9dea392-b543-4cfb-99cf-47bc7c3b8a52', N'02a9dfd9-91f9-437d-9272-30b948536034')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1004, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', N'9f667fcb-13f5-40d0-baaa-00a13e6e2f6b', N'fb00cbaa-298d-4315-baf5-087e47ed2208', N'3026daff-a467-484d-b29e-1b756b42061b', N'821b5ff4-d7b7-44e0-bf05-5aea4c236906', N'6e9a76d4-b849-4534-8b9b-e1ec7009ce1d')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1005, N'a875846b-0112-49a3-a8be-394e72ce2a91', N'3b62308c-0ce1-461d-8f46-af486a2e8dcb', N'0e04d61a-c6f5-4e58-9ff1-2a626a11a363', N'13776379-c3e5-4b36-adc4-bf16a2f6cfbe', N'632b3b61-c861-43df-b10f-75d4b0052923', N'7c88aa41-fcb7-407f-8208-048199cc80c8')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1006, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', N'4e7d5506-103c-46b8-b97e-8da37fbcddcd', N'6392df2d-c095-45e9-830d-d6c1dfb84ff6', N'c897f843-7adf-4a06-99a1-691d97902546', N'8cf0187f-16bd-4de9-b6ed-d6cedd5d6151', N'b2c202d6-ec86-4001-8086-7f8840b85d81')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1007, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', N'6da5786c-70df-4345-bbc6-5669ac7a4882', N'c02dde20-1147-45b5-bd1c-d24617a17c58', N'cd0f5929-dcae-4c50-99b9-5f2786f95d72', N'907318f0-9c5f-405a-9cd4-a0fc03340c38', N'510776ee-f9f6-41b6-b56a-0fe87fc7b2fc')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1008, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', N'b3387d25-4a6c-4127-95d8-b55c2506970b', N'85fd1bd9-98ec-4b2f-964e-dea306f8f3dc', N'255b9886-d325-4171-b5ca-622ed4a226f4', N'02f4368e-5046-4540-8d60-3adf7ffa20c1', N'0d58ad40-3717-4129-a36a-ebf0e9571bb4')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1009, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', N'ded04c47-f2a3-4d5a-9ac3-5b4e44565353', N'f6fb26bc-e229-4d25-b41a-5dac1342b734', N'9246d4bc-cac4-4a30-b48d-afcb7bf58301', N'c3758e48-96a1-4de8-bcc7-dd9064c903b3', N'fb3b1dd9-a8e4-47ba-be7b-969d7f59ad6b')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1010, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', N'd65d11bb-3d65-44ab-b242-a70d15a53837', N'952d1caf-dcbd-4e33-bfae-d801917a5cac', N'64af4fab-ad3d-43a9-89e7-5abdfbcde1eb', N'd5008d0e-9755-4694-8c5a-997b7f7bdac4', N'7a86d667-eb92-4ea0-9240-fd9577424e68')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1011, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', N'72fe454e-3d24-49bd-834a-b3f9592fb260', N'670e14d4-4f09-46a2-84aa-131fe0e25121', N'91c2ad8b-78f2-4289-a9de-8626ba510c04', N'ed050129-f79c-4f12-8b8e-950f6feff7fd', N'ea1d2e59-4009-40ed-9826-12a10d257d03')
INSERT [dbo].[SeedingPreferencesTransaction] ([SeedingPrefrencesID], [WaterfallStartPreferencesGroupID], [StandardLanePrefrencesGroupID], [StandardAlleyPrefrencesGroupID], [DualMeetGroupID], [SeedingRulesGroupID], [RendomizationRuleGroupID]) VALUES (1012, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', N'8e883bdb-b754-4068-8a7d-309ac2a1c9ae', N'e8be642b-cdda-49c9-ae92-33eeb1089049', N'50ba2b7e-81d7-4fb3-a2e9-b280f6523c41', N'ff16bc2a-413c-4b07-a0b1-36d4470df15a', N'75d578f1-0e30-4b30-a6b2-bcd800081a2c')
SET IDENTITY_INSERT [dbo].[SeedingPreferencesTransaction] OFF
SET IDENTITY_INSERT [dbo].[SeedingRulesTransaction] ON 

INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1, N'48311cbd-6fbb-4521-a971-b22856aa2a31', 1, 1, 1, 1, 1)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (2, N'ae40715e-083f-4386-afe5-9f9341a71dd7', 1, 1, 1, 1, 1)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (3, N'7c599a89-419d-447c-8454-be39b5366ab1', 1, 1, 1, 1, 1)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1003, N'b9dea392-b543-4cfb-99cf-47bc7c3b8a52', 1, 1, 1, 0, 1)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1004, N'821b5ff4-d7b7-44e0-bf05-5aea4c236906', 1, 1, 1, 0, 1)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1005, N'632b3b61-c861-43df-b10f-75d4b0052923', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1006, N'b2c19411-349a-4fc7-96b1-a2241f9321fc', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1007, N'4a28f9a4-e981-4ce1-b0e5-229aaedc4033', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1008, N'c5a4adca-c193-4650-899f-ddcb3e643267', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1009, N'58d1660a-7759-4468-8e72-d271895ef593', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1010, N'8cf0187f-16bd-4de9-b6ed-d6cedd5d6151', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1011, N'907318f0-9c5f-405a-9cd4-a0fc03340c38', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1012, N'02f4368e-5046-4540-8d60-3adf7ffa20c1', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1013, N'c3758e48-96a1-4de8-bcc7-dd9064c903b3', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1014, N'd5008d0e-9755-4694-8c5a-997b7f7bdac4', 1, 1, 1, 1, 0)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1015, N'ed050129-f79c-4f12-8b8e-950f6feff7fd', 1, 1, 1, 1, 1)
INSERT [dbo].[SeedingRulesTransaction] ([SeedingRulesTransactionID], [SeedingRulesGroupID], [AllowForeignAthletesInFinal], [AllowExhibitionAthletesInFinal], [SeedExhibitionAthletesLast], [ApplyNCAARule], [UseSpecialRandomSelectMethod]) VALUES (1016, N'ff16bc2a-413c-4b07-a0b1-36d4470df15a', 1, 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[SeedingRulesTransaction] OFF
SET IDENTITY_INSERT [dbo].[StandardAlleyPrefrences] ON 

INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (1, N'1 Alley', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (2, N'2 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (3, N'3 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (4, N'4 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (5, N'5 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (6, N'6 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (7, N'7 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (8, N'8 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (9, N'9 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (10, N'10 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[StandardAlleyPrefrences] OFF
SET IDENTITY_INSERT [dbo].[StandardAlleyPrefrencesTransaction] ON 

INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (1, N'1bf18249-1147-483f-bd9e-dab70ae77c6e', 2, 3, 4, 5, 6, 7, 1, 8, 9, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (2, N'018d64de-5339-438f-af1e-8d4939e6fd3c', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (3, N'b37131d8-ee2b-40ae-8ebe-1d5362ffcb96', 7, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (4, N'6f448349-32b6-4c91-a153-1803ecd9becd', 7, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (5, N'9f1d8082-dc62-43fe-9b8d-ff740f7ba405', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (6, N'87b9c6c8-4940-487d-a2eb-1a7c7fb9ca3d', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (7, N'71c63437-b84f-47de-871a-4094b8b56e29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (8, N'fb00cbaa-298d-4315-baf5-087e47ed2208', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (9, N'0e04d61a-c6f5-4e58-9ff1-2a626a11a363', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (10, N'ca5e159f-946a-4979-a31e-5043e6a5a681', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (11, N'6392df2d-c095-45e9-830d-d6c1dfb84ff6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (12, N'c02dde20-1147-45b5-bd1c-d24617a17c58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (13, N'85fd1bd9-98ec-4b2f-964e-dea306f8f3dc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (14, N'f6fb26bc-e229-4d25-b41a-5dac1342b734', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (15, N'952d1caf-dcbd-4e33-bfae-d801917a5cac', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (16, N'670e14d4-4f09-46a2-84aa-131fe0e25121', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrencesTransaction] ([StandardAlleyPrefrencesTransactionID], [StandardAlleyPrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (17, N'e8be642b-cdda-49c9-ae92-33eeb1089049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[StandardAlleyPrefrencesTransaction] OFF
SET IDENTITY_INSERT [dbo].[StandardLanePrefrences] ON 

INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (1, N'1 Lane', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (2, N'2 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (3, N'3 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (4, N'4 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (5, N'5 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (6, N'6 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (7, N'7 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (8, N'8 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (9, N'9 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (10, N'10 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[StandardLanePrefrences] OFF
SET IDENTITY_INSERT [dbo].[StandardLanePrefrencesTransaction] ON 

INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (1, N'cdfd4e1f-c109-4741-893d-cbcf3c4f0cb1', 10, 7, 5, 1, 2, 3, 6, 8, 9, 4, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 9, 1, 2, 3, 4, 5, 6, 8, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (2, N'3d7ee766-d040-4e68-bd00-3f4af8220fe8', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (3, N'6cd78cd3-7784-41e0-9197-469a3ac0e2a3', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 4, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (4, N'5b96c4b8-1f52-49e5-9e2e-4eab405ef439', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 4, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (5, N'8b46ec3b-3aaa-4e4d-9cea-9f428d8b74b2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 6, 5, 6, 7, 8, 9, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (6, N'38b884e1-6c27-497c-8d08-7ce96de7a0a7', 4, 5, 6, 7, 8, 9, 10, 2, 3, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 5, 4, 3, 2, 1, 6, 7, 8, 9, 10, 10, 9, 8, 7, 6, 5, 1, 2, 3, 4, 9, 7, 5, 3, 1, 6, 8, 10, 2, 4, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 4, 6, 8, 10, 3, 5, 7, 9, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (7, N'c301c1cd-a0ff-4302-baab-b767fe63413e', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (8, N'9f667fcb-13f5-40d0-baaa-00a13e6e2f6b', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (9, N'3b62308c-0ce1-461d-8f46-af486a2e8dcb', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (10, N'c49ac205-e9e4-4b66-88b1-809f636c2a82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (11, N'2836e418-a953-4eee-8f77-49093fb0527a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (12, N'084d1ed2-c21b-4f5f-bdb2-902087da3546', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (13, N'0924738d-e652-4332-8a8e-59382f313699', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (14, N'4e7d5506-103c-46b8-b97e-8da37fbcddcd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (15, N'6da5786c-70df-4345-bbc6-5669ac7a4882', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (16, N'b3387d25-4a6c-4127-95d8-b55c2506970b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (17, N'ded04c47-f2a3-4d5a-9ac3-5b4e44565353', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (18, N'd65d11bb-3d65-44ab-b242-a70d15a53837', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (19, N'72fe454e-3d24-49bd-834a-b3f9592fb260', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrencesTransaction] ([StandardLanePrefrencesTransactionID], [StandardLanePrefrencesGroupID], [RowZeroColumnFirstValue], [RowZeroColumnSecondValue], [RowZeroColumnThirdValue], [RowZeroColumnForthValue], [RowZeroColumnFifthValue], [RowZeroColumnSixthValue], [RowZeroColumnSeventhValue], [RowZeroColumnEightthValue], [RowZeroColumnNinthValue], [RowZeroColumnTenthValue], [RowOneColumnFirstValue], [RowOneColumnSecondValue], [RowOneColumnThirdValue], [RowOneColumnForthValue], [RowOneColumnFifthValue], [RowOneColumnSixthValue], [RowOneColumnSeventhValue], [RowOneColumnEightthValue], [RowOneColumnNinthValue], [RowOneColumnTenthValue], [RowSecondColumnFirstValue], [RowSecondColumnSecondValue], [RowSecondColumnThirdValue], [RowSecondColumnForthValue], [RowSecondColumnFifthValue], [RowSecondColumnSixthValue], [RowSecondColumnSeventhValue], [RowSecondColumnEightthValue], [RowSecondColumnNinthValue], [RowSecondColumnTenthValue], [RowThirdColumnFirstValue], [RowThirdColumnSecondValue], [RowThirdColumnThirdValue], [RowThirdColumnForthValue], [RowThirdColumnFifthValue], [RowThirdColumnSixthValue], [RowThirdColumnSeventhValue], [RowThirdColumnEightthValue], [RowThirdColumnNinthValue], [RowThridColumnTenthValue], [RowForthColumnFirstValue], [RowForthColumnSecondValue], [RowForthColumnThirdValue], [RowForthColumnForthValue], [RowForthColumnFifthValue], [RowForthColumnSixthValue], [RowForthColumnSeventhValue], [RowForthColumnEightthValue], [RowForthColumnNinthValue], [RowForthColumnTenthValue], [RowFifthColumnFirstValue], [RowFifthColumnSecondValue], [RowFifthColumnThirdValue], [RowFifthColumnForthValue], [RowFifthColumnFifthValue], [RowFifthColumnSixthValue], [RowFifthColumnSeventhValue], [RowFifthColumnEightthValue], [RowFifthColumnNinthValue], [RowFifthColumnTenthValue], [RowSixthColumnFirstValue], [RowSixthColumnSecondValue], [RowSixthColumnThirdValue], [RowSixthColumnForthValue], [RowSixthColumnFifthValue], [RowSixthColumnSixthValue], [RowSixthColumnSeventhValue], [RowSixthColumnEightthValue], [RowSixthColumnNinthValue], [RowSixthColumnTenthValue], [RowSeventhColumnFirstValue], [RowSeventhColumnSecondValue], [RowSeventhColumnThirdValue], [RowSeventhColumnForthValue], [RowSeventhColumnFifthValue], [RowSeventhColumnSixthValue], [RowSeventhColumnSeventhValue], [RowSeventhColumnEightthValue], [RowSeventhColumnNinthValue], [RowSeventhColumnTenthValue], [RowEighthColumnFirstValue], [RowEighthColumnSecondValue], [RowEighthColumnThirdValue], [RowEighthColumnForthValue], [RowEighthColumnFifthValue], [RowEighthColumnSixthValue], [RowEighthColumnSeventhValue], [RowEighthColumnEightthValue], [RowEighthColumnNinthValue], [RowEighthColumnTenthValue], [RowNineColumnFirstValue], [RowNineColumnSecondValue], [RowNineColumnThirdValue], [RowNineColumnForthValue], [RowNineColumnFifthValue], [RowNineColumnSixthValue], [RowNineColumnSeventhValue], [RowNineColumnEightthValue], [RowNineColumnNinthValue], [RowNineColumnTenthValue]) VALUES (20, N'8e883bdb-b754-4068-8a7d-309ac2a1c9ae', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[StandardLanePrefrencesTransaction] OFF
INSERT [dbo].[User] ([UserGuid], [EmailID], [FirstName], [LastName], [Password], [UserPhoto], [DisplayName], [UserName], [IsAdmin]) VALUES (N'1', N'grate.devender@gmail.com', N'devender', N'sharma', N'pass@123', NULL, N'DPS', N'grate.devender@gmail.com', 1)
INSERT [dbo].[UserInRole] ([UserGuid], [RoleGuid], [UserInRoleID]) VALUES (N'1', N'1', N'1')
SET IDENTITY_INSERT [dbo].[WaterfallStartPreferences] ON 

INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (1, NULL, 1)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (2, NULL, 2)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (3, NULL, 3)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (4, NULL, 4)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (5, NULL, 5)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (6, NULL, 6)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (7, NULL, 7)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (8, NULL, 8)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (9, NULL, 9)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (10, NULL, 10)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (11, NULL, 11)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (12, NULL, 12)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (13, NULL, 13)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (14, NULL, 14)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (15, NULL, 15)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (16, NULL, 16)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (17, NULL, 17)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (19, NULL, 18)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (20, NULL, 19)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (21, NULL, 20)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (22, NULL, 21)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (23, NULL, 22)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (24, NULL, 23)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (25, NULL, 24)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (26, NULL, 25)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (27, NULL, 26)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (28, NULL, 27)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (29, NULL, 28)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (30, NULL, 29)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (31, NULL, 30)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (32, NULL, 31)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (33, NULL, 32)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (34, NULL, 33)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (35, NULL, 34)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (36, NULL, 35)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (37, NULL, 36)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (38, NULL, 37)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (39, NULL, 38)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (40, NULL, 39)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (41, NULL, 40)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (42, NULL, 41)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (43, NULL, 42)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (44, NULL, 43)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (45, NULL, 44)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (46, NULL, 45)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (47, NULL, 46)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (48, NULL, 47)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (49, NULL, 48)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (50, NULL, 49)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (51, NULL, 50)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (52, NULL, 51)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (53, NULL, 52)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (54, NULL, 53)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (55, NULL, 54)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (56, NULL, 55)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (57, NULL, 56)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (58, NULL, 57)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (59, NULL, 58)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (60, NULL, 59)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (61, NULL, 60)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (62, NULL, 61)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (63, NULL, 62)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (64, NULL, 63)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (65, NULL, 64)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (66, NULL, 65)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (67, NULL, 66)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (68, NULL, 67)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (69, NULL, 68)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (70, NULL, 69)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (71, NULL, 70)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (72, NULL, 71)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (73, NULL, 72)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (74, NULL, 73)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (75, NULL, 74)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (76, NULL, 75)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (77, NULL, 76)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (78, NULL, 77)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (79, NULL, 78)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (80, NULL, 79)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (81, NULL, 80)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (82, NULL, 81)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (83, NULL, 82)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (84, NULL, 83)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (85, NULL, 84)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (86, NULL, 85)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (87, NULL, 86)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (88, NULL, 87)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (89, NULL, 88)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (90, NULL, 89)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (91, NULL, 90)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (92, NULL, 91)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (93, NULL, 92)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (94, NULL, 93)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (95, NULL, 94)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (96, NULL, 95)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (97, NULL, 96)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (98, NULL, 97)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (99, NULL, 98)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (100, NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (101, NULL, 100)
SET IDENTITY_INSERT [dbo].[WaterfallStartPreferences] OFF
SET IDENTITY_INSERT [dbo].[WaterfallStartPreferenceTransaction] ON 

INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 1, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (2, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 2, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (3, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 3, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (4, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 3, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (5, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 4, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (6, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 5, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (7, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 6, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (8, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 6, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (9, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', 1, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (10, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (11, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (12, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (13, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (14, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (15, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (16, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (17, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (18, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (19, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (20, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (21, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (22, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (23, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (24, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (25, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (26, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (27, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (28, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (29, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (30, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (31, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (32, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (33, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (34, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (35, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (36, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (37, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (38, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (39, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (40, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (41, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (42, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (43, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (44, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (45, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (46, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (47, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (48, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (49, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (50, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (51, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (52, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (53, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (54, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (55, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (56, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (57, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (58, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (59, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (60, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (61, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (62, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (63, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (64, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (65, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (66, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (67, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (68, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (69, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (70, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (71, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (72, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (73, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (74, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (75, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (76, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (77, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (78, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (79, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (80, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (81, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (82, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (83, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (84, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (85, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (86, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (87, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (88, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (89, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (90, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (91, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (92, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (93, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (94, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (95, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (96, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (97, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (98, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (99, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (100, N'bd174c0c-894f-4fb4-8b5c-cbda45bd8c2e', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (101, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 1, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (102, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 2, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (103, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 3, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (104, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 4, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (105, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 5, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (106, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 6, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (107, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 7, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (108, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 8, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (109, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 9, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (110, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 10, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (111, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 11, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (112, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', 12, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (113, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (114, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (115, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (116, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (117, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (118, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (119, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (120, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (121, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (122, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (123, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (124, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (125, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (126, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (127, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (128, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (129, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (130, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (131, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (132, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (133, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (134, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (135, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (136, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (137, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (138, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (139, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (140, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (141, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (142, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (143, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (144, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (145, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (146, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (147, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (148, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (149, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (150, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (151, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (152, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (153, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (154, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (155, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (156, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (157, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (158, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (159, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (160, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (161, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (162, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (163, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (164, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (165, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (166, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (167, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (168, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (169, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (170, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (171, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (172, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (173, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (174, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (175, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (176, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (177, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (178, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (179, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (180, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (181, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (182, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (183, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (184, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (185, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (186, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (187, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (188, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (189, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (190, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (191, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (192, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (193, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (194, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (195, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (196, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (197, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (198, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (199, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (200, N'dd6c4874-2417-432a-8b61-36b8e9f23e4d', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (201, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (202, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 6, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (203, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (204, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 6, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (205, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 7, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (206, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (207, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 7, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (208, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (209, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (210, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (211, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (212, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (213, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (214, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (215, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (216, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (217, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (218, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (219, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', 8, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (220, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (221, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (222, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (223, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (224, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (225, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (226, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (227, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (228, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (229, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (230, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (231, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (232, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (233, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (234, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (235, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (236, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (237, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (238, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (239, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (240, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (241, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (242, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (243, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (244, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (245, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (246, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (247, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (248, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (249, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (250, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (251, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (252, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (253, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (254, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (255, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (256, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (257, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (258, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (259, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (260, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (261, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (262, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (263, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (264, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (265, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (266, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (267, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (268, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (269, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (270, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (271, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (272, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (273, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (274, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (275, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (276, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (277, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (278, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (279, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (280, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (281, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (282, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (283, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (284, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (285, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (286, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (287, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (288, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (289, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (290, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (291, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (292, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (293, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (294, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (295, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (296, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (297, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (298, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (299, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (300, N'7e3c825a-fd80-4596-87ad-e3d7690f0cb3', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (301, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (302, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 6, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (303, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (304, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 6, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (305, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 7, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (306, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (307, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 7, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (308, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (309, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (310, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (311, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (312, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (313, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (314, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (315, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (316, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (317, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (318, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (319, N'9d04aecf-f1fd-452a-b385-19fb079cd535', 8, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (320, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (321, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (322, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (323, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (324, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (325, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (326, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (327, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (328, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (329, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (330, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (331, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (332, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (333, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (334, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (335, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (336, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (337, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (338, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (339, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (340, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (341, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (342, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (343, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (344, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (345, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (346, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (347, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (348, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (349, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (350, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (351, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (352, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (353, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (354, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (355, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (356, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (357, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (358, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (359, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (360, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (361, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (362, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (363, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (364, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (365, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (366, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (367, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (368, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (369, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (370, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (371, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (372, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (373, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (374, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (375, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (376, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (377, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (378, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (379, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (380, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (381, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (382, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (383, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (384, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (385, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (386, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (387, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (388, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (389, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (390, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (391, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (392, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (393, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (394, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (395, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (396, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (397, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (398, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (399, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (400, N'9d04aecf-f1fd-452a-b385-19fb079cd535', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (401, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', 5, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (402, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', 5, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (403, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', 5, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (404, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', 5, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (405, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', 5, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (406, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (407, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (408, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (409, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (410, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (411, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (412, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (413, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (414, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (415, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (416, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (417, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (418, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (419, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (420, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (421, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (422, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (423, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (424, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (425, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (426, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (427, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (428, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (429, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (430, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (431, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (432, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (433, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (434, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (435, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (436, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (437, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (438, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (439, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (440, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (441, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (442, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (443, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (444, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (445, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (446, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (447, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (448, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (449, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (450, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (451, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (452, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (453, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (454, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (455, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (456, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (457, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (458, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (459, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (460, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (461, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (462, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (463, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (464, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (465, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (466, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (467, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (468, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (469, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (470, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (471, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (472, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (473, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (474, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (475, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (476, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (477, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (478, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (479, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (480, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (481, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (482, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (483, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (484, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (485, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (486, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (487, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (488, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (489, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (490, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (491, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (492, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (493, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (494, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (495, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (496, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (497, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (498, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (499, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (500, N'e2b60190-647d-448d-b56f-c8fe82d73bb4', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (501, N'243395e5-d4ed-476d-844d-28de84a5be9f', 1, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (502, N'243395e5-d4ed-476d-844d-28de84a5be9f', 6, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (503, N'243395e5-d4ed-476d-844d-28de84a5be9f', 2, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (504, N'243395e5-d4ed-476d-844d-28de84a5be9f', 3, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (505, N'243395e5-d4ed-476d-844d-28de84a5be9f', 6, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (506, N'243395e5-d4ed-476d-844d-28de84a5be9f', 4, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (507, N'243395e5-d4ed-476d-844d-28de84a5be9f', 4, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (508, N'243395e5-d4ed-476d-844d-28de84a5be9f', 5, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (509, N'243395e5-d4ed-476d-844d-28de84a5be9f', 5, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (510, N'243395e5-d4ed-476d-844d-28de84a5be9f', 7, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (511, N'243395e5-d4ed-476d-844d-28de84a5be9f', 7, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (512, N'243395e5-d4ed-476d-844d-28de84a5be9f', 9, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (513, N'243395e5-d4ed-476d-844d-28de84a5be9f', 8, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (514, N'243395e5-d4ed-476d-844d-28de84a5be9f', 7, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (515, N'243395e5-d4ed-476d-844d-28de84a5be9f', 7, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (516, N'243395e5-d4ed-476d-844d-28de84a5be9f', 7, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (517, N'243395e5-d4ed-476d-844d-28de84a5be9f', 11, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (518, N'243395e5-d4ed-476d-844d-28de84a5be9f', 7, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (519, N'243395e5-d4ed-476d-844d-28de84a5be9f', 0, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (520, N'243395e5-d4ed-476d-844d-28de84a5be9f', 3, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (521, N'243395e5-d4ed-476d-844d-28de84a5be9f', 4, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (522, N'243395e5-d4ed-476d-844d-28de84a5be9f', 5, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (523, N'243395e5-d4ed-476d-844d-28de84a5be9f', 7, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (524, N'243395e5-d4ed-476d-844d-28de84a5be9f', 6, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (525, N'243395e5-d4ed-476d-844d-28de84a5be9f', 6, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (526, N'243395e5-d4ed-476d-844d-28de84a5be9f', 8, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (527, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (528, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (529, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (530, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (531, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (532, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (533, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (534, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (535, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (536, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (537, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (538, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (539, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (540, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (541, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (542, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (543, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (544, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (545, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (546, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (547, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (548, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (549, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (550, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (551, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (552, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (553, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (554, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (555, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (556, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (557, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (558, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (559, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (560, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (561, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (562, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (563, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (564, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (565, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (566, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (567, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (568, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (569, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (570, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (571, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (572, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (573, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (574, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (575, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (576, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (577, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (578, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (579, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (580, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (581, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (582, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (583, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (584, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (585, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (586, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (587, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (588, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (589, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (590, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (591, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (592, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (593, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (594, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (595, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (596, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (597, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (598, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (599, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (600, N'243395e5-d4ed-476d-844d-28de84a5be9f', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (601, N'0748298a-375a-4634-ac84-be9df2818254', 1, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (602, N'0748298a-375a-4634-ac84-be9df2818254', 2, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (603, N'0748298a-375a-4634-ac84-be9df2818254', 3, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (604, N'0748298a-375a-4634-ac84-be9df2818254', 4, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (605, N'0748298a-375a-4634-ac84-be9df2818254', 5, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (606, N'0748298a-375a-4634-ac84-be9df2818254', 7, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (607, N'0748298a-375a-4634-ac84-be9df2818254', 6, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (608, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (609, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (610, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (611, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (612, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (613, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (614, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (615, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (616, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (617, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (618, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (619, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (620, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (621, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (622, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (623, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (624, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (625, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (626, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (627, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (628, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (629, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (630, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (631, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (632, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (633, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (634, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (635, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (636, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (637, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (638, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (639, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (640, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (641, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (642, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (643, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (644, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (645, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (646, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (647, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (648, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (649, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (650, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (651, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (652, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (653, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (654, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (655, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (656, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (657, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (658, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (659, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (660, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (661, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (662, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (663, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (664, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (665, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (666, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (667, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (668, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (669, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (670, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (671, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (672, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (673, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (674, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (675, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (676, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (677, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (678, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (679, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (680, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (681, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (682, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (683, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (684, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (685, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (686, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (687, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (688, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (689, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (690, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (691, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (692, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (693, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (694, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (695, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (696, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (697, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (698, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (699, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (700, N'0748298a-375a-4634-ac84-be9df2818254', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (701, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', 1, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (702, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', 2, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (703, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', 3, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (704, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', 4, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (705, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', 5, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (706, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', 7, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (707, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', 6, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (708, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (709, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (710, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (711, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (712, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (713, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (714, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (715, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (716, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (717, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (718, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (719, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (720, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (721, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (722, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (723, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (724, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (725, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (726, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (727, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (728, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (729, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (730, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (731, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (732, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (733, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (734, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (735, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (736, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (737, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (738, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (739, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (740, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (741, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (742, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (743, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (744, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (745, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (746, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (747, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (748, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (749, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (750, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (751, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (752, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (753, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (754, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (755, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (756, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (757, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (758, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (759, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (760, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (761, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (762, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (763, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (764, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (765, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (766, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (767, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (768, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (769, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (770, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (771, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (772, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (773, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (774, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (775, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (776, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (777, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (778, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (779, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (780, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (781, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (782, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (783, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (784, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (785, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (786, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (787, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (788, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (789, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (790, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (791, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (792, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (793, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (794, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (795, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (796, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (797, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (798, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (799, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (800, N'9e12e9fd-e44e-4e2d-bf76-f8aab63c7246', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (801, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (802, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (803, N'a875846b-0112-49a3-a8be-394e72ce2a91', 4, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (804, N'a875846b-0112-49a3-a8be-394e72ce2a91', 4, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (805, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (806, N'a875846b-0112-49a3-a8be-394e72ce2a91', 4, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (807, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (808, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (809, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (810, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (811, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (812, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (813, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (814, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (815, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (816, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (817, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (818, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (819, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (820, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (821, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (822, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (823, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (824, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (825, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (826, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (827, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (828, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (829, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (830, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (831, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (832, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (833, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (834, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (835, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (836, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (837, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (838, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (839, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (840, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (841, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (842, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (843, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (844, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (845, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (846, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (847, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (848, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (849, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (850, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (851, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (852, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (853, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (854, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (855, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (856, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (857, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (858, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (859, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (860, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (861, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (862, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (863, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (864, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (865, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (866, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (867, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (868, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (869, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (870, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (871, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (872, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (873, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (874, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (875, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (876, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (877, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (878, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (879, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (880, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (881, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (882, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (883, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (884, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (885, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (886, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (887, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (888, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (889, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (890, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (891, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (892, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (893, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (894, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (895, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (896, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (897, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (898, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (899, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (900, N'a875846b-0112-49a3-a8be-394e72ce2a91', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (901, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (902, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (903, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (904, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (905, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (906, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (907, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (908, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (909, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (910, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (911, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (912, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (913, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (914, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (915, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (916, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (917, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (918, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (919, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (920, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (921, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (922, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (923, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (924, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (925, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (926, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (927, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (928, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (929, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (930, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (931, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (932, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (933, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (934, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (935, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (936, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (937, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (938, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (939, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (940, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (941, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (942, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (943, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (944, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (945, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (946, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (947, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (948, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (949, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (950, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (951, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (952, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (953, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (954, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (955, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (956, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (957, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (958, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (959, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (960, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (961, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (962, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (963, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (964, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (965, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (966, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (967, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (968, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (969, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (970, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (971, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (972, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (973, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (974, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (975, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (976, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (977, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (978, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (979, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (980, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (981, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (982, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (983, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (984, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (985, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (986, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (987, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (988, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (989, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (990, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (991, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (992, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (993, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (994, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (995, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (996, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (997, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (998, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (999, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1000, N'3be8f5d5-88d1-43a5-bedb-98df6e02b029', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1001, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1002, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1003, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1004, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1005, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1006, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1007, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1008, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1009, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1010, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1011, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1012, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1013, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1014, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1015, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1016, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1017, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1018, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1019, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1020, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1021, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1022, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1023, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1024, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1025, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1026, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1027, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1028, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1029, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1030, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1031, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1032, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1033, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1034, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1035, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1036, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1037, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1038, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1039, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1040, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1041, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1042, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1043, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1044, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1045, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1046, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1047, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1048, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1049, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1050, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1051, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1052, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1053, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1054, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1055, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1056, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1057, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1058, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1059, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1060, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1061, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1062, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1063, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1064, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1065, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1066, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1067, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1068, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1069, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1070, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1071, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1072, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1073, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1074, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1075, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1076, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1077, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1078, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1079, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1080, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1081, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1082, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1083, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1084, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1085, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1086, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1087, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1088, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1089, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1090, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1091, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1092, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1093, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1094, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1095, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1096, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1097, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1098, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1099, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1100, N'bf53051f-ac2a-4e10-8fb8-d5ddd247e765', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1101, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1102, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1103, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1104, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1105, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1106, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1107, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1108, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1109, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1110, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1111, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1112, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1113, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1114, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1115, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1116, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1117, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1118, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1119, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1120, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1121, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1122, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1123, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1124, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1125, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1126, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1127, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1128, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1129, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1130, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1131, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1132, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1133, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1134, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1135, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1136, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1137, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1138, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1139, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1140, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1141, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1142, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1143, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1144, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1145, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1146, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1147, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1148, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1149, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1150, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1151, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1152, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1153, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1154, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1155, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1156, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1157, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1158, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1159, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1160, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1161, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1162, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1163, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1164, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1165, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1166, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1167, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1168, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1169, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1170, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1171, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1172, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1173, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1174, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1175, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1176, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1177, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1178, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1179, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1180, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1181, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1182, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1183, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1184, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1185, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1186, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1187, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1188, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1189, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1190, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1191, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1192, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1193, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1194, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1195, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1196, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1197, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1198, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1199, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1200, N'7ae5fe1b-abdd-4615-b8ee-bc3fc575279f', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1201, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1202, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1203, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1204, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1205, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1206, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1207, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1208, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1209, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1210, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1211, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1212, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1213, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1214, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1215, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1216, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1217, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1218, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1219, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1220, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1221, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1222, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1223, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1224, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1225, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1226, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1227, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1228, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1229, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1230, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1231, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1232, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1233, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1234, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1235, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1236, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1237, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1238, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1239, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1240, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1241, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1242, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1243, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1244, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1245, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1246, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1247, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1248, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1249, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1250, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1251, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1252, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1253, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1254, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1255, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1256, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1257, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1258, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1259, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1260, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1261, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1262, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1263, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1264, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1265, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1266, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1267, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1268, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1269, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1270, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1271, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1272, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1273, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1274, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1275, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1276, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1277, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1278, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1279, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1280, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1281, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1282, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1283, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1284, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1285, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1286, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1287, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1288, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1289, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1290, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1291, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1292, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1293, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1294, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1295, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1296, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1297, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1298, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1299, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1300, N'ec7a6840-a866-4d41-8e3e-a50a7a12b4c3', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1301, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1302, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1303, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1304, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1305, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1306, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1307, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1308, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1309, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1310, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1311, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1312, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1313, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1314, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1315, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1316, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1317, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1318, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1319, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1320, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1321, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1322, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1323, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1324, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1325, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1326, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1327, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1328, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1329, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1330, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1331, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1332, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1333, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1334, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1335, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1336, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1337, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1338, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1339, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1340, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1341, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1342, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1343, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1344, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1345, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1346, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1347, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1348, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1349, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1350, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1351, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1352, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1353, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1354, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1355, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1356, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1357, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1358, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1359, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1360, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1361, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1362, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1363, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1364, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1365, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1366, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1367, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1368, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1369, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1370, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1371, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1372, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1373, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1374, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1375, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1376, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1377, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1378, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1379, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1380, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1381, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1382, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1383, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1384, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1385, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1386, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1387, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1388, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1389, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1390, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1391, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1392, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1393, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1394, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1395, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1396, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1397, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1398, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1399, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1400, N'45cccd85-81bb-4a99-8ee6-a8948aab6b55', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1401, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1402, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1403, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1404, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1405, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1406, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1407, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1408, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1409, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1410, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1411, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1412, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1413, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1414, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1415, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1416, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1417, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1418, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1419, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1420, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1421, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1422, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1423, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1424, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1425, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1426, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1427, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1428, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1429, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1430, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1431, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1432, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1433, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1434, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1435, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1436, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1437, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1438, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1439, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1440, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1441, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1442, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1443, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1444, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1445, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1446, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1447, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1448, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1449, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1450, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1451, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1452, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1453, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1454, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1455, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1456, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1457, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1458, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1459, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1460, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1461, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1462, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1463, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1464, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1465, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1466, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1467, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1468, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1469, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1470, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1471, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1472, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1473, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1474, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1475, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1476, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1477, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1478, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1479, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1480, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1481, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1482, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1483, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1484, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1485, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1486, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1487, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1488, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1489, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1490, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1491, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1492, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1493, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1494, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1495, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1496, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1497, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1498, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1499, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1500, N'891a475e-a00c-4e25-93fe-7dcfd7919bd9', NULL, 100)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1501, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 1)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1502, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 2)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1503, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 3)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1504, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 4)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1505, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 5)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1506, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 6)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1507, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 7)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1508, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 8)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1509, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 9)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1510, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 10)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1511, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 11)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1512, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 12)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1513, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 13)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1514, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 14)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1515, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 15)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1516, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 16)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1517, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 17)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1518, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 18)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1519, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 19)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1520, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 20)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1521, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 21)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1522, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 22)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1523, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 23)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1524, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 24)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1525, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 25)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1526, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 26)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1527, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 27)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1528, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 28)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1529, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 29)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1530, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 30)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1531, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 31)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1532, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 32)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1533, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 33)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1534, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 34)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1535, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 35)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1536, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 36)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1537, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 37)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1538, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 38)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1539, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 39)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1540, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 40)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1541, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 41)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1542, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 42)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1543, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 43)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1544, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 44)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1545, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 45)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1546, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 46)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1547, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 47)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1548, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 48)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1549, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 49)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1550, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 50)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1551, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 51)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1552, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 52)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1553, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 53)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1554, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 54)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1555, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 55)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1556, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 56)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1557, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 57)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1558, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 58)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1559, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 59)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1560, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 60)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1561, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 61)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1562, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 62)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1563, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 63)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1564, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 64)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1565, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 65)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1566, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 66)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1567, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 67)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1568, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 68)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1569, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 69)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1570, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 70)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1571, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 71)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1572, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 72)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1573, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 73)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1574, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 74)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1575, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 75)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1576, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 76)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1577, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 77)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1578, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 78)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1579, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 79)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1580, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 80)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1581, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 81)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1582, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 82)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1583, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 83)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1584, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 84)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1585, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 85)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1586, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 86)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1587, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 87)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1588, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 88)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1589, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 89)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1590, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 90)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1591, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 91)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1592, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 92)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1593, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 93)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1594, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 94)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1595, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 95)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1596, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 96)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1597, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 97)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1598, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 98)
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1599, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferenceTransaction] ([WaterfallStartPreferenceTransactionID], [WaterfallStartPreferencesGroupID], [Position], [Rank]) VALUES (1600, N'82eb610d-6e7f-4c53-84ba-64861ed9b8c6', NULL, 100)
SET IDENTITY_INSERT [dbo].[WaterfallStartPreferenceTransaction] OFF
/****** Object:  Index [IX_FK_ApplicationMenu1_ApplicationMenu1]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_ApplicationMenu1_ApplicationMenu1] ON [dbo].[ApplicationMenu]
(
	[ParentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MeetSetup_BaseCounty]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MeetSetup_BaseCounty] ON [dbo].[MeetSetups]
(
	[BaseCountyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MeetSetup_MeetArena]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MeetSetup_MeetArena] ON [dbo].[MeetSetups]
(
	[MeetArenaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MeetSetup_MeetClass]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MeetSetup_MeetClass] ON [dbo].[MeetSetups]
(
	[MeetClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MeetSetup_MeetKind]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MeetSetup_MeetKind] ON [dbo].[MeetSetups]
(
	[MeetKindID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MeetSetup_MeetStyle]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MeetSetup_MeetStyle] ON [dbo].[MeetSetups]
(
	[MeetStyleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MeetSetup_MeetType]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MeetSetup_MeetType] ON [dbo].[MeetSetups]
(
	[MeetTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MeetSetup_MeetTypeDivision]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MeetSetup_MeetTypeDivision] ON [dbo].[MeetSetups]
(
	[MeetTypeDivisionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MeetTypeDivision_MeetType]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MeetTypeDivision_MeetType] ON [dbo].[MeetTypeDivision]
(
	[MeetTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_MenuInRole_ApplicationMenu]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MenuInRole_ApplicationMenu] ON [dbo].[MenuInRole]
(
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_FK_MenuInRole_Role]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_MenuInRole_Role] ON [dbo].[MenuInRole]
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_SchoolDetail_MeetSetup]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_SchoolDetail_MeetSetup] ON [dbo].[SchoolDetail]
(
	[MeetSetupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_FK_UserInRole_Role]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_UserInRole_Role] ON [dbo].[UserInRole]
(
	[RoleGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_FK_UserInRole_User]    Script Date: 1/5/2019 1:17:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_UserInRole_User] ON [dbo].[UserInRole]
(
	[UserGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApplicationMenu]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1] FOREIGN KEY([ParentID])
REFERENCES [dbo].[ApplicationMenu] ([MenuID])
GO
ALTER TABLE [dbo].[ApplicationMenu] CHECK CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1]
GO
ALTER TABLE [dbo].[AthleticPreferencesTransaction]  WITH CHECK ADD  CONSTRAINT [FK_AthleticPreferencesTransaction_AthletePreferences] FOREIGN KEY([AthleticPreferencesID])
REFERENCES [dbo].[AthletePreferences] ([AthletePreferencesID])
GO
ALTER TABLE [dbo].[AthleticPreferencesTransaction] CHECK CONSTRAINT [FK_AthleticPreferencesTransaction_AthletePreferences]
GO
ALTER TABLE [dbo].[CompetitorNumbersTransaction]  WITH CHECK ADD  CONSTRAINT [FK_CompetitorNumbersTransaction_CompetitorNumbers] FOREIGN KEY([CompetitorNumbersID])
REFERENCES [dbo].[CompetitorNumbers] ([CompetitorNumberID])
GO
ALTER TABLE [dbo].[CompetitorNumbersTransaction] CHECK CONSTRAINT [FK_CompetitorNumbersTransaction_CompetitorNumbers]
GO
ALTER TABLE [dbo].[DualMeetTransaction]  WITH CHECK ADD  CONSTRAINT [FK_DualMeetTransaction_SchoolDetail] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[SchoolDetail] ([SchoolID])
GO
ALTER TABLE [dbo].[DualMeetTransaction] CHECK CONSTRAINT [FK_DualMeetTransaction_SchoolDetail]
GO
ALTER TABLE [dbo].[MeetSetupAthletePreferences]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupAthletePreferences_AthletePreferences] FOREIGN KEY([AthletePreferencesID])
REFERENCES [dbo].[AthletePreferences] ([AthletePreferencesID])
GO
ALTER TABLE [dbo].[MeetSetupAthletePreferences] CHECK CONSTRAINT [FK_MeetSetupAthletePreferences_AthletePreferences]
GO
ALTER TABLE [dbo].[MeetSetupAthletePreferences]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupAthletePreferences_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupAthletePreferences] CHECK CONSTRAINT [FK_MeetSetupAthletePreferences_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupCompetitorNumber]  WITH CHECK ADD  CONSTRAINT [FK_MeetCompetitorNumber_CompetitorNumbers] FOREIGN KEY([CompetitorNumberID])
REFERENCES [dbo].[CompetitorNumbers] ([CompetitorNumberID])
GO
ALTER TABLE [dbo].[MeetSetupCompetitorNumber] CHECK CONSTRAINT [FK_MeetCompetitorNumber_CompetitorNumbers]
GO
ALTER TABLE [dbo].[MeetSetupCompetitorNumber]  WITH CHECK ADD  CONSTRAINT [FK_MeetCompetitorNumber_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupCompetitorNumber] CHECK CONSTRAINT [FK_MeetCompetitorNumber_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupDualMeet]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupDualMeet_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupDualMeet] CHECK CONSTRAINT [FK_MeetSetupDualMeet_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupDualMeet]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupDualMeet_SchoolDetail] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[SchoolDetail] ([SchoolID])
GO
ALTER TABLE [dbo].[MeetSetupDualMeet] CHECK CONSTRAINT [FK_MeetSetupDualMeet_SchoolDetail]
GO
ALTER TABLE [dbo].[MeetSetupRelayPreferences]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupRelayPreferences_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupRelayPreferences] CHECK CONSTRAINT [FK_MeetSetupRelayPreferences_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupRelayPreferences]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupRelayPreferences_RelayPreferences] FOREIGN KEY([RelayPreferencesID])
REFERENCES [dbo].[RelayPreferences] ([RelayPreferencesID])
GO
ALTER TABLE [dbo].[MeetSetupRelayPreferences] CHECK CONSTRAINT [FK_MeetSetupRelayPreferences_RelayPreferences]
GO
ALTER TABLE [dbo].[MeetSetupRendomizationRule]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupRendomizationRule_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupRendomizationRule] CHECK CONSTRAINT [FK_MeetSetupRendomizationRule_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetups]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_BaseCounty] FOREIGN KEY([BaseCountyID])
REFERENCES [dbo].[BaseCounty] ([BaseCountyID])
GO
ALTER TABLE [dbo].[MeetSetups] CHECK CONSTRAINT [FK_MeetSetup_BaseCounty]
GO
ALTER TABLE [dbo].[MeetSetups]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetArena] FOREIGN KEY([MeetArenaID])
REFERENCES [dbo].[MeetArena] ([MeetArenaID])
GO
ALTER TABLE [dbo].[MeetSetups] CHECK CONSTRAINT [FK_MeetSetup_MeetArena]
GO
ALTER TABLE [dbo].[MeetSetups]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetClass] FOREIGN KEY([MeetClassID])
REFERENCES [dbo].[MeetClass] ([MeetClassID])
GO
ALTER TABLE [dbo].[MeetSetups] CHECK CONSTRAINT [FK_MeetSetup_MeetClass]
GO
ALTER TABLE [dbo].[MeetSetups]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetKind] FOREIGN KEY([MeetKindID])
REFERENCES [dbo].[MeetKind] ([MeetKindID])
GO
ALTER TABLE [dbo].[MeetSetups] CHECK CONSTRAINT [FK_MeetSetup_MeetKind]
GO
ALTER TABLE [dbo].[MeetSetups]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetStyle] FOREIGN KEY([MeetStyleID])
REFERENCES [dbo].[MeetStyle] ([MeetStyleID])
GO
ALTER TABLE [dbo].[MeetSetups] CHECK CONSTRAINT [FK_MeetSetup_MeetStyle]
GO
ALTER TABLE [dbo].[MeetSetups]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetType] FOREIGN KEY([MeetTypeID])
REFERENCES [dbo].[MeetType] ([MeetTypeID])
GO
ALTER TABLE [dbo].[MeetSetups] CHECK CONSTRAINT [FK_MeetSetup_MeetType]
GO
ALTER TABLE [dbo].[MeetSetups]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetTypeDivision] FOREIGN KEY([MeetTypeDivisionID])
REFERENCES [dbo].[MeetTypeDivision] ([MeetTypeDivisionID])
GO
ALTER TABLE [dbo].[MeetSetups] CHECK CONSTRAINT [FK_MeetSetup_MeetTypeDivision]
GO
ALTER TABLE [dbo].[MeetSetupSeedingRules]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupSeedingRules_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupSeedingRules] CHECK CONSTRAINT [FK_MeetSetupSeedingRules_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupWaterfallStart]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupWaterfallStart_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupWaterfallStart] CHECK CONSTRAINT [FK_MeetSetupWaterfallStart_MeetSetup]
GO
ALTER TABLE [dbo].[MeetStandardAlleyPrefrences]  WITH CHECK ADD  CONSTRAINT [FK_MeetStandardAlleyPrefrences_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetStandardAlleyPrefrences] CHECK CONSTRAINT [FK_MeetStandardAlleyPrefrences_MeetSetup]
GO
ALTER TABLE [dbo].[MeetTypeDivision]  WITH CHECK ADD  CONSTRAINT [FK_MeetTypeDivision_MeetType] FOREIGN KEY([MeetTypeID])
REFERENCES [dbo].[MeetType] ([MeetTypeID])
GO
ALTER TABLE [dbo].[MeetTypeDivision] CHECK CONSTRAINT [FK_MeetTypeDivision_MeetType]
GO
ALTER TABLE [dbo].[MenuInRole]  WITH CHECK ADD  CONSTRAINT [FK_MenuInRole_ApplicationMenu] FOREIGN KEY([MenuID])
REFERENCES [dbo].[ApplicationMenu] ([MenuID])
GO
ALTER TABLE [dbo].[MenuInRole] CHECK CONSTRAINT [FK_MenuInRole_ApplicationMenu]
GO
ALTER TABLE [dbo].[MenuInRole]  WITH CHECK ADD  CONSTRAINT [FK_MenuInRole_Role] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([RoleGuid])
GO
ALTER TABLE [dbo].[MenuInRole] CHECK CONSTRAINT [FK_MenuInRole_Role]
GO
ALTER TABLE [dbo].[RelayPreferencesTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RelayPreferencesTransaction_RelayPreferences] FOREIGN KEY([RelayPreferencesID])
REFERENCES [dbo].[RelayPreferences] ([RelayPreferencesID])
GO
ALTER TABLE [dbo].[RelayPreferencesTransaction] CHECK CONSTRAINT [FK_RelayPreferencesTransaction_RelayPreferences]
GO
ALTER TABLE [dbo].[SchoolDetail]  WITH CHECK ADD  CONSTRAINT [FK_SchoolDetail_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetups] ([MeetSetupID])
GO
ALTER TABLE [dbo].[SchoolDetail] CHECK CONSTRAINT [FK_SchoolDetail_MeetSetup]
GO
ALTER TABLE [dbo].[UserInRole]  WITH CHECK ADD  CONSTRAINT [FK_UserInRole_Role] FOREIGN KEY([RoleGuid])
REFERENCES [dbo].[Role] ([RoleGuid])
GO
ALTER TABLE [dbo].[UserInRole] CHECK CONSTRAINT [FK_UserInRole_Role]
GO
ALTER TABLE [dbo].[UserInRole]  WITH CHECK ADD  CONSTRAINT [FK_UserInRole_User] FOREIGN KEY([UserGuid])
REFERENCES [dbo].[User] ([UserGuid])
GO
ALTER TABLE [dbo].[UserInRole] CHECK CONSTRAINT [FK_UserInRole_User]
GO
/****** Object:  StoredProcedure [dbo].[GetRelayPrefrencesGroupData]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Devender Sharma
-- Create date: 30 December 2018
-- Description:	To get the Relay Preferences to Assign in Meet Setup
-- =============================================
CREATE PROCEDURE [dbo].[GetRelayPrefrencesGroupData] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  declare @InitialGroup nvarchar(30)
  SET @InitialGroup='RelayPrefrences';
	
    -- Insert statements for procedure here
	SELECT distinct @InitialGroup + Cast([AthleteRelayPreferencesID]AS VARCHAR(max)) as RelayGroupID
      ,AthletePreferences.AthletePreferencesName
      ,CompetitorNumbers.CompetitorNumbersName
      ,RelayPreferences.RelayPreferencesName
  FROM [AthleticsDB].[dbo].[AthleteRelayPreferencesTransaction]

  INNER JOIN [AthleticsDB].[dbo].AthleticPreferencesTransaction 
  on AthleticPreferencesTransaction.AthleticPreferencesGroupID = AthleteRelayPreferencesTransaction.PreferencesTransactionGroupID

   INNER JOIN [AthleticsDB].[dbo].AthletePreferences 
  on AthletePreferences.AthletePreferencesID = AthleticPreferencesTransaction.AthleticPreferencesID

  INNER JOIN [AthleticsDB].[dbo].CompetitorNumbersTransaction 
  on [AthleticsDB].[dbo].CompetitorNumbersTransaction.CompetitorNumbersGroupID =AthleteRelayPreferencesTransaction.CompetitorNumberTransactionGroupID

  INNER JOIN [AthleticsDB].[dbo].CompetitorNumbers 
  on CompetitorNumbers.CompetitorNumberID = CompetitorNumbersTransaction.CompetitorNumbersID


   INNER JOIN [AthleticsDB].[dbo].RelayPreferencesTransaction 
  on [AthleticsDB].[dbo].RelayPreferencesTransaction.RelayPreferencesGroupID =AthleteRelayPreferencesTransaction.RelayPreferencesTransactionGroupID
   INNER JOIN [AthleticsDB].[dbo].RelayPreferences 
  on [AthleticsDB].[dbo].RelayPreferences.RelayPreferencesID =RelayPreferencesTransaction.RelayPreferencesID

END



GO
/****** Object:  StoredProcedure [dbo].[GetSeedingPreferencesGroupDetail]    Script Date: 1/5/2019 1:17:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSeedingPreferencesGroupDetail]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @InitialGroup nvarchar(30)
	SET @InitialGroup='SeedPrefrences';
	
    -- Insert statements for procedure here
    -- Insert statements for procedure here
	SELECT distinct  @InitialGroup +  Cast(SeedingPrefrencesID AS VARCHAR(max)) as SeedingGroup, SeedingPrefrencesID,Position as WaterfallStartPosition,[Rank] as WaterfallStartRank,
	   [AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnFirstValue] as StandardLanePrefrencesRowZeroColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnSecondValue] as StandardLanePrefrencesRowZeroColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnThirdValue] as StandardLanePrefrencesRowZeroColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnForthValue] as StandardLanePrefrencesRowZeroColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnFifthValue] as StandardLanePrefrencesRowZeroColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnSixthValue] as StandardLanePrefrencesRowZeroColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnSeventhValue] as StandardLanePrefrencesRowZeroColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnEightthValue] as StandardLanePrefrencesRowZeroColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnNinthValue] as StandardLanePrefrencesRowZeroColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnTenthValue] as StandardLanePrefrencesRowZeroColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnFirstValue] as StandardLanePrefrencesRowOneColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnSecondValue] as StandardLanePrefrencesRowOneColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnThirdValue] as StandardLanePrefrencesRowOneColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnForthValue]  as StandardLanePrefrencesRowOneColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnFifthValue] as StandardLanePrefrencesRowOneColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnSixthValue] as StandardLanePrefrencesRowOneColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnSeventhValue] as StandardLanePrefrencesRowOneColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnEightthValue] as StandardLanePrefrencesRowOneColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnNinthValue] as StandardLanePrefrencesRowOneColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnTenthValue] as StandardLanePrefrencesRowOneColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnFirstValue] as StandardLanePrefrencesRowSecondColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnSecondValue] as StandardLanePrefrencesRowSecondColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnThirdValue] as StandardLanePrefrencesRowSecondColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnForthValue] as StandardLanePrefrencesRowSecondColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnFifthValue] as StandardLanePrefrencesRowSecondColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnSixthValue] as StandardLanePrefrencesRowSecondColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnSeventhValue] as StandardLanePrefrencesRowSecondColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnEightthValue] as StandardLanePrefrencesRowSecondColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnNinthValue] as StandardLanePrefrencesRowSecondColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnTenthValue] as StandardLanePrefrencesRowSecondColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnFirstValue] as StandardLanePrefrencesRowThirdColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnSecondValue] as StandardLanePrefrencesRowThirdColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnThirdValue] as StandardLanePrefrencesRowThirdColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnForthValue] as StandardLanePrefrencesRowThirdColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnFifthValue] as StandardLanePrefrencesRowThirdColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnSixthValue] as StandardLanePrefrencesRowThirdColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnSeventhValue] as StandardLanePrefrencesRowThirdColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnEightthValue] as StandardLanePrefrencesRowThirdColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnNinthValue] as StandardLanePrefrencesRowThirdColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThridColumnTenthValue] as StandardLanePrefrencesRowThridColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnFirstValue] as StandardLanePrefrencesRowForthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnSecondValue] as StandardLanePrefrencesRowForthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnThirdValue] as StandardLanePrefrencesRowForthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnForthValue] as StandardLanePrefrencesRowForthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnFifthValue] as StandardLanePrefrencesRowForthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnSixthValue] as StandardLanePrefrencesRowForthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnSeventhValue] as StandardLanePrefrencesRowForthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnEightthValue] as StandardLanePrefrencesRowForthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnNinthValue] as StandardLanePrefrencesRowForthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnTenthValue] as StandardLanePrefrencesRowForthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnFirstValue] as StandardLanePrefrencesRowFifthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnSecondValue] as StandardLanePrefrencesRowFifthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnThirdValue] as StandardLanePrefrencesRowFifthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnForthValue] as StandardLanePrefrencesRowFifthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnFifthValue] as StandardLanePrefrencesRowFifthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnSixthValue] as StandardLanePrefrencesRowFifthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnSeventhValue] as StandardLanePrefrencesRowFifthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnEightthValue] as StandardLanePrefrencesRowFifthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnNinthValue] as StandardLanePrefrencesRowFifthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnTenthValue] as StandardLanePrefrencesRowFifthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnFirstValue] as StandardLanePrefrencesRowSixthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnSecondValue] as StandardLanePrefrencesRowSixthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnThirdValue] as StandardLanePrefrencesRowSixthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnForthValue] as StandardLanePrefrencesRowSixthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnFifthValue] as StandardLanePrefrencesRowSixthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnSixthValue] as StandardLanePrefrencesRowSixthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnSeventhValue] as StandardLanePrefrencesRowSixthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnEightthValue] as StandardLanePrefrencesRowSixthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnNinthValue] as StandardLanePrefrencesRowSixthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnTenthValue] as StandardLanePrefrencesRowSixthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnFirstValue] as StandardLanePrefrencesRowSeventhColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnSecondValue] as StandardLanePrefrencesRowSeventhColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnThirdValue] as StandardLanePrefrencesRowSeventhColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnForthValue] as StandardLanePrefrencesRowSeventhColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnFifthValue] as StandardLanePrefrencesRowSeventhColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnSixthValue] as StandardLanePrefrencesRowSeventhColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnSeventhValue] as StandardLanePrefrencesRowSeventhColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnEightthValue] as StandardLanePrefrencesRowSeventhColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnNinthValue] as StandardLanePrefrencesRowSeventhColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnTenthValue] as StandardLanePrefrencesRowSeventhColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnFirstValue] as StandardLanePrefrencesRowEighthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnSecondValue] as StandardLanePrefrencesRowEighthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnThirdValue] as StandardLanePrefrencesRowEighthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnForthValue] as StandardLanePrefrencesRowEighthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnFifthValue] as StandardLanePrefrencesRowEighthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnSixthValue] as StandardLanePrefrencesRowEighthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnSeventhValue] as StandardLanePrefrencesRowEighthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnEightthValue] as StandardLanePrefrencesRowEighthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnNinthValue] as StandardLanePrefrencesRowEighthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnTenthValue] as StandardLanePrefrencesRowEighthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnFirstValue] as StandardLanePrefrencesRowNineColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnSecondValue] as StandardLanePrefrencesRowNineColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnThirdValue] as StandardLanePrefrencesRowNineColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnForthValue] as StandardLanePrefrencesRowNineColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnFifthValue] as StandardLanePrefrencesRowNineColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnSixthValue] as StandardLanePrefrencesRowNineColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnSeventhValue] as StandardLanePrefrencesRowNineColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnEightthValue] as StandardLanePrefrencesRowNineColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnNinthValue] as StandardLanePrefrencesRowNineColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnTenthValue]  as StandardLanePrefrencesRowNineColumnTenthValue
	   ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnFirstValue] as StandardAlleyPrefrencesRowZeroColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnSecondValue] as StandardAlleyPrefrencesRowZeroColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnThirdValue] as StandardAlleyPrefrencesRowZeroColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnForthValue] as StandardAlleyPrefrencesRowZeroColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnFifthValue] as StandardAlleyPrefrencesRowZeroColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnSixthValue] as StandardAlleyPrefrencesRowZeroColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnSeventhValue] as StandardAlleyPrefrencesRowZeroColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnEightthValue] as StandardAlleyPrefrencesRowZeroColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnNinthValue] as StandardAlleyPrefrencesRowZeroColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnTenthValue] as StandardAlleyPrefrencesRowZeroColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnFirstValue] as StandardAlleyPrefrencesRowOneColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnSecondValue] as StandardAlleyPrefrencesRowOneColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnThirdValue] as StandardAlleyPrefrencesRowOneColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnForthValue]  as StandardAlleyPrefrencesRowOneColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnFifthValue] as StandardAlleyPrefrencesRowOneColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnSixthValue] as StandardAlleyPrefrencesRowOneColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnSeventhValue] as StandardAlleyPrefrencesRowOneColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnEightthValue] as StandardAlleyPrefrencesRowOneColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnNinthValue] as StandardAlleyPrefrencesRowOneColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnTenthValue] as StandardAlleyPrefrencesRowOneColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnFirstValue] as StandardAlleyPrefrencesRowSecondColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnSecondValue] as StandardAlleyPrefrencesRowSecondColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnThirdValue] as StandardAlleyPrefrencesRowSecondColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnForthValue] as StandardAlleyPrefrencesRowSecondColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnFifthValue] as StandardAlleyPrefrencesRowSecondColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnSixthValue] as StandardAlleyPrefrencesRowSecondColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnSeventhValue] as StandardAlleyPrefrencesRowSecondColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnEightthValue] as StandardAlleyPrefrencesRowSecondColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnNinthValue] as StandardAlleyPrefrencesRowSecondColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnTenthValue] as StandardAlleyPrefrencesRowSecondColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnFirstValue] as StandardAlleyPrefrencesRowThirdColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnSecondValue] as StandardAlleyPrefrencesRowThirdColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnThirdValue] as StandardAlleyPrefrencesRowThirdColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnForthValue] as StandardAlleyPrefrencesRowThirdColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnFifthValue] as StandardAlleyPrefrencesRowThirdColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnSixthValue] as StandardAlleyPrefrencesRowThirdColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnSeventhValue] as StandardAlleyPrefrencesRowThirdColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnEightthValue] as StandardAlleyPrefrencesRowThirdColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnNinthValue] as StandardAlleyPrefrencesRowThirdColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThridColumnTenthValue] as StandardAlleyPrefrencesRowThridColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnFirstValue] as StandardAlleyPrefrencesRowForthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnSecondValue] as StandardAlleyPrefrencesRowForthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnThirdValue] as StandardAlleyPrefrencesRowForthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnForthValue] as StandardAlleyPrefrencesRowForthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnFifthValue] as StandardAlleyPrefrencesRowForthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnSixthValue] as StandardAlleyPrefrencesRowForthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnSeventhValue] as StandardAlleyPrefrencesRowForthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnEightthValue] as StandardAlleyPrefrencesRowForthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnNinthValue] as StandardAlleyPrefrencesRowForthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnTenthValue] as StandardAlleyPrefrencesRowForthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnFirstValue] as StandardAlleyPrefrencesRowFifthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnSecondValue] as StandardAlleyPrefrencesRowFifthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnThirdValue] as StandardAlleyPrefrencesRowFifthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnForthValue] as StandardAlleyPrefrencesRowFifthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnFifthValue] as StandardAlleyPrefrencesRowFifthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnSixthValue] as StandardAlleyPrefrencesRowFifthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnSeventhValue] as StandardAlleyPrefrencesRowFifthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnEightthValue] as StandardAlleyPrefrencesRowFifthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnNinthValue] as StandardAlleyPrefrencesRowFifthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnTenthValue] as StandardAlleyPrefrencesRowFifthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnFirstValue] as StandardAlleyPrefrencesRowSixthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnSecondValue] as StandardAlleyPrefrencesRowSixthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnThirdValue] as StandardAlleyPrefrencesRowSixthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnForthValue] as StandardAlleyPrefrencesRowSixthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnFifthValue] as StandardAlleyPrefrencesRowSixthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnSixthValue] as StandardAlleyPrefrencesRowSixthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnSeventhValue] as StandardAlleyPrefrencesRowSixthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnEightthValue] as StandardAlleyPrefrencesRowSixthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnNinthValue] as StandardAlleyPrefrencesRowSixthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnTenthValue] as StandardAlleyPrefrencesRowSixthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnFirstValue] as StandardAlleyPrefrencesRowSeventhColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnSecondValue] as StandardAlleyPrefrencesRowSeventhColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnThirdValue] as StandardAlleyPrefrencesRowSeventhColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnForthValue] as StandardAlleyPrefrencesRowSeventhColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnFifthValue] as StandardAlleyPrefrencesRowSeventhColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnSixthValue] as StandardAlleyPrefrencesRowSeventhColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnSeventhValue] as StandardAlleyPrefrencesRowSeventhColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnEightthValue] as StandardAlleyPrefrencesRowSeventhColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnNinthValue] as StandardAlleyPrefrencesRowSeventhColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnTenthValue] as StandardAlleyPrefrencesRowSeventhColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnFirstValue] as StandardAlleyPrefrencesRowEighthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnSecondValue] as StandardAlleyPrefrencesRowEighthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnThirdValue] as StandardAlleyPrefrencesRowEighthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnForthValue] as StandardAlleyPrefrencesRowEighthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnFifthValue] as StandardAlleyPrefrencesRowEighthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnSixthValue] as StandardAlleyPrefrencesRowEighthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnSeventhValue] as StandardAlleyPrefrencesRowEighthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnEightthValue] as StandardAlleyPrefrencesRowEighthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnNinthValue] as StandardAlleyPrefrencesRowEighthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnTenthValue] as StandardAlleyPrefrencesRowEighthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnFirstValue] as StandardAlleyPrefrencesRowNineColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnSecondValue] as StandardAlleyPrefrencesRowNineColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnThirdValue] as StandardAlleyPrefrencesRowNineColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnForthValue] as StandardAlleyPrefrencesRowNineColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnFifthValue] as StandardAlleyPrefrencesRowNineColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnSixthValue] as StandardAlleyPrefrencesRowNineColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnSeventhValue] as StandardAlleyPrefrencesRowNineColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnEightthValue] as StandardAlleyPrefrencesRowNineColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnNinthValue] as StandardAlleyPrefrencesRowNineColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnTenthValue]  as StandardAlleyPrefrencesRowNineColumnTenthValue
	  ,[AthleticsDB].[dbo].DualMeetTransaction.LaneNumber as DualMeetLaneNumber
	   ,[AthleticsDB].[dbo].SchoolDetail.Abbr  as DualMeetSchoolAbbr
      ,[AthleticsDB].[dbo].SchoolDetail.SchoolName  as DualMeetSchoolName
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.AlternamtUserOfUnAssignedLane as DualMeetAlternateUserOfUnAssignedLane
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.StrictAssignmentAllHeats as DualMeetStrictAssignmentAllHeats
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.StrictAssignmentFastestHeatOnly as DualMeetStrictAssignmentFastestHeatOnly
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.UseLaneAssignmentsForInLaneRacesOnly as DualMeetUseLaneAssignmentsForInLaneRacesOnly
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.UserLaneOrPositionAssignmentsAbove as DualMeetUserLaneOrPositionAssignmentsAbove
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.AlternamtUserOfUnAssignedLane as DualMeetAlternamtUserOfUnAssignedLane

	  ,[AthleticsDB].[dbo].RendomizationRuleTransaction.TimedFinalEvents as RendomizationRuleTimedFinalEvents
	  ,[AthleticsDB].[dbo].RendomizationRuleTransaction.RoundFirstMultipleRound as RendomizationRuleRoundFirstMultipleRound
	  ,[AthleticsDB].[dbo].RendomizationRuleTransaction.RoundSecondThirdAndForth as RendomizationRuleRoundSecondThirdAndForth
	  ,[AthleticsDB].[dbo].RendomizationRuleTransaction.CloseGap as RendomizationRuleCloseGap

	  ,[AthleticsDB].[dbo].SeedingRulesTransaction.AllowForeignAthletesInFinal as SeedingAllowForeignAthletesInFinal
	  ,[AthleticsDB].[dbo].SeedingRulesTransaction.AllowExhibitionAthletesInFinal as SeedingRulesAllowExhibitionAthletesInFinal
	  ,[AthleticsDB].[dbo].SeedingRulesTransaction.SeedExhibitionAthletesLast as SeedingRulesApplyNCAARule1
	  ,[AthleticsDB].[dbo].SeedingRulesTransaction.ApplyNCAARule as SeedingRulesApplyNCAARule
	    ,[AthleticsDB].[dbo].SeedingRulesTransaction.UseSpecialRandomSelectMethod as SeedingRulesUseSpecialRandomSelectMethod

  FROM [AthleticsDB].[dbo].SeedingPreferencesTransaction

  INNER JOIN [AthleticsDB].[dbo].WaterfallStartPreferenceTransaction 
  on WaterfallStartPreferenceTransaction.WaterfallStartPreferencesGroupID = SeedingPreferencesTransaction.WaterfallStartPreferencesGroupID

  INNER JOIN [AthleticsDB].[dbo].StandardLanePrefrencesTransaction 
  on StandardLanePrefrencesTransaction.StandardLanePrefrencesGroupID = SeedingPreferencesTransaction.StandardLanePrefrencesGroupID
   
  
  INNER JOIN [AthleticsDB].[dbo].StandardAlleyPrefrencesTransaction 
  on [AthleticsDB].[dbo].StandardAlleyPrefrencesTransaction.StandardAlleyPrefrencesGroupID =SeedingPreferencesTransaction.StandardAlleyPrefrencesGroupID

  INNER JOIN [AthleticsDB].[dbo].DualMeetTransaction 
  on DualMeetTransaction.DualMeetGroupID = SeedingPreferencesTransaction.DualMeetGroupID

 INNER JOIN [AthleticsDB].[dbo].DualMeetAssignmentTransaction 
  on DualMeetAssignmentTransaction.DualMeetGroupID = SeedingPreferencesTransaction.DualMeetGroupID

   INNER JOIN [AthleticsDB].[dbo].SchoolDetail 
  on SchoolDetail.SchoolID = DualMeetTransaction.SchoolID

  INNER JOIN [AthleticsDB].[dbo].SeedingRulesTransaction 
  on SeedingRulesTransaction.SeedingRulesGroupID = SeedingPreferencesTransaction.SeedingRulesGroupID

INNER JOIN [AthleticsDB].[dbo].RendomizationRuleTransaction 
  on RendomizationRuleTransaction.RendomizationRuleGroupID = SeedingPreferencesTransaction.RendomizationRuleGroupID

    
END

GO
USE [master]
GO
ALTER DATABASE [AthleticsDB] SET  READ_WRITE 
GO
