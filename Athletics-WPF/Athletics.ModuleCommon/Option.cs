﻿using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.ModuleCommon
{
    public delegate void OptionChangedDelegate(Option option, bool IsSelected);
    public class Option : BindableBase
    {
        public bool RaiseEvent = true;
        public event OptionChangedDelegate OptionChanged;
        private string _Display;
        public string Display
        {
            get { return _Display; }
            set
            {
                SetProperty(ref _Display, value);
            }
        }

        private int _DisplayID;
        public int DisplayID
        {
            get { return _DisplayID; }
            set
            {
                SetProperty(ref _DisplayID, value);
            }
        }

        private string _Tag;
        public string Tag
        {
            get { return _Tag; }
            set
            {
                SetProperty(ref _Tag, value);
            }
        }

        private bool _IsSelected;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                SetProperty(ref _IsSelected, value);
                if (RaiseEvent)
                {
                    if (OptionChanged != null)
                    {
                        OptionChanged(this, value);
                    }
                }
            }
        }


        private string groupName;
        public string GroupName
        {
            get { return groupName; }
            set
            {
                SetProperty(ref groupName, value);
            }
        }
        public void SetIsSelected(bool isSelcted)
        {
            RaiseEvent = false;
            IsSelected = isSelcted;
        }

    }
}
