﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Athletics.ModuleCommon
{
    public class ReportTemplateGenerator
    {
        public static string GenerateTemplate(string reportName, string reportDesription, List<string> ColumnList)
        {
            string reprtStartupXamlns = "<FlowDocument xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" \n" +
        "xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"\n" +
                     "xmlns:xrd=\"clr-namespace:CodeReason.Reports.Document;assembly=CodeReason.Reports\" " +
                     "PageHeight=\"29.7cm\" PageWidth=\"21cm\" ColumnWidth=\"21cm\">";

            StringBuilder sb = new StringBuilder();
            sb.Append(reprtStartupXamlns);
            sb.Append("<Section Padding=\"80, 10, 40, 10\" FontSize=\"12\">");
            sb.AppendLine();
            sb.Append("<xrd:SectionDataGroup DataGroupName = \"ItemList\">");
            sb.AppendLine();
            sb.Append("<xrd:ReportProperties>");
            sb.Append("<xrd:ReportProperties.ReportName>");
            sb.Append(reportName);
            sb.Append("</xrd:ReportProperties.ReportName>");
            sb.AppendLine();
            sb.Append("<xrd:ReportProperties.ReportTitle>");
            sb.Append(reportDesription);
            sb.Append("</xrd:ReportProperties.ReportTitle>");
            sb.AppendLine();
            sb.Append("</xrd:ReportProperties>");
            sb.AppendLine();
            sb.Append("<xrd:SectionReportHeader PageHeaderHeight = \"2\" Padding = \"10,10,10,0\" FontSize = \"12\">");
            sb.AppendLine();
            sb.Append("<Table CellSpacing = \"0\">");
            sb.AppendLine();
            sb.Append("<Table.Columns>");
            sb.AppendLine();
            sb.Append("<TableColumn Width = \"*\"/>");
            sb.AppendLine();
            sb.Append("<TableColumn Width = \"*\" />");
            sb.AppendLine();
            sb.Append("</Table.Columns>");
            sb.AppendLine();
            sb.Append("<TableRowGroup>");
            sb.AppendLine();
            sb.Append("<TableRow>");
            sb.AppendLine();
            sb.AppendLine("<TableCell>");
            sb.AppendLine("<Paragraph>");
            sb.AppendLine("<xrd:InlineContextValue PropertyName = \"ReportTitle\" />");
            sb.AppendLine("</Paragraph>");
            sb.AppendLine("</TableCell>");
            sb.Append("<TableCell>");
            sb.AppendLine("<Paragraph TextAlignment = \"Right\">");
            sb.AppendLine("<xrd:InlineDocumentValue PropertyName = \"PrintDate\" Format = \"dd.MM.yyyy HH:mm:ss\"/>");
            sb.AppendLine("</Paragraph>");
            sb.AppendLine("</TableCell>");
            sb.AppendLine("</TableRow>");
            sb.AppendLine("</TableRowGroup>");
            sb.AppendLine("</Table>");
            sb.AppendLine("</xrd:SectionReportHeader>");
            ConstructContent(sb,ColumnList);
            sb.AppendLine("<xrd:SectionReportFooter PageFooterHeight = \"2\" Padding = \"10, 0, 10, 10\" FontSize = \"12\">");
            sb.AppendLine("<Table CellSpacing = \"0\">");
            sb.AppendLine("<Table.Columns>");
            sb.AppendLine("<TableColumn Width = \"*\"/>");
            sb.AppendLine("<TableColumn Width = \"*\" />");
            sb.AppendLine("</Table.Columns>");
            sb.AppendLine("<TableRowGroup>");
            sb.AppendLine("<TableRow>");
            sb.AppendLine("<TableCell>");
            sb.AppendLine("<Paragraph TextAlignment = \"Right\">");
            sb.AppendLine("Page");
            sb.AppendLine("<xrd:InlineContextValue PropertyName = \"PageNumber\" FontWeight = \"Bold\" /> of");
            sb.AppendLine("<xrd:InlineContextValue PropertyName = \"PageCount\" FontWeight = \"Bold\" />");
            sb.AppendLine("</Paragraph>");
            sb.AppendLine("</TableCell>");
            sb.AppendLine("</TableRow>");
            sb.AppendLine("</TableRowGroup>");
            sb.AppendLine("</Table>");
            sb.AppendLine("</xrd:SectionReportFooter>");
            sb.AppendLine("</xrd:SectionDataGroup>");
            sb.AppendLine("</Section>");
            sb.AppendLine("</FlowDocument>");
            return sb.ToString();
        }
        private static void ConstructContent(StringBuilder sb, List<string> columnNames)
        {
            string FontSizeName = "12";
            string FontWeightName = "Normal";
            //string FontStyleName = "Normal";
            //string FontFamilyName = "Roboto";

            sb.Append("<Table CellSpacing = \"0\" BorderBrush = \"Black\" BorderThickness = \"0.02cm\">");
            sb.AppendLine();
            sb.Append("<Table.Resources>");
            sb.AppendLine();
            //sb.Append("<!--Style for header / footer rows. -->");
            sb.AppendLine();
            sb.Append("<Style x:Key = \"headerFooterRowStyle\" TargetType = \"{x:Type TableRowGroup}\" >");
            sb.AppendLine();

            sb.Append("<Setter Property = \"FontWeight\" Value = \"" + FontWeightName + "\" />");
            sb.AppendLine();

            sb.Append("<Setter Property = \"FontSize\" Value = \"" + FontSizeName + "\" />");
            sb.AppendLine();
            sb.Append("</Style>");
            sb.AppendLine();
            //sb.Append("<!--Style for data rows. -->");
            sb.AppendLine();
            sb.Append("<Style x:Key = \"dataRowStyle\" TargetType = \"{x:Type TableRowGroup}\">");
            sb.AppendLine();
            sb.Append("<Setter Property = \"FontSize\" Value = \"" + FontSizeName + "\"/>");
            sb.AppendLine();
            sb.Append("</Style>");
            sb.AppendLine();
            sb.Append("<Style TargetType = \"{x:Type TableCell}\" >");
            sb.AppendLine();
            sb.Append("<Setter Property = \"Padding\" Value = \"0.1cm\"/>");
            sb.AppendLine();
            sb.Append("<Setter Property = \"BorderBrush\" Value = \"Black\"/>");
            sb.AppendLine();
            sb.Append("<Setter Property = \"BorderThickness\" Value = \"0.01cm\"/>");
            sb.AppendLine();
            sb.Append("</Style>");
            sb.AppendLine();
            sb.Append("</Table.Resources>");
            sb.AppendLine();
            sb.AppendLine("<Table.Columns>");
            foreach (string columnName in columnNames)
            {
                int columnPercentage = 100 / columnNames.Count();
                sb.AppendLine("<TableColumn Width = \"0." + columnPercentage + "*\" />");
            }
            sb.AppendLine("</Table.Columns>");
            sb.AppendLine("<TableRowGroup Style = \"{ StaticResource headerFooterRowStyle}\" >");
            sb.AppendLine("<TableRow>");
            foreach (string columnName in columnNames)
            {
                sb.AppendLine("<TableCell>");
                sb.AppendLine("<Paragraph TextAlignment = \"Center\">");
                sb.AppendLine("<Bold> " + columnName + "</Bold>");
                sb.AppendLine("</Paragraph>");
                sb.AppendLine("</TableCell>");
            }
            sb.AppendLine("</TableRow>");
            sb.AppendLine("</TableRowGroup>");
            sb.AppendLine("<TableRowGroup Style = \"{StaticResource dataRowStyle}\">");
            sb.AppendLine("<xrd:TableRowForDataTable TableName = \"table1\">");

            foreach (string columnName in columnNames)
            {
                sb.AppendLine("<TableCell>");
                sb.AppendLine("<Paragraph>");
                sb.AppendLine("<xrd:InlineTableCellValue PropertyName = \"" + columnName + "\"/>");
                sb.AppendLine("</Paragraph>");
                sb.AppendLine("</TableCell>");
            }
            sb.AppendLine("</xrd:TableRowForDataTable>");
            sb.AppendLine("</TableRowGroup>");
            sb.AppendLine("</Table>");
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            if(items==null)
            {
                MessageBox.Show("Please Import the record first display in report","No Record Found");
                return null;
            }
            DataTable dataTable = new DataTable("table1");
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        public static List<string> GetColumnList<T>()
        {
            List<string> stringList = new List<string>();
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                stringList.Add(prop.Name);
            }
            return stringList;
        }
    }
}
