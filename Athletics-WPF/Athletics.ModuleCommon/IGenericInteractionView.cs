﻿namespace Athletics.ModuleCommon
{
    public interface IGenericInteractionView<T>
    {
        void SetEntity(T entity);
        T GetEntity();
    }
}
