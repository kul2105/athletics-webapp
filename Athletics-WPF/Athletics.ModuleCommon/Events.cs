﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.ModuleCommon
{
    public enum Windows
    {
        Event,
        Relay,
        Athlete,
        Team,
        Seeding,

    }
    public class Events
    {
        public delegate void OpenWindowDelegate(Windows window);
        public static event OpenWindowDelegate OpenWindow;
        public static void RaiseOpenWindow(Windows windowName)
        {
            if(OpenWindow!=null)
            {
                OpenWindow(windowName);
            }
        }

    }
}
