﻿using Athletics.ControlLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.ModuleCommon
{
    public abstract class NotifyBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Raises the PropertyChanged event.
        /// </summary>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Event raised to indicate that a property value has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class TableSchema : NotifyBase
    {
        private int _TableID = -1;
        public int TableID
        {
            get { return _TableID; }
            set
            {
                _TableID = value;
                base.OnPropertyChanged("TableID");
            }
        }

        private string _TableName = string.Empty;
        public string TableName
        {
            get { return _TableName; }
            set
            {
                _TableName = value;
                base.OnPropertyChanged("TableName");
            }
        }

        private string _TableDescription = string.Empty;
        public string TableDescription
        {
            get { return _TableDescription; }
            set
            {
                _TableDescription = value;
                base.OnPropertyChanged("TableDescription");
            }
        }
        private ImpObservableCollection<ColumnSchema> _ColumnCollection = new ImpObservableCollection<ColumnSchema>();
        public ImpObservableCollection<ColumnSchema> ColumnCollection
        {
            get { return _ColumnCollection; }
            set
            {
                _ColumnCollection = value;
                base.OnPropertyChanged("ColumnCollection");
            }
        }


    }

    public class ColumnSchema : NotifyBase
    {
        private int _ColumnID = -1;
        public int ColumnID
        {
            get { return _ColumnID; }
            set
            {
                _ColumnID = value;
                base.OnPropertyChanged("ColumnID");
            }
        }

        private string _ColumnName = string.Empty;
        public string ColumnName
        {
            get { return _ColumnName; }
            set
            {
                _ColumnName = value;
                base.OnPropertyChanged("ColumnName");
            }
        }

        private bool _IsSelected = false;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                base.OnPropertyChanged("IsSelected");
            }
        }

        private bool _IsForeignKey = false;
        public bool IsForeignKey
        {
            get { return _IsForeignKey; }
            set
            {
                _IsForeignKey = value;
                base.OnPropertyChanged("IsForeignKey");
            }
        }

        private bool _IsPrimaryKey = false;
        public bool IsPrimaryKey
        {
            get { return _IsPrimaryKey; }
            set
            {
                _IsPrimaryKey = value;
                base.OnPropertyChanged("IsPrimaryKey");
            }
        }

        private string _ReferencedTableName = string.Empty;
        public string ReferencedTableName
        {
            get { return _ReferencedTableName; }
            set
            {
                _ReferencedTableName = value;
                base.OnPropertyChanged("ReferencedTableName");
            }
        }

        private string _ReferencedColumneName = string.Empty;
        public string ReferencedColumneName
        {
            get { return _ReferencedColumneName; }
            set
            {
                _ReferencedColumneName = value;
                base.OnPropertyChanged("ReferencedColumneName");
            }
        }

        private string _ReferencingColumnName = string.Empty;
        public string ReferencingColumnName
        {
            get { return _ReferencingColumnName; }
            set
            {
                _ReferencingColumnName = value;
                base.OnPropertyChanged("ReferencingColumneName");
            }
        }
        private string _ReferencingTableName = string.Empty;
        public string ReferencingTableName
        {
            get { return _ReferencingTableName; }
            set
            {
                _ReferencingTableName = value;
                base.OnPropertyChanged("ReferencingTableName");
            }
        }
    }
}
