﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Athletics.Report.Behaviours
{
    public interface IDropable
    {
        Type DataType { get; }
        void Drop(object data, System.Windows.DragEventArgs e, int index = -1);
    }
}
