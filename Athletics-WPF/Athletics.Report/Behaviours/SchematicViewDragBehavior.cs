﻿using Athletics.Report.Common;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace Athletics.Report.Behaviours
{
    public class SchematicViewDragBehavior : Behavior<FrameworkElement>
    {
        private bool isMouseClicked = false;
        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseEnter += AssociatedObject_MouseEnter;
            this.AssociatedObject.MouseLeave += new MouseEventHandler(AssociatedObject_MouseLeave);
            this.AssociatedObject.DragOver += AssociatedObject_DragOver;
        }

        void AssociatedObject_DragOver(object sender, DragEventArgs e)
        {
        }

        void AssociatedObject_MouseEnter(object sender, MouseEventArgs e)
        {
            isMouseClicked = true;
        }

        void AssociatedObject_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                if (isMouseClicked && e.LeftButton == MouseButtonState.Pressed)
                {
                    //set the item's DataContext as the data to be transferred
                    IDragable dragObject = this.AssociatedObject.DataContext as IDragable;
                    if (dragObject != null)
                    {
                        DataObject data = new DataObject();
                        bool allowDrop = true;
                        if (this.AssociatedObject.DataContext is TreeNodeViewModel)
                        {
                            TreeNodeViewModel treeviewNode = (TreeNodeViewModel)this.AssociatedObject.DataContext;
                            if (treeviewNode.Children.Count() > 0)
                            {
                                allowDrop = false;
                            }
                        }
                        if (allowDrop)
                        {
                            data.SetData(dragObject.DataType, this.AssociatedObject.DataContext);
                            System.Windows.DragDrop.DoDragDrop(this.AssociatedObject, data, DragDropEffects.Move);
                        }
                        dragObject.Dragged();
                    }
                }
                isMouseClicked = false;
            }
            catch (Exception ex)
            {
                StaticLogger.log.VirtuosoWarn(ex.Message, VirLogger.GetStamp());
            }

        }
    }
}
