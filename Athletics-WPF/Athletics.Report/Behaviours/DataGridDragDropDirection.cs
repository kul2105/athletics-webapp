﻿namespace Athletics.Report.Behaviours
{
    public enum DataGridDragDropDirection
    {
        Indeterminate = 0,
        Up = 1,
        Down = 2
    }
}
