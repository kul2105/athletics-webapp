﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Athletics.Report.Behaviours
{
    interface IDragable
    {
        Type DataType { get; }
        void Dragged();
    }
}
