﻿using Athletics.Report.CustomCategories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Athletics.Report.AttachedProperties
{
    public class MoveElements
    {
        static UIPropertyMetadata _metadata = new UIPropertyMetadata(Mode.None, new PropertyChangedCallback(OnPropertyChangedValue));
        static DependencyObject _data;
        static ItemsControl _targetSource;

        public static Mode GetProcessMode(DependencyObject obj)
        {
            return (Mode)obj.GetValue(ProcessModeProperty);
        }

        public static void SetProcessMode(DependencyObject obj, Mode value)
        {
            obj.SetValue(ProcessModeProperty, value);
        }

        // Using a DependencyProperty as the backing store for ProcessMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ProcessModeProperty =
            DependencyProperty.RegisterAttached("ProcessMode",
            typeof(Mode),
            typeof(MoveElements),
            _metadata);
        static ItemsControl _control;
        static void OnPropertyChangedValue(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            _control = d as ItemsControl;
            //if ((Mode)args.NewValue == Mode.Drag)
            //{
            //    _control.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(_control_PreviewMouseLeftButtonDown);
            //}
            //else if ((Mode)args.NewValue == Mode.Drop)
            //{
            //    control.Drop += new DragEventHandler(control_Drop);
            //}
            //else if ((Mode)args.NewValue == Mode.DragAndDrop)
            //{
            //    control.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(control_PreviewMouseLeftButtonDown);
            //    control.Drop += new DragEventHandler(control_Drop);
            //}
            switch ((Mode)args.NewValue)
            {
                //Only drag
                case Mode.Drag:
                    {
                        _control.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(_control_PreviewMouseLeftButtonDown);
                        break;
                    }
                //Only drop
                case Mode.Drop:
                    {
                        _control.Drop += new DragEventHandler(_control_Drop);
                        break;
                    }
                //Drag and drop
                case Mode.DragAndDrop:
                    {
                        _control.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(_control_PreviewMouseLeftButtonDown);
                        _control.Drop += new DragEventHandler(_control_Drop);
                        break;
                    }
                //Drag and drop is disabled
                case Mode.None:
                    {
                        _control.PreviewMouseLeftButtonDown -= new MouseButtonEventHandler(_control_PreviewMouseLeftButtonDown);
                        _control.Drop -= new DragEventHandler(_control_Drop);
                        break;
                    }
                default:
                    {
                        _control.PreviewMouseLeftButtonDown -= new MouseButtonEventHandler(_control_PreviewMouseLeftButtonDown);
                        _control.Drop -= new DragEventHandler(_control_Drop);
                        break;
                    }
            }
        }

        static void _control_PreviewMouseLeftButtonDown(object sender, MouseEventArgs args)
        {
            /* Get a reference to the current container*/
            ItemsControl itemsControl = sender as ItemsControl;
            /* Hold a reference to the source*/
            _targetSource = itemsControl;
            /* Get the list view item which is an image in this case using the
               visual tree helper*/
            HitTestResult result = VisualTreeHelper.HitTest(itemsControl,
                                         args.GetPosition(itemsControl));
            /* Get a reference to the data going to be transfered from a  
               container to another one*/
            _data = result.VisualHit;
            try
            {
                DragDrop.DoDragDrop(itemsControl, _data, DragDropEffects.Copy);
            }
            catch (InvalidOperationException caught)
            {
                /* TO DO: Add a code here to handle the situation where the element 
                 has already a logical parent*/
            }
        }

        static void _control_Drop(object sender, DragEventArgs args)
        {
            ItemsControl control = sender as ItemsControl;
            if (_data.GetType() != typeof(Image)) return;
            //_targetSource.Items.Remove(_data);
            string imageName = System.IO.Path.GetFileNameWithoutExtension(((Image)_data).Source.ToString());
            StackPanel framelement = new StackPanel();
            switch (imageName)
            {
                case "images":
                    Grid stackPanel = new Grid();
                    TextBlock textBlock = new TextBlock();
                    // RichTextBoxExtension.InsertText("New text here", textBlock);
                    textBlock.Text = "New text here";
                    List<double> d = new List<double>();
                    d.Add(.5);
                    d.Add(1.5);
                    textBlock.Tag = "Text";
                    Rectangle rec = new Rectangle();
                    rec.StrokeDashArray = new DoubleCollection(d);
                    rec.StrokeDashCap = PenLineCap.Round;
                    rec.StrokeThickness = 3;
                    rec.Stroke = new SolidColorBrush(Colors.Red);
                    rec.Margin = new Thickness(-5);
                    stackPanel.Children.Add(textBlock);
                    stackPanel.Children.Add(rec);
                    framelement.Children.Add(stackPanel);
                    break;

                case "database_table-512":
                    Grid grid = new Grid();
                    grid.ShowGridLines = true;
                    grid.ColumnDefinitions.Add(new ColumnDefinition());
                    TextBlock textBlockgrid = new TextBlock();
                    textBlockgrid.Tag = "DataTable";
                    textBlockgrid.Text = "Column1";
                    textBlockgrid.FontSize = 20;
                    textBlockgrid.FontWeight = FontWeights.Bold;
                    grid.Children.Add(textBlockgrid);
                    List<double> d1 = new List<double>();
                    d1.Add(.5);
                    d1.Add(1.5);
                    Rectangle rec1 = new Rectangle();
                    rec1.StrokeDashArray = new DoubleCollection(d1);
                    rec1.StrokeDashCap = PenLineCap.Round;
                    rec1.StrokeThickness = 3;
                    rec1.Stroke = new SolidColorBrush(Colors.Red);
                    rec1.Margin = new Thickness(-10);
                    grid.Children.Add(rec1);

                    framelement.Children.Add(grid);
                    break;
                default:
                    break;
            }
            framelement.Margin = new Thickness(10);
            control.Items.Add(framelement);
        }
    }

    public enum Mode { Drag, Drop, None, DragAndDrop };

}
