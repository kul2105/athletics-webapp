﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Athletics.Report.CustomTypeEditors
{
    public class TextObject : DependencyObject, INotifyPropertyChanged
    {

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region Customizing "Text" category

        public static readonly DependencyProperty TextProperty = TextBox.TextProperty.AddOwner(typeof(TextObject),
         new FrameworkPropertyMetadata(TextBox.TextProperty.DefaultMetadata.DefaultValue, FrameworkPropertyMetadataOptions.None));
        public static readonly DependencyProperty FontFamilyProperty = Control.FontFamilyProperty.AddOwner(typeof(TextObject),
        new FrameworkPropertyMetadata(Control.FontFamilyProperty.DefaultMetadata.DefaultValue, FrameworkPropertyMetadataOptions.None));

        public static readonly DependencyProperty FontSizeProperty = Control.FontSizeProperty.AddOwner(typeof(TextObject),
          new FrameworkPropertyMetadata(Control.FontSizeProperty.DefaultMetadata.DefaultValue, FrameworkPropertyMetadataOptions.None));

        public static readonly DependencyProperty FontWeightProperty = Control.FontWeightProperty.AddOwner(typeof(TextObject),
          new FrameworkPropertyMetadata(Control.FontWeightProperty.DefaultMetadata.DefaultValue, FrameworkPropertyMetadataOptions.None));

        public static readonly DependencyProperty FontStyleProperty = Control.FontStyleProperty.AddOwner(typeof(TextObject),
          new FrameworkPropertyMetadata(Control.FontStyleProperty.DefaultMetadata.DefaultValue, FrameworkPropertyMetadataOptions.None));

        [Category("Text")]
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        [Category("Text")]
        public FontFamily FontFamily
        {
            get { return (FontFamily)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        [Category("Text")]
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        [Category("Text")]
        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }

        [Category("Text")]
        public FontStyle FontStyle
        {
            get { return (FontStyle)GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }

        #endregion

    }

    

}
