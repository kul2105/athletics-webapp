﻿using System.Windows;

namespace Athletics.Report.CustomTypeEditors
{
    public static class EditorKeys
    {
        private static ComponentResourceKey _ColumnEditorKey =
          new ComponentResourceKey(typeof(EditorKeys), "ColumnEditor");

        public static ComponentResourceKey ColumnEditorKey
        {
            get { return _ColumnEditorKey; }
        }
    }
}
