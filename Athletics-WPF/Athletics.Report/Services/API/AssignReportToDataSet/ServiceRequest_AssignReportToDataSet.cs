﻿using Athletics.Report.Common;
using Athletics.Report.ViewModel;
using System.Collections.Generic;

namespace Athletics.Report.Services.API
{
    public class ServiceRequest_AssignReportToDataSet
    {
        public int ReportDataSetID { get; set; }
        public int ReportID { get; set; }
        public int DataSetID { get; set; }
        public string ReportName { get; set; }
        public string DataSetName { get; set; }
        public string ReportTemplate { get; set; }
        public List<DataSetInReportInfo> AssignedReportList { get; set; }
    }
}
