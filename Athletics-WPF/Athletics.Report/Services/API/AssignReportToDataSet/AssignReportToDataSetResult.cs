﻿using System;
using System.Collections.Generic;

namespace Athletics.Report.Services.API
{
    public class AssignReportToDataSetResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
        public int AssignReportToDataSetID { get; set; }

    }

}
