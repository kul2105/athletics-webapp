﻿using Athletics.Report.Common;
using Athletics.Report.ViewModel;

namespace Athletics.Report.Services.API
{
    public class ServiceRequest_ReportDataSet
    {
        public DataSetSchema dataSetSchema { get; set; }

    }
}
