﻿using System;
using System.Collections.Generic;

namespace Athletics.Report.Services.API
{
    public class ReportDataSetResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
        public int ReportDataSetID { get; set; }

    }

}
