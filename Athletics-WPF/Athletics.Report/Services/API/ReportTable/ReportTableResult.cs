﻿namespace Athletics.Report
{
    public class ReportTableResult
    {
        public int ReportTableID { get; set; }
        public string ReportTableName { get; set; }
        public string ReportTableJoinStatement { get; set; }
        public string ReportTableGroupName { get; set; }
        public string ReportTableGroupColor { get; set; }
        public string ReportTableRootTableName { get; set; }
        public string ReportTableRootRelationalTableName { get; set; }
        public int? ReportTableGroupID { get; set; }
    }
}