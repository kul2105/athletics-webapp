﻿using Athletics.ControlLibrary;
using Athletics.Entities.Models;
using Athletics.Report.Common;
using Athletics.Report.Services.API;
using Athletics.Report.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Athletics.Report
{
    public enum eReport_REQUEST_RESPONSE
    {
        DEFAULT_ERROR,
        SUCCESS,
    }
    public class ReportResponse<T> where T : class
    {
        public ReportResponse()
        {
            this.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
            Message = "";
            Result = null;
        }

        public eReport_REQUEST_RESPONSE Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
    /// <summary>
    /// Anything having to do Meet Setup 
    /// inside this service
    /// </summary>
    public interface IReportManager
    {
        Task<ReportResponse<ReportDataSetResult>> CreateReportDataSet(ServiceRequest_ReportDataSet CreateReportRequest);
        Task<ReportResponse<ReportDataSetResult>> UpdateReportDataSet(ServiceRequest_ReportDataSet CreateReportRequest);
        Task<List<DataSetSchema>> GetAllTableSchema();
        Task<ReportResponse<ReportDataSetResult>> DeleteReportDataSet(int ReportDataSetID);

        Task<ReportResponse<ReportDataSetResult>> CreateReportTemplate(ServiceRequest_ReportTemplate CreateReportRequest, bool commitTransaction = true);
        Task<ReportResponse<ReportDataSetResult>> UpdateReportTemplate(ServiceRequest_ReportTemplate CreateReportRequest);
        Task<List<CreateReportEntity>> GetAllReportTemplate();
        Task<ReportResponse<ReportDataSetResult>> DeleteReportTemplate(int ReportID, bool commitTransaction = true);

        Task<ReportResponse<AssignReportToDataSetResult>> AssignReportToDataSet(ServiceRequest_AssignReportToDataSet ServiceRequest_AssignReportToDataSetRequest, bool commitTransaction = true);
        Task<ReportResponse<AssignReportToDataSetResult>> UpdateAssignReportToDataSet(ServiceRequest_AssignReportToDataSet ServiceRequest_AssignReportToDataSetRequest);
        Task<List<ServiceRequest_AssignReportToDataSet>> GetAssignReportToDataSet();
        Task<ReportResponse<AssignReportToDataSetResult>> DeleteAssignReportToDataSet(int ReportID, bool commitTransaction = true);
        Task<List<GetReportDetailByReportID_Result>> GetReportDetailByReportID(int ReportID);
        Task<List<GetAllReportDetail_Result>> GetGetAllReportViewDetail();
        Task<ImpObservableCollection<ReportTableResult>> GetAllReportTable();
        Task<ImpObservableCollection<ReportTableGroup>> GetAllReportTableGroup();
        Task<List<GetTableSchema_Result>> GetTableSchema(string tableName);
        Task<List<GetForeignKeyDetail_Result>> GetForeignKeyDetail(string tableName, string columnName);
    }
}
