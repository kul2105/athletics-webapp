﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Athletics.Entities.Models;
using Athletics.Report.Services.API;
using Athletics.Report.ViewModel;
using Microsoft.Practices.Prism.Mvvm;
using System.Linq;
using System;
using Athletics.Report.Common;
using Athletics.ControlLibrary;

namespace Athletics.Report
{
    public class ReportManager : Common.BindableBase, IReportManager
    {
        public async Task<ReportResponse<AssignReportToDataSetResult>> AssignReportToDataSet(ServiceRequest_AssignReportToDataSet ServiceRequest_AssignReportToDataSetRequest, bool commitTransaction = true)
        {
            ReportResponse<AssignReportToDataSetResult> Result = new ReportResponse<AssignReportToDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (ServiceRequest_AssignReportToDataSetRequest != null)
                        {
                            ReportDataSetMappedMaster reportDataSetMappedMaster = ctx.ReportDataSetMappedMasters.Where(p => p.ReportDataSetTableID == ServiceRequest_AssignReportToDataSetRequest.DataSetID && p.ReportID == ServiceRequest_AssignReportToDataSetRequest.ReportID).FirstOrDefault();
                            if (reportDataSetMappedMaster == null)
                            {
                                reportDataSetMappedMaster = new ReportDataSetMappedMaster();
                            }
                            reportDataSetMappedMaster.ReportID = ServiceRequest_AssignReportToDataSetRequest.ReportID;
                            reportDataSetMappedMaster.ReportDataSetTableID = ServiceRequest_AssignReportToDataSetRequest.DataSetID;
                            reportDataSetMappedMaster.ReportTemplate = ServiceRequest_AssignReportToDataSetRequest.ReportTemplate;
                            ServiceRequest_AssignReportToDataSetRequest.ReportDataSetID = reportDataSetMappedMaster.ReportDataSetMappedID;
                            foreach (var assignReportColumnInfo in ServiceRequest_AssignReportToDataSetRequest.AssignedReportList)
                            {
                                ReportDataSetMappedDetail reportDataSetMappedDetail = ctx.ReportDataSetMappedDetails.Where(p => p.ReportDataSetMappedDetailID == reportDataSetMappedMaster.ReportDataSetMappedID).FirstOrDefault();
                                if (reportDataSetMappedDetail == null)
                                {
                                    reportDataSetMappedDetail = new ReportDataSetMappedDetail();

                                }
                                reportDataSetMappedDetail.DataSetColumnName = assignReportColumnInfo.DataSetColumnName;
                                reportDataSetMappedDetail.ReportColumnName = assignReportColumnInfo.ReportColumnName;
                                reportDataSetMappedDetail.ReportDataSetMasterID = reportDataSetMappedMaster.ReportDataSetMappedID;
                                ctx.ReportDataSetMappedDetails.Add(reportDataSetMappedDetail);

                            }
                            ctx.ReportDataSetMappedMasters.Add(reportDataSetMappedMaster);
                            ctx.SaveChanges();
                            dbContextTransaction.Commit();

                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<AssignReportToDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Result = new AssignReportToDataSetResult();
                        Result.Result.AssignReportToDataSetID = ServiceRequest_AssignReportToDataSetRequest.ReportDataSetID;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new AssignReportToDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eReport_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new AssignReportToDataSetResult();
            Result.Result.AssignReportToDataSetID = ServiceRequest_AssignReportToDataSetRequest.ReportDataSetID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<ReportResponse<ReportDataSetResult>> CreateReportDataSet(ServiceRequest_ReportDataSet CreateReportRequest)
        {
            ReportResponse<ReportDataSetResult> Result = new ReportResponse<ReportDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (CreateReportRequest != null)
                        {
                            ReportDataSetInfo reportDataSetInfo = ctx.ReportDataSetInfoes.Where(p => p.ReportDataSetID == CreateReportRequest.dataSetSchema.DataSetID).FirstOrDefault();
                            if (reportDataSetInfo == null)
                            {
                                reportDataSetInfo = new ReportDataSetInfo();
                            }
                            reportDataSetInfo.ReportDataSetDescription = CreateReportRequest.dataSetSchema.DataSetDescription;
                            reportDataSetInfo.ReportDataSetName = CreateReportRequest.dataSetSchema.DataSetName;
                            reportDataSetInfo.ReportDataSetQuery = CreateReportRequest.dataSetSchema.DataSetQuery;
                            foreach (var dataSetTable in CreateReportRequest.dataSetSchema.TableList)
                            {
                                ReportDataSetTable table = ctx.ReportDataSetTables.Where(p => p.ReportDataSetTableID == dataSetTable.TableID).FirstOrDefault();
                                if (table == null)
                                {
                                    table = new ReportDataSetTable();

                                }
                                table.ReportDataSetTableName = dataSetTable.TableName;
                                //
                                foreach (var column in dataSetTable.ColumnCollection)
                                {
                                    if (column.IsSelected)
                                    {
                                        ReportDataSetTableColumn columnDetail = ctx.ReportDataSetTableColumns.Where(p => p.ReportDataSetTableColumnID == column.ColumnID).FirstOrDefault();
                                        if (columnDetail == null)
                                        {
                                            columnDetail = new ReportDataSetTableColumn();
                                        }
                                        columnDetail.ReportDataSetTableColumnName = column.ColumnName;
                                        ctx.ReportDataSetTableColumns.Add(columnDetail);
                                        ReportColumnInDataSetTable reportColumn = new ReportColumnInDataSetTable();
                                        reportColumn.ReportDataSetTable = table;
                                        reportColumn.ReportDataSetTableColumn = columnDetail;
                                        ctx.ReportColumnInDataSetTables.Add(reportColumn);
                                    }
                                }
                                ReportDataSetInTable datasetInTable = new ReportDataSetInTable();
                                datasetInTable.ReportDataSetTable = table;
                                datasetInTable.ReportDataSetInfo = reportDataSetInfo;
                                ctx.ReportDataSetInTables.Add(datasetInTable);



                            }
                            ctx.ReportDataSetInfoes.Add(reportDataSetInfo);
                            ctx.SaveChanges();
                            dbContextTransaction.Commit();

                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<ReportDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ReportDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eReport_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new ReportDataSetResult();
            Result.Result.ReportDataSetID = CreateReportRequest.dataSetSchema.DataSetID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<ReportResponse<ReportDataSetResult>> CreateReportTemplate(ServiceRequest_ReportTemplate CreateReportRequest, bool commitTransaction = true)
        {
            ReportResponse<ReportDataSetResult> Result = new ReportResponse<ReportDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (CreateReportRequest != null)
                        {
                            if (CreateReportRequest.CreateReportEntity != null)
                            {
                                ReportDetail reportDetail = ctx.ReportDetails.Where(p => p.ReportID == CreateReportRequest.CreateReportEntity.ReportID).FirstOrDefault();
                                if (reportDetail == null)
                                {
                                    reportDetail = new ReportDetail();
                                }
                                reportDetail.ReportName = CreateReportRequest.CreateReportEntity.ReportName;
                                reportDetail.ReportDescription = CreateReportRequest.CreateReportEntity.ReportDescription;

                                if (CreateReportRequest.CreateReportEntity.ReportTemplateData != null)
                                {
                                    ReportDetailInfo reportDetailInfo = null;
                                    foreach (var headerData in CreateReportRequest.CreateReportEntity.ReportTemplateData.Header)
                                    {
                                        reportDetailInfo = new ReportDetailInfo();
                                        reportDetail.ReportDetailInfoes = new List<ReportDetailInfo>();
                                        ReportDetailInfoItem reportDetailInfoItem = new ReportDetailInfoItem();
                                        reportDetailInfoItem.ReportDetailInfoItemType = headerData.ReportItemType.ToString();
                                        ReportGroup group = ctx.ReportGroups.Where(p => p.ReportGroupName == "Header").FirstOrDefault();
                                        reportDetailInfo.ReportGroupName = group.ReportGroupID;

                                        if (CreateReportRequest.CreateReportEntity.ReportTemplateProperty != null)
                                        {
                                            foreach (var item in CreateReportRequest.CreateReportEntity.ReportTemplateProperty.Header)
                                            {
                                                ReportDetailInfoItemProperty reportDetailInfoItemProperty = new ReportDetailInfoItemProperty();
                                                reportDetailInfoItemProperty.FontFamilyName = item.FontFamilyName;
                                                reportDetailInfoItemProperty.FontStyleName = item.FontStyleName;
                                                reportDetailInfoItemProperty.FontWeightName = item.FontWeightName;
                                                reportDetailInfoItemProperty.FontSizeName = item.FontSizeName;
                                                reportDetailInfoItemProperty.ItemContent = item.ItemContent;
                                                reportDetailInfoItemProperty.ReportDetailInfoItemID = reportDetailInfoItem.ReportDetailInfoItemID;
                                                reportDetailInfoItem.ReportDetailInfoItemProperties.Add(reportDetailInfoItemProperty);
                                            }
                                        }
                                        reportDetailInfo.ReportDetailInfoItems.Add(reportDetailInfoItem);

                                    }
                                    if (reportDetailInfo != null)
                                    {
                                        reportDetail.ReportDetailInfoes.Add(reportDetailInfo);
                                        reportDetailInfo = null;
                                    }
                                    foreach (var headerData in CreateReportRequest.CreateReportEntity.ReportTemplateData.Body)
                                    {
                                        reportDetailInfo = new ReportDetailInfo();
                                        ReportDetailInfoItem reportDetailInfoItem = new ReportDetailInfoItem();
                                        reportDetailInfoItem.ReportDetailInfoItemType = headerData.ReportItemType.ToString();
                                        ReportGroup group = ctx.ReportGroups.Where(p => p.ReportGroupName == "Body").FirstOrDefault();
                                        reportDetailInfo.ReportGroupName = group.ReportGroupID;
                                        if (CreateReportRequest.CreateReportEntity.ReportTemplateProperty != null)
                                        {
                                            foreach (var item in CreateReportRequest.CreateReportEntity.ReportTemplateProperty.Body)
                                            {
                                                ReportDetailInfoItemProperty reportDetailInfoItemProperty = new ReportDetailInfoItemProperty();
                                                reportDetailInfoItemProperty.FontFamilyName = item.FontFamilyName;
                                                reportDetailInfoItemProperty.FontStyleName = item.FontStyleName;
                                                reportDetailInfoItemProperty.FontWeightName = item.FontWeightName;
                                                reportDetailInfoItemProperty.FontSizeName = item.FontSizeName;
                                                reportDetailInfoItemProperty.ItemContent = item.ItemContent;
                                                reportDetailInfoItemProperty.ReportDetailInfoItemID = reportDetailInfoItem.ReportDetailInfoItemID;
                                                reportDetailInfoItem.ReportDetailInfoItemProperties.Add(reportDetailInfoItemProperty);
                                            }
                                        }
                                        reportDetailInfo.ReportDetailInfoItems.Add(reportDetailInfoItem);

                                    }
                                    if (reportDetailInfo != null)
                                    {
                                        reportDetail.ReportDetailInfoes.Add(reportDetailInfo);
                                        reportDetailInfo = null;
                                    }
                                    foreach (var headerData in CreateReportRequest.CreateReportEntity.ReportTemplateData.Footer)
                                    {
                                        reportDetailInfo = new ReportDetailInfo();
                                        ReportDetailInfoItem reportDetailInfoItem = new ReportDetailInfoItem();
                                        reportDetailInfoItem.ReportDetailInfoItemType = headerData.ReportItemType.ToString();
                                        ReportGroup group = ctx.ReportGroups.Where(p => p.ReportGroupName == "Footer").FirstOrDefault();
                                        reportDetailInfo.ReportGroupName = group.ReportGroupID;
                                        if (CreateReportRequest.CreateReportEntity.ReportTemplateProperty != null)
                                        {
                                            foreach (var item in CreateReportRequest.CreateReportEntity.ReportTemplateProperty.Footer)
                                            {
                                                ReportDetailInfoItemProperty reportDetailInfoItemProperty = new ReportDetailInfoItemProperty();
                                                reportDetailInfoItemProperty.FontFamilyName = item.FontFamilyName;
                                                reportDetailInfoItemProperty.FontStyleName = item.FontStyleName;
                                                reportDetailInfoItemProperty.FontWeightName = item.FontWeightName;
                                                reportDetailInfoItemProperty.ItemContent = item.ItemContent;
                                                reportDetailInfoItem.ReportDetailInfoItemProperties.Add(reportDetailInfoItemProperty);
                                            }
                                        }
                                        reportDetailInfo.ReportDetailInfoItems.Add(reportDetailInfoItem);


                                    }
                                    if (reportDetailInfo != null)
                                    {
                                        reportDetail.ReportDetailInfoes.Add(reportDetailInfo);
                                        reportDetailInfo = null;
                                    }
                                    ctx.ReportDetails.Add(reportDetail);
                                    ctx.SaveChanges();
                                    dbContextTransaction.Commit();

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<ReportDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ReportDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result = new ReportResponse<ReportDataSetResult>();
            Result.Message = "Sucess";
            Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
            Result.Result = new ReportDataSetResult();
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<ReportResponse<AssignReportToDataSetResult>> DeleteAssignReportToDataSet(int reportDataSetID, bool commitTransaction = true)
        {
            ReportResponse<AssignReportToDataSetResult> Result = new ReportResponse<AssignReportToDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<ReportDataSetMappedMaster> ReportDataSetMappedMasterList = ctx.ReportDataSetMappedMasters.Where(p => p.ReportDataSetMappedID == reportDataSetID).ToList();
                        foreach (ReportDataSetMappedMaster reportDataSetMappedMaster in ReportDataSetMappedMasterList)
                        {
                            List<ReportDataSetMappedDetail> ReportDataSetMappedDetailList = ctx.ReportDataSetMappedDetails.Where(p => p.ReportDataSetMasterID == reportDataSetMappedMaster.ReportDataSetMappedID).ToList();
                            foreach (var reportDataSetMappedDetail in ReportDataSetMappedDetailList)
                            {
                                ctx.ReportDataSetMappedDetails.Remove(reportDataSetMappedDetail);
                            }
                            ctx.ReportDataSetMappedMasters.Remove(reportDataSetMappedMaster);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<AssignReportToDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new AssignReportToDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }
                Result = new ReportResponse<AssignReportToDataSetResult>();
                Result.Message = "Sucess";
                Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new AssignReportToDataSetResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<ReportResponse<ReportDataSetResult>> DeleteReportDataSet(int ReportDataSetID)
        {
            ReportResponse<ReportDataSetResult> Result = new ReportResponse<ReportDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<ReportDataSetInfo> reportDataSetInfo = ctx.ReportDataSetInfoes.Where(p => p.ReportDataSetID == ReportDataSetID).ToList();
                        foreach (ReportDataSetInfo dataSetInfo in reportDataSetInfo)
                        {
                            List<ReportDataSetInTable> DataSetTableList = ctx.ReportDataSetInTables.Where(p => p.ReportDataSetID == dataSetInfo.ReportDataSetID).ToList();
                            foreach (var reportDataSetTable in DataSetTableList)
                            {
                                List<ReportColumnInDataSetTable> ColumnListInTable = ctx.ReportColumnInDataSetTables.Where(p => p.ReportDataSetTableID == reportDataSetTable.ReportDataSetTableID).ToList();
                                foreach (var reportDataSetCplumn in ColumnListInTable)
                                {
                                    List<ReportDataSetTableColumn> ColumnList = ctx.ReportDataSetTableColumns.Where(p => p.ReportDataSetTableColumnID == reportDataSetCplumn.ReportDataSetColumnID).ToList();
                                    ctx.ReportDataSetTableColumns.RemoveRange(ColumnList);
                                    ctx.ReportColumnInDataSetTables.Remove(reportDataSetCplumn);

                                }

                                List<ReportDataSetTable> TableList = ctx.ReportDataSetTables.Where(p => p.ReportDataSetTableID == reportDataSetTable.ReportDataSetTableID).ToList();
                                ctx.ReportDataSetTables.RemoveRange(TableList);
                            }
                            ctx.ReportDataSetInfoes.Remove(dataSetInfo);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<ReportDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ReportDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }
                Result.Message = "";
                Result.Status = eReport_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new ReportDataSetResult();
                Result.Result.ReportDataSetID = ReportDataSetID;
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<ReportResponse<ReportDataSetResult>> DeleteReportTemplate(int ReportID, bool commitTransaction = true)
        {
            ReportResponse<ReportDataSetResult> Result = new ReportResponse<ReportDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<ReportDetail> reportDetailList = ctx.ReportDetails.Where(p => p.ReportID == ReportID).ToList();
                        foreach (ReportDetail reportDetail in reportDetailList)
                        {
                            List<ReportDetailInfo> ReportDetailInfoList = ctx.ReportDetailInfoes.Where(p => p.ReportID == reportDetail.ReportID).ToList();
                            foreach (var reportDetailInfo in ReportDetailInfoList)
                            {
                                List<ReportDetailInfoItem> ReportDetailInfoItemList = ctx.ReportDetailInfoItems.Where(p => p.ReportDetailInfoID == reportDetailInfo.ReportDetailInfoID).ToList();
                                foreach (var ReportDetailInfoItem in ReportDetailInfoItemList)
                                {
                                    List<ReportDetailInfoItemProperty> reportPropertyList = ctx.ReportDetailInfoItemProperties.Where(p => p.ReportDetailInfoItemID == ReportDetailInfoItem.ReportDetailInfoItemID).ToList();
                                    ctx.ReportDetailInfoItemProperties.RemoveRange(reportPropertyList);
                                    ctx.ReportDetailInfoItems.Remove(ReportDetailInfoItem);
                                }

                                ctx.ReportDetailInfoes.Remove(reportDetailInfo);
                            }
                            ctx.ReportDetails.Remove(reportDetail);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<ReportDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ReportDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }

                }
                Result = new ReportResponse<ReportDataSetResult>();
                Result.Message = "Sucess";
                Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new ReportDataSetResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<ImpObservableCollection<ReportTableResult>> GetAllReportTable()
        {
            ImpObservableCollection<ReportTableResult> reportTableList = new ImpObservableCollection<ReportTableResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (var item in ctx.ReportTables)
                {
                    ReportTableResult result = new ReportTableResult();
                    result.ReportTableID = item.ReportTableID;
                    result.ReportTableName = item.ReportTableName;
                    result.ReportTableGroupID = item.ReportTableGroupID;
                    result.ReportTableJoinStatement = item.ReportTableJoinStatement;
                    ReportTableGroup reportGroup = ctx.ReportTableGroups.Where(p => p.ReportTableGroupID == item.ReportTableGroupID).FirstOrDefault();
                    if (reportGroup != null)
                    {
                        result.ReportTableGroupName = reportGroup.ReportTableGroupName;
                        result.ReportTableGroupColor = reportGroup.GroupColorCode;
                    }
                    if (item.ReportTableRootTableID != null)
                    {
                        ReportTable rootTable = ctx.ReportTables.Where(p => p.ReportTableID == item.ReportTableRootTableID).FirstOrDefault();
                        if (rootTable != null)
                        {
                            result.ReportTableRootTableName = rootTable.ReportTableName;
                        }
                    }
                    if (item.ReportTableRootRelationalTableID != null)
                    {
                        ReportTable repationalTable = ctx.ReportTables.Where(p => p.ReportTableID == item.ReportTableRootRelationalTableID).FirstOrDefault();
                        if (repationalTable != null)
                        {
                            result.ReportTableRootRelationalTableName = repationalTable.ReportTableName;
                        }
                    }
                    reportTableList.Add(result);

                }
            }

            return reportTableList;
        }

        public async Task<ImpObservableCollection<ReportTableGroup>> GetAllReportTableGroup()
        {
            ImpObservableCollection<ReportTableGroup> reportGroupList = new ImpObservableCollection<ReportTableGroup>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (var item in ctx.ReportTableGroups)
                {
                    reportGroupList.Add(item);
                }
            }
            return reportGroupList;
        }

        public async Task<List<CreateReportEntity>> GetAllReportTemplate()
        {
            List<CreateReportEntity> CreateReportEntityList = new List<CreateReportEntity>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (ReportDetail reportDetail in ctx.ReportDetails)
                {
                    CreateReportEntity reportEntity = new CreateReportEntity();
                    reportEntity.ReportID = reportDetail.ReportID;
                    reportEntity.ReportName = reportDetail.ReportName;
                    reportEntity.ReportDescription = reportDetail.ReportDescription;
                    reportEntity.ReportTemplateData = new ReportTemplateData();
                    reportEntity.ReportTemplateData.Header = new ImpObservableCollection<ReportItemTemplateData>();
                    reportEntity.ReportTemplateData.Body = new ImpObservableCollection<ReportItemTemplateData>();
                    reportEntity.ReportTemplateData.Footer = new ImpObservableCollection<ReportItemTemplateData>();
                    reportEntity.ReportTemplateProperty = new ReportTemplateProperty();
                    foreach (ReportDetailInfo reportDetailInfo in ctx.ReportDetailInfoes.Where(p => p.ReportID == reportDetail.ReportID))
                    {
                        foreach (ReportDetailInfoItem reportDetailInfoItem in ctx.ReportDetailInfoItems.Where(p => p.ReportDetailInfoID == reportDetailInfo.ReportDetailInfoID))
                        {
                            foreach (ReportGroup group in ctx.ReportGroups.Where(p => p.ReportGroupID == reportDetailInfo.ReportGroupName))
                            {
                                ReportSection section = (ReportSection)Enum.Parse(typeof(ReportSection), group.ReportGroupName);
                                switch (section)
                                {
                                    case ReportSection.Header:
                                        ReportItemTemplateData reportItemTemplateData = new ReportItemTemplateData();
                                        ReportType type = (ReportType)Enum.Parse(typeof(ReportType), reportDetailInfoItem.ReportDetailInfoItemType);
                                        reportItemTemplateData.ReportItemType = type;
                                        reportItemTemplateData.ReportItemID = reportDetailInfoItem.ReportDetailInfoItemID;
                                        reportEntity.ReportTemplateData.Header.Add(reportItemTemplateData);
                                        List<ReportDetailInfoItemProperty> propertyList = ctx.ReportDetailInfoItemProperties.Where(p => p.ReportDetailInfoItemID == reportDetailInfoItem.ReportDetailInfoItemID).ToList();
                                        reportEntity.ReportTemplateProperty.Header = new ImpObservableCollection<ReportItemProperty>();
                                        foreach (var item in propertyList)
                                        {
                                            ReportItemProperty templateProperty = new ReportItemProperty();
                                            templateProperty.FontFamilyName = item.FontFamilyName;
                                            templateProperty.FontSizeName = item.FontSizeName;
                                            templateProperty.FontStyleName = item.FontStyleName;
                                            templateProperty.FontWeightName = item.FontWeightName;
                                            templateProperty.ItemContent = item.ItemContent;
                                            reportEntity.ReportTemplateProperty.Header.Add(templateProperty);

                                        }
                                        break;
                                    case ReportSection.Body:
                                        ReportItemTemplateData reportItemTemplateBodyData = new ReportItemTemplateData();
                                        ReportType typeBody = (ReportType)Enum.Parse(typeof(ReportType), reportDetailInfoItem.ReportDetailInfoItemType);
                                        reportItemTemplateBodyData.ReportItemType = typeBody;
                                        reportItemTemplateBodyData.ReportItemID = reportDetailInfoItem.ReportDetailInfoItemID;
                                        reportEntity.ReportTemplateData.Body.Add(reportItemTemplateBodyData);
                                        List<ReportDetailInfoItemProperty> propertyListBody = ctx.ReportDetailInfoItemProperties.Where(p => p.ReportDetailInfoItemID == reportDetailInfoItem.ReportDetailInfoItemID).ToList();
                                        reportEntity.ReportTemplateProperty.Body = new ImpObservableCollection<ReportItemProperty>();
                                        foreach (var item in propertyListBody)
                                        {
                                            ReportItemProperty templateProperty = new ReportItemProperty();
                                            templateProperty.FontFamilyName = item.FontFamilyName;
                                            templateProperty.FontSizeName = item.FontSizeName;
                                            templateProperty.FontStyleName = item.FontStyleName;
                                            templateProperty.FontWeightName = item.FontWeightName;
                                            templateProperty.ItemContent = item.ItemContent;
                                            reportEntity.ReportTemplateProperty.Body.Add(templateProperty);

                                        }
                                        break;

                                    case ReportSection.Footer:
                                        ReportItemTemplateData reportItemTemplateFooterData = new ReportItemTemplateData();
                                        ReportType typefooter = (ReportType)Enum.Parse(typeof(ReportType), reportDetailInfoItem.ReportDetailInfoItemType);
                                        reportItemTemplateFooterData.ReportItemType = typefooter;
                                        reportItemTemplateFooterData.ReportItemID = reportDetailInfoItem.ReportDetailInfoItemID;
                                        reportEntity.ReportTemplateData.Footer.Add(reportItemTemplateFooterData);
                                        List<ReportDetailInfoItemProperty> propertyListFooter = ctx.ReportDetailInfoItemProperties.Where(p => p.ReportDetailInfoItemID == reportDetailInfoItem.ReportDetailInfoItemID).ToList();
                                        reportEntity.ReportTemplateProperty.Footer = new ImpObservableCollection<ReportItemProperty>();
                                        foreach (var item in propertyListFooter)
                                        {
                                            ReportItemProperty templateProperty = new ReportItemProperty();
                                            templateProperty.FontFamilyName = item.FontFamilyName;
                                            templateProperty.FontSizeName = item.FontSizeName;
                                            templateProperty.FontStyleName = item.FontStyleName;
                                            templateProperty.FontWeightName = item.FontWeightName;
                                            templateProperty.ItemContent = item.ItemContent;
                                            reportEntity.ReportTemplateProperty.Footer.Add(templateProperty);

                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                    }
                    CreateReportEntityList.Add(reportEntity);

                }
            }
            return CreateReportEntityList;
        }

        public async Task<List<DataSetSchema>> GetAllTableSchema()
        {
            List<DataSetSchema> TableSchemaList = new List<DataSetSchema>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (ReportDataSetInfo reportDataSet in ctx.ReportDataSetInfoes)
                {
                    DataSetSchema datasetSchema = new DataSetSchema();
                    datasetSchema.DataSetName = reportDataSet.ReportDataSetName;
                    datasetSchema.DataSetDescription = reportDataSet.ReportDataSetDescription;
                    datasetSchema.DataSetQuery = reportDataSet.ReportDataSetQuery;
                    datasetSchema.DataSetID = reportDataSet.ReportDataSetID;
                    foreach (var reportDataSetInTable in reportDataSet.ReportDataSetInTables.Where(p => p.ReportDataSetID == reportDataSet.ReportDataSetID))
                    {
                        foreach (ReportDataSetTable reportDataSettable in ctx.ReportDataSetTables.Where(p => p.ReportDataSetTableID == reportDataSetInTable.ReportDataSetTableID))
                        {
                            foreach (ReportColumnInDataSetTable reportDataSetTableColumn in ctx.ReportColumnInDataSetTables.Where(p => p.ReportDataSetTableID == reportDataSettable.ReportDataSetTableID))
                            {

                                TableSchema tableSchema = datasetSchema.TableList.Where(p => p.TableID == reportDataSettable.ReportDataSetTableID).FirstOrDefault();
                                if (tableSchema == null)
                                {
                                    tableSchema = new TableSchema();
                                }
                                tableSchema.TableName = reportDataSettable.ReportDataSetTableName;
                                tableSchema.TableDescription = reportDataSettable.ReportDataSetTableDescription;
                                tableSchema.TableID = reportDataSettable.ReportDataSetTableID;
                                foreach (ReportDataSetTableColumn reportDataSetColumn in ctx.ReportDataSetTableColumns.Where(p => p.ReportDataSetTableColumnID == reportDataSetTableColumn.ReportDataSetColumnID))
                                {
                                    ColumnSchema columnschema = new ColumnSchema();
                                    columnschema.ColumnName = reportDataSetColumn.ReportDataSetTableColumnName;
                                    columnschema.ColumnID = reportDataSetColumn.ReportDataSetTableColumnID;
                                    tableSchema.ColumnCollection.Add(columnschema);
                                }
                                TableSchema tableSchema1 = datasetSchema.TableList.Where(p => p.TableID == reportDataSettable.ReportDataSetTableID).FirstOrDefault();
                                if (tableSchema1 == null)
                                {
                                    datasetSchema.TableList.Add(tableSchema);
                                }
                            }
                        }
                    }

                    TableSchemaList.Add(datasetSchema);
                }
            }
            return TableSchemaList;
        }

        public async Task<List<ServiceRequest_AssignReportToDataSet>> GetAssignReportToDataSet()
        {
            List<ServiceRequest_AssignReportToDataSet> AssignReportToDataSetList = new List<ServiceRequest_AssignReportToDataSet>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (ReportDataSetMappedMaster reportDataSetMappedMaster in ctx.ReportDataSetMappedMasters)
                {
                    ServiceRequest_AssignReportToDataSet serviceRequest_AssignReportToDataSet = new ServiceRequest_AssignReportToDataSet();
                    serviceRequest_AssignReportToDataSet.ReportDataSetID = reportDataSetMappedMaster.ReportDataSetMappedID;
                    serviceRequest_AssignReportToDataSet.DataSetID = reportDataSetMappedMaster.ReportDataSetTableID.Value;
                    serviceRequest_AssignReportToDataSet.ReportTemplate = reportDataSetMappedMaster.ReportTemplate;
                    serviceRequest_AssignReportToDataSet.AssignedReportList = new List<DataSetInReportInfo>();
                    foreach (ReportDataSetMappedDetail reportDataSetMappedDetail in ctx.ReportDataSetMappedDetails.Where(p => p.ReportDataSetMappedDetailID == reportDataSetMappedMaster.ReportDataSetMappedID))
                    {
                        DataSetInReportInfo dataSetInfo = new DataSetInReportInfo();
                        dataSetInfo.DataSetColumnName = reportDataSetMappedDetail.DataSetColumnName;
                        dataSetInfo.ReportColumnName = reportDataSetMappedDetail.ReportColumnName;
                        if (reportDataSetMappedMaster.ReportDataSetTableID != null)
                            dataSetInfo.DataSetTableId = reportDataSetMappedMaster.ReportDataSetTableID.Value;
                        serviceRequest_AssignReportToDataSet.AssignedReportList.Add(dataSetInfo);

                    }
                    AssignReportToDataSetList.Add(serviceRequest_AssignReportToDataSet);

                }
            }
            return AssignReportToDataSetList;
        }

        public async Task<List<GetForeignKeyDetail_Result>> GetForeignKeyDetail(string tableName, string columnName)
        {
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                return ctx.GetForeignKeyDetail(tableName, columnName).ToList();
            }
            return null;
        }

        public async Task<List<GetAllReportDetail_Result>> GetGetAllReportViewDetail()
        {
            List<GetAllReportDetail_Result> response = new List<GetAllReportDetail_Result>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                response = ctx.GetAllReportDetail().ToList();
            }
            return response;
        }

        public async Task<List<GetReportDetailByReportID_Result>> GetReportDetailByReportID(int ReportID)
        {
            List<GetReportDetailByReportID_Result> response = new List<GetReportDetailByReportID_Result>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                response = ctx.GetReportDetailByReportID(ReportID).ToList();
            }
            return response;
        }

        public async Task<List<GetTableSchema_Result>> GetTableSchema(string tableName)
        {
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                return ctx.GetTableSchema(tableName).ToList();
            }
        }

        public async Task<ReportResponse<AssignReportToDataSetResult>> UpdateAssignReportToDataSet(ServiceRequest_AssignReportToDataSet ServiceRequest_AssignReportToDataSetRequest)
        {
            ReportResponse<AssignReportToDataSetResult> Result = new ReportResponse<AssignReportToDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (ServiceRequest_AssignReportToDataSetRequest != null)
                        {
                            ReportDataSetMappedMaster reportDataSetMappedMaster = ctx.ReportDataSetMappedMasters.Where(p => p.ReportDataSetMappedID == ServiceRequest_AssignReportToDataSetRequest.ReportDataSetID).FirstOrDefault();
                            if (reportDataSetMappedMaster != null)
                            {
                                reportDataSetMappedMaster.ReportDataSetTableID = ServiceRequest_AssignReportToDataSetRequest.ReportDataSetID;
                                reportDataSetMappedMaster.ReportID = ServiceRequest_AssignReportToDataSetRequest.ReportID;
                                reportDataSetMappedMaster.ReportTemplate = ServiceRequest_AssignReportToDataSetRequest.ReportTemplate;
                                foreach (var assignedReportList in ServiceRequest_AssignReportToDataSetRequest.AssignedReportList)
                                {
                                    ReportDataSetMappedDetail reportDataSetMappedDetail = ctx.ReportDataSetMappedDetails.Where(p => p.ReportDataSetMappedDetailID == assignedReportList.ReportDataSetMappedDetailID).FirstOrDefault();
                                    if (reportDataSetMappedDetail != null)
                                    {
                                        reportDataSetMappedDetail.ReportColumnName = assignedReportList.ReportColumnName;
                                        reportDataSetMappedDetail.DataSetColumnName = assignedReportList.DataSetColumnName;
                                    }
                                    else
                                    {
                                        reportDataSetMappedDetail = new ReportDataSetMappedDetail();
                                        reportDataSetMappedDetail.ReportColumnName = assignedReportList.ReportColumnName;
                                        reportDataSetMappedDetail.DataSetColumnName = assignedReportList.DataSetColumnName;
                                        ctx.ReportDataSetMappedDetails.Add(reportDataSetMappedDetail);
                                    }
                                }
                            }
                            else
                            {
                                reportDataSetMappedMaster = new ReportDataSetMappedMaster();
                                reportDataSetMappedMaster.ReportDataSetTableID = ServiceRequest_AssignReportToDataSetRequest.ReportDataSetID;
                                reportDataSetMappedMaster.ReportID = ServiceRequest_AssignReportToDataSetRequest.ReportID;
                                reportDataSetMappedMaster.ReportTemplate = ServiceRequest_AssignReportToDataSetRequest.ReportTemplate;
                                ctx.ReportDataSetMappedMasters.Add(reportDataSetMappedMaster);

                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<AssignReportToDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new AssignReportToDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                    ctx.SaveChanges();
                    dbContextTransaction.Commit();
                    Result.Message = "";
                    Result.Status = eReport_REQUEST_RESPONSE.SUCCESS;
                    Result.Result = new AssignReportToDataSetResult();
                    Result.Result.AssignReportToDataSetID = ServiceRequest_AssignReportToDataSetRequest.ReportDataSetID;
                    Result.Result.AuthToken = null;
                    return Result;
                }
            }
        }

        public async Task<ReportResponse<ReportDataSetResult>> UpdateReportDataSet(ServiceRequest_ReportDataSet CreateReportRequest)
        {
            ReportResponse<ReportDataSetResult> Result = new ReportResponse<ReportDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (CreateReportRequest != null)
                        {
                            ReportDataSetInfo reportDataSetInfo = ctx.ReportDataSetInfoes.Where(p => p.ReportDataSetID == CreateReportRequest.dataSetSchema.DataSetID).FirstOrDefault();
                            if (reportDataSetInfo != null)
                            {
                                reportDataSetInfo.ReportDataSetDescription = CreateReportRequest.dataSetSchema.DataSetDescription;
                                reportDataSetInfo.ReportDataSetName = CreateReportRequest.dataSetSchema.DataSetName;
                                reportDataSetInfo.ReportDataSetQuery = CreateReportRequest.dataSetSchema.DataSetQuery;
                                foreach (var dataSetTable in CreateReportRequest.dataSetSchema.TableList)
                                {
                                    ReportDataSetTable table = ctx.ReportDataSetTables.Where(p => p.ReportDataSetTableID == dataSetTable.TableID).FirstOrDefault();
                                    if (table != null)
                                    {
                                        table.ReportDataSetTableName = dataSetTable.TableName;
                                        foreach (var column in dataSetTable.ColumnCollection)
                                        {
                                            if (column.IsSelected)
                                            {
                                                ReportDataSetTableColumn columnDetail = ctx.ReportDataSetTableColumns.Where(p => p.ReportDataSetTableColumnName == column.ColumnName).FirstOrDefault();
                                                if (columnDetail != null)
                                                {
                                                    columnDetail.ReportDataSetTableColumnName = column.ColumnName;
                                                }
                                                else
                                                {
                                                    columnDetail = new ReportDataSetTableColumn();
                                                    columnDetail.ReportDataSetTableColumnName = column.ColumnName;
                                                    ctx.ReportDataSetTableColumns.Add(columnDetail);
                                                    ReportColumnInDataSetTable reportColumn = new ReportColumnInDataSetTable();
                                                    reportColumn.ReportDataSetTable = table;
                                                    reportColumn.ReportDataSetTableColumn = columnDetail;
                                                    ctx.ReportColumnInDataSetTables.Add(reportColumn);
                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        table = new ReportDataSetTable();
                                        table.ReportDataSetTableName = dataSetTable.TableName;
                                        ReportDataSetInTable datasetInTable = new ReportDataSetInTable();
                                        datasetInTable.ReportDataSetTable = table;
                                        datasetInTable.ReportDataSetInfo = reportDataSetInfo;
                                        ctx.ReportDataSetInTables.Add(datasetInTable);
                                    }
                                }
                            }
                            else
                            {
                                reportDataSetInfo = new ReportDataSetInfo();
                                reportDataSetInfo.ReportDataSetDescription = CreateReportRequest.dataSetSchema.DataSetDescription;
                                reportDataSetInfo.ReportDataSetName = CreateReportRequest.dataSetSchema.DataSetName;
                                reportDataSetInfo.ReportDataSetQuery = CreateReportRequest.dataSetSchema.DataSetQuery;
                                ctx.ReportDataSetInfoes.Add(reportDataSetInfo);

                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<ReportDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ReportDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                    ctx.SaveChanges();
                    dbContextTransaction.Commit();
                    Result.Message = "";
                    Result.Status = eReport_REQUEST_RESPONSE.SUCCESS;
                    Result.Result = new ReportDataSetResult();
                    Result.Result.ReportDataSetID = CreateReportRequest.dataSetSchema.DataSetID;
                    Result.Result.AuthToken = null;
                    return Result;
                }
            }
        }

        public async Task<ReportResponse<ReportDataSetResult>> UpdateReportTemplate(ServiceRequest_ReportTemplate CreateReportRequest)
        {
            ReportResponse<ReportDataSetResult> Result = new ReportResponse<ReportDataSetResult>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        if (CreateReportRequest != null)
                        {
                            ReportDetail reportDetail = ctx.ReportDetails.Where(p => p.ReportID == CreateReportRequest.CreateReportEntity.ReportID).FirstOrDefault();
                            if (reportDetail != null)
                            {
                                await this.DeleteReportTemplate(reportDetail.ReportID, false);
                                await this.CreateReportTemplate(CreateReportRequest, false);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Result = new ReportResponse<ReportDataSetResult>();
                        Result.Message = ex.Message;
                        Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new ReportDataSetResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                    ctx.SaveChanges();
                    dbContextTransaction.Commit();
                    Result = new ReportResponse<ReportDataSetResult>();
                    Result.Message = "Sucess";
                    Result.Status = eReport_REQUEST_RESPONSE.DEFAULT_ERROR;
                    Result.Result = new ReportDataSetResult();
                    Result.Result.AuthToken = null;
                    return Result;
                }
            }
        }

    }
}
