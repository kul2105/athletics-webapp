﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Athletics.Report.Common
{
   public class VirLogger
    {
        public static string GetStamp()
        {
            try
            {
                string strReturn = String.Empty;
                string dateTime = DateTime.Now.ToString("yyyy.mm.dd****hh.mm.ss.ffffff****");
                string exeName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;


                System.Diagnostics.StackFrame frame = new System.Diagnostics.StackFrame(1);
                //Get Calling method name
                string methodName = frame.GetMethod().Name;

                //Gets Calling class name
                string className = frame.GetMethod().DeclaringType.Name;

                string temp = frame.GetMethod().GetParameters().ToString();

                string assemblyName = "";
                Assembly thisAssembly = Assembly.GetExecutingAssembly();
                StackTrace stackTrace = new StackTrace();
                StackFrame[] framess = stackTrace.GetFrames();
                foreach (var stackFrame in framess)
                {
                    var ownerAssembly = stackFrame.GetMethod().DeclaringType.Assembly;
                    if (ownerAssembly != thisAssembly)
                    {
                        assemblyName = Path.GetFileName(ownerAssembly.Location);
                        break;
                    }
                }
                strReturn = exeName + " ## " + assemblyName + " ## " + className + " ## " + methodName + " ## ";
                return strReturn;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to log: " + ex.Message);
                return "Failed to log";
            }
        }


        public void VirtuosoLog(string msg, string loggerStamp)
        {
            try
            {
               // devLogger.DevLog("**** " + loggerStamp + msg + " ****");
               ///// RaiseStatusLog(MsgType.INFO, "**** " + msg + " ****");
            }
            catch (Exception ex)
            {
              //  MessageBox.Show(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        public void VirtuosoWarn(string msg, string loggerStamp)
        {
           // devLogger.DevLog("#### " + loggerStamp + msg + " ####");
           // RaiseStatusLog(MsgType.ERROR, "**** " + msg + " ****");
        }

    }
}
