﻿using System.Windows.Controls;

namespace Athletics.Report.Views.ReportGenerator
{
    public interface ICreateReportTemplateView
    {
        object DataContext { get; set; }
        void ResetFocus();
        ListBox HeaderList { get; }
        ListBox FooterList { get; }
        ListBox BodyList { get; }
    }

}
