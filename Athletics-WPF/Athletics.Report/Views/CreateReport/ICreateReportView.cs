﻿namespace Athletics.Report.Views.ReportGenerator
{
    public interface ICreateReportView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
