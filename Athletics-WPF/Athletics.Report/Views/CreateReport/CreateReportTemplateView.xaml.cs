﻿using Athletics.Report.ViewModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Report.Views.ReportGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CreateReportTemplateView : Window, ICreateReportTemplateView
    {
        public CreateReportTemplateView()
        {
            InitializeComponent();
            //this.DataContext = new CreateReportViewModel(propertyGrid, listBoxMiddle);

        }
        public ListBox HeaderList => this.listBoxMiddleHeader;
        public ListBox FooterList => this.listBoxMiddleFooter;
        public ListBox BodyList => this.listBoxMiddleBody;

        public void ResetFocus()
        {
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);

        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }
    }
}
