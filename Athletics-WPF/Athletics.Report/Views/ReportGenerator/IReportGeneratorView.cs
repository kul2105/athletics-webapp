﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Report.Views.ReportGenerator
{
    public interface IReportGeneratorView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
       

}
