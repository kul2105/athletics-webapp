﻿using System.Windows.Controls;

namespace Athletics.Report.Views.ReportGenerator
{
    public interface IReportGeneratorListView
    {
        object DataContext { get; set; }
        void ResetFocus();
        
    }

}
