﻿using Athletics.ControlLibrary;
using Athletics.Report.Common;
using Athletics.Report.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Athletics.Report.Views.ReportGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ReportGeneratorView : UserControl, IReportGeneratorView
    {
        public ReportGeneratorView()
        {
            InitializeComponent();
            //  this.Loaded += ReportGeneratorView_Loaded;
        }

        public void ResetFocus()
        {
            //this.r
        }

        private void DataGrid1_Drop(object sender, DragEventArgs e)
        {
            //if (e.OriginalSource.GetType() == typeof(TextBlock))
            //{
            DataObject d = (DataObject)e.Data;
            DataSetSchema lane = d.GetData("Athletics.Report.ViewModel.DataSetSchema") as DataSetSchema;
            if (lane != null)
            {
                ReportGeneratorViewModel reportGenerationVm = this.DataContext as ReportGeneratorViewModel;
                if (reportGenerationVm != null)
                {
                    reportGenerationVm.DataSetColumnList = new ImpObservableCollection<DataSetInReportInfo>();
                    foreach (var Table in lane.TableList)
                    {
                        foreach (ColumnSchema columnSchema in Table.ColumnCollection)
                        {
                            DataSetInReportInfo dataSetInReportInfo = new DataSetInReportInfo();
                            dataSetInReportInfo.DataSetColumnName = columnSchema.ColumnName;
                            dataSetInReportInfo.DataSetTableId = Table.TableID;
                            reportGenerationVm.DataSetColumnList.Add(dataSetInReportInfo);
                        }
                    }

                }
            }
        }

        private void DataGrid2_Drop(object sender, DragEventArgs e)
        {

            DataObject d = (DataObject)e.Data;
            CreateReportEntity lane = d.GetData("Athletics.Report.ViewModel.CreateReportEntity") as CreateReportEntity;
            if (lane != null)
            {
                ReportGeneratorViewModel reportGenerationVm = this.DataContext as ReportGeneratorViewModel;
                if (reportGenerationVm != null)
                {
                    reportGenerationVm.AssignedReportInDataSet = new ImpObservableCollection<DataSetInReportInfo>();
                    reportGenerationVm.ReportColumnList = new ImpObservableCollection<DataSetInReportInfo>();
                    foreach (var item in lane.ReportTemplateData.Body)
                    {
                        if (item.ReportItemType == ReportType.DataTable)
                        {
                            foreach (var property in lane.ReportTemplateProperty.Body)
                            {
                                string[] splittedString = property.ItemContent.Split(new char[] { ',' });
                                foreach (string columnName in splittedString)
                                {
                                    DataSetInReportInfo dataSetIn = new DataSetInReportInfo();
                                    dataSetIn.ReportColumnName = columnName;
                                    reportGenerationVm.AssignedReportInDataSet.Add(dataSetIn);
                                    reportGenerationVm.ReportColumnList.Add(dataSetIn);
                                }

                            }
                        }
                    }

                    foreach (var item in lane.ReportTemplateData.Header)
                    {
                        if (item.ReportItemType == ReportType.DataTable)
                        {
                            foreach (var property in lane.ReportTemplateProperty.Header)
                            {
                                string[] splittedString = property.ItemContent.Split(new char[] { ',' });
                                foreach (string columnName in splittedString)
                                {
                                    DataSetInReportInfo dataSetIn = new DataSetInReportInfo();
                                    dataSetIn.ReportColumnName = columnName;
                                    reportGenerationVm.AssignedReportInDataSet.Add(dataSetIn);
                                    reportGenerationVm.ReportColumnList.Add(dataSetIn);
                                }

                            }
                        }
                    }
                    foreach (var item in lane.ReportTemplateData.Footer)
                    {
                        if (item.ReportItemType == ReportType.DataTable)
                        {
                            foreach (var property in lane.ReportTemplateProperty.Footer)
                            {
                                string[] splittedString = property.ItemContent.Split(new char[] { ',' });
                                foreach (string columnName in splittedString)
                                {
                                    DataSetInReportInfo dataSetIn = new DataSetInReportInfo();
                                    dataSetIn.ReportColumnName = columnName;
                                    reportGenerationVm.AssignedReportInDataSet.Add(dataSetIn);
                                    reportGenerationVm.ReportColumnList.Add(dataSetIn);
                                }

                            }
                        }
                    }
                }
            }

        }

        private void DataGrid13_Drop(object sender, DragEventArgs e)
        {

        }

        //void ReportGeneratorView_Loaded(object sender, RoutedEventArgs e)
        //{
        //    ReportGeneratorViewModel vm = this.DataContext as ReportGeneratorViewModel;
        //    if(vm!=null)
        //    {
        //        vm.PopulateTableList();
        //        vm.PopulateReportHeads();
        //    }

        //}
    }
}
