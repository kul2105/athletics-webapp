﻿using Athletics.Report.Common;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace Athletics.Report.Utils
{
    public class InteractiveCommand : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            try
            {
                if (base.AssociatedObject != null)
                {
                    if (parameter.GetType()!= typeof(EventArgs))
                    {
                        ICommand command = this.ResolveCommand();
                        if ((command != null) && command.CanExecute(parameter))
                        {
                            command.Execute(parameter);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                StaticLogger.log.VirtuosoWarn(ex.Message, VirLogger.GetStamp());
            }

        }

        private ICommand ResolveCommand()
        {
            ICommand command = null;
            try
            {

                if (this.Command != null)
                {
                    return this.Command;
                }
                if (base.AssociatedObject != null)
                {
                    foreach (PropertyInfo info in base.AssociatedObject.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                    {
                        if (typeof(ICommand).IsAssignableFrom(info.PropertyType) && string.Equals(info.Name, this.CommandName, StringComparison.Ordinal))
                        {
                            command = (ICommand)info.GetValue(base.AssociatedObject, null);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                StaticLogger.log.VirtuosoWarn(ex.Message, VirLogger.GetStamp());
            }
            return command;
        }

        private string commandName;
        public string CommandName
        {
            get
            {
                base.ReadPreamble();
                return this.commandName;
            }
            set
            {
                if (this.CommandName != value)
                {
                    base.WritePreamble();
                    this.commandName = value;
                    base.WritePostscript();
                }
            }
        }

        #region Command
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(InteractiveCommand), new UIPropertyMetadata(null));
        #endregion
    }
}
