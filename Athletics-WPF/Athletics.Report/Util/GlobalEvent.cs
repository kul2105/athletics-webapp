﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Report.Util
{
    public class GlobalEvent
    {
        public delegate void TargetControlLoadedDelegate(object targetObject);
        public static event TargetControlLoadedDelegate TargetControlLoaded;
        public static void RaiseTargetControlLoaded(object targetObject)
        {
            if (TargetControlLoaded != null)
                TargetControlLoaded(targetObject);
        }

    }
}
