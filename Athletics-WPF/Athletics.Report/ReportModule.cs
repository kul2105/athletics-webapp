﻿using Athletics.Report.Controllers;
using Athletics.Report.ViewModel;
using Athletics.Report.Views.GenerateDataSet;
using Athletics.Report.Views.ReportGenerator;
using Athletics.Report.Views.ViewReport;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

namespace Athletics.Report
{
    public class ReportModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public ReportModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<IReportStateController, ReportStateController>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IReportGeneratorView, ReportGeneratorView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IReportGeneratorViewModel, ReportGeneratorViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ICreateReportView, CreateReportView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ICreateReportViewModel, CreateReportViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IDataSetViewModel, DataSetViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IDataSetView, DataSetView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ICreateDataSetViewModel, CreateDataSetViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ICreateDataSetView, CreateDataSetView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<ICreateReportTemplateViewModel, CreateReportTemplateViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ICreateReportTemplateView, CreateReportTemplateView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IReportGeneratorListViewModel, ReportGeneratorListViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IReportGeneratorListView, ReportGeneratorListView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IReportListViewModel, ReportListViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IReportViewList, ReportViewList>(new ContainerControlledLifetimeManager());


            this.container.RegisterType<IReportManager, ReportManager>(new ContainerControlledLifetimeManager());
            this.regionManager.RegisterViewWithRegion("ReportGeneratorRegion", typeof(ReportGeneratorView));

        }
    }
}
