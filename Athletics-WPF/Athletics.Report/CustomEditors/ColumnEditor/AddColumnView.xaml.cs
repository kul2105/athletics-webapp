﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Report.Views.ReportGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddColumnView : Window
    {
        public static RoutedUICommand DeleteColumn = new RoutedUICommand("DeleteColumn", "DeleteColumn", typeof(AddColumnView));
        public string ColumnListText = string.Empty;
        private List<string> columnList = new List<string>();
        public AddColumnView(string textColumn)
        {
            InitializeComponent();
            CommandBindings.Add(new System.Windows.Input.CommandBinding(DeleteColumn, DeleteEventArgs, CanExecuteEventHandler));
            this.ColumnListText = textColumn;
            if (!string.IsNullOrEmpty(ColumnListText))
            {
                columnList = ColumnListText.Split(new char[] { ',' }).ToList();
                foreach (var str in columnList)
                {
                    this.ColumnList.Items.Add(str);
                }
                
            }
            this.txtColumnName.Focus();
            this.DataContext = this;
        }

        private void CanExecuteEventHandler(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.ColumnList.Items.Count > 0)
                e.Handled = true;
        }

        private void DeleteEventArgs(object sender, ExecutedRoutedEventArgs e)
        {
            string selectedItem = (string)this.ColumnList.SelectedItem;
            this.ColumnList.Items.Remove(selectedItem);
            columnList.Remove(selectedItem);
            ColumnListText = string.Join(",", columnList);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!this.ColumnList.Items.Contains(this.txtColumnName.Text))
            {
                this.ColumnList.Items.Add(this.txtColumnName.Text);
                columnList.Add(this.txtColumnName.Text);
                ColumnListText = string.Join(",", columnList);
                this.txtColumnName.Text = string.Empty;
                this.txtColumnName.Focus();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string selectedItem = (string)this.ColumnList.SelectedItem;
            this.ColumnList.Items.Remove(selectedItem);
            columnList.Remove(selectedItem);
            ColumnListText = string.Join(",", columnList);
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);

        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }
    }
}
