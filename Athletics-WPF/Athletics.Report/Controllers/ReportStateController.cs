﻿using Microsoft.Practices.Prism.Mvvm;
using System;

namespace Athletics.Report.Controllers
{
    public class ReportStateController : BindableBase, IReportStateController
    {
        public ReportStateController()
        {
        }


        private e_Report_STATE _Report_STATE = e_Report_STATE.REPORT_DATA_GENERATOR;
        public e_Report_STATE Report_STATE
        {
            get
            {
                return _Report_STATE;
            }
            set
            {
                if (_Report_STATE != value)
                {
                    _Report_STATE = value;
                    OnPropertyChanged("Report_STATE");
                    switch (value)
                    {
                        case e_Report_STATE.REPORT_DATA_GENERATOR:
                            _s_Report_STATE = "REPORT_DATA_GENERATOR";
                            break;
                        case e_Report_STATE.NONE:
                            break;
                        default:
                            break;
                    }

                    OnPropertyChanged("S_Report_STATE");
                    Action<e_Report_STATE> pS_Report_STATEChanged = Report_STATEChanged;
                    if (pS_Report_STATEChanged != null)
                    {
                        pS_Report_STATEChanged(value);
                    }
                }
            }
        }


        private string _s_Report_STATE;
        public string sReportState
        {
            get { return _s_Report_STATE; }
        }


        public event Action<e_Report_STATE> Report_STATEChanged;
    }
}
