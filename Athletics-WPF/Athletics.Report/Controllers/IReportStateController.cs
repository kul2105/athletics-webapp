﻿using System;

namespace Athletics.Report.Controllers
{
    public enum e_Report_STATE
    {
        REPORT_DATA_GENERATOR,
        NONE
    }

    public interface IReportStateController
    {
        // For use by the view models
        e_Report_STATE Report_STATE { get; set; }

        // For use by the views
        string sReportState { get; }

        event Action<e_Report_STATE> Report_STATEChanged;
    }
}
