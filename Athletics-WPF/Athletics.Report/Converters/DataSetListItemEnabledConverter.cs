﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Athletics.Report.Converter
{
    public class DataSetListItemEnabledConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool IsPK = false;
            bool IsFK = false;
            bool.TryParse(values[1].ToString(), out IsPK);
            bool.TryParse(values[0].ToString(), out IsFK);
            bool isEnabled = true;
            if (IsPK)
            {
                isEnabled = false;
            }
            else if(IsFK)
            {
                isEnabled = false;
            }
            return isEnabled;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
