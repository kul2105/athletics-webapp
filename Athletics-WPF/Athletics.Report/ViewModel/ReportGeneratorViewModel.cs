﻿using Athletics.ControlLibrary;
using Athletics.Entities.Models;
using Athletics.ModuleCommon;
using Athletics.Report.Common;
using Athletics.Report.Services.API;
using Athletics.Report.Views.ReportGenerator;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Report.ViewModel
{
    public class ReportGeneratorViewModel : BindableBase, IReportGeneratorViewModel, IGenericInteractionView<List<ServiceRequest_AssignReportToDataSet>>
    {
        private ICreateReportTemplateView _View;
        IReportManager reportManager;
        public ReportGeneratorViewModel(ICreateReportTemplateView view, IReportManager reportMng)
        {
            _View = view;
            _PART_CLOSE = new DelegateCommand(PART_CLOSE_Click);
            _SaveAssignCommand = new DelegateCommand(SaveAssignCommandHandler);
            _View.DataContext = this;
            reportManager = reportMng;
            PopulateReportHeads();
            PopulateTableList();
        }

        private void SaveAssignCommandHandler()
        {
            this.PART_CLOSE_Click();
        }

        public void PopulateTableList()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<DataSetSchema> Response = await reportManager.GetAllTableSchema();
                foreach (var item in Response)
                {
                    TableList.Add(item);
                }
            })).Wait();
        }

        public void PopulateReportHeads()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<CreateReportEntity> Response = await reportManager.GetAllReportTemplate();
                foreach (var item in Response)
                {
                    ReportHeadList.Add(item);
                }
            })).Wait();

        }

        public void SetEntity(List<ServiceRequest_AssignReportToDataSet> entityList)
        {
            foreach (var entity in entityList)
            {
                if (entity == null) return;
                if (entity.DataSetID > 0)
                {
                    this.SelectedTable = TableList.Where(p => p.DataSetID == entity.DataSetID).FirstOrDefault();
                    this.SelectedReport = ReportHeadList.Where(p => p.ReportID == entity.ReportID).FirstOrDefault();
                    if (this.SelectedTable != null)
                    {
                        this.DataSetColumnList = new ImpObservableCollection<DataSetInReportInfo>();
                        List<ImpObservableCollection<ColumnSchema>> columnList = this.SelectedTable.TableList.Select(p => p.ColumnCollection).ToList();
                        foreach (ImpObservableCollection<ColumnSchema> item in columnList)
                        {
                            foreach (ColumnSchema columnSchema in item)
                            {
                                DataSetInReportInfo dataSetInReportInfo = new DataSetInReportInfo();
                                dataSetInReportInfo.DataSetColumnName = columnSchema.ColumnName;
                                this.DataSetColumnList.Add(dataSetInReportInfo);
                            }
                        }

                    }
                    if (this.SelectedReport != null)
                    {
                        this.ReportColumnList = new ImpObservableCollection<DataSetInReportInfo>();
                        foreach (var item in this.SelectedReport.ReportTemplateData.Body)
                        {
                            if (item.ReportItemType == ReportType.DataTable)
                            {
                                foreach (var property in this.SelectedReport.ReportTemplateProperty.Body)
                                {
                                    string[] splittedString = property.ItemContent.Split(new char[] { ',' });
                                    foreach (string columnName in splittedString)
                                    {
                                        DataSetInReportInfo dataSetIn = new DataSetInReportInfo();
                                        dataSetIn.ReportColumnName = columnName;
                                        //this.AssignedReportInDataSet.Add(dataSetIn);
                                        this.ReportColumnList.Add(dataSetIn);
                                    }

                                }
                            }
                        }

                        foreach (var item in this.SelectedReport.ReportTemplateData.Header)
                        {
                            if (item.ReportItemType == ReportType.DataTable)
                            {
                                foreach (var property in this.SelectedReport.ReportTemplateProperty.Header)
                                {
                                    string[] splittedString = property.ItemContent.Split(new char[] { ',' });
                                    foreach (string columnName in splittedString)
                                    {
                                        DataSetInReportInfo dataSetIn = new DataSetInReportInfo();
                                        dataSetIn.ReportColumnName = columnName;
                                        // reportGenerationVm.AssignedReportInDataSet.Add(dataSetIn);
                                        this.ReportColumnList.Add(dataSetIn);
                                    }

                                }
                            }
                        }
                        foreach (var item in this.SelectedReport.ReportTemplateData.Footer)
                        {
                            if (item.ReportItemType == ReportType.DataTable)
                            {
                                foreach (var property in this.SelectedReport.ReportTemplateProperty.Footer)
                                {
                                    string[] splittedString = property.ItemContent.Split(new char[] { ',' });
                                    foreach (string columnName in splittedString)
                                    {
                                        DataSetInReportInfo dataSetIn = new DataSetInReportInfo();
                                        dataSetIn.ReportColumnName = columnName;
                                        // reportGenerationVm.AssignedReportInDataSet.Add(dataSetIn);
                                        this.ReportColumnList.Add(dataSetIn);
                                    }

                                }
                            }
                        }
                    }

                    foreach (var item in entity.AssignedReportList)
                    {
                        this.AssignedReportInDataSet.Add(item);
                    }

                }
            }

        }

        public List<ServiceRequest_AssignReportToDataSet> GetEntity()
        {
            if (SelectedReport == null) return null;
            PopulateReportHeads();
            List<ServiceRequest_AssignReportToDataSet> serviceList = new List<ServiceRequest_AssignReportToDataSet>();
            foreach (var table in SelectedTable.TableList)
            {
                ServiceRequest_AssignReportToDataSet service = new ServiceRequest_AssignReportToDataSet();
                service.DataSetID = table.TableID;
                service.ReportID = SelectedReport.ReportID;
                service.ReportName = SelectedReport.ReportName;
                service.DataSetName = SelectedTable.DataSetName;
                service.ReportTemplate = this.GenerateTemplate();
                service.AssignedReportList = new List<DataSetInReportInfo>();
                foreach (var item in this.AssignedReportInDataSet)
                {
                    DataSetInReportInfo reportInfo = new DataSetInReportInfo();
                    reportInfo.DataSetColumnName = item.DataSetColumnName;
                    reportInfo.ReportColumnName = item.ReportColumnName;
                    service.AssignedReportList.Add(item);
                }
                serviceList.Add(service);
            }


            return serviceList;
        }

        private string GenerateTemplate()
        {
            string reprtStartupXamlns = "<FlowDocument xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" \n" +
        "xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"\n" +
                     "xmlns:xrd=\"clr-namespace:CodeReason.Reports.Document;assembly=CodeReason.Reports\" " +
                     "PageHeight=\"29.7cm\" PageWidth=\"21cm\" ColumnWidth=\"21cm\">";

            StringBuilder sb = new StringBuilder();
            sb.Append(reprtStartupXamlns);
            sb.Append("<Section Padding=\"80, 10, 40, 10\" FontSize=\"12\">");
            sb.AppendLine();
            sb.Append("<xrd:SectionDataGroup DataGroupName = \"ItemList\">");
            sb.AppendLine();
            sb.Append("<xrd:ReportProperties>");
            sb.Append("<xrd:ReportProperties.ReportName>");
            sb.Append(this.SelectedReport.ReportName);
            sb.Append("</xrd:ReportProperties.ReportName>");
            sb.AppendLine();
            sb.Append("<xrd:ReportProperties.ReportTitle>");
            sb.Append(this.SelectedReport.ReportDescription);
            sb.Append("</xrd:ReportProperties.ReportTitle>");
            sb.AppendLine();
            sb.Append("</xrd:ReportProperties>");
            sb.AppendLine();
            sb.Append("<xrd:SectionReportHeader PageHeaderHeight = \"2\" Padding = \"10,10,10,0\" FontSize = \"12\">");
            sb.AppendLine();
            sb.Append("<Table CellSpacing = \"0\">");
            sb.AppendLine();
            sb.Append("<Table.Columns>");
            sb.AppendLine();
            sb.Append("<TableColumn Width = \"*\"/>");
            sb.AppendLine();
            sb.Append("<TableColumn Width = \"*\" />");
            sb.AppendLine();
            sb.Append("</Table.Columns>");
            sb.AppendLine();
            sb.Append("<TableRowGroup>");
            sb.AppendLine();
            sb.Append("<TableRow>");
            sb.AppendLine();
            sb.AppendLine("<TableCell>");
            sb.AppendLine("<Paragraph>");
            sb.AppendLine("<xrd:InlineContextValue PropertyName = \"ReportTitle\" />");
            sb.AppendLine("</Paragraph>");
            sb.AppendLine("</TableCell>");
            sb.Append("<TableCell>");
            sb.AppendLine("<Paragraph TextAlignment = \"Right\">");
            sb.AppendLine("<xrd:InlineDocumentValue PropertyName = \"PrintDate\" Format = \"dd.MM.yyyy HH:mm:ss\"/>");
            sb.AppendLine("</Paragraph>");
            sb.AppendLine("</TableCell>");
            sb.AppendLine("</TableRow>");
            sb.AppendLine("</TableRowGroup>");
            sb.AppendLine("</Table>");
            sb.AppendLine("</xrd:SectionReportHeader>");
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
           {
               List<GetReportDetailByReportID_Result> result = await reportManager.GetReportDetailByReportID(this.SelectedReport.ReportID);
               List<string> GroupNameList = result.Select(p => p.ReportGroupName).Distinct().ToList();
               List<ReportSection> ReportSectionList = new List<ReportSection>();
               foreach (var reportGroup in GroupNameList)
               {
                   ReportSection section = (ReportSection)Enum.Parse(typeof(ReportSection), reportGroup);
                   ReportSectionList.Add(section);
               }
               foreach (var reportGroup in ReportSectionList.OrderBy(p => (int)p))
               {
                   switch (reportGroup)
                   {
                       case ReportSection.Header:
                           List<GetReportDetailByReportID_Result> HeaderGroupList = result.Where(p => p.ReportGroupName == reportGroup.ToString()).ToList();
                           foreach (var item in HeaderGroupList)
                           {
                               ConstructContent(sb, item);
                           }

                           break;
                       case ReportSection.Footer:
                           List<GetReportDetailByReportID_Result> FooterGroup = result.Where(p => p.ReportGroupName == reportGroup.ToString()).ToList();
                           foreach (var item in FooterGroup)
                           {
                               ConstructContent(sb, item);
                           }
                           break;
                       case ReportSection.Body:
                           List<GetReportDetailByReportID_Result> BodyGroupList = result.Where(p => p.ReportGroupName == reportGroup.ToString()).ToList();
                           foreach (var item in BodyGroupList)
                           {
                               ConstructContent(sb, item);
                           }
                           break;
                       default:
                           break;
                   }

               }


           })).Wait();
            sb.AppendLine("<xrd:SectionReportFooter PageFooterHeight = \"2\" Padding = \"10, 0, 10, 10\" FontSize = \"12\">");
            sb.AppendLine("<Table CellSpacing = \"0\">");
            sb.AppendLine("<Table.Columns>");
            sb.AppendLine("<TableColumn Width = \"*\"/>");
            sb.AppendLine("<TableColumn Width = \"*\" />");
            sb.AppendLine("</Table.Columns>");
            sb.AppendLine("<TableRowGroup>");
            sb.AppendLine("<TableRow>");
            sb.AppendLine("<TableCell>");
            sb.AppendLine("<Paragraph TextAlignment = \"Right\">");
            sb.AppendLine("Page");
            sb.AppendLine("<xrd:InlineContextValue PropertyName = \"PageNumber\" FontWeight = \"Bold\" /> of");
            sb.AppendLine("<xrd:InlineContextValue PropertyName = \"PageCount\" FontWeight = \"Bold\" />");
            sb.AppendLine("</Paragraph>");
            sb.AppendLine("</TableCell>");
            sb.AppendLine("</TableRow>");
            sb.AppendLine("</TableRowGroup>");
            sb.AppendLine("</Table>");
            sb.AppendLine("</xrd:SectionReportFooter>");
            sb.AppendLine("</xrd:SectionDataGroup>");
            sb.AppendLine("</Section>");
            sb.AppendLine("</FlowDocument>");
            return sb.ToString();
        }

        private void ConstructContent(StringBuilder sb, GetReportDetailByReportID_Result item)
        {
            if (string.IsNullOrEmpty(item.FontSizeName))
            {
                item.FontSizeName = "12";
            }
            if (string.IsNullOrEmpty(item.FontWeightName))
            {
                item.FontWeightName = "Normal";
            }
            if (string.IsNullOrEmpty(item.FontStyleName))
            {
                item.FontStyleName = "Normal";
            }
            if (string.IsNullOrEmpty(item.FontFamilyName))
            {
                item.FontFamilyName = "Roboto";
            }
            ReportType itemType = (ReportType)Enum.Parse(typeof(ReportType), item.ReportDetailInfoItemType);
            if (itemType == ReportType.Text)
            {
                sb.Append("<Paragraph ");
                sb.Append("FontSize= ");
                sb.Append("\"");
                sb.Append(item.FontSizeName);
                sb.Append("\"");
                sb.Append(" FontWeight= ");
                sb.Append("\"");
                sb.Append(item.FontWeightName);
                sb.Append("\"");
                sb.Append(" FontFamily= ");
                sb.Append("\"");
                sb.Append(item.FontFamilyName);
                sb.Append("\"");
                sb.Append(" FontStyle= ");
                sb.Append("\"");
                sb.Append(item.FontStyleName);
                sb.Append("\"");
                sb.Append(">");
                sb.Append(item.ItemContent);
                sb.Append("</Paragraph>");
            }
            if (itemType == ReportType.DataTable)
            {
                sb.Append("<Table CellSpacing = \"0\" BorderBrush = \"Black\" BorderThickness = \"0.02cm\">");
                sb.AppendLine();
                sb.Append("<Table.Resources>");
                sb.AppendLine();
                //sb.Append("<!--Style for header / footer rows. -->");
                sb.AppendLine();
                sb.Append("<Style x:Key = \"headerFooterRowStyle\" TargetType = \"{x:Type TableRowGroup}\" >");
                sb.AppendLine();

                sb.Append("<Setter Property = \"FontWeight\" Value = \"" + item.FontWeightName + "\" />");
                sb.AppendLine();

                sb.Append("<Setter Property = \"FontSize\" Value = \"" + item.FontSizeName + "\" />");
                sb.AppendLine();
                sb.Append("</Style>");
                sb.AppendLine();
                //sb.Append("<!--Style for data rows. -->");
                sb.AppendLine();
                sb.Append("<Style x:Key = \"dataRowStyle\" TargetType = \"{x:Type TableRowGroup}\">");
                sb.AppendLine();
                sb.Append("<Setter Property = \"FontSize\" Value = \"" + item.FontSizeName + "\"/>");
                sb.AppendLine();
                sb.Append("</Style>");
                sb.AppendLine();
                sb.Append("<Style TargetType = \"{x:Type TableCell}\" >");
                sb.AppendLine();
                sb.Append("<Setter Property = \"Padding\" Value = \"0.1cm\"/>");
                sb.AppendLine();
                sb.Append("<Setter Property = \"BorderBrush\" Value = \"Black\"/>");
                sb.AppendLine();
                sb.Append("<Setter Property = \"BorderThickness\" Value = \"0.01cm\"/>");
                sb.AppendLine();
                sb.Append("</Style>");
                sb.AppendLine();
                sb.Append("</Table.Resources>");
                sb.AppendLine();
                string[] columnNames = item.ItemContent.Split(new char[] { ',' });
                sb.AppendLine("<Table.Columns>");
                foreach (string columnName in columnNames)
                {
                    int columnPercentage = 100 / columnNames.Count();
                    sb.AppendLine("<TableColumn Width = \"0." + columnPercentage + "*\" />");
                }
                sb.AppendLine("</Table.Columns>");
                sb.AppendLine("<TableRowGroup Style = \"{ StaticResource headerFooterRowStyle}\" >");
                sb.AppendLine("<TableRow>");
                foreach (string columnName in columnNames)
                {
                    sb.AppendLine("<TableCell>");
                    sb.AppendLine("<Paragraph TextAlignment = \"Center\">");
                    sb.AppendLine("<Bold> " + columnName + "</Bold>");
                    sb.AppendLine("</Paragraph>");
                    sb.AppendLine("</TableCell>");
                }
                sb.AppendLine("</TableRow>");
                sb.AppendLine("</TableRowGroup>");
                sb.AppendLine("<TableRowGroup Style = \"{StaticResource dataRowStyle}\">");
                sb.AppendLine("<xrd:TableRowForDataTable TableName = \"table1\">");

                foreach (string columnName in columnNames)
                {
                    DataSetInReportInfo reportInfo = this.AssignedReportInDataSet.Where(p => p.ReportColumnName == columnName).FirstOrDefault();
                    if (reportInfo != null)
                    {
                        sb.AppendLine("<TableCell>");
                        sb.AppendLine("<Paragraph>");
                        sb.AppendLine("<xrd:InlineTableCellValue PropertyName = \"" + reportInfo.DataSetColumnName + "\"/>");
                        sb.AppendLine("</Paragraph>");
                        sb.AppendLine("</TableCell>");
                    }
                }
                sb.AppendLine("</xrd:TableRowForDataTable>");
                sb.AppendLine("</TableRowGroup>");
                sb.AppendLine("</Table>");
            }
        }

        private ImpObservableCollection<DataSetSchema> _TableList = new ImpObservableCollection<DataSetSchema>();
        public ImpObservableCollection<DataSetSchema> TableList
        {
            get { return _TableList; }
            set
            {
                _TableList = value;
                base.OnPropertyChanged("TableList");
            }
        }

        private ImpObservableCollection<CreateReportEntity> _ReportHeadList = new ImpObservableCollection<CreateReportEntity>();
        public ImpObservableCollection<CreateReportEntity> ReportHeadList
        {
            get { return _ReportHeadList; }
            set
            {
                _ReportHeadList = value;
                base.OnPropertyChanged("ReportHeadList");
            }
        }
        private ImpObservableCollection<DataSetInReportInfo> _AssignedReportInDataSet = new ImpObservableCollection<DataSetInReportInfo>();
        public ImpObservableCollection<DataSetInReportInfo> AssignedReportInDataSet
        {
            get { return _AssignedReportInDataSet; }
            set
            {
                _AssignedReportInDataSet = value;
                base.OnPropertyChanged("AssignedReportInDataSet");
            }
        }

        private ImpObservableCollection<DataSetInReportInfo> _ReportColumnList = new ImpObservableCollection<DataSetInReportInfo>();
        public ImpObservableCollection<DataSetInReportInfo> ReportColumnList
        {
            get { return _ReportColumnList; }
            set
            {
                _ReportColumnList = value;
                base.OnPropertyChanged("ReportColumnList");
            }
        }

        private ImpObservableCollection<DataSetInReportInfo> _DataSetColumnList = new ImpObservableCollection<DataSetInReportInfo>();
        public ImpObservableCollection<DataSetInReportInfo> DataSetColumnList
        {
            get { return _DataSetColumnList; }
            set
            {
                _DataSetColumnList = value;
                base.OnPropertyChanged("DataSetColumnList");
            }
        }
        private DataSetInReportInfo _SelectedColumn;
        public DataSetInReportInfo SelectedColumn
        {
            get { return _SelectedColumn; }
            set
            {
                _SelectedColumn = value;
                base.OnPropertyChanged("SelectedColumn");
                if (AssignSelectedIndex >= 0)
                {
                    AssignedReportInDataSet[AssignSelectedIndex].DataSetTableId = value.DataSetTableId;
                    AssignedReportInDataSet[AssignSelectedIndex].DataSetColumnName = value.DataSetColumnName;
                }

            }
        }
        private CreateReportEntity _SelectedReport;
        public CreateReportEntity SelectedReport
        {
            get { return _SelectedReport; }
            set
            {
                _SelectedReport = value;
                base.OnPropertyChanged("SelectedReport");
            }
        }
        private DataSetSchema _SelectedTable;
        public DataSetSchema SelectedTable
        {
            get { return _SelectedTable; }
            set
            {
                _SelectedTable = value;
                base.OnPropertyChanged("SelectedTable");
            }
        }
        private int _AssignSelectedIndex = -1;
        public int AssignSelectedIndex
        {
            get { return _AssignSelectedIndex; }
            set
            {
                _AssignSelectedIndex = value;
                base.OnPropertyChanged("AssignSelectedIndex");

            }
        }

        private readonly ICommand _PART_CLOSE;
        public ICommand PART_CLOSE
        {
            get
            {
                return _PART_CLOSE;
            }
        }
        private readonly ICommand _SaveAssignCommand;
        public ICommand SaveAssignCommand
        {
            get
            {
                return _SaveAssignCommand;
            }
        }

        private void PART_CLOSE_Click()
        {
            Window window = null;
            foreach (Window item in Application.Current.Windows)
            {
                if (item.Name == "AssignReport")
                {
                    window = item;
                }
            }
            if (window != null)
            {
                window.Close();
            }
        }
    }
    public class DataSetInReportInfo : BindableBase
    {
        private int _ReportDataSetMappedDetailID;
        public int ReportDataSetMappedDetailID
        {
            get { return _ReportDataSetMappedDetailID; }
            set
            {
                _ReportDataSetMappedDetailID = value;
                base.OnPropertyChanged("ReportDataSetMappedDetailID");
            }
        }

        private string _ReportColumnName;
        public string ReportColumnName
        {
            get { return _ReportColumnName; }
            set
            {
                _ReportColumnName = value;
                base.OnPropertyChanged("ReportColumnName");
            }
        }
        private string _DataSetColumnName;
        public string DataSetColumnName
        {
            get { return _DataSetColumnName; }
            set
            {
                _DataSetColumnName = value;
                base.OnPropertyChanged("DataSetColumnName");
            }
        }

        private int _DataSetTableId;
        public int DataSetTableId
        {
            get { return _DataSetTableId; }
            set
            {
                _DataSetTableId = value;
                base.OnPropertyChanged("DataSetTableId");
            }
        }
    }
}
