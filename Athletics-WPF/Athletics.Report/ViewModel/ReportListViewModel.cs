﻿using Athletics.ControlLibrary;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.ModuleCommon;
using Athletics.Report.Common;
using Athletics.Report.Services.API;
using Athletics.Report.Views.GenerateDataSet;
using Athletics.Report.Views.ViewReport;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Report.ViewModel
{

    public class ReportListViewModel : BindableBase, IReportListViewModel, IGenericInteractionView<ReportListViewModel>
    {
        IReportViewList _view;
        IUnityContainer unityContainer;
        IReportManager reportManager;
        string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AthleticsConnection"].ConnectionString;
        public ReportListViewModel(IReportViewList view, IUnityContainer unity, IReportManager reportMng)
        {
            _view = view;
            unityContainer = unity;
            reportManager = reportMng;
            _view.DataContext = this;
            GetAllReportToView();
            _PreviewReportCommand = new DelegateCommand(PreviewReportCommandHandler);

        }

        private void PreviewReportCommandHandler()
        {
            IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            Window window = new ReportView(SelectedReportViewDetail.ReportTemplate, QueryResult.Table);
            dialog.BindView(window);
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
        }

        #region Methods

        private void GetAllReportToView()
        {
            ReportViewDetailList.Clear();
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<GetAllReportDetail_Result> Response = await reportManager.GetGetAllReportViewDetail();
                foreach (var item in Response)
                {
                    ReportViewDetailList.Add(item);
                }
            })).Wait();
        }
        private void propertyGrid_SelectedObjectsChanged(object sender, EventArgs e)
        {
        }
        public ReportListViewModel GetEntity()
        {
            GetAllReportToView();
            return new ReportListViewModel(_view, unityContainer, reportManager);
        }

        public void SetEntity(ReportListViewModel entity)
        {
        }

        #endregion

        #region Public Properties

        private GetAllReportDetail_Result _SelectedReportViewDetail;
        public GetAllReportDetail_Result SelectedReportViewDetail
        {
            get { return _SelectedReportViewDetail; }
            set
            {
                _SelectedReportViewDetail = value;
                base.OnPropertyChanged("SelectedReportViewDetail");
                this.GetQueryData(value.ReportDataSetQuery);
            }
        }

        private void GetQueryData(string reportDataSetQuery)
        {
            DataTable dt = new DataTable("table1");
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlDataAdapter sda = new SqlDataAdapter(reportDataSetQuery, con);
                try
                {
                    con.Open();
                    sda.Fill(dt);
                }
                catch (SqlException se)
                {
                }
                finally
                {
                    con.Close();
                }
            }
            QueryResult = dt.DefaultView;
        }

        private ImpObservableCollection<GetAllReportDetail_Result> _ReportViewDetailList = new ImpObservableCollection<GetAllReportDetail_Result>();
        public ImpObservableCollection<GetAllReportDetail_Result> ReportViewDetailList
        {
            get { return _ReportViewDetailList; }
            set
            {
                _ReportViewDetailList = value;
                base.OnPropertyChanged("ReportViewDetailList");
            }
        }
        private DataView _QueryResult;
        public DataView QueryResult
        {
            get { return _QueryResult; }
            set
            {
                _QueryResult = value;
                base.OnPropertyChanged("QueryResult");
            }
        }

        private ICommand _PreviewReportCommand;
        public ICommand PreviewReportCommand
        {
            get
            {
                return _PreviewReportCommand;
            }
        }

        #endregion



    }
}
