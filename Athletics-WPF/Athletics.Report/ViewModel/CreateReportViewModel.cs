﻿using Athletics.ControlLibrary;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.ModuleCommon;
using Athletics.Report.Common;
using Athletics.Report.Services.API;
using Athletics.Report.Views.ReportGenerator;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Athletics.Report.ViewModel
{
    public class ReportTemplateData : BindableBase
    {

        private ImpObservableCollection<ReportItemTemplateData> _Header = new ImpObservableCollection<ReportItemTemplateData>();
        public ImpObservableCollection<ReportItemTemplateData> Header
        {
            get { return _Header; }
            set
            {
                _Header = value;
                OnPropertyChanged("Header");
            }
        }

        private ImpObservableCollection<ReportItemTemplateData> _Body = new ImpObservableCollection<ReportItemTemplateData>();
        public ImpObservableCollection<ReportItemTemplateData> Body
        {
            get { return _Body; }
            set
            {
                _Body = value;
                OnPropertyChanged("Body");
            }
        }

        private ImpObservableCollection<ReportItemTemplateData> _Footer = new ImpObservableCollection<ReportItemTemplateData>();
        public ImpObservableCollection<ReportItemTemplateData> Footer
        {
            get { return _Footer; }
            set
            {
                _Footer = value;
                OnPropertyChanged("Footer");
            }
        }
    }

    public class ReportTemplateProperty : BindableBase
    {
        private ImpObservableCollection<ReportItemProperty> _Header = new ImpObservableCollection<ReportItemProperty>();
        public ImpObservableCollection<ReportItemProperty> Header
        {
            get { return _Header; }
            set
            {
                _Header = value;
                OnPropertyChanged("Header");
            }
        }

        private ImpObservableCollection<ReportItemProperty> _Body = new ImpObservableCollection<ReportItemProperty>();
        public ImpObservableCollection<ReportItemProperty> Body
        {
            get { return _Body; }
            set
            {
                _Body = value;
                OnPropertyChanged("Body");
            }
        }

        private ImpObservableCollection<ReportItemProperty> _Footer = new ImpObservableCollection<ReportItemProperty>();
        public ImpObservableCollection<ReportItemProperty> Footer
        {
            get { return _Footer; }
            set
            {
                _Footer = value;
                OnPropertyChanged("Footer");
            }
        }
    }

    public enum ReportType
    {
        DataTable,
        Text
    }

    public enum ReportSection
    {
        Header = 1,
        Footer = 3,
        Body = 2
    }
    public class ReportItemTemplateData : BindableBase
    {
        private int _ReportItemID;
        public int ReportItemID
        {
            get { return _ReportItemID; }
            set
            {
                _ReportItemID = value;
                OnPropertyChanged("ReportItemID");
            }
        }


        private ReportType _ReportItemType;
        public ReportType ReportItemType
        {
            get { return _ReportItemType; }
            set
            {
                _ReportItemType = value;
                OnPropertyChanged("ReportItemType");
            }
        }


    }
    public class ReportItemProperty : BindableBase
    {
        public ReportItemProperty()
        {
            FontSizeList.Add("4");
            FontSizeList.Add("5");
            FontSizeList.Add("6");
            FontSizeList.Add("7");
            FontSizeList.Add("8");
            FontSizeList.Add("9");
            FontSizeList.Add("10");
            FontSizeList.Add("11");
            FontSizeList.Add("12");
            FontSizeList.Add("13");
            FontSizeList.Add("14");
            FontSizeList.Add("15");
            FontSizeList.Add("16");

            FontStyleNameList.Add("Normal");
            FontStyleNameList.Add("Italic");
            FontStyleNameList.Add("Oblique");

            FontWeightNameList.Add("Black");
            FontWeightNameList.Add("Bold");
            FontWeightNameList.Add("DemiBold");
            FontWeightNameList.Add("ExtraBlack");
            FontWeightNameList.Add("ExtraBold");
            FontWeightNameList.Add("ExtraLight");
            FontWeightNameList.Add("Heavy");
            FontWeightNameList.Add("Light");
            FontWeightNameList.Add("Medium");
            FontWeightNameList.Add("Normal");
            FontWeightNameList.Add("Regular");
            FontWeightNameList.Add("SemiBold");
            FontWeightNameList.Add("Thin");
            FontWeightNameList.Add("UltraBlack");
            FontWeightNameList.Add("UltraBold");
            FontWeightNameList.Add("UltraLight");

            foreach (FontFamily fontFamily in Fonts.SystemFontFamilies.OrderBy(p => p.Source))
            {
                FontFamilyNameList.Add(fontFamily.Source);

            }
        }
        private string _FontFamilyName;
        public string FontFamilyName
        {
            get { return _FontFamilyName; }
            set
            {
                _FontFamilyName = value;
                OnPropertyChanged("FontFamilyName");
            }
        }

        private string _FontSizeName;
        public string FontSizeName
        {
            get { return _FontSizeName; }
            set
            {
                _FontSizeName = value;
                OnPropertyChanged("FontSizeName");
            }
        }

        private string _FontStyleName;
        public string FontStyleName
        {
            get { return _FontStyleName; }
            set
            {
                _FontStyleName = value;
                OnPropertyChanged("FontStyleName");
            }
        }

        private string _FontWeightName;
        public string FontWeightName
        {
            get { return _FontWeightName; }
            set
            {
                _FontWeightName = value;
                OnPropertyChanged("FontWeightName");
            }
        }

        private string _ItemContent;
        public string ItemContent
        {
            get { return _ItemContent; }
            set
            {
                _ItemContent = value;
                OnPropertyChanged("ItemContent");
            }
        }

        private Visibility _GridBlockDesigner = Visibility.Collapsed;
        public Visibility GridBlockDesigner
        {
            get { return _GridBlockDesigner; }
            set
            {
                _GridBlockDesigner = value;
                OnPropertyChanged("GridBlockDesigner");
            }
        }
        private Visibility _FontFamilyVisibility = Visibility.Collapsed;
        public Visibility FontFamilyVisibility
        {
            get { return _FontFamilyVisibility; }
            set
            {
                _FontFamilyVisibility = value;
                OnPropertyChanged("FontFamilyVisibility");
            }
        }
        private Visibility _FontWeightVisibility = Visibility.Collapsed;
        public Visibility FontWeightVisibility
        {
            get { return _FontWeightVisibility; }
            set
            {
                _FontWeightVisibility = value;
                OnPropertyChanged("FontWeightVisibility");
            }
        }
        private Visibility _FontStyleVisibility = Visibility.Collapsed;
        public Visibility FontStyleVisibility
        {
            get { return _FontStyleVisibility; }
            set
            {
                _FontStyleVisibility = value;
                OnPropertyChanged("FontStyleVisibility");
            }
        }

        private Visibility _FontSizeVisibility = Visibility.Collapsed;
        public Visibility FontSizeVisibility
        {
            get { return _FontSizeVisibility; }
            set
            {
                _FontSizeVisibility = value;
                OnPropertyChanged("FontSizeVisibility");
            }
        }

        private Visibility _TextBoxVisibility = Visibility.Collapsed;
        public Visibility TextBoxVisibility
        {
            get { return _TextBoxVisibility; }
            set
            {
                _TextBoxVisibility = value;
                OnPropertyChanged("TextBoxVisibility");
            }
        }


        private ImpObservableCollection<string> _FontSizeList = new ImpObservableCollection<string>();
        public ImpObservableCollection<string> FontSizeList
        {
            get { return _FontSizeList; }
            set
            {
                _FontSizeList = value;
                OnPropertyChanged("FontSizeList");
            }
        }
        private ImpObservableCollection<string> _FontWeightNameList = new ImpObservableCollection<string>();
        public ImpObservableCollection<string> FontWeightNameList
        {
            get { return _FontWeightNameList; }
            set
            {
                _FontWeightNameList = value;
                OnPropertyChanged("FontWeightNameList");
            }
        }

        private ImpObservableCollection<string> _FontStyleNameList = new ImpObservableCollection<string>();
        public ImpObservableCollection<string> FontStyleNameList
        {
            get { return _FontStyleNameList; }
            set
            {
                _FontStyleNameList = value;
                OnPropertyChanged("FontStyleNameList");
            }
        }

        private ImpObservableCollection<string> _FontFamilyNameList = new ImpObservableCollection<string>();
        public ImpObservableCollection<string> FontFamilyNameList
        {
            get { return _FontFamilyNameList; }
            set
            {
                _FontFamilyNameList = value;
                OnPropertyChanged("FontFamilyNameList");
            }
        }

    }

    public class CreateReportViewModel : BindableBase, ICreateReportViewModel, IGenericInteractionView<CreateReportViewModel>
    {
        public ICreateReportView _view;
        IUnityContainer unityContainer;
        IReportManager reportManager;
        public CreateReportViewModel(ICreateReportView view, IUnityContainer unity, IReportManager reportMng)
        {


            reportManager = reportMng;
            unityContainer = unity;
            GetAllDataSet();
            _CreateReportCommand = new DelegateCommand(CreateDataSetCommandHandler);
            _UpdateReportCommand = new DelegateCommand(UpdateDataSetCommandHandler);
            _DeleteReportCommand = new DelegateCommand(DeleteDataSetCommandHandler);
            view.DataContext = this;
            _view = view;

        }

        #region Methods
        public CreateReportViewModel GetEntity()
        {
            GetAllDataSet();
            return new CreateReportViewModel(_view, unityContainer, reportManager);
        }

        public void SetEntity(CreateReportViewModel entity)
        {
        }
        private void DeleteDataSetCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedReportEntity != null)
                {
                    ReportResponse<ReportDataSetResult> Response = await reportManager.DeleteReportTemplate(SelectedReportEntity.ReportID);
                    this.ReportList.Remove(SelectedReportEntity);
                }
            })).Wait();
        }

        private void UpdateDataSetCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedReportEntity == null) return;
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<ICreateReportTemplateView>() as Window;
                IGenericInteractionView<CreateReportEntity> createDesignDataSetViewModel = unityContainer.Resolve<ICreateReportTemplateViewModel>() as IGenericInteractionView<CreateReportEntity>;
                if (createDesignDataSetViewModel != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedReportEntity);
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                CreateReportEntity model = createDesignDataSetViewModel.GetEntity();
                if (model != null)
                {
                    CreateReportEntity existedDataSetSchema = this.ReportList.Where(p => p.ReportID == SelectedReportEntity.ReportID).FirstOrDefault();
                    if (existedDataSetSchema != null)
                    {
                        existedDataSetSchema.ReportID = SelectedReportEntity.ReportID;
                        existedDataSetSchema.ReportName = model.ReportName;
                        existedDataSetSchema.ReportDescription = model.ReportDescription;
                    }
                    Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        if (SelectedReportEntity != null)
                        {
                            ServiceRequest_ReportTemplate reportDataSet = new ServiceRequest_ReportTemplate();
                            reportDataSet.CreateReportEntity = existedDataSetSchema;
                            ReportResponse<ReportDataSetResult> Response = await reportManager.UpdateReportTemplate(reportDataSet);
                        }
                    })).Wait();

                }
                dialog.Close();
            })).Wait();

        }

        private void GetAllDataSet()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<CreateReportEntity> Response = await reportManager.GetAllReportTemplate();
                foreach (var item in Response)
                {
                    ReportList.Add(item);
                }
            })).Wait();
        }
        private void CreateDataSetCommandHandler()
        {

            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = unityContainer.Resolve<ICreateReportTemplateView>() as Window;
                IGenericInteractionView<CreateReportEntity> createDesignDataSetViewModel = unityContainer.Resolve<ICreateReportTemplateViewModel>() as IGenericInteractionView<CreateReportEntity>;
                if (createDesignDataSetViewModel != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedReportEntity);
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                dialog.Close();
                SelectedReportEntity = createDesignDataSetViewModel.GetEntity();
                if (SelectedReportEntity != null)
                {
                    this.ReportList.Add(SelectedReportEntity);
                }
                if (SelectedReportEntity != null)
                {
                    ServiceRequest_ReportTemplate reportDataSet = new ServiceRequest_ReportTemplate();
                    reportDataSet.CreateReportEntity = SelectedReportEntity;
                    ReportResponse<ReportDataSetResult> Response = await reportManager.CreateReportTemplate(reportDataSet);
                }
            })).Wait();

        }
        #endregion

        #region Properties
        private CreateReportEntity _SelectedReportEntity;
        public CreateReportEntity SelectedReportEntity
        {
            get { return _SelectedReportEntity; }
            set
            {
                _SelectedReportEntity = value;
                base.OnPropertyChanged("SelectedReportEntity");
            }
        }

        private ImpObservableCollection<CreateReportEntity> _ReportList = new ImpObservableCollection<CreateReportEntity>();
        public ImpObservableCollection<CreateReportEntity> ReportList
        {
            get { return _ReportList; }
            set
            {
                _ReportList = value;
                base.OnPropertyChanged("DataSetList");
            }
        }

        #endregion

        #region Commands
        private ICommand _CreateReportCommand;
        public ICommand CreateReportCommand
        {
            get
            {
                return _CreateReportCommand;
            }
        }

        private ICommand _UpdateReportCommand;
        public ICommand UpdateReportCommand
        {
            get
            {
                return _UpdateReportCommand;
            }
        }

        private ICommand _DeleteReportCommand;
        public ICommand DeleteReportCommand
        {
            get
            {
                return _DeleteReportCommand;
            }
        }
        #endregion
    }

}

