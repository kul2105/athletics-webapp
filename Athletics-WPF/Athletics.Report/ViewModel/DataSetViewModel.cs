﻿using Athletics.ControlLibrary;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.ModuleCommon;
using Athletics.Report.Common;
using Athletics.Report.Services.API;
using Athletics.Report.Views.GenerateDataSet;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Report.ViewModel
{
    public class DataSetSchema : BindableBase
    {
        private List<TableSchema> _TableList = new List<TableSchema>();
        public List<TableSchema> TableList
        {
            get { return _TableList; }
            set
            {
                _TableList = value;
                base.OnPropertyChanged("TableList");
            }
        }

        private string _DataSetName = string.Empty;
        public string DataSetName
        {
            get { return _DataSetName; }
            set
            {
                _DataSetName = value;
                base.OnPropertyChanged("DataSetName");
            }
        }

        private int _DataSetID = -1;
        public int DataSetID
        {
            get { return _DataSetID; }
            set
            {
                _DataSetID = value;
                base.OnPropertyChanged("DataSetID");
            }
        }
        private string _DataSetDescription = string.Empty;
        public string DataSetDescription
        {
            get { return _DataSetDescription; }
            set
            {
                _DataSetDescription = value;
                base.OnPropertyChanged("DataSetDescription");
            }
        }
        private string _DataSetQuery = string.Empty;
        public string DataSetQuery
        {
            get { return _DataSetQuery; }
            set
            {
                _DataSetQuery = value;
                base.OnPropertyChanged("DataSetQuery");
            }
        }

    }
    public class TableSchema : BindableBase
    {
        private int _TableID = -1;
        public int TableID
        {
            get { return _TableID; }
            set
            {
                _TableID = value;
                base.OnPropertyChanged("TableID");
            }
        }

        private string _TableName = string.Empty;
        public string TableName
        {
            get { return _TableName; }
            set
            {
                _TableName = value;
                base.OnPropertyChanged("TableName");
            }
        }

        private string _TableDescription = string.Empty;
        public string TableDescription
        {
            get { return _TableDescription; }
            set
            {
                _TableDescription = value;
                base.OnPropertyChanged("TableDescription");
            }
        }
        private ImpObservableCollection<ColumnSchema> _ColumnCollection = new ImpObservableCollection<ColumnSchema>();
        public ImpObservableCollection<ColumnSchema> ColumnCollection
        {
            get { return _ColumnCollection; }
            set
            {
                _ColumnCollection = value;
                base.OnPropertyChanged("ColumnCollection");
            }
        }


    }

    public class ColumnSchema : BindableBase
    {
        private int _ColumnID = -1;
        public int ColumnID
        {
            get { return _ColumnID; }
            set
            {
                _ColumnID = value;
                base.OnPropertyChanged("ColumnID");
            }
        }

        private string _ColumnName = string.Empty;
        public string ColumnName
        {
            get { return _ColumnName; }
            set
            {
                _ColumnName = value;
                base.OnPropertyChanged("ColumnName");
            }
        }

        private bool _IsSelected = false;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                base.OnPropertyChanged("IsSelected");
            }
        }

        private bool _IsForeignKey = false;
        public bool IsForeignKey
        {
            get { return _IsForeignKey; }
            set
            {
                _IsForeignKey = value;
                base.OnPropertyChanged("IsForeignKey");
            }
        }

        private bool _IsPrimaryKey = false;
        public bool IsPrimaryKey
        {
            get { return _IsPrimaryKey; }
            set
            {
                _IsPrimaryKey = value;
                base.OnPropertyChanged("IsPrimaryKey");
            }
        }

        private string _ReferencedTableName = string.Empty;
        public string ReferencedTableName
        {
            get { return _ReferencedTableName; }
            set
            {
                _ReferencedTableName = value;
                base.OnPropertyChanged("ReferencedTableName");
            }
        }

        private string _ReferencedColumneName = string.Empty;
        public string ReferencedColumneName
        {
            get { return _ReferencedColumneName; }
            set
            {
                _ReferencedColumneName = value;
                base.OnPropertyChanged("ReferencedColumneName");
            }
        }

        private string _ReferencingColumnName = string.Empty;
        public string ReferencingColumnName
        {
            get { return _ReferencingColumnName; }
            set
            {
                _ReferencingColumnName = value;
                base.OnPropertyChanged("ReferencingColumneName");
            }
        }
        private string _ReferencingTableName = string.Empty;
        public string ReferencingTableName
        {
            get { return _ReferencingTableName; }
            set
            {
                _ReferencingTableName = value;
                base.OnPropertyChanged("ReferencingTableName");
            }
        }
    }

    public class DataSetViewModel : BindableBase, IDataSetViewModel, IGenericInteractionView<DataSetViewModel>
    {
        IDataSetView _view;
        IUnityContainer unityContainer;
        IReportManager reportManager;
        public DataSetViewModel(IDataSetView view, IUnityContainer unity, IReportManager reportMng)
        {
            _view = view;
            unityContainer = unity;
            reportManager = reportMng;
            _view.DataContext = this;
            GetAllDataSet();
            _CreateDataSetCommand = new DelegateCommand(CreateDataSetCommandHandler);
            _UpdateDataSetCommand = new DelegateCommand(UpdateDataSetCommandHandler);
            _DeleteDataSetCommand = new DelegateCommand(DeleteDataSetCommandHandler);

        }

        #region Methods
        private void DeleteDataSetCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedDataSet != null)
                {
                    ReportResponse<ReportDataSetResult> Response = await reportManager.DeleteReportDataSet(SelectedDataSet.DataSetID);
                    this.DataSetList.Remove(SelectedDataSet);
                }
            })).Wait();
        }

        private void UpdateDataSetCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = new CreateDataSetView();
                IGenericInteractionView<DataSetSchema> createDesignDataSetViewModel = unityContainer.Resolve<ICreateDataSetViewModel>() as IGenericInteractionView<DataSetSchema>;
                if (createDesignDataSetViewModel != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedDataSet);
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                DataSetSchema model = createDesignDataSetViewModel.GetEntity();
                if (model != null)
                {
                    DataSetSchema existedDataSetSchema = this.DataSetList.Where(p => p.DataSetID == SelectedDataSet.DataSetID).FirstOrDefault();
                    if (existedDataSetSchema != null)
                    {
                        existedDataSetSchema.DataSetName = model.DataSetName;
                        existedDataSetSchema.DataSetQuery = model.DataSetQuery;
                        existedDataSetSchema.DataSetDescription = model.DataSetDescription;
                    }
                    Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        if (SelectedDataSet != null)
                        {
                            ServiceRequest_ReportDataSet reportDataSet = new ServiceRequest_ReportDataSet();
                            reportDataSet.dataSetSchema = existedDataSetSchema;
                            ReportResponse<ReportDataSetResult> Response = await reportManager.UpdateReportDataSet(reportDataSet);
                        }
                    })).Wait();

                }
                dialog.Close();
            })).Wait();

        }

        private void GetAllDataSet()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
{
    List<DataSetSchema> Response = await reportManager.GetAllTableSchema();
    foreach (var item in Response)
    {
        DataSetList.Add(item);
    }
})).Wait();
        }
        private void CreateDataSetCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {

                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    Window window = new CreateDataSetView();
                    IGenericInteractionView<DataSetSchema> createDesignDataSetViewModel = unityContainer.Resolve<ICreateDataSetViewModel>() as IGenericInteractionView<DataSetSchema>;
                    if (createDesignDataSetViewModel != null)
                    {
                        createDesignDataSetViewModel.SetEntity(this.SelectedDataSet);
                        dialog.BindView(window);
                        dialog.BindViewModel(createDesignDataSetViewModel);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    SelectedDataSet = createDesignDataSetViewModel.GetEntity();
                    if (SelectedDataSet != null)
                    {
                        this.DataSetList.Add(SelectedDataSet);
                    }
                    Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        if (SelectedDataSet != null)
                        {
                            ServiceRequest_ReportDataSet reportDataSet = new ServiceRequest_ReportDataSet();
                            reportDataSet.dataSetSchema = SelectedDataSet;
                            ReportResponse<ReportDataSetResult> Response = await reportManager.CreateReportDataSet(reportDataSet);
                        }
                    })).Wait();
                })).Wait();
            })).Wait();

        }
        private void propertyGrid_SelectedObjectsChanged(object sender, EventArgs e)
        {
        }
        public DataSetViewModel GetEntity()
        {
            GetAllDataSet();
            return new DataSetViewModel(_view, unityContainer, reportManager);
        }

        public void SetEntity(DataSetViewModel entity)
        {
        }

        #endregion

        #region Public Properties

        private DataSetSchema _SelectedDataSet;
        public DataSetSchema SelectedDataSet
        {
            get { return _SelectedDataSet; }
            set
            {
                _SelectedDataSet = value;
                base.OnPropertyChanged("SelectedDataSet");
            }
        }

        private ImpObservableCollection<DataSetSchema> _DataSetList = new ImpObservableCollection<DataSetSchema>();
        public ImpObservableCollection<DataSetSchema> DataSetList
        {
            get { return _DataSetList; }
            set
            {
                _DataSetList = value;
                base.OnPropertyChanged("DataSetList");
            }
        }
        #endregion

        private ICommand _CreateDataSetCommand;
        public ICommand CreateDataSetCommand
        {
            get
            {
                return _CreateDataSetCommand;
            }
        }

        private ICommand _UpdateDataSetCommand;
        public ICommand UpdateDataSetCommand
        {
            get
            {
                return _UpdateDataSetCommand;
            }
        }

        private ICommand _DeleteDataSetCommand;
        public ICommand DeleteDataSetCommand
        {
            get
            {
                return _DeleteDataSetCommand;
            }
        }


    }
}
