﻿using Athletics.ControlLibrary;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.ModuleCommon;
using Athletics.Report.Common;
using Athletics.Report.Services.API;
using Athletics.Report.Views.GenerateDataSet;
using Athletics.Report.Views.ReportGenerator;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Report.ViewModel
{
    public class ReportGeneratorListViewModel : BindableBase, IReportGeneratorListViewModel, IGenericInteractionView<ReportGeneratorListViewModel>
    {
        IReportGeneratorListView _view;
        IUnityContainer unityContainer;
        IReportManager reportManager;
        public ReportGeneratorListViewModel(IReportGeneratorListView view, IUnityContainer unity, IReportManager reportMng)
        {
            _view = view;
            unityContainer = unity;
            reportManager = reportMng;
            _view.DataContext = this;
            GetAllDataSet();
            _AssignReportCommand = new DelegateCommand(AssignReportHandler);
            _UpdateAssignReportCommand = new DelegateCommand(UpdateAssignReportHandler);
            _DeleteAssignReportCommand = new DelegateCommand(DeleteAssignReportHandler);

        }

        #region Methods
        private void DeleteAssignReportHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                if (SelectedAssignReportToDataSet != null)
                {
                    ReportResponse<AssignReportToDataSetResult> Response = await reportManager.DeleteAssignReportToDataSet(SelectedAssignReportToDataSet.ReportDataSetID);
                    this.AssignReportToDataSetList.Remove(SelectedAssignReportToDataSet);
                }
            })).Wait();
        }

        private void UpdateAssignReportHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                Window window = new Window();
                window.Content = new ReportGeneratorView();
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                window.WindowStyle = WindowStyle.None;
                window.Name = "AssignReport";
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                window.Style = Application.Current.FindResource("DialogStyle") as Style;
                window.ResizeMode = ResizeMode.NoResize;
                window.SizeToContent = SizeToContent.WidthAndHeight;
                window.MouseLeftButtonDown += Window_MouseLeftButtonDown;
                window.Title = "Assign DataSet to Report";
                IGenericInteractionView<ServiceRequest_AssignReportToDataSet> createDesignDataSetViewModel = unityContainer.Resolve<IReportGeneratorViewModel>() as IGenericInteractionView<ServiceRequest_AssignReportToDataSet>;
                if (createDesignDataSetViewModel != null)
                {
                    createDesignDataSetViewModel.SetEntity(this.SelectedAssignReportToDataSet);
                    dialog.BindView(window);
                    dialog.BindViewModel(createDesignDataSetViewModel);
                }
                dialog.ShowDialog();
                ServiceRequest_AssignReportToDataSet model = createDesignDataSetViewModel.GetEntity();
                if (model != null)
                {
                    ServiceRequest_AssignReportToDataSet existedDataSetSchema = this.AssignReportToDataSetList.Where(p => p.ReportDataSetID == SelectedAssignReportToDataSet.ReportDataSetID).FirstOrDefault();
                    if (existedDataSetSchema != null)
                    {
                        existedDataSetSchema.DataSetName = model.DataSetName;
                        existedDataSetSchema.DataSetID = model.DataSetID;
                        existedDataSetSchema.AssignedReportList = model.AssignedReportList;
                        existedDataSetSchema.ReportDataSetID = model.ReportDataSetID;
                        existedDataSetSchema.ReportID = model.ReportID;
                        existedDataSetSchema.ReportName = model.ReportName;
                        existedDataSetSchema.ReportTemplate = model.ReportTemplate;
                    }
                    Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        if (SelectedAssignReportToDataSet != null)
                        {
                            ReportResponse<AssignReportToDataSetResult> Response = await reportManager.UpdateAssignReportToDataSet(existedDataSetSchema);
                            if (Response.Status != eReport_REQUEST_RESPONSE.SUCCESS)
                            {
                                MessageBox.Show(Response.Message);
                            }
                        }
                    })).Wait();

                }
                dialog.Close();
            })).Wait();

        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender);
            if (window != null)
            {
                if (e.RightButton == MouseButtonState.Pressed)
                    window.DragMove();
            }
        }

        private void GetAllDataSet()
        {

            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<ServiceRequest_AssignReportToDataSet> Response = await reportManager.GetAssignReportToDataSet();
                foreach (var item in Response)
                {
                    AssignReportToDataSetList.Add(item);
                }
            })).Wait();
        }
        private void AssignReportHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {

                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    UserControl assignReport = unityContainer.Resolve<IReportGeneratorView>() as UserControl;
                    Window window = new Window();
                    window.Content = assignReport;
                    window.WindowStyle = WindowStyle.None;
                    window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    window.Style = Application.Current.FindResource("DialogStyle") as Style;
                    window.ResizeMode = ResizeMode.NoResize;
                    window.SizeToContent = SizeToContent.WidthAndHeight;
                    window.Title = "Assign Report To DataSet";
                    window.Name = "AssignReport";
                    window.MouseLeftButtonDown += Window_MouseLeftButtonDown;
                    IGenericInteractionView<List<ServiceRequest_AssignReportToDataSet>> assignReportVm = unityContainer.Resolve<IReportGeneratorViewModel>() as IGenericInteractionView<List<ServiceRequest_AssignReportToDataSet>>;
                    if (assignReportVm != null)
                    {
                        assignReportVm.GetEntity();
                        dialog.BindView(window);
                        dialog.BindViewModel(assignReportVm);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    List<ServiceRequest_AssignReportToDataSet> ListofTables = new List<ServiceRequest_AssignReportToDataSet>();
                    ListofTables = assignReportVm.GetEntity();
                    SelectedAssignReportToDataSet = ListofTables.FirstOrDefault();
                    if (SelectedAssignReportToDataSet != null)
                    {
                        this.AssignReportToDataSetList.Add(SelectedAssignReportToDataSet);
                    }
                    foreach (var item in ListofTables)
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            if (SelectedAssignReportToDataSet != null)
                            {
                                ReportResponse<AssignReportToDataSetResult> Response = await reportManager.AssignReportToDataSet(item);
                                if (Response.Status != eReport_REQUEST_RESPONSE.SUCCESS)
                                {
                                    MessageBox.Show(Response.Message);
                                }
                            }
                        })).Wait();
                    }
                   
                })).Wait();
            })).Wait();

        }
        private void propertyGrid_SelectedObjectsChanged(object sender, EventArgs e)
        {
        }
        public ReportGeneratorListViewModel GetEntity()
        {
            GetAllDataSet();
            return new ReportGeneratorListViewModel(_view, unityContainer, reportManager);
        }

        public void SetEntity(ReportGeneratorListViewModel entity)
        {
        }

        #endregion

        #region Public Properties

        private ServiceRequest_AssignReportToDataSet _SelectedAssignReportToDataSet;
        public ServiceRequest_AssignReportToDataSet SelectedAssignReportToDataSet
        {
            get { return _SelectedAssignReportToDataSet; }
            set
            {
                _SelectedAssignReportToDataSet = value;
                base.OnPropertyChanged("SelectedAssignReportToDataSet");
            }
        }

        private ImpObservableCollection<ServiceRequest_AssignReportToDataSet> _AssignReportToDataSetList = new ImpObservableCollection<ServiceRequest_AssignReportToDataSet>();
        public ImpObservableCollection<ServiceRequest_AssignReportToDataSet> AssignReportToDataSetList
        {
            get { return _AssignReportToDataSetList; }
            set
            {
                _AssignReportToDataSetList = value;
                base.OnPropertyChanged("AssignReportToDataSetList");
            }
        }
        #endregion

        private ICommand _AssignReportCommand;
        public ICommand AssignReportCommand
        {
            get
            {
                return _AssignReportCommand;
            }
        }

        private ICommand _UpdateAssignReportCommand;
        public ICommand UpdateAssignReportCommand
        {
            get
            {
                return _UpdateAssignReportCommand;
            }
        }

        private ICommand _DeleteAssignReportCommand;
        public ICommand DeleteAssignReportCommand
        {
            get
            {
                return _DeleteAssignReportCommand;
            }
        }


    }
}
