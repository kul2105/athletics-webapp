﻿using Athletics.ModuleCommon;
using Athletics.Report.Common;
using Athletics.Report.Views.GenerateDataSet;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Input;
using System.Linq;
using System.Windows;
using Athletics.Report.Services.API;
using Athletics.ControlLibrary;

namespace Athletics.Report.ViewModel
{

    public class CreateDataSetViewModel : BindableBase, ICreateDataSetViewModel, IGenericInteractionView<DataSetSchema>
    {
        ICreateDataSetView _view;
        internal DataSetSchema datsetSchema;
        string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AthleticsConnection"].ConnectionString;
        IReportManager reportManager;
        public CreateDataSetViewModel(ICreateDataSetView view, IReportManager reportMng)
        {
            _view = view;
            _view.DataContext = this;
            reportManager = reportMng;
            GetAllTableList();
            _GenerateQueryCommand = new DelegateCommand(GenerateCommandHandler);

        }

        #region Methods
        private void GenerateCommandHandler()
        {
            string columnNames = string.Empty;
            string fromtable = string.Empty;
            string joinTable = string.Empty;
            DataTable dt = new DataTable();
            List<string> columnList = new List<string>();
            List<string> TableListAllSelected = this.TableCollection.Select(p => p.TableName).ToList();

            foreach (var item in this.TableCollection)
            {
                foreach (var columnSchema in item.ColumnCollection.Where(p => p.IsSelected == true))
                {
                    columnList.Add(string.Format("[{0}].[{1}]", item.TableName, columnSchema.ColumnName));
                }

            }
            if (columnList.Count <= 0)
            {
                MessageBox.Show("No Column Selected");
                return;
            }
            List<ReportTableResult> reportTableResultList = this.TableList.Where(p => TableListAllSelected.Contains(p.ReportTableName)).ToList();
            if (reportTableResultList != null)
            {
                ReportTableResult reportTableResult = reportTableResultList.Where(p => p.ReportTableRootTableName == null).FirstOrDefault();
                if (reportTableResult == null)
                {
                    reportTableResult = reportTableResultList.FirstOrDefault();
                }
                fromtable = reportTableResult.ReportTableName;
            }
            columnNames = string.Join(",", columnList);
            joinTable = this.GetTableJoin(reportTableResultList);
            QueryText = string.Format("Select {0} from {1} {2}", columnNames, fromtable, joinTable);

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlDataAdapter sda = new SqlDataAdapter(QueryText, con);
                try
                {
                    con.Open();
                    sda.Fill(dt);
                }
                catch (SqlException se)
                {
                }
                finally
                {
                    con.Close();
                }
            }
            QueryResult = dt.DefaultView;

        }

        private string GetTableJoin(List<ReportTableResult> ReportList)
        {
            List<ReportTableResult> RelationalTable = ReportList.Where(p => p.ReportTableRootRelationalTableName != null).ToList();
            List<ReportTableResult> OtherTableList = ReportList.Where(p => p.ReportTableRootTableName != null && (RelationalTable.Contains(p) == false)).ToList();
            string joinQuery = String.Join(" ", RelationalTable.Select(p => p.ReportTableJoinStatement));
            joinQuery = joinQuery + String.Join(" ", OtherTableList.Select(p => p.ReportTableJoinStatement));
            return joinQuery;
        }

        public void GetTableSchema(string tablename)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("GetTableSchema", con))
                {
                    TableSchema tableSchema = new TableSchema();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = tablename;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    tableSchema.ColumnCollection = new ImpObservableCollection<ColumnSchema>();
                    tableSchema.TableName = tablename;
                    while (rdr.Read())
                    {
                        ColumnSchema columnSchema = new ColumnSchema();
                        columnSchema.ColumnName = rdr["COLUMN_NAME"].ToString();
                        columnSchema.IsPrimaryKey = bool.Parse(rdr["IsPrimaryKey"].ToString());
                        columnSchema.IsForeignKey = bool.Parse(rdr["IsForeignKey"].ToString());
                        if (columnSchema.IsForeignKey)
                        {
                            using (SqlCommand cmdForeignkeyDetail = new SqlCommand("GetForeignKeyDetail", con))
                            {
                                cmdForeignkeyDetail.CommandType = CommandType.StoredProcedure;
                                cmdForeignkeyDetail.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = tablename;
                                cmdForeignkeyDetail.Parameters.Add("@ColumnName ", SqlDbType.NVarChar).Value = columnSchema.ColumnName;
                                // con.Open();
                                SqlDataReader rdr1 = cmdForeignkeyDetail.ExecuteReader();
                                while (rdr1.Read())
                                {
                                    columnSchema.ReferencedTableName = rdr1["Referenced_Table_Name"].ToString();
                                    columnSchema.ReferencedColumneName = rdr1["Referenced_Column_As_FK"].ToString();
                                    columnSchema.ReferencingTableName = rdr1["Referencing_Table_Name"].ToString();
                                    columnSchema.ReferencingColumnName = rdr1["Referencing_Column_Name"].ToString();
                                }
                            }
                        }
                        tableSchema.ColumnCollection.Add(columnSchema);

                    }
                    TableCollection.Add(tableSchema);
                    //base.OnPropertyChanged("TableCollection");
                }
            }
        }
        private void GetAllTableList()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                TableList = await reportManager.GetAllReportTable();

            })).Wait();
            //string CommandText = "SELECT TABLE_NAME FROM AthleticsDB.INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' order by TABLE_NAME";
            //SqlConnection con = new SqlConnection(ConnectionString);
            //con.Open();
            //SqlCommand cmd = new SqlCommand(CommandText);
            //cmd.Connection = con;
            //SqlDataReader rdr = cmd.ExecuteReader();
            //// Fill the list box with the values retrieved
            //while (rdr.Read())
            //{
            //    TableList.Add(rdr["TABLE_NAME"].ToString());
            //}
        }
        private void propertyGrid_SelectedObjectsChanged(object sender, EventArgs e)
        {
        }
        public DataSetSchema GetEntity()
        {
            GetAllTableList();
            foreach (var item in TableCollection)
            {
                TableSchema tableSchemaExist = TableCollection.Where(p => p.TableName == item.TableName).FirstOrDefault();
                if (tableSchemaExist != null)
                {
                    datsetSchema.TableList.Add(item);
                }
                else
                {
                    tableSchemaExist.TableName = item.TableName;
                    tableSchemaExist.TableDescription = item.TableDescription;
                    tableSchemaExist.ColumnCollection.Clear();
                    foreach (var column in item.ColumnCollection.Where(p => p.IsSelected == true))
                    {
                        tableSchemaExist.ColumnCollection.Add(column);
                    }

                }

            }

            return datsetSchema;
        }

        public void SetEntity(DataSetSchema entity)
        {
            datsetSchema = entity;
            if (entity == null)
            {
                datsetSchema = new DataSetSchema();
            }
            this.DataSetName = datsetSchema.DataSetName;
            this.DataSetDescription = datsetSchema.DataSetDescription;
            this.QueryText = datsetSchema.DataSetQuery;
            if (datsetSchema.TableList == null)
            {
                datsetSchema.TableList = new List<TableSchema>();
            }
            foreach (var item in datsetSchema.TableList)
            {
                ReportTableResult reportTableResult = TableList.Where(p => p.ReportTableName == item.TableName).FirstOrDefault();
                this.SelectedTable = reportTableResult;
                foreach (var table in TableCollection.Where(p => p.TableName == item.TableName))
                {
                    foreach (var column in item.ColumnCollection)
                    {
                        ColumnSchema existedColumn = table.ColumnCollection.Where(p => p.ColumnName == column.ColumnName).FirstOrDefault();
                        if (existedColumn != null)
                        {
                            existedColumn.IsSelected = true;
                        }
                    }
                }
            }
        }

        private void AddTableToSurface(string value)
        {
            GetTableSchema(value);
        }

        #endregion

        #region Public Properties
        private ImpObservableCollection<ReportTableResult> _TableList = new ImpObservableCollection<ReportTableResult>();
        public ImpObservableCollection<ReportTableResult> TableList
        {
            get { return _TableList; }
            set
            {
                _TableList = value;
                base.OnPropertyChanged("TableList");
            }
        }
        private string _DataSetName = string.Empty;
        public string DataSetName
        {
            get { return _DataSetName; }
            set
            {
                _DataSetName = value;
                base.OnPropertyChanged("DataSetName");
                datsetSchema.DataSetName = value;
            }
        }

        private string _DataSetDescription = string.Empty;
        public string DataSetDescription
        {
            get { return _DataSetDescription; }
            set
            {
                _DataSetDescription = value;
                base.OnPropertyChanged("DataSetDescription");
                datsetSchema.DataSetDescription = value;
            }
        }

        private ReportTableResult _SelectedTable;
        public ReportTableResult SelectedTable
        {
            get { return _SelectedTable; }
            set
            {
                _SelectedTable = value;
                if (_SelectedTable == null) return;
                base.OnPropertyChanged("SelectedTable");
                AddTableToSurface(value.ReportTableName);
                TableSchema tableSchema = TableCollection.Where(p => p.TableName == value.ReportTableRootTableName).FirstOrDefault();
                if (tableSchema != null)
                {
                    if (!string.IsNullOrEmpty(value.ReportTableRootRelationalTableName))
                    {
                        AddTableToSurface(value.ReportTableRootRelationalTableName);
                    }
                }


            }
        }

        private string _QueryText = string.Empty;
        public string QueryText
        {
            get { return _QueryText; }
            set
            {
                _QueryText = value;
                base.OnPropertyChanged("QueryText");
                datsetSchema.DataSetQuery = value;
            }
        }

        private ImpObservableCollection<TableSchema> _TableCollection = new ImpObservableCollection<TableSchema>();
        public ImpObservableCollection<TableSchema> TableCollection
        {
            get { return _TableCollection; }
            set
            {
                _TableCollection = value;
                base.OnPropertyChanged("TableCollection");
            }
        }

        private DataView _QueryResult;
        public DataView QueryResult
        {
            get { return _QueryResult; }
            set
            {
                _QueryResult = value;
                base.OnPropertyChanged("QueryResult");
            }
        }
        #endregion

        private ICommand _GenerateQueryCommand;
        public ICommand GenerateQueryCommand
        {
            get
            {
                return _GenerateQueryCommand;
            }
        }

    }
}
