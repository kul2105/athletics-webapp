﻿using Athletics.ControlLibrary;
using Athletics.ModuleCommon;
using Athletics.Report.Common;
using Athletics.Report.CustomTypeEditors;
using Athletics.Report.Views.ReportGenerator;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Athletics.Report.ViewModel
{
    public class CreateReportEntity
    {
        private int _ReportID;
        public int ReportID
        {
            get { return _ReportID; }
            set
            {
                _ReportID = value;
            }
        }

        private string _ReportName;
        public string ReportName
        {
            get { return _ReportName; }
            set
            {
                _ReportName = value;
            }
        }

        private string _ReportDescription;
        public string ReportDescription
        {
            get { return _ReportDescription; }
            set
            {
                _ReportDescription = value;
            }
        }
        private ReportTemplateData _ReportTemplateData = new ReportTemplateData();
        public ReportTemplateData ReportTemplateData
        {
            get { return _ReportTemplateData; }
            set
            {
                _ReportTemplateData = value;
            }
        }

        private ReportTemplateProperty _ReportTemplateProperty = new ReportTemplateProperty();
        public ReportTemplateProperty ReportTemplateProperty
        {
            get { return _ReportTemplateProperty; }
            set
            {
                _ReportTemplateProperty = value;
            }
        }
    }
    public class CreateReportTemplateViewModel : BindableBase, ICreateReportTemplateViewModel, IGenericInteractionView<CreateReportEntity>
    {
        private TextObject bo;
        public ICreateReportTemplateView _view;
        public CreateReportTemplateViewModel(ICreateReportTemplateView view)
        {

            ObservableCollection<Image> _collection = new ObservableCollection<Image> {
                new Image{ Height=30, Width=30,Source=new BitmapImage(new Uri("/Athletics.Report;component/Resources/images/database_table-512.png", UriKind.Relative))} ,
                new Image{Height=30, Width=30,Source=new BitmapImage(new Uri("/Athletics.Report;component/Resources/images/images.jpg", UriKind.Relative))},
             };
            ImageCollection = _collection;
            _view = view;
            _view.DataContext = this;
            _AddColumnCommand = new DelegateCommand(AddColumnHandler);
        }



        private string _ReportName;
        public string ReportName
        {
            get { return _ReportName; }
            set
            {
                _ReportName = value;
                OnPropertyChanged("ReportName");
            }
        }

        private string _ReportDescription;
        public string ReportDescription
        {
            get { return _ReportDescription; }
            set
            {
                _ReportDescription = value;
                OnPropertyChanged("ReportDescription");
            }
        }

        private ReportTemplateData _ReportTemplateData = new ReportTemplateData();
        public ReportTemplateData ReportTemplateData
        {
            get { return _ReportTemplateData; }
            set
            {
                _ReportTemplateData = value;
                OnPropertyChanged("ReportTemplateData");
            }
        }

        private ReportTemplateProperty _ReportTemplateProperty = new ReportTemplateProperty();
        public ReportTemplateProperty ReportTemplateProperty
        {
            get { return _ReportTemplateProperty; }
            set
            {
                _ReportTemplateProperty = value;
                OnPropertyChanged("ReportTemplateProperty");
            }
        }
        private FrameworkElement _SelectedHeader;
        public FrameworkElement SelectedHeader
        {
            get { return _SelectedHeader; }
            set
            {
                _SelectedHeader = value;
                PropertyWindowVisibility = Visibility.Visible;
                OnPropertyChanged("SelectedHeader");
                if (value == null) return;
                _SelectedHeader = value;
                SelectedFooter = null;
                SelectedBody = null;
                int bodyIndex = ReportTemplateProperty.Header.Count;
                int index = _view.HeaderList.Items.IndexOf(value);
                if (index >= 0 && bodyIndex > 0)
                {
                    if (ReportTemplateProperty.Header.Count <= index)
                    {
                        StackPanel stackPanel = _SelectedHeader as StackPanel;
                        Grid grid = _SelectedHeader as Grid;
                        if (stackPanel == null)
                        {
                            this.AddItemToList(grid, ReportTemplateData.Header, index);
                            this.AddPropertyToList(grid, ReportTemplateProperty.Header, index);
                        }
                        else
                        {
                            this.AddItemToList(stackPanel, ReportTemplateData.Header, index);
                            this.AddPropertyToList(stackPanel, ReportTemplateProperty.Header, index);
                        }
                    }
                    CurrentItemProperty = ReportTemplateProperty.Header[index];

                }
                else
                {
                    if (bodyIndex == 0)
                    {
                        if (ReportTemplateProperty.Header.Count <= index)
                        {
                            StackPanel stackPanel = _SelectedHeader as StackPanel;
                            Grid grid = _SelectedHeader as Grid;
                            if (stackPanel == null)
                            {
                                this.AddItemToList(grid, ReportTemplateData.Header, index);
                                this.AddPropertyToList(grid, ReportTemplateProperty.Header, index);
                            }
                            else
                            {
                                this.AddItemToList(stackPanel, ReportTemplateData.Header, index);
                                this.AddPropertyToList(stackPanel, ReportTemplateProperty.Header, index);
                            }
                        }
                        CurrentItemProperty = ReportTemplateProperty.Header[0];
                    }
                }
            }
        }

        private FrameworkElement _SelectedFooter;
        public FrameworkElement SelectedFooter
        {
            get { return _SelectedFooter; }
            set
            {
                _SelectedFooter = value;
                OnPropertyChanged("SelectedFooter");
                PropertyWindowVisibility = Visibility.Visible;
                if (value == null) return;
                _SelectedFooter = value;
                SelectedHeader = null;
                SelectedBody = null;
                int index = _view.FooterList.Items.IndexOf(value);
                int bodyIndex = ReportTemplateProperty.Footer.Count;
                if (index >= 0 && bodyIndex > 0)
                {
                    if (ReportTemplateProperty.Footer.Count <= index)
                    {
                        StackPanel stackPanel = _SelectedFooter as StackPanel;
                        Grid grid = _SelectedFooter as Grid;
                        if (stackPanel == null)
                        {
                            this.AddItemToList(grid, ReportTemplateData.Footer, index);
                            this.AddPropertyToList(grid, ReportTemplateProperty.Footer, index);
                        }
                        else
                        {
                            this.AddItemToList(stackPanel, ReportTemplateData.Footer, index);
                            this.AddPropertyToList(stackPanel, ReportTemplateProperty.Footer, index);
                        }
                    }
                    CurrentItemProperty = ReportTemplateProperty.Footer[index];

                }
                else
                {
                    if (bodyIndex == 0)
                    {
                        if (ReportTemplateProperty.Footer.Count <= index)
                        {
                            StackPanel stackPanel = _SelectedFooter as StackPanel;
                            Grid grid = _SelectedFooter as Grid;
                            if (stackPanel == null)
                            {
                                this.AddItemToList(grid, ReportTemplateData.Footer, index);
                                this.AddPropertyToList(grid, ReportTemplateProperty.Footer, index);
                            }
                            else
                            {
                                this.AddItemToList(stackPanel, ReportTemplateData.Footer, index);
                                this.AddPropertyToList(stackPanel, ReportTemplateProperty.Footer, index);
                            }
                        }
                        CurrentItemProperty = ReportTemplateProperty.Footer[0];
                    }
                }
            }
        }

        private FrameworkElement _SelectedBody;
        public FrameworkElement SelectedBody
        {
            get { return _SelectedBody; }
            set
            {

                _SelectedBody = value;
                OnPropertyChanged("SelectedBody");
                if (value == null) return;
                SelectedHeader = null;
                SelectedFooter = null;

                PropertyWindowVisibility = Visibility.Visible;
                int index = _view.BodyList.Items.IndexOf(value);
                int bodyIndex = ReportTemplateProperty.Body.Count;
                if (index >= 0 && bodyIndex > 0)
                {
                    if (ReportTemplateProperty.Body.Count <= index)
                    {
                        StackPanel stackPanel = _SelectedBody as StackPanel;
                        Grid grid = _SelectedBody as Grid;
                        if (stackPanel == null)
                        {
                            this.AddItemToList(grid, ReportTemplateData.Body, index);
                            this.AddPropertyToList(grid, ReportTemplateProperty.Body, index);
                        }
                        else
                        {
                            this.AddItemToList(stackPanel, ReportTemplateData.Body, index);
                            this.AddPropertyToList(stackPanel, ReportTemplateProperty.Body, index);
                        }
                    }
                    CurrentItemProperty = ReportTemplateProperty.Body[index];

                }
                else
                {
                    if (bodyIndex == 0)
                    {
                        if (ReportTemplateProperty.Body.Count <= index)
                        {
                            StackPanel stackPanel = _SelectedBody as StackPanel;
                            Grid grid = _SelectedBody as Grid;
                            if (stackPanel == null)
                            {
                                this.AddItemToList(grid, ReportTemplateData.Body, index);
                                this.AddPropertyToList(grid, ReportTemplateProperty.Body, index);
                            }
                            else
                            {
                                this.AddItemToList(stackPanel, ReportTemplateData.Body, index);
                                this.AddPropertyToList(stackPanel, ReportTemplateProperty.Body, index);
                            }
                        }
                        CurrentItemProperty = ReportTemplateProperty.Body[0];
                    }
                }
            }
        }

        private ReportItemProperty _CurrentItemProperty;
        public ReportItemProperty CurrentItemProperty
        {
            get { return _CurrentItemProperty; }
            set
            {
                _CurrentItemProperty = value;
                OnPropertyChanged("CurrentItemProperty");
            }
        }

        public ObservableCollection<Image> collection;
        public ObservableCollection<Image> ImageCollection
        {
            get { return collection; }
            set
            {
                collection = value;
                OnPropertyChanged("ImageCollection");
            }
        }

        private Visibility _PropertyWindowVisibility = Visibility.Collapsed;
        public Visibility PropertyWindowVisibility
        {
            get { return _PropertyWindowVisibility; }
            set
            {
                _PropertyWindowVisibility = value;
                OnPropertyChanged("PropertyWindowVisibility");
            }
        }
        private readonly ICommand _AddColumnCommand;
        public ICommand AddColumnCommand
        {
            get
            {
                return _AddColumnCommand;
            }
        }
        internal bool isInEditMode = false;


        #region Methods
        public void SetEntity(CreateReportEntity entity)
        {
            if (entity == null) return;
            if (entity.ReportTemplateData == null) return;
            int index = 0;
            if (entity.ReportTemplateData.Header != null)
            {
                _view.HeaderList.Items.Clear();
                foreach (var item in entity.ReportTemplateData.Header)
                {
                    Grid stackPanel = null;
                    AddChildToListView(item, _view.HeaderList, out stackPanel);
                    AddPropertyToList(stackPanel, entity.ReportTemplateProperty.Header, index);
                    index++;
                }
            }
            index = 0;
            if (entity.ReportTemplateData.Body != null)
            {
                _view.BodyList.Items.Clear();
                foreach (var item in entity.ReportTemplateData.Body)
                {
                    Grid stackPanel = null;
                    AddChildToListView(item, _view.BodyList, out stackPanel);
                    AddPropertyToList(stackPanel, entity.ReportTemplateProperty.Body, index);
                    index++;
                }
            }
            index = 0;
            if (entity.ReportTemplateData.Footer != null)
            {
                _view.FooterList.Items.Clear();
                foreach (var item in entity.ReportTemplateData.Footer)
                {
                    Grid stackPanel = null;
                    AddChildToListView(item, _view.FooterList, out stackPanel);
                    AddPropertyToList(stackPanel, entity.ReportTemplateProperty.Footer, index);
                    index++;

                }

            }
            isInEditMode = true;
            this.ReportTemplateData = entity.ReportTemplateData;
            this.ReportTemplateProperty = entity.ReportTemplateProperty;
            this.ReportName = entity.ReportName;
            this.ReportDescription = entity.ReportDescription;
        }


        private void AddChildToListView(ReportItemTemplateData item, ListBox listBox, out Grid stack)
        {
            switch (item.ReportItemType)
            {
                case ReportType.Text:
                    Grid stackPanel = new Grid();
                    TextBlock textBlock = new TextBlock();
                    // RichTextBoxExtension.InsertText("New text here", textBlock);
                    textBlock.Text = "New text here";
                    List<double> d = new List<double>();
                    d.Add(.5);
                    d.Add(1.5);
                    textBlock.Tag = "Text";
                    Rectangle rec = new Rectangle();
                    rec.StrokeDashArray = new DoubleCollection(d);
                    rec.StrokeDashCap = PenLineCap.Round;
                    rec.StrokeThickness = 3;
                    rec.Stroke = new SolidColorBrush(Colors.Red);
                    rec.Margin = new Thickness(-5);
                    stackPanel.Children.Add(textBlock);
                    stackPanel.Children.Add(rec);
                    listBox.Items.Add(stackPanel);
                    stack = stackPanel;
                    break;

                case ReportType.DataTable:
                    Grid grid = new Grid();
                    grid.ShowGridLines = true;
                    grid.ColumnDefinitions.Add(new ColumnDefinition());
                    TextBlock textBlockgrid = new TextBlock();
                    textBlockgrid.Tag = "DataTable";
                    textBlockgrid.Text = "Column1";
                    textBlockgrid.FontSize = 20;
                    textBlockgrid.FontWeight = FontWeights.Bold;
                    grid.Children.Add(textBlockgrid);
                    List<double> d1 = new List<double>();
                    d1.Add(.5);
                    d1.Add(1.5);
                    Rectangle rec1 = new Rectangle();
                    rec1.StrokeDashArray = new DoubleCollection(d1);
                    rec1.StrokeDashCap = PenLineCap.Round;
                    rec1.StrokeThickness = 3;
                    rec1.Stroke = new SolidColorBrush(Colors.Red);
                    rec1.Margin = new Thickness(-10);
                    grid.Children.Add(rec1);
                    listBox.Items.Add(grid);
                    stack = grid;
                    break;
                default:
                    stack = new Grid();
                    break;
            }
        }

        public CreateReportEntity GetEntity()
        {
            CreateReportEntity createentity = new CreateReportEntity();
            createentity.ReportTemplateData = this.ReportTemplateData;
            createentity.ReportTemplateProperty = this.ReportTemplateProperty;
            createentity.ReportName = this.ReportName;
            createentity.ReportDescription = this.ReportDescription;
            return createentity;
        }

        private void AddColumnHandler()
        {
            AddColumnView addColumnView = new AddColumnView(CurrentItemProperty.ItemContent);
            addColumnView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            addColumnView.ShowDialog();
            CurrentItemProperty.ItemContent = addColumnView.ColumnListText;
        }


        private void AddPropertyToList(StackPanel stackPanel, ImpObservableCollection<ReportItemProperty> header, int seletedIndex)
        {
            if (stackPanel != null)
            {
                Grid grid = stackPanel.Children[0] as Grid;
                AddPropertyToList(grid, header, seletedIndex);
            }
        }

        private void AddPropertyToList(Grid grid, ImpObservableCollection<ReportItemProperty> header, int seletedIndex)
        {
            //if (isInEditMode == true) return;
            if (grid != null)
            {
                if (grid.Children[0].GetType() == typeof(TextBlock))
                {
                    ReportItemProperty reportData = new ReportItemProperty();
                    TextBlock textBlock = grid.Children[0] as TextBlock;
                    if (textBlock.Tag.ToString() == "Text")
                    {
                        reportData.FontFamilyVisibility = Visibility.Visible;
                        reportData.GridBlockDesigner = Visibility.Collapsed;
                        reportData.FontWeightVisibility = Visibility.Visible;
                        reportData.FontSizeVisibility = Visibility.Visible;
                        reportData.FontStyleVisibility = Visibility.Visible;
                        reportData.TextBoxVisibility = Visibility.Visible;
                    }
                    else if (textBlock.Tag.ToString() == "DataTable")
                    {
                        reportData.FontFamilyVisibility = Visibility.Visible;
                        reportData.GridBlockDesigner = Visibility.Visible;
                        reportData.FontWeightVisibility = Visibility.Visible;
                        reportData.FontSizeVisibility = Visibility.Visible;
                        reportData.FontStyleVisibility = Visibility.Visible;
                        reportData.TextBoxVisibility = Visibility.Collapsed;
                    }
                    else
                    {
                        reportData.FontFamilyVisibility = Visibility.Collapsed;
                        reportData.GridBlockDesigner = Visibility.Collapsed;
                        reportData.FontWeightVisibility = Visibility.Collapsed;
                        reportData.FontSizeVisibility = Visibility.Collapsed;
                        reportData.FontStyleVisibility = Visibility.Collapsed;
                    }
                    if (header.Count <= seletedIndex)
                    {
                        header.Add(reportData);
                    }
                    else
                    {
                        header[seletedIndex].FontFamilyVisibility = reportData.FontFamilyVisibility;
                        header[seletedIndex].GridBlockDesigner = reportData.GridBlockDesigner;
                        header[seletedIndex].FontWeightVisibility = reportData.FontWeightVisibility;
                        header[seletedIndex].FontSizeVisibility = reportData.FontSizeVisibility;
                        header[seletedIndex].FontStyleVisibility = reportData.FontStyleVisibility;
                        header[seletedIndex].TextBoxVisibility = reportData.TextBoxVisibility;
                    }
                }
            }
        }

        private void AddItemToList(StackPanel stackPanel, ImpObservableCollection<ReportItemTemplateData> header, int selectedIndex)
        {
            if (stackPanel != null)
            {
                Grid grid = stackPanel.Children[0] as Grid;
                AddItemToList(grid, header, selectedIndex);

            }
        }

        private void AddItemToList(Grid grid, ImpObservableCollection<ReportItemTemplateData> header, int selectedIndex)
        {
            //if (isInEditMode == true) return;
            if (grid != null)
            {
                if (grid.Children[0].GetType() == typeof(TextBlock))
                {
                    ReportItemTemplateData reportData = new ReportItemTemplateData();
                    TextBlock textBlock = grid.Children[0] as TextBlock;
                    if (textBlock.Tag.ToString() == "Text")
                    {
                        reportData.ReportItemType = ReportType.Text;
                    }
                    else if (textBlock.Tag.ToString() == "DataTable")
                    {
                        reportData.ReportItemType = ReportType.DataTable;
                    }
                    else
                    {
                    }
                    if (header.Count <= selectedIndex)
                    {
                        header.Add(reportData);
                    }
                    else
                    {
                        header[selectedIndex].ReportItemType = reportData.ReportItemType;

                    }
                }
            }
        }
        #endregion

    }
}
