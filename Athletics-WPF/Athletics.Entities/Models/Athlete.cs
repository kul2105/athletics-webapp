//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Athlete
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Athlete()
        {
            this.RelayFinalOrders = new HashSet<RelayFinalOrder>();
            this.RelayPrelimOrders = new HashSet<RelayPrelimOrder>();
            this.AthletiesElegibles = new HashSet<AthletiesElegible>();
            this.AtheleteContacts = new HashSet<AtheleteContact>();
        }
    
        public int AthletesID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public string Gender { get; set; }
        public Nullable<int> Age { get; set; }
        public string ClassYR { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public string RegistrationNumber { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public Nullable<int> TeamID { get; set; }
        public string CompetitorNumber { get; set; }
        public string Status { get; set; }
        public Nullable<int> CitizenID { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    
        public virtual Team Team { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RelayFinalOrder> RelayFinalOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RelayPrelimOrder> RelayPrelimOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AthletiesElegible> AthletiesElegibles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AtheleteContact> AtheleteContacts { get; set; }
    }
}
