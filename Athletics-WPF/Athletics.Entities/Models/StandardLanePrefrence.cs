//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StandardLanePrefrence
    {
        public int StandardLanePreferencesID { get; set; }
        public string PreferencesName { get; set; }
        public Nullable<int> FirstLane { get; set; }
        public Nullable<int> SecondLine { get; set; }
        public Nullable<int> ThirdLine { get; set; }
        public Nullable<int> ForthLine { get; set; }
        public Nullable<int> FifthLine { get; set; }
        public Nullable<int> SixthLine { get; set; }
        public Nullable<int> SeventhLine { get; set; }
        public Nullable<int> EightsLine { get; set; }
        public Nullable<int> NinthLine { get; set; }
        public Nullable<int> TenthLine { get; set; }
    }
}
