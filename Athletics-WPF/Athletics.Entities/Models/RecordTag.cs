//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RecordTag
    {
        public int RecordTagID { get; set; }
        public Nullable<int> TagOrder { get; set; }
        public string TagName { get; set; }
        public string TagFlag { get; set; }
        public string RecordOnlyFor { get; set; }
        public Nullable<bool> ExhOk { get; set; }
        public Nullable<bool> ForeignOk { get; set; }
    }
}
