//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompetitorNumber
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompetitorNumber()
        {
            this.CompetitorNumbersTransactions = new HashSet<CompetitorNumbersTransaction>();
            this.MeetSetupCompetitorNumbers = new HashSet<MeetSetupCompetitorNumber>();
        }
    
        public int CompetitorNumberID { get; set; }
        public string CompetitorNumbersName { get; set; }
        public bool IsSingleSelectionGroup { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompetitorNumbersTransaction> CompetitorNumbersTransactions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MeetSetupCompetitorNumber> MeetSetupCompetitorNumbers { get; set; }
    }
}
