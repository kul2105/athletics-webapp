//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReportDataSetMappedMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ReportDataSetMappedMaster()
        {
            this.ReportDataSetMappedDetails = new HashSet<ReportDataSetMappedDetail>();
        }
    
        public int ReportDataSetMappedID { get; set; }
        public Nullable<int> ReportDataSetTableID { get; set; }
        public Nullable<int> ReportID { get; set; }
        public string ReportTemplate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReportDataSetMappedDetail> ReportDataSetMappedDetails { get; set; }
        public virtual ReportDataSetTable ReportDataSetTable { get; set; }
        public virtual ReportDetail ReportDetail { get; set; }
    }
}
