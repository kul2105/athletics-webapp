//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TimeMarkStandardTagMaster
    {
        public int TimeMarkStandardTagMasterID { get; set; }
        public string StandardTagName { get; set; }
        public string StandardPerpose { get; set; }
        public Nullable<System.TimeSpan> StandardTime { get; set; }
    }
}
