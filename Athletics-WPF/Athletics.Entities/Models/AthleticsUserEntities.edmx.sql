
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/22/2018 11:28:01
-- Generated from EDMX file: D:\AthleticsWebApp\Athletics-WPF\Athletics.Entities\Models\AthleticsUserEntities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [AthleticsDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ApplicationMenu1_ApplicationMenu1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ApplicationMenu] DROP CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetup_BaseCounty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetup] DROP CONSTRAINT [FK_MeetSetup_BaseCounty];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetup_MeetArena]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetup] DROP CONSTRAINT [FK_MeetSetup_MeetArena];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetup_MeetClass]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetup] DROP CONSTRAINT [FK_MeetSetup_MeetClass];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetup_MeetKind]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetup] DROP CONSTRAINT [FK_MeetSetup_MeetKind];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetup_MeetStyle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetup] DROP CONSTRAINT [FK_MeetSetup_MeetStyle];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetup_MeetType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetup] DROP CONSTRAINT [FK_MeetSetup_MeetType];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetup_MeetTypeDivision]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetup] DROP CONSTRAINT [FK_MeetSetup_MeetTypeDivision];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetupAthletPrefrencesDetails_AthletePreferences]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetupAthletPrefrences] DROP CONSTRAINT [FK_MeetSetupAthletPrefrencesDetails_AthletePreferences];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetupAthletPrefrencesDetails_MeetSetup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetupAthletPrefrences] DROP CONSTRAINT [FK_MeetSetupAthletPrefrencesDetails_MeetSetup];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetupCompetitorNumber_CompetitorNumbers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetupCompetitorNumber] DROP CONSTRAINT [FK_MeetSetupCompetitorNumber_CompetitorNumbers];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetupCompetitorNumber_MeetSetup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetupCompetitorNumber] DROP CONSTRAINT [FK_MeetSetupCompetitorNumber_MeetSetup];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetupRelayPreference_MeetSetup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetupRelayPreference] DROP CONSTRAINT [FK_MeetSetupRelayPreference_MeetSetup];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetSetupRelayPreference_RelayPreferences]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetSetupRelayPreference] DROP CONSTRAINT [FK_MeetSetupRelayPreference_RelayPreferences];
GO
IF OBJECT_ID(N'[dbo].[FK_MeetTypeDivision_MeetType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeetTypeDivision] DROP CONSTRAINT [FK_MeetTypeDivision_MeetType];
GO
IF OBJECT_ID(N'[dbo].[FK_MenuInRole_ApplicationMenu]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MenuInRole] DROP CONSTRAINT [FK_MenuInRole_ApplicationMenu];
GO
IF OBJECT_ID(N'[dbo].[FK_MenuInRole_Role]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MenuInRole] DROP CONSTRAINT [FK_MenuInRole_Role];
GO
IF OBJECT_ID(N'[dbo].[FK_SchoolDetail_MeetSetup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SchoolDetail] DROP CONSTRAINT [FK_SchoolDetail_MeetSetup];
GO
IF OBJECT_ID(N'[dbo].[FK_UserInRole_Role]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserInRole] DROP CONSTRAINT [FK_UserInRole_Role];
GO
IF OBJECT_ID(N'[dbo].[FK_UserInRole_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserInRole] DROP CONSTRAINT [FK_UserInRole_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ApplicationMenu]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ApplicationMenu];
GO
IF OBJECT_ID(N'[dbo].[AthletePreferences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AthletePreferences];
GO
IF OBJECT_ID(N'[dbo].[BaseCounty]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BaseCounty];
GO
IF OBJECT_ID(N'[dbo].[CompetitorNumbers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CompetitorNumbers];
GO
IF OBJECT_ID(N'[dbo].[LaneDetail]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LaneDetail];
GO
IF OBJECT_ID(N'[dbo].[MeetArena]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetArena];
GO
IF OBJECT_ID(N'[dbo].[MeetClass]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetClass];
GO
IF OBJECT_ID(N'[dbo].[MeetKind]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetKind];
GO
IF OBJECT_ID(N'[dbo].[MeetSetup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetSetup];
GO
IF OBJECT_ID(N'[dbo].[MeetSetupAthletPrefrences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetSetupAthletPrefrences];
GO
IF OBJECT_ID(N'[dbo].[MeetSetupCompetitorNumber]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetSetupCompetitorNumber];
GO
IF OBJECT_ID(N'[dbo].[MeetSetupRelayPreference]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetSetupRelayPreference];
GO
IF OBJECT_ID(N'[dbo].[MeetStyle]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetStyle];
GO
IF OBJECT_ID(N'[dbo].[MeetType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetType];
GO
IF OBJECT_ID(N'[dbo].[MeetTypeDivision]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeetTypeDivision];
GO
IF OBJECT_ID(N'[dbo].[MenuInRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MenuInRole];
GO
IF OBJECT_ID(N'[dbo].[RelayPreferences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RelayPreferences];
GO
IF OBJECT_ID(N'[dbo].[Role]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Role];
GO
IF OBJECT_ID(N'[dbo].[SchoolDetail]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SchoolDetail];
GO
IF OBJECT_ID(N'[dbo].[StandardAlleyPrefrences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StandardAlleyPrefrences];
GO
IF OBJECT_ID(N'[dbo].[StandardLanePrefrences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StandardLanePrefrences];
GO
IF OBJECT_ID(N'[dbo].[User]', 'U') IS NOT NULL
    DROP TABLE [dbo].[User];
GO
IF OBJECT_ID(N'[dbo].[UserInRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserInRole];
GO
IF OBJECT_ID(N'[dbo].[WaterfallStartPreferences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WaterfallStartPreferences];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'UserInRoles'
CREATE TABLE [dbo].[UserInRoles] (
    [UserGuid] nvarchar(50)  NULL,
    [RoleGuid] nvarchar(50)  NULL,
    [UserInRoleID] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserGuid] nvarchar(50)  NOT NULL,
    [EmailID] nvarchar(max)  NULL,
    [FirstName] nvarchar(max)  NULL,
    [LastName] nvarchar(max)  NULL,
    [Password] nvarchar(max)  NULL,
    [UserPhoto] varbinary(max)  NULL,
    [DisplayName] nvarchar(max)  NULL,
    [UserName] nvarchar(max)  NULL,
    [IsAdmin] bit  NULL
);
GO

-- Creating table 'ApplicationMenus'
CREATE TABLE [dbo].[ApplicationMenus] (
    [MenuID] int  NOT NULL,
    [MenuName] nvarchar(50)  NULL,
    [ParentID] int  NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [RoleGuid] nvarchar(50)  NOT NULL,
    [RoleID] nvarchar(50)  NULL,
    [RoleName] nvarchar(50)  NULL
);
GO

-- Creating table 'MenuInRoles'
CREATE TABLE [dbo].[MenuInRoles] (
    [MenuInRoleID] int IDENTITY(1,1) NOT NULL,
    [RoleID] nvarchar(50)  NULL,
    [MenuID] int  NULL
);
GO

-- Creating table 'BaseCounties'
CREATE TABLE [dbo].[BaseCounties] (
    [BaseCountyID] int IDENTITY(1,1) NOT NULL,
    [BaseCountyName] nvarchar(250)  NULL
);
GO

-- Creating table 'MeetArenas'
CREATE TABLE [dbo].[MeetArenas] (
    [MeetArenaID] int IDENTITY(1,1) NOT NULL,
    [MeetArenaName] nvarchar(250)  NULL
);
GO

-- Creating table 'MeetKinds'
CREATE TABLE [dbo].[MeetKinds] (
    [MeetKindID] int IDENTITY(1,1) NOT NULL,
    [MeetKindName] nvarchar(50)  NULL
);
GO

-- Creating table 'MeetStyles'
CREATE TABLE [dbo].[MeetStyles] (
    [MeetStyleID] int IDENTITY(1,1) NOT NULL,
    [MeetStyleName] nvarchar(50)  NULL
);
GO

-- Creating table 'MeetTypes'
CREATE TABLE [dbo].[MeetTypes] (
    [MeetTypeID] int IDENTITY(1,1) NOT NULL,
    [MeetTypeName] nvarchar(150)  NULL
);
GO

-- Creating table 'MeetTypeDivisions'
CREATE TABLE [dbo].[MeetTypeDivisions] (
    [MeetTypeDivisionID] int IDENTITY(1,1) NOT NULL,
    [MeetTypeDivisionName] nvarchar(250)  NULL,
    [MeetTypeID] int  NULL
);
GO

-- Creating table 'MeetClasses'
CREATE TABLE [dbo].[MeetClasses] (
    [MeetClassID] int IDENTITY(1,1) NOT NULL,
    [MeetClassName] nvarchar(250)  NULL
);
GO

-- Creating table 'MeetSetups'
CREATE TABLE [dbo].[MeetSetups] (
    [MeetSetupID] int IDENTITY(1,1) NOT NULL,
    [MeetSetupName] nvarchar(250)  NULL,
    [MeetSetupName2] nvarchar(250)  NULL,
    [MeetSetupStartDate] datetime  NULL,
    [MeetSetupEndDate] datetime  NULL,
    [MeetSetupAgeUpDate] datetime  NULL,
    [MeetKindID] int  NULL,
    [MeetClassID] int  NULL,
    [MeetTypeID] int  NULL,
    [BaseCountyID] int  NULL,
    [MeetArenaID] int  NULL,
    [MeetStyleID] int  NULL,
    [UseDivisionBirthdateRange] bit  NULL,
    [LinkTermToDivision] bit  NULL,
    [MeetTypeDivisionID] int  NULL
);
GO

-- Creating table 'AthletePreferences'
CREATE TABLE [dbo].[AthletePreferences] (
    [AthletePreferencesID] int IDENTITY(1,1) NOT NULL,
    [AthletePreferencesName] nvarchar(250)  NULL
);
GO

-- Creating table 'CompetitorNumbers'
CREATE TABLE [dbo].[CompetitorNumbers] (
    [CompetitorNumberID] int IDENTITY(1,1) NOT NULL,
    [CompetitorNumbersName] nvarchar(250)  NULL,
    [IsSingleSelectionGroup] bit  NOT NULL
);
GO

-- Creating table 'RelayPreferences'
CREATE TABLE [dbo].[RelayPreferences] (
    [RelayPreferencesID] int IDENTITY(1,1) NOT NULL,
    [RelayPreferencesName] nvarchar(250)  NULL
);
GO

-- Creating table 'LaneDetails'
CREATE TABLE [dbo].[LaneDetails] (
    [LaneID] int IDENTITY(1,1) NOT NULL,
    [LaneNumber] int  NULL,
    [SchoolID] int  NULL
);
GO

-- Creating table 'SchoolDetails'
CREATE TABLE [dbo].[SchoolDetails] (
    [SchoolID] int IDENTITY(1,1) NOT NULL,
    [Abbr] nvarchar(50)  NULL,
    [SchoolName] nvarchar(500)  NULL,
    [MeetSetupID] int  NULL
);
GO

-- Creating table 'MeetSetupAthletPrefrences'
CREATE TABLE [dbo].[MeetSetupAthletPrefrences] (
    [MeetSetupAthletPrefrencesID] int IDENTITY(1,1) NOT NULL,
    [AthletePreferencesID] int  NULL,
    [MeetSetupID] int  NULL
);
GO

-- Creating table 'MeetSetupCompetitorNumbers'
CREATE TABLE [dbo].[MeetSetupCompetitorNumbers] (
    [MeetSetupCompetitorNumberID] int IDENTITY(1,1) NOT NULL,
    [MeetSetupID] int  NULL,
    [CompetitorNumberID] int  NULL
);
GO

-- Creating table 'MeetSetupRelayPreferences'
CREATE TABLE [dbo].[MeetSetupRelayPreferences] (
    [MeetSetupRelayPreferencesID] int IDENTITY(1,1) NOT NULL,
    [RelayPreferencesID] int  NULL,
    [MeetSetupID] int  NULL
);
GO

-- Creating table 'StandardLanePrefrences'
CREATE TABLE [dbo].[StandardLanePrefrences] (
    [StandardLanePreferencesID] int IDENTITY(1,1) NOT NULL,
    [PreferencesName] nvarchar(250)  NULL,
    [FirstLane] int  NULL,
    [SecondLine] int  NULL,
    [ThirdLine] int  NULL,
    [ForthLine] int  NULL,
    [FifthLine] int  NULL,
    [SixthLine] int  NULL,
    [SeventhLine] int  NULL,
    [EightsLine] int  NULL,
    [NinthLine] int  NULL,
    [TenthLine] int  NULL
);
GO

-- Creating table 'StandardAlleyPrefrences'
CREATE TABLE [dbo].[StandardAlleyPrefrences] (
    [StandardAlleyPreferencesID] int IDENTITY(1,1) NOT NULL,
    [PreferencesName] nvarchar(250)  NULL,
    [FirstLane] int  NULL,
    [SecondLine] int  NULL,
    [ThirdLine] int  NULL,
    [ForthLine] int  NULL,
    [FifthLine] int  NULL,
    [SixthLine] int  NULL,
    [SeventhLine] int  NULL,
    [EightsLine] int  NULL,
    [NinthLine] int  NULL,
    [TenthLine] int  NULL
);
GO

-- Creating table 'WaterfallStartPreferences'
CREATE TABLE [dbo].[WaterfallStartPreferences] (
    [WaterfallStartPreferencesID] int IDENTITY(1,1) NOT NULL,
    [Position] int  NULL,
    [Rank] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [UserInRoleID] in table 'UserInRoles'
ALTER TABLE [dbo].[UserInRoles]
ADD CONSTRAINT [PK_UserInRoles]
    PRIMARY KEY CLUSTERED ([UserInRoleID] ASC);
GO

-- Creating primary key on [UserGuid] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserGuid] ASC);
GO

-- Creating primary key on [MenuID] in table 'ApplicationMenus'
ALTER TABLE [dbo].[ApplicationMenus]
ADD CONSTRAINT [PK_ApplicationMenus]
    PRIMARY KEY CLUSTERED ([MenuID] ASC);
GO

-- Creating primary key on [RoleGuid] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([RoleGuid] ASC);
GO

-- Creating primary key on [MenuInRoleID] in table 'MenuInRoles'
ALTER TABLE [dbo].[MenuInRoles]
ADD CONSTRAINT [PK_MenuInRoles]
    PRIMARY KEY CLUSTERED ([MenuInRoleID] ASC);
GO

-- Creating primary key on [BaseCountyID] in table 'BaseCounties'
ALTER TABLE [dbo].[BaseCounties]
ADD CONSTRAINT [PK_BaseCounties]
    PRIMARY KEY CLUSTERED ([BaseCountyID] ASC);
GO

-- Creating primary key on [MeetArenaID] in table 'MeetArenas'
ALTER TABLE [dbo].[MeetArenas]
ADD CONSTRAINT [PK_MeetArenas]
    PRIMARY KEY CLUSTERED ([MeetArenaID] ASC);
GO

-- Creating primary key on [MeetKindID] in table 'MeetKinds'
ALTER TABLE [dbo].[MeetKinds]
ADD CONSTRAINT [PK_MeetKinds]
    PRIMARY KEY CLUSTERED ([MeetKindID] ASC);
GO

-- Creating primary key on [MeetStyleID] in table 'MeetStyles'
ALTER TABLE [dbo].[MeetStyles]
ADD CONSTRAINT [PK_MeetStyles]
    PRIMARY KEY CLUSTERED ([MeetStyleID] ASC);
GO

-- Creating primary key on [MeetTypeID] in table 'MeetTypes'
ALTER TABLE [dbo].[MeetTypes]
ADD CONSTRAINT [PK_MeetTypes]
    PRIMARY KEY CLUSTERED ([MeetTypeID] ASC);
GO

-- Creating primary key on [MeetTypeDivisionID] in table 'MeetTypeDivisions'
ALTER TABLE [dbo].[MeetTypeDivisions]
ADD CONSTRAINT [PK_MeetTypeDivisions]
    PRIMARY KEY CLUSTERED ([MeetTypeDivisionID] ASC);
GO

-- Creating primary key on [MeetClassID] in table 'MeetClasses'
ALTER TABLE [dbo].[MeetClasses]
ADD CONSTRAINT [PK_MeetClasses]
    PRIMARY KEY CLUSTERED ([MeetClassID] ASC);
GO

-- Creating primary key on [MeetSetupID] in table 'MeetSetups'
ALTER TABLE [dbo].[MeetSetups]
ADD CONSTRAINT [PK_MeetSetups]
    PRIMARY KEY CLUSTERED ([MeetSetupID] ASC);
GO

-- Creating primary key on [AthletePreferencesID] in table 'AthletePreferences'
ALTER TABLE [dbo].[AthletePreferences]
ADD CONSTRAINT [PK_AthletePreferences]
    PRIMARY KEY CLUSTERED ([AthletePreferencesID] ASC);
GO

-- Creating primary key on [CompetitorNumberID] in table 'CompetitorNumbers'
ALTER TABLE [dbo].[CompetitorNumbers]
ADD CONSTRAINT [PK_CompetitorNumbers]
    PRIMARY KEY CLUSTERED ([CompetitorNumberID] ASC);
GO

-- Creating primary key on [RelayPreferencesID] in table 'RelayPreferences'
ALTER TABLE [dbo].[RelayPreferences]
ADD CONSTRAINT [PK_RelayPreferences]
    PRIMARY KEY CLUSTERED ([RelayPreferencesID] ASC);
GO

-- Creating primary key on [LaneID] in table 'LaneDetails'
ALTER TABLE [dbo].[LaneDetails]
ADD CONSTRAINT [PK_LaneDetails]
    PRIMARY KEY CLUSTERED ([LaneID] ASC);
GO

-- Creating primary key on [SchoolID] in table 'SchoolDetails'
ALTER TABLE [dbo].[SchoolDetails]
ADD CONSTRAINT [PK_SchoolDetails]
    PRIMARY KEY CLUSTERED ([SchoolID] ASC);
GO

-- Creating primary key on [MeetSetupAthletPrefrencesID] in table 'MeetSetupAthletPrefrences'
ALTER TABLE [dbo].[MeetSetupAthletPrefrences]
ADD CONSTRAINT [PK_MeetSetupAthletPrefrences]
    PRIMARY KEY CLUSTERED ([MeetSetupAthletPrefrencesID] ASC);
GO

-- Creating primary key on [MeetSetupCompetitorNumberID] in table 'MeetSetupCompetitorNumbers'
ALTER TABLE [dbo].[MeetSetupCompetitorNumbers]
ADD CONSTRAINT [PK_MeetSetupCompetitorNumbers]
    PRIMARY KEY CLUSTERED ([MeetSetupCompetitorNumberID] ASC);
GO

-- Creating primary key on [MeetSetupRelayPreferencesID] in table 'MeetSetupRelayPreferences'
ALTER TABLE [dbo].[MeetSetupRelayPreferences]
ADD CONSTRAINT [PK_MeetSetupRelayPreferences]
    PRIMARY KEY CLUSTERED ([MeetSetupRelayPreferencesID] ASC);
GO

-- Creating primary key on [StandardLanePreferencesID] in table 'StandardLanePrefrences'
ALTER TABLE [dbo].[StandardLanePrefrences]
ADD CONSTRAINT [PK_StandardLanePrefrences]
    PRIMARY KEY CLUSTERED ([StandardLanePreferencesID] ASC);
GO

-- Creating primary key on [StandardAlleyPreferencesID] in table 'StandardAlleyPrefrences'
ALTER TABLE [dbo].[StandardAlleyPrefrences]
ADD CONSTRAINT [PK_StandardAlleyPrefrences]
    PRIMARY KEY CLUSTERED ([StandardAlleyPreferencesID] ASC);
GO

-- Creating primary key on [WaterfallStartPreferencesID] in table 'WaterfallStartPreferences'
ALTER TABLE [dbo].[WaterfallStartPreferences]
ADD CONSTRAINT [PK_WaterfallStartPreferences]
    PRIMARY KEY CLUSTERED ([WaterfallStartPreferencesID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserGuid] in table 'UserInRoles'
ALTER TABLE [dbo].[UserInRoles]
ADD CONSTRAINT [FK_UserInRole_User]
    FOREIGN KEY ([UserGuid])
    REFERENCES [dbo].[Users]
        ([UserGuid])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserInRole_User'
CREATE INDEX [IX_FK_UserInRole_User]
ON [dbo].[UserInRoles]
    ([UserGuid]);
GO

-- Creating foreign key on [ParentID] in table 'ApplicationMenus'
ALTER TABLE [dbo].[ApplicationMenus]
ADD CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1]
    FOREIGN KEY ([ParentID])
    REFERENCES [dbo].[ApplicationMenus]
        ([MenuID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ApplicationMenu1_ApplicationMenu1'
CREATE INDEX [IX_FK_ApplicationMenu1_ApplicationMenu1]
ON [dbo].[ApplicationMenus]
    ([ParentID]);
GO

-- Creating foreign key on [RoleGuid] in table 'UserInRoles'
ALTER TABLE [dbo].[UserInRoles]
ADD CONSTRAINT [FK_UserInRole_Role]
    FOREIGN KEY ([RoleGuid])
    REFERENCES [dbo].[Roles]
        ([RoleGuid])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserInRole_Role'
CREATE INDEX [IX_FK_UserInRole_Role]
ON [dbo].[UserInRoles]
    ([RoleGuid]);
GO

-- Creating foreign key on [MenuID] in table 'MenuInRoles'
ALTER TABLE [dbo].[MenuInRoles]
ADD CONSTRAINT [FK_MenuInRole_ApplicationMenu]
    FOREIGN KEY ([MenuID])
    REFERENCES [dbo].[ApplicationMenus]
        ([MenuID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MenuInRole_ApplicationMenu'
CREATE INDEX [IX_FK_MenuInRole_ApplicationMenu]
ON [dbo].[MenuInRoles]
    ([MenuID]);
GO

-- Creating foreign key on [RoleID] in table 'MenuInRoles'
ALTER TABLE [dbo].[MenuInRoles]
ADD CONSTRAINT [FK_MenuInRole_Role]
    FOREIGN KEY ([RoleID])
    REFERENCES [dbo].[Roles]
        ([RoleGuid])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MenuInRole_Role'
CREATE INDEX [IX_FK_MenuInRole_Role]
ON [dbo].[MenuInRoles]
    ([RoleID]);
GO

-- Creating foreign key on [MeetTypeID] in table 'MeetTypeDivisions'
ALTER TABLE [dbo].[MeetTypeDivisions]
ADD CONSTRAINT [FK_MeetTypeDivision_MeetType]
    FOREIGN KEY ([MeetTypeID])
    REFERENCES [dbo].[MeetTypes]
        ([MeetTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetTypeDivision_MeetType'
CREATE INDEX [IX_FK_MeetTypeDivision_MeetType]
ON [dbo].[MeetTypeDivisions]
    ([MeetTypeID]);
GO

-- Creating foreign key on [BaseCountyID] in table 'MeetSetups'
ALTER TABLE [dbo].[MeetSetups]
ADD CONSTRAINT [FK_MeetSetup_BaseCounty]
    FOREIGN KEY ([BaseCountyID])
    REFERENCES [dbo].[BaseCounties]
        ([BaseCountyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetup_BaseCounty'
CREATE INDEX [IX_FK_MeetSetup_BaseCounty]
ON [dbo].[MeetSetups]
    ([BaseCountyID]);
GO

-- Creating foreign key on [MeetArenaID] in table 'MeetSetups'
ALTER TABLE [dbo].[MeetSetups]
ADD CONSTRAINT [FK_MeetSetup_MeetArena]
    FOREIGN KEY ([MeetArenaID])
    REFERENCES [dbo].[MeetArenas]
        ([MeetArenaID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetup_MeetArena'
CREATE INDEX [IX_FK_MeetSetup_MeetArena]
ON [dbo].[MeetSetups]
    ([MeetArenaID]);
GO

-- Creating foreign key on [MeetClassID] in table 'MeetSetups'
ALTER TABLE [dbo].[MeetSetups]
ADD CONSTRAINT [FK_MeetSetup_MeetClass]
    FOREIGN KEY ([MeetClassID])
    REFERENCES [dbo].[MeetClasses]
        ([MeetClassID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetup_MeetClass'
CREATE INDEX [IX_FK_MeetSetup_MeetClass]
ON [dbo].[MeetSetups]
    ([MeetClassID]);
GO

-- Creating foreign key on [MeetKindID] in table 'MeetSetups'
ALTER TABLE [dbo].[MeetSetups]
ADD CONSTRAINT [FK_MeetSetup_MeetKind]
    FOREIGN KEY ([MeetKindID])
    REFERENCES [dbo].[MeetKinds]
        ([MeetKindID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetup_MeetKind'
CREATE INDEX [IX_FK_MeetSetup_MeetKind]
ON [dbo].[MeetSetups]
    ([MeetKindID]);
GO

-- Creating foreign key on [MeetStyleID] in table 'MeetSetups'
ALTER TABLE [dbo].[MeetSetups]
ADD CONSTRAINT [FK_MeetSetup_MeetStyle]
    FOREIGN KEY ([MeetStyleID])
    REFERENCES [dbo].[MeetStyles]
        ([MeetStyleID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetup_MeetStyle'
CREATE INDEX [IX_FK_MeetSetup_MeetStyle]
ON [dbo].[MeetSetups]
    ([MeetStyleID]);
GO

-- Creating foreign key on [MeetTypeID] in table 'MeetSetups'
ALTER TABLE [dbo].[MeetSetups]
ADD CONSTRAINT [FK_MeetSetup_MeetType]
    FOREIGN KEY ([MeetTypeID])
    REFERENCES [dbo].[MeetTypes]
        ([MeetTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetup_MeetType'
CREATE INDEX [IX_FK_MeetSetup_MeetType]
ON [dbo].[MeetSetups]
    ([MeetTypeID]);
GO

-- Creating foreign key on [MeetTypeDivisionID] in table 'MeetSetups'
ALTER TABLE [dbo].[MeetSetups]
ADD CONSTRAINT [FK_MeetSetup_MeetTypeDivision]
    FOREIGN KEY ([MeetTypeDivisionID])
    REFERENCES [dbo].[MeetTypeDivisions]
        ([MeetTypeDivisionID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetup_MeetTypeDivision'
CREATE INDEX [IX_FK_MeetSetup_MeetTypeDivision]
ON [dbo].[MeetSetups]
    ([MeetTypeDivisionID]);
GO

-- Creating foreign key on [AthletePreferencesID] in table 'MeetSetupAthletPrefrences'
ALTER TABLE [dbo].[MeetSetupAthletPrefrences]
ADD CONSTRAINT [FK_MeetSetupAthletPrefrencesDetails_AthletePreferences]
    FOREIGN KEY ([AthletePreferencesID])
    REFERENCES [dbo].[AthletePreferences]
        ([AthletePreferencesID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetupAthletPrefrencesDetails_AthletePreferences'
CREATE INDEX [IX_FK_MeetSetupAthletPrefrencesDetails_AthletePreferences]
ON [dbo].[MeetSetupAthletPrefrences]
    ([AthletePreferencesID]);
GO

-- Creating foreign key on [CompetitorNumberID] in table 'MeetSetupCompetitorNumbers'
ALTER TABLE [dbo].[MeetSetupCompetitorNumbers]
ADD CONSTRAINT [FK_MeetSetupCompetitorNumber_CompetitorNumbers]
    FOREIGN KEY ([CompetitorNumberID])
    REFERENCES [dbo].[CompetitorNumbers]
        ([CompetitorNumberID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetupCompetitorNumber_CompetitorNumbers'
CREATE INDEX [IX_FK_MeetSetupCompetitorNumber_CompetitorNumbers]
ON [dbo].[MeetSetupCompetitorNumbers]
    ([CompetitorNumberID]);
GO

-- Creating foreign key on [MeetSetupID] in table 'MeetSetupAthletPrefrences'
ALTER TABLE [dbo].[MeetSetupAthletPrefrences]
ADD CONSTRAINT [FK_MeetSetupAthletPrefrencesDetails_MeetSetup]
    FOREIGN KEY ([MeetSetupID])
    REFERENCES [dbo].[MeetSetups]
        ([MeetSetupID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetupAthletPrefrencesDetails_MeetSetup'
CREATE INDEX [IX_FK_MeetSetupAthletPrefrencesDetails_MeetSetup]
ON [dbo].[MeetSetupAthletPrefrences]
    ([MeetSetupID]);
GO

-- Creating foreign key on [MeetSetupID] in table 'MeetSetupCompetitorNumbers'
ALTER TABLE [dbo].[MeetSetupCompetitorNumbers]
ADD CONSTRAINT [FK_MeetSetupCompetitorNumber_MeetSetup]
    FOREIGN KEY ([MeetSetupID])
    REFERENCES [dbo].[MeetSetups]
        ([MeetSetupID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetupCompetitorNumber_MeetSetup'
CREATE INDEX [IX_FK_MeetSetupCompetitorNumber_MeetSetup]
ON [dbo].[MeetSetupCompetitorNumbers]
    ([MeetSetupID]);
GO

-- Creating foreign key on [MeetSetupID] in table 'MeetSetupRelayPreferences'
ALTER TABLE [dbo].[MeetSetupRelayPreferences]
ADD CONSTRAINT [FK_MeetSetupRelayPreference_MeetSetup]
    FOREIGN KEY ([MeetSetupID])
    REFERENCES [dbo].[MeetSetups]
        ([MeetSetupID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetupRelayPreference_MeetSetup'
CREATE INDEX [IX_FK_MeetSetupRelayPreference_MeetSetup]
ON [dbo].[MeetSetupRelayPreferences]
    ([MeetSetupID]);
GO

-- Creating foreign key on [MeetSetupID] in table 'SchoolDetails'
ALTER TABLE [dbo].[SchoolDetails]
ADD CONSTRAINT [FK_SchoolDetail_MeetSetup]
    FOREIGN KEY ([MeetSetupID])
    REFERENCES [dbo].[MeetSetups]
        ([MeetSetupID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SchoolDetail_MeetSetup'
CREATE INDEX [IX_FK_SchoolDetail_MeetSetup]
ON [dbo].[SchoolDetails]
    ([MeetSetupID]);
GO

-- Creating foreign key on [RelayPreferencesID] in table 'MeetSetupRelayPreferences'
ALTER TABLE [dbo].[MeetSetupRelayPreferences]
ADD CONSTRAINT [FK_MeetSetupRelayPreference_RelayPreferences]
    FOREIGN KEY ([RelayPreferencesID])
    REFERENCES [dbo].[RelayPreferences]
        ([RelayPreferencesID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MeetSetupRelayPreference_RelayPreferences'
CREATE INDEX [IX_FK_MeetSetupRelayPreference_RelayPreferences]
ON [dbo].[MeetSetupRelayPreferences]
    ([RelayPreferencesID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------