//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SeedPreferenceTemplate
    {
        public int SeedPreferenceTemplateID { get; set; }
        public string SeedPreferenceTemplateName { get; set; }
        public Nullable<int> SeedPreferenceID { get; set; }
    
        public virtual SeedingPreferencesTransaction SeedingPreferencesTransaction { get; set; }
    }
}
