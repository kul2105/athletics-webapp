//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AthletePreference
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AthletePreference()
        {
            this.AthleticPreferencesTransactions = new HashSet<AthleticPreferencesTransaction>();
            this.MeetSetupAthletePreferences = new HashSet<MeetSetupAthletePreference>();
        }
    
        public int AthletePreferencesID { get; set; }
        public string AthletePreferencesName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AthleticPreferencesTransaction> AthleticPreferencesTransactions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MeetSetupAthletePreference> MeetSetupAthletePreferences { get; set; }
    }
}
