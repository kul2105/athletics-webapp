//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScroringPreferencesTemplateEntryLimit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScroringPreferencesTemplateEntryLimit()
        {
            this.ScroringPreferencesTemplates = new HashSet<ScroringPreferencesTemplate>();
        }
    
        public int ScroringPreferencesTemplateEntryLimitID { get; set; }
        public Nullable<int> MaximumEntriesPerAthleteIncludingRelays { get; set; }
        public Nullable<int> MaximumTrackEventEntriesPerAthleteIncludingRelay { get; set; }
        public Nullable<int> MaximumFieldEventEntriesPerAthletes { get; set; }
        public Nullable<bool> WarnIfEntryLimitsExceeds { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScroringPreferencesTemplate> ScroringPreferencesTemplates { get; set; }
    }
}
