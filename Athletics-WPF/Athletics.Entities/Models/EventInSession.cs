//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventInSession
    {
        public int EventInSessionID { get; set; }
        public Nullable<int> EventID { get; set; }
        public Nullable<int> SessionScheduleID { get; set; }
    
        public virtual EventDetail EventDetail { get; set; }
        public virtual EventSessionSchedule EventSessionSchedule { get; set; }
    }
}
