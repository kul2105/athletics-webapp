//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Athletics.Entities.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MeetSetupScroringPreferencesEntriesResult
    {
        public int ScroringPreferencesEntriesResultID { get; set; }
        public Nullable<int> MeetSetupID { get; set; }
        public Nullable<bool> UseEntryDeclarationMethod { get; set; }
        public Nullable<bool> EntryNotesWithEachEntry { get; set; }
        public Nullable<bool> ConvertHandEntryTimesToFAT { get; set; }
        public Nullable<bool> ShowFinishedPlaceWithEachEntry { get; set; }
        public Nullable<bool> RecordWindReading { get; set; }
        public Nullable<bool> RoundUpResultToTenthofReport { get; set; }
        public Nullable<bool> RankResultByAgeGradedMarks { get; set; }
        public Nullable<bool> WarnIfTimesMarksAreOutofRange { get; set; }
        public Nullable<bool> RoundDownTheNearestEventCentimerer { get; set; }
    }
}
