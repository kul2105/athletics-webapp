﻿using Athletics.Entities.Models;
using Athletics.Athletes.ViewModels;
using Athletics.MeetSetup.Services.API;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Athletics.Athletes.SetupMeetManager
{
    public enum eMEET_REQUEST_RESPONSE
    {
        DEFAULT_ERROR,
        SUCCESS,
    }

    public class AuthorizedResponse<T> where T : class
    {
        public AuthorizedResponse()
        {
            this.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
            Message = "";
            Result = null;
        }

        public eMEET_REQUEST_RESPONSE Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }


    }


    /// <summary>
    /// Anything having to do Meet Setup 
    /// inside this service
    /// </summary>
    public interface IEventManager
    {

        Task<AuthorizedResponse<EventResult>> CreateNewEvent(ServiceRequest_Event EventRequest);
        Task<AuthorizedResponse<EventResult>> UpdateEvent(ServiceRequest_Event EventRequest);
        Task<AuthorizedResponse<EventResult>> DeleteEvent(int EventID);
        Task<List<AddEventViewModel>> GetAllEvent();
        Task<List<AddSessionViewModel>> GetAllSessions();
        Task<AuthorizedResponse<SessionResult>> CreateNewSession(ServiceRequest_Session SessionRequest);
        Task<AuthorizedResponse<SessionResult>> UpdateSession(ServiceRequest_Session SessionRequest);
        Task<AuthorizedResponse<SessionResult>> DeleteSession(int SessionID);
        Task<List<EventSessionSchedule>> GetAllSessionSchedules();
        Task<List<EventSessionSchedule>> GetAllSessionSchedules(int SessionID);
        AuthorizedResponse<SessionScheduleResult> CreateNewSessionSchedule(ServiceRequest_SessionSchedule SessionRequest);
        Task<AuthorizedResponse<SessionScheduleResult>> UpdateSessionSchedule(ServiceRequest_SessionSchedule SessionRequest);
        Task<AuthorizedResponse<SessionScheduleResult>> DeleteSessionSchedule(int SessionScheduleID);
        Task<List<RecordTag>> GetAllRecordTag();
        Task<AuthorizedResponse<RecordTagResult>> CreateRecordTag(ServiceRequest_RecordTag RecordTag);
        Task<AuthorizedResponse<RecordTagResult>> UpdateRecordTag(ServiceRequest_RecordTag RecordTag);
        Task<AuthorizedResponse<RecordTagResult>> DeleteRecordTag(int RecordTagID);
        Task<AuthorizedResponse<RecordUpdateResult>> AddRecord(ServiceRequest_RecordUpdate RecordUpdate);
        Task<AuthorizedResponse<RecordUpdateResult>> UpdateRecord(ServiceRequest_RecordUpdate RecordUpdate);
        Task<List<RecordDetail>> GetAllRecord();
        Task<List<EventTimeMarkStandardTag>> GetAllTimeMarkStandardTag();
        Task<AuthorizedResponse<TimeMarkStandardTagResult>> CreateTimeMarkStandardTag(ServiceRequest_TimeMarkStandardTag RecordTag);
        Task<AuthorizedResponse<TimeMarkStandardTagResult>> UpdateTimeMarkStandardTag(ServiceRequest_TimeMarkStandardTag RecordTag);
        Task<AuthorizedResponse<TimeMarkStandardTagResult>> DeleteTimeMarkStandardTag(int EventTimeMarkStandardTagID);
        Task<AuthorizedResponse<TimeMarkStandardResult>> AddTimeMarkStandard(ServiceRequest_TimeMarkStandard TimeMarkStandard);
        Task<List<EventTimeMarkStandard>> GetAllTimeMarkStandard();
        Task<List<TimeMarkStandardTagMaster>> GetAllTimeMarkStandardTagMaster();
        Task<AuthorizedResponse<TimeMarkStandardTagMasterResult>> CreateTimeMarkStandardTagMaster(ServiceRequest_TimeMarkStandardTagMaster TimeMarkStandardTagMaster);
        Task<AuthorizedResponse<TimeMarkStandardTagMasterResult>> UpdateTimeMarkStandardTagMaster(ServiceRequest_TimeMarkStandardTagMaster TimeMarkStandardTagMaster);
        Task<AuthorizedResponse<TimeMarkStandardTagMasterResult>> DeleteTimeMarkStandardTagMaster(int EventTimeMarkStandardTagMasterID);
        Task<List<EventInSession>> GetAllEventInSession();
        Task<List<EventInMeet>> GetAllEventInMeet();
        Task<AuthorizedResponse<EventResult>> AddEventInMeet(int EventId, int MeetId);
        void CreateMeetUsingTemplate(int templateMeetID, string meetName, string meetName2, DateTime? meetStatupDate, DateTime? meetEndDate, DateTime? meetAgeupDate);
        void CreateEventUsingTemplate(int meetID,int templateEventID, string eventNotes, int? startageRange, int? endageRange);



    }
}
