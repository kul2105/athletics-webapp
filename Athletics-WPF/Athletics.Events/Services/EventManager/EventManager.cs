﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Athletics.Entities.Models;
using Athletics.Athletes.ViewModels;
using Athletics.MeetSetup.Services.API;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Athletes.SetupMeetManager
{
    public class EventManager : BindableBase, IEventManager
    {
        public async Task<AuthorizedResponse<EventResult>> AddEventInMeet(int EventId, int MeetId)
        {
            try
            {
                if (EventId > 0)
                {
                    using (EventEntities ctx = new EventEntities())
                    {
                        EventInMeet eventInMeet = new EventInMeet();
                        eventInMeet.EventID = EventId;
                        eventInMeet.MeetID = MeetId;
                        ctx.EventInMeets.Add(eventInMeet);
                        ctx.SaveChanges();
                    }
                }

                AuthorizedResponse<EventResult> Result = new AuthorizedResponse<EventResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new EventResult();
                Result.Result.EventID = EventId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<EventResult> Result = new AuthorizedResponse<EventResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new EventResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<RecordUpdateResult>> AddRecord(ServiceRequest_RecordUpdate RecordUpdate)
        {
            try
            {
                int InsertedId = 0;
                if (RecordUpdate.RecordUpdate != null)
                {
                    using (EventEntities ctx = new EventEntities())
                    {
                        ctx.RecordDetails.Add(RecordUpdate.RecordUpdate);
                        ctx.SaveChanges();
                        InsertedId = RecordUpdate.RecordUpdate.RecordDetailID;
                    }
                }

                AuthorizedResponse<RecordUpdateResult> Result = new AuthorizedResponse<RecordUpdateResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new RecordUpdateResult();
                Result.Result.RecordUpdateID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<RecordUpdateResult> Result = new AuthorizedResponse<RecordUpdateResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new RecordUpdateResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<TimeMarkStandardResult>> AddTimeMarkStandard(ServiceRequest_TimeMarkStandard TimeMarkStandard)
        {
            try
            {
                int InsertedId = 0;
                if (TimeMarkStandard.TimeMarkStandard != null)
                {
                    using (EventEntities ctx = new EventEntities())
                    {
                        ctx.EventTimeMarkStandards.Add(TimeMarkStandard.TimeMarkStandard);
                        ctx.SaveChanges();
                        InsertedId = TimeMarkStandard.TimeMarkStandard.EventTimeMarkStandardID;
                    }
                }

                AuthorizedResponse<TimeMarkStandardResult> Result = new AuthorizedResponse<TimeMarkStandardResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new TimeMarkStandardResult();
                Result.Result.TimeMarkStandardID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<TimeMarkStandardResult> Result = new AuthorizedResponse<TimeMarkStandardResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new TimeMarkStandardResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<EventResult>> CreateNewEvent(ServiceRequest_Event EventRequest)
        {
            try
            {
                int InsertedId = 0;
                if (EventRequest.Event != null)
                {
                    using (EventEntities ctx = new EventEntities())
                    {
                        Entities.Models.EventDetail eventDetail = new Entities.Models.EventDetail()
                        {
                            EventNumber = EventRequest.Event.EventNumber,
                            Status = EventRequest.Event.Status,
                            Gender = EventRequest.Event.Gender,
                            StartAgeRange = EventRequest.Event.StartAgeRange,
                            EndAgeRange = EventRequest.Event.EndAgeRange,
                            IsMultiAgeGroup = EventRequest.Event.IsMultiAgeGroup,
                            Division = EventRequest.Event.Division,
                            EventNotes = EventRequest.Event.EventNotes,
                            Entries = EventRequest.Event.Entries,
                            Results = EventRequest.Event.Results,
                            AgeGroup = EventRequest.Event.AgeGroup,
                            Distance = EventRequest.Event.Distance,
                            Event = EventRequest.Event.RunningEvent,
                            Advance = EventRequest.Event.Advance,
                            HeatOrder = EventRequest.Event.HeatOrder,
                            LinePosition = EventRequest.Event.LanePosAssignment,
                            HeatInSemis = EventRequest.Event.HeatInSemis,
                            HeatInQtrs = EventRequest.Event.HeatInQtrs,
                            IsScoreEvent = EventRequest.Event.IsScoreEvent,
                            isJumbOff = EventRequest.Event.IsJumbOff,
                            EntryFee = EventRequest.Event.EntryFee,
                            Type = EventRequest.Event.Type,
                            Finals = EventRequest.Event.Finals,

                            FinalNumberOfLanes = EventRequest.Event.FinalNumberOfLanes,
                            FnalOrder = EventRequest.Event.FinalOrder,
                            NumberOfLane = EventRequest.Event.NumberOfLanes,
                            Heats = EventRequest.Event.HeadFlightAssign,
                            Rnds = EventRequest.Event.Round,

                        };

                        ctx.EventDetails.Add(eventDetail);
                        ctx.SaveChanges();
                        InsertedId = eventDetail.EventID;
                        if (EventRequest.Event.IsMultiAgeGroup)
                        {
                            foreach (var eventRequest in EventRequest.Event.EventMultiAgeDetailList)
                            {

                                EventMultiAgeDetail multiAgeDetail = new EventMultiAgeDetail()
                                {
                                    EventID = InsertedId,
                                    HighAge = eventRequest.HighAge,
                                    LowAge = eventRequest.LowAge,
                                    StandardId = eventRequest.StandardID

                                };
                                ctx.EventMultiAgeDetails.Add(multiAgeDetail);
                                ctx.SaveChanges();
                            }
                        }
                    }
                }

                AuthorizedResponse<EventResult> Result = new AuthorizedResponse<EventResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new EventResult();
                Result.Result.EventID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<EventResult> Result = new AuthorizedResponse<EventResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new EventResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }
        public async Task<AuthorizedResponse<SessionResult>> CreateNewSession(ServiceRequest_Session SessionRequest)
        {
            try
            {
                int InsertedId = 0;
                if (SessionRequest.Session != null)
                {
                    using (EventEntities ctx = new EventEntities())
                    {
                        EventSession session = new EventSession()
                        {
                            Day = SessionRequest.Session.Day,
                            EntryLimit = SessionRequest.Session.EntryLimit,
                            Session = SessionRequest.Session.Session,
                            SessionTitle = SessionRequest.Session.SessionTitle,
                            StartTime = SessionRequest.Session.StartTime,

                        };
                        ctx.EventSessions.Add(session);
                        ctx.SaveChanges();
                        InsertedId = session.EventSessionID;
                    }
                }

                AuthorizedResponse<SessionResult> Result = new AuthorizedResponse<SessionResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new SessionResult();
                Result.Result.SessionID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<SessionResult> Result = new AuthorizedResponse<SessionResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new SessionResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public AuthorizedResponse<SessionScheduleResult> CreateNewSessionSchedule(ServiceRequest_SessionSchedule SessionRequest)
        {
            try
            {
                int InsertedId = 0;
                if (SessionRequest.Session != null)
                {

                    using (EventEntities ctx = new EventEntities())
                    {
                        using (var dbContextTransaction = ctx.Database.BeginTransaction())
                        {
                            EventSessionSchedule session = new EventSessionSchedule()
                            {
                                AmPM = SessionRequest.Session.AmPM,
                                EventName = SessionRequest.Session.EventName,
                                EventNumber = SessionRequest.Session.EventNumber,
                                Report = SessionRequest.Session.Report,
                                StartTime = SessionRequest.Session.StartTime,
                                RND = SessionRequest.Session.RND,
                                SessionOrder = SessionRequest.Session.SessionOrder,
                                EventSessionID = SessionRequest.SessionID,
                            };
                            ctx.EventSessionSchedules.Add(session);
                            EventInSession EventInSession = new EventInSession()
                            {
                                EventID = SessionRequest.EventID,
                                SessionScheduleID = session.SessionScheduleID,
                            };
                            ctx.EventInSessions.Add(EventInSession);
                            ctx.SaveChanges();
                            InsertedId = session.SessionScheduleID;
                            dbContextTransaction.Commit();
                        }
                    }

                }

                AuthorizedResponse<SessionScheduleResult> Result = new AuthorizedResponse<SessionScheduleResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new SessionScheduleResult();
                Result.Result.SessionScheduleID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<SessionScheduleResult> Result = new AuthorizedResponse<SessionScheduleResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new SessionScheduleResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<RecordTagResult>> CreateRecordTag(ServiceRequest_RecordTag RecordTag)
        {
            try
            {
                int InsertedId = 0;
                if (RecordTag != null)
                {
                    using (EventEntities ctx = new EventEntities())
                    {
                        Entities.Models.RecordTag recordTag = new Entities.Models.RecordTag()
                        {
                            TagName = RecordTag.RecordTag.TagName,
                            ExhOk = RecordTag.RecordTag.ExhOk,
                            ForeignOk = RecordTag.RecordTag.ForeignOk,
                            RecordOnlyFor = RecordTag.RecordTag.RecordOnlyFor,
                            TagFlag = RecordTag.RecordTag.TagFlag,
                            TagOrder = RecordTag.RecordTag.TagOrder
                        };

                        ctx.RecordTags.Add(recordTag);
                        ctx.SaveChanges();
                        InsertedId = recordTag.RecordTagID;
                    }
                }

                AuthorizedResponse<RecordTagResult> Result = new AuthorizedResponse<RecordTagResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new RecordTagResult();
                Result.Result.RecordTagID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<RecordTagResult> Result = new AuthorizedResponse<RecordTagResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new RecordTagResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<TimeMarkStandardTagResult>> CreateTimeMarkStandardTag(ServiceRequest_TimeMarkStandardTag RecordTag)
        {
            try
            {
                int InsertedId = 0;
                if (RecordTag != null)
                {
                    using (EventEntities ctx = new EventEntities())
                    {
                        foreach (var TimeMarkStandardTag in RecordTag.SelectedTimeMarkStandardTagList)
                        {

                            EventTimeMarkStandardTag recordTag = new EventTimeMarkStandardTag()
                            {
                                TimeMarkStandardTagName = TimeMarkStandardTag.TimeMarkStandardTagName,
                                ForEntryQual = TimeMarkStandardTag.ForEntryQual,
                                ForTimeMark = TimeMarkStandardTag.ForTimeMark,
                                TimeMarkStandardEventName = TimeMarkStandardTag.TimeMarkStandardEventName,
                                TimeMarkStandardTagDescription = TimeMarkStandardTag.TimeMarkStandardTagDescription,

                            };

                            ctx.EventTimeMarkStandardTags.Add(recordTag);
                            ctx.SaveChanges();
                            InsertedId = recordTag.TimeMarkStandardTagID;
                        }
                    }
                }

                AuthorizedResponse<TimeMarkStandardTagResult> Result = new AuthorizedResponse<TimeMarkStandardTagResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new TimeMarkStandardTagResult();
                Result.Result.EventID = RecordTag.EventID;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<TimeMarkStandardTagResult> Result = new AuthorizedResponse<TimeMarkStandardTagResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new TimeMarkStandardTagResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<TimeMarkStandardTagMasterResult>> CreateTimeMarkStandardTagMaster(ServiceRequest_TimeMarkStandardTagMaster TimeMarkStandardTagMaster)
        {
            try
            {
                int InsertedId = 0;
                if (TimeMarkStandardTagMaster != null)
                {
                    using (EventEntities ctx = new EventEntities())
                    {
                        TimeMarkStandardTagMaster recordTag = new TimeMarkStandardTagMaster()
                        {
                            StandardTagName = TimeMarkStandardTagMaster.TimeMarkStandardTag.StandardTagName,
                            StandardPerpose = TimeMarkStandardTagMaster.TimeMarkStandardTag.StandardPerpose,
                            StandardTime = TimeMarkStandardTagMaster.TimeMarkStandardTag.StandardTime,
                        };

                        ctx.TimeMarkStandardTagMasters.Add(recordTag);
                        ctx.SaveChanges();
                        InsertedId = recordTag.TimeMarkStandardTagMasterID;
                    }
                }

                AuthorizedResponse<TimeMarkStandardTagMasterResult> Result = new AuthorizedResponse<TimeMarkStandardTagMasterResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new TimeMarkStandardTagMasterResult();
                Result.Result.TimeMarkStandardTagMasteID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<TimeMarkStandardTagMasterResult> Result = new AuthorizedResponse<TimeMarkStandardTagMasterResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new TimeMarkStandardTagMasterResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<EventResult>> DeleteEvent(int EventID)
        {
            AuthorizedResponse<EventResult> Result = new AuthorizedResponse<EventResult>();
            using (EventEntities ctx = new EventEntities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<EventMultiAgeDetail> multiageEventDetailList = ctx.EventMultiAgeDetails.Where(p => p.EventID == EventID).ToList();
                        if (multiageEventDetailList != null)
                        {
                            ctx.EventMultiAgeDetails.RemoveRange(multiageEventDetailList);
                        }
                        List<EventInMeet> eventInMeetList = ctx.EventInMeets.Where(p => p.EventID == EventID).ToList();
                        if (eventInMeetList != null)
                        {
                            ctx.EventInMeets.RemoveRange(eventInMeetList);
                        }
                        List<EventInSession>EventInSessionList= ctx.EventInSessions.Where(p => p.EventID == EventID).ToList();
                        if (EventInSessionList != null)
                        {
                            ctx.EventInSessions.RemoveRange(EventInSessionList);
                        }
                        List<Entities.Models.EventDetail> eventDetailList = ctx.EventDetails.Where(p => p.EventID == EventID).ToList();
                        if (eventDetailList != null)
                        {
                            ctx.EventDetails.RemoveRange(eventDetailList);
                        }
                        
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new EventResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new EventResult();
            Result.Result.EventID = EventID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<RecordTagResult>> DeleteRecordTag(int RecordTagID)
        {
            AuthorizedResponse<RecordTagResult> Result = new AuthorizedResponse<RecordTagResult>();
            using (EventEntities ctx = new EventEntities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<Entities.Models.RecordTag> RecordTagList = ctx.RecordTags.Where(p => p.RecordTagID == RecordTagID).ToList();
                        if (RecordTagList != null)
                        {
                            ctx.RecordTags.RemoveRange(RecordTagList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new RecordTagResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RecordTagResult();
            Result.Result.RecordTagID = RecordTagID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<SessionResult>> DeleteSession(int SessionID)
        {
            AuthorizedResponse<SessionResult> Result = new AuthorizedResponse<SessionResult>();
            using (EventEntities ctx = new EventEntities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<EventSession> sessionDetailList = ctx.EventSessions.Where(p => p.EventSessionID == SessionID).ToList();
                        if (sessionDetailList != null)
                        {
                            ctx.EventSessions.RemoveRange(sessionDetailList);
                        }
                        List<EventSessionSchedule> sessionScheduleList = this.GetAllSessionSchedules(SessionID).Result.ToList();
                        foreach (var item in sessionScheduleList)
                        {
                            this.DeleteSessionSchedule(item.SessionScheduleID);
                        }

                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new SessionResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new SessionResult();
            Result.Result.SessionID = SessionID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<SessionScheduleResult>> DeleteSessionSchedule(int SessionScheduleID)
        {
            AuthorizedResponse<SessionScheduleResult> Result = new AuthorizedResponse<SessionScheduleResult>();
            using (EventEntities ctx = new EventEntities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<EventSessionSchedule> sessionDetailList = ctx.EventSessionSchedules.Where(p => p.SessionScheduleID == SessionScheduleID).ToList();
                        if (sessionDetailList != null)
                        {
                            ctx.EventSessionSchedules.RemoveRange(sessionDetailList);
                        }
                        List<EventInSession> eventInSessionList = ctx.EventInSessions.Where(p => p.SessionScheduleID == SessionScheduleID).ToList();
                        if (eventInSessionList != null)
                        {
                            ctx.EventInSessions.RemoveRange(eventInSessionList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new SessionScheduleResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new SessionScheduleResult();
            Result.Result.SessionScheduleID = SessionScheduleID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<TimeMarkStandardTagResult>> DeleteTimeMarkStandardTag(int EventTimeMarkStandardTagID)
        {
            AuthorizedResponse<TimeMarkStandardTagResult> Result = new AuthorizedResponse<TimeMarkStandardTagResult>();
            using (EventEntities ctx = new EventEntities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<EventTimeMarkStandardTag> EventTimeMarkStandardTagList = ctx.EventTimeMarkStandardTags.Where(p => p.TimeMarkStandardTagID == EventTimeMarkStandardTagID).ToList();
                        if (EventTimeMarkStandardTagList != null)
                        {
                            ctx.EventTimeMarkStandardTags.RemoveRange(EventTimeMarkStandardTagList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new TimeMarkStandardTagResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new TimeMarkStandardTagResult();
            Result.Result.EventID = EventTimeMarkStandardTagID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<TimeMarkStandardTagMasterResult>> DeleteTimeMarkStandardTagMaster(int EventTimeMarkStandardTagMasterID)
        {
            AuthorizedResponse<TimeMarkStandardTagMasterResult> Result = new AuthorizedResponse<TimeMarkStandardTagMasterResult>();
            using (EventEntities ctx = new EventEntities())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<TimeMarkStandardTagMaster> EventTimeMarkStandardTagList = ctx.TimeMarkStandardTagMasters.Where(p => p.TimeMarkStandardTagMasterID == EventTimeMarkStandardTagMasterID).ToList();
                        if (EventTimeMarkStandardTagList != null)
                        {
                            ctx.TimeMarkStandardTagMasters.RemoveRange(EventTimeMarkStandardTagList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new TimeMarkStandardTagMasterResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new TimeMarkStandardTagMasterResult();
            Result.Result.TimeMarkStandardTagMasteID = EventTimeMarkStandardTagMasterID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<List<AddEventViewModel>> GetAllEvent()
        {
            List<AddEventViewModel> eventList = new List<AddEventViewModel>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (Entities.Models.EventDetail eventDetail in ctx.EventDetails)
                {
                    AddEventViewModel eventModel = new AddEventViewModel();
                    eventModel.EventID = eventDetail.EventID;
                    eventModel.EventNumber = eventDetail.EventNumber;
                    eventModel.Status = eventDetail.Status;
                    eventModel.Gender = eventDetail.Gender;
                    eventModel.StartAgeRange = eventDetail.StartAgeRange == null ? 0 : eventDetail.StartAgeRange.Value;
                    eventModel.EndAgeRange = eventDetail.EndAgeRange == null ? 0 : eventDetail.EndAgeRange.Value;
                    eventModel.IsMultiAgeGroup = eventDetail.IsMultiAgeGroup == null ? false : eventDetail.IsMultiAgeGroup.Value;
                    eventModel.Division = eventDetail.Division;
                    eventModel.EventNotes = eventDetail.EventNotes;
                    eventModel.Entries = eventDetail.Entries;
                    eventModel.Results = eventDetail.Results;
                    eventModel.AgeGroup = eventDetail.AgeGroup;
                    eventModel.Distance = eventDetail.Distance == null ? 0 : eventDetail.Distance.Value;
                    eventModel.RunningEvent = eventDetail.Event;
                    eventModel.Advancement = eventDetail.Advance;
                    eventModel.HeatOrder = eventDetail.HeatOrder;
                    eventModel.NumberOfLane = eventDetail.NumberOfLane == null ? 0 : eventDetail.NumberOfLane.Value;
                    eventModel.HeadFlightAssign = eventDetail.Heats;
                    eventModel.LanePosAssignment = eventDetail.LinePosition;
                    eventModel.HeatInSemis = eventDetail.HeatInSemis == null ? 0 : eventDetail.HeatInSemis.Value;
                    eventModel.HeatInQtrs = eventDetail.HeatInQtrs == null ? 0 : eventDetail.HeatInQtrs.Value;
                    eventModel.IsScoreEvent = eventDetail.IsScoreEvent == null ? false : eventDetail.IsScoreEvent.Value;
                    eventModel.IsJumbOff = eventDetail.isJumbOff == null ? false : eventDetail.isJumbOff.Value;
                    eventModel.EntryFee = eventDetail.EntryFee == null ? 0.0 : eventDetail.EntryFee.Value;
                    eventModel.Type = eventDetail.Type;
                    eventModel.Rnds = eventDetail.Rnds;
                    eventModel.Finals = eventDetail.Finals;
                    eventModel.FinalNumberOfLanes = eventDetail.FinalNumberOfLanes == null ? 0 : eventDetail.FinalNumberOfLanes.Value;
                    eventModel.FinalOrder = eventDetail.FnalOrder;
                    eventModel.Round = eventDetail.Rnds;
                    eventModel.Event = eventDetail.Event;
                    if (eventModel.IsMultiAgeGroup)
                    {
                        eventModel.EventMultiAgeDetailList = new List<MultiAgeGroup>();
                        foreach (EventMultiAgeDetail multiAgeDetail in ctx.EventMultiAgeDetails.Where(p => p.EventID == eventModel.EventID))
                        {
                            MultiAgeGroup multiAge = new MultiAgeGroup();
                            multiAge.EventID = multiAgeDetail.EventID == null ? 0 : multiAgeDetail.EventID.Value;
                            multiAge.HighAge = multiAgeDetail.HighAge == null ? 0 : multiAgeDetail.HighAge.Value;
                            multiAge.LowAge = multiAgeDetail.LowAge == null ? 0 : multiAgeDetail.LowAge.Value;
                            multiAge.StandardID = multiAgeDetail.StandardId == null ? 0 : multiAgeDetail.StandardId.Value;
                            eventModel.EventMultiAgeDetailList.Add(multiAge);
                        }

                    }
                    eventList.Add(eventModel);
                }
            }
            return eventList;
        }

        public async Task<List<EventInMeet>> GetAllEventInMeet()
        {
            List<EventInMeet> EventInMeetList = new List<EventInMeet>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (EventInMeet eventInMeet in ctx.EventInMeets)
                {
                    EventInMeetList.Add(eventInMeet);
                }
            }
            return EventInMeetList;
        }

        public async Task<List<EventInSession>> GetAllEventInSession()
        {
            List<EventInSession> recordList = new List<EventInSession>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (EventInSession recordTag in ctx.EventInSessions)
                {
                    recordList.Add(recordTag);
                }
            }
            return recordList;
        }

        public async Task<List<RecordDetail>> GetAllRecord()
        {
            List<RecordDetail> recordList = new List<RecordDetail>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (Entities.Models.RecordDetail recordTag in ctx.RecordDetails)
                {
                    recordList.Add(recordTag);
                }
            }
            return recordList;
        }

        public async Task<List<RecordTag>> GetAllRecordTag()
        {
            List<RecordTag> recordTagList = new List<RecordTag>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (Entities.Models.RecordTag recordTag in ctx.RecordTags)
                {
                    recordTagList.Add(recordTag);
                }
            }
            return recordTagList;
        }

        public async Task<List<AddSessionViewModel>> GetAllSessions()
        {
            List<AddSessionViewModel> eventList = new List<AddSessionViewModel>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (EventSession eventDetail in ctx.EventSessions)
                {
                    AddSessionViewModel eventModel = new AddSessionViewModel();
                    eventModel.Day = eventDetail.Day == null ? 0 : eventDetail.Day.Value;
                    eventModel.EntryLimit = eventDetail.EntryLimit == null ? 0 : eventDetail.EntryLimit.Value;
                    eventModel.StartTime = eventDetail.StartTime;
                    eventModel.Session = eventDetail.Session;
                    eventModel.SessionTitle = eventDetail.SessionTitle;
                    eventModel.SessionID = eventDetail.EventSessionID;
                    eventList.Add(eventModel);
                }
            }
            return eventList;
        }

        public async Task<List<EventSessionSchedule>> GetAllSessionSchedules(int SessionID)
        {
            List<EventSessionSchedule> eventList = new List<EventSessionSchedule>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (EventSessionSchedule eventDetail in ctx.EventSessionSchedules.Where(p => p.EventSessionID == SessionID))
                {
                    eventList.Add(eventDetail);
                }
            }
            return eventList;
        }

        public async Task<List<EventSessionSchedule>> GetAllSessionSchedules()
        {
            List<EventSessionSchedule> eventList = new List<EventSessionSchedule>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (EventSessionSchedule eventDetail in ctx.EventSessionSchedules)
                {
                    eventList.Add(eventDetail);
                }
            }
            return eventList;
        }

        public async Task<List<EventTimeMarkStandard>> GetAllTimeMarkStandard()
        {
            List<EventTimeMarkStandard> EventTimeMarkStandardList = new List<EventTimeMarkStandard>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (EventTimeMarkStandard EventTimeMarkStandardTag in ctx.EventTimeMarkStandards)
                {
                    EventTimeMarkStandardList.Add(EventTimeMarkStandardTag);
                }
            }
            return EventTimeMarkStandardList;
        }

        public async Task<List<EventTimeMarkStandardTag>> GetAllTimeMarkStandardTag()
        {
            List<EventTimeMarkStandardTag> EventTimeMarkStandardList = new List<EventTimeMarkStandardTag>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (EventTimeMarkStandardTag EventTimeMarkStandardTag in ctx.EventTimeMarkStandardTags)
                {
                    EventTimeMarkStandardList.Add(EventTimeMarkStandardTag);
                }
            }
            return EventTimeMarkStandardList;
        }

        public async Task<List<TimeMarkStandardTagMaster>> GetAllTimeMarkStandardTagMaster()
        {
            List<TimeMarkStandardTagMaster> EventTimeMarkStandardList = new List<TimeMarkStandardTagMaster>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (TimeMarkStandardTagMaster EventTimeMarkStandardTag in ctx.TimeMarkStandardTagMasters)
                {
                    EventTimeMarkStandardList.Add(EventTimeMarkStandardTag);
                }
            }
            return EventTimeMarkStandardList;
        }

        public async Task<AuthorizedResponse<EventResult>> UpdateEvent(ServiceRequest_Event EventRequest)
        {
            using (EventEntities ctx = new EventEntities())
            {
                Entities.Models.EventDetail eventDetail = ctx.EventDetails.Where(p => p.EventID == EventRequest.Event.EventID).FirstOrDefault();
                if (eventDetail != null)
                {
                    eventDetail.EventNumber = EventRequest.Event.EventNumber;
                    eventDetail.Status = EventRequest.Event.Status;
                    eventDetail.Gender = EventRequest.Event.Gender;
                    eventDetail.StartAgeRange = EventRequest.Event.StartAgeRange;
                    eventDetail.EndAgeRange = EventRequest.Event.EndAgeRange;
                    eventDetail.IsMultiAgeGroup = EventRequest.Event.IsMultiAgeGroup;
                    eventDetail.Division = EventRequest.Event.Division;
                    eventDetail.EventNotes = EventRequest.Event.EventNotes;
                    eventDetail.Entries = EventRequest.Event.Entries;
                    eventDetail.Results = EventRequest.Event.Results;
                    eventDetail.AgeGroup = EventRequest.Event.AgeGroup;
                    eventDetail.Distance = EventRequest.Event.Distance;
                    eventDetail.Event = EventRequest.Event.RunningEvent;
                    eventDetail.Advance = EventRequest.Event.Advancement;
                    eventDetail.HeatOrder = EventRequest.Event.HeatOrder;
                    eventDetail.NumberOfLane = EventRequest.Event.NumberOfLane;
                    eventDetail.HeatInSemis = EventRequest.Event.HeatInSemis;
                    eventDetail.HeatInQtrs = EventRequest.Event.HeatInQtrs;
                    eventDetail.IsScoreEvent = EventRequest.Event.IsScoreEvent;
                    eventDetail.isJumbOff = EventRequest.Event.IsJumbOff;
                    eventDetail.EntryFee = EventRequest.Event.EntryFee;
                    eventDetail.Type = EventRequest.Event.Type;
                    eventDetail.Finals = EventRequest.Event.Finals;

                    eventDetail.FinalNumberOfLanes = EventRequest.Event.FinalNumberOfLanes;
                    eventDetail.FnalOrder = EventRequest.Event.FinalOrder;
                    eventDetail.NumberOfLane = EventRequest.Event.NumberOfLanes;
                    eventDetail.FinalNumberOfLanes = EventRequest.Event.FinalNumberOfLanes;
                    eventDetail.Heats = EventRequest.Event.HeadFlightAssign;
                    eventDetail.Rnds = EventRequest.Event.Round;
                    eventDetail.LinePosition = EventRequest.Event.LanePosAssignment;
                    if (EventRequest.Event.IsMultiAgeGroup)
                    {
                        List<EventMultiAgeDetail> eventDetailList = ctx.EventMultiAgeDetails.Where(p => p.EventID == eventDetail.EventID).ToList();
                        if (eventDetailList != null)
                        {
                            if (eventDetailList.Count() > 0)
                            {
                                ctx.EventMultiAgeDetails.RemoveRange(eventDetailList);
                            }
                        }
                        foreach (var eventRequest in EventRequest.Event.EventMultiAgeDetailList)
                        {
                            EventMultiAgeDetail multiAgeDetail = new EventMultiAgeDetail()
                            {
                                EventID = eventDetail.EventID,
                                HighAge = eventRequest.HighAge,
                                LowAge = eventRequest.LowAge,
                                StandardId = eventRequest.StandardID

                            };
                            ctx.EventMultiAgeDetails.Add(multiAgeDetail);
                            ctx.SaveChanges();
                        }
                    }

                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<EventResult> Result = new AuthorizedResponse<EventResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new EventResult();
            Result.Result.EventID = EventRequest.Event.EventID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<RecordUpdateResult>> UpdateRecord(ServiceRequest_RecordUpdate RecordUpdate)
        {
            using (EventEntities ctx = new EventEntities())
            {
                RecordDetail recordDetail = ctx.RecordDetails.Where(p => p.EventID == RecordUpdate.RecordUpdate.EventID).FirstOrDefault();
                if (recordDetail != null)
                {
                    recordDetail.Day = RecordUpdate.RecordUpdate.Day;
                    recordDetail.EndAgeRange = RecordUpdate.RecordUpdate.EndAgeRange;
                    recordDetail.EventID = RecordUpdate.RecordUpdate.EventID;
                    recordDetail.EventName = RecordUpdate.RecordUpdate.EventName;
                    recordDetail.EventType = RecordUpdate.RecordUpdate.EventType;
                    recordDetail.Gender = RecordUpdate.RecordUpdate.Gender;
                    recordDetail.Mark = RecordUpdate.RecordUpdate.Mark;
                    recordDetail.Month = RecordUpdate.RecordUpdate.Month;
                    recordDetail.RecordHolderName = RecordUpdate.RecordUpdate.RecordHolderName;
                    recordDetail.StartAgeRange = RecordUpdate.RecordUpdate.StartAgeRange;
                    recordDetail.TagName = RecordUpdate.RecordUpdate.TagName;
                    recordDetail.Year = RecordUpdate.RecordUpdate.Year;
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<RecordUpdateResult> Result = new AuthorizedResponse<RecordUpdateResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RecordUpdateResult();
            Result.Result.RecordUpdateID = RecordUpdate.RecordUpdateID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<RecordTagResult>> UpdateRecordTag(ServiceRequest_RecordTag RecordTag)
        {
            using (EventEntities ctx = new EventEntities())
            {
                Entities.Models.RecordTag recordTag = ctx.RecordTags.Where(p => p.RecordTagID == RecordTag.RecordTagID).FirstOrDefault();
                if (recordTag != null)
                {

                    recordTag.ExhOk = RecordTag.RecordTag.ExhOk;
                    recordTag.ForeignOk = RecordTag.RecordTag.ForeignOk;
                    recordTag.RecordOnlyFor = RecordTag.RecordTag.RecordOnlyFor;
                    recordTag.TagFlag = RecordTag.RecordTag.TagFlag;
                    recordTag.TagName = RecordTag.RecordTag.TagName;
                    recordTag.TagOrder = RecordTag.RecordTag.TagOrder;
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<RecordTagResult> Result = new AuthorizedResponse<RecordTagResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RecordTagResult();
            Result.Result.RecordTagID = RecordTag.RecordTag.RecordTagID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<SessionResult>> UpdateSession(ServiceRequest_Session SessionRequest)
        {
            using (EventEntities ctx = new EventEntities())
            {
                EventSession sessionDetail = ctx.EventSessions.Where(p => p.EventSessionID == SessionRequest.Session.SessionID).FirstOrDefault();
                if (sessionDetail != null)
                {
                    sessionDetail.Day = SessionRequest.Session.Day;
                    sessionDetail.EntryLimit = SessionRequest.Session.EntryLimit;
                    sessionDetail.Session = SessionRequest.Session.Session;
                    sessionDetail.SessionTitle = SessionRequest.Session.SessionTitle;
                    sessionDetail.StartTime = SessionRequest.Session.StartTime;
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<SessionResult> Result = new AuthorizedResponse<SessionResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new SessionResult();
            Result.Result.SessionID = SessionRequest.Session.SessionID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<SessionScheduleResult>> UpdateSessionSchedule(ServiceRequest_SessionSchedule SessionRequest)
        {
            using (EventEntities ctx = new EventEntities())
            {
                EventSessionSchedule sessionDetail = ctx.EventSessionSchedules.Where(p => p.SessionScheduleID == SessionRequest.Session.SessionScheduleID).FirstOrDefault();
                if (sessionDetail != null)
                {
                    sessionDetail.AmPM = SessionRequest.Session.AmPM;
                    sessionDetail.EventName = SessionRequest.Session.EventName;
                    sessionDetail.EventNumber = SessionRequest.Session.EventNumber;
                    sessionDetail.EventSessionID = SessionRequest.Session.EventSessionID;
                    sessionDetail.Report = SessionRequest.Session.Report;
                    sessionDetail.RND = SessionRequest.Session.RND;
                    sessionDetail.SessionOrder = SessionRequest.Session.SessionOrder;
                    sessionDetail.StartTime = SessionRequest.Session.StartTime;
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<SessionScheduleResult> Result = new AuthorizedResponse<SessionScheduleResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new SessionScheduleResult();
            Result.Result.SessionScheduleID = SessionRequest.Session.SessionScheduleID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<TimeMarkStandardTagResult>> UpdateTimeMarkStandardTag(ServiceRequest_TimeMarkStandardTag RecordTag)
        {
            using (EventEntities ctx = new EventEntities())
            {
                foreach (var TimeMarkStandardTag in RecordTag.SelectedTimeMarkStandardTagList)
                {

                    EventTimeMarkStandardTag EventTimeMarkStandardTagDetail = ctx.EventTimeMarkStandardTags.Where(p => p.TimeMarkStandardTagID == TimeMarkStandardTag.TimeMarkStandardTagID).FirstOrDefault();
                    if (EventTimeMarkStandardTagDetail != null)
                    {
                        EventTimeMarkStandardTagDetail.ForEntryQual = TimeMarkStandardTag.ForEntryQual;
                        EventTimeMarkStandardTagDetail.ForTimeMark = TimeMarkStandardTag.ForTimeMark;
                        EventTimeMarkStandardTagDetail.TimeMarkStandardEventName = TimeMarkStandardTag.TimeMarkStandardEventName;
                        EventTimeMarkStandardTagDetail.TimeMarkStandardTagDescription = TimeMarkStandardTag.TimeMarkStandardTagDescription;
                        EventTimeMarkStandardTagDetail.TimeMarkStandardTagID = TimeMarkStandardTag.TimeMarkStandardTagID;
                        EventTimeMarkStandardTagDetail.TimeMarkStandardTagName = TimeMarkStandardTag.TimeMarkStandardTagName;
                    }
                    ctx.SaveChanges();
                }
            }

            AuthorizedResponse<TimeMarkStandardTagResult> Result = new AuthorizedResponse<TimeMarkStandardTagResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new TimeMarkStandardTagResult();
            Result.Result.EventID = RecordTag.EventID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<TimeMarkStandardTagMasterResult>> UpdateTimeMarkStandardTagMaster(ServiceRequest_TimeMarkStandardTagMaster TimeMarkStandardTagMaster)
        {
            using (EventEntities ctx = new EventEntities())
            {
                TimeMarkStandardTagMaster EventTimeMarkStandardTagDetail = ctx.TimeMarkStandardTagMasters.Where(p => p.TimeMarkStandardTagMasterID == TimeMarkStandardTagMaster.TimeMarkStandardTag.TimeMarkStandardTagMasterID).FirstOrDefault();
                if (EventTimeMarkStandardTagDetail != null)
                {
                    EventTimeMarkStandardTagDetail.StandardPerpose = TimeMarkStandardTagMaster.TimeMarkStandardTag.StandardPerpose;
                    EventTimeMarkStandardTagDetail.StandardTagName = TimeMarkStandardTagMaster.TimeMarkStandardTag.StandardTagName;
                    EventTimeMarkStandardTagDetail.StandardTime = TimeMarkStandardTagMaster.TimeMarkStandardTag.StandardTime;
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<TimeMarkStandardTagMasterResult> Result = new AuthorizedResponse<TimeMarkStandardTagMasterResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new TimeMarkStandardTagMasterResult();
            Result.Result.TimeMarkStandardTagMasteID = TimeMarkStandardTagMaster.TimeMarkStandardTag.TimeMarkStandardTagMasterID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public void CreateMeetUsingTemplate(int templateMeetID, string meetName, string meetName2, DateTime? meetStatupDate, DateTime? meetEndDate, DateTime? meetAgeupDate)
        {
            using (EventEntities ctx = new EventEntities())
            {
                ctx.CreateMeetFromTemplate(templateMeetID, meetName, meetName2, meetStatupDate, meetEndDate, meetAgeupDate);
            }
        }
        public void CreateEventUsingTemplate(int meetID,int templateEventID, string eventNotes, int? startageRange,int? endageRange)
        {
            using (EventEntities ctx = new EventEntities())
            {
                ctx.CreateEventFromTemplate(meetID,templateEventID, eventNotes, startageRange, endageRange);
            }
        }
    }
}
