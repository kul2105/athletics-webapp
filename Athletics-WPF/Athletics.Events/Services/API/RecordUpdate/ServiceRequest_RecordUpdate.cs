﻿using Athletics.Entities.Models;
using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_RecordUpdate
    {
        public int RecordUpdateID { get; set; }
        public RecordDetail RecordUpdate { get; set; }
    }
}
