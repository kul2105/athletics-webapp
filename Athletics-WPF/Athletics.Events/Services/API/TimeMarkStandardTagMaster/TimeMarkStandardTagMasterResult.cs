﻿using System;
using System.Collections.Generic;

namespace Athletics.MeetSetup.Services.API
{
    public class TimeMarkStandardTagMasterResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
        public int TimeMarkStandardTagMasteID { get; set; }

    }
}
