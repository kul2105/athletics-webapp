﻿using Athletics.Entities.Models;
using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_TimeMarkStandardTagMaster
    {
        public int TimeMarkStandardTagMasterID { get; set; }
        public TimeMarkStandardTagMaster TimeMarkStandardTag { get; set; }
    }
}
