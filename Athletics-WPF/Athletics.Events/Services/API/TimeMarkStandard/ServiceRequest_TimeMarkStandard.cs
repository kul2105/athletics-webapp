﻿using Athletics.Entities.Models;
using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_TimeMarkStandard
    {
        public int RecordUpdateID { get; set; }
        public EventTimeMarkStandard TimeMarkStandard { get; set; }
    }
}
