﻿using System;
using System.Collections.Generic;
using Athletics.Athletes.SetupMeetManager;

namespace Athletics.MeetSetup.Services.API
{
    public class SessionScheduleResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
        public int SessionScheduleID { get; set; }

        public static implicit operator SessionScheduleResult(AuthorizedResponse<SessionScheduleResult> v)
        {
            throw new NotImplementedException();
        }
    }
}
