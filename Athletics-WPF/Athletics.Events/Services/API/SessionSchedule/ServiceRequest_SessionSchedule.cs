﻿using Athletics.Entities.Models;
using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_SessionSchedule
    {
        public int EventID { get; set; }
        public int SessionID { get; set; }
        public int SessionScheduleID { get; set; }
        public EventSessionSchedule Session { get; set; }
    }
}
