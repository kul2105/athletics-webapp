﻿using Athletics.Entities.Models;
using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_RecordTag
    {
        public int RecordTagID { get; set; }
        public RecordTag RecordTag { get; set; }
    }
}
