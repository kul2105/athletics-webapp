﻿using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_Event
    {
        public int MeetSetupID { get; set; }
        public AddEventViewModel Event { get; set; }
    }
}
