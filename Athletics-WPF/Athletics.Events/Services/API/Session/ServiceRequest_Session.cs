﻿using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_Session
    {
        public int SessionIDID { get; set; }
        public AddSessionViewModel Session { get; set; }
    }
}
