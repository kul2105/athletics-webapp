﻿using Athletics.Entities.Models;
using Athletics.Athletes.ViewModels;
using System.Collections.Generic;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_TimeMarkStandardTag
    {
        public int EventID { get; set; }
        public List<EventTimeMarkStandardTag> SelectedTimeMarkStandardTagList { get; set; }
    }
}
