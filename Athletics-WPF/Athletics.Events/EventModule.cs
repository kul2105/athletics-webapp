﻿using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.ViewModels;
using Athletics.Athletes.Views;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

namespace Athletics.Athletes
{
    public class EventModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public EventModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<IEventManager, EventManager>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageEventView, ManageEventView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddEventView, AddEventView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageEventViewModel, ManageEventViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddEventView, AddEventView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddEventViewModel, AddEventViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageSessionView, ManageSessionView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageSessionViewModel, ManageSessionViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IEventsMenuView, EventsMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IEventsMenuViewModel, EventsMenuViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<ISessionMenuView, SessionMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISessionMenuViewModel, SessionMenuViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<ITimeMarkStandardMenuView, TimeMarkStandardMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ITimeMarkStandardMenuViewModel, TimeMarkStandardMenuViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IRecordMenuView, RecordMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IRecordMenuViewModel, RecordMenuViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IManageSeedingView, ManageSeedingView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageSeedingViewModel, ManageSeedingViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<ISeedingMenuViewModel, SeedingMenuViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISeedingMenuView, SeedingMenuView>(new ContainerControlledLifetimeManager());


            this.container.RegisterType<IMultiAgeGroupViewModel, MultiAgeGroupViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IMultiAgeGroupView, MultiAgeGroupView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IRenumEventViewModel, RenumEventViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IRenumEventView, RenumEventView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IMeetSetupTemplateViewModel, MeetSetupTemplateViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IMeetSetupTemplateView, MeetSetupTemplateView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IEventTemplateViewModel, EventTemplateViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IEventTemplateView, EventTemplateView>(new ContainerControlledLifetimeManager());

            this.regionManager.RegisterViewWithRegion("EventMenuRegion", typeof(EventsMenuView));
            this.regionManager.RegisterViewWithRegion("TimeMarkMenuRegion", typeof(TimeMarkStandardMenuView));
            this.regionManager.RegisterViewWithRegion("SessionMenuRegion", typeof(SessionMenuView));
            this.regionManager.RegisterViewWithRegion("RecordMenuRegion", typeof(RecordMenuView));
            this.regionManager.RegisterViewWithRegion("SeedingMenuRegion", typeof(SeedingMenuView));

        }
    }
}
