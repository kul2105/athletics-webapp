﻿namespace Athletics.Athletes.Views
{
    public interface IAddRecordTagView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
