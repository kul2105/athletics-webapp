﻿namespace Athletics.Athletes.Views
{
    public interface IManageRecordView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
