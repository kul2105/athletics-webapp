﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class RecordMenuView : UserControl, IRecordMenuView
    {
        public RecordMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
