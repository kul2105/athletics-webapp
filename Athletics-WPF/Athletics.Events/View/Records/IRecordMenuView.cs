﻿namespace Athletics.Athletes.Views
{
    public interface IRecordMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
