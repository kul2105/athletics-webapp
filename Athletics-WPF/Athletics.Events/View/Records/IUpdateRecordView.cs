﻿namespace Athletics.Athletes.Views
{
    public interface IUpdateRecordView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
