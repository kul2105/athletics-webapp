﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class EventsMenuView : UserControl, IEventsMenuView
    {
        public EventsMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
