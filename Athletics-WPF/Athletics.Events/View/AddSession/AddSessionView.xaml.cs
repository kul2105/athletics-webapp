﻿using Athletics.Athletes.ViewModels;
using Athletics.ControlLibrary;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddSessionView : Window, IAddSessionView
    {
        public AddSessionView()
        {
            InitializeComponent();

        }
        public void ResetFocus()
        {
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddSessionViewModel vm = this.DataContext as AddSessionViewModel;
            if (vm != null)
            {
                vm.IsCanceled = true;
            }
            PART_CLOSE_Click(null, e);
        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }

        private void TimePicker_TimeChanged(object sender, RoutedEventArgs e)
        {
            TimePicker picker = sender as TimePicker;
            if(picker!=null)
            {
                AddSessionViewModel addSessionVm = this.DataContext as AddSessionViewModel;
                if(addSessionVm!=null)
                {
                    addSessionVm.StartTime = picker.SelectedTime;
                }
            }
        }
    }
}
