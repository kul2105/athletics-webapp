﻿namespace Athletics.Athletes.Views
{
    public interface IAddSessionView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
