﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class TimeMarkStandardMenuView : UserControl, ITimeMarkStandardMenuView
    {
        public TimeMarkStandardMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
