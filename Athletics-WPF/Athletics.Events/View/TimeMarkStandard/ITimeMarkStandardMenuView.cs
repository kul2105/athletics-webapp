﻿namespace Athletics.Athletes.Views
{
    public interface ITimeMarkStandardMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
