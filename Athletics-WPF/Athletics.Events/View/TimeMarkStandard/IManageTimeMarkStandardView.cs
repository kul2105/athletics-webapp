﻿namespace Athletics.Athletes.Views
{
    public interface IManageTimeMarkStandardView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
