﻿namespace Athletics.Athletes.Views
{
    public interface IAddTimeMarkStandardTagView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
