﻿namespace Athletics.Athletes.Views
{
    public interface ICommentsView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
