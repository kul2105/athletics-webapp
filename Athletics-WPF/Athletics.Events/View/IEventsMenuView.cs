﻿namespace Athletics.Athletes.Views
{
    public interface IEventsMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
