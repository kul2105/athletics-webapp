﻿namespace Athletics.Athletes.Views
{
    public interface IEventTemplateView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
