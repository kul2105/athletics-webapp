﻿namespace Athletics.Athletes.Views
{
    public interface IAddEventView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
