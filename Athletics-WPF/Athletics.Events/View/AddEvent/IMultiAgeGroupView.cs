﻿namespace Athletics.Athletes.Views
{
    public interface IMultiAgeGroupView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
