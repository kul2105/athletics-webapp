﻿namespace Athletics.Athletes.Views
{
    public interface IMeetSetupTemplateView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
