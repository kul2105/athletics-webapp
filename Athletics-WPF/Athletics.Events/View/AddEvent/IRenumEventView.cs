﻿namespace Athletics.Athletes.Views
{
    public interface IRenumEventView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
