﻿using Athletics.Athletes.ViewModels;
using Athletics.ControlLibrary.DataGrid;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.Views
{
    public interface IManageSeedingView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
