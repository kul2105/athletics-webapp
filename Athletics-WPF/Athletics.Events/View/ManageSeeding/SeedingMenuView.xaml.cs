﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class SeedingMenuView : UserControl, ISeedingMenuView
    {
        public SeedingMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
