﻿namespace Athletics.Athletes.Views
{
    public interface ISeedingMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
