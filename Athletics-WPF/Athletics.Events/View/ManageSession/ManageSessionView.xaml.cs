﻿using Athletics.Entities.Models;
using Athletics.Athletes.Common;
using Athletics.Athletes.ViewModels;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using tAthletics.Events.Views;
using Athletics.ControlLibrary.DataGrid;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ManageSessionView : Window, IManageSessionView
    {
        #region DraggedItem

        /// <summary>
        /// DraggedItem Dependency Property
        /// </summary>
        public static readonly DependencyProperty DraggedItemProperty =
            DependencyProperty.Register("DraggedItem", typeof(EventSessionSchedule), typeof(ManageSessionView));

        /// <summary>
        /// Gets or sets the DraggedItem property.  This dependency property 
        /// indicates ....
        /// </summary>
        public EventSessionSchedule DraggedItem
        {
            get { return (EventSessionSchedule)GetValue(DraggedItemProperty); }
            set { SetValue(DraggedItemProperty, value); }
        }

        #endregion
        public ManageSessionView()
        {
            InitializeComponent();
            //this.DataContext = new CreateReportViewModel(propertyGrid, listBoxMiddle);

        }
        public void ResetFocus()
        {
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);
        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }


        private void DataGrid1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ManageSessionViewModel vm = this.DataContext as ManageSessionViewModel;
            if (vm != null)
            {
                vm.AddEventToSchedule(vm.SelectedEventDetail);
                //vm.GetSessionSchedule();
            }
        }

        private void DataGrid2_Drop(object sender, DragEventArgs e)
        {
            ViewModels.EventDetail eventDetail = e.Data.GetData(typeof(ViewModels.EventDetail)) as ViewModels.EventDetail;
            if (eventDetail != null)
            {
                GlobalEvent.RaiseTargetControlLoaded(eventDetail);
            }

        }
        public bool IsDragging { get; set; }
        private void ResetDragDrop()
        {
            IsDragging = false;
            popup1.IsOpen = false;
            ScheduleGrid.IsReadOnly = false;
        }
        public bool IsEditing { get; set; }
        public AthleticsGrid ScheduledGrid { get => this.ScheduleGrid; set => this.ScheduleGrid = value; }

        private void DataGrid3_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            IsEditing = true;
            //in case we are in the middle of a drag/drop operation, cancel it...
            if (IsDragging) ResetDragDrop();
        }
        bool IsNumber(string s)
        {
            return s.Any() && s.All(c => Char.IsDigit(c));
        }
        private void DataGrid3_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            EventSessionSchedule editeditem = e.Row.DataContext as EventSessionSchedule;
            EventSessionSchedule targetItem = null;
            if (editeditem != null)
            {

                ManageSessionViewModel vm = this.DataContext as ManageSessionViewModel;
                int ChangedOrderValue = 0;
                string startTime = string.Empty;
                if (vm != null)
                {
                    if (e.Column.Header.ToString() == "Order")
                    {
                        ChangedOrderValue = System.Convert.ToInt32(((TextBox)e.EditingElement).Text);
                        targetItem = vm.sessionSheduleAll.Where(p => p.SessionOrder == ChangedOrderValue).FirstOrDefault();
                    }
                    if (e.Column.Header.ToString() == "Start")
                    {
                        startTime = ((TextBox)e.EditingElement).Text;
                        if (string.IsNullOrEmpty(startTime))
                        {
                            MessageBox.Show("Time should not be empty");
                            return;
                        }
                        char[] delimiters = new char[] { ':' };
                        string[] timeFormate = startTime.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (timeFormate.Count() != 2)
                        {
                            MessageBox.Show("Time is not in proper formate.Suggested Time Format is 12:30.");
                            return;
                        }

                        else if (!IsNumber(timeFormate[0]))
                        {
                            MessageBox.Show("Please enter valid Number in time.");
                            return;
                        }
                        else if (!IsNumber(timeFormate[1]))
                        {
                            MessageBox.Show("Please enter valid Number in time.");
                            return;
                        }
                        else if (Convert.ToInt32(timeFormate[0]) > 12)
                        {
                            MessageBox.Show("Please enter in 12 Hrs formate.");
                            return;
                        }
                        else if (Convert.ToInt32(timeFormate[1]) >= 60)
                        {
                            MessageBox.Show("Not a valid minutes please enter less than or equal 59");
                            return;
                        }
                        //if (Convert.ToInt32(timeFormate[0]) >= 12)
                        //{
                        //    editeditem.AmPM = "PM";
                        //}
                        //else
                        //{
                        //    editeditem.AmPM = "AM";
                        //}
                        editeditem.StartTime = startTime;
                    }
                    if (e.Column.Header.ToString() == "AM/PM")
                    {
                        string ampmString = ((TextBox)e.EditingElement).Text;
                        if (string.IsNullOrEmpty(ampmString))
                        {
                            MessageBox.Show("AM/PM should not be empty");
                            return;
                        }
                        if (ampmString.ToUpper() != "AM" && ampmString.ToUpper() != "PM")
                        {
                            MessageBox.Show("AM/PM should not enter AM or PM");
                            return;
                        }
                        editeditem.AmPM = ampmString;
                    }
                    if (targetItem != null)
                    {
                        int draggedItemsSessionOrder = editeditem.SessionOrder.Value;
                        string draggedItemTime = editeditem.StartTime;
                        string draggedItemAMPM = editeditem.AmPM;
                        DraggedItem.SessionOrder = targetItem.SessionOrder.Value;
                        DraggedItem.StartTime = targetItem.StartTime;
                        DraggedItem.AmPM = targetItem.AmPM;
                        targetItem.SessionOrder = draggedItemsSessionOrder;
                        targetItem.StartTime = draggedItemTime;
                        targetItem.AmPM = draggedItemAMPM;
                        if (vm != null)
                        {
                            vm.UpdateSessionSchedule(targetItem);
                        }

                    }
                    vm.UpdateSessionSchedule(editeditem);
                    vm.GetSessionSchedule();
                }
            }

            IsEditing = false;
        }

        private void DataGrid3_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (IsEditing) return;
            var row = UIHelpers.TryFindFromPoint<DataGridRow>((UIElement)sender, e.GetPosition(ScheduleGrid));
            if (row == null || row.IsEditing) return;

            //set flag that indicates we're capturing mouse movements
            IsDragging = true;
            DraggedItem = (EventSessionSchedule)row.Item;//data retrievement
        }
        /// <summary>
        /// Completes a drag/drop operation.
        /// </summary>
        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!IsDragging || IsEditing)
            {
                return;
            }

            //get the target item
            EventSessionSchedule targetItem = (EventSessionSchedule)ScheduleGrid.SelectedItem;

            if (targetItem == null || !ReferenceEquals(DraggedItem, targetItem))
            {
                int draggedItemsSessionOrder = DraggedItem.SessionOrder.Value;
                string draggedItemTime = DraggedItem.StartTime;
                string draggedItemAMPM = DraggedItem.AmPM;
                DraggedItem.SessionOrder = targetItem.SessionOrder.Value;
                DraggedItem.StartTime = targetItem.StartTime;
                DraggedItem.AmPM = targetItem.AmPM;
                targetItem.SessionOrder = draggedItemsSessionOrder;
                targetItem.StartTime = draggedItemTime;
                targetItem.AmPM = draggedItemAMPM;
                ManageSessionViewModel vm = this.DataContext as ManageSessionViewModel;
                if (vm != null)
                {
                    vm.UpdateSessionSchedule(DraggedItem);
                    vm.UpdateSessionSchedule(targetItem);
                }
                vm.GetSessionSchedule();
                ScheduleGrid.SelectedItem = DraggedItem;
            }

            //reset
            ResetDragDrop();
        }
        /// <summary>
        /// Updates the popup's position in case of a drag/drop operation.
        /// </summary>
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!IsDragging || e.LeftButton != MouseButtonState.Pressed) return;

            //display the popup if it hasn't been opened yet
            if (!popup1.IsOpen)
            {
                //switch to read-only mode
                ScheduleGrid.IsReadOnly = true;

                //make sure the popup is visible
                popup1.IsOpen = true;
            }


            Size popupSize = new Size(popup1.ActualWidth, popup1.ActualHeight);
            popup1.PlacementRectangle = new Rect(e.GetPosition(this.ScheduleGrid), popupSize);

            //make sure the row under the grid is being selected
            Point position = e.GetPosition(ScheduleGrid);
            var row = UIHelpers.TryFindFromPoint<DataGridRow>(ScheduleGrid, position);
            if (row != null) ScheduleGrid.SelectedItem = row.Item;
        }

    }
}
