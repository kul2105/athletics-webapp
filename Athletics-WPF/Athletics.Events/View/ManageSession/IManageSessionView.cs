﻿using Athletics.ControlLibrary.DataGrid;

namespace Athletics.Athletes.Views
{
    public interface IManageSessionView
    {
        object DataContext { get; set; }
        void ResetFocus();
        AthleticsGrid ScheduledGrid { get; set; }
    }

}
