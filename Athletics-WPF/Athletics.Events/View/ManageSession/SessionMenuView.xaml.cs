﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class SessionMenuView : UserControl, ISessionMenuView
    {
        public SessionMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
