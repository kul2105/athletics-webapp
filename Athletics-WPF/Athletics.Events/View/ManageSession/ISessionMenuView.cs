﻿namespace Athletics.Athletes.Views
{
    public interface ISessionMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
