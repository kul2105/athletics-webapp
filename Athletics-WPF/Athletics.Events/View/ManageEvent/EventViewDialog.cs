﻿using Athletics.ControlLibrary.DialogWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    public class EventViewDialog : IModalDialog
    {
        private  Window view;

        void IModalDialog.BindViewModel<TViewModel>(TViewModel viewModel)
        {
            //().DataContext = viewModel;
            GetDialog().DataContext = viewModel;
        }

        void IModalDialog.ShowDialog()
        {
            GetDialog().ShowDialog();
        }

        void IModalDialog.Close()
        {
           
            // GetDialog().Close();
            view = null;
        }

        private Window GetDialog()
        {
            if (view == null)
            {
                //create the view if the view does not exist
                view = new Window();
                view.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                view.Closed += new EventHandler(view_Closed);
            }
            return view;
        }

        void view_Closed(object sender, EventArgs e)
        {
            view = null;
        }

        public void BindView(Window userControl)
        {
            view = userControl;
        }
    }
}
