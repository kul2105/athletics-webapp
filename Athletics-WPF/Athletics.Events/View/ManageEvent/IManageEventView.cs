﻿using Athletics.ControlLibrary.DataGrid;

namespace Athletics.Athletes.Views
{
    public interface IManageEventView
    {
        AthleticsGrid EventGrid { get; }
        object DataContext { get; set; }
        void ResetFocus();
    }

}
