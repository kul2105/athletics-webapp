﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Athletics.Athletes.Model
{
    public class EventMenuItemViewModel
    {
        private ICommand _command;
        public EventMenuItemViewModel()
        {
            MenuItems = new ObservableCollection<EventMenuItemViewModel>();
        }

        public string Header { get; set; }
        public int MenuID { get; set; }
        public ObservableCollection<EventMenuItemViewModel> MenuItems { get; set; }

        public ICommand Command
        {
            get
            {
                return _command;
            }
            set
            {
                _command = value;
            }
        }
    }
    public class MeetCommandViewModel : ICommand
    {
        private readonly Action _action;

        public MeetCommandViewModel(Action action)
        {
            _action = action;
        }

        public void Execute(object o)
        {
            _action();
        }

        public bool CanExecute(object o)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { }
            remove { }
        }
    }
}
