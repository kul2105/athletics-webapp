﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{

    public class CommentViewModel : BindableBase, ICommentViewModel, IGenericInteractionView<CommentViewModel>
    {
        private IAddEventView View = null;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public CommentViewModel()
        {
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "commentView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "commentView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

        }

        #region Properties
        private string _Comment1;
        public string Comment1
        {
            get { return _Comment1; }
            set
            {
                SetProperty(ref _Comment1, value);
            }
        }
        private string _Comment2;
        public string Comment2
        {
            get { return _Comment2; }
            set
            {
                SetProperty(ref _Comment2, value);
            }
        }
        private string _Comment3;
        public string Comment3
        {
            get { return _Comment3; }
            set
            {
                SetProperty(ref _Comment3, value);
            }
        }
        private string _Comment4;
        public string Comment4
        {
            get { return _Comment4; }
            set
            {
                SetProperty(ref _Comment4, value);
            }
        }
        private bool _IsAddSponser;
        public bool IsAddSponser
        {
            get { return _IsAddSponser; }
            set
            {
                SetProperty(ref _IsAddSponser, value);
            }
        }

        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(CommentViewModel entity)
        {


        }
        public CommentViewModel GetEntity()
        {

            return this;
        }


        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }
        #endregion
    }

}

