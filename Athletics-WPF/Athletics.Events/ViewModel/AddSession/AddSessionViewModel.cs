﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{

    public class AddSessionViewModel : BindableBase, IAddSessionViewModel, IGenericInteractionView<AddSessionViewModel>
    {
        AddSessionView sessionView = null;
        public AddSessionViewModel()
        {

        }
        public AddSessionViewModel(Window win)
        {
            sessionView = win as AddSessionView;
            InitilizedEvents(win);

        }

        public void InitilizedEvents(Window window)
        {
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                this.IsCanceled = false;
                                window.Close();

                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                window.Close();
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });
            ZoneList = new List<string>();
            ZoneList.Add("AM");
            ZoneList.Add("PM");
        }


        #region Form Properties

        private bool _IsCanceled = true;
        public bool IsCanceled
        {
            get { return _IsCanceled; }
            set
            {
                SetProperty(ref _IsCanceled, value);
            }
        }

        private int _SessionID;
        public int SessionID
        {
            get { return _SessionID; }
            set
            {
                SetProperty(ref _SessionID, value);
            }
        }

        private int _Day;
        public int Day
        {
            get { return _Day; }
            set
            {
                SetProperty(ref _Day, value);
            }
        }

        private string _StartTime;
        public string StartTime
        {
            get { return _StartTime; }
            set
            {
                SetProperty(ref _StartTime, value);
                //if(sessionView!=null)
                //{
                //    //sessionView.SessionTime.SelectedTime = value;
                //    sessionView.SessionTime.CurrentTime = Convert.ToDateTime(value);
                //}
            }
        }

        private int _EntryLimit;
        public int EntryLimit
        {
            get { return _EntryLimit; }
            set
            {
                SetProperty(ref _EntryLimit, value);
            }
        }

        private string _Session;
        public string Session
        {
            get { return _Session; }
            set
            {
                SetProperty(ref _Session, value);
            }
        }

        private string _SessionTitle;
        public string SessionTitle
        {
            get { return _SessionTitle; }
            set
            {
                SetProperty(ref _SessionTitle, value);
            }
        }
        private List<string> _ZoneList;
        public List<string> ZoneList
        {
            get { return _ZoneList; }
            set
            {
                SetProperty(ref _ZoneList, value);
            }
        }
        private string _SelectedTimeZone;
        public string SelectedTimeZone
        {
            get { return _SelectedTimeZone; }
            set
            {
                SetProperty(ref _SelectedTimeZone, value);
            }
        }
        private string _Hrs;
        public string Hrs
        {
            get { return _Hrs; }
            set
            {
                SetProperty(ref _Hrs, value);
            }
        }
        private string _Mins;
        public string Mins
        {
            get { return _Mins; }
            set
            {
                SetProperty(ref _Mins, value);
            }
        }
        private bool _IsSelected;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                SetProperty(ref _IsSelected, value);
            }
        }
        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(AddSessionViewModel entity)
        {
            if (entity == null) return;
            this.Day = entity.Day;
            this.EntryLimit = entity.EntryLimit;
            this.Session = entity.Session;
            this.SessionID = entity.SessionID;
            this.SessionTitle = entity.SessionTitle;
            this.StartTime = entity.StartTime;
            string[] strTime = StartTime.Split(new char[] { ':' });
            this.Hrs = strTime[0];
            string[] spliteSecond = strTime[1].Split(new char[] { ' ' });
            this.Hrs = strTime[0];
            this.Mins = spliteSecond[0];
            this.SelectedTimeZone = spliteSecond[1];
        }
        public AddSessionViewModel GetEntity()
        {
            this.StartTime = string.Format("{0}:{1} {2}", this.Hrs, this.Mins, this.SelectedTimeZone);
            return this;
        }


        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        #endregion
    }

}

