﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{

    public class AddRecordTagViewModel : BindableBase, IAddRecordTagViewModel
    {
        public AddRecordTagViewModel()
        {
            RecordOnlyForList = new List<string>();
            RecordOnlyForList.Add("Team");
            RecordOnlyForList.Add("Other");
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "AddRecordTagView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "AddRecordTagView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

        }

        #region Properties

        private RecordTag _SelectedRecordTag;
        public RecordTag SelectedRecordTag
        {
            get { return _SelectedRecordTag; }
            set
            {
                if (value == null)
                {
                    value = new RecordTag();
                    SetProperty(ref _SelectedRecordTag, value);
                }
                else
                {
                    SetProperty(ref _SelectedRecordTag, value);
                    RecordTagID = value.RecordTagID;
                    TagOrder = value.TagOrder == null ? 1 : value.TagOrder.Value;
                    TagName = value.TagName;
                    TagFlag = value.TagFlag;
                    RecordOnlyFor = value.RecordOnlyFor;
                    ExhOk = value.ExhOk == null ? false : value.ExhOk.Value;
                    ForeignOk = value.ForeignOk == null ? false : value.ForeignOk.Value;
                }

            }
        }
        private List<string> _RecordOnlyForList;
        public List<string> RecordOnlyForList
        {
            get { return _RecordOnlyForList; }
            set
            {
                SetProperty(ref _RecordOnlyForList, value);
            }
        }

        private int _RecordTagID;
        public int RecordTagID
        {
            get { return _RecordTagID; }
            set
            {
                SetProperty(ref _RecordTagID, value);
            }
        }
        private int _TagOrder;
        public int TagOrder
        {
            get { return _TagOrder; }
            set
            {
                SetProperty(ref _TagOrder, value);
                if(SelectedRecordTag==null)
                {
                    SelectedRecordTag = new RecordTag();
                }
                SelectedRecordTag.TagOrder = value;
            }
        }
        private string _TagName;
        public string TagName
        {
            get { return _TagName; }
            set
            {
                SetProperty(ref _TagName, value);
                if (SelectedRecordTag == null)
                {
                    SelectedRecordTag = new RecordTag();
                }
                SelectedRecordTag.TagName = value;
            }
        }
        private string _TagFlag;
        public string TagFlag
        {
            get { return _TagFlag; }
            set
            {
                SetProperty(ref _TagFlag, value);
                if (SelectedRecordTag == null)
                {
                    SelectedRecordTag = new RecordTag();
                }
                SelectedRecordTag.TagFlag = value;
            }
        }
        private string _RecordOnlyFor;
        public string RecordOnlyFor
        {
            get { return _RecordOnlyFor; }
            set
            {
                SetProperty(ref _RecordOnlyFor, value);
                if (SelectedRecordTag == null)
                {
                    SelectedRecordTag = new RecordTag();
                }
                SelectedRecordTag.RecordOnlyFor = value;
            }
        }
        private bool _ExhOk;
        public bool ExhOk
        {
            get { return _ExhOk; }
            set
            {
                SetProperty(ref _ExhOk, value);
                if (SelectedRecordTag == null)
                {
                    SelectedRecordTag = new RecordTag();
                }
                SelectedRecordTag.ExhOk = value;
            }
        }

        private bool _ForeignOk;
        public bool ForeignOk
        {
            get { return _ForeignOk; }
            set
            {
                SetProperty(ref _ForeignOk, value);
                if (SelectedRecordTag == null)
                {
                    SelectedRecordTag = new RecordTag();
                }
                SelectedRecordTag.ForeignOk = value;
            }
        }
        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }
        #endregion
    }

}

