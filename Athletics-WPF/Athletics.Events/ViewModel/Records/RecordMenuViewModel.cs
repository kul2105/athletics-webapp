﻿using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Athletics.Report.Views.ViewReport;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class RecordMenuViewModel : BindableBase, IRecordMenuViewModel
    {
        private ObservableCollection<EventMenuItemViewModel> _MenuItems = new ObservableCollection<EventMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IEventManager eventManager;
        IManageRecordView View;
        IManageRecordViewModel ViewModel;
        IRecordMenuView SessionMenuView;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public RecordMenuViewModel(IUnityContainer unity, IRecordMenuView _View, IManageRecordView ManageSessionView, IEventManager manager)
        {
            eventManager = manager;
            View = ManageSessionView;
            ViewModel = ManageSessionView.DataContext as IManageRecordViewModel;
            SessionMenuView = _View;
            SessionMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<EventMenuItemViewModel>();
                EventMenuItemViewModel eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Import";
                _ImportToCommand = new MeetCommandViewModel(ImportToCommandHandler);
                eventMenu.Command = ImportToCommand;
                MenuItems.Add(eventMenu);
                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Export";
                _ExportToCommand = new MeetCommandViewModel(ExportToCommandHandler);
                eventMenu.Command = ImportToCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Update";
                _ShowCommand = new MeetCommandViewModel(UpdateCommandCommandHandler);
                eventMenu.Command = ShowCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Refresh";
                _RefreshCommand = new MeetCommandViewModel(RefreshCommandCommandHandler);
                eventMenu.Command = RefreshCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "View";
                _ShowCommand = new MeetCommandViewModel(RefreshCommandCommandHandler);
                eventMenu.Command = ShowCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Create Report";
                _CreateReportCommand = new MeetCommandViewModel(CreateReportCommandCommandHandler);
                eventMenu.Command = CreateReportCommand;
                MenuItems.Add(eventMenu);
                #endregion

            });
            unityContainer = unity;
            //View.DataContext = this;

        }
        #region Menu Command

        private ICommand _ImportToCommand;
        public ICommand ImportToCommand
        {
            get { return _ImportToCommand; }
        }

        private ICommand _ExportToCommand;
        public ICommand ExportToCommand
        {
            get { return _ExportToCommand; }
        }
        private ICommand _RefreshCommand;
        public ICommand RefreshCommand
        {
            get { return _RefreshCommand; }
        }

        private ICommand _CreateReportCommand;
        public ICommand CreateReportCommand
        {
            get { return _CreateReportCommand; }
        }

        private ICommand _ShowCommand;
        public ICommand ShowCommand
        {
            get { return _ShowCommand; }
        }
        #endregion

        #region Command Handler
        private void CreateReportCommandCommandHandler()
        {
            DataTable dt = ReportTemplateGenerator.ToDataTable<RecordDetail>(ViewModel.ManageRecordList);
            if (dt != null)
            {
                string ReportTemplate = ReportTemplateGenerator.GenerateTemplate("Record List", "Record List", ReportTemplateGenerator.GetColumnList<RecordDetail>());
                Window window = new ReportView(ReportTemplate, dt);
                window.ShowDialog();
            }
        }
        private void RefreshCommandCommandHandler()
        {
            ViewModel.ManageRecordList = ViewModel.ManageRecordListAll;
        }

        private void UpdateCommandCommandHandler()
        {
            if (ViewModel.ManageRecordList != null)
            {
                UpdateRecordView updateRecord = new UpdateRecordView(ViewModel.ManageRecordList, eventManager);
                updateRecord.Name = "UpdateRecordView";
                updateRecord.ShowDialog();
                updateRecord.Close();
                updateRecord = null;
            }
            else
            {
                MessageBox.Show("Please export the record first", "No Record exist to update");
            }
        }
        private void ExportToCommandHandler()
        {
            ExportToExcel<RecordDetail, List<RecordDetail>> s = new ExportToExcel<RecordDetail, List<RecordDetail>>();
            s.dataToPrint = ViewModel.ManageRecordList;
            s.GenerateReport();
        }

        private void ImportToCommandHandler()
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.DefaultExt = ".xlsx";
            openfile.Filter = "(.xlsx)|*.xlsx";
            var browsefile = openfile.ShowDialog();
            if (browsefile == true)
            {
                DataTable dt = ExportFromExcel.ExportFrom(openfile.FileName);
                ViewModel.ManageRecordListAll = new List<RecordDetail>();
                foreach (DataRow item in dt.Rows)
                {
                    RecordDetail recordList = new RecordDetail();
                    recordList.EventID = Convert.ToInt32(item.ItemArray[0]);
                    recordList.EventName = Convert.ToString(item.ItemArray[1]);
                    recordList.TagName = Convert.ToString(item.ItemArray[2]);
                    recordList.Mark = Convert.ToString(item.ItemArray[3]);
                    recordList.Year = Convert.ToInt16(item.ItemArray[4]);
                    recordList.Month = Convert.ToInt16(item.ItemArray[5]);
                    recordList.Day = Convert.ToInt16(item.ItemArray[6]);
                    recordList.RecordHolderName = Convert.ToString(item.ItemArray[7]);
                    recordList.Gender = Convert.ToString(item.ItemArray[8]);
                    recordList.StartAgeRange = Convert.ToInt16(item.ItemArray[9]);
                    recordList.EndAgeRange = Convert.ToInt16(item.ItemArray[10]);
                    recordList.EventType = Convert.ToString(item.ItemArray[11]);
                    ViewModel.ManageRecordListAll.Add(recordList);
                }
                ViewModel.ManageRecordList = ViewModel.ManageRecordListAll;
                // recordGrid.ItemsSource = dt.DefaultView;
            }
        }


        #endregion

    }
}

