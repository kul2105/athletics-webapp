﻿using Athletics.Entities.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IManageRecordViewModel
    {
        List<RecordDetail> ManageRecordList { get; set; }
        List<RecordDetail> ManageRecordListAll { get; set; }
    }
}
