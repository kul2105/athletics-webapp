﻿using Athletics.ControlLibrary.DataGrid;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Athletics.Report.Views.ViewReport;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{

    public class ManageRecordViewModel : BindableBase, IManageRecordViewModel, IGenericInteractionView<ManageRecordViewModel>
    {
        private IEventManager EventManager;
        AthleticsGrid recordGrid;
        public ManageRecordViewModel(AthleticsGrid datagridRecord)
        {
            recordGrid = datagridRecord;
            EventManager = new SetupMeetManager.EventManager();
            //#region Initilize Command
            //MenuItems = new ObservableCollection<EventMenuItemViewModel>();
            //EventMenuItemViewModel eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "Import";
            //_ImportToCommand = new MeetCommandViewModel(ImportToCommandHandler);
            //eventMenu.Command = ImportToCommand;
            //MenuItems.Add(eventMenu);
            //eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "Export";
            //_ImportToCommand = new MeetCommandViewModel(ExportToCommandHandler);
            //eventMenu.Command = ImportToCommand;
            //MenuItems.Add(eventMenu);

            //eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "Update";
            //_ShowCommand = new MeetCommandViewModel(UpdateCommandCommandHandler);
            //eventMenu.Command = ShowCommand;
            //MenuItems.Add(eventMenu);

            //eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "Refresh";
            //_RefreshCommand = new MeetCommandViewModel(RefreshCommandCommandHandler);
            //eventMenu.Command = RefreshCommand;
            //MenuItems.Add(eventMenu);

            //eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "View";
            //_ShowCommand = new MeetCommandViewModel(RefreshCommandCommandHandler);
            //eventMenu.Command = ShowCommand;
            //MenuItems.Add(eventMenu);

            //eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "Create Report";
            //_CreateReportCommand = new MeetCommandViewModel(CreateReportCommandCommandHandler);
            //eventMenu.Command = CreateReportCommand;
            //MenuItems.Add(eventMenu);
            //#endregion
            _ImportToCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            ImportToCommandHandler();
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _ExportToCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            ExportToCommandHandler();
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

            _AddTagCommand = new DelegateCommand(async () =>
            {
                await Task.Run(async () =>
                {
                    await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        AddRecordTagView window = new AddRecordTagView();
                        AddRecordTagViewModel vm = window.DataContext as AddRecordTagViewModel;
                        //if (vm != null)
                        //{
                        //    vm.SelectedRecordTag = SelectedRecordTag;
                        //}
                        window.Name = "AddRecordTagView";
                        window.ShowDialog();
                        window.Close();
                        window = null;
                        SelectedRecordTag = vm.SelectedRecordTag;
                        if (SelectedRecordTag == null)
                        {
                            return;
                        }
                        await Task.Run(async () =>
                         {
                             Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                             {
                                 await Task.Run(async () =>
                                 {
                                     ServiceRequest_RecordTag serviceObj = new ServiceRequest_RecordTag();
                                     serviceObj.RecordTag = SelectedRecordTag;
                                     AuthorizedResponse<RecordTagResult> Response = await EventManager.CreateRecordTag(serviceObj);
                                     if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                                     {
                                         MessageBox.Show("Error while adding the Tag", "Tag Add Error");
                                         return;
                                     }
                                     await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                     {
                                         if (this.RecordTagList == null)
                                         {
                                             this.RecordTagList = new ObservableCollection<RecordTag>();
                                         }
                                         this.RecordTagList.Add(serviceObj.RecordTag);
                                         //  SelectedRecordTag = serviceObj.RecordTag;
                                     }));
                                 });

                             })).Wait();
                         });
                    }));

                });
            }, () =>
            {
                return ValidateOkCancel();
            });

            _EditTagCommand = new DelegateCommand(async () =>
            {
                await Task.Run(async () =>
                {
                    await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        AddRecordTagView window = new AddRecordTagView();
                        AddRecordTagViewModel vm = window.DataContext as AddRecordTagViewModel;
                        if (vm != null)
                        {
                            vm.SelectedRecordTag = SelectedRecordTag;
                        }
                        window.Name = "AddRecordTagView";
                        window.ShowDialog();
                        window.Close();
                        window = null;
                        SelectedRecordTag = vm.SelectedRecordTag;
                        await Task.Run(async () =>
                         {
                             Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                             {
                                 await Task.Run(async () =>
                                 {
                                     ServiceRequest_RecordTag serviceObj = new ServiceRequest_RecordTag();
                                     if (SelectedRecordTag == null)
                                     {
                                         MessageBox.Show("Plesae select Tag to Edit", "No Tag Selected");
                                         return;
                                     }
                                     serviceObj.RecordTag = SelectedRecordTag;
                                     serviceObj.RecordTagID = SelectedRecordTag.RecordTagID;
                                     AuthorizedResponse<RecordTagResult> Response = await EventManager.UpdateRecordTag(serviceObj);
                                     if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                                     {
                                         MessageBox.Show("Error while editing the Tag", "Tag Edit Error");
                                         return;
                                     }
                                     await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                     {
                                         RecordTag tag = this.RecordTagList.Where(p => p.RecordTagID == SelectedRecordTag.RecordTagID).FirstOrDefault();
                                         int index = this.RecordTagList.IndexOf(tag);
                                         this.RecordTagList.Remove(tag);
                                         this.RecordTagList.Add(tag);
                                         SelectedRecordTag = tag;
                                     }));
                                 });

                             })).Wait();
                         });
                    }));

                });
            }, () =>
            {
                return ValidateOkCancel();
            });
            _DeleteTagCommand = new DelegateCommand(async () =>
            {
                await Task.Run(async () =>
                {
                    await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        await Task.Run(async () =>
                          {
                              Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  await Task.Run(async () =>
                                  {
                                      if (SelectedRecordTag == null)
                                      {
                                          MessageBox.Show("Please select Tag to Edit", "No Tag Selected");
                                          return;
                                      }
                                      MessageBoxResult result = MessageBox.Show("Are you sure Delete Tag?", "Delete Tag", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                      if (result == MessageBoxResult.Yes)
                                      {
                                          ServiceRequest_RecordTag serviceObj = new ServiceRequest_RecordTag();
                                          serviceObj.RecordTag = SelectedRecordTag;
                                          AuthorizedResponse<RecordTagResult> Response = await EventManager.DeleteRecordTag(SelectedRecordTag.RecordTagID);
                                          if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                                          {
                                              MessageBox.Show("Error while editing the Tag", "Tag Edit Error");
                                              return;
                                          }
                                          await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                          {
                                              this.RecordTagList.Remove(SelectedRecordTag);
                                          }));
                                      }

                                  });

                              })).Wait();
                          });
                    }));

                });
            }, () =>
            {
                return ValidateOkCancel();
            });

            _PrintCommand = new DelegateCommand(async () =>
            {
                System.Windows.Controls.PrintDialog pdPrintDialog = new System.Windows.Controls.PrintDialog();

                if ((bool)pdPrintDialog.ShowDialog().GetValueOrDefault())
                {
                    Size pntPageSize = new Size(pdPrintDialog.PrintableAreaWidth, pdPrintDialog.PrintableAreaHeight);
                    recordGrid.Measure(pntPageSize);
                    recordGrid.Arrange(new Rect(5, 5, pntPageSize.Width, pntPageSize.Height));
                    pdPrintDialog.PrintVisual(recordGrid, "Record");
                }
            });

            List<RecordTag> tagList = EventManager.GetAllRecordTag().Result.ToList();
            RecordTagList = new ObservableCollection<RecordTag>();
            foreach (RecordTag item in tagList)
            {
                RecordTagList.Add(item);
            }
        }


        private void CreateReportCommandCommandHandler()
        {
            DataTable dt = ReportTemplateGenerator.ToDataTable<RecordDetail>(ManageRecordList);
            if (dt != null)
            {
                string ReportTemplate = ReportTemplateGenerator.GenerateTemplate("Record List", "Record List", ReportTemplateGenerator.GetColumnList<RecordDetail>());
                Window window = new ReportView(ReportTemplate, dt);
                window.ShowDialog();
            }
        }

        private void RefreshCommandCommandHandler()
        {
            ManageRecordList = ManageRecordListAll;
        }

        private void UpdateCommandCommandHandler()
        {
            if (this.ManageRecordList != null)
            {
                UpdateRecordView updateRecord = new UpdateRecordView(this.ManageRecordList, EventManager);
                updateRecord.Name = "UpdateRecordView";
                updateRecord.ShowDialog();
                updateRecord.Close();
                updateRecord = null;
            }
            else
            {
                MessageBox.Show("Please export the record first", "No Record exist to update");
            }
        }

        private void ShowCommandCommandHandler()
        {
        }

        private void ExportToCommandHandler()
        {
            if (ManageRecordList == null) return;
            if (ManageRecordList.Count <= 0) return;
            ExportToExcel<RecordDetail, List<RecordDetail>> s = new ExportToExcel<RecordDetail, List<RecordDetail>>();
            s.dataToPrint = ManageRecordList;
            s.GenerateReport();
        }

        private void ImportToCommandHandler()
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.DefaultExt = ".xlsx";
            openfile.Filter = "(.xlsx)|*.xlsx";
            var browsefile = openfile.ShowDialog();
            if (browsefile == true)
            {
                DataTable dt = ExportFromExcel.ExportFrom(openfile.FileName);
                ManageRecordListAll = new List<RecordDetail>();
                foreach (DataRow item in dt.Rows)
                {
                    RecordDetail recordList = new RecordDetail();
                    recordList.EventID = Convert.ToInt32(item.ItemArray[0]);
                    recordList.EventName = Convert.ToString(item.ItemArray[1]);
                    recordList.TagName = Convert.ToString(item.ItemArray[2]);
                    recordList.Mark = Convert.ToString(item.ItemArray[3]);
                    recordList.Year = Convert.ToInt16(item.ItemArray[4]);
                    recordList.Month = Convert.ToInt16(item.ItemArray[5]);
                    recordList.Day = Convert.ToInt16(item.ItemArray[6]);
                    recordList.RecordHolderName = Convert.ToString(item.ItemArray[7]);
                    recordList.Gender = Convert.ToString(item.ItemArray[8]);
                    recordList.StartAgeRange = Convert.ToInt16(item.ItemArray[9]);
                    recordList.EndAgeRange = Convert.ToInt16(item.ItemArray[10]);
                    recordList.EventType = Convert.ToString(item.ItemArray[11]);
                    ManageRecordListAll.Add(recordList);
                }
                ManageRecordList = ManageRecordListAll;
                // recordGrid.ItemsSource = dt.DefaultView;
            }
        }

        #region Properties
        private bool _IsShowRecordsForSelectedOnly;
        public bool IsShowRecordsForSelectedOnly
        {
            get { return _IsShowRecordsForSelectedOnly; }
            set
            {
                SetProperty(ref _IsShowRecordsForSelectedOnly, value);
            }
        }
        private ObservableCollection<EventMenuItemViewModel> _MenuItems;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }

        private ObservableCollection<RecordTag> _RecordTagList;
        public ObservableCollection<RecordTag> RecordTagList
        {
            get { return _RecordTagList; }
            set
            {
                SetProperty(ref _RecordTagList, value);
            }
        }
        private List<RecordDetail> _ManageRecordList;
        public List<RecordDetail> ManageRecordList
        {
            get { return _ManageRecordList; }
            set
            {
                SetProperty(ref _ManageRecordList, value);
            }
        }

        private List<RecordDetail> _ManageRecordListAll;
        public List<RecordDetail> ManageRecordListAll
        {
            get { return _ManageRecordListAll; }
            set
            {
                SetProperty(ref _ManageRecordListAll, value);
            }
        }

        private RecordDetail _SelectedManageRecord;
        public RecordDetail SelectedManageRecord
        {
            get { return _SelectedManageRecord; }
            set
            {
                SetProperty(ref _SelectedManageRecord, value);
            }
        }

        private RecordTag _SelectedRecordTag;
        public RecordTag SelectedRecordTag
        {
            get { return _SelectedRecordTag; }
            set
            {
                SetProperty(ref _SelectedRecordTag, value);
                this.FilterRecord();
            }
        }

        private bool _IsFemaleChecked;
        public bool IsFemaleChecked
        {
            get { return _IsFemaleChecked; }
            set
            {
                SetProperty(ref _IsFemaleChecked, value);
                FilterRecord();
            }
        }

        private bool _IsMaleChecked;
        public bool IsMaleChecked
        {
            get { return _IsMaleChecked; }
            set
            {
                SetProperty(ref _IsMaleChecked, value);
                FilterRecord();
            }
        }
        private bool _IsAllChecked;
        public bool IsAllChecked
        {
            get { return _IsAllChecked; }
            set
            {
                SetProperty(ref _IsAllChecked, value);
                FilterRecord();
            }
        }
        private bool _IsMixedChecked;
        public bool IsMixedChecked
        {
            get { return _IsMixedChecked; }
            set
            {
                SetProperty(ref _IsMixedChecked, value);
                FilterRecord();
            }
        }
        private int _StartAgeRange;
        public int StartAgeRange
        {
            get { return _StartAgeRange; }
            set
            {
                SetProperty(ref _StartAgeRange, value);
                FilterRecord();
            }
        }
        private int _EndAgeRange;
        public int EndAgeRange
        {
            get { return _EndAgeRange; }
            set
            {
                SetProperty(ref _EndAgeRange, value);
                FilterRecord();
            }
        }
        private bool _IsRelayOnlyChecked;
        public bool IsRelayOnlyChecked
        {
            get { return _IsRelayOnlyChecked; }
            set
            {
                SetProperty(ref _IsRelayOnlyChecked, value);
                FilterRecord();
            }
        }

        private bool _IsIndvOnlyChecked;
        public bool IsIndvOnlyChecked
        {
            get { return _IsIndvOnlyChecked; }
            set
            {
                SetProperty(ref _IsIndvOnlyChecked, value);
                FilterRecord();
            }
        }

        private bool _IsIndvRelayChecked;
        public bool IsIndvRelayChecked
        {
            get { return _IsIndvRelayChecked; }
            set
            {
                SetProperty(ref _IsIndvRelayChecked, value);
                FilterRecord();
            }
        }
        private bool _IsByEventChecked;
        public bool IsByEventChecked
        {
            get { return _IsByEventChecked; }
            set
            {
                SetProperty(ref _IsByEventChecked, value);
                FilterRecord();
            }
        }
        private bool _IsByAgeGroupChecked;
        public bool IsByAgeGroupChecked
        {
            get { return _IsByAgeGroupChecked; }
            set
            {
                SetProperty(ref _IsByAgeGroupChecked, value);
                FilterRecord();
            }
        }

        private void FilterRecord()
        {
            if (this.ManageRecordListAll == null) return;
            List<RecordDetail> tempRecordList = this.ManageRecordListAll;
            if (SelectedRecordTag != null)
            {
                tempRecordList = tempRecordList.Where(p => p.TagName == SelectedRecordTag.TagName).ToList();
            }
            if (IsAllChecked)
            {
            }
            else if (IsFemaleChecked)
            {
                tempRecordList = tempRecordList.Where(p => p.Gender.ToUpper() == "FEMALE").ToList();
            }
            else if (IsMaleChecked)
            {
                tempRecordList = tempRecordList.Where(p => p.Gender.ToUpper() == "MALE").ToList();
            }
            else if (IsMixedChecked)
            {
                tempRecordList = tempRecordList.Where(p => p.Gender.ToUpper() == "MIXED").ToList();
            }
            if (StartAgeRange > 0)
            {
                tempRecordList = tempRecordList.Where(p => p.StartAgeRange >= StartAgeRange).ToList();
            }
            if (EndAgeRange > 0)
            {
                tempRecordList = tempRecordList.Where(p => p.EndAgeRange >= EndAgeRange).ToList();
            }
            if (IsByAgeGroupChecked)
            {
                tempRecordList = tempRecordList.OrderBy(p => p.StartAgeRange).ToList();
            }
            else
            {
                tempRecordList = tempRecordList.OrderBy(p => p.EventID).ToList();
            }
            ManageRecordList = tempRecordList;
        }

        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(ManageRecordViewModel entity)
        {


        }
        public ManageRecordViewModel GetEntity()
        {

            return this;
        }


        #region Command
        private ICommand _ImportToCommand;
        public ICommand ImportToCommand
        {
            get { return _ImportToCommand; }
        }

        private ICommand _ExportToCommand;
        public ICommand ExportToCommand
        {
            get { return _ExportToCommand; }
        }

        private ICommand _ShowCommand;
        public ICommand ShowCommand
        {
            get { return _ShowCommand; }
        }

        private ICommand _PrintCommand;
        public ICommand PrintCommand
        {
            get { return _PrintCommand; }
        }

        private ICommand _ArchiveCommand;
        public ICommand ArchiveCommand
        {
            get { return _ArchiveCommand; }
        }

        private ICommand _RefreshCommand;
        public ICommand RefreshCommand
        {
            get { return _RefreshCommand; }
        }
        private ICommand _CreateReportCommand;
        public ICommand CreateReportCommand
        {
            get { return _CreateReportCommand; }
        }

        private ICommand _AddTagCommand;
        public ICommand AddTagCommand
        {
            get { return _AddTagCommand; }
        }

        private ICommand _EditTagCommand;
        public ICommand EditTagCommand
        {
            get { return _EditTagCommand; }
        }

        private ICommand _DeleteTagCommand;
        public ICommand DeleteTagCommand
        {
            get { return _DeleteTagCommand; }
        }

        #endregion
    }

}

