﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class RecordDetailUpdate : RecordDetail
    {
        private string _OldOrNew;
        public string OldOrNew
        {
            get { return _OldOrNew; }
            set
            {
                _OldOrNew = value;
            }
        }
        private bool _Incl;
        public bool Incl
        {
            get { return _Incl; }
            set
            {
                _Incl = value;
            }
        }
    }
    public class UpdateRecordViewModel : BindableBase
    {
        private IEventManager EventManager;
        public UpdateRecordViewModel(List<RecordDetail> recordListNew, IEventManager eventManager)
        {
            ManageRecordList = new List<RecordDetailUpdate>();
            EventManager = eventManager;
            foreach (var item in recordListNew)
            {
                RecordDetailUpdate recordUpdate = new RecordDetailUpdate();
                recordUpdate.Day = item.Day;
                recordUpdate.EndAgeRange = item.EndAgeRange;
                recordUpdate.EventID = item.EventID;
                recordUpdate.EventName = item.EventName;
                recordUpdate.EventType = item.EventType;
                recordUpdate.Gender = item.Gender;
                recordUpdate.Incl = true;
                recordUpdate.Mark = item.Mark;
                recordUpdate.Month = item.Month;
                recordUpdate.OldOrNew = "New";
                recordUpdate.RecordHolderName = item.RecordHolderName;
                recordUpdate.StartAgeRange = item.StartAgeRange;
                recordUpdate.TagName = item.TagName;
                recordUpdate.Year = item.Year;
                ManageRecordList.Add(recordUpdate);
            }
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            List<RecordDetailUpdate> NewList = ManageRecordList.Where(p => p.OldOrNew == "New").ToList();
                            List<RecordDetailUpdate> OldList = ManageRecordList.Where(p => p.OldOrNew == "Old").ToList();
                            foreach (var item in OldList)
                            {
                                RecordDetailUpdate newRecord = NewList.Where(p => p.EventName == item.EventName && p.Incl == true).FirstOrDefault();
                                if (newRecord != null)
                                {
                                    ServiceRequest_RecordUpdate serviceObj = new ServiceRequest_RecordUpdate();
                                    RecordDetail recordDetail = new RecordDetail();
                                    recordDetail.RecordDetailID = item.RecordDetailID;
                                    recordDetail.Day = newRecord.Day;
                                    recordDetail.EndAgeRange = newRecord.EndAgeRange;
                                    recordDetail.EventID = newRecord.EventID;
                                    recordDetail.EventName = newRecord.EventName;
                                    recordDetail.EventType = newRecord.EventType;
                                    recordDetail.Gender = newRecord.Gender;
                                    recordDetail.Mark = newRecord.Mark;
                                    recordDetail.Month = newRecord.Month;
                                    recordDetail.RecordHolderName = newRecord.RecordHolderName;
                                    recordDetail.StartAgeRange = newRecord.StartAgeRange;
                                    recordDetail.TagName = newRecord.TagName;
                                    recordDetail.Year = newRecord.Year;
                                    serviceObj.RecordUpdate = recordDetail;
                                    serviceObj.RecordUpdateID = recordDetail.RecordDetailID;
                                    //await Task.Run(async () =>
                                    //{
                                    AuthorizedResponse<RecordUpdateResult> Response = await EventManager.UpdateRecord(serviceObj);
                                    //});
                                }
                            }
                            var result = NewList.Where(p => !OldList.Any(p2 => p2.EventName != p.EventName));
                            foreach (var newRecord in result)
                            {
                                ServiceRequest_RecordUpdate serviceObj = new ServiceRequest_RecordUpdate();
                                RecordDetail recordDetail = new RecordDetail();
                                recordDetail.Day = newRecord.Day;
                                recordDetail.EndAgeRange = newRecord.EndAgeRange;
                                recordDetail.EventID = newRecord.EventID;
                                recordDetail.EventName = newRecord.EventName;
                                recordDetail.EventType = newRecord.EventType;
                                recordDetail.Gender = newRecord.Gender;
                                recordDetail.Mark = newRecord.Mark;
                                recordDetail.Month = newRecord.Month;
                                recordDetail.RecordHolderName = newRecord.RecordHolderName;
                                recordDetail.StartAgeRange = newRecord.StartAgeRange;
                                recordDetail.TagName = newRecord.TagName;
                                recordDetail.Year = newRecord.Year;
                                serviceObj.RecordUpdate = recordDetail;
                                serviceObj.RecordUpdateID = recordDetail.RecordDetailID;
                                //await Task.Run(async () =>
                                //{
                                AuthorizedResponse<RecordUpdateResult> Response = await EventManager.AddRecord(serviceObj);
                                //});
                            }
                            foreach (Window item in Application.Current.Windows)
                            {
                                if (item.Name == "UpdateRecordView")
                                {
                                    item.Close();
                                    break;
                                }
                            }
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "UpdateRecordView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });
            List<RecordDetail> tagList = EventManager.GetAllRecord().Result.ToList();
            foreach (RecordDetail item in tagList)
            {
                RecordDetail rec = recordListNew.Where(p => p.EventName == item.EventName).FirstOrDefault();
                if (rec != null)
                {
                    RecordDetailUpdate updateRecord = new RecordDetailUpdate();
                    updateRecord.Day = item.Day;
                    updateRecord.EndAgeRange = item.EndAgeRange;
                    updateRecord.EventID = item.EventID;
                    updateRecord.EventName = item.EventName;
                    updateRecord.EventType = item.EventType;
                    updateRecord.Gender = item.Gender;
                    updateRecord.Incl = false;
                    updateRecord.Mark = item.Mark;
                    updateRecord.Month = item.Month;
                    updateRecord.OldOrNew = "Old";
                    updateRecord.RecordDetailID = item.RecordDetailID;
                    updateRecord.RecordHolderName = item.RecordHolderName;
                    updateRecord.StartAgeRange = item.StartAgeRange;
                    updateRecord.TagName = item.TagName;
                    updateRecord.Year = item.Year;
                    ManageRecordList.Add(updateRecord);
                }
            }
            ManageRecordList = ManageRecordList.OrderByDescending(p => p.OldOrNew).ToList().OrderBy(p => p.EventID).ToList();
        }

        #region Properties

        private List<RecordDetailUpdate> _ManageRecordList;
        public List<RecordDetailUpdate> ManageRecordList
        {
            get { return _ManageRecordList; }
            set
            {
                SetProperty(ref _ManageRecordList, value);
            }
        }
        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }
        #endregion
    }

}

