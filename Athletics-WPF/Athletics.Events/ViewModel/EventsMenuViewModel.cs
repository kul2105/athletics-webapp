﻿using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class EventsMenuViewModel : BindableBase, IEventsMenuViewModel
    {
        private ObservableCollection<EventMenuItemViewModel> _MenuItems = new ObservableCollection<EventMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IEventManager eventManager;
        IManageEventView View;
        IManageEventViewModel ViewModel;
        IEventsMenuView AthleticMenuView;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public EventsMenuViewModel(IUnityContainer unity, IEventsMenuView _View, IManageEventView ManageEventView, IEventManager manager)
        {
            eventManager = manager;
            View = ManageEventView;
            ViewModel = ManageEventView.DataContext as IManageEventViewModel;
            AthleticMenuView = _View;
            AthleticMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<EventMenuItemViewModel>();
                EventMenuItemViewModel eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Layout";
                _LayoutCommand = new MeetCommandViewModel(LayoutCommandHandler);
                eventMenu.Command = LayoutCommand;
                MenuItems.Add(eventMenu);
                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Add";
                _AddCommand = new MeetCommandViewModel(AddCommandHandler);
                eventMenu.Command = AddCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Edit";
                _EditCommand = new MeetCommandViewModel(EditCommandHandler);
                eventMenu.Command = EditCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Delete";
                _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
                eventMenu.Command = DeleteCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Combined Events";
                _CombinedEventsCommand = new MeetCommandViewModel(CombinedEventsCommandHandler);
                eventMenu.Command = CombinedEventsCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Standard Lanes";
                _StandardLaneCommand = new MeetCommandViewModel(CombinedEventsCommandHandler);
                eventMenu.Command = StandardLaneCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Sessions";
                _SessionCommand = new MeetCommandViewModel(SessionCommandHandler);
                eventMenu.Command = SessionCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Comments";
                _CommentsCommand = new MeetCommandViewModel(CommentsCommandHandler);
                eventMenu.Command = CommentsCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Re-Number";
                _ReNumberCommand = new MeetCommandViewModel(ReNumberCommandandler);
                eventMenu.Command = ReNumberCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Records";
                _RecordCommand = new MeetCommandViewModel(RecordCommandandler);
                eventMenu.Command = RecordCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Standards";
                _StandardCommand = new MeetCommandViewModel(StandardCommandandler);
                eventMenu.Command = StandardCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "View";
                _ViewCommand = new MeetCommandViewModel(ViewCommandandler);
                eventMenu.Command = ViewCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Print";
                _PrintCommand = new MeetCommandViewModel(PrintCommandandler);
                eventMenu.Command = PrintCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Export To HTML";
                _ExportToHTMLCommand = new MeetCommandViewModel(ExportToHTMLCommandHandler);
                eventMenu.Command = ExportToHTMLCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Help";
                _HelpCommand = new MeetCommandViewModel(HelpCommandHandler);
                eventMenu.Command = HelpCommand;
                MenuItems.Add(eventMenu);

                #endregion

            });
            unityContainer = unity;
            //View.DataContext = this;

        }
        #region Menu Command

        private ICommand _LayoutCommand;
        public ICommand LayoutCommand
        {
            get
            {
                return _LayoutCommand;
            }
        }

        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }

        private ICommand _CombinedEventsCommand;
        public ICommand CombinedEventsCommand
        {
            get
            {
                return _CombinedEventsCommand;
            }
        }

        private ICommand _StandardLaneCommand;
        public ICommand StandardLaneCommand
        {
            get
            {
                return _StandardLaneCommand;
            }
        }

        private ICommand _SessionCommand;
        public ICommand SessionCommand
        {
            get
            {
                return _SessionCommand;
            }
        }
        private ICommand _CommentsCommand;
        public ICommand CommentsCommand
        {
            get
            {
                return _CommentsCommand;
            }
        }

        private ICommand _ReNumberCommand;
        public ICommand ReNumberCommand
        {
            get
            {
                return _ReNumberCommand;
            }
        }
        private ICommand _RecordCommand;
        public ICommand RecordCommand
        {
            get
            {
                return _RecordCommand;
            }
        }

        private ICommand _StandardCommand;
        public ICommand StandardCommand
        {
            get
            {
                return _StandardCommand;
            }
        }
        private ICommand _ViewCommand;
        public ICommand ViewCommand
        {
            get
            {
                return _ViewCommand;
            }
        }

        private ICommand _PrintCommand;
        public ICommand PrintCommand
        {
            get
            {
                return _PrintCommand;
            }
        }

        private ICommand _ExportToHTMLCommand;
        public ICommand ExportToHTMLCommand
        {
            get
            {
                return _ExportToHTMLCommand;
            }
        }

        private ICommand _HelpCommand;
        public ICommand HelpCommand
        {
            get
            {
                return _HelpCommand;
            }
        }
        #endregion

        #region Command Handler


        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        //if (AthleteRelayPreferencesId == 0)
                        //{
                        if (ViewModel.SelectedEventDetail == null) return;
                        AuthorizedResponse<EventResult> Response = await eventManager.DeleteEvent(ViewModel.SelectedEventDetail.EventID);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleteing the Event", "Event Delete Error");
                            return;
                        }
                        //  this.AthleteRelayPreferencesId = Response.Result.EventID;
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            ViewModel.EventDetailList.Remove(ViewModel.SelectedEventDetail);
                        }));
                        //else
                        //{
                        //}

                    });

                })).Wait();
            });

        }

        private void EditCommandHandler()
        {
            if (ViewModel.SelectedEventDetail == null) return;
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddEventView();
            window.Name = "AddEvents";
            IAddEventView eventView = window as IAddEventView;
            AddEventViewModel addEventViewModel = new AddEventViewModel(this.unityContainer, eventView, eventManager);
            if (addEventViewModel != null)
            {
                IAddEventViewModel addEvent = addEventViewModel as IAddEventViewModel;
                if (addEvent != null)
                {
                    addEvent.EventDetailList = ViewModel.EventDetailList;
                }
                addEvent.SelectedEventDetail = ViewModel.SelectedEventDetail;
                dialog.BindView(window);
                dialog.BindViewModel(addEvent);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            addEventViewModel = addEventViewModel.GetEntity();
            if (addEventViewModel.IsCanceled == true) return;
            if (addEventViewModel.IsGirlsChecked)
            {
                addEventViewModel.Gender = "Girls";
            }
            else if (addEventViewModel.IsBoysChecked)
            {
                addEventViewModel.Gender = "Boys";
            }
            else if (addEventViewModel.IsMenChecked)
            {
                addEventViewModel.Gender = "Men";
            }
            else if (addEventViewModel.IsWomenChecked)
            {
                addEventViewModel.Gender = "Women";
            }
            else if (addEventViewModel.IsMixedChecked)
            {
                addEventViewModel.Gender = "Mixed";
            }
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        //if (AthleteRelayPreferencesId == 0)
                        //{

                        ServiceRequest_Event serviceObj = new ServiceRequest_Event();
                        serviceObj.Event = addEventViewModel;
                        AuthorizedResponse<EventResult> Response = await eventManager.UpdateEvent(serviceObj);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while Updating the Event", "Event Add Error");
                            return;
                        }
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            // this.AthleteRelayPreferencesId = Response.Result.EventID;
                            ViewModel.EventDetailList.Remove(ViewModel.SelectedEventDetail);
                            ViewModel.EventDetailList.Add(addEventViewModel);
                            ViewModel.SelectedEventDetail = addEventViewModel;
                        }));
                        //}
                        //else
                        //{
                        //}

                    });

                })).Wait();
            });

        }

        private void AddCommandHandler()
        {
            if (ViewModel.SelectedMeet == null)
            {
                MessageBox.Show("Please Select the meet to Add event into meet.");
                return;
            }
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddEventView();
            window.Name = "AddEvents";
            IAddEventView eventView = window as IAddEventView;
            AddEventViewModel addEventViewModel = new AddEventViewModel(this.unityContainer, eventView, eventManager);
            this.ClearViewModel(addEventViewModel);
            if (addEventViewModel != null)
            {
                IAddEventViewModel addEvent = addEventViewModel as IAddEventViewModel;
                if (addEvent != null)
                {
                    addEvent.EventDetailList = ViewModel.EventDetailList;
                }
                dialog.BindView(window);
                dialog.BindViewModel(addEventViewModel);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            if (addEventViewModel.IsGirlsChecked)
            {
                addEventViewModel.Gender = "Girls";
            }
            else if (addEventViewModel.IsBoysChecked)
            {
                addEventViewModel.Gender = "Boys";
            }
            else if (addEventViewModel.IsMenChecked)
            {
                addEventViewModel.Gender = "Men";
            }
            else if (addEventViewModel.IsWomenChecked)
            {
                addEventViewModel.Gender = "Women";
            }
            else if (addEventViewModel.IsMixedChecked)
            {
                addEventViewModel.Gender = "Mixed";
            }

            if (addEventViewModel.IsCanceled == true) return;
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        //if (AthleteRelayPreferencesId == 0)
                        //{

                        ServiceRequest_Event serviceObj = new ServiceRequest_Event();
                        serviceObj.Event = addEventViewModel;
                        AuthorizedResponse<EventResult> Response = await eventManager.CreateNewEvent(serviceObj);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while adding the Event", "Event Add Error");
                            return;
                        }
                        Response = await eventManager.AddEventInMeet(Response.Result.EventID, ViewModel.SelectedMeet.MeetID);
                        if (Response.Status == SetupMeetManager.eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while adding the Event into Meet", "Event Add Error");
                            return;
                        }
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            ViewModel.EventDetailList.Add(addEventViewModel);
                        }));

                    });

                })).Wait();
            });

        }
        private void ClearViewModel(IGenericInteractionView<AddEventViewModel> addEventViewModel)
        {
            addEventViewModel.SetEntity(new AddEventViewModel());
        }

        private void LayoutCommandHandler()
        {
            MessageBox.Show("Layout");
        }
        private void CombinedEventsCommandHandler()
        {
            MessageBox.Show("combined Event");
        }

        private void HelpCommandHandler()
        {
            MessageBox.Show("Help");
        }

        private void ExportToHTMLCommandHandler()
        {
            View.EventGrid.SelectAllCells();
            View.EventGrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;

            ApplicationCommands.Copy.Execute(
                     null, View.EventGrid);
            String result = (string)Clipboard.GetData(
                         DataFormats.Html);
            View.EventGrid.UnselectAll();
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document1"; // Default file name
            dlg.DefaultExt = ".html"; // Default file extension
            dlg.Filter = "Html documents (.html)|*.html"; // Filter files by extension
            Nullable<bool> resultDialog = dlg.ShowDialog();

            // Process save file dialog box results
            if (resultDialog == true)
            {
                // Save document
                string filename = dlg.FileName;
                System.IO.File.WriteAllText(filename, result);
            }


        }

        private void PrintCommandandler()
        {
            System.Windows.Controls.PrintDialog pdPrintDialog = new System.Windows.Controls.PrintDialog();
            if ((bool)pdPrintDialog.ShowDialog().GetValueOrDefault())
            {
                Size pntPageSize = new Size(pdPrintDialog.PrintableAreaWidth, pdPrintDialog.PrintableAreaHeight);
                View.EventGrid.Measure(pntPageSize);
                View.EventGrid.Arrange(new Rect(5, 5, pntPageSize.Width, pntPageSize.Height));
                pdPrintDialog.PrintVisual(View.EventGrid, "Events");
            }
        }

        private void ViewCommandandler()
        {
            MessageBox.Show("View");
        }

        private void StandardCommandandler()
        {
            ManageTimeMarkStandardView manageRecords = new ManageTimeMarkStandardView();
            manageRecords.Name = "ManageTimeMarkStandardView";
            manageRecords.ShowDialog();
            manageRecords.Close();
        }

        private void RecordCommandandler()
        {
            ManageRecordView manageRecords = new ManageRecordView();
            manageRecords.Name = "ManageRecordView";
            manageRecords.ShowDialog();
            manageRecords.Close();
        }

        private void ReNumberCommandandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new RenumEventView();
            window.Name = "RenumEvent";
            IGenericInteractionView<RenumEventViewModel> multiAgeGroupVM = unityContainer.Resolve<IRenumEventViewModel>() as IGenericInteractionView<RenumEventViewModel>;
            if (multiAgeGroupVM != null)
            {
                IRenumEventViewModel renumEventVm = multiAgeGroupVM as IRenumEventViewModel;
                if (renumEventVm != null)
                {
                    List<AddEventViewModel> EventList = eventManager.GetAllEvent().Result.ToList();
                    ObservableCollection<AddEventViewModel> list = new ObservableCollection<AddEventViewModel>();
                    foreach (var item in EventList)
                    {
                        list.Add(item);

                    }
                    renumEventVm.EventDetailList = list;
                }
                dialog.BindView(window);
                dialog.BindViewModel(multiAgeGroupVM);
                dialog.ShowDialog();
                dialog.Close();
                ViewModel.RefreshEvent();
            }
        }

        private void CommentsCommandHandler()
        {
            CommentsView window1 = new CommentsView();
            window1.Name = "commentView";
            window1.ShowDialog();
            window1.Close();
        }

        private void SessionCommandHandler()
        {
            //dialog = ServiceProvider.Instance.Get<IModalDialog>();
            ManageSessionView window1 = new ManageSessionView();
            window1.Name = "Sessions";
            //IManageSessionView sessions = window as IManageSessionView;
            ManageSessionViewModel addEventViewModel = new ManageSessionViewModel(this.unityContainer, window1, eventManager);
            if (addEventViewModel != null)
            {
                // dialog.BindView(window);
                window1.DataContext = addEventViewModel;
            }
            window1.ShowDialog();
            window1.Close();
        }
        #endregion

    }
}

