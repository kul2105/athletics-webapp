﻿using Athletics.Entities.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IManageTimeMarkStandardViewModel
    {
        List<EventTimeMarkStandard> ManageTimeMarkStandardList { get; set; }
        List<EventTimeMarkStandard> ManageTimeMarkStandardListAll { get; set; }
        ObservableCollection<EventTimeMarkStandardTag> TimeMarkStandardTagList { get; set; }
    }
}
