﻿using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Athletics.Report.Views.ViewReport;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class TimeMarkStandardMenuViewModel : BindableBase, ITimeMarkStandardMenuViewModel
    {
        private ObservableCollection<EventMenuItemViewModel> _MenuItems = new ObservableCollection<EventMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IEventManager eventManager;
        IManageTimeMarkStandardView View;
        IManageTimeMarkStandardViewModel ViewModel;
        ITimeMarkStandardMenuView TimeMarkStandardMenuView;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public TimeMarkStandardMenuViewModel(IUnityContainer unity, ITimeMarkStandardMenuView _View, IManageTimeMarkStandardView ManageEventView, IEventManager manager)
        {
            eventManager = manager;
            View = ManageEventView;
            ViewModel = ManageEventView.DataContext as IManageTimeMarkStandardViewModel;
            TimeMarkStandardMenuView = _View;
            TimeMarkStandardMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<EventMenuItemViewModel>();
                EventMenuItemViewModel eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Import";
                _ImportToCommand = new MeetCommandViewModel(ImportToCommandHandler);
                eventMenu.Command = ImportToCommand;
                MenuItems.Add(eventMenu);
                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Export";
                _ImportToCommand = new MeetCommandViewModel(ExportToCommandHandler);
                eventMenu.Command = ImportToCommand;
                MenuItems.Add(eventMenu);


                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "View";
                _ShowCommand = new MeetCommandViewModel(ViewCommandCommandHandler);
                eventMenu.Command = ShowCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Create Report";
                _CreateReportCommand = new MeetCommandViewModel(CreateReportCommandCommandHandler);
                eventMenu.Command = CreateReportCommand;
                MenuItems.Add(eventMenu);
                #endregion

            });
            unityContainer = unity;
            //View.DataContext = this;

        }
        #region Menu Command

        private ICommand _ImportToCommand;
        public ICommand ImportToCommand
        {
            get { return _ImportToCommand; }
        }

        private ICommand _ExportToCommand;
        public ICommand ExportToCommand
        {
            get { return _ExportToCommand; }
        }
        private ICommand _ShowCommand;
        public ICommand ShowCommand
        {
            get { return _ShowCommand; }
        }
        private ICommand _CreateReportCommand;
        public ICommand CreateReportCommand
        {
            get { return _CreateReportCommand; }
        }
        #endregion

        #region Command Handler


        private void ViewCommandCommandHandler()
        {
        }

        private void CreateReportCommandCommandHandler()
        {
            DataTable dt = ReportTemplateGenerator.ToDataTable<EventTimeMarkStandard>(ViewModel.ManageTimeMarkStandardList);
            if (dt != null)
            {
                string ReportTemplate = ReportTemplateGenerator.GenerateTemplate("TimeMarkStandard List", "TimeMarkStandard List", ReportTemplateGenerator.GetColumnList<EventTimeMarkStandard>());
                Window window = new ReportView(ReportTemplate, dt);
                window.ShowDialog();
            }
        }


        private void ShowCommandCommandHandler()
        {
        }

        private void ExportToCommandHandler()
        {
            ExportToExcel<EventTimeMarkStandard, List<EventTimeMarkStandard>> s = new ExportToExcel<EventTimeMarkStandard, List<EventTimeMarkStandard>>();
            s.dataToPrint =ViewModel.ManageTimeMarkStandardList;
            s.GenerateReport();
        }

        private void ImportToCommandHandler()
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.DefaultExt = ".xlsx";
            openfile.Filter = "(.xlsx)|*.xlsx";
            var browsefile = openfile.ShowDialog();
            if (browsefile == true)
            {
                DataTable dt = ExportFromExcel.ExportFrom(openfile.FileName);
                ViewModel.ManageTimeMarkStandardListAll = new List<EventTimeMarkStandard>();
                foreach (DataRow item in dt.Rows)
                {
                    EventTimeMarkStandard recordList = new EventTimeMarkStandard();
                    recordList.EventID = Convert.ToInt32(item.ItemArray[0]);
                    recordList.EventName = Convert.ToString(item.ItemArray[1]);

                    int timespane1 = 0, timespane2 = 0, timespane3 = 0;
                    Int32.TryParse(item.ItemArray[2].ToString(), out timespane1);
                    Int32.TryParse(item.ItemArray[3].ToString(), out timespane2);
                    Int32.TryParse(item.ItemArray[4].ToString(), out timespane3);
                    recordList.EventTimeMarkStandardIDTagValue = new TimeSpan(timespane1, timespane2, timespane3);
                    recordList.Gender = Convert.ToString(item.ItemArray[5]);
                    recordList.StartAgeRange = Convert.ToInt16(item.ItemArray[6]);
                    recordList.EndAgeRange = Convert.ToInt16(item.ItemArray[7]);
                    recordList.EventType = Convert.ToString(item.ItemArray[8]);
                    recordList.EventTimeMarkStandardTagID = this.GetTagID(dt.Columns[2].ColumnName);


                    ViewModel.ManageTimeMarkStandardListAll.Add(recordList);
                }
                ViewModel.ManageTimeMarkStandardList = ViewModel.ManageTimeMarkStandardListAll;
                // recordGrid.ItemsSource = dt.DefaultView;
            }
        }
        private int? GetTagID(string columnName)
        {
            EventTimeMarkStandardTag tag = ViewModel.TimeMarkStandardTagList.Where(p => p.TimeMarkStandardTagName == columnName).FirstOrDefault();
            if (tag == null)
            {
                return 0;
            }
            else
            {
                return tag.TimeMarkStandardTagID;
            }
        }
        #endregion

    }
}

