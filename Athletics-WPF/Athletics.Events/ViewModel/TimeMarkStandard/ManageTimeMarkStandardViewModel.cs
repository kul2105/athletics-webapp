﻿using Athletics.ControlLibrary.DataGrid;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Athletics.Report.Views.ViewReport;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{

    public class ManageTimeMarkStandardViewModel : BindableBase, IManageTimeMarkStandardViewModel, IGenericInteractionView<ManageTimeMarkStandardViewModel>
    {
        private IEventManager EventManager;
        AthleticsGrid recordGrid;
        public ManageTimeMarkStandardViewModel(AthleticsGrid datagridRecord)
        {
            recordGrid = datagridRecord;
            EventManager = new SetupMeetManager.EventManager();
            //#region Initilize Command
            //MenuItems = new ObservableCollection<EventMenuItemViewModel>();
            //EventMenuItemViewModel eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "Import";
            //_ImportToCommand = new MeetCommandViewModel(ImportToCommandHandler);
            //eventMenu.Command = ImportToCommand;
            //MenuItems.Add(eventMenu);
            //eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "Export";
            //_ImportToCommand = new MeetCommandViewModel(ExportToCommandHandler);
            //eventMenu.Command = ImportToCommand;
            //MenuItems.Add(eventMenu);


            //eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "View";
            //_ShowCommand = new MeetCommandViewModel(ViewCommandCommandHandler);
            //eventMenu.Command = ShowCommand;
            //MenuItems.Add(eventMenu);

            //eventMenu = new EventMenuItemViewModel();
            //eventMenu.Header = "Create Report";
            //_CreateReportCommand = new MeetCommandViewModel(CreateReportCommandCommandHandler);
            //eventMenu.Command = CreateReportCommand;
            //MenuItems.Add(eventMenu);
            //#endregion
            _ImportToCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            ImportToCommandHandler();
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _ExportToCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            ExportToCommandHandler();
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

            _AddTagCommand = new DelegateCommand(async () =>
            {
                if (this.SelectedManageTimeMarkStandard.EventTimeMarkStandardID == 0)
                {
                    MessageBoxResult result = MessageBox.Show("Event Time Mark standard is not saved are you sure you wants to save before add Tag", "Event Standard is Not Saved", MessageBoxButton.YesNo);
                    if (result == MessageBoxResult.Yes)
                    {
                        ServiceRequest_TimeMarkStandard serviceObjTimeMark = new ServiceRequest_TimeMarkStandard();
                        List<EventTimeMarkStandard> timeMarkStandardList = this.ManageTimeMarkStandardList.Where(p => p.EventTimeMarkStandardID == 0).ToList();
                        serviceObjTimeMark.TimeMarkStandard = this.SelectedManageTimeMarkStandard;
                        AuthorizedResponse<TimeMarkStandardResult> ResponseTimeMark = await EventManager.AddTimeMarkStandard(serviceObjTimeMark);
                        if (ResponseTimeMark.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while adding the Time Mark Standard", "Time Mark Standard Add Error");
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }

                if (this.SelectedManageTimeMarkStandard == null)
                {
                    MessageBox.Show("Please select event", "Event not Selected");
                    return;
                }
                TimeMarkStandardTagView window = new TimeMarkStandardTagView(EventManager);
                TimeMarkStandardViewModel vm = window.DataContext as TimeMarkStandardViewModel;
                window.Name = "AddTimeMarkStandardView";
                window.ShowDialog();
                window.Close();
                window = null;
                if (vm.EventTimeMarkStandardTagList == null) return;
                ServiceRequest_TimeMarkStandardTag serviceObj = new ServiceRequest_TimeMarkStandardTag();
                serviceObj.SelectedTimeMarkStandardTagList = new List<EventTimeMarkStandardTag>();
                foreach (var item in vm.EventTimeMarkStandardTagList.Where(p => p.AddToEvent == true))
                {
                    EventTimeMarkStandardTag tag = new EventTimeMarkStandardTag();
                    tag.ForEntryQual = item.IsForEntryQual;
                    tag.ForTimeMark = item.IsForTimeMark;
                    tag.TimeMarkStandardEventName = this.SelectedManageTimeMarkStandard.EventName;
                    tag.TimeMarkStandardTagName = item.TimeMarkStandardTagName;
                    tag.TimeMarkStandardTagDescription = item.TimeMarkStandardTagDescription;
                    serviceObj.SelectedTimeMarkStandardTagList.Add(tag);
                }
                AuthorizedResponse<TimeMarkStandardTagResult> Response = await EventManager.CreateTimeMarkStandardTag(serviceObj);
                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while adding the Tag", "Tag Add Error");
                    return;
                }
                if (this.TimeMarkStandardTagList == null)
                {
                    this.TimeMarkStandardTagList = new ObservableCollection<EventTimeMarkStandardTag>();
                }
                serviceObj.EventID = serviceObj.EventID;
                foreach (var item in vm.EventTimeMarkStandardTagList.Where(p => p.AddToEvent == true))
                {
                    this.TimeMarkStandardTagList.Add(item);
                }
            });
           

            _EditTagCommand = new DelegateCommand(async () =>
            {
                await Task.Run(async () =>
                {
                    await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        TimeMarkStandardTagView window = new TimeMarkStandardTagView(EventManager, true);
                        TimeMarkStandardViewModel vm = window.DataContext as TimeMarkStandardViewModel;
                        if (vm != null)
                        {
                            vm.EventTimeMarkStandardTagList = new List<EventTimeMarkStandardTagView>();
                            EventTimeMarkStandardTagView tagView = new EventTimeMarkStandardTagView();
                            tagView.AddToEvent = true;
                            tagView.TimeMarkStandardTagID = SelectedTimeMarkStandardTag.TimeMarkStandardTagID;
                            tagView.IsForEntryQual = SelectedTimeMarkStandardTag.ForEntryQual == null ? false : SelectedTimeMarkStandardTag.ForEntryQual.Value;
                            tagView.IsForTimeMark = SelectedTimeMarkStandardTag.ForTimeMark == null ? false : SelectedTimeMarkStandardTag.ForTimeMark.Value;
                            tagView.TimeMarkStandardEventName = SelectedTimeMarkStandardTag.TimeMarkStandardEventName;
                            tagView.TimeMarkStandardTagDescription = SelectedTimeMarkStandardTag.TimeMarkStandardTagDescription;
                            tagView.TimeMarkStandardTagName = SelectedTimeMarkStandardTag.TimeMarkStandardTagName;
                            vm.EventTimeMarkStandardTagList.Add(tagView);
                        }
                        window.Name = "AddTimeMarkStandardView";
                        window.ShowDialog();
                        window.Close();
                        window = null;
                        foreach (var item in vm.EventTimeMarkStandardTagList)
                        {
                            EventTimeMarkStandardTag tag = new EventTimeMarkStandardTag();
                            tag.TimeMarkStandardTagID = item.TimeMarkStandardTagID;
                            tag.ForEntryQual = item.IsForEntryQual;
                            tag.ForTimeMark = item.IsForTimeMark;
                            tag.TimeMarkStandardEventName = item.TimeMarkStandardEventName;
                            tag.TimeMarkStandardTagDescription = item.TimeMarkStandardTagDescription;
                            tag.TimeMarkStandardTagID = item.TimeMarkStandardTagID;
                            tag.TimeMarkStandardTagName = item.TimeMarkStandardTagName;
                            SelectedTimeMarkStandardTag = tag;
                        }

                        await Task.Run(async () =>
                             {
                                 Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                 {
                                     await Task.Run(async () =>
                                     {
                                         ServiceRequest_TimeMarkStandardTag serviceObj = new ServiceRequest_TimeMarkStandardTag();
                                         if (SelectedTimeMarkStandardTag == null)
                                         {
                                             MessageBox.Show("Plesae select Tag to Edit", "No Tag Selected");
                                             return;
                                         }
                                         serviceObj.SelectedTimeMarkStandardTagList = new List<EventTimeMarkStandardTag>();
                                         serviceObj.SelectedTimeMarkStandardTagList.Add(SelectedTimeMarkStandardTag);
                                         if (SelectedManageTimeMarkStandard.EventID == null)
                                         {
                                             MessageBox.Show("Event Id Should not be null");
                                             return;
                                         }
                                         serviceObj.EventID = SelectedManageTimeMarkStandard.EventID.Value;
                                         AuthorizedResponse<TimeMarkStandardTagResult> Response = await EventManager.UpdateTimeMarkStandardTag(serviceObj);
                                         if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                                         {
                                             MessageBox.Show("Error while editing the Tag", "Tag Edit Error");
                                             return;
                                         }
                                         await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                         {
                                             EventTimeMarkStandardTag tag = this.TimeMarkStandardTagList.Where(p => p.TimeMarkStandardTagID == SelectedTimeMarkStandardTag.TimeMarkStandardTagID).FirstOrDefault();
                                             int index = this.TimeMarkStandardTagList.IndexOf(tag);
                                             this.TimeMarkStandardTagList.Remove(tag);
                                             this.TimeMarkStandardTagList.Add(tag);
                                             SelectedTimeMarkStandardTag = tag;
                                         }));
                                     });

                                 })).Wait();
                             });
                    }));

                });
            }, () =>
            {
                return ValidateOkCancel();
            });
            _DeleteTagCommand = new DelegateCommand(async () =>
            {
                await Task.Run(async () =>
                {
                    await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        await Task.Run(async () =>
                          {
                              Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  await Task.Run(async () =>
                                  {
                                      if (SelectedTimeMarkStandardTag == null)
                                      {
                                          MessageBox.Show("Please select Tag to Edit", "No Tag Selected");
                                          return;
                                      }
                                      MessageBoxResult result = MessageBox.Show("Are you sure Delete Tag?", "Delete Tag", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                      if (result == MessageBoxResult.Yes)
                                      {
                                          ServiceRequest_TimeMarkStandardTag serviceObj = new ServiceRequest_TimeMarkStandardTag();
                                          serviceObj.SelectedTimeMarkStandardTagList = new List<EventTimeMarkStandardTag>();
                                          //serviceObj.EventID = SelectedManageTimeMarkStandard.EventID.Value;
                                          serviceObj.SelectedTimeMarkStandardTagList.Add(SelectedTimeMarkStandardTag);
                                          AuthorizedResponse<TimeMarkStandardTagResult> Response = await EventManager.DeleteTimeMarkStandardTag(SelectedTimeMarkStandardTag.TimeMarkStandardTagID);
                                          if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                                          {
                                              MessageBox.Show("Error while editing the Tag", "Tag Edit Error");
                                              return;
                                          }
                                          await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                          {
                                              this.TimeMarkStandardTagList.Remove(SelectedTimeMarkStandardTag);
                                          }));
                                      }

                                  });

                              })).Wait();
                          });
                    }));

                });
            }, () =>
            {
                return ValidateOkCancel();
            });

            _PrintCommand = new DelegateCommand(async () =>
            {
                System.Windows.Controls.PrintDialog pdPrintDialog = new System.Windows.Controls.PrintDialog();

                if ((bool)pdPrintDialog.ShowDialog().GetValueOrDefault())
                {
                    Size pntPageSize = new Size(pdPrintDialog.PrintableAreaWidth, pdPrintDialog.PrintableAreaHeight);
                    recordGrid.Measure(pntPageSize);
                    recordGrid.Arrange(new Rect(5, 5, pntPageSize.Width, pntPageSize.Height));
                    pdPrintDialog.PrintVisual(recordGrid, "Time/Mark Standard");
                }
            });


            foreach (var item in EventManager.GetAllTimeMarkStandard().Result.ToList())
            {
                ManageTimeMarkStandardListAll.Add(item);
            }
            ManageTimeMarkStandardList = ManageTimeMarkStandardListAll;
        }

        private void ViewCommandCommandHandler()
        {
        }

        private void CreateReportCommandCommandHandler()
        {
            DataTable dt = ReportTemplateGenerator.ToDataTable<EventTimeMarkStandard>(ManageTimeMarkStandardList);
            if (dt != null)
            {
                string ReportTemplate = ReportTemplateGenerator.GenerateTemplate("TimeMarkStandard List", "TimeMarkStandard List", ReportTemplateGenerator.GetColumnList<EventTimeMarkStandard>());
                Window window = new ReportView(ReportTemplate, dt);
                window.ShowDialog();
            }
        }


        private void ShowCommandCommandHandler()
        {
        }

        private void ExportToCommandHandler()
        {
            ExportToExcel<EventTimeMarkStandard, List<EventTimeMarkStandard>> s = new ExportToExcel<EventTimeMarkStandard, List<EventTimeMarkStandard>>();
            s.dataToPrint = ManageTimeMarkStandardList;
            s.GenerateReport();
        }

        private void ImportToCommandHandler()
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.DefaultExt = ".xlsx";
            openfile.Filter = "(.xlsx)|*.xlsx";
            var browsefile = openfile.ShowDialog();
            if (browsefile == true)
            {
                DataTable dt = ExportFromExcel.ExportFrom(openfile.FileName);
                ManageTimeMarkStandardListAll = new List<EventTimeMarkStandard>();
                foreach (DataRow item in dt.Rows)
                {
                    EventTimeMarkStandard recordList = new EventTimeMarkStandard();
                    recordList.EventID = Convert.ToInt32(item.ItemArray[0]);
                    recordList.EventName = Convert.ToString(item.ItemArray[1]);

                    int timespane1 = 0, timespane2 = 0, timespane3 = 0;
                    Int32.TryParse(item.ItemArray[2].ToString(), out timespane1);
                    Int32.TryParse(item.ItemArray[3].ToString(), out timespane2);
                    Int32.TryParse(item.ItemArray[4].ToString(), out timespane3);
                    recordList.EventTimeMarkStandardIDTagValue = new TimeSpan(timespane1, timespane2, timespane3);
                    recordList.Gender = Convert.ToString(item.ItemArray[5]);
                    recordList.StartAgeRange = Convert.ToInt16(item.ItemArray[6]);
                    recordList.EndAgeRange = Convert.ToInt16(item.ItemArray[7]);
                    recordList.EventType = Convert.ToString(item.ItemArray[8]);
                    recordList.EventTimeMarkStandardTagID = this.GetTagID(dt.Columns[2].ColumnName);


                    ManageTimeMarkStandardListAll.Add(recordList);
                }
                ManageTimeMarkStandardList = ManageTimeMarkStandardListAll;
                // recordGrid.ItemsSource = dt.DefaultView;
            }
        }

        private int? GetTagID(string columnName)
        {
            if (TimeMarkStandardTagList == null) return null;
            EventTimeMarkStandardTag tag = TimeMarkStandardTagList.Where(p => p.TimeMarkStandardTagName == columnName).FirstOrDefault();
            if (tag == null)
            {
                return 0;
            }
            else
            {
                return tag.TimeMarkStandardTagID;
            }
        }

        #region Properties
        private bool _IsShowRecordsForSelectedOnly;
        public bool IsShowRecordsForSelectedOnly
        {
            get { return _IsShowRecordsForSelectedOnly; }
            set
            {
                SetProperty(ref _IsShowRecordsForSelectedOnly, value);
            }
        }
        private ObservableCollection<EventMenuItemViewModel> _MenuItems;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }

        private ObservableCollection<EventTimeMarkStandardTag> _TimeMarkStandardTagList;
        public ObservableCollection<EventTimeMarkStandardTag> TimeMarkStandardTagList
        {
            get { return _TimeMarkStandardTagList; }
            set
            {
                SetProperty(ref _TimeMarkStandardTagList, value);
            }
        }
        private List<EventTimeMarkStandard> _ManageTimeMarkStandardList;
        public List<EventTimeMarkStandard> ManageTimeMarkStandardList
        {
            get { return _ManageTimeMarkStandardList; }
            set
            {
                SetProperty(ref _ManageTimeMarkStandardList, value);
            }
        }

        private List<EventTimeMarkStandard> _ManageTimeMarkStandardListAll = new List<EventTimeMarkStandard>();
        public List<EventTimeMarkStandard> ManageTimeMarkStandardListAll
        {
            get { return _ManageTimeMarkStandardListAll; }
            set
            {
                SetProperty(ref _ManageTimeMarkStandardListAll, value);
            }
        }

        private EventTimeMarkStandard _SelectedManageTimeMarkStandard;
        public EventTimeMarkStandard SelectedManageTimeMarkStandard
        {
            get { return _SelectedManageTimeMarkStandard; }
            set
            {
                SetProperty(ref _SelectedManageTimeMarkStandard, value);
                this.GetTimeMarkStandardTag();
            }
        }

        private void GetTimeMarkStandardTag()
        {
            if (SelectedManageTimeMarkStandard == null) return;
            List<EventTimeMarkStandardTag> tagList = EventManager.GetAllTimeMarkStandardTag().Result.ToList();
            TimeMarkStandardTagList = new ObservableCollection<EventTimeMarkStandardTag>();
            foreach (EventTimeMarkStandardTag item in tagList.Where(p => p.TimeMarkStandardEventName == SelectedManageTimeMarkStandard.EventName))
            {
                TimeMarkStandardTagList.Add(item);
            }
        }

        private EventTimeMarkStandardTag _SelectedTimeMarkStandardTag;
        public EventTimeMarkStandardTag SelectedTimeMarkStandardTag
        {
            get { return _SelectedTimeMarkStandardTag; }
            set
            {
                SetProperty(ref _SelectedTimeMarkStandardTag, value);
            }
        }
        private bool _IsFemaleChecked;
        public bool IsFemaleChecked
        {
            get { return _IsFemaleChecked; }
            set
            {
                SetProperty(ref _IsFemaleChecked, value);
                FilterRecord();
            }
        }

        private bool _IsMaleChecked;
        public bool IsMaleChecked
        {
            get { return _IsMaleChecked; }
            set
            {
                SetProperty(ref _IsMaleChecked, value);
                FilterRecord();
            }
        }
        private bool _IsAllChecked;
        public bool IsAllChecked
        {
            get { return _IsAllChecked; }
            set
            {
                SetProperty(ref _IsAllChecked, value);
                FilterRecord();
            }
        }
        private bool _IsMixedChecked;
        public bool IsMixedChecked
        {
            get { return _IsMixedChecked; }
            set
            {
                SetProperty(ref _IsMixedChecked, value);
                FilterRecord();
            }
        }
        private int _StartAgeRange;
        public int StartAgeRange
        {
            get { return _StartAgeRange; }
            set
            {
                SetProperty(ref _StartAgeRange, value);
                FilterRecord();
            }
        }
        private int _EndAgeRange;
        public int EndAgeRange
        {
            get { return _EndAgeRange; }
            set
            {
                SetProperty(ref _EndAgeRange, value);
                FilterRecord();
            }
        }
        private bool _IsRelayOnlyChecked;
        public bool IsRelayOnlyChecked
        {
            get { return _IsRelayOnlyChecked; }
            set
            {
                SetProperty(ref _IsRelayOnlyChecked, value);
                FilterRecord();
            }
        }

        private bool _IsIndvOnlyChecked;
        public bool IsIndvOnlyChecked
        {
            get { return _IsIndvOnlyChecked; }
            set
            {
                SetProperty(ref _IsIndvOnlyChecked, value);
                FilterRecord();
            }
        }

        private bool _IsIndvRelayChecked;
        public bool IsIndvRelayChecked
        {
            get { return _IsIndvRelayChecked; }
            set
            {
                SetProperty(ref _IsIndvRelayChecked, value);
                FilterRecord();
            }
        }
        private bool _IsByEventChecked;
        public bool IsByEventChecked
        {
            get { return _IsByEventChecked; }
            set
            {
                SetProperty(ref _IsByEventChecked, value);
                FilterRecord();
            }
        }
        private bool _IsByAgeGroupChecked;
        public bool IsByAgeGroupChecked
        {
            get { return _IsByAgeGroupChecked; }
            set
            {
                SetProperty(ref _IsByAgeGroupChecked, value);
                FilterRecord();
            }
        }
        private string _TagName;
        public string TagName
        {
            get { return _TagName; }
            set
            {
                SetProperty(ref _TagName, value);
            }
        }
        private void FilterRecord()
        {
            if (this.ManageTimeMarkStandardListAll == null) return;
            List<EventTimeMarkStandard> tempRecordList = this.ManageTimeMarkStandardListAll;
            //if (SelectedTimeMarkStandardTag != null)
            //{
            //    tempRecordList = tempRecordList.Where(p => p.EventTimeMarkStandardTagID == SelectedTimeMarkStandardTag.TimeMarkStandardTagID).ToList();
            //}
            if (IsAllChecked)
            {
            }
            else if (IsFemaleChecked)
            {
                tempRecordList = tempRecordList.Where(p => p.Gender.ToUpper() == "FEMALE").ToList();
            }
            else if (IsMaleChecked)
            {
                tempRecordList = tempRecordList.Where(p => p.Gender.ToUpper() == "MALE").ToList();
            }
            else if (IsMixedChecked)
            {
                tempRecordList = tempRecordList.Where(p => p.Gender.ToUpper() == "MIXED").ToList();
            }
            if (StartAgeRange > 0)
            {
                tempRecordList = tempRecordList.Where(p => p.StartAgeRange >= StartAgeRange).ToList();
            }
            if (EndAgeRange > 0)
            {
                tempRecordList = tempRecordList.Where(p => p.EndAgeRange >= EndAgeRange).ToList();
            }
            if (IsByAgeGroupChecked)
            {
                tempRecordList = tempRecordList.OrderBy(p => p.StartAgeRange).ToList();
            }
            else
            {
                tempRecordList = tempRecordList.OrderBy(p => p.EventID).ToList();
            }
            ManageTimeMarkStandardList = tempRecordList;
        }

        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(ManageTimeMarkStandardViewModel entity)
        {


        }
        public ManageTimeMarkStandardViewModel GetEntity()
        {

            return this;
        }


        #region Command
        private ICommand _ImportToCommand;
        public ICommand ImportToCommand
        {
            get { return _ImportToCommand; }
        }

        private ICommand _ExportToCommand;
        public ICommand ExportToCommand
        {
            get { return _ExportToCommand; }
        }

        private ICommand _ShowCommand;
        public ICommand ShowCommand
        {
            get { return _ShowCommand; }
        }

        private ICommand _PrintCommand;
        public ICommand PrintCommand
        {
            get { return _PrintCommand; }
        }

        private ICommand _ArchiveCommand;
        public ICommand ArchiveCommand
        {
            get { return _ArchiveCommand; }
        }

        private ICommand _RefreshCommand;
        public ICommand RefreshCommand
        {
            get { return _RefreshCommand; }
        }
        private ICommand _CreateReportCommand;
        public ICommand CreateReportCommand
        {
            get { return _CreateReportCommand; }
        }

        private ICommand _AddTagCommand;
        public ICommand AddTagCommand
        {
            get { return _AddTagCommand; }
        }

        private ICommand _EditTagCommand;
        public ICommand EditTagCommand
        {
            get { return _EditTagCommand; }
        }

        private ICommand _DeleteTagCommand;
        public ICommand DeleteTagCommand
        {
            get { return _DeleteTagCommand; }
        }

        #endregion
    }

}

