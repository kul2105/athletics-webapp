﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{

    public class AddTimeMarkStandardViewModel : BindableBase, IAddTimeMarkStandardViewModel
    {
        private IEventManager EventManager;
        public AddTimeMarkStandardViewModel()
        {
            EventManager = new SetupMeetManager.EventManager();
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                {
                                    await Task.Run(async () =>
                                    {
                                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                        {
                                            await Task.Run(async () =>
                                            {
                                                ServiceRequest_TimeMarkStandardTagMaster serviceObj = new ServiceRequest_TimeMarkStandardTagMaster();
                                                serviceObj.TimeMarkStandardTag = new TimeMarkStandardTagMaster();
                                                serviceObj.TimeMarkStandardTag.StandardPerpose = this.TagDescription;
                                                serviceObj.TimeMarkStandardTag.StandardTagName = this.TagName;
                                                serviceObj.TimeMarkStandardTag.StandardTime = this.TagTimeSpan;
                                                AuthorizedResponse<TimeMarkStandardTagMasterResult> Response = await EventManager.CreateTimeMarkStandardTagMaster(serviceObj);
                                                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                                                {
                                                    MessageBox.Show("Error while adding the Tag", "Tag Add Error");
                                                    return;
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Time Mark Standard Added successfully");
                                                    return;
                                                }

                                            });

                                        })).Wait();
                                    });
                                }));
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "AddTimeMarkStandardView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "AddTimeMarkStandardView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

        }

        #region Properties

       

        private int _RecordTagID;
        public int RecordTagID
        {
            get { return _RecordTagID; }
            set
            {
                SetProperty(ref _RecordTagID, value);
            }
        }
        private string _TagName;
        public string TagName
        {
            get { return _TagName; }
            set
            {
                SetProperty(ref _TagName, value);
            }
        }
        private string _TagDescription;
        public string TagDescription
        {
            get { return _TagDescription; }
            set
            {
                SetProperty(ref _TagDescription, value);
            }
        }

        private TimeSpan _TagTimeSpan;
        public TimeSpan TagTimeSpan
        {
            get { return _TagTimeSpan; }
            set
            {
                SetProperty(ref _TagTimeSpan, value);
            }
        }

        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }
        #endregion
    }

}

