﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using EventManager = System.Windows.EventManager;

namespace Athletics.Athletes.ViewModels
{

    public class EventTimeMarkStandardTagView : EventTimeMarkStandardTag, INotifyPropertyChanged
    {

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        private bool _AddToEvent;
        public bool AddToEvent
        {
            get { return _AddToEvent; }
            set
            {
                _AddToEvent = value;
                OnPropertyChanged("AddToEvent");
            }
        }

        private bool _IsForTimeMark;
        public bool IsForTimeMark
        {
            get { return _IsForTimeMark; }
            set
            {
                _IsForTimeMark = value;
                OnPropertyChanged("IsForTimeMark");
            }
        }

        private bool _IsForEntryQual;
        public bool IsForEntryQual
        {
            get { return _IsForEntryQual; }
            set
            {
                _IsForEntryQual = value;
                OnPropertyChanged("IsForEntryQual");
            }
        }
    }

    public class TimeMarkStandardViewModel : BindableBase, ITimeMarkStandardViewModel
    {
        IEventManager eventManger = null;
        public TimeMarkStandardViewModel(IEventManager eventManager, bool isInEditMode)
        {
            eventManger = eventManager;
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "AddTimeMarkStandardView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                foreach (Window item in Application.Current.Windows)
                                {
                                    if (item.Name == "AddTimeMarkStandardView")
                                    {
                                        item.Close();
                                        break;
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });
            if (!isInEditMode)
            {
                Task.Run(async () =>
                                       {
                                           Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                                           {
                                               await Task.Run(async () =>
                                               {
                                                   List<TimeMarkStandardTagMaster> masterList = eventManger.GetAllTimeMarkStandardTagMaster().Result;
                                                   EventTimeMarkStandardTagList = new List<EventTimeMarkStandardTagView>();
                                                   foreach (TimeMarkStandardTagMaster item in masterList)
                                                   {
                                                       EventTimeMarkStandardTagView eventTag = new EventTimeMarkStandardTagView();
                                                       eventTag.TimeMarkStandardTagName = item.StandardTagName;
                                                       eventTag.TimeMarkStandardEventName = item.StandardPerpose;
                                                       EventTimeMarkStandardTagList.Add(eventTag);
                                                   }

                                               });

                                           })).Wait();
                                       });
            }

        }

        #region Properties

        private List<EventTimeMarkStandardTagView> _EventTimeMarkStandardTagList;
        public List<EventTimeMarkStandardTagView> EventTimeMarkStandardTagList
        {
            get { return _EventTimeMarkStandardTagList; }
            set
            {
                SetProperty(ref _EventTimeMarkStandardTagList, value);
            }
        }

        private EventTimeMarkStandardTagView _SelectedEventTimeMarkStandardTag;
        public EventTimeMarkStandardTagView SelectedEventTimeMarkStandardTag
        {
            get { return _SelectedEventTimeMarkStandardTag; }
            set
            {
                if (value == null)
                {
                    value = new EventTimeMarkStandardTagView();
                    SetProperty(ref _SelectedEventTimeMarkStandardTag, value);
                }
                else
                {
                    SetProperty(ref _SelectedEventTimeMarkStandardTag, value);
                }

            }
        }

        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }
        #endregion
    }

}

