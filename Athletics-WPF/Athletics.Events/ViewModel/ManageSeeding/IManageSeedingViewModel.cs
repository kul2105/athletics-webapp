﻿using Athletics.Athletes.SetupMeetManager;
using Athletics.MeetSetup.Services.API;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Athletics.Athletes.ViewModels
{
    public interface IManageSeedingViewModel
    {
        ObservableCollection<SeedingSession> SeedingSessionList { get; set; }
        void EditCommandHandler();
    }
}
