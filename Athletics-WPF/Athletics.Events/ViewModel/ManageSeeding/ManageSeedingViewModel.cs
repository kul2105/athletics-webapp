﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class SeedingSession : BindableBase
    {
        private bool _Seed;
        public bool Seed
        {
            get { return _Seed; }
            set
            {
                SetProperty(ref _Seed, value);
            }
        }
        private string _EventNumber;
        public string EventNumber
        {
            get { return _EventNumber; }
            set
            {
                SetProperty(ref _EventNumber, value);
            }
        }
        private string _RnD;
        public string RND
        {
            get { return _RnD; }
            set
            {
                SetProperty(ref _RnD, value);
            }
        }
        private bool _Mannual;
        public bool Mannual
        {
            get { return _Mannual; }
            set
            {
                SetProperty(ref _Mannual, value);
            }
        }
        private string _EventName;
        public string EventName
        {
            get { return _EventName; }
            set
            {
                SetProperty(ref _EventName, value);
            }
        }
        private string _Entries;
        public string Entries
        {
            get { return _Entries; }
            set
            {
                SetProperty(ref _Entries, value);
            }
        }
        private string _SrC;
        public string SRC
        {
            get { return _SrC; }
            set
            {
                SetProperty(ref _SrC, value);
            }
        }
        private string _Heat;
        public string Heat
        {
            get { return _Heat; }
            set
            {
                SetProperty(ref _Heat, value);
            }
        }
        private string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                SetProperty(ref _Status, value);
            }
        }
        private int _EventID;
        public int EventID
        {
            get { return _EventID; }
            set
            {
                SetProperty(ref _EventID, value);
            }
        }
        private bool _IsEditable=false;
        public bool IsEditable
        {
            get { return _IsEditable; }
            set
            {
                SetProperty(ref _IsEditable, value);
            }
        }
    }

    public class ManageSeedingViewModel : BindableBase, IManageSeedingViewModel, IGenericInteractionView<ManageSeedingViewModel>
    {
        private IManageSeedingView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public ManageSeedingViewModel(IUnityContainer unity, IManageSeedingView view, IEventManager eventManager)
        {
            View = view;
            unityContainer = unity;
            EventManager = eventManager;
            View.DataContext = this;
            List<AddSessionViewModel> SessionList = EventManager.GetAllSessions().Result.ToList();
            foreach (AddSessionViewModel item in SessionList)
            {
                SessionDetailList.Add(item);
            }
            List<AddEventViewModel> EventDetailList = EventManager.GetAllEvent().Result.ToList();
            foreach (AddEventViewModel item in EventDetailList)
            {
                EventList.Add(item);
            }
            List<EventInSession> EventInSessionList = EventManager.GetAllEventInSession().Result.ToList();
            foreach (EventInSession item in EventInSessionList)
            {
                EventinSessionList.Add(item);
            }
        }

        private bool ValidateInputs()
        {
            return true;

        }

        public void SetEntity(ManageSeedingViewModel entity)
        {

        }
        private List<EventInSession> _EventinSessionList = new List<EventInSession>();
        public List<EventInSession> EventinSessionList
        {
            get { return _EventinSessionList; }
            set
            {
                SetProperty(ref _EventinSessionList, value);
            }
        }
        private List<AddEventViewModel> _EventList = new List<AddEventViewModel>();
        public List<AddEventViewModel> EventList
        {
            get { return _EventList; }
            set
            {
                SetProperty(ref _EventList, value);
            }
        }
        private ObservableCollection<AddSessionViewModel> _SessionDetailList = new ObservableCollection<AddSessionViewModel>();
        public ObservableCollection<AddSessionViewModel> SessionDetailList
        {
            get { return _SessionDetailList; }
            set
            {
                SetProperty(ref _SessionDetailList, value);
            }
        }
        private AddSessionViewModel _SelectedSessionDetail;
        public AddSessionViewModel SelectedSessionDetail
        {
            get { return _SelectedSessionDetail; }
            set
            {
                SetProperty(ref _SelectedSessionDetail, value);
                GetSessionSchedule();
            }
        }
        private ObservableCollection<SeedingSession> _SeedingSessionList = new ObservableCollection<SeedingSession>();
        public ObservableCollection<SeedingSession> SeedingSessionList
        {
            get { return _SeedingSessionList; }
            set
            {
                SetProperty(ref _SeedingSessionList, value);
            }
        }
        private SeedingSession _SelectedSeedingSession;
        public SeedingSession SelectedSeedingSession
        {
            get { return _SelectedSeedingSession; }
            set
            {
                SetProperty(ref _SelectedSeedingSession, value);
            }
        }

        private ObservableCollection<EventMenuItemViewModel> _MenuItems;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }
        public ManageSeedingViewModel GetEntity()
        {
            return this;
        }
        public void GetSessionSchedule()
        {
            SeedingSessionList.Clear();
            if (SelectedSessionDetail == null) return;
            List<EventSessionSchedule> listofScheduld = EventManager.GetAllSessionSchedules(SelectedSessionDetail.SessionID).Result.ToList();
            foreach (EventSessionSchedule item in listofScheduld.OrderBy(p => p.SessionOrder))
            {
                SeedingSession session = new SeedingSession();
                List<EventInSession> EventInSessionList = EventinSessionList.Where(p => p.SessionScheduleID == item.SessionScheduleID).ToList();
                foreach (var sessionInEvent in EventInSessionList)
                {
                    List<AddEventViewModel> eventList = EventList.Where(p => p.EventID == sessionInEvent.EventID).ToList();
                    foreach (var Event in eventList)
                    {
                        session.Entries = Event.Entries;
                        session.EventName = Event.Event;
                        session.EventNumber = Event.EventNumber;
                        session.Heat = Event.HeatsInQtrs.ToString();
                        session.RND = Event.Rnds;
                        session.SRC = Event.HeatInSemis.ToString();
                        session.Status = Event.Status;
                        session.Entries = Event.Entries;
                        session.EventID = Event.EventID;
                        if(Event.Status== "Seeded")
                        {
                            session.Seed = true;
                        }
                        else
                        {
                            session.Seed = false;
                        }
                        SeedingSessionList.Add(session);
                    }


                }

            }

        }

        public void EditCommandHandler()
        {
            foreach (var item in SeedingSessionList)
            {
                AddEventViewModel eventDetail = EventList.Where(p => p.EventID == item.EventID).FirstOrDefault();
                if (eventDetail != null)
                {
                    ServiceRequest_Event serviceReqEvent = new ServiceRequest_Event();
                    if (item.Seed)
                    {
                        eventDetail.Status = "Seeded";
                    }
                    else
                    {
                        eventDetail.Status = "Un-Seeded";
                    }
                    serviceReqEvent.Event = eventDetail;
                    AuthorizedResponse<EventResult> response = EventManager.UpdateEvent(serviceReqEvent).Result;
                    if (response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show("Error while Editing Event Status", "Edit Event Status Error");

                    }

                }

            }
        }
    }

}

