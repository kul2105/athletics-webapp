﻿using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class SeedingMenuViewModel : BindableBase, ISeedingMenuViewModel
    {
        private ObservableCollection<EventMenuItemViewModel> _MenuItems = new ObservableCollection<EventMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IEventManager eventManager;
        IManageSeedingView View;
        IManageSeedingViewModel ViewModel;
        ISeedingMenuView SeedingMenuView;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public SeedingMenuViewModel(IUnityContainer unity, ISeedingMenuView _View, IManageSeedingView ManageSeadingView, IEventManager manager)
        {
            eventManager = manager;
            View = ManageSeadingView;
            ViewModel = ManageSeadingView.DataContext as IManageSeedingViewModel;
            SeedingMenuView = _View;
            SeedingMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<EventMenuItemViewModel>();
                EventMenuItemViewModel eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Start Seeding";
                _StartSeedingCommand = new MeetCommandViewModel(StartSeedingCommandHandler);
                eventMenu.Command = StartSeedingCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Select All";
                _SelectAllCommand = new MeetCommandViewModel(SelectAllCommandHandler);
                eventMenu.Command = SelectAllCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "De-Select All";
                _DeSelectAllCommand = new MeetCommandViewModel(DeSelectAllCommandHandler);
                eventMenu.Command = DeSelectAllCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Preview";
                _PreviewCommand = new MeetCommandViewModel(PreviewCommandHandler);
                eventMenu.Command = PreviewCommand;
                MenuItems.Add(eventMenu);


                #endregion

            });
            unityContainer = unity;

        }


        #region Menu Command


        private ICommand _StartSeedingCommand;
        public ICommand StartSeedingCommand
        {
            get
            {
                return _StartSeedingCommand;
            }
        }

        private ICommand _SelectAllCommand;
        public ICommand SelectAllCommand
        {
            get
            {
                return _SelectAllCommand;
            }
        }
        private ICommand _DeSelectAllCommand;
        public ICommand DeSelectAllCommand
        {
            get
            {
                return _DeSelectAllCommand;
            }
        }

        private ICommand _PreviewCommand;
        public ICommand PreviewCommand
        {
            get
            {
                return _PreviewCommand;
            }
        }



        #endregion

        #region Command Handler

        private void PreviewCommandHandler()
        {
        }
        private void DeSelectAllCommandHandler()
        {
            foreach (var item in ViewModel.SeedingSessionList)
            {
                item.Seed = false;
            }
        }

        private void SelectAllCommandHandler()
        {
            foreach (var item in ViewModel.SeedingSessionList)
            {
                item.Seed = true;
            }

        }
        private void StartSeedingCommandHandler()
        {
            ViewModel.EditCommandHandler();

        }



        #endregion

    }
}

