﻿using Athletics.Athletes.SetupMeetManager;
using Athletics.MeetSetup.Services.API;
using Athletics.SetupMeet.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Athletics.Athletes.ViewModels
{
    public interface IManageEventViewModel
    {
        ObservableCollection<AddEventViewModel> EventDetailList { get; set; }
        AddEventViewModel SelectedEventDetail { get; set; }
        MeetSetupModel SelectedMeet { get; set; }
        void RefreshEvent();
    }
}
