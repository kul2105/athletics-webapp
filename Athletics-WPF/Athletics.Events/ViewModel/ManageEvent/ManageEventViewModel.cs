﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Athletics.SetupMeet.Model;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.MeetSetup.Views;
using Athletics.MeetSetup.ViewModels;
using Microsoft.Practices.Prism.Commands;

namespace Athletics.Athletes.ViewModels
{

    public class ManageEventViewModel : BindableBase, IManageEventViewModel, IGenericInteractionView<ManageEventViewModel>
    {
        private IManageEventView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        IMeetSetupManage MeetSetupManage;
        List<EventInMeet> EventInMeet = null;
        public ManageEventViewModel(IUnityContainer unity, IManageEventView view, IEventManager eventManager, IMeetSetupManage meetSetupManage)
        {
            View = view;
            unityContainer = unity;
            EventManager = eventManager;
            View.DataContext = this;
            MeetSetupManage = meetSetupManage;


            _AddCommand = new Model.MeetCommandViewModel(AddCommandHandler);
            _EditCommand = new Model.MeetCommandViewModel(EditCommandHandler);
            _DeleteCommand = new Model.MeetCommandViewModel(DeleteCommandHandler);
            _CreateMeetCommand = new Microsoft.Practices.Prism.Commands.DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {

                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                            IGenericInteractionView<MeetSetupModel> MeetsetupViewModel = unity.Resolve<IMeetSetupViewModel>() as IGenericInteractionView<MeetSetupModel>;
                            if (MeetsetupViewModel != null)
                            {
                                //  MeetsetupViewModel.SetEntity(MeetsetupViewModel);
                                window = unityContainer.Resolve<IMeetSetupView>() as System.Windows.Window;
                                if (MeetsetupViewModel != null)
                                {
                                    dialog.BindView(window);
                                    dialog.BindViewModel(MeetsetupViewModel);
                                }
                                dialog.ShowDialog();
                                dialog.Close();
                                MeetSetupModel model = MeetsetupViewModel.GetEntity();
                                if (model != null)
                                {
                                    this.MeetSetupList.Add(model);
                                    ReloadMeet();
                                }
                            }



                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });
            _AssignPrefrencesMeetCommand = new Microsoft.Practices.Prism.Commands.DelegateCommand(async () =>
            {
                if (SelectedMeet != null)
                {
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = unityContainer.Resolve<IAssigPrefrencesView>() as System.Windows.Window;
                    IGenericInteractionView<MeetSetupModel> seedprefrences = unityContainer.Resolve<IAssigPrefrencesViewModel>() as IGenericInteractionView<MeetSetupModel>;
                    if (seedprefrences != null)
                    {
                        seedprefrences.SetEntity(this.SelectedMeet);
                        dialog.BindView(window);
                        dialog.BindViewModel(seedprefrences);
                    }

                    dialog.ShowDialog();
                    dialog.Close();
                    ReloadMeet();
                }
            });

            _DeleteMeetCommand = new DelegateCommand(async () =>
            {
                if (SelectedMeet != null)
                {
                    MeetSetup.SetupMeetManager.AuthorizedResponse<MeetSetupResult> Response = await meetSetupManage.DeleteMeet(SelectedMeet.MeetID);
                    this.MeetSetupList.Remove(SelectedMeet);
                    ReloadMeet();
                }
            });
            _UpdateMeetCommand = new DelegateCommand(async () =>
           {
               Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
               {
                   IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
                   IGenericInteractionView<MeetSetupModel> MeetsetupViewModel = unity.Resolve<IMeetSetupViewModel>() as IGenericInteractionView<MeetSetupModel>;
                   if (MeetsetupViewModel != null)
                   {
                       MeetsetupViewModel.SetEntity(this.SelectedMeet);
                       System.Windows.Window window = unityContainer.Resolve<IMeetSetupView>() as System.Windows.Window;
                       dialog.BindViewModel(MeetsetupViewModel);
                       dialog.BindView(window);
                   }
                   dialog.ShowDialog();
                   MeetSetupModel model = MeetsetupViewModel.GetEntity();
                   if (model != null)
                   {
                       MeetSetupModel existedMeet = this.MeetSetupList.Where(p => p.MeetID == SelectedMeet.MeetID).FirstOrDefault();
                       if (existedMeet != null)
                       {
                           existedMeet.MeetName = model.MeetName;
                           existedMeet.MeetName2 = model.MeetName2;
                           existedMeet.MeetStyleOption = model.MeetStyleOption;
                           existedMeet.MeetTypeDivisionOption = model.MeetTypeDivisionOption;
                           existedMeet.MeetTypeOption = model.MeetTypeOption;
                           existedMeet.StartDate = model.StartDate;
                           existedMeet.AgeUpDate = model.AgeUpDate;
                           existedMeet.BaseCounty = model.BaseCounty;
                           existedMeet.EndDate = model.EndDate;
                           existedMeet.IsLinkTeamToDivision = model.IsLinkTeamToDivision;
                           existedMeet.IsUserDivisionBirthDate = model.IsUserDivisionBirthDate;
                           existedMeet.KindOfMeetOption = model.KindOfMeetOption;
                           existedMeet.MeetArenaOption = model.MeetArenaOption;
                           existedMeet.MeetClassOption = model.MeetClassOption;
                       }
                   }


               })).Wait();
           });

            _CopyEventFromTemplateCommand = new Microsoft.Practices.Prism.Commands.DelegateCommand(async () =>
            {
                if (SelectedEventDetail != null)
                {
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = unityContainer.Resolve<IEventTemplateView>() as System.Windows.Window;
                    window.Name = "EventTemplate";
                    IEventTemplateView IeventViewTemplate = window as IEventTemplateView;
                    EventTemplateViewModel eventTemplate = new EventTemplateViewModel(this.unityContainer, IeventViewTemplate, this.EventManager, this.MeetSetupList.ToList());
                    eventTemplate.SelectedAddEventViewModel = SelectedEventDetail;
                    //IGenericInteractionView<EventTemplateViewModel> eventTemplate = unityContainer.Resolve<IEventTemplateViewModel>() as IGenericInteractionView<EventTemplateViewModel>;
                    if (eventTemplate != null)
                    {
                        IEventTemplateViewModel IeventTemplate = eventTemplate as IEventTemplateViewModel;
                        if (IeventTemplate != null)
                        {
                            IeventTemplate.SelectedAddEventViewModel = SelectedEventDetail;
                            dialog.BindView(window);
                            dialog.BindViewModel(IeventTemplate);
                        }
                    }

                    dialog.ShowDialog();
                    dialog.Close();
                    RefreshEvent();
                }
            });

            _CopyMeetFromTemplateCommand = new Microsoft.Practices.Prism.Commands.DelegateCommand(async () =>
            {
                if (SelectedMeet != null)
                {
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = unityContainer.Resolve<IMeetSetupTemplateView>() as System.Windows.Window;
                    window.Name = "MeetTemplate";
                    IGenericInteractionView<MeetSetupTemplateViewModel> meetSetupTemplateVm = unityContainer.Resolve<IMeetSetupTemplateViewModel>() as IGenericInteractionView<MeetSetupTemplateViewModel>;
                    if (meetSetupTemplateVm != null)
                    {
                        IMeetSetupTemplateViewModel iIMeetSetupTemplateViewModel = meetSetupTemplateVm as IMeetSetupTemplateViewModel;
                        if (iIMeetSetupTemplateViewModel != null)
                        {
                            iIMeetSetupTemplateViewModel.SelectedMeetSetup = SelectedMeet;
                            dialog.BindView(window);
                            dialog.BindViewModel(meetSetupTemplateVm);
                        }

                    }

                    dialog.ShowDialog();
                    dialog.Close();
                    ReloadMeet();
                }
            });
            ReloadMeet();
        }

        private bool ValidateMeets()
        {
            return this.SelectedMeet == null ? false : true;
        }

        public void RefreshEvent()
        {
            LoadEvents();
        }
        private void ReloadMeet()
        {
            this.MeetSetupList.Clear();
            List<MeetSetupModel> modelM = (from t1 in MeetSetupManage.GetAllMeetSetup().Result
                                           join t2 in MeetSetupManage.GetAllBaseCounty().Result
                                           on t1.BaseCountyID equals t2.BaseCountyID
                                           join t3 in MeetSetupManage.GetAllMeetArena().Result
                                           on t1.MeetArenaID equals t3.MeetArenaID
                                           join t4 in MeetSetupManage.GetAllMeetClass().Result
                                           on t1.MeetClassID equals t4.MeetClassID
                                           join t5 in MeetSetupManage.GetAllMeetKind().Result
                                           on t1.MeetKindID equals t5.MeetKindID
                                           join t6 in MeetSetupManage.GetAllMeetStyle().Result
                                           on t1.MeetStyleID equals t6.MeetStyleID
                                           join t7 in MeetSetupManage.GetAllMeetType().Result
                                          on t1.MeetTypeID equals t7.MeetTypeID
                                           join t8 in MeetSetupManage.GetAllMeetTypeDivision().Result
                                          on t1.MeetTypeDivisionID equals t8.MeetTypeDivisionID
                                           select new MeetSetupModel
                                           {
                                               MeetID = t1.MeetSetupID,
                                               MeetName = t1.MeetSetupName,
                                               MeetName2 = t1.MeetSetupName2,
                                               BaseCounty = t2.BaseCountyName,
                                               MeetArenaOption = t3.MeetArenaName,
                                               MeetClassOption = t4.MeetClassName,
                                               KindOfMeetOption = t5.MeetKindName,
                                               AgeUpDate = t1.MeetSetupAgeUpDate,
                                               EndDate = t1.MeetSetupEndDate,
                                               IsLinkTeamToDivision = t1.LinkTermToDivision,
                                               IsUserDivisionBirthDate = t1.UseDivisionBirthdateRange,
                                               MeetStyleOption = t6.MeetStyleName,
                                               MeetTypeOption = t7.MeetTypeName,
                                               StartDate = t1.MeetSetupStartDate,
                                               MeetTypeDivisionOption = t8.MeetTypeDivisionName,
                                               SeedingPrefrencesGroup = this.GetSeeingPrfrenceGroupForMeet(t1.MeetSetupID),
                                               AthleteRelayPrefrencesGroup = this.GetAthleteRelayPrefrencesGroupForMeet(t1.MeetSetupID)
                                           }).Distinct().ToList();


            foreach (var item in modelM)
            {
                this.MeetSetupList.Add(item);
            }
        }

        #region Command Handler


        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        //if (AthleteRelayPreferencesId == 0)
                        //{
                        if (SelectedEventDetail == null) return;
                        SetupMeetManager.AuthorizedResponse<EventResult> Response = await EventManager.DeleteEvent(SelectedEventDetail.EventID);
                        if (Response.Status == SetupMeetManager.eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleteing the Event", "Event Delete Error");
                            return;
                        }
                        this.AthleteRelayPreferencesId = Response.Result.EventID;
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             this.EventDetailList.Remove(SelectedEventDetail);
                         }));
                        //else
                        //{
                        //}

                    });

                })).Wait();
            });

        }
        private AthleteRelayPrefrencesGroup GetAthleteRelayPrefrencesGroupForMeet(int MeetID)
        {
            AthleteRelayPrefrencesGroup opt = new AthleteRelayPrefrencesGroup();
            opt.AthletePreferencesList = new ObservableCollection<Option>();
            opt.RelayPreferencesList = new ObservableCollection<Option>();
            opt.CompetitorNumbersList = new ObservableCollection<Option>();
            foreach (var AthletePreference in MeetSetupManage.GetRelayPrefrencesMeetData(MeetID).Result.Select(p => p.AthletePreferencesName).Distinct())
            {
                Option optRelayPrefrences = new Option();
                optRelayPrefrences.Display = AthletePreference;
                opt.AthletePreferencesList.Add(optRelayPrefrences);
            }

            foreach (var RelayPreference in MeetSetupManage.GetRelayPrefrencesMeetData(MeetID).Result.Select(p => p.RelayPreferencesName).Distinct())
            {
                Option optRelayPrefrences = new Option();
                optRelayPrefrences.Display = RelayPreference;
                opt.RelayPreferencesList.Add(optRelayPrefrences);
            }

            foreach (var CompetitorNumber in MeetSetupManage.GetRelayPrefrencesMeetData(MeetID).Result.Select(p => p.CompetitorNumbersName).Distinct())
            {
                Option optRelayPrefrences = new Option();
                optRelayPrefrences.Display = CompetitorNumber;
                opt.CompetitorNumbersList.Add(optRelayPrefrences);
            }
            if (opt.AthletePreferencesList.Count <= 0 && opt.RelayPreferencesList.Count <= 0 && opt.CompetitorNumbersList.Count <= 0)
            {
                return null;
            }
            return opt;
        }
        private SeedingPrefrencesGroup GetSeeingPrfrenceGroupForMeet(int MeetID)
        {
            SeedingPrefrencesGroup opt = new SeedingPrefrencesGroup();
            opt.DualMeetList = new ObservableCollection<DualMeetSchoolTransaction>();
            opt.MeetAssignment = null;
            opt.RendomizationRule = null;
            opt.SeedingRules = null;
            opt.StandardAlleyPrefrencesList = new ObservableCollection<Entities.Models.StandardAlleyPrefrence>();
            opt.StandardLanePrefrencesList = new ObservableCollection<Entities.Models.StandardLanePrefrence>();
            opt.WaterfallStartPreferencesList = new ObservableCollection<Entities.Models.WaterfallStartPreference>();

            var rendomizeRules = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                    .Select(x => new { x.RendomizationRuleCloseGap, x.RendomizationRuleRoundFirstMultipleRound, x.RendomizationRuleRoundSecondThirdAndForth, x.RendomizationRuleTimedFinalEvents });

            var rendoizationRule = rendomizeRules.Distinct().FirstOrDefault();
            if (rendoizationRule != null)
            {
                opt.RendomizationRule = new Entities.Models.RendomizationRuleTransaction();
                opt.RendomizationRule.RoundFirstMultipleRound = rendoizationRule.RendomizationRuleRoundFirstMultipleRound;
                opt.RendomizationRule.RoundSecondThirdAndForth = rendoizationRule.RendomizationRuleRoundSecondThirdAndForth;
                opt.RendomizationRule.TimedFinalEvents = rendoizationRule.RendomizationRuleTimedFinalEvents;
                opt.RendomizationRule.CloseGap = rendoizationRule.RendomizationRuleCloseGap;
            }

            var dualMeetAssignList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                    .Select(x => new { x.DualMeetStrictAssignmentAllHeats, x.DualMeetAlternateUserOfUnAssignedLane, x.DualMeetStrictAssignmentFastestHeatOnly, x.DualMeetUseLaneAssignmentsForInLaneRacesOnly, x.DualMeetUserLaneOrPositionAssignmentsAbove });

            var dualMeetList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                     .Select(x => new { x.DualMeetLaneNumber, x.DualMeetSchoolAbbr, x.DualMeetSchoolName });

            foreach (var transaction in dualMeetList.Distinct())
            {
                DualMeetSchoolTransaction trans1 = new DualMeetSchoolTransaction();
                trans1.LaneNumber = transaction.DualMeetLaneNumber;
                trans1.SchoolAbb = transaction.DualMeetSchoolAbbr;
                trans1.SchoolName = transaction.DualMeetSchoolName;
                opt.DualMeetList.Add(trans1);
            }
            var trans = dualMeetAssignList.FirstOrDefault();
            if (trans != null)
            {
                opt.MeetAssignment = new Entities.Models.DualMeetAssignmentTransaction();
                opt.MeetAssignment.AlternamtUserOfUnAssignedLane = trans.DualMeetAlternateUserOfUnAssignedLane;
                opt.MeetAssignment.StrictAssignmentAllHeats = trans.DualMeetStrictAssignmentAllHeats;
                opt.MeetAssignment.StrictAssignmentFastestHeatOnly = trans.DualMeetStrictAssignmentFastestHeatOnly;
                opt.MeetAssignment.UseLaneAssignmentsForInLaneRacesOnly = trans.DualMeetUseLaneAssignmentsForInLaneRacesOnly;
                opt.MeetAssignment.UserLaneOrPositionAssignmentsAbove = trans.DualMeetUserLaneOrPositionAssignmentsAbove;
            }

            var SeedingRuleList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                   .Select(x => new { x.SeedingAllowForeignAthletesInFinal, x.SeedingRulesAllowExhibitionAthletesInFinal, x.SeedingRulesApplyNCAARule, x.SeedingRulesApplyNCAARule1, x.SeedingRulesUseSpecialRandomSelectMethod });

            var seedingRule = SeedingRuleList.Distinct().FirstOrDefault();

            if (seedingRule != null)
            {
                opt.SeedingRules = new Entities.Models.MeetSetupSeedingRule();
                opt.SeedingRules.AllowExhibitionAthletesInFinal = seedingRule.SeedingRulesAllowExhibitionAthletesInFinal;
                opt.SeedingRules.AllowForeignAthletesInFinal = seedingRule.SeedingAllowForeignAthletesInFinal;
                opt.SeedingRules.ApplyNCAARule = seedingRule.SeedingRulesApplyNCAARule;
                opt.SeedingRules.UseSpecialRandomSelectMethod = seedingRule.SeedingRulesUseSpecialRandomSelectMethod;
                opt.SeedingRules.SeedExhibitionAthletesLast = seedingRule.SeedingRulesApplyNCAARule1;
            }


            var StandardAlleyList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                .Select(x => new
                {
                    x.StandardAlleyPrefrencesRowEighthColumnEightthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnFifthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnFirstValue,
                    x.StandardAlleyPrefrencesRowEighthColumnForthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnNinthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnSecondValue,
                    x.StandardAlleyPrefrencesRowEighthColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowEighthColumnSixthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnTenthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnThirdValue,
                    x.StandardAlleyPrefrencesRowFifthColumnEightthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnFifthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnFirstValue,
                    x.StandardAlleyPrefrencesRowFifthColumnForthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnNinthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnSecondValue,
                    x.StandardAlleyPrefrencesRowFifthColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowFifthColumnSixthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnTenthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnThirdValue,
                    x.StandardAlleyPrefrencesRowForthColumnEightthValue,
                    x.StandardAlleyPrefrencesRowForthColumnFifthValue,
                    x.StandardAlleyPrefrencesRowForthColumnFirstValue,
                    x.StandardAlleyPrefrencesRowForthColumnForthValue,
                    x.StandardAlleyPrefrencesRowForthColumnNinthValue,
                    x.StandardAlleyPrefrencesRowForthColumnSecondValue,
                    x.StandardAlleyPrefrencesRowForthColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowForthColumnSixthValue,
                    x.StandardAlleyPrefrencesRowForthColumnTenthValue,
                    x.StandardAlleyPrefrencesRowForthColumnThirdValue,
                    x.StandardAlleyPrefrencesRowNineColumnEightthValue,
                    x.StandardAlleyPrefrencesRowNineColumnFifthValue,
                    x.StandardAlleyPrefrencesRowNineColumnFirstValue,
                    x.StandardAlleyPrefrencesRowNineColumnForthValue,
                    x.StandardAlleyPrefrencesRowNineColumnNinthValue,
                    x.StandardAlleyPrefrencesRowNineColumnSecondValue,
                    x.StandardAlleyPrefrencesRowNineColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowNineColumnSixthValue,
                    x.StandardAlleyPrefrencesRowNineColumnTenthValue,
                    x.StandardAlleyPrefrencesRowNineColumnThirdValue,
                    x.StandardAlleyPrefrencesRowOneColumnEightthValue,
                    x.StandardAlleyPrefrencesRowOneColumnFifthValue,
                    x.StandardAlleyPrefrencesRowOneColumnFirstValue,
                    x.StandardAlleyPrefrencesRowOneColumnForthValue,
                    x.StandardAlleyPrefrencesRowOneColumnNinthValue,
                    x.StandardAlleyPrefrencesRowOneColumnSecondValue,
                    x.StandardAlleyPrefrencesRowOneColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowOneColumnSixthValue,
                    x.StandardAlleyPrefrencesRowOneColumnTenthValue,
                    x.StandardAlleyPrefrencesRowOneColumnThirdValue,
                    x.StandardAlleyPrefrencesRowSecondColumnEightthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnFifthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnFirstValue,
                    x.StandardAlleyPrefrencesRowSecondColumnForthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnNinthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnSecondValue,
                    x.StandardAlleyPrefrencesRowSecondColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowSecondColumnSixthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnTenthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnThirdValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnEightthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnFifthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnFirstValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnForthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnNinthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnSecondValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnSixthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnTenthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnThirdValue,
                    x.StandardAlleyPrefrencesRowSixthColumnEightthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnFifthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnFirstValue,
                    x.StandardAlleyPrefrencesRowSixthColumnForthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnNinthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnSecondValue,
                    x.StandardAlleyPrefrencesRowSixthColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowSixthColumnSixthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnTenthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnThirdValue,
                    x.StandardAlleyPrefrencesRowThirdColumnEightthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnFifthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnFirstValue,
                    x.StandardAlleyPrefrencesRowThirdColumnForthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnNinthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnSecondValue,
                    x.StandardAlleyPrefrencesRowThirdColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowThirdColumnSixthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnThirdValue,
                    x.StandardAlleyPrefrencesRowThridColumnTenthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnEightthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnFifthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnFirstValue,
                    x.StandardAlleyPrefrencesRowZeroColumnForthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnNinthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnSecondValue,
                    x.StandardAlleyPrefrencesRowZeroColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowZeroColumnSixthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnTenthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnThirdValue,
                });

            Entities.Models.StandardAlleyPrefrence standardAlley = new Entities.Models.StandardAlleyPrefrence();
            foreach (var standredAlley in StandardAlleyList.Distinct())
            {
                Entities.Models.StandardAlleyPrefrence standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "1 Alley";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowZeroColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "2 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowOneColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowOneColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowOneColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowOneColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowOneColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "3 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowSecondColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "4 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowThirdColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowThridColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "5 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowForthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowForthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowForthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowForthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowForthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "6 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowFifthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "7 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowSixthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "8 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowSeventhColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "9 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowEighthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "10 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowNineColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowNineColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowNineColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowNineColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowNineColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


            }
            var StandardLaneList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                                    .Select(x => new
                                    {
                                        x.StandardLanePrefrencesRowEighthColumnEightthValue,
                                        x.StandardLanePrefrencesRowEighthColumnFifthValue,
                                        x.StandardLanePrefrencesRowEighthColumnFirstValue,
                                        x.StandardLanePrefrencesRowEighthColumnForthValue,
                                        x.StandardLanePrefrencesRowEighthColumnNinthValue,
                                        x.StandardLanePrefrencesRowEighthColumnSecondValue,
                                        x.StandardLanePrefrencesRowEighthColumnSeventhValue,
                                        x.StandardLanePrefrencesRowEighthColumnSixthValue,
                                        x.StandardLanePrefrencesRowEighthColumnTenthValue,
                                        x.StandardLanePrefrencesRowEighthColumnThirdValue,
                                        x.StandardLanePrefrencesRowFifthColumnEightthValue,
                                        x.StandardLanePrefrencesRowFifthColumnFifthValue,
                                        x.StandardLanePrefrencesRowFifthColumnFirstValue,
                                        x.StandardLanePrefrencesRowFifthColumnForthValue,
                                        x.StandardLanePrefrencesRowFifthColumnNinthValue,
                                        x.StandardLanePrefrencesRowFifthColumnSecondValue,
                                        x.StandardLanePrefrencesRowFifthColumnSeventhValue,
                                        x.StandardLanePrefrencesRowFifthColumnSixthValue,
                                        x.StandardLanePrefrencesRowFifthColumnTenthValue,
                                        x.StandardLanePrefrencesRowFifthColumnThirdValue,
                                        x.StandardLanePrefrencesRowForthColumnEightthValue,
                                        x.StandardLanePrefrencesRowForthColumnFifthValue,
                                        x.StandardLanePrefrencesRowForthColumnFirstValue,
                                        x.StandardLanePrefrencesRowForthColumnForthValue,
                                        x.StandardLanePrefrencesRowForthColumnNinthValue,
                                        x.StandardLanePrefrencesRowForthColumnSecondValue,
                                        x.StandardLanePrefrencesRowForthColumnSeventhValue,
                                        x.StandardLanePrefrencesRowForthColumnSixthValue,
                                        x.StandardLanePrefrencesRowForthColumnTenthValue,
                                        x.StandardLanePrefrencesRowForthColumnThirdValue,
                                        x.StandardLanePrefrencesRowNineColumnEightthValue,
                                        x.StandardLanePrefrencesRowNineColumnFifthValue,
                                        x.StandardLanePrefrencesRowNineColumnFirstValue,
                                        x.StandardLanePrefrencesRowNineColumnForthValue,
                                        x.StandardLanePrefrencesRowNineColumnNinthValue,
                                        x.StandardLanePrefrencesRowNineColumnSecondValue,
                                        x.StandardLanePrefrencesRowNineColumnSeventhValue,
                                        x.StandardLanePrefrencesRowNineColumnSixthValue,
                                        x.StandardLanePrefrencesRowNineColumnTenthValue,
                                        x.StandardLanePrefrencesRowNineColumnThirdValue,
                                        x.StandardLanePrefrencesRowOneColumnEightthValue,
                                        x.StandardLanePrefrencesRowOneColumnFifthValue,
                                        x.StandardLanePrefrencesRowOneColumnFirstValue,
                                        x.StandardLanePrefrencesRowOneColumnForthValue,
                                        x.StandardLanePrefrencesRowOneColumnNinthValue,
                                        x.StandardLanePrefrencesRowOneColumnSecondValue,
                                        x.StandardLanePrefrencesRowOneColumnSeventhValue,
                                        x.StandardLanePrefrencesRowOneColumnSixthValue,
                                        x.StandardLanePrefrencesRowOneColumnTenthValue,
                                        x.StandardLanePrefrencesRowOneColumnThirdValue,
                                        x.StandardLanePrefrencesRowSecondColumnEightthValue,
                                        x.StandardLanePrefrencesRowSecondColumnFifthValue,
                                        x.StandardLanePrefrencesRowSecondColumnFirstValue,
                                        x.StandardLanePrefrencesRowSecondColumnForthValue,
                                        x.StandardLanePrefrencesRowSecondColumnNinthValue,
                                        x.StandardLanePrefrencesRowSecondColumnSecondValue,
                                        x.StandardLanePrefrencesRowSecondColumnSeventhValue,
                                        x.StandardLanePrefrencesRowSecondColumnSixthValue,
                                        x.StandardLanePrefrencesRowSecondColumnTenthValue,
                                        x.StandardLanePrefrencesRowSecondColumnThirdValue,
                                        x.StandardLanePrefrencesRowSeventhColumnEightthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnFifthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnFirstValue,
                                        x.StandardLanePrefrencesRowSeventhColumnForthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnNinthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnSecondValue,
                                        x.StandardLanePrefrencesRowSeventhColumnSeventhValue,
                                        x.StandardLanePrefrencesRowSeventhColumnSixthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnTenthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnThirdValue,
                                        x.StandardLanePrefrencesRowSixthColumnEightthValue,
                                        x.StandardLanePrefrencesRowSixthColumnFifthValue,
                                        x.StandardLanePrefrencesRowSixthColumnFirstValue,
                                        x.StandardLanePrefrencesRowSixthColumnForthValue,
                                        x.StandardLanePrefrencesRowSixthColumnNinthValue,
                                        x.StandardLanePrefrencesRowSixthColumnSecondValue,
                                        x.StandardLanePrefrencesRowSixthColumnSeventhValue,
                                        x.StandardLanePrefrencesRowSixthColumnSixthValue,
                                        x.StandardLanePrefrencesRowSixthColumnTenthValue,
                                        x.StandardLanePrefrencesRowSixthColumnThirdValue,
                                        x.StandardLanePrefrencesRowThirdColumnEightthValue,
                                        x.StandardLanePrefrencesRowThirdColumnFifthValue,
                                        x.StandardLanePrefrencesRowThirdColumnFirstValue,
                                        x.StandardLanePrefrencesRowThirdColumnForthValue,
                                        x.StandardLanePrefrencesRowThirdColumnNinthValue,
                                        x.StandardLanePrefrencesRowThirdColumnSecondValue,
                                        x.StandardLanePrefrencesRowThirdColumnSeventhValue,
                                        x.StandardLanePrefrencesRowThirdColumnSixthValue,
                                        x.StandardLanePrefrencesRowThirdColumnThirdValue,
                                        x.StandardLanePrefrencesRowThridColumnTenthValue,
                                        x.StandardLanePrefrencesRowZeroColumnEightthValue,
                                        x.StandardLanePrefrencesRowZeroColumnFifthValue,
                                        x.StandardLanePrefrencesRowZeroColumnFirstValue,
                                        x.StandardLanePrefrencesRowZeroColumnForthValue,
                                        x.StandardLanePrefrencesRowZeroColumnNinthValue,
                                        x.StandardLanePrefrencesRowZeroColumnSecondValue,
                                        x.StandardLanePrefrencesRowZeroColumnSeventhValue,
                                        x.StandardLanePrefrencesRowZeroColumnSixthValue,
                                        x.StandardLanePrefrencesRowZeroColumnTenthValue,
                                        x.StandardLanePrefrencesRowZeroColumnThirdValue,
                                    });

            foreach (var standredAlley in StandardLaneList.Distinct())
            {
                Entities.Models.StandardLanePrefrence standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "1 Lane";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowZeroColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowZeroColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowZeroColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowZeroColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowZeroColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowZeroColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowZeroColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowZeroColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowZeroColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowZeroColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "2 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowOneColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowOneColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowOneColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowOneColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowOneColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowOneColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowOneColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowOneColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowOneColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowOneColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "3 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowSecondColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowSecondColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowSecondColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowSecondColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowSecondColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowSecondColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowSecondColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowSecondColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowSecondColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowSecondColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "4 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowThirdColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowThirdColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowThirdColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowThirdColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowThirdColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowThirdColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowThirdColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowThirdColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowThirdColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowThridColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "5 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowForthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowForthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowForthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowForthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowForthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowForthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowForthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowForthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowForthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowForthColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "6 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowFifthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowFifthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowFifthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowFifthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowFifthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowFifthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowFifthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowFifthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowFifthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowFifthColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "7 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowSixthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowSixthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowSixthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowSixthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowSixthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowSixthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowSixthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowSixthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowSixthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowSixthColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "8 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowSeventhColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowSeventhColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowSeventhColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowSeventhColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowSeventhColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "9 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowEighthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowEighthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowEighthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowEighthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowEighthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowEighthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowEighthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowEighthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowEighthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowEighthColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "10 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowNineColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowNineColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowNineColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowNineColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowNineColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowNineColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowNineColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowNineColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowNineColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowNineColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

            }

            var WaterfallList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
             .Select(x => new { x.WaterfallStartPosition, x.WaterfallStartRank });

            foreach (var waterfall in WaterfallList.Distinct())
            {
                Entities.Models.WaterfallStartPreference waterfallpref = new Entities.Models.WaterfallStartPreference();
                waterfallpref.Position = waterfall.WaterfallStartPosition;
                waterfallpref.Rank = waterfall.WaterfallStartRank;
                opt.WaterfallStartPreferencesList.Add(waterfallpref);
            }

            foreach (var schoolDetail in MeetSetupManage.GetAllSchoolDetail().Result)
            {
                opt.SchoolDetailList.Add(schoolDetail);
            }
            if (opt.DualMeetList.Count <= 0 && opt.MeetAssignment == null && opt.RendomizationRule == null && opt.SeedingRules == null
                && opt.StandardAlleyPrefrencesList.Count <= 0 && opt.StandardLanePrefrencesList.Count <= 0)
            {
                return null;
            }

            return opt;
        }
        private void EditCommandHandler()
        {
            if (SelectedEventDetail == null) return;
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddEventView();
            window.Name = "AddEvents";
            IAddEventView eventView = window as IAddEventView;
            AddEventViewModel addEventViewModel = new AddEventViewModel(this.unityContainer, eventView, this.EventManager);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            if (addEventViewModel != null)
            {
                IAddEventViewModel addEvent = addEventViewModel as IAddEventViewModel;
                if (addEvent != null)
                {
                    addEvent.EventDetailList = this.EventDetailList;
                }
                addEvent.SelectedEventDetail = SelectedEventDetail;
                dialog.BindView(window);
                dialog.BindViewModel(addEvent);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            addEventViewModel = addEventViewModel.GetEntity();
            if (addEventViewModel.IsCanceled == true) return;
            if (addEventViewModel.IsGirlsChecked)
            {
                addEventViewModel.Gender = "Girls";
            }
            else if (addEventViewModel.IsBoysChecked)
            {
                addEventViewModel.Gender = "Boys";
            }
            else if (addEventViewModel.IsMenChecked)
            {
                addEventViewModel.Gender = "Men";
            }
            else if (addEventViewModel.IsWomenChecked)
            {
                addEventViewModel.Gender = "Women";
            }
            else if (addEventViewModel.IsMixedChecked)
            {
                addEventViewModel.Gender = "Mixed";
            }
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        //if (AthleteRelayPreferencesId == 0)
                        //{

                        ServiceRequest_Event serviceObj = new ServiceRequest_Event();
                        serviceObj.Event = addEventViewModel;
                        SetupMeetManager.AuthorizedResponse<EventResult> Response = await EventManager.UpdateEvent(serviceObj);
                        if (Response.Status == SetupMeetManager.eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while Updating the Event", "Event Add Error");
                            return;
                        }
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             this.AthleteRelayPreferencesId = Response.Result.EventID;
                             this.EventDetailList.Remove(this.SelectedEventDetail);
                             this.EventDetailList.Add(addEventViewModel);
                             this.SelectedEventDetail = addEventViewModel;
                         }));
                        //}
                        //else
                        //{
                        //}

                    });

                })).Wait();
            });

        }

        private void AddCommandHandler()
        {
            if (this.SelectedMeet == null)
            {
                MessageBox.Show("Please Select the meet to Add event into meet.");
                return;
            }
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddEventView();
            window.Name = "AddEvents";
            IAddEventView eventView = window as IAddEventView;
            AddEventViewModel addEventViewModel = new AddEventViewModel(this.unityContainer, eventView, this.EventManager);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            this.ClearViewModel(addEventViewModel);
            if (addEventViewModel != null)
            {
                IAddEventViewModel addEvent = addEventViewModel as IAddEventViewModel;
                if (addEvent != null)
                {
                    addEvent.EventNumber = (Convert.ToInt32(this.EventDetailList.Max(p => p.EventNumber)) + 1).ToString();
                    addEvent.EventDetailList = this.EventDetailList;
                }
                dialog.BindView(window);
                dialog.BindViewModel(addEventViewModel);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            // AddEventViewModel addeventVm = addEventViewModel.GetEntity();
            if (addEventViewModel.IsGirlsChecked)
            {
                addEventViewModel.Gender = "Girls";
            }
            else if (addEventViewModel.IsBoysChecked)
            {
                addEventViewModel.Gender = "Boys";
            }
            else if (addEventViewModel.IsMenChecked)
            {
                addEventViewModel.Gender = "Men";
            }
            else if (addEventViewModel.IsWomenChecked)
            {
                addEventViewModel.Gender = "Women";
            }
            else if (addEventViewModel.IsMixedChecked)
            {
                addEventViewModel.Gender = "Mixed";
            }

            if (addEventViewModel.IsCanceled == true) return;
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        //if (AthleteRelayPreferencesId == 0)
                        //{

                        ServiceRequest_Event serviceObj = new ServiceRequest_Event();
                        serviceObj.Event = addEventViewModel;
                        SetupMeetManager.AuthorizedResponse<EventResult> Response = await EventManager.CreateNewEvent(serviceObj);
                        if (Response.Status == SetupMeetManager.eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while adding the Event", "Event Add Error");
                            return;
                        }
                        Response = await EventManager.AddEventInMeet(Response.Result.EventID, SelectedMeet.MeetID);
                        if (Response.Status == SetupMeetManager.eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while adding the Event into Meet", "Event Add Error");
                            return;
                        }
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            this.AthleteRelayPreferencesId = Response.Result.EventID;
                            this.EventDetailList.Add(addEventViewModel);
                        }));

                        //}
                        //else
                        //{
                        //}

                    });

                })).Wait();
            });

        }

        private void ClearViewModel(IGenericInteractionView<AddEventViewModel> addEventViewModel)
        {
            addEventViewModel.SetEntity(new AddEventViewModel());
        }

        private void LayoutCommandHandler()
        {
            //MessageBox.Show("Layout");
        }

        #endregion

        #region Filter Properties
        private ObservableCollection<MeetSetupModel> _MeetSetupList = new ObservableCollection<MeetSetupModel>();
        public ObservableCollection<MeetSetupModel> MeetSetupList
        {
            get { return _MeetSetupList; }
            set
            {
                SetProperty(ref _MeetSetupList, value);
            }
        }

        private MeetSetupModel _SelectedMeet;
        public MeetSetupModel SelectedMeet
        {
            get { return _SelectedMeet; }
            set
            {
                SetProperty(ref _SelectedMeet, value);
                this.LoadEvents();
            }
        }

        private void LoadEvents()
        {

            if (SelectedMeet == null) return;
            List<AddEventViewModel> EventList = EventManager.GetAllEvent().Result.ToList();
            EventInMeet = EventManager.GetAllEventInMeet().Result.ToList();
            if (EventInMeet == null) return;
            List<int?> list = EventInMeet.Where(p => p.MeetID == SelectedMeet.MeetID).Select(p => p.EventID).ToList();
            if (list == null) return;
            EventDetailList.Clear();
            foreach (AddEventViewModel item in EventList.Where(p => list.Contains(p.EventID)))
            {
                EventDetailList.Add(item);
            }
        }

        private bool _IsAllChecked;
        public bool IsAllChecked
        {
            get { return _IsAllChecked; }
            set
            {
                SetProperty(ref _IsAllChecked, value);
            }
        }

        private bool _IsMaleChecked;
        public bool IsMaleChecked
        {
            get { return _IsMaleChecked; }
            set
            {
                SetProperty(ref _IsMaleChecked, value);
            }
        }
        private bool _IsMixedChecked;
        public bool IsMixedChecked
        {
            get { return _IsMixedChecked; }
            set
            {
                SetProperty(ref _IsMixedChecked, value);
            }
        }

        private bool _IsFemaleChecked;
        public bool IsFemaleChecked
        {
            get { return _IsFemaleChecked; }
            set
            {
                SetProperty(ref _IsFemaleChecked, value);
            }
        }

        private int _StartAgeRange;
        public int StartAgeRange
        {
            get { return _StartAgeRange; }
            set
            {
                SetProperty(ref _StartAgeRange, value);
            }
        }
        private int _EndAgeRange;
        public int EndAgeRange
        {
            get { return _EndAgeRange; }
            set
            {
                SetProperty(ref _EndAgeRange, value);
            }
        }

        private bool _IsAllEventsChecked;
        public bool IsAllEventsChecked
        {
            get { return _IsAllEventsChecked; }
            set
            {
                SetProperty(ref _IsAllEventsChecked, value);
            }
        }

        private bool _IsRunningOnlyChecked;
        public bool IsRunningOnlyChecked
        {
            get { return _IsRunningOnlyChecked; }
            set
            {
                SetProperty(ref _IsRunningOnlyChecked, value);
            }
        }
        private bool _IsFieldOnlyChecked;
        public bool IsFieldOnlyChecked
        {
            get { return _IsFieldOnlyChecked; }
            set
            {
                SetProperty(ref _IsFieldOnlyChecked, value);
            }
        }
        private bool _IsCombinedOnlyChecked;
        public bool IsCombinedOnlyChecked
        {
            get { return _IsCombinedOnlyChecked; }
            set
            {
                SetProperty(ref _IsCombinedOnlyChecked, value);
            }
        }
        private bool _IsCCRRonlyChecked;
        public bool IsCCRRonlyChecked
        {
            get { return _IsCCRRonlyChecked; }
            set
            {
                SetProperty(ref _IsCCRRonlyChecked, value);
            }
        }
        private bool _IsFieldRelayonlyChecked;
        public bool IsFieldRelayonlyChecked
        {
            get { return _IsFieldRelayonlyChecked; }
            set
            {
                SetProperty(ref _IsFieldRelayonlyChecked, value);
            }
        }
        private bool _IsIndivRelaysChecked;
        public bool IsIndivRelaysChecked
        {
            get { return _IsIndivRelaysChecked; }
            set
            {
                SetProperty(ref _IsIndivRelaysChecked, value);
            }
        }
        private bool _IsIndivOnlyChecked;
        public bool IsIndivOnlyChecked
        {
            get { return _IsIndivOnlyChecked; }
            set
            {
                SetProperty(ref _IsIndivOnlyChecked, value);
            }
        }
        private bool _IsRelaysOnlyChecked;
        public bool IsRelaysOnlyChecked
        {
            get { return _IsRelaysOnlyChecked; }
            set
            {
                SetProperty(ref _IsRelaysOnlyChecked, value);
            }
        }


        #endregion

        private ObservableCollection<AddEventViewModel> _EventDetailList = new ObservableCollection<AddEventViewModel>();
        public ObservableCollection<AddEventViewModel> EventDetailList
        {
            get { return _EventDetailList; }
            set
            {
                SetProperty(ref _EventDetailList, value);
            }
        }
        private AddEventViewModel _SelectedEventDetail;
        public AddEventViewModel SelectedEventDetail
        {
            get { return _SelectedEventDetail; }
            set
            {
                SetProperty(ref _SelectedEventDetail, value);
            }
        }


        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }

        private ICommand _CreateMeetCommand;
        public ICommand CreateMeetCommand
        {
            get { return _CreateMeetCommand; }
        }

        private ICommand _DeleteMeetCommand;
        public ICommand DeleteMeetCommand
        {
            get { return _DeleteMeetCommand; }
        }

        private ICommand _UpdateMeetCommand;
        public ICommand UpdateMeetCommand
        {
            get { return _UpdateMeetCommand; }
        }
        private ICommand _AssignPrefrencesMeetCommand;
        public ICommand AssignPrefrencesMeetCommand
        {
            get { return _AssignPrefrencesMeetCommand; }
        }
        private ICommand _CopyMeetFromTemplateCommand;
        public ICommand CopyMeetFromTemplateCommand
        {
            get { return _CopyMeetFromTemplateCommand; }
        }
        private ICommand _CopyEventFromTemplateCommand;
        public ICommand CopyEventFromTemplateCommand
        {
            get { return _CopyEventFromTemplateCommand; }
        }
        private bool ValidateInputs()
        {
            return true;

        }

        public void SetEntity(ManageEventViewModel entity)
        {

        }
        private ObservableCollection<EventMenuItemViewModel> _MenuItems;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }
        public ManageEventViewModel GetEntity()
        {
            return this;
        }

    }

}

