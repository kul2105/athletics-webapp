﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.MeetSetup.Services.API;
using Athletics.SetupMeet.Model;

namespace Athletics.Athletes.ViewModels
{

    public class MeetSetupTemplateViewModel : BindableBase, IMeetSetupTemplateViewModel, IGenericInteractionView<MeetSetupTemplateViewModel>
    {
        private IRenumEventView View = null;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public MeetSetupTemplateViewModel()
        {

        }
        public MeetSetupTemplateViewModel(IUnityContainer unity, IRenumEventView view, IEventManager meetSetupManage)
        {
            View = view;
            unityContainer = unity;
            EventManager = meetSetupManage;
            View.DataContext = this;


            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {

                                      if (this.SelectedMeetSetup != null)
                                      {
                                          EventManager.CreateMeetUsingTemplate(this.SelectedMeetSetup.MeetID, this.MeetName, this.MeetName2, this.SelectedStartDate, this.SelectedEndDate, this.SelectedAgeupDate);
                                      }

                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "MeetTemplate")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "MeetTemplate")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

        }




        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(MeetSetupTemplateViewModel entity)
        {
            if (entity == null) return;


        }
        public MeetSetupTemplateViewModel GetEntity()
        {

            return this;
        }

        #region Properties
        private MeetSetupModel _SelectedMeetSetup;
        public MeetSetupModel SelectedMeetSetup
        {
            get { return _SelectedMeetSetup; }
            set
            {
                SetProperty(ref _SelectedMeetSetup, value);
            }
        }

        private bool _IsCanceled = true;
        public bool IsCanceled
        {
            get { return _IsCanceled; }
            set
            {
                SetProperty(ref _IsCanceled, value);
            }
        }

        private string _MeetName = string.Empty;
        public string MeetName
        {
            get { return _MeetName; }
            set
            {
                SetProperty(ref _MeetName, value);
            }
        }

        private string _MeetName2 = string.Empty;
        public string MeetName2
        {
            get { return _MeetName2; }
            set
            {
                SetProperty(ref _MeetName2, value);
            }
        }
        private DateTime? _SelectedStartDate = DateTime.Now;
        public DateTime? SelectedStartDate
        {
            get { return _SelectedStartDate; }
            set
            {
                SetProperty(ref _SelectedStartDate, value);
            }
        }

        private DateTime? _SelectedEndDate = DateTime.Now;
        public DateTime? SelectedEndDate
        {
            get { return _SelectedEndDate; }
            set
            {
                SetProperty(ref _SelectedEndDate, value);
            }
        }
        private DateTime? _SelectedAgeupDate = DateTime.Now;
        public DateTime? SelectedAgeupDate
        {
            get { return _SelectedAgeupDate; }
            set
            {
                SetProperty(ref _SelectedAgeupDate, value);
            }
        }
        #endregion
        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }



        #endregion
    }

}

