﻿using Athletics.SetupMeet.Model;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IMeetSetupTemplateViewModel
    {
        MeetSetupModel SelectedMeetSetup { get; set; }
    }
}
