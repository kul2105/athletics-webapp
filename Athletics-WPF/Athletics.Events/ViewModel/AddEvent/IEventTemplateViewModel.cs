﻿using Athletics.SetupMeet.Model;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IEventTemplateViewModel
    {
        AddEventViewModel SelectedAddEventViewModel { get; set; }
    }
}
