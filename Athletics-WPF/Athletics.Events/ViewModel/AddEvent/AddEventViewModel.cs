﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{

    public class AddEventViewModel : BindableBase, IAddEventViewModel, IGenericInteractionView<AddEventViewModel>
    {
        private IAddEventView View = null;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public AddEventViewModel()
        {

        }
        private List<string> FieldEventAdvancement = new List<string>();
        private List<string> RunningEventAdvancement = new List<string>();
        private List<string> FieldEventHeatAndAssign = new List<string>();
        private List<string> RunningEventHeatAndAssign = new List<string>();
        public AddEventViewModel(IUnityContainer unity, IAddEventView view, IEventManager meetSetupManage)
        {
            View = view;
            unityContainer = unity;
            EventManager = meetSetupManage;
            View.DataContext = this;
            MeetTypeDivisionList = new ObservableCollection<string>();
            IndividualMeetTypeDivisionList = new ObservableCollection<string>();
            IndividualMeetTypeDivisionList.Add("By Event");
            IndividualMeetTypeDivisionList.Add("By Team");
            IndividualMeetTypeDivisionList.Add("By Entry");
            GroupMeetTypeDivisionList = new ObservableCollection<string>();
            GroupMeetTypeDivisionList.Add("01 8 & under Division");
            GroupMeetTypeDivisionList.Add("02 9-10 Division");
            GroupMeetTypeDivisionList.Add("03 11-12 Division");
            GroupMeetTypeDivisionList.Add("04 13-14 Division");
            GroupMeetTypeDivisionList.Add("05 15-16 Division");
            GroupMeetTypeDivisionList.Add("06 17-18 Division");
            GroupMeetTypeDivisionList.Add("07 Masters");

            FieldEventAdvancement.Add("Mark");
            RunningEventAdvancement.Add("Time Only");
            RunningEventAdvancement.Add("Place and Time");
            FieldEventHeatAndAssign.Add("Seed Mark");
            FieldEventHeatAndAssign.Add("Random");

            RunningEventHeatAndAssign.Add("Seed Time");
            RunningEventHeatAndAssign.Add("Random");
            RunningEventHeatAndAssign.Add("Seed Prelims as Timed Finals");
            RunningEventHeatAndAssign.Add("Seed Timed Finals as Prelims");
            LanePosAssignmentItems = new ObservableCollection<string>();
            LanePosAssignmentItems.Add("Standard Lanes");
            LanePosAssignmentItems.Add("Custom Lanes");
            LanePosAssignmentItems.Add("All Random");
            LanePosAssignmentItems.Add("Standard Alleys");
            LanePosAssignmentItems.Add("Custom Alleys");
            LanePosAssignmentItems.Add("waterfall Start");


            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "AddEvents")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "AddEvents")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

            _NextCommand = new DelegateCommand(async () =>
            {
                if (!ValidateNextPrevious())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                AddEventViewModel model = EventDetailList.Where(p => p.EventID == this.EventID).First();
                                if (model == null) return;
                                int index = EventDetailList.IndexOf(model);
                                if (EventDetailList.Count > (index + 1))
                                {
                                    AddEventViewModel eventviewModel = EventDetailList[index + 1];
                                    this.SelectedEventDetail = eventviewModel;
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateNextPrevious();
            });

            _PreviousCommand = new DelegateCommand(async () =>
            {
                if (!ValidateNextPrevious())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                AddEventViewModel model = EventDetailList.Where(p => p.EventID == this.EventID).First();
                                if (model == null) return;
                                int index = EventDetailList.IndexOf(model);
                                if ((index - 1) >= 0)
                                {
                                    AddEventViewModel eventviewModel = EventDetailList[index - 1];
                                    this.SelectedEventDetail = eventviewModel;
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateNextPrevious();
            });
            _MultiAgeGroupCommand = new DelegateCommand(async () =>
            {

                dialog = ServiceProvider.Instance.Get<IModalDialog>();
                window = new MultiAgeGroupView();
                window.Name = "Multiage";
                IGenericInteractionView<MultiAgeGroupViewModel> multiAgeGroupVM = unityContainer.Resolve<IMultiAgeGroupViewModel>() as IGenericInteractionView<MultiAgeGroupViewModel>;
                if (multiAgeGroupVM != null)
                {
                    IMultiAgeGroupViewModel Imultage = multiAgeGroupVM as IMultiAgeGroupViewModel;
                    if (Imultage != null)
                    {
                        Imultage.EventMultiAgeDetailList.Clear();
                        Imultage.AgeGroup = string.Format("AGE RANGE ={0} to {1}", this.StartAgeRange, this.EndAgeRange);
                        for (int i = 1; i <= 24; i++)
                        {
                            MultiAgeGroup multiageGroup = this.EventMultiAgeDetailList.Where(p => p.standardID == i).FirstOrDefault();
                            if (multiageGroup == null)
                            {
                                multiageGroup = new MultiAgeGroup();
                                multiageGroup.StandardID = i;
                            }
                            Imultage.EventMultiAgeDetailList.Add(multiageGroup);
                        }
                    }
                    dialog.BindView(window);
                    dialog.BindViewModel(multiAgeGroupVM);
                    dialog.ShowDialog();
                    dialog.Close();
                    this.EventMultiAgeDetailList.Clear();
                    foreach (var item in Imultage.EventMultiAgeDetailList)
                    {
                        if (item.LowAge > 0)
                        {
                            item.EventID = this.EventID;
                            this.EventMultiAgeDetailList.Add(item);
                        }
                    }
                    dialog = null;
                }

            }, () =>
            {
                return true;
            });
        }

        #region Properties

        private ObservableCollection<AddEventViewModel> _EventDetailList = new ObservableCollection<AddEventViewModel>();
        public ObservableCollection<AddEventViewModel> EventDetailList
        {
            get { return _EventDetailList; }
            set
            {
                SetProperty(ref _EventDetailList, value);
            }
        }
        private bool _IsCanceled = true;
        public bool IsCanceled
        {
            get { return _IsCanceled; }
            set
            {
                SetProperty(ref _IsCanceled, value);
            }
        }

        private int _EventID;
        public int EventID
        {
            get { return _EventID; }
            set
            {
                SetProperty(ref _EventID, value);
            }
        }

        private string _EventNumber;
        public string EventNumber
        {
            get { return _EventNumber; }
            set
            {
                SetProperty(ref _EventNumber, value);
            }
        }

        private string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                SetProperty(ref _Status, value);
            }
        }

        private string _Gender;
        public string Gender
        {
            get { return _Gender; }
            set
            {
                SetProperty(ref _Gender, value);
            }
        }

        private int _StartAgeRange;
        public int StartAgeRange
        {
            get { return _StartAgeRange; }
            set
            {
                SetProperty(ref _StartAgeRange, value);
            }
        }

        private int _EndAgeRange;
        public int EndAgeRange
        {
            get { return _EndAgeRange; }
            set
            {
                SetProperty(ref _EndAgeRange, value);
            }
        }

        private bool _IsMultiAgeGroup;
        public bool IsMultiAgeGroup
        {
            get { return _IsMultiAgeGroup; }
            set
            {
                SetProperty(ref _IsMultiAgeGroup, value);
                if (value == false)
                {
                    MeetTypeDivisionList = IndividualMeetTypeDivisionList;
                }
                else
                {
                    MeetTypeDivisionList = GroupMeetTypeDivisionList;
                }

            }
        }
        private string _Division;
        public string Division
        {
            get { return _Division; }
            set
            {
                SetProperty(ref _Division, value);
            }
        }

        private string _EventNotes;
        public string EventNotes
        {
            get { return _EventNotes; }
            set
            {
                SetProperty(ref _EventNotes, value);
            }
        }
        private string _Entries;
        public string Entries
        {
            get { return _Entries; }
            set
            {
                SetProperty(ref _Entries, value);
            }
        }

        private string _Results;
        public string Results
        {
            get { return _Results; }
            set
            {
                SetProperty(ref _Results, value);
            }
        }

        private string _AgeGroup;
        public string AgeGroup
        {
            get { return _AgeGroup; }
            set
            {
                SetProperty(ref _AgeGroup, value);
            }
        }
        private int _Distance;
        public int Distance
        {
            get { return _Distance; }
            set
            {
                SetProperty(ref _Distance, value);
            }
        }

        private string _Event;
        public string Event
        {
            get { return _Event; }
            set
            {
                SetProperty(ref _Event, value);
            }
        }
        private string _Advance;
        public string Advance
        {
            get { return _Advance; }
            set
            {
                SetProperty(ref _Advance, value);
            }
        }

        private string _HeatOrder;
        public string HeatOrder
        {
            get { return _HeatOrder; }
            set
            {
                SetProperty(ref _HeatOrder, value);
            }
        }

        private int _NumberOfLane;
        public int NumberOfLane
        {
            get { return _NumberOfLane; }
            set
            {
                SetProperty(ref _NumberOfLane, value);
            }
        }

        private string _Heats;
        public string Heats
        {
            get { return _Heats; }
            set
            {
                SetProperty(ref _Heats, value);
            }
        }
        private string _LinePosition;
        public string LinePosition
        {
            get { return _LinePosition; }
            set
            {
                SetProperty(ref _LinePosition, value);
            }
        }

        private int _HeatInSemis;
        public int HeatInSemis
        {
            get { return _HeatInSemis; }
            set
            {
                SetProperty(ref _HeatInSemis, value);
            }
        }
        private int _HeatInQtrs;
        public int HeatInQtrs
        {
            get { return _HeatInQtrs; }
            set
            {
                SetProperty(ref _HeatInQtrs, value);
            }
        }

        private bool _IsScoreEvent;
        public bool IsScoreEvent
        {
            get { return _IsScoreEvent; }
            set
            {
                SetProperty(ref _IsScoreEvent, value);
            }
        }

        private bool _IsJumbOff;
        public bool IsJumbOff
        {
            get { return _IsJumbOff; }
            set
            {
                SetProperty(ref _IsJumbOff, value);
            }
        }

        private double _EntryFee;
        public double EntryFee
        {
            get { return _EntryFee; }
            set
            {
                SetProperty(ref _EntryFee, value);
            }
        }

        private string _Type;
        public string Type
        {
            get { return _Type; }
            set
            {
                SetProperty(ref _Type, value);
            }
        }

        private string _Rnds;
        public string Rnds
        {
            get { return _Rnds; }
            set
            {
                SetProperty(ref _Rnds, value);
            }
        }
        private string _Finals;
        public string Finals
        {
            get { return _Finals; }
            set
            {
                SetProperty(ref _Finals, value);
            }
        }
        private int _FinalNumberOfLanes;
        public int FinalNumberOfLanes
        {
            get { return _FinalNumberOfLanes; }
            set
            {
                SetProperty(ref _FinalNumberOfLanes, value);
            }
        }
        private string _FinalOrder;
        public string FinalOrder
        {
            get { return _FinalOrder; }
            set
            {
                SetProperty(ref _FinalOrder, value);
            }
        }
        private List<MultiAgeGroup> _EventMultiAgeDetailList = new List<MultiAgeGroup>();
        public List<MultiAgeGroup> EventMultiAgeDetailList
        {
            get { return _EventMultiAgeDetailList; }
            set
            {
                SetProperty(ref _EventMultiAgeDetailList, value);
            }
        }
        #endregion

        #region Reset UI
        private void ResetGender()
        {
            _IsWomenChecked = false;
            _IsGirlsChecked = false;
            _IsBoysChecked = false;
            _IsMenChecked = false;
            _IsMixedChecked = false;
            base.OnPropertyChanged("IsWomenChecked");
            base.OnPropertyChanged("IsGirlsChecked");
            base.OnPropertyChanged("IsBoysChecked");
            base.OnPropertyChanged("IsMenChecked");
            base.OnPropertyChanged("IsMixedChecked");
        }

        private void ResetDistance()
        {
            _IsDistance55 = false;
            _IsDistance800 = false;
            _IsDistance60 = false;
            _IsDistance1500 = false;
            _IsDistance80 = false;
            _IsDistance1600 = false;
            _IsDistance100 = false;
            _IsDistance2000 = false;
            _IsDistance110 = false;
            _IsDistance3000 = false;
            _IsDistance200 = false;
            _IsDistance3200 = false;
            _IsDistance300 = false;
            _IsDistance5000 = false;
            _IsDistance400 = false;
            _IsDistance10000 = false;
            _IsDistanceNone = false;
            _IsDistanceCustom = false;

            base.OnPropertyChanged("IsDistance55");
            base.OnPropertyChanged("IsDistance800");
            base.OnPropertyChanged("IsDistance60");
            base.OnPropertyChanged("IsDistance1500");
            base.OnPropertyChanged("IsDistance80");
            base.OnPropertyChanged("IsDistance1600");
            base.OnPropertyChanged("IsDistance100");
            base.OnPropertyChanged("IsDistance2000");
            base.OnPropertyChanged("IsDistance110");
            base.OnPropertyChanged("IsDistance3000");
            base.OnPropertyChanged("IsDistance200");
            base.OnPropertyChanged("IsDistance3200");
            base.OnPropertyChanged("IsDistance300");
            base.OnPropertyChanged("IsDistance5000");
            base.OnPropertyChanged("IsDistance400");
            base.OnPropertyChanged("IsDistance10000");
            base.OnPropertyChanged("IsDistanceNone");
            base.OnPropertyChanged("IsDistanceCustom");
        }
        private void ResetRound()
        {
            _IsFinalOnlyRounds = false;
            _IsPrelimsFinalRounds = false;
            _IsPrelimsSemisFinalRounds = false;
            _IsPrelimsQtrsSemisFinalRounds = false;
            base.OnPropertyChanged("IsFinalOnlyRounds");
            base.OnPropertyChanged("IsPrelimsFinalRounds");
            base.OnPropertyChanged("IsPrelimsSemisFinalRounds");
            base.OnPropertyChanged("IsPrelimsQtrsSemisFinalRounds");
        }

        private void ResetHeatOrder()
        {
            _IsFasttoSlowHeatOrder = false;
            _IsSlowtoFastHeatOrder = false;
            _IsRandomHeatOrder = false;
            base.OnPropertyChanged("IsFasttoSlowHeatOrder");
            base.OnPropertyChanged("IsSlowtoFastHeatOrder");
            base.OnPropertyChanged("IsRandomHeatOrder");
        }
        public void ResetOrder()
        {
            _IsFasttoSloworder = false;
            _IsSlowtoFastOrder = false;
            base.OnPropertyChanged("IsSlowtoFastOrder");
            base.OnPropertyChanged("IsFasttoSloworder");
        }
        private void ResetEntry()
        {
            _IsMaticEntries = false;
            _IsEnglishEntries = false;
            base.OnPropertyChanged("IsMaticEntries");
            base.OnPropertyChanged("IsEnglishEntries");
        }
        private void ResetResult()
        {
            _IsMaticResult = false;
            _IsEnglishResult = false;
            base.OnPropertyChanged("IsMaticResult");
            base.OnPropertyChanged("IsEnglishResult");
        }
        private void ResetFinalNumberOfHeat()
        {
            _IsNummberofFinalHeat1Final = false;
            _IsNummberofFinalHeat2Final = false;
            _IsNummberofFinalHeat3PluseFinal = false;
            _IsFinalScorePrelimsAs2ndFinal = false;
            base.OnPropertyChanged("IsNummberofFinalHeat1Final");
            base.OnPropertyChanged("IsNummberofFinalHeat2Final");
            base.OnPropertyChanged("IsNummberofFinalHeat3PluseFina");
            base.OnPropertyChanged("IsFinalScorePrelimsAs2ndFinal");
        }

        private void ResetEvent()
        {
            _IsDashRunningEvent = false;
            _IsRunRunningEvent = false;
            _IsMileRunRunningEvent = false;
            _IsRaceWalkRunningEvent = false;
            _IsHurdlesRunningEvent = false;
            _IsSteeplechaseRunningEvent = false;
            _IsHalfMarathonRunningEvent = false;
            _IsMarathonRunningEvent = false;
            _IsHightJumpFieldEvent = false;
            _IsPoleVaultFieldEvent = false;
            _IsLongJumpFieldEvent = false;
            _IsTripleJumpFieldEvent = false;
            _IsDiscusThrowFieldEvent = false;
            _IsHammerThrowFieldEvent = false;
            _IsJavelinThrowFieldEvent = false;
            _IsShotPutFieldEvent = false;
            _IsWeightThrowFieldEvent = false;
            _IsSuperWeightFieldEvent = false;
            _IsOtherFieldEvent = false;
            _IsRelayRelayEvent = false;
            _IsSprintMedleyRelayEvent = false;
            _IsDistanceMedleyRelayEvent = false;
            _IsShuttleHurdleRelayEvent = false;
            _IsDecathlonCombinedEvent = false;
            _IsOctathlonCombinedEvent = false;
            _IsHeptathlonCombinedEvent = false;
            _IsWeightPantathlonCombinedEvent = false;
            _IsIndoorPentathlonCombinedEvent = false;
            _IsOutDoorPentathlonCombinedEvent = false;
            _IsTetrathlonCombinedEvent = false;
            _IsTriathlonCombinedEvent = false;
            _IsBiathlonCombinedEvent = false;

            base.OnPropertyChanged("IsDashRunningEvent");
            base.OnPropertyChanged("IsRunRunningEvent");
            base.OnPropertyChanged("IsMileRunRunningEvent");
            base.OnPropertyChanged("IsRaceWalkRunningEvent");
            base.OnPropertyChanged("IsHurdlesRunningEvent");
            base.OnPropertyChanged("IsSteeplechaseRunningEvent");
            base.OnPropertyChanged("IsHalfMarathonRunningEvent");
            base.OnPropertyChanged("IsMarathonRunningEvent");
            base.OnPropertyChanged("IsHightJumpFieldEvent");
            base.OnPropertyChanged("IsPoleVaultFieldEvent");
            base.OnPropertyChanged("IsLongJumpFieldEvent");
            base.OnPropertyChanged("IsTripleJumpFieldEvent");
            base.OnPropertyChanged("IsDiscusThrowFieldEvent");
            base.OnPropertyChanged("IsHammerThrowFieldEvent");
            base.OnPropertyChanged("IsJavelinThrowFieldEvent");
            base.OnPropertyChanged("IsShotPutFieldEvent");
            base.OnPropertyChanged("IsWeightThrowFieldEvent");
            base.OnPropertyChanged("IsSuperWeightFieldEvent");
            base.OnPropertyChanged("IsOtherFieldEvent");
            base.OnPropertyChanged("IsRelayRelayEvent");
            base.OnPropertyChanged("IsSprintMedleyRelayEvent");
            base.OnPropertyChanged("IsDistanceMedleyRelayEvent");
            base.OnPropertyChanged("IsShuttleHurdleRelayEvent");
            base.OnPropertyChanged("IsDecathlonCombinedEvent");
            base.OnPropertyChanged("IsOctathlonCombinedEvent");
            base.OnPropertyChanged("IsHeptathlonCombinedEvent");
            base.OnPropertyChanged("IsWeightPantathlonCombinedEvent");
            base.OnPropertyChanged("IsIndoorPentathlonCombinedEvent");
            base.OnPropertyChanged("IsOutDoorPentathlonCombinedEvent");
            base.OnPropertyChanged("IsTetrathlonCombinedEvent");
            base.OnPropertyChanged("IsTriathlonCombinedEvent");
            base.OnPropertyChanged("IsBiathlonCombinedEvent");

        }
        #endregion

        #region Form Properties

        private bool _IsGirlsChecked;
        public bool IsGirlsChecked
        {
            get { return _IsGirlsChecked; }
            set
            {
                this.ResetGender();
                SetProperty(ref _IsGirlsChecked, value);
                this.Gender = "Girls";
            }
        }



        private bool _IsWomenChecked;
        public bool IsWomenChecked
        {
            get { return _IsWomenChecked; }
            set
            {
                this.ResetGender();
                SetProperty(ref _IsWomenChecked, value);
                this.Gender = "Women";
            }
        }

        private bool _IsBoysChecked;
        public bool IsBoysChecked
        {
            get { return _IsBoysChecked; }
            set
            {
                this.ResetGender();
                SetProperty(ref _IsBoysChecked, value);
                this.Gender = "Boys";
            }
        }
        private bool _IsMenChecked;
        public bool IsMenChecked
        {
            get { return _IsMenChecked; }
            set
            {
                this.ResetGender();
                SetProperty(ref _IsMenChecked, value);
                this.Gender = "Men";
            }
        }


        private bool _IsMixedChecked;
        public bool IsMixedChecked
        {
            get { return _IsMixedChecked; }
            set
            {
                this.ResetGender();
                SetProperty(ref _IsMixedChecked, value);
                this.Gender = "Mixed";
            }
        }

        private bool _IsDistance55;
        public bool IsDistance55
        {
            get { return _IsDistance55; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance55, value);
                this.Distance = 55;
            }
        }
        private bool _IsDistance800;
        public bool IsDistance800
        {
            get { return _IsDistance800; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance800, value);
                this.Distance = 800;
            }
        }

        private bool _IsDistance60;
        public bool IsDistance60
        {
            get { return _IsDistance60; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance60, value);
                this.Distance = 60;
            }
        }
        private bool _IsDistance1500;
        public bool IsDistance1500
        {
            get { return _IsDistance1500; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance1500, value);
                this.Distance = 500;
            }
        }

        private bool _IsDistance80;
        public bool IsDistance80
        {
            get { return _IsDistance80; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance80, value);
                this.Distance = 80;
            }
        }

        private bool _IsDistance1600;
        public bool IsDistance1600
        {
            get { return _IsDistance1600; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance1600, value);
                this.Distance = 1600;
            }
        }

        private bool _IsDistance100;
        public bool IsDistance100
        {
            get { return _IsDistance100; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance100, value);
                this.Distance = 100;
            }
        }

        private bool _IsDistance2000;
        public bool IsDistance2000
        {
            get { return _IsDistance2000; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance2000, value);
                this.Distance = 2000;
            }
        }

        private bool _IsDistance110;
        public bool IsDistance110
        {
            get { return _IsDistance110; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance110, value);
                this.Distance = 110;
            }
        }

        private bool _IsDistance3000;
        public bool IsDistance3000
        {
            get { return _IsDistance3000; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance3000, value);
                this.Distance = 3000;
            }
        }

        private bool _IsDistance200;
        public bool IsDistance200
        {
            get { return _IsDistance200; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance200, value);
                this.Distance = 200;
            }
        }

        private bool _IsDistance3200;
        public bool IsDistance3200
        {
            get { return _IsDistance3200; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance3200, value);
                this.Distance = 3200;
            }
        }
        private bool _IsDistance300;
        public bool IsDistance300
        {
            get { return _IsDistance300; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance300, value);
                this.Distance = 300;
            }
        }
        private bool _IsDistance5000;
        public bool IsDistance5000
        {
            get { return _IsDistance5000; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance5000, value);
                this.Distance = 5000;
            }
        }

        private bool _IsDistance400;
        public bool IsDistance400
        {
            get { return _IsDistance400; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance400, value);
                this.Distance = 400;
            }
        }
        private bool _IsDistance10000;
        public bool IsDistance10000
        {
            get { return _IsDistance10000; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistance10000, value);
                this.Distance = 10000;
            }
        }
        private bool _IsDistanceNone;
        public bool IsDistanceNone
        {
            get { return _IsDistanceNone; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistanceNone, value);
                this.Distance = 0;
            }
        }
        private bool _IsDistanceCustom;
        public bool IsDistanceCustom
        {
            get { return _IsDistanceCustom; }
            set
            {
                ResetDistance();
                SetProperty(ref _IsDistanceCustom, value);
                this.Distance = DistanceCustom;
            }
        }
        private int _DistanceCustom;
        public int DistanceCustom
        {
            get { return _DistanceCustom; }
            set
            {
                SetProperty(ref _DistanceCustom, value);
            }
        }

        private bool _IsRunningEvent;
        public bool IsRunningEvent
        {
            get { return _IsRunningEvent; }
            set
            {
                if (_IsRunningEvent != value)
                {
                    SetProperty(ref _IsRunningEvent, value);
                    this._IsCombinedEvent = false;
                    _IsFieldEvent = false;
                    this.AdvancementItems.Clear();
                    this.HeadFlightAssigmentItems.Clear();
                    foreach (var item in RunningEventAdvancement)
                    {
                        this.AdvancementItems.Add(item);
                    }

                    foreach (var item in RunningEventHeatAndAssign)
                    {
                        this.HeadFlightAssigmentItems.Add(item);
                    }
                    this.Type = "Running Event";
                }
            }
        }

        private string _RunningEvent;
        public string RunningEvent
        {
            get { return _RunningEvent; }
            set
            {
                SetProperty(ref _RunningEvent, value);
            }
        }

        private bool _IsFieldEvent;
        public bool IsFieldEvent
        {
            get { return _IsFieldEvent; }
            set
            {
                if (_IsFieldEvent != value)
                {
                    SetProperty(ref _IsFieldEvent, value);
                    this._IsRunningEvent = false;
                    _IsCombinedEvent = false;
                    this.AdvancementItems.Clear();
                    this.HeadFlightAssigmentItems.Clear();
                    foreach (var item in FieldEventAdvancement)
                    {
                        this.AdvancementItems.Add(item);
                    }

                    foreach (var item in FieldEventHeatAndAssign)
                    {
                        this.HeadFlightAssigmentItems.Add(item);
                    }
                    this.Type = "Field Event";
                }
            }
        }

        private bool _IsCombinedEvent;
        public bool IsCombinedEvent
        {
            get { return _IsCombinedEvent; }
            set
            {
                SetProperty(ref _IsCombinedEvent, value);
                this._IsRunningEvent = false;
                _IsFieldEvent = false;
                this.AdvancementItems.Clear();
                this.Type = "Combined Event";
            }
        }

        private bool _IsRelayEvent;
        public bool IsRelayEvent
        {
            get { return _IsRelayEvent; }
            set
            {
                SetProperty(ref _IsRelayEvent, value);
            }
        }

        private string _RelayEvent;
        public string RelayEvent
        {
            get { return _RelayEvent; }
            set
            {
                SetProperty(ref _RelayEvent, value);
            }
        }

        private string _Round;
        public string Round
        {
            get { return _Round; }
            set
            {
                SetProperty(ref _Round, value);
            }
        }


        #region RunningEvent
        private bool _IsDashRunningEvent;
        public bool IsDashRunningEvent
        {
            get { return _IsDashRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsDashRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Dash";
            }
        }
        private bool _IsRunRunningEvent;
        public bool IsRunRunningEvent
        {
            get { return _IsRunRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsRunRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Run";
            }
        }
        private bool _IsMileRunRunningEvent;
        public bool IsMileRunRunningEvent
        {
            get { return _IsMileRunRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsMileRunRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Mile Run";
            }
        }

        private bool _IsRaceWalkRunningEvent;
        public bool IsRaceWalkRunningEvent
        {
            get { return _IsRaceWalkRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsRaceWalkRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Race Walk";
            }
        }

        private bool _IsHurdlesRunningEvent;
        public bool IsHurdlesRunningEvent
        {
            get { return _IsHurdlesRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsHurdlesRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Hurdles";
            }
        }

        private bool _IsSteeplechaseRunningEvent;
        public bool IsSteeplechaseRunningEvent
        {
            get { return _IsSteeplechaseRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsSteeplechaseRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Steeplechase";
            }
        }

        private bool _IsHalfMarathonRunningEvent;
        public bool IsHalfMarathonRunningEvent
        {
            get { return _IsHalfMarathonRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsHalfMarathonRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Half Marathon";
            }
        }
        private bool _IsMarathonRunningEvent;
        public bool IsMarathonRunningEvent
        {
            get { return _IsMarathonRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsMarathonRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Marathon";
            }
        }

        private bool _IsFieldOrRunRelayRunningEvent;
        public bool IsFieldOrRunRelayRunningEvent
        {
            get { return _IsFieldOrRunRelayRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsFieldOrRunRelayRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Field Or RunRelay";
            }
        }

        private bool _IsCrossCountyRunningEvent;
        public bool IsCrossCountyRunningEvent
        {
            get { return _IsCrossCountyRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsCrossCountyRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Cross County";
            }
        }

        private bool _IsRoadRaceRunningEvent;
        public bool IsRoadRaceRunningEvent
        {
            get { return _IsRoadRaceRunningEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsRoadRaceRunningEvent, value);
                IsRunningEvent = true;
                RunningEvent = "Road Race";
            }
        }


        #endregion

        private ObservableCollection<string> _MeetTypeDivisionList;
        public ObservableCollection<string> MeetTypeDivisionList
        {
            get { return _MeetTypeDivisionList; }
            set
            {
                SetProperty(ref _MeetTypeDivisionList, value);
            }
        }
        private ObservableCollection<string> _IndividualMeetTypeDivisionList;
        public ObservableCollection<string> IndividualMeetTypeDivisionList
        {
            get { return _IndividualMeetTypeDivisionList; }
            set
            {
                SetProperty(ref _IndividualMeetTypeDivisionList, value);
            }
        }
        private ObservableCollection<string> _GroupMeetTypeDivisionList;
        public ObservableCollection<string> GroupMeetTypeDivisionList
        {
            get { return _GroupMeetTypeDivisionList; }
            set
            {
                SetProperty(ref _GroupMeetTypeDivisionList, value);
            }
        }
        private AddEventViewModel _SelectedEventDetail;
        public AddEventViewModel SelectedEventDetail
        {
            get { return _SelectedEventDetail; }
            set
            {
                SetProperty(ref _SelectedEventDetail, value);
                this.SetEntity(value);
            }
        }

        #region FieldEvent

        private bool _IsHightJumpFieldEvent;
        public bool IsHightJumpFieldEvent
        {
            get { return _IsHightJumpFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsHightJumpFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Hight Jump";
            }
        }

        private bool _IsPoleVaultFieldEvent;
        public bool IsPoleVaultFieldEvent
        {
            get { return _IsPoleVaultFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsPoleVaultFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Pole Vault";
            }
        }

        private bool _IsLongJumpFieldEvent;
        public bool IsLongJumpFieldEvent
        {
            get { return _IsLongJumpFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsLongJumpFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Long Jump";
            }
        }
        private bool _IsTripleJumpFieldEvent;
        public bool IsTripleJumpFieldEvent
        {
            get { return _IsTripleJumpFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsTripleJumpFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Triple Jump";
            }
        }

        private bool _IsDiscusThrowFieldEvent;
        public bool IsDiscusThrowFieldEvent
        {
            get { return _IsDiscusThrowFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsDiscusThrowFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Discus Throw";
            }
        }

        private bool _IsHammerThrowFieldEvent;
        public bool IsHammerThrowFieldEvent
        {
            get { return _IsHammerThrowFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsHammerThrowFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Hammer Throw";
            }
        }

        private bool _IsJavelinThrowFieldEvent;
        public bool IsJavelinThrowFieldEvent
        {
            get { return _IsJavelinThrowFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsJavelinThrowFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Javelin Throw";
            }
        }
        private bool _IsShotPutFieldEvent;
        public bool IsShotPutFieldEvent
        {
            get { return _IsShotPutFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsShotPutFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Shot Put";
            }
        }

        private bool _IsWeightThrowFieldEvent;
        public bool IsWeightThrowFieldEvent
        {
            get { return _IsWeightThrowFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsWeightThrowFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Weight Throw";
            }
        }
        private bool _IsSuperWeightFieldEvent;
        public bool IsSuperWeightFieldEvent
        {
            get { return _IsSuperWeightFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsSuperWeightFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Super Weight";
            }
        }

        private bool _IsOtherFieldEvent;
        public bool IsOtherFieldEvent
        {
            get { return _IsOtherFieldEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsOtherFieldEvent, value);
                IsFieldEvent = true;
                RunningEvent = "Other";
            }
        }
        #endregion

        #region Relay Event
        private bool _IsRelayRelayEvent;
        public bool IsRelayRelayEvent
        {
            get { return _IsRelayRelayEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsRelayRelayEvent, value);
                IsRelayEvent = true;
                RunningEvent = "Relay";
            }
        }

        private bool _IsSprintMedleyRelayEvent;
        public bool IsSprintMedleyRelayEvent
        {
            get { return _IsSprintMedleyRelayEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsSprintMedleyRelayEvent, value);
                IsRelayEvent = true;
                RunningEvent = "Sprint Medley";
            }
        }

        private bool _IsDistanceMedleyRelayEvent;
        public bool IsDistanceMedleyRelayEvent
        {
            get { return _IsDistanceMedleyRelayEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsDistanceMedleyRelayEvent, value);
                IsRelayEvent = true;
                RunningEvent = "Distance Medley";
            }
        }

        private bool _IsShuttleHurdleRelayEvent;
        public bool IsShuttleHurdleRelayEvent
        {
            get { return _IsShuttleHurdleRelayEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsShuttleHurdleRelayEvent, value);
                IsRelayEvent = true;
                RunningEvent = "Shuttle Hurdle";
            }
        }
        private int _offRunnersRelayEvent;
        public int offRunnersRelayEvent
        {
            get { return _offRunnersRelayEvent; }
            set
            {

                SetProperty(ref _offRunnersRelayEvent, value);
            }
        }
        #endregion

        #region Combined Event
        private bool _IsDecathlonCombinedEvent;
        public bool IsDecathlonCombinedEvent
        {
            get { return _IsDecathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsDecathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Decathlon";
            }
        }
        private bool _IsOctathlonCombinedEvent;
        public bool IsOctathlonCombinedEvent
        {
            get { return _IsOctathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsOctathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Octathlon";
            }
        }
        private bool _IsHeptathlonCombinedEvent;
        public bool IsHeptathlonCombinedEvent
        {
            get { return _IsHeptathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsHeptathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Heptathlon";
            }
        }

        private bool _IsWeightPantathlonCombinedEvent;
        public bool IsWeightPantathlonCombinedEvent
        {
            get { return _IsWeightPantathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsWeightPantathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Weight Pantathlon";
            }
        }
        private bool _IsIndoorPentathlonCombinedEvent;
        public bool IsIndoorPentathlonCombinedEvent
        {
            get { return _IsIndoorPentathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsIndoorPentathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Indoor Pentathlon";
            }
        }
        private bool _IsOutDoorPentathlonCombinedEvent;
        public bool IsOutDoorPentathlonCombinedEvent
        {
            get { return _IsOutDoorPentathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsOutDoorPentathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Outdoor Pentathlon";
            }
        }

        private bool _IsTetrathlonCombinedEvent;
        public bool IsTetrathlonCombinedEvent
        {
            get { return _IsTetrathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsTetrathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Tetrathlon";
            }
        }

        private bool _IsTriathlonCombinedEvent;
        public bool IsTriathlonCombinedEvent
        {
            get { return _IsTriathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsTriathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Triathlon";
            }
        }
        private bool _IsBiathlonCombinedEvent;
        public bool IsBiathlonCombinedEvent
        {
            get { return _IsBiathlonCombinedEvent; }
            set
            {
                ResetEvent();
                SetProperty(ref _IsBiathlonCombinedEvent, value);
                IsCombinedEvent = true;
                RunningEvent = "Biathlon";
            }
        }

        #region Prelim Qtrs Final  Only

        private int _NumberOfLanes;
        public int NumberOfLanes
        {
            get { return _NumberOfLanes; }
            set
            {
                SetProperty(ref _NumberOfLanes, value);
            }
        }

        private string _Advancement;
        public string Advancement
        {
            get { return _Advancement; }
            set
            {
                SetProperty(ref _Advancement, value);
            }
        }

        private ObservableCollection<string> _AdvancementItems = new ObservableCollection<string>();
        public ObservableCollection<string> AdvancementItems
        {
            get { return _AdvancementItems; }
            set
            {
                SetProperty(ref _AdvancementItems, value);
            }
        }

        private string _HeadFlightAssign;
        public string HeadFlightAssign
        {
            get { return _HeadFlightAssign; }
            set
            {
                SetProperty(ref _HeadFlightAssign, value);
            }
        }

        private ObservableCollection<string> _HeadFlightAssigmentItems = new ObservableCollection<string>();
        public ObservableCollection<string> HeadFlightAssigmentItems
        {
            get { return _HeadFlightAssigmentItems; }
            set
            {
                SetProperty(ref _HeadFlightAssigmentItems, value);
            }
        }

        private string _LanePosAssignment;
        public string LanePosAssignment
        {
            get { return _LanePosAssignment; }
            set
            {
                SetProperty(ref _LanePosAssignment, value);
            }
        }

        private ObservableCollection<string> _LanePosAssignmentItems = new ObservableCollection<string>();
        public ObservableCollection<string> LanePosAssignmentItems
        {
            get { return _LanePosAssignmentItems; }
            set
            {
                SetProperty(ref _LanePosAssignmentItems, value);
            }
        }

        private bool _IsFasttoSlowHeatOrder;
        public bool IsFasttoSlowHeatOrder
        {
            get { return _IsFasttoSlowHeatOrder; }
            set
            {
                this.ResetHeatOrder();
                SetProperty(ref _IsFasttoSlowHeatOrder, value);
                HeatOrder = "FasttoSlow";

            }
        }


        private bool _IsSlowtoFastHeatOrder;
        public bool IsSlowtoFastHeatOrder
        {
            get { return _IsSlowtoFastHeatOrder; }
            set
            {
                this.ResetHeatOrder();
                SetProperty(ref _IsSlowtoFastHeatOrder, value);
                HeatOrder = "SlowtoFast";
            }
        }

        private bool _IsRandomHeatOrder;
        public bool IsRandomHeatOrder
        {
            get { return _IsRandomHeatOrder; }
            set
            {
                this.ResetHeatOrder();
                SetProperty(ref _IsRandomHeatOrder, value);
                HeatOrder = "Random";
            }
        }

        private int _HeatsInSemis;
        public int HeatsInSemis
        {
            get { return _HeatsInSemis; }
            set
            {
                SetProperty(ref _HeatsInSemis, value);
            }
        }

        private int _HeatsInQtrs;
        public int HeatsInQtrs
        {
            get { return _HeatsInQtrs; }
            set
            {
                SetProperty(ref _HeatsInQtrs, value);
            }
        }


        #endregion

        private bool _IsFinalOnlyRounds;
        public bool IsFinalOnlyRounds
        {
            get { return _IsFinalOnlyRounds; }
            set
            {
                this.ResetRound();
                SetProperty(ref _IsFinalOnlyRounds, value);
                Round = "F";
            }
        }



        private bool _IsPrelimsFinalRounds;
        public bool IsPrelimsFinalRounds
        {
            get { return _IsPrelimsFinalRounds; }
            set
            {
                this.ResetRound();
                SetProperty(ref _IsPrelimsFinalRounds, value);
                Round = "P/F";
            }
        }
        private bool _IsPrelimsSemisFinalRounds;
        public bool IsPrelimsSemisFinalRounds
        {
            get { return _IsPrelimsSemisFinalRounds; }
            set
            {
                this.ResetRound();
                SetProperty(ref _IsPrelimsSemisFinalRounds, value);
                Round = "P/S/F";
            }
        }

        private bool _IsPrelimsQtrsSemisFinalRounds;
        public bool IsPrelimsQtrsSemisFinalRounds
        {
            get { return _IsPrelimsQtrsSemisFinalRounds; }
            set
            {
                this.ResetRound();
                SetProperty(ref _IsPrelimsQtrsSemisFinalRounds, value);
                Round = "P/Q/S/F";
            }
        }
        private bool _IsJumpOff;
        public bool IsJumpOff
        {
            get { return _IsJumpOff; }
            set
            {
                this.ResetRound();
                SetProperty(ref _IsJumpOff, value);
            }
        }

        #endregion

        #region Final Line Round
        private int _NumberOfLanesFinalRound;
        public int NumberOfLanesFinalRound
        {
            get { return _NumberOfLanesFinalRound; }
            set
            {
                SetProperty(ref _NumberOfLanesFinalRound, value);
            }
        }

        private bool _IsNummberofFinalHeat1Final;
        public bool IsNummberofFinalHeat1Final
        {
            get { return _IsNummberofFinalHeat1Final; }
            set
            {
                this.ResetFinalNumberOfHeat();
                SetProperty(ref _IsNummberofFinalHeat1Final, value);
                Finals = "1 Final";
            }
        }



        private bool _IsNummberofFinalHeat2Final;
        public bool IsNummberofFinalHeat2Final
        {
            get { return _IsNummberofFinalHeat2Final; }
            set
            {
                this.ResetFinalNumberOfHeat();
                SetProperty(ref _IsNummberofFinalHeat2Final, value);
                Finals = "2 Final";
            }
        }

        private bool _IsNummberofFinalHeat3PluseFinal;
        public bool IsNummberofFinalHeat3PluseFinal
        {
            get { return _IsNummberofFinalHeat3PluseFinal; }
            set
            {
                this.ResetFinalNumberOfHeat();
                SetProperty(ref _IsNummberofFinalHeat3PluseFinal, value);
                Finals = ThreePluseText;
            }
        }
        private bool _IsFinalScorePrelimsAs2ndFinal;
        public bool IsFinalScorePrelimsAs2ndFinal
        {
            get { return _IsFinalScorePrelimsAs2ndFinal; }
            set
            {
                this.ResetFinalNumberOfHeat();
                SetProperty(ref _IsFinalScorePrelimsAs2ndFinal, value);
                Finals = "Score Prelims 2nd";
            }
        }

        private bool _IsFasttoSloworder;
        public bool IsFasttoSloworder
        {
            get { return _IsFasttoSloworder; }
            set
            {
                ResetOrder();
                SetProperty(ref _IsFasttoSloworder, value);
                FinalOrder = "FasttoSlow";
            }
        }

        private bool _IsSlowtoFastOrder;
        public bool IsSlowtoFastOrder
        {
            get { return _IsSlowtoFastOrder; }
            set
            {
                ResetOrder();
                SetProperty(ref _IsSlowtoFastOrder, value);
                FinalOrder = "SlowtoFast";
            }
        }

        private string _ThreePluseText;
        public string ThreePluseText
        {
            get { return _ThreePluseText; }
            set
            {
                SetProperty(ref _ThreePluseText, value);
            }
        }

        #endregion

        #region Entry and Result

        private bool _IsMaticEntries;
        public bool IsMaticEntries
        {
            get { return _IsMaticEntries; }
            set
            {
                ResetEntry();
                SetProperty(ref _IsMaticEntries, value);
                Entries = "Matic";

            }
        }



        private bool _IsEnglishEntries;
        public bool IsEnglishEntries
        {
            get { return _IsEnglishEntries; }
            set
            {
                ResetEntry();
                SetProperty(ref _IsEnglishEntries, value);
                Entries = "English";
            }
        }

        private bool _IsMaticResult;
        public bool IsMaticResult
        {
            get { return _IsMaticResult; }
            set
            {
                ResetResult();
                SetProperty(ref _IsMaticResult, value);
                Results = "Matic";
            }
        }



        private bool _IsEnglishResult;
        public bool IsEnglishResult
        {
            get { return _IsEnglishResult; }
            set
            {
                ResetResult();
                SetProperty(ref _IsEnglishResult, value);
                Entries = "English";
            }
        }
        #endregion
        #endregion

        private bool ValidateNextPrevious()
        {
            if (this.EventID > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(AddEventViewModel entity)
        {
            if (entity == null) return;
            this.EventID = entity.EventID;
            this.Event = entity.Event;
            this.EventNumber = entity.EventNumber;
            this.StartAgeRange = entity.StartAgeRange;
            this.EndAgeRange = entity.EndAgeRange;
            this.IsMultiAgeGroup = entity.IsMultiAgeGroup;
            this.Division = entity.Division;
            this.EventNotes = entity.EventNotes;
            #region Gender
            switch (entity.Gender)
            {
                case "Girls":
                    this._IsGirlsChecked = true;
                    base.OnPropertyChanged("IsGirlsChecked");
                    break;
                case "Boys":
                    this._IsBoysChecked = true;
                    base.OnPropertyChanged("IsBoysChecked");
                    break;
                case "Men":
                    this._IsMenChecked = true;
                    base.OnPropertyChanged("IsMenChecked");
                    break;
                case "Women":
                    this._IsWomenChecked = true;
                    base.OnPropertyChanged("IsWomenChecked");
                    break;
                case "Mixed":
                    this._IsMixedChecked = true;
                    base.OnPropertyChanged("IsMixedChecked");
                    break;
                default:
                    break;
            }
            #endregion

            #region Distance
            switch (entity.Distance)
            {
                case 100:
                    this.IsDistance100 = true;
                    break;
                case 10000:
                    this.IsDistance10000 = true;
                    break;
                case 110:
                    this.IsDistance110 = true;
                    break;
                case 500:
                    this.IsDistance1500 = true;
                    break;
                case 1600:
                    this.IsDistance1600 = true;
                    break;
                case 200:
                    this.IsDistance200 = true;
                    break;
                case 2000:
                    this.IsDistance2000 = true;
                    break;

                case 300:
                    this.IsDistance300 = true;
                    break;

                case 3000:
                    this.IsDistance3000 = true;
                    break;

                case 3200:
                    this.IsDistance3200 = true;
                    break;

                case 400:
                    this.IsDistance400 = true;
                    break;
                case 5000:
                    this.IsDistance5000 = true;
                    break;

                case 55:
                    this.IsDistance55 = true;
                    break;

                case 60:
                    this.IsDistance60 = true;
                    break;

                case 80:
                    this.IsDistance80 = true;
                    break;

                case 800:
                    this.IsDistance800 = true;
                    break;

                case 0:
                    this.IsDistanceNone = true;
                    break;

                default:
                    this.IsDistanceCustom = true;
                    this.DistanceCustom = entity.Distance;
                    break;
            }
            #endregion

            #region Entries

            switch (entity.Entries)
            {
                case "Matic":
                    this.IsMaticEntries = true;
                    break;
                case "English":
                    this.IsEnglishEntries = true;
                    break;
                default:
                    break;
            }

            #endregion

            #region Result

            switch (entity.Results)
            {
                case "Matic":
                    this.IsMaticResult = true;
                    break;
                case "English":
                    this.IsEnglishResult = true;
                    break;
                default:
                    break;
            }

            #endregion

            #region Event
            switch (entity.RunningEvent)
            {
                case "Dash":
                    this.IsDashRunningEvent = true;
                    break;
                case "Cross County":
                    this.IsCrossCountyRunningEvent = true;
                    break;
                case "Field Or RunRelay":
                    this.IsFieldOrRunRelayRunningEvent = true;
                    break;
                case "Run":
                    this.IsRunRunningEvent = true;
                    break;
                case "Mile Run":
                    this.IsMileRunRunningEvent = true;
                    break;
                case "Race Walk":
                    this.IsRaceWalkRunningEvent = true;
                    break;
                case "Hurdles":
                    this.IsHurdlesRunningEvent = true;
                    break;
                case "Steeplechase":
                    this.IsSteeplechaseRunningEvent = true;
                    break;
                case "Half Marathon":
                    this.IsHalfMarathonRunningEvent = true;
                    break;
                case "Marathon":
                    this.IsMarathonRunningEvent = true;
                    break;
                case "Road Race":
                    this.IsRoadRaceRunningEvent = true;
                    break;
                case "Hight Jump":
                    this.IsHightJumpFieldEvent = true;
                    break;
                case "Pole Vault":
                    this.IsPoleVaultFieldEvent = true;
                    break;
                case "Long Jump":
                    this.IsLongJumpFieldEvent = true;
                    break;
                case "Triple Jump":
                    this.IsTripleJumpFieldEvent = true;
                    break;
                case "Discus Throw":
                    this.IsDiscusThrowFieldEvent = true;
                    break;
                case "Hammer Throw":
                    this.IsHammerThrowFieldEvent = true;
                    break;
                case "Javelin Throw":
                    this.IsJavelinThrowFieldEvent = true;
                    break;
                case "Shot Put":
                    this.IsShotPutFieldEvent = true;
                    break;
                case "Weight Throw":
                    this.IsWeightThrowFieldEvent = true;
                    break;
                case "Super Weight":
                    this.IsSuperWeightFieldEvent = true;
                    break;
                case "Other":
                    this.IsOtherFieldEvent = true;
                    break;
                case "Relay":
                    this.IsRelayRelayEvent = true;
                    break;
                case "Distance Medley":
                    this.IsDistanceMedleyRelayEvent = true;
                    break;
                case "Sprint Medley":
                    this.IsSprintMedleyRelayEvent = true;
                    break;

                case "Shuttle Hurdle":
                    this.IsShuttleHurdleRelayEvent = true;
                    break;
                case "Decathlon":
                    this.IsDecathlonCombinedEvent = true;
                    break;
                case "Octathlon":
                    this.IsOctathlonCombinedEvent = true;
                    break;
                case "Heptathlon":
                    this.IsHeptathlonCombinedEvent = true;
                    break;
                case "Indoor Pentathlon":
                    this.IsIndoorPentathlonCombinedEvent = true;
                    break;
                case "Outdoor Pentathlon":
                    this.IsOutDoorPentathlonCombinedEvent = true;
                    break;
                case "Weight Pantathlon":
                    this.IsWeightPantathlonCombinedEvent = true;
                    break;
                case "Tetrathlon":
                    this.IsTetrathlonCombinedEvent = true;
                    break;
                case "Triathlon":
                    this.IsTriathlonCombinedEvent = true;
                    break;
                case "Biathlon":
                    this.IsBiathlonCombinedEvent = true;
                    break;
                default:
                    break;
            }

            #endregion

            this.NumberOfLane = entity.NumberOfLane;
            this.Advancement = entity.Advancement;
            this.HeadFlightAssign = entity.HeadFlightAssign;
            this.LanePosAssignment = entity.LanePosAssignment;

            #region Heat Order
            switch (entity.HeatOrder)
            {
                case "FasttoSlow":
                    this.IsFasttoSlowHeatOrder = true;
                    break;
                case "SlowtoFast":
                    this.IsSlowtoFastHeatOrder = true;
                    break;
                case "Random":
                    this.IsRandomHeatOrder = true;
                    break;
                default:
                    break;
            }

            #endregion

            this.HeatInSemis = entity.HeatInSemis;
            this.HeatsInQtrs = entity.HeatsInQtrs;

            #region Round
            switch (entity.Round)
            {
                case "F":
                    IsFinalOnlyRounds = true;
                    break;
                case "P/F":
                    IsPrelimsFinalRounds = true;
                    break;
                case "P/S/F":
                    IsPrelimsSemisFinalRounds = true;
                    break;
                case "P/Q/S/F":
                    IsPrelimsQtrsSemisFinalRounds = true;
                    break;
                default:
                    break;
            }
            #endregion

            this.IsScoreEvent = entity.IsScoreEvent;
            this.IsJumbOff = entity.IsJumbOff;
            this.EntryFee = entity.EntryFee;

            this.NumberOfLanesFinalRound = entity.NumberOfLanesFinalRound;
            #region Number of Final Heat
            switch (entity.Finals)
            {
                case "1 Final":
                    IsNummberofFinalHeat1Final = true;
                    break;
                case "2 Final":
                    IsNummberofFinalHeat2Final = true;
                    break;
                case "Score Prelims 2nd":
                    IsFinalScorePrelimsAs2ndFinal = true;
                    break;
                default:
                    IsNummberofFinalHeat3PluseFinal = true;
                    ThreePluseText = entity.Finals;
                    break;
            }
            FinalNumberOfLanes = entity.FinalNumberOfLanes;
            FinalOrder = entity.FinalOrder;
            NumberOfLane = entity.NumberOfLanes;
            Heats = entity.HeadFlightAssign;
            Round = entity.Round;

            #endregion

            #region final Order

            switch (entity.FinalOrder)
            {
                case "FasttoSlow":
                    IsFasttoSloworder = true;
                    break;
                case "SlowtoFast":
                    IsSlowtoFastHeatOrder = true;
                    break;
                default:
                    break;
            }
            #endregion
            this.EventMultiAgeDetailList = entity.EventMultiAgeDetailList;

        }
        public AddEventViewModel GetEntity()
        {

            return this;
        }


        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        private ICommand _NextCommand;
        public ICommand NextCommand
        {
            get { return _NextCommand; }
        }

        private ICommand _PreviousCommand;
        public ICommand PreviousCommand
        {
            get { return _PreviousCommand; }
        }
        private ICommand _MultiAgeGroupCommand;
        public ICommand MultiAgeGroupCommand
        {
            get { return _MultiAgeGroupCommand; }
        }


        #endregion
    }

}

