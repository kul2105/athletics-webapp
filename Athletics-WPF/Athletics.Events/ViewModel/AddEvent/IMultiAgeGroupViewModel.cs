﻿using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IMultiAgeGroupViewModel
    {
        ObservableCollection<MultiAgeGroup> EventMultiAgeDetailList { get; set; }
        string AgeGroup { get; set; }
    }
}
