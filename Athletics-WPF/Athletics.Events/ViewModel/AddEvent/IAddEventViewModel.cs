﻿using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IAddEventViewModel
    {
        ObservableCollection<AddEventViewModel> EventDetailList { get; set; }
        AddEventViewModel SelectedEventDetail { get; set; }
        string EventNumber { get; set; }
    }
}
