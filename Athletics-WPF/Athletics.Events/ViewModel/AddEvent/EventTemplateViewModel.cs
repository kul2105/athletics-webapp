﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.MeetSetup.Services.API;
using Athletics.SetupMeet.Model;
using Athletics.MeetSetup.SetupMeetManager;

namespace Athletics.Athletes.ViewModels
{

    public class EventTemplateViewModel : BindableBase, IEventTemplateViewModel, IGenericInteractionView<EventTemplateViewModel>
    {
        private IEventTemplateView View = null;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public EventTemplateViewModel()
        {

        }
        public EventTemplateViewModel(IUnityContainer unity, IEventTemplateView view, IEventManager meetSetupManage, List<MeetSetupModel> meetList)
        {
            View = view;
            unityContainer = unity;
            EventManager = meetSetupManage;
            View.DataContext = this;
            MeetSetupModelList = meetList;

            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      if (SelectedAddEventViewModel != null)
                                      {
                                          if (SelectedMeetSetupModel != null)
                                          {
                                              EventManager.CreateEventUsingTemplate(SelectedMeetSetupModel.MeetID, SelectedAddEventViewModel.EventID, this.EventNotes, this.StartAgeRange, this.EndAgeRange);
                                          }
                                      }
                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "EventTemplate")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "EventTemplate")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

        }




        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(EventTemplateViewModel entity)
        {
            if (entity == null) return;


        }
        public EventTemplateViewModel GetEntity()
        {

            return this;
        }

        #region Properties
        private AddEventViewModel _SelectedAddEventViewModel;
        public AddEventViewModel SelectedAddEventViewModel
        {
            get { return _SelectedAddEventViewModel; }
            set
            {
                SetProperty(ref _SelectedAddEventViewModel, value);
            }
        }

        private bool _IsCanceled = true;
        public bool IsCanceled
        {
            get { return _IsCanceled; }
            set
            {
                SetProperty(ref _IsCanceled, value);
            }
        }

        private string _EventName = string.Empty;
        public string EventName
        {
            get { return _EventName; }
            set
            {
                SetProperty(ref _EventName, value);
            }
        }

        private string _EventNotes = string.Empty;
        public string EventNotes
        {
            get { return _EventNotes; }
            set
            {
                SetProperty(ref _EventNotes, value);
            }
        }
        private MeetSetupModel _SelectedMeetSetupModel;
        public MeetSetupModel SelectedMeetSetupModel
        {
            get { return _SelectedMeetSetupModel; }
            set
            {
                SetProperty(ref _SelectedMeetSetupModel, value);
            }
        }
        private List<MeetSetupModel> _MeetSetupModelList;
        public List<MeetSetupModel> MeetSetupModelList
        {
            get { return _MeetSetupModelList; }
            set
            {
                SetProperty(ref _MeetSetupModelList, value);
            }
        }

        private int? _StartAgeRange;
        public int? StartAgeRange
        {
            get { return _StartAgeRange; }
            set
            {
                SetProperty(ref _StartAgeRange, value);
            }
        }

        private int? _EndAgeRange;
        public int? EndAgeRange
        {
            get { return _EndAgeRange; }
            set
            {
                SetProperty(ref _EndAgeRange, value);
            }
        }

        #endregion
        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }



        #endregion
    }

}

