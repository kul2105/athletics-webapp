﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.MeetSetup.Services.API;

namespace Athletics.Athletes.ViewModels
{

    public class RenumEventViewModel : BindableBase, IRenumEventViewModel, IGenericInteractionView<RenumEventViewModel>
    {
        private IRenumEventView View = null;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public RenumEventViewModel()
        {

        }
        public RenumEventViewModel(IUnityContainer unity, IRenumEventView view, IEventManager meetSetupManage)
        {
            View = view;
            unityContainer = unity;
            EventManager = meetSetupManage;
            View.DataContext = this;


            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      List<AddEventViewModel> eventList = this.EventDetailList.Where(p => Convert.ToInt32(p.EventNumber) >= this.StartingNumber && Convert.ToInt32(p.EventNumber) <= this.EndingNumber).ToList();
                                      foreach (var item in eventList)
                                      {
                                          if (IsIncreaseEvent)
                                          {
                                              item.EventNumber = (Convert.ToInt32(item.EventNumber) + 1).ToString();
                                          }
                                          else
                                          {
                                              item.EventNumber = (Convert.ToInt32(item.EventNumber) - 1).ToString();
                                          }
                                          item.EntryFee = item.EntryFee + this.OffSetAmount;
                                          ServiceRequest_Event eventServiceRequest = new ServiceRequest_Event();
                                          eventServiceRequest.Event = item;
                                          await EventManager.UpdateEvent(eventServiceRequest);
                                      }


                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "RenumEvent")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "RenumEvent")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

        }




        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(RenumEventViewModel entity)
        {
            if (entity == null) return;


        }
        public RenumEventViewModel GetEntity()
        {

            return this;
        }

        #region Properties
        private ObservableCollection<AddEventViewModel> _EventDetailList = new ObservableCollection<AddEventViewModel>();
        public ObservableCollection<AddEventViewModel> EventDetailList
        {
            get { return _EventDetailList; }
            set
            {
                SetProperty(ref _EventDetailList, value);
                if (value.Count > 0)
                {
                    int min = value.Min(p => Convert.ToInt32(p.EventNumber));
                    int max = value.Max(p => Convert.ToInt32(p.EventNumber));
                    this.StartingNumberText = string.Format("Starting Number({0}-{1})", min, max);
                }
            }
        }
        private bool _IsCanceled = true;
        public bool IsCanceled
        {
            get { return _IsCanceled; }
            set
            {
                SetProperty(ref _IsCanceled, value);
            }
        }
        private bool _IsIncreaseEvent = true;
        public bool IsIncreaseEvent
        {
            get { return _IsIncreaseEvent; }
            set
            {
                SetProperty(ref _IsIncreaseEvent, value);
            }
        }
        private bool _IsDecreaseEvent = true;
        public bool IsDecreaseEvent
        {
            get { return _IsDecreaseEvent; }
            set
            {
                SetProperty(ref _IsDecreaseEvent, value);
            }
        }
        private double _OffSetAmount;
        public double OffSetAmount
        {
            get { return _OffSetAmount; }
            set
            {
                SetProperty(ref _OffSetAmount, value);
            }
        }
        private int _StartingNumber;
        public int StartingNumber
        {
            get { return _StartingNumber; }
            set
            {
                SetProperty(ref _StartingNumber, value);
            }
        }
        private int _EndingNumber;
        public int EndingNumber
        {
            get { return _EndingNumber; }
            set
            {
                SetProperty(ref _EndingNumber, value);
            }
        }
        private string _StartingNumberText;
        public string StartingNumberText
        {
            get { return _StartingNumberText; }
            set
            {
                SetProperty(ref _StartingNumberText, value);
            }
        }
        #endregion
        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }



        #endregion
    }

}

