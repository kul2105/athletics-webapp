﻿using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IRenumEventViewModel
    {
        ObservableCollection<AddEventViewModel> EventDetailList { get; set; }
    }
}
