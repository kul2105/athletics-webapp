﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class MultiAgeGroup : BindableBase
    {
        public int standardID;
        public int StandardID
        {
            get { return standardID; }
            set
            {
                SetProperty(ref standardID, value);
            }
        }
        public int lowAge;
        public int LowAge
        {
            get { return lowAge; }
            set
            {
                SetProperty(ref lowAge, value);
            }
        }
        public int highAge;
        public int HighAge
        {
            get { return highAge; }
            set
            {
                SetProperty(ref highAge, value);
            }
        }
        public int eventID;
        public int EventID
        {
            get { return eventID; }
            set
            {
                SetProperty(ref eventID, value);
            }
        }
    }

    public class MultiAgeGroupViewModel : BindableBase, IMultiAgeGroupViewModel, IGenericInteractionView<MultiAgeGroupViewModel>
    {
        private IAddEventView View = null;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public MultiAgeGroupViewModel()
        {

        }
        private List<string> FieldEventAdvancement = new List<string>();
        private List<string> RunningEventAdvancement = new List<string>();
        private List<string> FieldEventHeatAndAssign = new List<string>();
        private List<string> RunningEventHeatAndAssign = new List<string>();
        public MultiAgeGroupViewModel(IUnityContainer unity, IAddEventView view, IEventManager meetSetupManage)
        {
            View = view;
            unityContainer = unity;
            EventManager = meetSetupManage;
            View.DataContext = this;


            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "Multiage")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "Multiage")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

        }




        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(MultiAgeGroupViewModel entity)
        {
            if (entity == null) return;


        }
        public MultiAgeGroupViewModel GetEntity()
        {

            return this;
        }

        #region Properties
        private bool _IsCanceled = true;
        public bool IsCanceled
        {
            get { return _IsCanceled; }
            set
            {
                SetProperty(ref _IsCanceled, value);
            }
        }
        private string _AgeGroup = string.Empty;
        public string AgeGroup
        {
            get { return _AgeGroup; }
            set
            {
                SetProperty(ref _AgeGroup, value);
            }
        }

        private ObservableCollection<MultiAgeGroup> _EventMultiAgeDetailList = new ObservableCollection<MultiAgeGroup>();
        public ObservableCollection<MultiAgeGroup> EventMultiAgeDetailList
        {
            get { return _EventMultiAgeDetailList; }
            set
            {
                SetProperty(ref _EventMultiAgeDetailList, value);
            }
        }
        private MultiAgeGroup _SelectedEventMultiAgeDetail = new MultiAgeGroup();
        public MultiAgeGroup SelectedEventMultiAgeDetail
        {
            get { return _SelectedEventMultiAgeDetail; }
            set
            {
                SetProperty(ref _SelectedEventMultiAgeDetail, value);
            }
        }
        #endregion
        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }



        #endregion
    }

}

