﻿using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.Athletes.Behaviors;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Athletes.Common;

namespace Athletics.Athletes.ViewModels
{
    public class EventDetail : BindableBase
    {
        private int _EventID;
        public int EventID
        {
            get { return _EventID; }
            set
            {
                SetProperty(ref _EventID, value);
            }
        }
        private string _EventNumber;
        public string EventNumber
        {
            get { return _EventNumber; }
            set
            {
                SetProperty(ref _EventNumber, value);
            }
        }

        private string _RND;
        public string RND
        {
            get { return _RND; }
            set
            {
                SetProperty(ref _RND, value);
            }
        }

        private string _EventName;
        public string EventName
        {
            get { return _EventName; }
            set
            {
                SetProperty(ref _EventName, value);
            }
        }
        private bool _IsAddedInSession;
        public bool IsAddedInSession
        {
            get { return _IsAddedInSession; }
            set
            {
                SetProperty(ref _IsAddedInSession, value);
            }
        }
    }

    public class ScessionScheduled : BindableBase
    {
        private int _SessionScheduleID;
        public int SessionScheduleID
        {
            get { return _SessionScheduleID; }
            set
            {
                SetProperty(ref _SessionScheduleID, value);
            }
        }
        private int _EventSessionID;
        public int EventSessionID
        {
            get { return _EventSessionID; }
            set
            {
                SetProperty(ref _EventSessionID, value);
            }
        }
        private Nullable<int> _EventNumber;
        public Nullable<int> EventNumber
        {
            get { return _EventNumber; }
            set
            {
                SetProperty(ref _EventNumber, value);
            }
        }
        private string _Report;
        public string Report
        {
            get { return _Report; }
            set
            {
                SetProperty(ref _Report, value);
            }
        }
        private string _RND;
        public string RND
        {
            get { return _RND; }
            set
            {
                SetProperty(ref _RND, value);
            }
        }
        private string _EventName;
        public string EventName
        {
            get { return _EventName; }
            set
            {
                SetProperty(ref _EventName, value);
            }
        }
        private Nullable<int> _SessionOrder;
        public Nullable<int> SessionOrder
        {
            get { return _SessionOrder; }
            set
            {
                SetProperty(ref _SessionOrder, value);
            }
        }
        private string _StartTime;
        public string StartTime
        {
            get { return _StartTime; }
            set
            {
                SetProperty(ref _StartTime, value);
            }
        }
        private string _AmPM;
        public string AmPM
        {
            get { return _AmPM; }
            set
            {
                SetProperty(ref _AmPM, value);
            }
        }
    }

    public class ManageSessionViewModel : BindableBase, IManageSessionViewModel, IGenericInteractionView<ManageSessionViewModel>
    {
        private IManageSessionView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IEventManager EventManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public ManageSessionViewModel(IUnityContainer unity, ManageSessionView view, IEventManager eventManager)
        {
            View = view;
            unityContainer = unity;
            EventManager = eventManager;
            View.DataContext = this;


            List<AddSessionViewModel> SessionList = EventManager.GetAllSessions().Result.ToList();
            foreach (AddSessionViewModel item in SessionList)
            {
                SessionDetailList.Add(item);
            }

            List<AddEventViewModel> EventList = EventManager.GetAllEvent().Result.ToList();
            foreach (AddEventViewModel item in EventList)
            {
                EventDetail eventDetail = new EventDetail();
                eventDetail.EventNumber = item.EventNumber;
                eventDetail.EventID = item.EventID;
                eventDetail.RND = item.Rnds;
                eventDetail.EventName = string.Format("{0} {1} Meter {2}", item.Gender, item.Distance, item.Event);
                eventDetail.IsAddedInSession = this.IsEventAddedToSession(eventDetail.EventID);
                EventDetailList.Add(eventDetail);
            }
            _DeleteScheduleCommand = new DelegateCommand(DeleteScheduleCommandHandler);
            ItemsDragDropCommand = new RelayCommand<DataGridDragDropEventArgs>((args) => DragDropItem(args), (args) => args != null && args.TargetObject != null && args.DroppedObject != null && args.Effects != System.Windows.DragDropEffects.None);
            _AddCommand = new MeetCommandViewModel(AddCommandHandler);
            _EditCommand = new MeetCommandViewModel(EditCommandHandler);
            _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
            if (EventSessionListAll != null)
                EventSessionListAll = EventManager.GetAllEventInSession().Result.ToList();
        }
        List<EventInSession> EventSessionListAll = null;
        private bool IsEventAddedToSession(int eventId)
        {
            if (EventSessionListAll == null)
            {
                EventSessionListAll = EventManager.GetAllEventInSession().Result.ToList();
            }
            EventInSession eventInsession = this.EventSessionListAll.Where(p => p.EventID == eventId).FirstOrDefault();
            if (eventInsession == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void RefreshSessionSchedule()
        {
            this.GetSessionSchedule();
            int index = SessionDetailList.IndexOf(this.SelectedSessionDetail);
            if (index + 1 == SessionDetailList.Count)
            {
                this.SelectedSessionDetail = SessionDetailList[index - 1];
            }
            else
            {
                this.SelectedSessionDetail = SessionDetailList[index + 1];
            }
        }
        private void DeleteScheduleCommandHandler()
        {
            if (SelectedEventSessionSchedule == null) return;
            EventManager.DeleteSessionSchedule(SelectedEventSessionSchedule.SessionScheduleID);
            this.GetSessionSchedule();
        }

        private void DragDropItem(DataGridDragDropEventArgs args)
        {
            if (args.DroppedObject.GetType() == typeof(EventDetail))
            {
                EventDetail eventDetail = args.DroppedObject as EventDetail;
                if (eventDetail != null)
                {
                    SelectedEventDetail = eventDetail;
                    this.AddEventToSchedule(SelectedEventDetail);
                }
            }

        }

        #region Command Handler
        private void HelpCommandHandler()
        {
            MessageBox.Show("Help");
        }


        private void PrintCommandandler()
        {
            MessageBox.Show("Print");
        }


        private void RemoveAllCommandHandler()
        {
            EventSessionScheduleList.Clear();
        }

        private void AutoNumberCommandHandler()
        {
            MessageBox.Show("Standards");
        }


        private void MoveAllCommandHandler()
        {
            foreach (var eventDetail in EventDetailList)
            {
                ScessionScheduled schedule = EventSessionScheduleList.Where(p => p.EventName == eventDetail.EventName && ((p.EventNumber != null) && p.EventNumber.Value.ToString() == eventDetail.EventNumber)).FirstOrDefault();
                if (schedule == null)
                {
                    if (eventDetail != null)
                    {
                        GlobalEvent.RaiseTargetControlLoaded(eventDetail);
                    }
                }
            }
        }

        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        if (SelectedSessionDetail == null) return;
                        AuthorizedResponse<SessionResult> Response = await EventManager.DeleteSession(SelectedSessionDetail.SessionID);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleteing the Event", "Event Delete Error");
                            return;
                        }
                        this.AthleteRelayPreferencesId = Response.Result.SessionID;
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             this.SessionDetailList.Remove(SelectedSessionDetail);
                         }));

                    });

                })).Wait();
            });
            this.GetSessionSchedule();
        }

        private void EditCommandHandler()
        {
            if (SelectedSessionDetail == null) return;
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddSessionView();
            window.Name = "AddSession";
            // IAddSessionView sessionView = window as IAddSessionView;
            AddSessionViewModel addSessionViewModel = new AddSessionViewModel(window);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            if (addSessionViewModel != null)
            {
                addSessionViewModel.SetEntity(SelectedSessionDetail);
                dialog.BindView(window);
                dialog.BindViewModel(addSessionViewModel);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            AddSessionViewModel sessionVM = addSessionViewModel.GetEntity();
            this.SelectedSessionDetail.Day = sessionVM.Day;
            this.SelectedSessionDetail.EntryLimit = sessionVM.EntryLimit;
            this.SelectedSessionDetail.Session = sessionVM.Session;
            this.SelectedSessionDetail.SessionID = sessionVM.SessionID;
            this.SelectedSessionDetail.SessionTitle = sessionVM.SessionTitle;
            this.SelectedSessionDetail.StartTime = sessionVM.StartTime;
            //AddSessionView window1 = new AddSessionView();
            //window1.Name = "AddSession";
            //this.SelectedSessionDetail.InitilizedEvents(window1);
            //window1.DataContext = this.SelectedSessionDetail;
            //window1.ShowDialog();
            //window1.Close();
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        ServiceRequest_Session serviceObj = new ServiceRequest_Session();
                        serviceObj.Session = this.SelectedSessionDetail;
                        AuthorizedResponse<SessionResult> Response = await EventManager.UpdateSession(serviceObj);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while Updating the Event", "Event Add Error");
                            return;
                        }
                    });

                })).Wait();
            });

        }
        public List<EventSessionSchedule> sessionSheduleAll = new List<EventSessionSchedule>();
        private void AddCommandHandler()
        {
            AddSessionView window1 = new AddSessionView();
            window1.Name = "AddSession";
            AddSessionViewModel addSessionViewModel = new AddSessionViewModel(window1);
            window1.DataContext = addSessionViewModel;
            window1.ShowDialog();
            window1.Close();
            addSessionViewModel = addSessionViewModel.GetEntity();
            if (addSessionViewModel.IsCanceled == true) return;
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        ServiceRequest_Session serviceObj = new ServiceRequest_Session();
                        serviceObj.Session = addSessionViewModel;
                        AuthorizedResponse<SessionResult> Response = await EventManager.CreateNewSession(serviceObj);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while adding the Event", "Event Add Error");
                            return;
                        }
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            addSessionViewModel.SessionID = Response.Result.SessionID;
                            this.SessionDetailList.Add(addSessionViewModel);
                        }));
                    });

                })).Wait();
            });

        }

        private void ClearViewModel(IGenericInteractionView<AddSessionViewModel> addEventViewModel)
        {
            addEventViewModel.SetEntity(new AddSessionViewModel());
        }


        #endregion

        private ObservableCollection<AddSessionViewModel> _SessionDetailList = new ObservableCollection<AddSessionViewModel>();
        public ObservableCollection<AddSessionViewModel> SessionDetailList
        {
            get { return _SessionDetailList; }
            set
            {
                SetProperty(ref _SessionDetailList, value);
            }
        }
        public static int SelectedSessionID { get; set; }
        private AddSessionViewModel _SelectedSessionDetail;
        public AddSessionViewModel SelectedSessionDetail
        {
            get { return _SelectedSessionDetail; }
            set
            {
                SetProperty(ref _SelectedSessionDetail, value);
                SessionDetailList.ToList().ForEach(p => p.IsSelected = false);
                if (value != null)
                {
                    SelectedSessionID = value.SessionID;
                }
                this.GetSessionSchedule();

            }
        }



        public ObservableCollection<EventDetail> _EventDetailList = new ObservableCollection<EventDetail>();
        public ObservableCollection<EventDetail> EventDetailList
        {
            get { return _EventDetailList; }
            set
            {
                SetProperty(ref _EventDetailList, value);
            }
        }
        public ObservableCollection<ScessionScheduled> _EventSessionScheduleList = new ObservableCollection<ScessionScheduled>();
        public ObservableCollection<ScessionScheduled> EventSessionScheduleList
        {
            get { return _EventSessionScheduleList; }
            set
            {
                SetProperty(ref _EventSessionScheduleList, value);
            }
        }
        private ScessionScheduled _SelectedEventSessionSchedule;
        public ScessionScheduled SelectedEventSessionSchedule
        {
            get { return _SelectedEventSessionSchedule; }
            set
            {
                SetProperty(ref _SelectedEventSessionSchedule, value);
            }
        }
        private EventDetail _SelectedEventDetail;
        public EventDetail SelectedEventDetail
        {
            get { return _SelectedEventDetail; }
            set
            {
                SetProperty(ref _SelectedEventDetail, value);
            }
        }

        //#region Menu Command


        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }

        //private ICommand _MoveAllCommand;
        //public ICommand MoveAllCommand
        //{
        //    get
        //    {
        //        return _MoveAllCommand;
        //    }
        //}

        //private ICommand _RemoveAllCommand;
        //public ICommand RemoveAllCommand
        //{
        //    get
        //    {
        //        return _RemoveAllCommand;
        //    }
        //}

        //private ICommand _AutoNumberCommand;
        //public ICommand AutoNumberCommand
        //{
        //    get
        //    {
        //        return _AutoNumberCommand;
        //    }
        //}


        //private ICommand _PrintCommand;
        //public ICommand PrintCommand
        //{
        //    get
        //    {
        //        return _PrintCommand;
        //    }
        //}

        //private ICommand _HelpCommand;
        //public ICommand HelpCommand
        //{
        //    get
        //    {
        //        return _HelpCommand;
        //    }
        //}
        private ICommand _DeleteScheduleCommand;
        public ICommand DeleteScheduleCommand
        {
            get
            {
                return _DeleteScheduleCommand;
            }
        }

        public RelayCommand<DataGridDragDropEventArgs> ItemsDragDropCommand { get; private set; }
        //#endregion

        private bool ValidateInputs()
        {
            return true;

        }

        public void GetSessionSchedule()
        {
            if (SelectedSessionDetail == null)
            {
                EventSessionScheduleList = null;
                return;
            }
            EventSessionScheduleList = new ObservableCollection<ScessionScheduled>();
            List<EventSessionSchedule> listofScheduld = EventManager.GetAllSessionSchedules(SelectedSessionDetail.SessionID).Result.ToList();
            ObservableCollection<ScessionScheduled> schedule = new ObservableCollection<ScessionScheduled>();
            foreach (EventSessionSchedule item in listofScheduld.OrderBy(p => p.SessionOrder))
            {
                ScessionScheduled sessionScheduled = new ScessionScheduled();
                sessionScheduled.AmPM = item.AmPM;
                sessionScheduled.EventName = item.EventName;
                sessionScheduled.EventName = item.EventName;
                sessionScheduled.EventNumber = item.EventNumber;
                sessionScheduled.EventSessionID = item.EventSessionID;
                sessionScheduled.Report = item.Report;
                sessionScheduled.RND = item.RND;
                sessionScheduled.SessionOrder = item.SessionOrder;
                sessionScheduled.SessionScheduleID = item.SessionScheduleID;
                sessionScheduled.StartTime = item.StartTime;
                schedule.Add(sessionScheduled);
            }
            sessionSheduleAll = EventManager.GetAllSessionSchedules().Result.ToList();
            EventSessionScheduleList = schedule;
            View.ScheduledGrid.FilteredItemsSource = EventSessionScheduleList;
        }

        public void SetEntity(ManageSessionViewModel entity)
        {

        }
        private ObservableCollection<EventMenuItemViewModel> _MenuItems;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }
        public ManageSessionViewModel GetEntity()
        {
            return this;
        }

        public void AddEventToSchedule(EventDetail EventDetail)
        {
            if (SelectedSessionDetail == null)
            {
                MessageBox.Show(string.Format("Session is not selected.Please select session first"));
                return;
            }
            if (EventDetail == null) return;
            if (this.EventSessionScheduleList.Where(p => p.EventNumber.Value.ToString() == EventDetail.EventNumber).Count() > 0) return;
            if (this.sessionSheduleAll.Where(p => p.EventNumber.Value.ToString() == EventDetail.EventNumber).Count() > 0)
            {
                MessageBox.Show(string.Format("Event with event number {0} is already add to some other schedule", EventDetail.EventNumber));
                return;
            }
            ServiceRequest_SessionSchedule sessionSchedule = new ServiceRequest_SessionSchedule();
            sessionSchedule.EventID = EventDetail.EventID;
            EventSessionSchedule eventSessionShedule = new EventSessionSchedule();
            eventSessionShedule.EventName = EventDetail.EventName;
            eventSessionShedule.EventNumber = Convert.ToInt32(EventDetail.EventNumber);
            eventSessionShedule.EventSessionID = SelectedSessionDetail.SessionID;
            eventSessionShedule.Report = "H";
            eventSessionShedule.RND = EventDetail.RND;
            eventSessionShedule.SessionOrder = this.EventSessionScheduleList.Count + 1;
            string time = Convert.ToDateTime(SelectedSessionDetail.StartTime.ToString()).ToShortTimeString();// SelectedSessionDetail.StartTime.Substring(0, SelectedSessionDetail.StartTime.Count() - 2);
            string AMPM = Convert.ToDateTime(SelectedSessionDetail.StartTime.ToString()).ToString("tt"); //SelectedSessionDetail.StartTime.Substring(SelectedSessionDetail.StartTime.Count() - 2, 2);
            eventSessionShedule.AmPM = AMPM;
            sessionSchedule.Session = eventSessionShedule;
            sessionSchedule.SessionID = SelectedSessionDetail.SessionID;
            SessionScheduleResult sessionScheduleResult = EventManager.CreateNewSessionSchedule(sessionSchedule).Result;
            this.GetSessionSchedule();
        }

        internal void UpdateSessionSchedule(EventSessionSchedule draggedItem)
        {
            if (draggedItem == null) return;
            ServiceRequest_SessionSchedule sessionSchedule = new ServiceRequest_SessionSchedule();
            sessionSchedule.Session = draggedItem;
            sessionSchedule.SessionScheduleID = draggedItem.SessionScheduleID;
            EventManager.UpdateSessionSchedule(sessionSchedule);
        }
    }

}

