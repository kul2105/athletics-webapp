﻿using Athletics.Athletes.SetupMeetManager;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Athletics.Athletes.ViewModels
{
    public interface IManageSessionViewModel
    {
        ObservableCollection<AddSessionViewModel> SessionDetailList { get; set; }
        AddSessionViewModel SelectedSessionDetail { get; set; }
        ObservableCollection<ScessionScheduled> EventSessionScheduleList { get; set; }
        ObservableCollection<EventDetail> EventDetailList { get; set; }
        void AddEventToSchedule(EventDetail eventDetail);
        EventDetail SelectedEventDetail { get; set; }
        void RefreshSessionSchedule();
       
    }
}
