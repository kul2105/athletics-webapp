﻿using Athletics.Athletes.Common;
using Athletics.Athletes.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class SessionMenuViewModel : BindableBase, ISessionMenuViewModel
    {
        private ObservableCollection<EventMenuItemViewModel> _MenuItems = new ObservableCollection<EventMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IEventManager eventManager;
        IManageSessionView View;
        IManageSessionViewModel ViewModel;
        ISessionMenuView SessionMenuView;
        public ObservableCollection<EventMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public SessionMenuViewModel(IUnityContainer unity, ISessionMenuView _View, IManageSessionView ManageSessionView, IEventManager manager)
        {
            eventManager = manager;
            View = ManageSessionView;
            ViewModel = ManageSessionView.DataContext as IManageSessionViewModel;
            SessionMenuView = _View;
            SessionMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<EventMenuItemViewModel>();
                EventMenuItemViewModel eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Add";
                _AddCommand = new MeetCommandViewModel(AddCommandHandler);
                eventMenu.Command = AddCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Edit";
                _EditCommand = new MeetCommandViewModel(EditCommandHandler);
                eventMenu.Command = EditCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Delete";
                _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
                eventMenu.Command = DeleteCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Move All";
                _MoveAllCommand = new MeetCommandViewModel(MoveAllCommandHandler);
                eventMenu.Command = MoveAllCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Remove All";
                _RemoveAllCommand = new MeetCommandViewModel(RemoveAllCommandHandler);
                eventMenu.Command = RemoveAllCommand;
                MenuItems.Add(eventMenu);

                //eventMenu = new EventMenuItemViewModel();
                //eventMenu.Header = "Auto Number";
                //_AutoNumberCommand = new MeetCommandViewModel(AutoNumberCommandHandler);
                //eventMenu.Command = AutoNumberCommand;
                //MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Print";
                _PrintCommand = new MeetCommandViewModel(PrintCommandandler);
                eventMenu.Command = PrintCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new EventMenuItemViewModel();
                eventMenu.Header = "Help";
                _HelpCommand = new MeetCommandViewModel(HelpCommandHandler);
                eventMenu.Command = HelpCommand;
                MenuItems.Add(eventMenu);

                #endregion

            });
            unityContainer = unity;
            //View.DataContext = this;

        }
        #region Menu Command


        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }

        private ICommand _MoveAllCommand;
        public ICommand MoveAllCommand
        {
            get
            {
                return _MoveAllCommand;
            }
        }

        private ICommand _RemoveAllCommand;
        public ICommand RemoveAllCommand
        {
            get
            {
                return _RemoveAllCommand;
            }
        }

        private ICommand _AutoNumberCommand;
        public ICommand AutoNumberCommand
        {
            get
            {
                return _AutoNumberCommand;
            }
        }


        private ICommand _PrintCommand;
        public ICommand PrintCommand
        {
            get
            {
                return _PrintCommand;
            }
        }

        private ICommand _HelpCommand;
        public ICommand HelpCommand
        {
            get
            {
                return _HelpCommand;
            }
        }
        private ICommand _DeleteScheduleCommand;
        public ICommand DeleteScheduleCommand
        {
            get
            {
                return _DeleteScheduleCommand;
            }
        }


        #endregion

        #region Command Handler

        private void HelpCommandHandler()
        {
            MessageBox.Show("Help");
        }


        private void PrintCommandandler()
        {
            System.Windows.Controls.PrintDialog pdPrintDialog = new System.Windows.Controls.PrintDialog();
            if ((bool)pdPrintDialog.ShowDialog().GetValueOrDefault())
            {
                ManageSessionView sessionView = View as ManageSessionView;
                if (sessionView != null)
                {
                    Size pntPageSize = new Size(pdPrintDialog.PrintableAreaWidth, pdPrintDialog.PrintableAreaHeight);
                    sessionView.dataGrid2.Measure(pntPageSize);
                    sessionView.dataGrid2.Arrange(new Rect(5, 5, pntPageSize.Width, pntPageSize.Height));
                    pdPrintDialog.PrintVisual(sessionView.dataGrid2, "Session");
                }
            }
        }


        private void RemoveAllCommandHandler()
        {
            List<EventSessionSchedule> ListSessionSchedule = eventManager.GetAllSessionSchedules(ManageSessionViewModel.SelectedSessionID).Result.ToList();
            foreach (var item in ListSessionSchedule)
            {
                eventManager.DeleteSessionSchedule(item.SessionScheduleID);
            }
            AddSessionViewModel currentSelected = eventManager.GetAllSessions().Result.Where(p => p.SessionID == ManageSessionViewModel.SelectedSessionID).FirstOrDefault();
            ViewModel.SelectedSessionDetail = currentSelected;
            //ViewModel.RefreshSessionSchedule();

        }

        private void AutoNumberCommandHandler()
        {
            MessageBox.Show("Standards");
        }


        private void MoveAllCommandHandler()
        {
            foreach (var eventDetail in ViewModel.EventDetailList)
            {
                if (ViewModel.EventSessionScheduleList == null) continue;
                ScessionScheduled schedule = ViewModel.EventSessionScheduleList.Where(p => p.EventName == eventDetail.EventName && ((p.EventNumber != null) && p.EventNumber.Value.ToString() == eventDetail.EventNumber)).FirstOrDefault();
                if (schedule == null)
                {
                    if (eventDetail != null)
                    {
                        ViewModel.SelectedEventDetail = eventDetail;
                        AddSessionViewModel sessiondetail = ViewModel.SessionDetailList.Where(p => p.SessionID == ManageSessionViewModel.SelectedSessionID).FirstOrDefault();
                        ViewModel.SelectedSessionDetail = sessiondetail;
                        ViewModel.AddEventToSchedule(eventDetail);

                    }
                }
            }
            AddSessionViewModel currentSelected = ViewModel.SessionDetailList.Where(p => p.SessionID == ManageSessionViewModel.SelectedSessionID).FirstOrDefault();
            ViewModel.SelectedSessionDetail = null;
            ViewModel.SelectedSessionDetail = currentSelected;
            ViewModel.RefreshSessionSchedule();
        }

        private void DeleteCommandHandler()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {

                if (ViewModel.SelectedSessionDetail == null) return;
                AuthorizedResponse<SessionResult> Response = await eventManager.DeleteSession(ViewModel.SelectedSessionDetail.SessionID);
                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while deleteing the Event", "Event Delete Error");
                    return;
                }
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        ViewModel.SessionDetailList.Remove(ViewModel.SelectedSessionDetail);
                    }));

            }));

            //this.GetSessionSchedule();
        }

        private void EditCommandHandler()
        {
            if (ViewModel.SelectedSessionDetail == null) return;
            AddSessionView window1 = new AddSessionView();
            window1.Name = "AddSession";
            ViewModel.SelectedSessionDetail.InitilizedEvents(window1);
            window1.DataContext = ViewModel.SelectedSessionDetail;
            window1.ShowDialog();
            window1.Close();
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        ServiceRequest_Session serviceObj = new ServiceRequest_Session();
                        serviceObj.Session = ViewModel.SelectedSessionDetail;
                        AuthorizedResponse<SessionResult> Response = await eventManager.UpdateSession(serviceObj);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while Updating the Event", "Event Add Error");
                            return;
                        }
                    });

                })).Wait();
            });

        }
        private void AddCommandHandler()
        {
            AddSessionView window1 = new AddSessionView();
            window1.Name = "AddSession";
            AddSessionViewModel addSessionViewModel = new AddSessionViewModel(window1);
            window1.DataContext = addSessionViewModel;
            window1.ShowDialog();
            window1.Close();
            addSessionViewModel = addSessionViewModel.GetEntity();
            if (addSessionViewModel.IsCanceled == true) return;
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        ServiceRequest_Session serviceObj = new ServiceRequest_Session();
                        serviceObj.Session = addSessionViewModel;
                        AuthorizedResponse<SessionResult> Response = await eventManager.CreateNewSession(serviceObj);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while adding the Event", "Event Add Error");
                            return;
                        }
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            addSessionViewModel.SessionID = Response.Result.SessionID;
                            ViewModel.SessionDetailList.Add(addSessionViewModel);
                        }));
                    });

                })).Wait();
            });

        }



        #endregion

    }
}

