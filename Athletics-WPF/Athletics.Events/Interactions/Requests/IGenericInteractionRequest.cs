﻿using System;

namespace Athletics.Athletes.Requests
{
    public interface IGenericInteractionRequest<T>
    {
        event EventHandler<GenericInteractionRequestEventArgs<T>> Raised;
    }
}
