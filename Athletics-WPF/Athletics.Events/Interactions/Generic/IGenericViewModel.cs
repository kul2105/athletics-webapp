﻿
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using System.ComponentModel;
namespace Athletics.Athletes.Generic
{
    public interface IGenericViewModel<T> : IGenericInteractionView<T>, INotifyPropertyChanged
    {
        T Entity { get; set; }
    }
}
