﻿
namespace Athletics.Athletes.Generic
{
    public interface IGenericAdapter<T>
    {
        IGenericViewModel<T> ViewModel { get; }
    }
}
