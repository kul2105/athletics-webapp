﻿using Athletics.Athletes.Behaviors;
using Athletics.Athletes.Common;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace Athletics.Athletes.Behaviors
{
    public class ListViewDragDropBehavior : Behavior<ListView>
    {
        #region Dependency Properties
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(RelayCommand<DataGridDragDropEventArgs>), typeof(ListViewDragDropBehavior), new UIPropertyMetadata(null));

        public static readonly DependencyProperty AllowedEffectsProperty =
            DependencyProperty.Register("AllowedEffects", typeof(DragDropEffects), typeof(ListViewDragDropBehavior), new UIPropertyMetadata(DragDropEffects.Move));

        public static readonly DependencyProperty TargetGridProperty =
           DependencyProperty.Register("TargetGrid", typeof(Grid), typeof(ListViewDragDropBehavior), new UIPropertyMetadata(null));

        #endregion

        #region Properties
        public DragDropEffects AllowedEffects
        {
            get { return (DragDropEffects)GetValue(AllowedEffectsProperty); }
            set { SetValue(AllowedEffectsProperty, value); }
        }
        public ListView TargetGrid
        {
            get { return (ListView)GetValue(TargetGridProperty); }
            set { SetValue(TargetGridProperty, value); }
        }


        public RelayCommand<DataGridDragDropEventArgs> Command
        {
            get { return (RelayCommand<DataGridDragDropEventArgs>)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
        #endregion

        private object _itemToMove = null;
        private object _dropTarget = null;

        private DataGridDragDropDirection _direction = DataGridDragDropDirection.Indeterminate;
        private object _source = null;
        private object _destination = null;

        private int _lastIndex = -1;

        protected override void OnAttached()
        {
            // Mouse Move
            WeakEventListener<ListViewDragDropBehavior, ListView, MouseEventArgs> mouseListener = new WeakEventListener<ListViewDragDropBehavior, ListView, MouseEventArgs>(this, AssociatedObject);
            mouseListener.OnEventAction = (instance, source, args) => instance.DataGrid_MouseMove(source, args);
            mouseListener.OnDetachAction = (listenerRef, source) => source.MouseMove -= listenerRef.OnEvent;
            AssociatedObject.MouseMove += mouseListener.OnEvent;

            // Drag Enter/Leave/Over
            WeakEventListener<ListViewDragDropBehavior, ListView, DragEventArgs> dragListener = new WeakEventListener<ListViewDragDropBehavior, ListView, DragEventArgs>(this, AssociatedObject);
            dragListener.OnEventAction = (instance, source, args) => instance.DataGrid_CheckDropTarget(source, args);
            dragListener.OnDetachAction = (listenerRef, source) =>
            {
                source.DragEnter -= listenerRef.OnEvent;
                source.DragLeave -= listenerRef.OnEvent;
                source.DragOver -= listenerRef.OnEvent;
            };
            AssociatedObject.DragEnter += dragListener.OnEvent;
            AssociatedObject.DragLeave += dragListener.OnEvent;
            AssociatedObject.DragOver += dragListener.OnEvent;

            //// Drop
            DependencyObject dobj = this.TargetGrid;
            WeakEventListener<ListViewDragDropBehavior, ListView, DragEventArgs> dropListener = new WeakEventListener<ListViewDragDropBehavior, ListView, DragEventArgs>(this, AssociatedObject);
            dropListener.OnEventAction = (instance, source, args) => instance.DataGrid_Drop(source, args);
            dropListener.OnDetachAction = (listenerRef, source) => source.Drop -= listenerRef.OnEvent;
            AssociatedObject.Drop += dropListener.OnEvent;
            GlobalEvent.TargetControlLoaded += SeedPreferencesViewModel_TargetControlLoaded;
            base.OnAttached();
        }

        private void SeedPreferencesViewModel_TargetControlLoaded(object targetGrid)
        {
            //WeakEventListener<DataGridDragDropBehavior, ListView, DragEventArgs> dropListener = new WeakEventListener<DataGridDragDropBehavior, ListView, DragEventArgs>(this, targetGrid);
            //dropListener.OnEventAction = (instance, source, args) => instance.DataGrid_Drop(source, args);
            //dropListener.OnDetachAction = (listenerRef, source) => source.Drop -= listenerRef.OnEvent;
            //AssociatedObject.Drop += dropListener.OnEvent;
            _dropTarget = targetGrid;
        }

        private void DataGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                ListViewItem row = UIHelper.FindVisualParent<ListViewItem>(e.OriginalSource as FrameworkElement);
                if (row != null && row.IsSelected)
                {
                    _source = UIHelper.FindVisualParent<ListView>(row).ItemsSource;
                    _itemToMove = row;
                    DragDropEffects finalEffects = DragDrop.DoDragDrop(row, _itemToMove, AllowedEffects);
                    DataGridDragDropEventArgs args = new DataGridDragDropEventArgs()
                    {
                        Destination = _destination,
                        Direction = _direction,
                        DroppedObject = _itemToMove,
                        Effects = finalEffects,
                        Source = _source,
                        TargetObject = _dropTarget
                    };

                    if (_dropTarget != null && Command != null && Command.CanExecute(args))
                    {
                        Command.Execute(args);

                        _itemToMove = null;
                        _dropTarget = null;
                        _source = null;
                        _destination = null;
                        _direction = DataGridDragDropDirection.Indeterminate;
                        _lastIndex = -1;
                    }
                }
            }
        }

        private void DataGrid_CheckDropTarget(object sender, DragEventArgs e)
        {
            ListViewItem row = UIHelper.FindVisualParent<ListViewItem>(e.OriginalSource as UIElement);
            if (row == null)
            {
                // Not over a ListViewItem
                e.Effects = DragDropEffects.None;
            }
            else
            {
                ListView listView = e.OriginalSource as ListView;
                int curIndex = listView.Items.IndexOf(row);
                _direction = curIndex > _lastIndex ? DataGridDragDropDirection.Down : (curIndex < _lastIndex ? DataGridDragDropDirection.Up : _direction);
                _lastIndex = curIndex;

                e.Effects = ((e.AllowedEffects & DragDropEffects.Copy) == DragDropEffects.Copy) && (e.KeyStates & DragDropKeyStates.ControlKey) == DragDropKeyStates.ControlKey ? DragDropEffects.Copy : DragDropEffects.Move;
            }

            e.Handled = true;
        }

        private void DataGrid_Drop(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            // Verify that this is a valid drop and then store the drop target
            ListViewItem row = UIHelper.FindVisualParent<ListViewItem>(e.OriginalSource as UIElement);
            if (row != null)
            {
                _destination = UIHelper.FindVisualParent<ListView>(row).ItemsSource;
                _dropTarget = row;
                if (_dropTarget != null)
                {
                    e.Effects = ((e.AllowedEffects & DragDropEffects.Copy) == DragDropEffects.Copy) && (e.KeyStates & DragDropKeyStates.ControlKey) == DragDropKeyStates.ControlKey ? DragDropEffects.Copy : DragDropEffects.Move;
                }
            }
        }

    }

}
