﻿using Athletics.Athletes.SetupMeetManager;

namespace Athletics.Athletes.Common
{
    public class GlobalEvent
    {
        public delegate void TargetControlLoadedDelegate(object targetObject);
        public static event TargetControlLoadedDelegate TargetControlLoaded;
        public static void RaiseTargetControlLoaded(object targetObject)
        {
            if (TargetControlLoaded != null)
                TargetControlLoaded(targetObject);
        }

        public static IEventManager EventManager { get; set; }
    }
}
