﻿using log4net.Appender;
using log4net.Core;
using log4net.Util;
using System;

namespace Athletics.Logger
{
    public class AthleticsAppender : AppenderSkeleton
    {
        private readonly static Type declaringType = typeof(AthleticsAppender);

        internal static void LogWarning(string Message)
        {
            LogLog.Warn(declaringType, Message);
        }

        internal static void LogError(string Message)
        {
            LogLog.Error(declaringType, Message);
        }

        internal static void LogDebug(string Message)
        {
            LogLog.Debug(declaringType, Message);
        }
        public override void ActivateOptions()
        {
            LogLog.Debug(declaringType, "Debug - Appending...");
            LogLog.Warn(declaringType, "Warn - Activating options...");
            LogLog.Error(declaringType, "Error - Activating options...");
            base.ActivateOptions();
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            LogLog.Debug(declaringType, "Debug - Appending...");
            LogLog.Warn(declaringType, "Warn - Appending...");
            LogLog.Error(declaringType, "Error - Appending...");
        }

    }
}
