﻿namespace Athletics.Logger
{
    public class LoggerManager
    {
        public static void LogWarning(string Message)
        {

            AthleticsAppender.LogWarning(Message);
        }

        public static void LogError(string Message)
        {
            AthleticsAppender.LogError(Message);
        }

        public static void LogDebug(string Message)
        {
            AthleticsAppender.LogDebug(Message);
        }
    }
}
