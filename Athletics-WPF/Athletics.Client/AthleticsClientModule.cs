﻿using Athletics.Client.Controllers;
using Athletics.Client.Interactions;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.ViewModels;
using Athletics.Client.Views;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

namespace Athletics.Client
{
    public class AthleticsClientModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public AthleticsClientModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            // Implements the application-wide authorization controller
            this.container.RegisterType<IApplicationAuthStateController, ApplicationAuthStateController>(new ContainerControlledLifetimeManager());

            // Implements the application-wide ASP.NET API interface
            this.container.RegisterType<IAuthorizedAPIServiceProvider, AuthorizedAPIServiceProvider>(new ContainerControlledLifetimeManager());
            // Implements the application-wide authentication token cache


            this.container.RegisterType<IApplicationUserController, ApplicationUserController>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<ISignInManager, SignInManager>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISignedInUserViewModel, SignedInUserViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISignedInUserViewModelWriteable, SignedInUserViewModel>(new ContainerControlledLifetimeManager());

            // These are view regions within the authenticated app shell view
            this.regionManager.RegisterViewWithRegion("SignInDefaultViewRegion", typeof(SignInDefaultView));
            this.regionManager.RegisterViewWithRegion("SigningInPleaseWaitViewRegion", typeof(SigningInPleaseWaitView));
            this.regionManager.RegisterViewWithRegion("SigningUpPleaseWaitViewRegion", typeof(SigningUpPleaseWaitView));
            this.regionManager.RegisterViewWithRegion("SignUpViewRegion", typeof(SignUpView));
            this.regionManager.RegisterViewWithRegion("AccountTitleBarViewRegion", typeof(AccountTitleBarView));
            this.regionManager.RegisterViewWithRegion("CreateRoleRegion", typeof(UserRoleView));
            this.regionManager.RegisterViewWithRegion("AssignRoleRegion", typeof(UserInRoleView));
            this.regionManager.RegisterViewWithRegion("CreateMenuRegion", typeof(ApplicationMenuView));
            this.regionManager.RegisterViewWithRegion("AssignMenuRegion", typeof(MenuInRoleView));
            // View and view model registration
            this.container.RegisterType<ISignInDefaultViewModel, SignInDefaultViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISignInDefaultView, SignInDefaultView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISigningInPleaseWaitViewModel, SigningInPleaseWaitViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISigningUpPleaseWaitViewModel, SigningUpPleaseWaitViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISigningInPleaseWaitView, SigningInPleaseWaitView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISignUpViewModel, SignUpViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ISignUpView, SignUpView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IUserRoleViewModel, UserRoleViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IUserRoleView, UserRoleView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IUserInRoleViewModel, UserInRoleViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IUserInRoleView, UserInRoleView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IMenuInRoleViewModel, MenuInRoleViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IMenuInRoleView, MenuInRoleView>(new ContainerControlledLifetimeManager());


            this.container.RegisterType<IApplicationMenuViewModel, ApplicationMenuViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IApplicationMenuView, ApplicationMenuView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IAuthenticatedShellViewModel, AuthenticatedShellViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAuthenticatedShellView, AuthenticatedShellView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IAccountTitleBarViewModel, AccountTitleBarViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAccountTitleBarView, AccountTitleBarView>(new ContainerControlledLifetimeManager());

            // Register interactions
            this.container.RegisterType<IManageUsersViewModel, ManageUsersViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageUsersView, ManageUsersView>(new ContainerControlledLifetimeManager());

            //Interaction regions
            this.regionManager.RegisterViewWithRegion("ManageUsersViewRegion", typeof(ManageUsersView));

            //this.regionManager.RegisterViewWithRegion("SignUpViewRegion", typeof(SignUpView));
        }
    }
}
