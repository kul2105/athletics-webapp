﻿using Athletics.Client.ViewModels;
using Athletics.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Models
{
    public class ApplicationUserObservableCollection : INotifyCollectionChanged, IEnumerable<SignedInUserViewModel>
    {
        private ObservableCollection<SignedInUserViewModel> _RegisteredUsers = new ObservableCollection<SignedInUserViewModel>();
        public ObservableCollection<SignedInUserViewModel> RegisteredUsers
        {
            get { return _RegisteredUsers; }
        }

        public ApplicationUserObservableCollection()
        {
            _RegisteredUsers.CollectionChanged += NotifyCollectionChanged;
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public IEnumerator<SignedInUserViewModel> GetEnumerator()
        {
            foreach (SignedInUserViewModel registeredUser in _RegisteredUsers)
            {
                yield return registeredUser;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (SignedInUserViewModel registeredUser in _RegisteredUsers)
            {
                yield return registeredUser;
            }
        }

        void NotifyCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedEventHandler pCollectionChanged = CollectionChanged;
            if (pCollectionChanged != null)
            {
                pCollectionChanged(sender, e);
            }
        }
        public void Clear()
        {
            RegisteredUsers.Clear();
        }
    }
}
