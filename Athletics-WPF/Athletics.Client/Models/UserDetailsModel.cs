﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Athletics.Client.Models
{
    public class UserDetailsModel
    {
        public string UserKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName 
        {
            get { return FirstName + " " + LastName; }
        }

        public string DisplayName { get; set; }
        public Image Avatar { get; set; }
        public string ContactNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool IsAdmin { get; set; }
    }
}
