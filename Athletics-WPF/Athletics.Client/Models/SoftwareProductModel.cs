﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Athletics.Client.Models
{
    public class SoftwareProductModel
    {
        public string PackageID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public Image FeaturedImage { get; set; }
        public string PublisherName { get; set; }
        public string PublisherKey { get; set; }
        public bool IsFeatured { get; set; }
        public bool IsHot { get; set; }
        public bool IsNew { get; set; }
        public bool IsOnSale { get; set; }
        public float BasePrice { get; set; }
        public float Tax { get; set; }
    }
}
