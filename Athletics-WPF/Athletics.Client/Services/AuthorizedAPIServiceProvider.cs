﻿using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Athletics.Client.Services.API
{
    public class AuthorizedAPIServiceProvider : BindableBase, IAuthorizedAPIServiceProvider
    {
        private object _Lock = new object();

        public AuthorizedAPIServiceProvider()
        {
        }

        public string AuthToken { get; set; }

        private eAUTHORIZED_REQUEST_RESPONSE ErrorResponseFromMessage(string Message)
        {
            switch(Message)
            {
                case "User Not Found.":
                    return eAUTHORIZED_REQUEST_RESPONSE.USER_NOT_FOUND;
                case "A password reset is required.":
                    return eAUTHORIZED_REQUEST_RESPONSE.USER_PASSWORD_RESET_REQUIRED;
                case "Email not confirmed.":
                    return eAUTHORIZED_REQUEST_RESPONSE.USER_EMAIL_NOT_VERIFIED;
                case "Email address has not been confirmed.":
                    return eAUTHORIZED_REQUEST_RESPONSE.USER_EMAIL_NOT_VERIFIED;
                case "Email address already exists.":
                    return eAUTHORIZED_REQUEST_RESPONSE.USER_EMAIL_ALREADY_EXISTS;
                case "The organization already exists.":
                    return eAUTHORIZED_REQUEST_RESPONSE.ORGANIZATION_ALREADY_EXISTS;
            }
            return eAUTHORIZED_REQUEST_RESPONSE.DEFAULT_ERROR;
        }

        public RequestResponse PostData(string SERVICE_URL, object myComplexObject)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(SERVICE_URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Set("apiKey", "LocalDevApiKey");
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(myComplexObject);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        string _Result = result.ToString();
                        RequestResponse tfb = new JavaScriptSerializer().Deserialize<RequestResponse>(_Result);
                        return tfb;
                    }
                }
            }
            catch (ArgumentException ex)
            {
                //return string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
                return null;
            }
            catch (WebException ex)
            {
                //return string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
                return null;
            }
            catch (Exception ex)
            {
                //return string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
                return null;
            }
        }


        private class GetUserInfoBindingModel
        {
            public string ProfilePicHash { get; set; }
        }
            
        
        public async Task<AuthorizedResponse<SignInAPIResultPayload>> SignInUserDefault(APIServiceRequest_DefaultSignIn SignInRequest)
        {
            RequestResponse response = new RequestResponse();

            ////return ApiCallHandling.PostLoginRequestCookieContainer(APIConstants.GetAPI(eAPI_REQUESTS.SIGN_IN, 1), SignInRequest.EmailAddress, SignInRequest.Password);
            //response = await ApiCallHandling.PostJsonAsync<RequestResponse>(APIConstants.GetAPI(eAPI_REQUESTS.SIGN_IN, 1), "", SignInRequest);

            //if (response == null)
            //{
            //    return new AuthorizedResponse<SignInAPIResultPayload>()
            //    {
            //        Status = eAUTHORIZED_REQUEST_RESPONSE.SERVER_NOT_AVAILABLE,
            //        Message = "Server not available.",
            //        Result = null,
            //    };
            //}
            //try
            //{
            //    if (response.Success)
            //    {
            //        return new AuthorizedResponse<SignInAPIResultPayload>()
            //        {
            //            Status = eAUTHORIZED_REQUEST_RESPONSE.SUCCESS,
            //            Message = response.Message,
            //            Result = APISerializer.Deserialize<SignInAPIResultPayload>(response.Response)
            //        };
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            return new AuthorizedResponse<SignInAPIResultPayload>()
            {
                Status = ErrorResponseFromMessage(response.Message),
                Message = response.Message,
                Result = null,
            };
        }
        

    }
}
