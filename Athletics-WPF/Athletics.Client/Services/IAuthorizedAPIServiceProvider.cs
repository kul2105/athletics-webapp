﻿using System.Threading.Tasks;

namespace Athletics.Client.Services.API
{
    public enum eAUTHORIZED_REQUEST_RESPONSE
    {
        USER_NOT_LOGGED_IN,
        USER_NOT_FOUND,
        USER_EMAIL_NOT_VERIFIED,
        USER_PASSWORD_RESET_REQUIRED,
        USER_EMAIL_ALREADY_EXISTS,   // Sign-up error
        ORGANIZATION_ALREADY_EXISTS, // Sign-up error
        USER_FORBIDDEN,
        SERVER_NOT_AVAILABLE,
        SERVER_ERROR,
        CLIENT_ERROR,
        DEFAULT_ERROR,
        SUCCESS,
    }

    public class AuthorizedResponse<T> where T : class
    {
        public AuthorizedResponse()
        {
            this.Status = eAUTHORIZED_REQUEST_RESPONSE.DEFAULT_ERROR;
            Message = "";
            Result = null;
        }

        public eAUTHORIZED_REQUEST_RESPONSE Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }

    public interface IAuthorizedAPIServiceProvider
    {
        string AuthToken { get; set; }

        
        // Sign In Using Embeddetech Account
        Task<AuthorizedResponse<SignInAPIResultPayload>> SignInUserDefault(APIServiceRequest_DefaultSignIn SignInRequest);

    }
}
