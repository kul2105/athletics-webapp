﻿using Athletics.Client.Services.API;
using Athletics.Client.ViewModels;
using Athletics.Entities.Models;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Linq;

namespace Athletics.Client.Services.SignInManager
{
    public class SignInManager : BindableBase, ISignInManager
    {
        private readonly ISignedInUserViewModelWriteable SignedInUserViewModel;

        public SignInManager(ISignedInUserViewModelWriteable SignedInUserViewModel)
        {
            this.SignedInUserViewModel = SignedInUserViewModel;
        }
        public async Task<AuthorizedResponse<RegisterNewUserResult>> RegisterNewUserAsync(APIServiceRequest_RegisterNewUser SignupRequest)
        {
            try
            {
                SignedInUserViewModel.ClearCache();
                SignedInUserViewModel.ClearCachedToken();
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    User user = new User
                    {
                        EmailID = SignupRequest.EmailAddress,
                        FirstName = SignupRequest.FirstName,
                        LastName = SignupRequest.LastName,
                        UserGuid = Guid.NewGuid().ToString(),
                        Password = SignupRequest.Password,
                        UserPhoto = SignupRequest.UserImage,
                        DisplayName = SignupRequest.DisplayName,
                        UserName = SignupRequest.UserName
                    };

                    ctx.Users.Add(user);
                    // Insert into the database.
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
            }


            AuthorizedResponse<RegisterNewUserResult> Result = new AuthorizedResponse<RegisterNewUserResult>();
            Result.Message = "";
            Result.Status = eAUTHORIZED_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RegisterNewUserResult();
            Result.Result.Token = null;
            return Result;
        }

        public async Task<AuthorizedResponse<SignInAPIResultPayload>> SignInAsync(string username, string password, bool RememberMe)
        {
            AuthorizedResponse<SignInAPIResultPayload> Result = new AuthorizedResponse<SignInAPIResultPayload>();

            Application.Current.Dispatcher.Invoke(async () =>
                {
                    AthleticsDBEntities1 athenticsEntity = new AthleticsDBEntities1();
                    AuthorizedResponse<GetMyAccountDetailsResult> InfoResult = new AuthorizedResponse<GetMyAccountDetailsResult>();
                    InfoResult.Status = eAUTHORIZED_REQUEST_RESPONSE.USER_NOT_FOUND;
                    User exitedUser = null;
                    foreach (User user in athenticsEntity.Users)
                    {
                        if (user.EmailID == username && user.Password == password)
                        {
                            exitedUser = user;
                            break;
                        }
                    }

                    if (exitedUser != null)
                    {
                        InfoResult.Status = eAUTHORIZED_REQUEST_RESPONSE.SUCCESS;
                        InfoResult.Result = new GetMyAccountDetailsResult();
                        InfoResult.Result.DisplayName = exitedUser.DisplayName;
                        InfoResult.Result.EmailAddress = exitedUser.EmailID;
                        InfoResult.Result.FirstName = exitedUser.FirstName;
                        InfoResult.Result.IsAdmin = exitedUser.IsAdmin == null ? false : exitedUser.IsAdmin.Value;
                        InfoResult.Result.LastName = exitedUser.LastName;
                        InfoResult.Result.UserName = exitedUser.UserName;
                        InfoResult.Result.UserImage = exitedUser.UserPhoto;
                    }
                    if (InfoResult.Status == eAUTHORIZED_REQUEST_RESPONSE.SUCCESS)
                    {
                        SignedInUserViewModel.LoadAccountDetails(InfoResult.Result);
                        SignedInUserViewModel.IsSignedIn = true;
                        Result.Status = eAUTHORIZED_REQUEST_RESPONSE.SUCCESS;
                        SignInAPIResultPayload result = new SignInAPIResultPayload();
                        result.EmailVerified = true;
                        Result.Result = result;
                    }
                    else
                    {
                        SignedInUserViewModel.ClearCachedToken();
                        SignedInUserViewModel.AuthToken = null;
                        Result.Status = InfoResult.Status;
                        //}
                        SignedInUserViewModel.DisplayName = "";
                        SignedInUserViewModel.EmailAddress = "";
                        SignedInUserViewModel.FirstName = "";
                        SignedInUserViewModel.LastName = "";
                        SignedInUserViewModel.IsSignedIn = false;
                    }
                }).Wait();
            return Result;
        }


        public void ClearCachedToken()
        {
            Properties.Settings.Default.AuthenticationToken = null;
            Properties.Settings.Default.Save();
        }

        public async Task SignOut()
        {
            ClearCachedToken();
            SignedInUserViewModel.LoadAccountDetails(null);
            SignedInUserViewModel.IsSignedIn = false;
            await Task.Delay(0);
            return;
        }

        public async Task<AuthorizedResponse<RegisterNewRoleResult>> CreateNewRole(APIServiceRequest_GetMyRoleDetails CreateRoleRequest)
        {
            try
            {
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    Role role = new Role
                    {
                        RoleGuid = CreateRoleRequest.RoleGuid,
                        RoleName = CreateRoleRequest.RoleName,
                        RoleID = CreateRoleRequest.RoleID,
                    };

                    ctx.Roles.Add(role);
                    // Insert into the database.
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
            }


            AuthorizedResponse<RegisterNewRoleResult> Result = new AuthorizedResponse<RegisterNewRoleResult>();
            Result.Message = "";
            Result.Status = eAUTHORIZED_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RegisterNewRoleResult();
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<RegisterNewRoleResult>> AssignRoleToUser(APIServiceRequest_AssignUserInRole UserInRole)
        {
            try
            {
                SignedInUserViewModel.ClearCache();
                SignedInUserViewModel.ClearCachedToken();
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    UserInRole userInRole = new UserInRole
                    {
                        RoleGuid = UserInRole.RoleID,
                        UserGuid = UserInRole.UserID,
                        UserInRoleID = Guid.NewGuid().ToString()
                    };

                    ctx.UserInRoles.Add(userInRole);
                    // Insert into the database.
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
            }

            AuthorizedResponse<RegisterNewRoleResult> Result = new AuthorizedResponse<RegisterNewRoleResult>();
            Result.Message = "";
            Result.Status = eAUTHORIZED_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RegisterNewRoleResult();
            Result.Result.AuthToken = null;
            return Result;
        }


        public async Task<List<User>> GetAllUser()
        {
            List<User> userList = new List<User>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (User user in ctx.Users)
                {
                    userList.Add(user);
                }
            }
            return userList;
        }

        public async Task<List<Role>> GetAllRole()
        {
            List<Role> roleList = new List<Role>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (Role role in ctx.Roles)
                {
                    roleList.Add(role);
                }
            }
            return roleList;
        }

        public async Task<List<Role>> GetUserInRole(string UserGuid)
        {
            List<Role> roleList = new List<Role>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (UserInRole roleInGuid in ctx.UserInRoles.Where(p => p.UserGuid == UserGuid))
                {
                    Role role = ctx.Roles.Where(p => p.RoleGuid == roleInGuid.RoleGuid).FirstOrDefault();
                    if (role != null)
                    {
                        roleList.Add(role);
                    }
                }
            }
            return roleList;
        }

        public async Task<AuthorizedResponse<RegisterNewApplicationMenuResult>> CreateNewMenu(APIServiceRequest_GetApplicationMenuDetails CreateMenuRequest)
        {
            try
            {
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    ApplicationMenu menu = new ApplicationMenu
                    {
                        MenuID = CreateMenuRequest.ApplicationMenuGuid,
                        MenuName = CreateMenuRequest.ApplicationMenuName,
                        ParentID = CreateMenuRequest.ApplicationMenuID,
                    };

                    ctx.ApplicationMenus.Add(menu);
                    // Insert into the database.
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
            }


            AuthorizedResponse<RegisterNewApplicationMenuResult> Result = new AuthorizedResponse<RegisterNewApplicationMenuResult>();
            Result.Message = "";
            Result.Status = eAUTHORIZED_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RegisterNewApplicationMenuResult();
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<AssignMenuInRoleResult>> AssignMenuToRole(List<APIServiceRequest_AssignMenuInRole> CreateMenuRequestList)
        {
            try
            {
                using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
                {
                    foreach (APIServiceRequest_AssignMenuInRole CreateMenuRequest in CreateMenuRequestList)
                    {
                        MenuInRole menu = new MenuInRole
                        {
                            MenuInRoleID = ctx.MenuInRoles.Count() + 1,
                            MenuID = CreateMenuRequest.MenuID,
                            RoleID = CreateMenuRequest.RoleID,
                        };
                        ctx.MenuInRoles.Add(menu);
                    }
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
            }


            AuthorizedResponse<AssignMenuInRoleResult> Result = new AuthorizedResponse<AssignMenuInRoleResult>();
            Result.Message = "";
            Result.Status = eAUTHORIZED_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new AssignMenuInRoleResult();
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<List<ApplicationMenu>> GetAllApplicationMenu()
        {
            List<ApplicationMenu> ApplicationMenuList = new List<ApplicationMenu>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                if (ctx.ApplicationMenus == null) return ApplicationMenuList;

                foreach (ApplicationMenu applicationMenu in ctx.ApplicationMenus.OrderBy(p => p.MenuID))
                {
                    ApplicationMenuList.Add(applicationMenu);
                }
            }
            return ApplicationMenuList;
        }

        public async Task<List<ApplicationMenu>> GetAssignedApplicationMenu(string RoleGuid)
        {
            List<ApplicationMenu> ApplicationMenuList = new List<ApplicationMenu>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (MenuInRole roleInGuid in ctx.MenuInRoles.Where(p => p.RoleID == RoleGuid))
                {
                    ApplicationMenu role = ctx.ApplicationMenus.Where(p => p.MenuID == roleInGuid.MenuID).FirstOrDefault();
                    if (role != null)
                    {
                        ApplicationMenuList.Add(role);
                    }
                }
            }
            return ApplicationMenuList;
        }

        public async Task<bool> RemoveUserFromRole(string UserGuid)
        {
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                List<UserInRole> userInRole = ctx.UserInRoles.Where(p => p.UserGuid == UserGuid).ToList();
                if (userInRole != null)
                {
                    if (userInRole.Count > 0)
                    {
                        ctx.UserInRoles.RemoveRange(userInRole);
                    }
                }
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
