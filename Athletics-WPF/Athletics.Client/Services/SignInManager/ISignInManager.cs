﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Athletics.Client.Services.API;
using Athletics.Entities.Models;

namespace Athletics.Client.Services.SignInManager
{
    /// <summary>
    /// Anything having to do with signing in, signing out, or registering should be 
    /// inside this service
    /// </summary>
    public interface ISignInManager
    {
        Task<AuthorizedResponse<RegisterNewRoleResult>> CreateNewRole(APIServiceRequest_GetMyRoleDetails CreateRoleRequest);
        Task<AuthorizedResponse<RegisterNewUserResult>> RegisterNewUserAsync(APIServiceRequest_RegisterNewUser SignupRequest);
        Task<AuthorizedResponse<SignInAPIResultPayload>> SignInAsync(string username, string password, bool RememberMe);
        Task<AuthorizedResponse<RegisterNewRoleResult>> AssignRoleToUser(APIServiceRequest_AssignUserInRole UserInRole);
        Task<AuthorizedResponse<RegisterNewApplicationMenuResult>> CreateNewMenu(APIServiceRequest_GetApplicationMenuDetails CreateMenuRequest);
        Task<AuthorizedResponse<AssignMenuInRoleResult>> AssignMenuToRole(List<APIServiceRequest_AssignMenuInRole> CreateMenuRequestList);

        Task<List<User>> GetAllUser();
        Task<List<Role>> GetAllRole();
        Task<List<Role>> GetUserInRole(string UserGuid);
        Task<List<ApplicationMenu>> GetAllApplicationMenu();
        Task<List<ApplicationMenu>> GetAssignedApplicationMenu(string RoleGuid);
        Task<bool> RemoveUserFromRole(string UserGuid);

        Task SignOut();
    }
}
