﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Services.API
{
    public class APIServiceRequest_DefaultSignIn
    {
        public string grant_type { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        //public string EmailAddress { get; set; }
        //public string Password { get; set; }
    }
}
