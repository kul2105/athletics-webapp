﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Services.API
{
    public class SignInAPIResultPayload
    {
        public string Token { get; set; }
        public bool EmailVerified { get; set; }
    }
}
