﻿using System;
using System.Collections.Generic;

namespace Athletics.Client.Services.API
{
    public class AssignMenuInRoleResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
    }

}
