﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.Client.Services.API
{
    public class APIServiceRequest_AssignMenuInRole
    {
        public int MenuID { get; set; }
        public string RoleID { get; set; }

    }
}
