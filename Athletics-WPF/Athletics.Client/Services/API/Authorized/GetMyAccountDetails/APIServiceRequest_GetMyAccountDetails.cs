﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.Client.Services.API
{
    public class APIServiceRequest_GetMyAccountDetails
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
    }
}
