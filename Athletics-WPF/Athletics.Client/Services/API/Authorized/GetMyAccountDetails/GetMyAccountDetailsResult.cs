﻿using System;
using System.Collections.Generic;

namespace Athletics.Client.Services.API
{
    public class GetMyAccountDetailsResult
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public DateTime UserCreationDate { get; set; }
        public byte[] UserImage { get; set; }
        public List<int> OrganizationMemberships { get; set; }
        public string OrganizationName { get; set; }
        public bool IsAdmin { get; set; }
        //public bool IsAuthorizedToInviteUsers { get; set; }
        //public bool IsAuthorizedToPurchase { get; set; }
        //public bool IsOrganizationMember { get; set; }
        //public string OrganizationDescription { get; set; }
        //public bool OrganizationIsForEducationalUse { get; set; }
        //public bool OrganizationIsIndividual { get; set; }
    }
}
