﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.Client.Services.API
{
    public class RegisterNewUserResult
    {
        public string Token { get; set; }
    }
}
