﻿namespace Athletics.Client.Services.API
{
    public class APIServiceRequest_RegisterNewUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string CountryCode { get; set; }
        public byte[] UserImage { get; set; }

    }
}
