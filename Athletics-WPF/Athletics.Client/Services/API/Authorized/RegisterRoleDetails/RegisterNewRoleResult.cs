﻿using System;
using System.Collections.Generic;

namespace Athletics.Client.Services.API
{
    public class RegisterNewRoleResult
    {
        public string AuthToken { get; set; }
        public byte[] ProfilePicHash { get; set; }
    }

}
