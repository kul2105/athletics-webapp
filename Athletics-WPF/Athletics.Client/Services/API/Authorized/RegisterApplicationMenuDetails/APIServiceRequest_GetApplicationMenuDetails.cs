﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Athletics.Client.Services.API
{
    public class APIServiceRequest_GetApplicationMenuDetails
    {
        public int ApplicationMenuGuid { get; set; }
        public int ApplicationMenuID { get; set; }
        public string ApplicationMenuName { get; set; }

    }
}
