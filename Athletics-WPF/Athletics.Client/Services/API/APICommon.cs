﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Athletics.Client.Services.API
{
    public class RequestResponse
    {
        public int Substatuscode { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Response { get; set; }
    }

    public enum eAPI_REQUESTS
    {
        CHANGE_PASSWORD,
        CHECK_IF_ORGANIZATIN_EXISTS,
        CHECK_IF_USER_EXISTS,
        CREATE_NEW_ORGANIZATION,
        GET_FACEBOOK_USER_LOGIN_INFO,
        GET_MY_ACCOUNT_DETAILS,
        GET_ORGANIZATION_INVITATIONS,
        GET_ORGANIZATION_USERS_BASIC_INFO,
        GET_USER_ORG_INVITATIONS,
        INVITE_USER_TO_JOIN_ORGANIZATION,
        JOIN_ORGANIZATION,
        REMOVE_USER_FROM_ORGANIZATION,
        RESEND_VERIFICATION_EMAIL,
        RESEND_VERIFICATION_EMAIL_EXTERNAL_PROVIDER,
        //RESET_PASSWORD,
        SEND_RESET_PASSWORD_LINK,
        SET_USER_AUTHORIZATIONS,
        SIGN_IN,
        SIGN_IN_WITH_EXTERNAL_PROVIDER,
        SIGN_UP,
        SIGN_UP_WITH_EXTERNAL_PROVIDER,
    };

    
   
}
