﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Athletics.Client.Helpers
{
    public class ImageHelpers
    {
        public static BitmapImage BitmapImageFromByteArray(byte[] data)
        {
            try
            {
                if(data == null || data.Length == 0)
                {
                    return null;
                }
                BitmapImage image = new BitmapImage();
                using (var ms = new MemoryStream(data))
                {
                    ms.Position = 0;
                    image.BeginInit();
                    image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.UriSource = null;
                    image.StreamSource = ms;
                    image.EndInit();
                    image.Freeze();
                }
                return image;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
