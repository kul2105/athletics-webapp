﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Helpers
{
    public class FacebookHelper
    {
        public static async Task SignOut(string AccessToken)
        {
            //await Task.Delay(100);
            // Log out
            string destURL = @"https://www.facebook.com/logout.php?next=https%3A%2F%2Fwww.facebook.com&access_token=" + AccessToken;

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(destURL);
            req.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705;)";
            req.Method = "GET";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            req.Headers.Add("Accept-Language: en-us,en;q=0.5");
            req.Headers.Add("Accept-Encoding: gzip,deflate");
            req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
            //req.KeepAlive = true;
            //req.Headers.Add("Keep-Alive: 300");
            req.KeepAlive = false;
            req.Referer = "api";
            try
            {
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
            }
            catch (Exception ex)
            {
                Athletics.Logger.LoggerManager.LogError(ex.Message);
            }
        }
    }
}
