﻿using Athletics.Client.Models;
using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Controllers
{
    public interface IApplicationUserController
    {
        ApplicationUserObservableCollection UserList { get; }
        Task<List<Role>> GetAllRole();
        Task<List<Role>> GetUserInRole(string UserGuid);
    }
}
