﻿using Athletics.Client.Models;
using Athletics.Entities.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Controllers
{
    public class ApplicationUserController : IApplicationUserController
    {
        private ApplicationUserObservableCollection _UserList = new ApplicationUserObservableCollection();
        public ApplicationUserObservableCollection UserList
        {
            get
            {
                return _UserList;
            }
        }
        public async Task<List<Role>> GetAllRole()
        {
            List<Role> roleList = new List<Role>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (Role role in ctx.Roles)
                {
                    roleList.Add(role);
                }
            }
            return roleList;
        }

        public async Task<List<Role>> GetUserInRole(string UserGuid)
        {
            List<Role> roleList = new List<Role>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (UserInRole roleInGuid in ctx.UserInRoles.Where(p => p.UserGuid == UserGuid))
                {
                    Role role = ctx.Roles.Where(p => p.RoleGuid == roleInGuid.RoleGuid).FirstOrDefault();
                    if (role != null)
                    {
                        roleList.Add(role);
                    }
                }
            }
            return roleList;
        }
    }
}
