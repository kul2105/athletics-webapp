﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Controllers
{
    public enum e_AUTH_STATE
    {
        SIGN_IN_DEFAULT,
        SIGN_UP,
        FINISH_EXTERNAL_SIGN_UP,
        JOIN_EXISTING_ORGANIZATION,
        REGISTRATION_COMPLETE,
        CREATING_USER,          // Transition State
        SIGNING_UP,             // Transition State
        WELCOME_NEW_USER,
        UPLOADING_NEW_USER_INFO,// Transition State
        LOGGING_IN,             // Transition State
        LOG_IN_FAILED,          // Notification State
        LOGGED_IN,
        NOT_LOGGED_IN,          // "Log In Later" (Using As Guest)
        CREATE_ROLE,
        CREATE_ROLE_COMPLETE,
        ASSIGN_ROLE,
        ASSIGN_ROLE_COMPLETE,
        CREATE_MENU,
        CREATE_MENU_COMPLETE,
        ASSIGN_MENU,
        ASSIGN_MENU_COMPLETE,
        NONE,
    }

    public enum e_AUTH_PROVIDER
    {
        PASSWORD,
        FACEBOOK,
        GOOGLE,
    }

    public interface IApplicationAuthStateController
    {
        // For use by the view models
        e_AUTH_STATE AuthenticationState { get; set; }

        // For use by the views
        string sAuthenticationState { get; }

        string CurrentUserName { get; set; }

        event Action<e_AUTH_STATE> AuthorizationStateChanged;
    }
}
