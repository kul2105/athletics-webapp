﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.Controllers
{
    public class ApplicationAuthStateController : BindableBase, IApplicationAuthStateController
    {
        public ApplicationAuthStateController()
        {
        }

        private e_AUTH_STATE _AuthenticationState = e_AUTH_STATE.SIGN_IN_DEFAULT;
        public e_AUTH_STATE AuthenticationState
        {
            get
            {
                return _AuthenticationState;
            }
            set
            {
                if (_AuthenticationState != value)
                {
                    _AuthenticationState = value;
                    OnPropertyChanged("AuthenticationState");
                    switch (value)
                    {
                        case e_AUTH_STATE.SIGN_IN_DEFAULT:
                        default:
                            _sAuthenticationState = "SignInDefault";
                            break;
                        case e_AUTH_STATE.SIGN_UP:
                            _sAuthenticationState = "SignUp";
                            break;
                        case e_AUTH_STATE.REGISTRATION_COMPLETE:
                            _sAuthenticationState = "VerifyingEmail";
                            break;
                        case e_AUTH_STATE.WELCOME_NEW_USER:
                            _sAuthenticationState = "WelcomeNewUser";
                            break;
                        case e_AUTH_STATE.LOGGED_IN:
                            _sAuthenticationState = "LoggedIn";
                            break;
                        case e_AUTH_STATE.LOGGING_IN:
                            _sAuthenticationState = "LoggingIn";
                            break;
                        case e_AUTH_STATE.CREATE_ROLE:
                            _sAuthenticationState = "CreateRole";
                            break;
                        case e_AUTH_STATE.ASSIGN_ROLE:
                            _sAuthenticationState = "AssignRole";
                            break;
                        case e_AUTH_STATE.CREATE_MENU:
                            _sAuthenticationState = "CreateMenu";
                            break;
                        case e_AUTH_STATE.ASSIGN_MENU:
                            _sAuthenticationState = "AssignMenu";
                            break;
                    }
                    OnPropertyChanged("sAuthenticationState");
                    Action<e_AUTH_STATE> pAuthorizationStateChanged = AuthorizationStateChanged;
                    if (pAuthorizationStateChanged != null)
                    {
                        pAuthorizationStateChanged(value);
                    }
                }
            }
        }


        private string _sAuthenticationState;
        public string sAuthenticationState
        {
            get { return _sAuthenticationState; }
        }

        private string _CurrentUserName;
        public string CurrentUserName { get => _CurrentUserName; set => _CurrentUserName=value; }

        public event Action<e_AUTH_STATE> AuthorizationStateChanged;
    }
}
