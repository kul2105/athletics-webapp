﻿namespace Athletics.Client.Views
{
    public interface ISignUpView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
