﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Athletics.Client.ViewModels;

namespace Athletics.Client.Views
{
    /// <summary>
    /// Interaction logic for SignUpView.xaml
    /// </summary>
    public partial class SignUpView : UserControl, ISignUpView
    {
        public SignUpView()
        {
            InitializeComponent();

        }

        public void ResetFocus()
        {
            FirstNameTextBox.Focus();
            FirstNameTextBox.Focusable = true;
            Keyboard.Focus(FirstNameTextBox);
        }

        private void PasswordTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            PasswordTextBox.Text = PasswordTextBox.Text;
        }

        private void ConfirmPasswordTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ConfirmPasswordTextBox.Text = ConfirmPasswordTextBox.Text;
        }

        private void DisplayNameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ((ISignUpViewModel)DataContext).DisplayNameLostFocus();
        }

        private void Button_LostFocus(object sender, RoutedEventArgs e)
        {
            ResetFocus();
        }

        private void Button_LostFocus_1(object sender, RoutedEventArgs e)
        {
            ResetFocus();
        }
    }
}
