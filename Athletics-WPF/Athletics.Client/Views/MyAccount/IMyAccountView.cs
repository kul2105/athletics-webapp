﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Views
{
    public interface IMyAccountView
    {
        object DataContext { get; set; }
    }
}
