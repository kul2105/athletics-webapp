﻿using System.Windows;
using System.Windows.Controls;

namespace Athletics.Client.Views
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class UserRoleView : UserControl, IUserRoleView
    {
        public UserRoleView()
        {
            InitializeComponent();
            Loaded += (sender, e) => { RoleIDTextBox.Focus(); };
        }

        public void ResetFocus()
        {
            RoleIDTextBox.Focus();
        }

    }
}
