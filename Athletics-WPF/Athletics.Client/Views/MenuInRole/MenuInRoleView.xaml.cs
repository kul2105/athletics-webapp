﻿using Athletics.Client.ViewModels;
using Microsoft.Practices.Prism.Commands;
using System.Windows;
using System.Windows.Controls;

namespace Athletics.Client.Views
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class MenuInRoleView : UserControl, IMenuInRoleView
    {
        public MenuInRoleView()
        {
            InitializeComponent();
            Loaded += (sender, e) => { userList.Focus(); AllMenus = this.AllMenueList; };
        }
        private ListBox _AllMenu;
        public ListBox AllMenus { get => _AllMenu; set => _AllMenu = value; }

        public void ResetFocus()
        {
            userList.Focus();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            MenuInRoleViewModel vm = (MenuInRoleViewModel)this.DataContext;
            if (vm != null)
            {
                if (((CheckBox)sender).Tag != null)
                {
                    int menueID = System.Convert.ToInt32(((CheckBox)sender).Tag.ToString());
                    vm.SelectedMenues.Add(menueID);
                }
            }
            ((DelegateCommand)vm.AssignModuleCommand).RaiseCanExecuteChanged();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            MenuInRoleViewModel vm = (MenuInRoleViewModel)this.DataContext;
            if (vm != null)
            {
                if (((CheckBox)sender).Tag != null)
                {
                    int menueID = System.Convert.ToInt32(((CheckBox)sender).Tag.ToString());

                    if (vm.SelectedMenues.Contains(menueID))
                    {
                        vm.SelectedMenues.Remove(menueID);
                    }
                }
            }
            ((DelegateCommand)vm.AssignModuleCommand).RaiseCanExecuteChanged();
        }
    }
}
