﻿using System.Windows.Controls;

namespace Athletics.Client.Views
{
    public interface IMenuInRoleView
    {
        object DataContext { get; set; }
        void ResetFocus();
        ListBox AllMenus { get; set; }
    }

}
