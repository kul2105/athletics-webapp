﻿namespace Athletics.Client.Views
{
    public interface ISignInDefaultView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
