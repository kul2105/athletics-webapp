﻿using System.Windows;
using System.Windows.Controls;

namespace Athletics.Client.Views
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class SignInDefaultView : UserControl, ISignInDefaultView
    {
        public SignInDefaultView()
        {
            InitializeComponent();
            Loaded += (sender, e) => { EmailTextBox.Focus(); };
        }

        private void WatermarkPasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            PasswordTextBox.Text = PasswordTextBox.Text;
        }

        public void ResetFocus()
        {
            EmailTextBox.Focus();
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {

        }
    }
}
