﻿using System.Windows;
using System.Windows.Controls;

namespace Athletics.Client.Views
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class ApplicationMenuView : UserControl, IApplicationMenuView
    {
        public ApplicationMenuView()
        {
            InitializeComponent();
            Loaded += (sender, e) => { MenuNameTextBox.Focus(); };
        }

        public void ResetFocus()
        {
            MenuNameTextBox.Focus();
        }

    }
}
