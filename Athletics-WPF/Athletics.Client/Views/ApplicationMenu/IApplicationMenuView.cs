﻿namespace Athletics.Client.Views
{
    public interface IApplicationMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
