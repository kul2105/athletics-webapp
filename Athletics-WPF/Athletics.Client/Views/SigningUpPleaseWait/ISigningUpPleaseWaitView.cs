﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Client.Views
{
    public interface ISigningUpPleaseWaitView
    {
        object DataContext { get; set; }
    }

}
