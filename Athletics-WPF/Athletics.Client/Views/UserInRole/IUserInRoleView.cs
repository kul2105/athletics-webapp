﻿using Athletics.Entities.Models;
using System.Collections.ObjectModel;

namespace Athletics.Client.Views
{
    public interface IUserInRoleView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
