﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using Athletics.Entities.Models;

namespace Athletics.Client.Views
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class UserInRoleView : UserControl, IUserInRoleView
    {
        public UserInRoleView()
        {
            InitializeComponent();
            Loaded += (sender, e) => { RoleCombo.Focus(); };
        }
     

        public void ResetFocus()
        {
            RoleCombo.Focus();
        }

    }
}
