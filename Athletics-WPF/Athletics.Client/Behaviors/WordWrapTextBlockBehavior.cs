﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Interactivity;
using System.Globalization;
using System.Drawing;
//using Microsoft.Expression.Interactivity.Core;

namespace Athletics.Client.Behaviors
{
	public class WordWrapTextBlockBehavior : Behavior<TextBlock>
	{
		public WordWrapTextBlockBehavior()
		{
		}

        public string SourceText
        {
            get { return (string)GetValue(SourceTextProperty); }
            set { SetValue(SourceTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SourceText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceTextProperty =
            DependencyProperty.Register("SourceText", typeof(string), typeof(WordWrapTextBlockBehavior), new PropertyMetadata(new PropertyChangedCallback(OnSourceTextChanged)));

        private static void OnSourceTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            WordWrapTextBlockBehavior o = (WordWrapTextBlockBehavior)d;
            o.UpdateDisplayedText((string)e.NewValue);
        }



        public static double StringWidth(System.Windows.Media.FontFamily font, System.Windows.FontStyle style, FontWeight weight, string text)
        {
               var formattedText = new FormattedText(text, CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                    new Typeface(font, style, weight, new FontStretch()),
                    16, System.Windows.Media.Brushes.Black);//, new NumberSubstitution(), 1);

                return formattedText.WidthIncludingTrailingWhitespace * 0.9;
        }

        public static string WrapText(System.Windows.Media.FontFamily font, System.Windows.FontStyle style, FontWeight weight, string text, double lineWidth)
        {
            const string space = " ";

            if(text == null)
            {
                return null;
            }
            string[] words = text.Split(new string[] { space }, StringSplitOptions.None);
            double spaceWidth = StringWidth(font, style, weight, space);
            double spaceLeft = lineWidth;
            double wordWidth;
            StringBuilder result = new StringBuilder();

            foreach (string word in words)
            {
                wordWidth = StringWidth(font, style, weight, word);
                if (wordWidth + spaceWidth > spaceLeft)
                {
                    result.AppendLine();
                    spaceLeft = lineWidth - wordWidth;
                }
                else
                {
                    spaceLeft -= (wordWidth + spaceWidth);
                }
                result.Append(word + space);
            }
            return result.ToString();
        }

        private void UpdateDisplayedText(string NewText)
        {
            if (AssociatedObject == null)
            {
                return;
            }
            if (NewText == null)
            {
                NewText = "";
            }
            AssociatedObject.Text = WrapText(FontFamily, FontStyle, FontWeight, NewText, DisplaySize);
        }

        

        public System.Windows.Media.FontFamily FontFamily
        {
            get { return (System.Windows.Media.FontFamily)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for fontFamily.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontFamilyProperty = 
            DependencyProperty.Register("FontFamily", typeof(System.Windows.Media.FontFamily), typeof(WordWrapTextBlockBehavior), new PropertyMetadata(new System.Windows.Media.FontFamily("Tahoma")));

        
        public System.Windows.FontStyle FontStyle
        {
            get { return (System.Windows.FontStyle)GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FontStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontStyleProperty =
            DependencyProperty.Register("FontStyle", typeof(System.Windows.FontStyle), typeof(WordWrapTextBlockBehavior), new PropertyMetadata(System.Windows.FontStyles.Normal));


        

        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FontWeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontWeightProperty =
            DependencyProperty.Register("FontWeight", typeof(FontWeight), typeof(WordWrapTextBlockBehavior), new PropertyMetadata(System.Windows.FontWeights.Normal));




        public double DisplaySize
        {
            get { return (double)GetValue(DisplaySizeProperty); }
            set { SetValue(DisplaySizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisplaySize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplaySizeProperty =
            DependencyProperty.Register("DisplaySize", typeof(double), typeof(WordWrapTextBlockBehavior), new PropertyMetadata(400.0, OnDisplaySizeChanged));


        private static void OnDisplaySizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            WordWrapTextBlockBehavior o = (WordWrapTextBlockBehavior)d;
            o.UpdateDisplayedText(o.SourceText);
        }


	}
}