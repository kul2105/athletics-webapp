﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Interactivity;
using System.Globalization;
using System.Drawing;
using Athletics.Client.ValidationRules;
//using Microsoft.Expression.Interactivity.Core;

namespace Athletics.Client.Behaviors
{
	public class ConfirmPasswordBehavior : Behavior<TextBox>
	{
        public ConfirmPasswordBehavior()
		{
		}

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.TextChanged += AssociatedObject_TextChanged;
        }

        void AssociatedObject_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Validation != null)
            {
                if (Password == AssociatedObject.Text)
                {
                    Validation.Result = new ValidationResult(true, null);
                }
                else
                {
                    Validation.Result = new ValidationResult(false, "Password does not match.");
                }
            }
            AssociatedObject.GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }



        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Password.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(ConfirmPasswordBehavior), new PropertyMetadata(null));



        public BehaviorDrivenValidation Validation
        {
            get { return (BehaviorDrivenValidation)GetValue(ValidationProperty); }
            set { SetValue(ValidationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Validation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValidationProperty =
            DependencyProperty.Register("Validation", typeof(BehaviorDrivenValidation), typeof(ConfirmPasswordBehavior), new PropertyMetadata(null));

	}
}