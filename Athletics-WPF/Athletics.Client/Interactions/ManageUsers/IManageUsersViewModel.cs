﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.Interactions
{
    public interface IManageUsersViewModel
    {
        IManageUsersView View { get; set; }
        Task OnInteractionStarted();
    }
}
