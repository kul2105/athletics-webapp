﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Athletics.Client.Services.API;
using Athletics.Client.ViewModels;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.Interactions
{
    public class ManageUsersViewModel : BindableBase, IInteractionRequestAware, IManageUsersViewModel
    {
        private readonly ISignedInUserViewModel signedInUserViewModel;
        public ManageUsersViewModel(ISignedInUserViewModel signedInUserViewModel)
        {
            this.signedInUserViewModel = signedInUserViewModel;
            this.SelectItemCommand = new DelegateCommand(this.AcceptSelectedItem);
            this.CancelCommand = new DelegateCommand(this.CancelInteraction);
            this.InviteUserCommand = new DelegateCommand(InviteUser, () => Email != null && Email.Length >= 4 && _EmailEntryValid);

            signedInUserViewModel.OrgUsersReloaded += () =>
                {
                  //  SelectedUser = signedInUserViewModel.OrganizationUsers.FirstOrDefault();
                    if (View != null)
                    {
                        View.ResetFocus();
                    }
                };

            _DoneCommand = new DelegateCommand(() =>
            {
                if (this.notification != null)
                {
                    this.notification.Confirmed = true;
                    this.notification.TestString = "Goodbye, World!";
                }
                if (this.FinishInteraction != null)
                {
                    this.FinishInteraction();
                }
            });
        }

        public ISignedInUserViewModel SignedInUserViewModel
        {
            get { return signedInUserViewModel; }
        }

        public IManageUsersView View { get; set; }

        private Action _FinishInteraction;
        public Action FinishInteraction 
        {
            get { return _FinishInteraction; }
            set
            {
                _FinishInteraction = value; 
            }
        }

        private ManageUsersArgs notification;
        public INotification Notification
        {
            get
            {
                return this.notification;
            }
            set
            {
                if (value is ManageUsersArgs)
                {
                    this.notification = value as ManageUsersArgs;
                    this.OnPropertyChanged(() => this.Notification);
                    //SelectedUser = signedInUserViewModel.OrganizationUsers.FirstOrDefault();
                    if(View != null)
                    {
                        View.ResetFocus();
                    }
                }
            }
        }

        public async Task OnInteractionStarted()
        {
            if (View != null)
            {
                View.ResetFocus();
            }
        }

       
        private bool _EmailEntryValid = false;
        private string _Email;
        public string Email
        {
            get { return _Email; }
            set 
            { 
                SetProperty(ref _Email, value);
                try
                {
                    MailAddress m = new MailAddress(_Email);
                    _EmailEntryValid = true;
                }
                catch (Exception)
                {
                    _EmailEntryValid = false;
                }
                ((DelegateCommand)InviteUserCommand).RaiseCanExecuteChanged();
            }
        }

        private string _InvitationErrorString;
        public string InvitationErrorString
        {
            get { return _InvitationErrorString; }
            set { SetProperty(ref _InvitationErrorString, value); }
        }

        private bool _IsAdmin = false;
        public bool IsAdmin
        {
            get { return _IsAdmin; }
            set { SetProperty(ref _IsAdmin, value); }
        }

        private bool _CanInviteMembers = false;
        public bool CanInviteMembers
        {
            get { return _CanInviteMembers; }
            set { SetProperty(ref _CanInviteMembers, value); }
        }
        
        
        
        
        public ICommand SelectItemCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }

        private ICommand _DoneCommand;
        public ICommand DoneCommand
        {
            get { return _DoneCommand; }
        }

        public ICommand InviteUserCommand { get; private set; }

        public void AcceptSelectedItem()
        {
            if (this.notification != null)
            {
                //this.notification.SelectedItem = this.SelectedItem;
                this.notification.Confirmed = true;
            }
            this.FinishInteraction();
        }

        public void CancelInteraction()
        {
            if (this.notification != null)
            {
                //this.notification.SelectedItem = null;
                this.notification.Confirmed = false;
            }
            if (this.FinishInteraction != null)
            {
                this.FinishInteraction();
            }
        }

        public async void InviteUser()
        {
           
        }
    }
}
