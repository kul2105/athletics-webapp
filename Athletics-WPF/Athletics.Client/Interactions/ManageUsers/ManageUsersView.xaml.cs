﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Athletics.Client.Interactions
{
    public interface IManageUsersView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

    /// <summary>
    /// Interaction logic for InviteUserToJoinOrganizationView.xaml
    /// </summary>
    public partial class ManageUsersView : UserControl, IManageUsersView
    {
        IManageUsersViewModel _ViewModel;

        public ManageUsersView(IManageUsersViewModel ViewModel)
        {
            _ViewModel = ViewModel;
            ViewModel.View = this;
            InitializeComponent();
            try
            {
                DataContextChanged += _DataContextChanged;
                Loaded += ManageUsersView_Loaded;
            }
            catch (Exception ex)
            {
            }
            //DataContextChanged += _DataContextChanged;
            //ViewModel.View = this;
            //Loaded += ManageUsersView_Loaded;
        }

        async void ManageUsersView_Loaded(object sender, RoutedEventArgs e)
        {
            await _ViewModel.OnInteractionStarted();
        }

        void _DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is ManageUsersArgs)
            {
                ManageUsersViewModel vm = ((ManageUsersViewModel)_ViewModel);
                if (vm.FinishInteraction == null)
                {
                    vm.FinishInteraction = new Action(() =>
                    {
                        DependencyObject parent = this.Parent;
                        while((parent as Window) == null)
                        {
                            parent = VisualTreeHelper.GetParent(parent);
                        }
                        ((Window)parent).Close();
                    });
                }
                vm.Notification = (ManageUsersArgs)e.NewValue;
                DataContext = _ViewModel;
            }   
        }

        public void ResetFocus()
        {
            Task.Run(async () =>
            {
                await Task.Delay(50);
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    EmailAddressTextBox.Focus();
                }));
            });
        }

    }
}
