﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;

namespace Athletics.Client.Interactions
{
    public class ManageUsersInteractionViewModel
    {
        public ManageUsersInteractionViewModel()
        {
            _RaiseManageUsersInteractionCommand = new DelegateCommand(() =>
            {
                RaiseManageUsersInteraction();
            });
        }

        private InteractionRequest<ManageUsersArgs> _ManageUsersRequest = new InteractionRequest<ManageUsersArgs>();
        public InteractionRequest<ManageUsersArgs> ManageUsersRequest
        {
            get { return _ManageUsersRequest; }
        }

        ManageUsersArgs ManageUsersArgs = new ManageUsersArgs();
        private void RaiseManageUsersInteraction()
        {
            //ManageUsersArgs.QueryImportResult = null;
            ManageUsersArgs.Title = "Manage Users";

            _ManageUsersRequest.Raise(ManageUsersArgs, (x) =>
            {
                if(x.Confirmed)
                {
                    // Do something
                }
                //((DelegateCommand)_SaveDataSetCommand).RaiseCanExecuteChanged();
            });
        }

        private ICommand _RaiseManageUsersInteractionCommand;
        public ICommand RaiseManageUsersInteractionCommand
        {
            get { return _RaiseManageUsersInteractionCommand; }
        }
    }
}
