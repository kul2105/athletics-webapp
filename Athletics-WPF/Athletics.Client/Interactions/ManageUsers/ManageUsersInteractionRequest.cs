﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;

namespace Athletics.Client.Interactions.InviteUserToJoinOrganization
{
    public class ManageUsersInteractionRequest : InteractionRequest<ManageUsersArgs>
    {
    }
}
