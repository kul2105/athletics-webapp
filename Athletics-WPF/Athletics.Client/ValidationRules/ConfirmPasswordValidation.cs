﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;

namespace Athletics.Client.ValidationRules
{
    public class PasswordComparisonValue : DependencyObject
    {
        public string Value
        {
            get { return (string)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "ValueProperty",
            typeof(string),
            typeof(PasswordComparisonValue),
            new PropertyMetadata(""));
    }

    public class ConfirmPasswordValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var str = value as string;

            if (String.IsNullOrEmpty(str))
            {
                return new ValidationResult(false, "Password confirmation required.");
            }

            if (str != ComparisonValue.Value)
            {
                return new ValidationResult(false, "Password does not match.");
            }
            return new ValidationResult(true, null);
        }

        public PasswordComparisonValue ComparisonValue { get; set; }

    }
}
