﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace Athletics.Client.ValidationRules
{
    public class IsCheckedValidation : ValidationRule
    {
        private string _ErrorString = "Terms of service acceptance required.";
        public string ErrorString
        {
            get { return _ErrorString; }
            set { _ErrorString = value; }
        }

        private bool ValidationHasExecuted = false;

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!ValidationHasExecuted)
            {
                // Ignore the first one
                ValidationHasExecuted = true;
                return new ValidationResult(true, null);
            }
            try
            {
                var ischecked = (bool)value;
                if (ischecked)
                {
                    return new ValidationResult(true, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LoggerManager.LogError(ex.Message);
            }
            return new ValidationResult(false, ErrorString);
        }
    }
}
