﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace Athletics.Client.ValidationRules
{
    public class PasswordValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var str = value as string;

            if (String.IsNullOrEmpty(str))
            {
                return new ValidationResult(false, "Password required.");
            }

            if (str.Length < 6)
            {
                return new ValidationResult(false, "Length must be >= 6");
            }
            return new ValidationResult(true, null);
        }
    }
}
