﻿using System.Globalization;
using System.Windows.Controls;

namespace Athletics.Client.ValidationRules
{
    public class BehaviorDrivenValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (Result == null)
            {
                return new ValidationResult(true, null);
            }
            return Result;
        }

        public ValidationResult Result { get; set; }

    }
}
