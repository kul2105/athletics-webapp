﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Athletics.Client.Controllers;
using Athletics.Client.Models;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.ValidationRules;
using Athletics.Client.Views;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{
    public class SignUpViewModel : BindableBase, ISignUpViewModel
    {
        private ISignUpView View;
        private ISignInManager signInManager;
        private IApplicationAuthStateController AuthStateController;
        //private IOrganizationSignUpViewModel OrgSignupViewModel;
        private const bool PREFILL_FORM = false;
        public SignUpViewModel(ISignUpView View, ISignInManager SignInManager, IApplicationAuthStateController AuthStateController, ISignedInUserViewModel signedInUserViewModel)
        {
            this.signInManager = SignInManager;
            this.AuthStateController = AuthStateController;
            AuthStateController.AuthorizationStateChanged += (x) =>
                {
                    try
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            if (AuthStateController.AuthenticationState == e_AUTH_STATE.SIGN_UP)
                            {
                                FirstName = "";
                                ShowFirstNameError = false;
                                LastName = "";
                                ShowLastNameError = false;
                                DisplayName = "";
                                ShowDisplayNameError = false;
                                Password = "";
                                ConfirmPassword = "";
                                _PasswordValid = false;
                                _PasswordHasBeenEntered = false;
                                _ConfirmPasswordValid = false;
                                _ConfirmPasswordHasBeenEntered = false;
                                _ValidDisplayNameHasBeenEntered = false;
                                PasswordValidationString = "";
                                ConfirmPasswordValidationString = "";
                                OnPropertyChanged("ShowPasswordValidationError");
                                OnPropertyChanged("ShowConfirmPasswordValidationError");
                            }
                            OnPropertyChanged("Visible");
                        }));
                    }
                    catch { }
                };

            _BrowseUserPicCommand = new DelegateCommand(() =>
              {
                  Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
                  openFileDlg.Filter = "Profile JPG Image (.jpg)|*.jpg";
                  // Launch OpenFileDialog by calling ShowDialog method
                  Nullable<bool> result = openFileDlg.ShowDialog();
                  // Get the selected file name and display in a TextBox.
                  // Load content of file in a TextBlock
                  if (result == true)
                  {
                      this.UserImage = new BitmapImage(new Uri(openFileDlg.FileName));
                  }
              });

            _SignUpCommand = new DelegateCommand(async () =>
            {
                APIServiceRequest_RegisterNewUser SignupInfo = new APIServiceRequest_RegisterNewUser(); // OrgSignupViewModel.SignUpInfo;
                SignupInfo.FirstName = FirstName;
                SignupInfo.LastName = LastName;
                SignupInfo.UserName = Email;
                SignupInfo.DisplayName = DisplayName;
                SignupInfo.EmailAddress = Email;
                SignupInfo.Password = Password;
                SignupInfo.ConfirmPassword = ConfirmPassword;
                SignupInfo.UserImage = this.GetJPGFromImageControl(this.UserImage as BitmapImage);

                AuthStateController.AuthenticationState = e_AUTH_STATE.SIGNING_UP;
                Password = null;
                ConfirmPassword = null;

                await Task.Run(async () =>
                 {
                     AuthorizedResponse<RegisterNewUserResult> Response = await signInManager.RegisterNewUserAsync(SignupInfo);
                     Application.Current.Dispatcher.Invoke(new Action(() =>
                     {
                         if (Response.Status == eAUTHORIZED_REQUEST_RESPONSE.SUCCESS)
                         {
                             //_VerifyEmailVM.FirstName = FirstName;
                             //_VerifyEmailVM.Email = Email;
                             //_VerifyEmailVM.Password = Password;
                             _PasswordHasBeenEntered = false;
                             _ConfirmPasswordHasBeenEntered = false;
                             TermsAndConditionsAccepted = false;
                             _Email = null;
                             OnPropertyChanged("Email");
                             EmailHasBeenEntered = false;
                             _EmailValid = false;
                             _EmailAlreadyUsed = false;
                             _CheckingEmail = false;
                             _EmailValidationString = "";
                             ShowEmailHasError = false;
                             AuthStateController.AuthenticationState = e_AUTH_STATE.REGISTRATION_COMPLETE;
                         }
                         else
                         {
                             AuthStateController.AuthenticationState = e_AUTH_STATE.SIGN_UP;
                         }
                     }));

                 });
            }, () =>
            {
                return FormDataPrevalidated;
            });

            _SignInCommand = new DelegateCommand(() =>
          {
              AuthStateController.AuthenticationState = e_AUTH_STATE.SIGN_IN_DEFAULT;
          });

            _SignInLaterCommand = new DelegateCommand(() =>
            {
                AuthStateController.AuthenticationState = e_AUTH_STATE.NOT_LOGGED_IN;
            });

            _CancelSignUpCommand = new DelegateCommand(() =>
            {
                signedInUserViewModel.ClearCache();
                AuthStateController.AuthenticationState = e_AUTH_STATE.NONE;
            });

            this.View = View;
            this.View.DataContext = this;
            if (PREFILL_FORM)
            {
                Task.Run(async () =>
                {
                    await Task.Delay(500);
                    await Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        Prefill();
                    }));
                });
            }
        }

        private void Prefill()
        {
            FirstName = "Devender";
            LastName = "Sharma";
            DisplayName = "displayname";
            Email = "asdf@asdf";
            Password = "asdf";
            ConfirmPassword = "asdf";
            UsageTypeConfirmed = true;
            TermsAndConditionsAccepted = true;
        }

        public byte[] GetJPGFromImageControl(BitmapImage imageC)
        {
            if (imageC == null) return null;
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.ToArray();
        }

        public bool Visible
        {
            get
            {
                bool Result = AuthStateController.AuthenticationState == e_AUTH_STATE.SIGN_UP;
                Application.Current.Dispatcher.InvokeAsync(async () =>
                    {
                        await Task.Delay(50);
                        if (Result)
                        {
                            View.ResetFocus();
                        }
                    });
                return Result;
            }
        }

        private bool _FirstNameValid = false;
        private string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                SetProperty(ref _FirstName, value);
                Prevalidate();
            }
        }
        private ImageSource _UserImage;
        public ImageSource UserImage
        {
            get { return _UserImage; }
            set { SetProperty(ref _UserImage, value); }
        }
        private bool _ShowFirstNameError = false;
        public bool ShowFirstNameError
        {
            get { return _ShowFirstNameError; }
            set { SetProperty(ref _ShowFirstNameError, value); }
        }


        private bool _LastNameValid = false;
        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set
            {
                SetProperty(ref _LastName, value);
                Prevalidate();
            }
        }

        private bool _ShowLastNameError = false;
        public bool ShowLastNameError
        {
            get { return _ShowLastNameError; }
            set { SetProperty(ref _ShowLastNameError, value); }
        }

        private bool _DisplayNameValid = false;
        private bool _ValidDisplayNameHasBeenEntered = false;
        private string _DisplayName;
        public string DisplayName
        {
            get { return _DisplayName; }
            set
            {
                SetProperty(ref _DisplayName, value);
                Prevalidate();
            }
        }

        private bool _ShowDisplayNameError = false;
        public bool ShowDisplayNameError
        {
            get { return _ShowDisplayNameError; }
            set { SetProperty(ref _ShowDisplayNameError, value); }
        }

        private string _DisplayNameErrorString = "Display Name is required.";
        public string DisplayNameErrorString
        {
            get { return _DisplayNameErrorString; }
            set { SetProperty(ref _DisplayNameErrorString, value); }
        }

        public void DisplayNameLostFocus()
        {
            if (!string.IsNullOrEmpty(_DisplayName))
            {
                _ValidDisplayNameHasBeenEntered = true;
                Prevalidate("DisplayName");
            }
        }



        private bool _EmailValid = false;
        private bool EmailHasBeenEntered = false;
        public bool EmailHasError
        {
            get
            {
                return EmailHasBeenEntered ? !_EmailValid : false;
            }
            set
            {
                EmailHasBeenEntered = true;
                _EmailValid = !value;
                OnPropertyChanged("EmailHasError");
            }
        }

        private bool _ShowEmailHasError;
        public bool ShowEmailHasError
        {
            get { return _ShowEmailHasError; }
            set { SetProperty(ref _ShowEmailHasError, value); }
        }


        private string _Email;
        public string Email
        {
            get { return _Email; }
            set
            {
                EmailHasBeenEntered = true;
                SetProperty(ref _Email, value);
                _EmailAlreadyUsed = true;
                CheckingEmail = true;
                Prevalidate();

                _EmailValidationForeground = Brushes.Orange;
                CheckingEmail = false;
                Application.Current.Dispatcher.Invoke(() =>
                    {
                        Prevalidate();
                    });
            }
        }

        private bool _EmailAlreadyUsed = false;
        public bool EmailAlreadyUsed
        {
            get { return _EmailAlreadyUsed; }
            set
            {
                SetProperty(ref _EmailAlreadyUsed, value);
                Prevalidate();
            }
        }

        private bool _CheckingEmail = false;
        public bool CheckingEmail
        {
            get { return _CheckingEmail; }
            set { SetProperty(ref _CheckingEmail, value); }
        }

        private string _EmailValidationString = "";
        public string EmailValidationString
        {
            get { return _EmailValidationString; }
            set { SetProperty(ref _EmailValidationString, value); }
        }

        private Brush _EmailValidationForeground = Brushes.Orange;
        public Brush EmailValidationForeground
        {
            get { return _EmailValidationForeground; }
            set { SetProperty(ref _EmailValidationForeground, value); }
        }




        private bool _PasswordValid = false;
        private bool _PasswordHasBeenEntered = false;

        public bool ShowPasswordValidationError
        {
            get { return (!_PasswordValid) && _PasswordHasBeenEntered; }
        }

        private string _PasswordValidationString = "";
        public string PasswordValidationString
        {
            get { return _PasswordValidationString; }
            set { SetProperty(ref _PasswordValidationString, value); }
        }


        private string _Password;
        public string Password
        {
            get { return _Password; }
            set
            {
                if (_Password == value)
                {
                    _PasswordHasBeenEntered = true;
                }
                if (_Password != value)
                {
                    _ConfirmPassword = null;
                    _ConfirmPasswordValid = false;
                    Prevalidate("ConfirmPassword");
                    OnPropertyChanged("ConfirmPassword");
                    SetProperty(ref _Password, value);
                }
                Prevalidate();
            }
        }

        private bool _ConfirmPasswordValid = false;
        private bool _ConfirmPasswordHasBeenEntered = false;

        public bool ShowConfirmPasswordValidationError
        {
            get { return (!_ConfirmPasswordValid) && _ConfirmPasswordHasBeenEntered; }
        }

        private string _ConfirmPasswordValidationString = "";
        public string ConfirmPasswordValidationString
        {
            get { return _ConfirmPasswordValidationString; }
            set { SetProperty(ref _ConfirmPasswordValidationString, value); }
        }

        private string _ConfirmPassword;
        public string ConfirmPassword
        {
            get { return _ConfirmPassword; }
            set
            {
                if (_ConfirmPassword == value)
                {
                    _ConfirmPasswordHasBeenEntered = true;
                }
                SetProperty(ref _ConfirmPassword, value);
                Prevalidate();
            }
        }

        private const string USER_EDUCATIONAL_INDIVIDUAL = "Educational User - Individual";
        private const string USER_EDUCATIONAL_ORGANIZATION = "Educational User - University Or Educational Organization";
        private const string USER_COMMERCIAL_INDIVIDUAL = "Commercial User - Individual";
        private const string USER_COMMERCIAL_SMALL = "Commercial User - Small Business (<$1M USD)";
        private const string USER_COMMERCIAL_MEDIUM = "Commercial User - Medium Business ($1M-$50M USD)";
        private const string USER_COMMERCIAL_LARGE = "Commercial User - Large Business ($50M-$250M USD)";
        private const string USER_COMMERCIAL_ENTERPRISE = "Commercial User - Enterprise (>$250M USD)";
        private List<string> _OrganizationTypes = new List<string>() { USER_EDUCATIONAL_INDIVIDUAL, USER_EDUCATIONAL_ORGANIZATION, USER_COMMERCIAL_INDIVIDUAL, USER_COMMERCIAL_SMALL, USER_COMMERCIAL_MEDIUM, USER_COMMERCIAL_LARGE, USER_COMMERCIAL_ENTERPRISE };
        public List<string> OrganizationTypes
        {
            get { return _OrganizationTypes; }
            set
            {
                SetProperty(ref _OrganizationTypes, value);
                Prevalidate();
            }
        }
        private string _CertificationText;
        public string CertificationText
        {
            get { return _CertificationText; }
            set
            {
                SetProperty(ref _CertificationText, value);
            }
        }

        private Visibility _CertificationVisibility = Visibility.Hidden;
        public Visibility CertificationVisibility
        {
            get { return _CertificationVisibility; }
            set { SetProperty(ref _CertificationVisibility, value); }
        }

        private bool _TermsAndConditionsAccepted;
        public bool TermsAndConditionsAccepted
        {
            get { return _TermsAndConditionsAccepted; }
            set
            {
                SetProperty(ref _TermsAndConditionsAccepted, value);
                Prevalidate();
            }
        }

        private bool _UsageTypeConfirmed;
        public bool UsageTypeConfirmed
        {
            get { return _UsageTypeConfirmed; }
            set
            {
                SetProperty(ref _UsageTypeConfirmed, value);
                Prevalidate();
            }
        }

        private bool _OrganizationInfoRequired;
        public bool OrganizationInfoRequired
        {
            get { return _OrganizationInfoRequired; }
            set { SetProperty(ref _OrganizationInfoRequired, value); }
        }


        private string _OrganizationNameRequest;
        public string OrganizationNameRequest
        {
            get { return _OrganizationNameRequest; }
            set { SetProperty(ref _OrganizationNameRequest, value); }
        }


        private bool _ShowOrganizationNameHasError;
        public bool ShowOrganizationNameHasError
        {
            get { return _ShowOrganizationNameHasError; }
            set { SetProperty(ref _ShowOrganizationNameHasError, value); }
        }



        private ICommand _BrowseUserPicCommand;
        public ICommand BrowseUserPicCommand
        {
            get { return _BrowseUserPicCommand; }
        }



        private ICommand _SignUpCommand;
        public ICommand SignUpCommand
        {
            get { return _SignUpCommand; }
        }

        private ICommand _SignInCommand;
        public ICommand SignInCommand
        {
            get { return _SignInCommand; }
        }

        private ICommand _SignInLaterCommand;
        public ICommand SignInLaterCommand
        {
            get { return _SignInLaterCommand; }
        }

        private ICommand _CancelSignUpCommand;
        public ICommand CancelSignUpCommand
        {
            get { return _CancelSignUpCommand; }
        }

        private bool FormDataPrevalidated = false;
        private void Prevalidate([CallerMemberName] string propertyName = null)
        {
            switch (propertyName)
            {

                case "FirstName":
                    _FirstNameValid = !string.IsNullOrEmpty(_FirstName);
                    ShowFirstNameError = !_FirstNameValid;
                    break;
                case "LastName":
                    _LastNameValid = !string.IsNullOrEmpty(_LastName);
                    ShowLastNameError = !_LastNameValid;
                    break;
                case "DisplayName":
                    if (string.IsNullOrEmpty(_DisplayName))
                    {
                        _DisplayNameValid = false;
                        DisplayNameErrorString = "Display Name is required.";
                        ShowDisplayNameError = true;
                    }
                    else if (_DisplayName.Length < 3)
                    {
                        _DisplayNameValid = false;
                        DisplayNameErrorString = "Must be at least 3 characters.";
                        ShowDisplayNameError = _ValidDisplayNameHasBeenEntered;
                    }
                    else if (_DisplayName.Length > 100)
                    {
                        _DisplayNameValid = false;
                        DisplayNameErrorString = "Maximum of 100 characters.";
                        ShowDisplayNameError = _ValidDisplayNameHasBeenEntered;
                    }
                    else
                    {
                        _ValidDisplayNameHasBeenEntered = true;
                        _DisplayNameValid = true;
                        ShowDisplayNameError = false;
                    }
                    break;

                case "Email":
                    if (string.IsNullOrEmpty(_Email))
                    {
                        EmailValidationString = "Email address required.";
                        EmailValidationForeground = Brushes.Orange;
                        EmailHasError = true;
                        ShowEmailHasError = true;
                    }
                    else
                    {
                        bool InitialEmailCheck = false;
                        try
                        {
                            var addr = new System.Net.Mail.MailAddress(_Email);
                            InitialEmailCheck = true;
                        }
                        catch { }
                        if (!InitialEmailCheck)
                        {
                            EmailValidationString = "Invalid format.";
                            EmailValidationForeground = Brushes.Orange;
                            EmailHasError = true;
                            ShowEmailHasError = true;
                        }
                        else if (CheckingEmail)
                        {
                            _EmailValid = true;
                            OnPropertyChanged("EmailValid");
                            EmailValidationForeground = new SolidColorBrush(Color.FromRgb((byte)0xE8, (byte)0xE8, (byte)0xFF));
                            EmailValidationString = "Checking email availability...";
                            Entities.Models.User user = signInManager.GetAllUser().Result.Where(p => p.EmailID == Email).FirstOrDefault();
                            if (user == null)
                            {
                                CheckingEmail = false;
                                EmailHasError = false;
                                ShowEmailHasError = false;
                                EmailValidationString = string.Empty;
                                Prevalidate("Email");
                                
                            }
                            else
                            {
                                EmailValidationString = "Email address exists.";
                                EmailHasError = true;
                                ShowEmailHasError = false;
                               
                            }
                              
                            return;
                        }
                        else
                        {
                            OnPropertyChanged("EmailValidationForeground");
                            OnPropertyChanged("EmailValidationString");
                            EmailHasError = (_EmailValidationString != null && _EmailValidationString != "");
                            ShowEmailHasError = EmailHasError;
                        }
                    }
                    break;
                case "Password":
                    ValidationResult passwordValidationResult = new PasswordValidation().Validate(_Password, CultureInfo.CurrentCulture);
                    _PasswordValid = passwordValidationResult.IsValid;
                    OnPropertyChanged("ShowPasswordValidationError");
                    PasswordValidationString = ShowPasswordValidationError ? (string)passwordValidationResult.ErrorContent : "";
                    break;
                case "ConfirmPassword":
                    ConfirmPasswordValidation confirm = new ConfirmPasswordValidation();
                    confirm.ComparisonValue = new PasswordComparisonValue()
                    {
                        Value = _Password
                    };
                    ValidationResult confirmPasswordValidationResult = confirm.Validate(_ConfirmPassword, CultureInfo.CurrentCulture);
                    _ConfirmPasswordValid = confirmPasswordValidationResult.IsValid;
                    OnPropertyChanged("ShowConfirmPasswordValidationError");
                    ConfirmPasswordValidationString = ShowConfirmPasswordValidationError ? (string)confirmPasswordValidationResult.ErrorContent : "";
                    break;
                case "TermsAndConditionsAccepted":
                    break;
                case "UsageTypeConfirmed":
                    break;

            }
            FormDataPrevalidated =
                _FirstNameValid &&
                _LastNameValid &&
                _DisplayNameValid &&
                /*
                _UsernameValid &&
                !_UsernameAlreadyTaken &&
                */

                _EmailValid &&
                _PasswordValid &&
                _ConfirmPasswordValid &&
                /*
                _SelectedOrganizationTypeValid &&
                (!_OrganizationInfoRequired || (_OrganizationNameValid && _OrganizationURLValid)) &&
                _OrganizationNameValid &&
                _UsageTypeConfirmed &&
                */
                _TermsAndConditionsAccepted;
            ((DelegateCommand)_SignUpCommand).RaiseCanExecuteChanged();
        }

    }
}
