﻿namespace Athletics.Client.ViewModels
{
    public interface ISignUpViewModel
    {
        void DisplayNameLostFocus();
    }
}
