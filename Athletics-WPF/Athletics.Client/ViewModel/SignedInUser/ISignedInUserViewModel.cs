﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Athletics.Client.Services.API;

namespace Athletics.Client.ViewModels
{
    public interface ISignedInUserViewModel : INotifyPropertyChanged
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string DisplayName { get; set; }
        string EmailAddress { get; set; }
        DateTime? UserCreationDate { get; set; }
        BitmapImage UserImage { get; set; }

        /// <summary>
        /// Provides persistent client authorization token caching (cookie)
        /// </summary>
        string AuthToken { get; }
        bool UserEmailIsVerified { get; }
        bool UserHasSpecifiedOrganization { get; }

        byte[] ProfilePicHash { get; }
        byte[] ProfilePic { get; set; }

        bool LoadCachedAuthenticationDetails();
        void CacheAuthToken();
        void ClearCache();
        void ClearCachedToken();

        bool IsSignedIn { get; }

        event Action SignedIn;
        event Action SignedOut;
        event Action OrgUsersReloaded;

        void LoadAccountDetails(GetMyAccountDetailsResult Details);

    }
}
