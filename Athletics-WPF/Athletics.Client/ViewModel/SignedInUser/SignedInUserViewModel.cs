﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Athletics.Client.Helpers;
using Athletics.Client.Services.API;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{
    public class SignedInUserViewModel : BindableBase, ISignedInUserViewModel, ISignedInUserViewModelWriteable
    {
        private readonly IAuthorizedAPIServiceProvider AuthorizedAPIs;
        public SignedInUserViewModel(IAuthorizedAPIServiceProvider AuthorizedAPIs)
        {
            this.AuthorizedAPIs = AuthorizedAPIs;
        }

        private string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set { SetProperty(ref _FirstName, value); }
        }

        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set { SetProperty(ref _LastName, value); }
        }

        private string _DisplayName;
        public string DisplayName
        {
            get { return _DisplayName; }
            set { SetProperty(ref _DisplayName, value); }
        }

        private string _EmailAddress;
        public string EmailAddress
        {
            get { return _EmailAddress; }
            set { SetProperty(ref _EmailAddress, value); }
        }

        private DateTime? _UserCreationDate;
        public DateTime? UserCreationDate
        {
            get { return _UserCreationDate; }
            set { SetProperty(ref _UserCreationDate, value); }
        }


        private BitmapImage _UserImage;
        public BitmapImage UserImage
        {
            get { return _UserImage; }
            set { SetProperty(ref _UserImage, value); }
        }

        private string _AuthToken;
        public string AuthToken
        {
            get
            {
                return _AuthToken;
            }
            set
            {
                SetProperty(ref _AuthToken, value);
                AuthorizedAPIs.AuthToken = value;
            }
        }

        public bool AuthTokenValid
        {
            get
            {
                return _AuthToken != null && _AuthToken != "";
            }
        }

        private bool _UserEmailIsVerified = false;
        public bool UserEmailIsVerified
        {
            get { return _UserEmailIsVerified; }
            set { SetProperty(ref _UserEmailIsVerified, value); }
        }

        private bool _UserHasSpecifiedOrganization = false;
        public bool UserHasSpecifiedOrganization
        {
            get { return _UserHasSpecifiedOrganization; }
            set { SetProperty(ref _UserHasSpecifiedOrganization, value); }
        }


        private byte[] _ProfilePicHash;
        public byte[] ProfilePicHash
        {
            get
            {
                if (_ProfilePicHash == null)
                {
                    _ProfilePicHash = System.Convert.FromBase64String(Properties.Settings.Default.ProfilePicHash);
                }
                return _ProfilePicHash;
            }
            set
            {
                SetProperty(ref _ProfilePicHash, value);
            }
        }

        private byte[] _ProfilePic;
        public byte[] ProfilePic
        {
            get
            {
                if (_ProfilePic == null)
                {
                    if (Properties.Settings.Default.ProfilePic != null)
                    {
                        _ProfilePic = System.Convert.FromBase64String(Properties.Settings.Default.ProfilePic);
                    }
                }
                return _ProfilePic;
            }
            set
            {
                _ProfilePic = value;
                MD5 _md5 = MD5.Create();
                ProfilePicHash = _md5.ComputeHash(_ProfilePic);
                Properties.Settings.Default.ProfilePic = System.Convert.ToBase64String(_ProfilePic);
                Properties.Settings.Default.ProfilePicHash = System.Convert.ToBase64String(_ProfilePicHash);
                Properties.Settings.Default.Save();

                ProfilePicImage = ImageHelpers.BitmapImageFromByteArray(_ProfilePic);
                /*
                try
                {
                    BitmapImage image = new BitmapImage();
                    using (var ms = new MemoryStream(_ProfilePic))
                    {
                        ms.Position = 0;
                        image.BeginInit();
                        image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.UriSource = null;
                        image.StreamSource = ms;
                        image.EndInit();
                        image.Freeze();
                        ProfilePicImage = image;
                    }
                }
                catch (Exception ex)
                {
                    ProfilePicImage = null;
                }
                */
            }
        }

        private BitmapImage _ProfilePicImage;
        public BitmapImage ProfilePicImage
        {
            get { return _ProfilePicImage; }
            set
            {
                SetProperty(ref _ProfilePicImage, value);
            }
        }

        private string GetMd5Hash(byte[] input)
        {
            MD5 md5Hash = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(input);

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public bool LoadCachedAuthenticationDetails()
        {
            if (Properties.Settings.Default.AuthenticationToken != null && Properties.Settings.Default.AuthenticationToken != "")
            {
                AuthToken = Properties.Settings.Default.AuthenticationToken;
                return true;
            }
            return false;
        }

        public void CacheAuthToken()
        {
            Properties.Settings.Default.AuthenticationToken = AuthToken;
            Properties.Settings.Default.Save();
        }

        public void ClearCache()
        {
            Properties.Settings.Default.ProfilePic = null;
            Properties.Settings.Default.ProfilePicHash = null;
            Properties.Settings.Default.AuthenticationToken = null;
            Properties.Settings.Default.Save();
        }


        public void ClearCachedToken()
        {
            Properties.Settings.Default.AuthenticationToken = null;
            Properties.Settings.Default.Save();
        }

        private bool _IsSignedIn = false;
        public bool IsSignedIn
        {
            get { return _IsSignedIn; }
            set
            {
                if (_IsSignedIn != value)
                {
                    SetProperty(ref _IsSignedIn, value);
                    if (value)
                    {
                        Action pSignedIn = SignedIn;
                        if (pSignedIn != null)
                        {
                            pSignedIn();
                        }
                    }
                    else
                    {
                        Action pSignedOut = SignedOut;
                        if (pSignedOut != null)
                        {
                            pSignedOut();
                        }
                    }
                }
            }
        }

        public event Action SignedIn;

        public event Action SignedOut;

        public event Action OrgUsersReloaded;

        public async Task<bool> ReloadOrgUsers()
        {

            Action pOrgUsersReloaded = OrgUsersReloaded;
            if (pOrgUsersReloaded != null)
            {
                pOrgUsersReloaded();
            }
            return true;
        }

        public void LoadAccountDetails(GetMyAccountDetailsResult Details)
        {
            if (Details == null)
            {
                DisplayName = "";
                EmailAddress = "";
                FirstName = "";
                LastName = "";

            }
            else
            {
                DisplayName = Details.DisplayName;
                EmailAddress = Details.EmailAddress;
                FirstName = Details.FirstName;
                LastName = Details.LastName;
                UserCreationDate = Details.UserCreationDate;
            }
        }
    }
}
