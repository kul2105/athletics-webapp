﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Athletics.Client.Services.API;

namespace Athletics.Client.ViewModels
{
    public interface ISignedInUserViewModelWriteable : ISignedInUserViewModel, INotifyPropertyChanged
    {
        /// <summary>
        /// Provides persistent client authorization token caching (cookie)
        /// </summary>
        string AuthToken { get; set; }
        bool UserEmailIsVerified { get; set; }
        bool UserHasSpecifiedOrganization { get; set; }

        byte[] ProfilePicHash { get; set; }
        bool IsSignedIn { get; set; }
    }
}
