﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Athletics.Client.Controllers;
using Athletics.Client.Models;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.ValidationRules;
using Athletics.Client.Views;
using Athletics.Entities.Models;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{
    public class UserRoleViewModel : BindableBase, IUserRoleViewModel
    {
        private IUserRoleView View = null;
        //private IAnonymousAPIServiceProvider AnonymousAPIs;

        private IApplicationAuthStateController AuthStateController;
        private ISignInManager SignInManager;
        public UserRoleViewModel(IUserRoleView view, ISignInManager signInManager, IApplicationAuthStateController authController)
        {
            View = view;
            //this.AnonymousAPIs = ServerAPI;

            this.AuthStateController = authController;
            this.SignInManager = signInManager;
            _CreateRoleCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            APIServiceRequest_GetMyRoleDetails newRoleInfo = new APIServiceRequest_GetMyRoleDetails(); // OrgSignupViewModel.SignUpInfo;
                            newRoleInfo.RoleGuid = Guid.NewGuid().ToString();
                            newRoleInfo.RoleID = RoleID;
                            newRoleInfo.RoleName = RoleName;
                            AuthStateController.AuthenticationState = e_AUTH_STATE.CREATE_ROLE;

                            await Task.Run(async () =>
                            {
                                AuthorizedResponse<RegisterNewRoleResult> Response = await signInManager.CreateNewRole(newRoleInfo);
                                Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    if (Response.Status == eAUTHORIZED_REQUEST_RESPONSE.SUCCESS)
                                    {
                                        AuthStateController.AuthenticationState = e_AUTH_STATE.CREATE_ROLE_COMPLETE;
                                    }
                                    else
                                    {
                                        AuthStateController.AuthenticationState = e_AUTH_STATE.CREATE_ROLE;
                                    }
                                }));

                            });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });
            _CancelUserRoleCommand = new DelegateCommand(async () =>
            {
                AuthStateController.AuthenticationState = e_AUTH_STATE.CREATE_ROLE_COMPLETE;
            });

            View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            if (_RoleNameValid == true && _RoleIDValid == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool _RoleNameValid = false;
        private string _RoleName;
        public string RoleName
        {
            get { return _RoleName; }
            set
            {
                SetProperty(ref _RoleName, value);
                Prevalidate();
                ((DelegateCommand)_CreateRoleCommand).RaiseCanExecuteChanged();
            }
        }
        private string _RoleID;
        private bool _RoleIDValid = false;
        public string RoleID
        {
            get { return _RoleID; }
            set
            {
                SetProperty(ref _RoleID, value);
                Prevalidate();
                ((DelegateCommand)_CreateRoleCommand).RaiseCanExecuteChanged();
            }
        }
        private ICommand _CreateRoleCommand;
        public ICommand CreateRoleCommand
        {
            get { return _CreateRoleCommand; }
        }
        private ICommand _CancelUserRoleCommand;
        public ICommand CancelUserRoleCommand
        {
            get { return _CancelUserRoleCommand; }
        }

        private bool FormDataPrevalidated = false;
        private void Prevalidate([CallerMemberName] string propertyName = null)
        {
            switch (propertyName)
            {

                case "RoleName":
                    _RoleNameValid = !string.IsNullOrEmpty(_RoleName);
                    break;
                case "RoleID":
                    _RoleIDValid = !string.IsNullOrEmpty(_RoleID);
                    break;

            }
        }

    }
}
