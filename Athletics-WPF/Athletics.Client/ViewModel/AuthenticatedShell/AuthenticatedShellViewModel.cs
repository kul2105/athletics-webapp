﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Athletics.Client.Controllers;
using Athletics.Client.Interactions;
using Athletics.Client.Views;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{
    public class AuthenticatedShellViewModel : BindableBase, IAuthenticatedShellViewModel
    {
        private IAuthenticatedShellView View;
        private IApplicationAuthStateController _AuthStateController;
        public delegate void ViewVisibility(string Name);
        public static event ViewVisibility MakeVisibleView;
        public AuthenticatedShellViewModel(IAuthenticatedShellView View, IApplicationAuthStateController AuthStateController)
        {
            this.View = View;
            View.DataContext = this;
            this._AuthStateController = AuthStateController;
            _AuthStateController.AuthorizationStateChanged += AuthorizationStateChanged;
            AuthorizationStateChanged(_AuthStateController.AuthenticationState);
            OnPropertyChanged(null);

        }

        private void DisableAllViews()
        {
            SignInDefaultViewVisible = false;
            SigningInPleaseWaitViewVisible = false;
            FacebookLogInViewVisible = false;
            GoogleLogInViewVisible = false;
            CreatingOrganizationPleaseWaitViewVisible = false;
            SignUpViewVisible = false;
            FinishExternalSignUpViewVisible = false;
            JoinExistingOrganizationOrCreateNewViewVisible = false;
            CreateNewOrganizationViewVisible = false;
            SigningUpPleaseWaitViewVisible = false;
            PasswordResetRequiredViewVisible = false;
            EmailVerificationRequiredViewVisible = false;
            SendResetPasswordLinkViewVisible = false;
            RegistrationCompleteViewVisible = false;
            AuthenticatedAppShellViewVisible = false;
            CreateUserRoleViewVisible = false;
            AssignUserRoleViewVisible = false;
            CreateMenuViewVisible = false;
            AssignMenuViewVisible = false;
        }

        private void AuthorizationStateChanged(e_AUTH_STATE NewAuthState)
        {
            DisableAllViews();
            switch (NewAuthState)
            {
                case e_AUTH_STATE.SIGN_IN_DEFAULT:
                    SignInDefaultViewVisible = true;
                    break;
                case e_AUTH_STATE.SIGN_UP:
                    SignUpViewVisible = true;
                    break;
                case e_AUTH_STATE.FINISH_EXTERNAL_SIGN_UP:
                    FinishExternalSignUpViewVisible = true;
                    break;
                case e_AUTH_STATE.SIGNING_UP:
                    SigningUpPleaseWaitViewVisible = true;
                    break;
                case e_AUTH_STATE.REGISTRATION_COMPLETE:
                    RegistrationCompleteViewVisible = true;
                    break;
                case e_AUTH_STATE.CREATING_USER:
                    break;// Transition State
                case e_AUTH_STATE.WELCOME_NEW_USER:
                    break;
                case e_AUTH_STATE.UPLOADING_NEW_USER_INFO:
                    // Transition State
                    break;
                case e_AUTH_STATE.LOGGING_IN:
                    // Transition State
                    SigningInPleaseWaitViewVisible = true;
                    break;
                case e_AUTH_STATE.LOG_IN_FAILED:
                    // Notification State
                    break;
                case e_AUTH_STATE.LOGGED_IN:
                    AuthenticatedAppShellViewVisible = true;
                    break;
                case e_AUTH_STATE.NOT_LOGGED_IN:
                    // "Log In Later" (Using As Guest)
                    AuthenticatedAppShellViewVisible = true;
                    break;
                case e_AUTH_STATE.CREATE_ROLE:
                    CreateUserRoleViewVisible = true;
                    break;

                case e_AUTH_STATE.ASSIGN_ROLE:
                    AssignUserRoleViewVisible = true;
                    if (MakeVisibleView != null)
                        MakeVisibleView("AssignRole");
                    break;

                case e_AUTH_STATE.CREATE_MENU:
                    CreateMenuViewVisible = true;
                    break;
                case e_AUTH_STATE.ASSIGN_MENU:
                    if (MakeVisibleView != null)
                        MakeVisibleView("AssignMenu");
                    AssignMenuViewVisible = true;
                    break;
            }
        }

        private bool _SignInDefaultViewVisible = true;
        public bool SignInDefaultViewVisible
        {
            get
            {
                return _SignInDefaultViewVisible;
            }
            protected set { SetProperty(ref _SignInDefaultViewVisible, value); }
        }

        private bool _FacebookLogInViewVisible = false;
        public bool FacebookLogInViewVisible
        {
            get
            {
                return _FacebookLogInViewVisible;
            }
            protected set { SetProperty(ref _FacebookLogInViewVisible, value); }
        }

        private bool _GoogleLogInViewVisible = false;
        public bool GoogleLogInViewVisible
        {
            get
            {
                return _GoogleLogInViewVisible;
            }
            protected set { SetProperty(ref _GoogleLogInViewVisible, value); }
        }

        private bool _SigningInPleaseWaitViewVisible = false;
        public bool SigningInPleaseWaitViewVisible
        {
            get { return _SigningInPleaseWaitViewVisible; }
            set
            {
                SetProperty(ref _SigningInPleaseWaitViewVisible, value);
            }
        }

        private bool _CreatingOrganizationPleaseWaitViewVisible;
        public bool CreatingOrganizationPleaseWaitViewVisible
        {
            get { return _CreatingOrganizationPleaseWaitViewVisible; }
            set { SetProperty(ref _CreatingOrganizationPleaseWaitViewVisible, value); }
        }


        private bool _SignUpViewVisible = false;
        public bool SignUpViewVisible
        {
            get { return _SignUpViewVisible; }
            protected set
            {
                SetProperty(ref _SignUpViewVisible, value);
            }
        }

        private bool _FinishExternalSignUpViewVisible = false;
        public bool FinishExternalSignUpViewVisible
        {
            get { return _FinishExternalSignUpViewVisible; }
            protected set
            {
                SetProperty(ref _FinishExternalSignUpViewVisible, value);
            }
        }


        private bool _JoinExistingOrganizationOrCreateNewViewVisible = false;
        public bool JoinExistingOrganizationOrCreateNewViewVisible
        {
            get { return _JoinExistingOrganizationOrCreateNewViewVisible; }
            protected set
            {
                SetProperty(ref _JoinExistingOrganizationOrCreateNewViewVisible, value);
            }
        }


        private bool _CreateNewOrganizationViewVisible = false;
        public bool CreateNewOrganizationViewVisible
        {
            get { return _CreateNewOrganizationViewVisible; }
            protected set
            {
                SetProperty(ref _CreateNewOrganizationViewVisible, value);
            }
        }


        private bool _SigningUpPleaseWaitViewVisible = false;
        public bool SigningUpPleaseWaitViewVisible
        {
            get { return _SigningUpPleaseWaitViewVisible; }
            set
            {
                SetProperty(ref _SigningUpPleaseWaitViewVisible, value);
            }
        }

        private bool _PasswordResetRequiredViewVisible = false;
        public bool PasswordResetRequiredViewVisible
        {
            get { return _PasswordResetRequiredViewVisible; }
            protected set
            {
                SetProperty(ref _PasswordResetRequiredViewVisible, value);
            }
        }

        private bool _EmailVerificationRequiredViewVisible = false;
        public bool EmailVerificationRequiredViewVisible
        {
            get { return _EmailVerificationRequiredViewVisible; }
            protected set
            {
                SetProperty(ref _EmailVerificationRequiredViewVisible, value);
            }
        }

        private bool _SendResetPasswordLinkViewVisible = false;
        public bool SendResetPasswordLinkViewVisible
        {
            get { return _SendResetPasswordLinkViewVisible; }
            protected set
            {
                SetProperty(ref _SendResetPasswordLinkViewVisible, value);
            }
        }



        private bool _RegistrationCompleteViewVisible = false;
        public bool RegistrationCompleteViewVisible
        {
            get { return _RegistrationCompleteViewVisible; }
            protected set
            {
                SetProperty(ref _RegistrationCompleteViewVisible, value);
            }
        }


        private bool _AuthenticatedAppShellViewVisible = false;
        public bool AuthenticatedAppShellViewVisible
        {
            get { return _AuthenticatedAppShellViewVisible; }
            protected set
            {
                SetProperty(ref _AuthenticatedAppShellViewVisible, value);
            }
        }

        private bool _CreateUserRoleViewVisible = false;
        public bool CreateUserRoleViewVisible
        {
            get { return _CreateUserRoleViewVisible; }
            protected set
            {
                SetProperty(ref _CreateUserRoleViewVisible, value);
            }
        }

        private bool _CreateMenuViewVisible = false;
        public bool CreateMenuViewVisible
        {
            get { return _CreateMenuViewVisible; }
            protected set
            {
                SetProperty(ref _CreateMenuViewVisible, value);
            }
        }

        private bool _AssignUserRoleViewVisible = false;
        public bool AssignUserRoleViewVisible
        {
            get { return _AssignUserRoleViewVisible; }
            protected set
            {
                SetProperty(ref _AssignUserRoleViewVisible, value);
            }
        }

        private bool _AssignMenuViewVisible = false;
        public bool AssignMenuViewVisible
        {
            get { return _AssignMenuViewVisible; }
            protected set
            {
                SetProperty(ref _AssignMenuViewVisible, value);
            }
        }

        private ManageUsersInteractionViewModel _ManageUsersVM = new ManageUsersInteractionViewModel();
        public ManageUsersInteractionViewModel ManageUsersVM
        {
            get { return _ManageUsersVM; }
        }

    }
}
