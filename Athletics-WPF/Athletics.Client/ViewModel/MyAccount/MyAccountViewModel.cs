﻿using Athletics.Client.Controllers;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.Views;
using Microsoft.Practices.Prism.Mvvm;
using System.Windows.Media.Imaging;

namespace Athletics.Client.ViewModels
{

    public class MyAccountViewModel : BindableBase, IMyAccountViewModel
    {
        private readonly IMyAccountView View;
        private readonly IApplicationAuthStateController AuthStateController;
        private readonly ISignInManager SignInManager;
        private readonly ISignedInUserViewModel _SignedInUserVM;

        public MyAccountViewModel(IMyAccountView View, IApplicationAuthStateController AuthStateController, ISignInManager SignInManager, ISignedInUserViewModel SignedInUserVM)
        {
            this.View = View;
            this.AuthStateController = AuthStateController;
            this.SignInManager = SignInManager;
            this._SignedInUserVM = SignedInUserVM;

            AuthStateController.AuthorizationStateChanged += (x) =>
                {
                    if (AuthStateController.AuthenticationState == e_AUTH_STATE.LOGGED_IN)
                    {
                    }
                };

            View.DataContext = this;
        }

        public ISignedInUserViewModel SignedInUserVM
        {
            get { return _SignedInUserVM; }
        }

        private BitmapImage _ProfilePic;
        public BitmapImage ProfilePic
        {
            get { return _ProfilePic; }
            set
            {
                SetProperty(ref _ProfilePic, value);
            }
        }

        

    }
}
