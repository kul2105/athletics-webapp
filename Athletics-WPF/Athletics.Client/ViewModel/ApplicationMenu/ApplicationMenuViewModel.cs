﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Athletics.Client.Controllers;
using Athletics.Client.Models;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.ValidationRules;
using Athletics.Client.Views;
using Athletics.Entities.Models;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{
    public class ApplicationMenuViewModel : BindableBase, IApplicationMenuViewModel
    {
        private IApplicationMenuView View = null;
        //private IAnonymousAPIServiceProvider AnonymousAPIs;

        private IApplicationAuthStateController AuthStateController;
        private ISignInManager SignInManager;
        public ApplicationMenuViewModel(IApplicationMenuView view, ISignInManager signInManager, IApplicationAuthStateController authController)
        {
            View = view;
            //this.AnonymousAPIs = ServerAPI;

            this.AuthStateController = authController;
            this.SignInManager = signInManager;
            _ExitMenuCommand = new DelegateCommand(async () =>
            {
                AuthStateController.AuthenticationState = e_AUTH_STATE.CREATE_MENU_COMPLETE;
                MenuName = string.Empty;
            });

            _CreateMenuCommand = new DelegateCommand(async () =>
        {
            if (!ValidateInputs())
            {

            }
            else
            {
                await Task.Run(async () =>
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        if (this.ParentMenuList.Where(p => p.MenuName == MenuName && p.ParentID == SelectedParentMenu.MenuID).Count() <= 0)
                        {
                            APIServiceRequest_GetApplicationMenuDetails newMenueInfo = new APIServiceRequest_GetApplicationMenuDetails(); // OrgSignupViewModel.SignUpInfo;
                            newMenueInfo.ApplicationMenuGuid = this.ParentMenuList.Count() + 1;
                            newMenueInfo.ApplicationMenuID = SelectedParentMenu.MenuID;
                            newMenueInfo.ApplicationMenuName = MenuName;
                            AuthStateController.AuthenticationState = e_AUTH_STATE.CREATE_MENU;
                            ApplicationMenu appMen = new ApplicationMenu();
                            appMen.MenuID = newMenueInfo.ApplicationMenuID;
                            appMen.MenuName = newMenueInfo.ApplicationMenuName;
                            appMen.ParentID = newMenueInfo.ApplicationMenuID;
                            this.ParentMenuList.Add(appMen);
                            await Task.Run(async () =>
                            {
                                AuthorizedResponse<RegisterNewApplicationMenuResult> Response = await signInManager.CreateNewMenu(newMenueInfo);


                            });
                        }

                    })).Wait();
                });
            }
        }, () =>
        {
            return ValidateInputs();
        });
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (this.AuthStateController.CurrentUserName == null);
                ParentMenuList.Clear();
               Task<List<ApplicationMenu>> applicationMenuList= signInManager.GetAllApplicationMenu();
                if (applicationMenuList == null) return;
                foreach (Entities.Models.ApplicationMenu menu in applicationMenuList.Result)
                {

                    ParentMenuList.Add(menu);
                }

            }));
            View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            if (_MenuNameValid == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool _MenuNameValid = false;
        private string _MenuName;
        public string MenuName
        {
            get { return _MenuName; }
            set
            {
                SetProperty(ref _MenuName, value);
                Prevalidate();
                ((DelegateCommand)_CreateMenuCommand).RaiseCanExecuteChanged();
            }
        }
        private string _MenuID;
        private bool _RoleIDValid = false;
        public string MenuID
        {
            get { return _MenuID; }
            set
            {
                SetProperty(ref _MenuID, value);
                Prevalidate();
                ((DelegateCommand)_CreateMenuCommand).RaiseCanExecuteChanged();
            }
        }

        private ObservableCollection<Entities.Models.ApplicationMenu> _ParentMenuList = new ObservableCollection<Entities.Models.ApplicationMenu>();
        public ObservableCollection<Entities.Models.ApplicationMenu> ParentMenuList
        {
            get { return _ParentMenuList; }
        }

        private ICommand _CreateMenuCommand;
        public ICommand CreateMenuCommand
        {
            get { return _CreateMenuCommand; }
        }
        private ICommand _ExitMenuCommand;
        public ICommand ExitMenuCommand
        {
            get { return _ExitMenuCommand; }
        }

        private ApplicationMenu _SelectedParentMenu;
        public ApplicationMenu SelectedParentMenu
        {
            get { return _SelectedParentMenu; }
            set
            {
                SetProperty(ref _SelectedParentMenu, value);
                ((DelegateCommand)_CreateMenuCommand).RaiseCanExecuteChanged();
            }
        }

        private bool FormDataPrevalidated = false;
        private void Prevalidate([CallerMemberName] string propertyName = null)
        {
            switch (propertyName)
            {

                case "MenuName":
                    _MenuNameValid = !string.IsNullOrEmpty(_MenuName);
                    break;
                case "MenuID":
                    _RoleIDValid = !string.IsNullOrEmpty(_MenuID);
                    break;

            }
        }

    }
}
