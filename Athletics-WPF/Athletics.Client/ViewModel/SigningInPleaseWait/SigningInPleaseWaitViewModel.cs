﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Client.Controllers;
using Athletics.Client.Helpers;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.Views;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{

    public class SigningInPleaseWaitViewModel : BindableBase, ISigningInPleaseWaitViewModel
    {
        private ISigningInPleaseWaitView View;
        private IApplicationAuthStateController AuthStateController;

        public SigningInPleaseWaitViewModel(ISigningInPleaseWaitView View, IApplicationAuthStateController AuthStateController, ISignInManager AuthTokenCache)
        {
            this.View = View;
            this.AuthStateController = AuthStateController;
            AuthStateController.AuthorizationStateChanged += (x) =>
                {
                    try
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            OnPropertyChanged("Visible");
                        }));
                    }
                    catch { }
                };
            View.DataContext = this;
        }


        public bool Visible
        {
            get
            {
                return AuthStateController.AuthenticationState == e_AUTH_STATE.LOGGING_IN;
            }
        }
    }
}
