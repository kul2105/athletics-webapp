﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Athletics.Client.Controllers;
using Athletics.Client.Models;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.ValidationRules;
using Athletics.Client.Views;
using Athletics.Entities.Models;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{
    public class UserInRoleViewModel : BindableBase, IUserInRoleViewModel
    {
        private IUserInRoleView View = null;
        private IApplicationAuthStateController AuthStateController;
        private ISignInManager SignInManager;
        private readonly IApplicationUserController UserController;
        private IAuthorizedAPIServiceProvider AuthorizedAPIs;
        public UserInRoleViewModel(IUserInRoleView view, ISignInManager signInManager, IApplicationAuthStateController authController, IApplicationUserController userController, IAuthorizedAPIServiceProvider AuthorizedAPI)
        {
            View = view;
            this.AuthStateController = authController;
            this.SignInManager = signInManager;
            this.UserController = userController;
            this.AuthorizedAPIs = AuthorizedAPI;
            AuthenticatedShellViewModel.MakeVisibleView += AuthenticatedShellViewModel_MakeVisibleView;
            _AssignRoleCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            if (this.SelectedUser == null)
                            {
                                MessageBox.Show("Please select the user to Assign");
                                return;
                            }
                            List<Role> roleList = await signInManager.GetUserInRole(this.SelectedUser.UserGuid);
                            if (roleList != null)
                            {
                                if (roleList.Count > 0)
                                {
                                    MessageBoxResult dresult = MessageBox.Show(string.Format("{0} User is already assigned with Role. Do you wants to Remove the User from Existing Role", this.SelectedUser.UserName), "User Assign to Role", MessageBoxButton.YesNo);
                                    if (dresult == MessageBoxResult.Yes)
                                    {
                                        await signInManager.RemoveUserFromRole(this.SelectedUser.UserGuid);
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                            }
                            APIServiceRequest_AssignUserInRole newUserInRoleInfo = new APIServiceRequest_AssignUserInRole();
                            newUserInRoleInfo.RoleID = this.SelectedRole.RoleGuid;
                            newUserInRoleInfo.UserID = this.SelectedUser.UserGuid;
                            AuthStateController.AuthenticationState = e_AUTH_STATE.ASSIGN_ROLE;

                            await Task.Run(async () =>
                            {
                                AuthorizedResponse<RegisterNewRoleResult> Response = await signInManager.AssignRoleToUser(newUserInRoleInfo);
                                Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    if (Response.Status == eAUTHORIZED_REQUEST_RESPONSE.SUCCESS)
                                    {
                                        AuthStateController.AuthenticationState = e_AUTH_STATE.ASSIGN_ROLE_COMPLETE;
                                        AssignedRoleList.Add(SelectedRole);
                                    }
                                    else
                                    {
                                        AuthStateController.AuthenticationState = e_AUTH_STATE.ASSIGN_ROLE;
                                    }
                                }));

                            });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });
            try
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    _ApplicationUsers.Clear();
                    RoleList.Clear();
                    foreach (User user in SignInManager.GetAllUser().Result)
                    {
                        _ApplicationUsers.Add(user);
                    }
                    foreach (Role user in SignInManager.GetAllRole().Result)
                    {
                        RoleList.Add(user);
                    }
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in pouplating data: " + ex.Message);
            }

            _CancelAssignUserCommand = new DelegateCommand(async () =>
            {
                AuthStateController.AuthenticationState = e_AUTH_STATE.ASSIGN_ROLE_COMPLETE;
            });
            View.DataContext = this;
        }

        private void AuthenticatedShellViewModel_MakeVisibleView(string Name)
        {
            if (Name == "AssignRole")
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    _ApplicationUsers.Clear();
                    RoleList.Clear();
                    foreach (User user in SignInManager.GetAllUser().Result)
                    {
                        _ApplicationUsers.Add(user);
                    }
                    foreach (Role user in SignInManager.GetAllRole().Result)
                    {
                        RoleList.Add(user);
                    }
                }));
            }
        }

        private bool ValidateInputs()
        {
            if (SelectedRole != null && AssignedRoleList.Where(p => p.RoleGuid == SelectedRole.RoleGuid).Count() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private ICommand _AssignRoleCommand;
        public ICommand AssignRoleCommand
        {
            get { return _AssignRoleCommand; }
        }
        private ICommand _CancelAssignUserCommand;
        public ICommand CancelAssignUserCommand
        {
            get { return _CancelAssignUserCommand; }
        }

        private ObservableCollection<User> _ApplicationUsers = new ObservableCollection<User>();
        public ObservableCollection<User> ApplicationUsers
        {
            get
            {
                return _ApplicationUsers;
            }
        }
        public IApplicationUserController ApplicationUserController
        {
            get { return UserController; }
        }

        private User _SelectedUser;
        public User SelectedUser
        {
            get { return _SelectedUser; }
            set
            {
                SetProperty(ref _SelectedUser, value);
                ((DelegateCommand)AssignRoleCommand).RaiseCanExecuteChanged();
            }
        }
        private Role _SelectedRole;
        public Role SelectedRole
        {
            get { return _SelectedRole; }
            set
            {
                SetProperty(ref _SelectedRole, value);
                ((DelegateCommand)AssignRoleCommand).RaiseCanExecuteChanged();
            }
        }

        private ObservableCollection<Role> _RoleList = new ObservableCollection<Role>();
        public ObservableCollection<Role> RoleList
        {
            get { return _RoleList; }
            set
            {
                SetProperty(ref _RoleList, value);
            }
        }

        private ObservableCollection<Role> _AssignedRoleList = new ObservableCollection<Role>();
        public ObservableCollection<Role> AssignedRoleList
        {
            get { return _AssignedRoleList; }
            set
            {
                SetProperty(ref _AssignedRoleList, value);
                value.CollectionChanged -= Value_CollectionChanged;
                value.CollectionChanged += Value_CollectionChanged;
            }
        }

        private void Value_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ((DelegateCommand)AssignRoleCommand).RaiseCanExecuteChanged();
        }

        private Role _SelectedAssignedRole;
        public Role SelectedAssignedRole
        {
            get { return _SelectedAssignedRole; }
            set
            {
                SetProperty(ref _SelectedAssignedRole, value);
            }
        }
    }
}
