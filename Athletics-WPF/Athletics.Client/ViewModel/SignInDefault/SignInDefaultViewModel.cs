﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Client.Controllers;
using Athletics.Client.Helpers;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.Views;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{

    public class SignInDefaultViewModel : BindableBase, ISignInDefaultViewModel
    {
        private readonly ISignInDefaultView View;
        private readonly IApplicationAuthStateController AuthStateController;
        private readonly ISignInManager SignInManager;
        private readonly ISignedInUserViewModel signedInUserViewModel;
        public SignInDefaultViewModel(ISignInDefaultView View, IApplicationAuthStateController AuthStateController, IAuthorizedAPIServiceProvider AuthorizedAPI, ISignInManager SignInManager, ISignedInUserViewModel signedInUserViewModel)
        {
            this.View = View;
            this.AuthStateController = AuthStateController;
            this.SignInManager = SignInManager;
            this.signedInUserViewModel = signedInUserViewModel;
            AuthStateController.AuthorizationStateChanged += (x) =>
   {
       try
       {
           Application.Current.Dispatcher.Invoke(new Action(() =>
           {
               OnPropertyChanged("Visible");
               if (AuthStateController.AuthenticationState == e_AUTH_STATE.SIGN_IN_DEFAULT)
               {
                   View.ResetFocus();
               }
           }));
       }
       catch { }
   };

            bool FlushCache = false;
            if (FlushCache)
            {
                Properties.Settings.Default.RememberMe = false;
                Properties.Settings.Default.Save();
            }

            _RememberMe = Properties.Settings.Default.RememberMe;

            _SignInWithAthleticCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {
                    ShowEntryErrors = true;
                }
                else
                {
                    SignInErrorMessage = "";
                    ResendVerificationEmailCommandVisible = false;

                    AuthStateController.AuthenticationState = e_AUTH_STATE.LOGGING_IN;
                    await Task.Run(async () =>
                         {
                             Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                             {
                                 AuthorizedResponse<SignInAPIResultPayload> Response = await SignInManager.SignInAsync(Email, Password, RememberMe);
                                 if (Response.Status == eAUTHORIZED_REQUEST_RESPONSE.SUCCESS)
                                 {
                                     if (!Response.Result.EmailVerified)
                                     {
                                         SignInErrorMessage = "Email Verification Required";
                                         ResendVerificationEmailCommandVisible = true;
                                     }
                                     else
                                     {
                                         AuthStateController.CurrentUserName = Email;
                                         AuthStateController.AuthenticationState = e_AUTH_STATE.WELCOME_NEW_USER;
                                         ShowEntryErrors = false;
                                     }
                                 }
                                 else if (Response.Status == eAUTHORIZED_REQUEST_RESPONSE.USER_EMAIL_NOT_VERIFIED)
                                 {
                                     SignInErrorMessage = Response.Message;
                                     ResendVerificationEmailCommandVisible = true;
                                     AuthStateController.AuthenticationState = e_AUTH_STATE.SIGN_IN_DEFAULT;
                                     ShowEntryErrors = true;
                                 }
                                 else
                                 {
                                     SignInErrorMessage = "Login error.";
                                     ResendVerificationEmailCommandVisible = false;
                                     AuthStateController.AuthenticationState = e_AUTH_STATE.SIGN_IN_DEFAULT;
                                     ShowEntryErrors = true;
                                 }
                                 ShowEntryErrors = true;

                             })).Wait();
                         });
                }
            }, () =>
            {
                return ValidateInputs();
            });

            _SignUpCommand = new DelegateCommand(() =>
            {
                Email = "";
                Password = "";
                SignInErrorMessage = "";
                ResendVerificationEmailCommandVisible = false;
                signedInUserViewModel.ClearCache();
                AuthStateController.AuthenticationState = e_AUTH_STATE.SIGN_UP;
            });

            _CancelSignUpCommand = new DelegateCommand(() =>
            {
                AuthStateController.AuthenticationState = e_AUTH_STATE.SIGN_UP;
            });
            View.DataContext = this;
        }

        private bool ValidateInputs()
        {
            EmailErrorString = null;
            PasswordErrorString = null;

            // ToDo: Validate form data
            if (_Email == null || _Email == "")
            {
                EmailErrorString = "Email Required";
            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(_Email);
                }
                catch (Exception)
                {
                    EmailErrorString = "Invalid Format";
                }
            }
            if (_Password == null)
            {
                PasswordErrorString = "Password Required";
            }
            else
            {
                try
                {
                    PasswordScore Score = PasswordAdvisor.CheckStrength(_Password);
                    if (Score == PasswordScore.Blank)
                    {
                        PasswordErrorString = "Password Required";
                    }
                }
                catch (Exception ex)
                {
                    PasswordErrorString = "Invalid Password";
                }
            }
            return (EmailErrorString == null && PasswordErrorString == null);
        }

        public bool Visible
        {
            get
            {
                return AuthStateController.AuthenticationState == e_AUTH_STATE.SIGN_IN_DEFAULT;
            }
        }

        private bool _ShowEntryErrors = false;
        public bool ShowEntryErrors
        {
            get { return _ShowEntryErrors; }
            set { SetProperty(ref _ShowEntryErrors, value); }
        }

        private bool _RememberMe;
        public bool RememberMe
        {
            get { return _RememberMe; }
            set
            {
                Properties.Settings.Default.RememberMe = value;
                Properties.Settings.Default.Save();
                SetProperty(ref _RememberMe, value);
            }
        }

        private string _Email = "admin@athleticssoftware.com";
        public string Email
        {
            get { return _Email; }
            set
            {
                SetProperty(ref _Email, value);
                ((DelegateCommand)_SignInWithAthleticCommand).RaiseCanExecuteChanged();
            }
        }

        private string _EmailErrorString;
        public string EmailErrorString
        {
            get { return _EmailErrorString; }
            set { SetProperty(ref _EmailErrorString, value); }
        }


        private string _Password = "pass@123";
        public string Password
        {
            get { return _Password; }
            set
            {
                SetProperty(ref _Password, value);
                ((DelegateCommand)_SignInWithAthleticCommand).RaiseCanExecuteChanged();
            }
        }

        private bool _ResendVerificationEmailCommandVisible = false;
        public bool ResendVerificationEmailCommandVisible
        {
            get { return _ResendVerificationEmailCommandVisible; }
            set { SetProperty(ref _ResendVerificationEmailCommandVisible, value); }
        }



        private string _PasswordErrorString;
        public string PasswordErrorString
        {
            get { return _PasswordErrorString; }
            set { SetProperty(ref _PasswordErrorString, value); }
        }


        private string _SignInErrorMessage = "";
        public string SignInErrorMessage
        {
            get { return _SignInErrorMessage; }
            set { SetProperty(ref _SignInErrorMessage, value); }
        }


        private ICommand _SignInWithFacebookCommand;
        public ICommand SignInWithFacebookCommand
        {
            get { return _SignInWithFacebookCommand; }
        }

        private ICommand _SignInWithGoogleCommand;
        public ICommand SignInWithGoogleCommand
        {
            get { return _SignInWithGoogleCommand; }
        }

        private ICommand _ForgotPasswordCommand;
        public ICommand ForgotPasswordCommand
        {
            get { return _ForgotPasswordCommand; }
        }

        private ICommand _SignInWithAthleticCommand;
        public ICommand SignInWithAthleticCommand
        {
            get { return _SignInWithAthleticCommand; }
        }

        private ICommand _SignUpCommand;
        public ICommand SignUpCommand
        {
            get { return _SignUpCommand; }
        }

        private ICommand _CancelSignUpCommand;
        public ICommand CancelSignUpCommand
        {
            get { return _CancelSignUpCommand; }
        }

    }
}
