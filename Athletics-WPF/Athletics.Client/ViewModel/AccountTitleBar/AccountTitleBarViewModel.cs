﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Athletics.Client.Controllers;
using Athletics.Client.Helpers;
using Athletics.Client.Interactions;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.Views;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Client.ViewModels
{

    public class AccountTitleBarViewModel : BindableBase, IAccountTitleBarViewModel
    {
        private readonly IAccountTitleBarView View;
        private readonly IApplicationAuthStateController AuthStateController;
        private readonly ISignedInUserViewModel _SignedInUserViewModel;
        private readonly ISignInManager SignInManager;
        private readonly IAuthenticatedShellViewModel _AuthenticatedShellViewModel;
        public IAuthenticatedShellViewModel AuthenticatedShellViewModel
        {
            get { return _AuthenticatedShellViewModel; }
        }

        public AccountTitleBarViewModel(IAccountTitleBarView View, IApplicationAuthStateController AuthStateController,  ISignedInUserViewModel SignedInUserViewModel, ISignInManager SignInManager, IAuthenticatedShellViewModel AuthenticatedShellViewModel)
        {
            this.View = View;
            this.AuthStateController = AuthStateController;
            this._SignedInUserViewModel = SignedInUserViewModel;
            this.SignInManager = SignInManager;
            this._AuthenticatedShellViewModel = AuthenticatedShellViewModel;

            _SignedInUserViewModel.SignedIn += AuthTokenCache_SignedIn;
            _SignedInUserViewModel.SignedOut += AuthTokenCache_SignedOut;

            AuthStateController.AuthorizationStateChanged += (x) =>
                {
                    if (AuthStateController.AuthenticationState == e_AUTH_STATE.LOGGED_IN)
                    {
                    }
                };

            _LogOutCommand = new DelegateCommand(async () =>
            {
                await SignInManager.SignOut();
                _SignedInUserViewModel.ClearCache();
                AuthStateController.AuthenticationState = e_AUTH_STATE.SIGN_IN_DEFAULT;
            });

            View.DataContext = this;
        }

        void AuthTokenCache_SignedOut()
        {
            Visible = false;
        }

        void AuthTokenCache_SignedIn()
        {
            //DisplayName = _SignInManager.AccountDetails.DisplayName;
            Visible = true;
        }


        public ISignedInUserViewModel SignedInUserViewModel
        {
            get { return _SignedInUserViewModel; }
        }

        private string _DisplayName;
        public string DisplayName
        {
            get { return _DisplayName; }
            set { SetProperty(ref _DisplayName, value); }
        }
        


        private ICommand _LogOutCommand;
        public ICommand LogOutCommand
        {
            get { return _LogOutCommand; }
        }

        private bool _Visible;
        public bool Visible
        {
            get { return _Visible; }
            set { SetProperty(ref _Visible, value); }
        }



        
    }
}
