﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Athletics.Client.Controllers;
using Athletics.Client.Models;
using Athletics.Client.Services.API;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.ValidationRules;
using Athletics.Client.Views;
using Athletics.Entities.Models;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using ApplicationMenu = Athletics.Entities.Models.ApplicationMenu;

namespace Athletics.Client.ViewModels
{
    public class MenuInRoleViewModel : BindableBase, IMenuInRoleViewModel
    {
        private IMenuInRoleView View = null;
        private IApplicationAuthStateController AuthStateController;
        private ISignInManager SignInManager;
        private readonly IApplicationUserController UserController;
        private IAuthorizedAPIServiceProvider AuthorizedAPIs;
        internal List<int> SelectedMenues;
        public MenuInRoleViewModel(IMenuInRoleView view, ISignInManager signInManager, IApplicationAuthStateController authController, IApplicationUserController userController, IAuthorizedAPIServiceProvider AuthorizedAPI)
        {
            View = view;
            SelectedMenues = new List<int>();
            this.AuthStateController = authController;
            this.SignInManager = signInManager;
            this.UserController = userController;
            this.AuthorizedAPIs = AuthorizedAPI;
            AuthenticatedShellViewModel.MakeVisibleView += AuthenticatedShellViewModel_MakeVisibleView;
            _AssignModuleCommand = new DelegateCommand(async () =>
            {
                if (!ValidateInputs())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            List<APIServiceRequest_AssignMenuInRole> assignedList = new List<APIServiceRequest_AssignMenuInRole>();
                            foreach (var item in SelectedMenues)
                            {
                                ApplicationMenu existedMenu = MenuListAsigned.Where(p => p.MenuID == item).FirstOrDefault();
                                if (!MenuListAsigned.Contains(existedMenu))
                                {
                                    APIServiceRequest_AssignMenuInRole newMenuInRoleInfo = new APIServiceRequest_AssignMenuInRole();
                                    newMenuInRoleInfo.RoleID = this.SelectedRole.RoleGuid;
                                    newMenuInRoleInfo.MenuID = item;
                                    assignedList.Add(newMenuInRoleInfo);
                                    ApplicationMenu menuToAdd = MenuListAll.Where(p => p.MenuID == item).FirstOrDefault();
                                    if (menuToAdd != null)
                                    {

                                        MenuListAsigned.Add(menuToAdd);
                                        ApplicationMenu menu = _MenuListAll.Where(p => p.MenuID == menuToAdd.MenuID).FirstOrDefault();
                                        if (menu != null)
                                        {
                                            _MenuListAll.Remove(menu);
                                        }
                                    }
                                }
                            }

                            AuthStateController.AuthenticationState = e_AUTH_STATE.ASSIGN_MENU;

                            await Task.Run(async () =>
                            {
                                AuthorizedResponse<AssignMenuInRoleResult> Response = await signInManager.AssignMenuToRole(assignedList);
                                Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    if (Response.Status == eAUTHORIZED_REQUEST_RESPONSE.SUCCESS)
                                    {
                                        AuthStateController.AuthenticationState = e_AUTH_STATE.ASSIGN_MENU_COMPLETE;
                                        // MenuListAsigned.Add(SelectedRole);
                                    }
                                    else
                                    {
                                        AuthStateController.AuthenticationState = e_AUTH_STATE.ASSIGN_MENU;
                                    }
                                }));

                            });

                        })).Wait();
                    });
                }
            }, () =>
            {
                return ValidateInputs();
            });
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {

                MenuListAll.Clear();
                RoleList.Clear();
                foreach (Role user in SignInManager.GetAllRole().Result)
                {
                    RoleList.Add(user);
                }
                foreach (Entities.Models.ApplicationMenu menu in signInManager.GetAllApplicationMenu().Result)
                {
                    MenuListAll.Add(menu);
                }

            }));

            _CancelAssignCommand = new DelegateCommand(async () =>
            {
                AuthStateController.AuthenticationState = e_AUTH_STATE.ASSIGN_MENU_COMPLETE;
            });
            View.DataContext = this;
        }
        private void AuthenticatedShellViewModel_MakeVisibleView(string Name)
        {
            if (Name == "AssignMenu")
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {

                    MenuListAll.Clear();
                    RoleList.Clear();
                    foreach (Role user in SignInManager.GetAllRole().Result)
                    {
                        RoleList.Add(user);
                    }
                    foreach (Entities.Models.ApplicationMenu menu in SignInManager.GetAllApplicationMenu().Result)
                    {
                        MenuListAll.Add(menu);
                    }

                }));
            }
        }
        private bool ValidateInputs()
        {
            if (SelectedRole != null && SelectedMenues.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private ICommand _AssignModuleCommand;
        public ICommand AssignModuleCommand
        {
            get { return _AssignModuleCommand; }
        }
        private ICommand _CancelAssignCommand;
        public ICommand CancelAssignCommand
        {
            get { return _CancelAssignCommand; }
        }
        private ObservableCollection<Role> _RoleList = new ObservableCollection<Role>();
        public ObservableCollection<Role> RoleList
        {
            get
            {
                return _RoleList;
            }
        }
        private bool _CheckAll = false;
        public bool CheckAll
        {
            get
            {
                return _CheckAll;

            }
            set
            {
                _CheckAll = value;
                CheckUnCheckAllItem(value);
            }
        }

        private void CheckUnCheckAllItem(bool value)
        {
            if (value)
            {
                //this.SelectedMenues.AddRange(this.MenuListAll.Select(p => p.MenuID));
                foreach (var item in View.AllMenus.Items)
                {
                    var container = View.AllMenus.ItemContainerGenerator.ContainerFromItem(item) as FrameworkElement;
                    if (container != null)
                    {
                        ContentPresenter queueListBoxItemCP = FindVisualChild<ContentPresenter>(container);
                        if (queueListBoxItemCP == null)
                            return;
                        CheckBox checkbox = queueListBoxItemCP.TemplatedParent as CheckBox;
                        if (checkbox != null)
                            checkbox.IsChecked = true;
                    }
                }

            }
            else
            {
                foreach (var item in View.AllMenus.Items)
                {
                    var container = View.AllMenus.ItemContainerGenerator.ContainerFromItem(item) as FrameworkElement;
                    if (container != null)
                    {
                        ContentPresenter queueListBoxItemCP = FindVisualChild<ContentPresenter>(container);
                        if (queueListBoxItemCP == null)
                            return;
                        CheckBox checkbox = queueListBoxItemCP.TemplatedParent as CheckBox;
                        if (checkbox != null)
                            checkbox.IsChecked = false;
                    }
                }
            }
        }
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj)
       where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        public static childItem FindVisualChild<childItem>(DependencyObject obj)
            where childItem : DependencyObject
        {
            foreach (childItem child in FindVisualChildren<childItem>(obj))
            {
                return child;
            }

            return null;
        }
        public IApplicationUserController ApplicationUserController
        {
            get { return UserController; }
        }


        private Role _SelectedRole;
        public Role SelectedRole
        {
            get { return _SelectedRole; }
            set
            {
                SetProperty(ref _SelectedRole, value);
                if (value == null) return;
                ((DelegateCommand)AssignModuleCommand).RaiseCanExecuteChanged();
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    MenuListAsigned.Clear();
                    foreach (Entities.Models.ApplicationMenu menu in SignInManager.GetAssignedApplicationMenu(value.RoleGuid).Result)
                    {
                        MenuListAsigned.Add(menu);
                    }
                    this.ResetMenuListAll();
                }));
            }
        }

        private void ResetMenuListAll()
        {
            MenuListAll.Clear();
            foreach (Entities.Models.ApplicationMenu menu in SignInManager.GetAllApplicationMenu().Result)
            {
                MenuListAll.Add(menu);
            }
            List<ApplicationMenu> list = MenuListAll.Where(item => !MenuListAsigned.Any(item2 => item2.MenuID == item.MenuID)).ToList();
            _MenuListAll.Clear();
            foreach (var item in list)
            {
                _MenuListAll.Add(item);
            }

        }

        private ObservableCollection<Entities.Models.ApplicationMenu> _MenuListAll = new ObservableCollection<Entities.Models.ApplicationMenu>();
        public ObservableCollection<Entities.Models.ApplicationMenu> MenuListAll
        {
            get { return _MenuListAll; }

        }

        private ObservableCollection<Entities.Models.ApplicationMenu> _MenuListAsigned = new ObservableCollection<Entities.Models.ApplicationMenu>();
        public ObservableCollection<Entities.Models.ApplicationMenu> MenuListAsigned
        {
            get { return _MenuListAsigned; }

        }


        private Entities.Models.ApplicationMenu _SelectedAssignedRole;
        public Entities.Models.ApplicationMenu SelectedAssignedRole
        {
            get { return _SelectedAssignedRole; }
            set
            {
                SetProperty(ref _SelectedAssignedRole, value);
            }
        }
    }
}
