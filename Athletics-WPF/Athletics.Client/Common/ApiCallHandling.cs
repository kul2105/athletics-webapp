﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;

public class LoginResponse
{
    public string access_token { get; set; }
    public string token_type { get; set; }
    public int expires_in { get; set; }
    public string userName { get; set; }
    public string issued { get; set; }
    public string expires { get; set; }
}

namespace Athletics.Client.Common
{

    public static class ApiCallHandling
    {
        public static T PostJson<T>(string _ApiPath, string ApiKey, object RequestObject)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_ApiPath);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Set("apiKey", ApiKey);
                T ServerResponse = default(T);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(RequestObject);
                    streamWriter.Write(json);
                }

                var httpResponse = GetResponseNoException(httpWebRequest);
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        string RxData = String.Empty;
                        RxData = streamReader.ReadToEnd();
                        if (!(string.IsNullOrEmpty(RxData)))
                        {
                            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                            ServerResponse = jsonSerializer.Deserialize<T>(RxData);
                        }
                        else
                        {
                            MessageBox.Show("No Data Received.");
                        }
                        return ServerResponse;
                    }
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                return default(T);
                //MessageBox.Show(ex.Message, MethodBase.GetCurrentMethod().Name);
                //return default(T);
            }
        }

        public static async Task<T> PostJsonAsync<T>(string _ApiPath, string ApiKey, object RequestObject)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_ApiPath);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Set("apiKey", ApiKey);
                T ServerResponse = default(T);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(RequestObject);
                    streamWriter.Write(json);
                }

                var httpResponse = await GetResponseNoExceptionAsync(httpWebRequest);
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        string RxData = String.Empty;
                        RxData = streamReader.ReadToEnd();
                        if (!(string.IsNullOrEmpty(RxData)))
                        {
                            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                            ServerResponse = jsonSerializer.Deserialize<T>(RxData);
                        }
                        else
                        {
                            MessageBox.Show("No Data Received.");
                        }
                        return ServerResponse;
                    }
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                return default(T);
                //MessageBox.Show(ex.Message, MethodBase.GetCurrentMethod().Name);
                //return default(T);
            }
        }

        public static T PostJsonBearerAuth<T>(string _ApiPath, string ApiKey, string AccessToken, object RequestObject)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_ApiPath);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Set("apiKey", ApiKey);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + AccessToken);
                T ServerResponse = default(T);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(RequestObject);
                    streamWriter.Write(json);
                }

                var httpResponse = GetResponseNoException(httpWebRequest);
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string RxData = String.Empty;
                    RxData = streamReader.ReadToEnd();
                    if (!(string.IsNullOrEmpty(RxData)))
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        ServerResponse = jsonSerializer.Deserialize<T>(RxData);
                    }
                    else
                    {
                        MessageBox.Show("No Data Received.");
                    }
                    return ServerResponse;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, MethodBase.GetCurrentMethod().Name);
                //return default(T);
                throw;
            }
        }

        public static async Task<T> PostJsonBearerAuthAsync<T>(string _ApiPath, string ApiKey, string AccessToken, object RequestObject)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_ApiPath);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Set("apiKey", ApiKey);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + AccessToken);
                T ServerResponse = default(T);

                if (RequestObject != null)
                {
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = new JavaScriptSerializer().Serialize(RequestObject);
                        streamWriter.Write(json);
                    }
                }

                var httpResponse = await GetResponseNoExceptionAsync(httpWebRequest);
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string RxData = String.Empty;
                    RxData = streamReader.ReadToEnd();
                    if (!(string.IsNullOrEmpty(RxData)))
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        ServerResponse = jsonSerializer.Deserialize<T>(RxData);
                    }
                    else
                    {
                        MessageBox.Show("No Data Received.");
                    }
                    return ServerResponse;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, MethodBase.GetCurrentMethod().Name);
                //return default(T);
                throw;
            }
        }


        public static async Task<T> GetJsonBearerAuthAsync<T>(string _ApiPath, string ApiKey, string AccessToken)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_ApiPath);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                httpWebRequest.Headers.Set("apiKey", ApiKey);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + AccessToken);
                T ServerResponse = default(T);

                //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                //{
                //    string json = new JavaScriptSerializer().Serialize(RequestObject);
                //    streamWriter.Write(json);
                //}

                var httpResponse = await GetResponseNoExceptionAsync(httpWebRequest);
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string RxData = String.Empty;
                    RxData = streamReader.ReadToEnd();
                    if (!(string.IsNullOrEmpty(RxData)))
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        ServerResponse = jsonSerializer.Deserialize<T>(RxData);
                    }
                    else
                    {
                        MessageBox.Show("No Data Received.");
                    }
                    return ServerResponse;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, MethodBase.GetCurrentMethod().Name);
                //return default(T);
                throw;
            }
        }

        internal static HttpWebResponse GetResponseNoException(HttpWebRequest req)
        {
            try
            {
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                HttpStatusCode statusCode = res.StatusCode;
                if (!(statusCode == HttpStatusCode.OK))
                {
                    MessageBox.Show("HttpStatusCode is Not OK");
                }
                return res;
            }
            catch (WebException we)
            {
                var resp = we.Response as HttpWebResponse;
                if (resp == null) MessageBox.Show(we.Message, MethodBase.GetCurrentMethod().Name);
                HttpStatusCode statusCode = resp.StatusCode;
                return resp;
            }
        }

        internal static async Task<HttpWebResponse> GetResponseNoExceptionAsync(HttpWebRequest req)
        {
            try
            {
                HttpWebResponse res = (HttpWebResponse)(await req.GetResponseAsync());
                HttpStatusCode statusCode = res.StatusCode;
                if (!(statusCode == HttpStatusCode.OK))
                {
                    MessageBox.Show("HttpStatusCode is Not OK");
                }
                return res;
            }
            catch (WebException we)
            {
                var resp = we.Response as HttpWebResponse;
                if (resp == null) MessageBox.Show(we.Message, MethodBase.GetCurrentMethod().Name);
                HttpStatusCode statusCode = resp.StatusCode;
                return resp;
            }
        }

        /*
        public static APIServiceResponse_DefaultSignIn PostLoginRequestCookieContainer(string api, string Username, string Password)
        {
            APIServiceResponse_DefaultSignIn DefaultError = new APIServiceResponse_DefaultSignIn()
            {
                Success = false,
                ResultMessage = null,
                Token = null,
            };

            try
            {
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(api);
                req.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705;)";
                req.Method = "POST";
                req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/
        /*;q=0.8";
req.Headers.Add("Accept-Language: en-us,en;q=0.5");
req.Headers.Add("Accept-Encoding: gzip,deflate");
req.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
req.KeepAlive = true;
req.Headers.Add("Keep-Alive: 300");
req.Referer = "api";

req.ContentType = "application/x-www-form-urlencoded";

StreamWriter sw = new StreamWriter(req.GetRequestStream());
sw.Write("grant_type=password&username=" + Username + "&password=" + Password);
sw.Close();

HttpWebResponse response = (HttpWebResponse)req.GetResponse();

if (response != null && response.StatusCode == HttpStatusCode.OK)
{
APIServiceResponse_DefaultSignIn Result = new APIServiceResponse_DefaultSignIn();
StreamReader reader = new StreamReader(response.GetResponseStream());
string tmp = reader.ReadToEnd();

JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
LoginResponse Response = jsonSerializer.Deserialize<LoginResponse>(tmp);
Result.ResultMessage = "OK";
Result.Success = true;
Result.Token = Response.access_token;
return Result;
}
}
catch (WebException webEx)
{
APIServiceResponse_DefaultSignIn Result = new APIServiceResponse_DefaultSignIn();
Result.Success = false;
Result.Token = null;

if (webEx.Message == "Unable to connect to the remote server")
{
Result.ResultMessage = "Unable to connect to the server.";
}
else if (webEx.Response == null)
{
Result.ResultMessage = "Login Error"; // Default message
}
else 
{
var Reason = webEx.Response.Headers["TokenErrorReason"];
if (Reason.Length > 23 && Reason.Substring(0, 22) == "PasswordResetRequired:")
{
Result.ResultMessage = "A password reset is required.";
Result.Token = Reason.Substring(22, Reason.Length - 22);
}
else
{
switch (Reason)
{
case "BadCredentials":
Result.ResultMessage = "Invalid credentials.";
break;
case "PasswordResetRequired":
Result.ResultMessage = "A password reset is required.";
break;
case "EmailNotConfirmed":
Result.ResultMessage = "Email address has not been confirmed.";
break;
default:
Result.ResultMessage = "Login Error"; // Default message
break;
}
}
}
return Result;
}
catch (Exception ex)
{
}
return DefaultError;
}
*/
    }
}
