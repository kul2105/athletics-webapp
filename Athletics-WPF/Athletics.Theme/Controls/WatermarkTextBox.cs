﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Athletics.Theme.Controls
{
    // http://putridparrot.com/blog/basics-of-extending-a-wpf-control/
    public class WatermarkTextBox : TextBox
    {
        static WatermarkTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WatermarkTextBox),
                new FrameworkPropertyMetadata(typeof(WatermarkTextBox)));
        }

        static void TextPropertyChanged(DependencyObject sender,
                  DependencyPropertyChangedEventArgs args)
        {
            WatermarkTextBox watermarkTextBox = (WatermarkTextBox)sender;

            bool textExists = watermarkTextBox.Text.Length > 0;
            if (textExists != watermarkTextBox.RemoveWatermark)
            {
                watermarkTextBox.SetValue(RemoveWatermarkPropertyKey, textExists);
                watermarkTextBox.UpdateWatermarkVisualState();
            }
        }

        public WatermarkTextBox()
            : base()
        {
            LostFocus += WatermarkTextBox_FocusChanged;
            GotFocus += WatermarkTextBox_GotFocus;
            TextChanged += WatermarkTextBox_FocusChanged;
            IsVisibleChanged += WatermarkTextBox_IsVisibleChanged;
        }

        void WatermarkTextBox_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UpdateWatermarkVisualState();
        }

        void WatermarkTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll();
            UpdateWatermarkVisualState();
        }

        void WatermarkTextBox_FocusChanged(object sender, RoutedEventArgs e)
        {
            UpdateWatermarkVisualState();
        }

        protected virtual void UpdateWatermarkVisualState()
        {
            try
            {
                if (Text.Length > 0 || IsFocused)
                {
                    VisualStateManager.GoToState(this, "WatermarkContracted", true);
                }
                else
                {
                    VisualStateManager.GoToState(this, "WatermarkExpanded", true);
                }
            }
            catch (Exception ex)
            {
                Logger.LoggerManager.LogError(ex.Message);
            }
        }

        public static readonly DependencyProperty WatermarkProperty =
         DependencyProperty.Register("Watermark", typeof(string),
                      typeof(WatermarkTextBox),
              new FrameworkPropertyMetadata("", WatermarkPropertyChanged));

        public string Watermark
        {
            get { return (string)GetValue(WatermarkProperty); }
            set 
            {
                SetValue(WatermarkProperty, value);
                UpdateWatermarkVisualState();
            }
        }

        private static void WatermarkPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            WatermarkTextBox watermarkTextBox = (WatermarkTextBox)sender;
            watermarkTextBox.UpdateWatermarkVisualState();
        }


        private static readonly DependencyPropertyKey RemoveWatermarkPropertyKey =
            DependencyProperty.RegisterReadOnly("RemoveWatermark", typeof(bool),
                typeof(WatermarkTextBox),
                new FrameworkPropertyMetadata((bool)false));

        public static readonly DependencyProperty RemoveWatermarkProperty =
                    RemoveWatermarkPropertyKey.DependencyProperty;

        public bool RemoveWatermark
        {
            get { return (bool)GetValue(RemoveWatermarkProperty); }
        }
    }
}
