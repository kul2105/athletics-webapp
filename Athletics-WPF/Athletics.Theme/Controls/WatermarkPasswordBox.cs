﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Athletics.Theme.Controls
{
    [TemplatePart(Name = "PART_PasswordBox", Type = typeof(PasswordBox))]
    public class WatermarkPasswordBox : WatermarkTextBox
    {
        private bool IsUpdatingTextFromPasswordControl = false;
        private bool IsUpdatingTextFromViewModel = false;

        public WatermarkPasswordBox()
        {
        }

        static WatermarkPasswordBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WatermarkPasswordBox),
                new FrameworkPropertyMetadata(typeof(WatermarkPasswordBox)));
        }


        private PasswordBox _PasswordBox;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _PasswordBox = (PasswordBox)Template.FindName("PART_PasswordBox", this);

            ScrollViewer contentHost = (ScrollViewer)Template.FindName("PART_ContentHost", this);
            if (contentHost != null)
            {
                contentHost.Visibility = Visibility.Collapsed;
            }

            if (_PasswordBox != null)
            {
                _PasswordBox.PasswordChanged += _PasswordBox_PasswordChanged;
                TextChanged += WatermarkPasswordBox_TextChanged;
                _PasswordBox.TabIndex = TabIndex;
                _PasswordBox.GotFocus += _PasswordBox_GotFocus;
                this.Focusable = false;
            }

        }

        void WatermarkPasswordBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsUpdatingTextFromPasswordControl)
            {
                // Do nothing, the view model will automatically get updated
            }
            else
            {
                // The view model must have changed independently, update the passwrod
                IsUpdatingTextFromViewModel = true;
                _PasswordBox.Password = Text;
                IsUpdatingTextFromViewModel = false;
            }
            UpdateWatermarkVisualState();
        }

        void _PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (!IsUpdatingTextFromViewModel)
            {
                // The password changed because of user typing.
                // Update the Text
                IsUpdatingTextFromPasswordControl = true;
                Text = _PasswordBox.Password;
                IsUpdatingTextFromPasswordControl = false;
            }
            else
            {
                // Do nothing, the PasswordBox is getting updated from the Text/view model
            }
            UpdateWatermarkVisualState();
        }

        protected override void UpdateWatermarkVisualState()
        {
            try
            {
                if (_PasswordBox != null)
                {
                    if (_PasswordBox.Password.Length > 0 || _PasswordBox.IsFocused)
                    {
                        VisualStateManager.GoToState(this, "WatermarkContracted", true);
                    }
                    else
                    {
                        VisualStateManager.GoToState(this, "WatermarkExpanded", true);
                    }
                }
                else
                {
                    VisualStateManager.GoToState(this, "WatermarkExpanded", true);
                }
            }
            catch (Exception ex)
            {
                Logger.LoggerManager.LogError(ex.Message);
            }
        }

        void _PasswordBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((PasswordBox)sender).SelectAll();
        }


    }
}
