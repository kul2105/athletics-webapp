﻿using System;
using System.Windows.Data;

namespace Athletics.Theme.Converters
{
    public class ConfirmPasswordMultivalueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parm, System.Globalization.CultureInfo culture)
        {
            string Password = (values[0] as string);
            string ConfirmPassword = (values[1] as string);

            if (Password == null || ConfirmPassword == null || !Password.Equals(ConfirmPassword))
            {
                return null;
            }
            return "OK";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parm, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
