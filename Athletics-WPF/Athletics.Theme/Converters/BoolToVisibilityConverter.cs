﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Athletics.Theme.Converters
{
    public class BoolToVisibilityConverter : DependencyObject, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((bool)value) ? Visibility.Visible : (CollapseIfFalse ? Visibility.Collapsed : Visibility.Hidden);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }



        public bool CollapseIfFalse
        {
            get { return (bool)GetValue(CollapseIfFalseProperty); }
            set { SetValue(CollapseIfFalseProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CollapseIfFalse.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CollapseIfFalseProperty =
            DependencyProperty.Register("CollapseIfFalse", typeof(bool), typeof(BoolToVisibilityConverter), new PropertyMetadata(false));


    }
}
