﻿#pragma checksum "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "B44FD2848FE0C9AFDCF7A6A0E78ADCEA10FEE0B5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Athletics.Athletes.Model;
using Athletics.ControlLibrary.AthleticImageButton;
using Athletics.ControlLibrary.DataGrid;
using Microsoft.Practices.Prism.Interactivity;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Regions.Behaviors;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Athletics.Athletes.Views {
    
    
    /// <summary>
    /// ManageAthletesView
    /// </summary>
    public partial class ManageAthletesView : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 9 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Athletics.Athletes.Views.ManageAthletesView ManageAthlete;
        
        #line default
        #line hidden
        
        
        #line 248 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Athletics.ControlLibrary.DataGrid.AthleticsGrid dataGrid1;
        
        #line default
        #line hidden
        
        
        #line 274 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Athletics.ControlLibrary.DataGrid.AthleticsGrid elegibalEvent;
        
        #line default
        #line hidden
        
        
        #line 281 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Athletics.ControlLibrary.DataGrid.AthleticsGrid Entries;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Athletics.Athletes;component/view/managerelay/manageathletesview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ManageAthlete = ((Athletics.Athletes.Views.ManageAthletesView)(target));
            return;
            case 6:
            this.dataGrid1 = ((Athletics.ControlLibrary.DataGrid.AthleticsGrid)(target));
            return;
            case 7:
            this.elegibalEvent = ((Athletics.ControlLibrary.DataGrid.AthleticsGrid)(target));
            return;
            case 8:
            this.Entries = ((Athletics.ControlLibrary.DataGrid.AthleticsGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 2:
            
            #line 91 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
            ((System.Windows.Controls.Border)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.PART_TITLEBAR_MouseLeftButtonDown);
            
            #line default
            #line hidden
            break;
            case 3:
            
            #line 100 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.PART_CLOSE_Click);
            
            #line default
            #line hidden
            break;
            case 4:
            
            #line 107 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.PART_MAXIMIZE_RESTORE_Click);
            
            #line default
            #line hidden
            break;
            case 5:
            
            #line 115 "..\..\..\..\View\ManageRelay\ManageAthletesView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.PART_MINIMIZE_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

