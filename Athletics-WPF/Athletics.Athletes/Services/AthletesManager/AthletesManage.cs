﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Athletics.Athletes.Services.API.Athleties;
using Athletics.Athletes.ViewModels;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using Microsoft.Practices.Prism.Mvvm;

namespace Athletics.Athletes.AthleticsManager
{
    public class AthletesManager : BindableBase, IAthletesManager
    {
        public async Task<AuthorizedResponse<AthletiesResult>> CreateNewAthleties(ServiceRequest_Athleties AthletiesRequest)
        {
            try
            {
                int InsertedId = 0;
                if (AthletiesRequest.Athleties != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        Athlete athlete = new Athlete();
                        athlete.Age = AthletiesRequest.Athleties.Age;
                        if (AthletiesRequest.Athleties.SelectedCitizen != null)
                        {
                            athlete.CitizenID = AthletiesRequest.Athleties.SelectedCitizen.BaseCountyID;
                        }
                        athlete.City = AthletiesRequest.Athleties.City;
                        athlete.ClassYR = AthletiesRequest.Athleties.ClassYR;
                        athlete.CompetitorNumber = AthletiesRequest.Athleties.CompetitorNumber;
                        athlete.DateOfBirth = AthletiesRequest.Athleties.DateOfBirth;
                        athlete.FirstName = AthletiesRequest.Athleties.FirstName;
                        athlete.Gender = AthletiesRequest.Athleties.Gender;
                        athlete.LastName = AthletiesRequest.Athleties.LastName;
                        athlete.MI = AthletiesRequest.Athleties.MI;
                        athlete.RegistrationNumber = AthletiesRequest.Athleties.RegistrationNumber;
                        if (AthletiesRequest.Athleties.SelectedSchool != null)
                        {
                            athlete.SchoolID = AthletiesRequest.Athleties.SelectedSchool.SchoolID;
                        }
                        athlete.State = AthletiesRequest.Athleties.State;
                        athlete.Status = AthletiesRequest.Athleties.Status;
                        if (AthletiesRequest.Athleties.Team != null)
                        {
                            athlete.TeamID = AthletiesRequest.Athleties.Team.TeamID;
                        }
                        ctx.Athletes.Add(athlete);
                        ctx.SaveChanges();
                        InsertedId = athlete.AthletesID;
                    }
                }

                AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new AthletiesResult();
                Result.Result.AthletiesID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new AthletiesResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<AthletiesResult>> DeleteAthleties(int AthletiesID)
        {
            AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<Athlete> eventDetailList = ctx.Athletes.Where(p => p.AthletesID == AthletiesID).ToList();
                        if (eventDetailList != null)
                        {
                            ctx.Athletes.RemoveRange(eventDetailList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new AthletiesResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new AthletiesResult();
            Result.Result.AthletiesID = AthletiesID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<List<AddAthletesViewModel>> GetAllAthleties()
        {
            List<AddAthletesViewModel> athleticsList = new List<AddAthletesViewModel>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                foreach (Athlete athlete in ctx.Athletes)
                {
                    AddAthletesViewModel eventModel = new AddAthletesViewModel();
                    eventModel.Age = athlete.Age == null ? 0 : athlete.Age.Value;
                    eventModel.AthletesID = athlete.AthletesID;
                    if (athlete.CitizenID != null)
                    {
                        eventModel.SelectedCitizen = this.GetCounty(athlete.CitizenID);
                    }
                    if (athlete.SchoolID != null)
                    {
                        eventModel.SelectedSchool = this.GetSchool(athlete.SchoolID);
                    }
                    eventModel.City = athlete.City;
                    eventModel.ClassYR = athlete.ClassYR;
                    eventModel.CompetitorNumber = athlete.CompetitorNumber;
                    eventModel.DateOfBirth = athlete.DateOfBirth == null ? DateTime.Now : athlete.DateOfBirth.Value;
                    eventModel.FirstName = athlete.FirstName;
                    eventModel.Gender = athlete.Gender;
                    eventModel.LastName = athlete.LastName;
                    eventModel.MI = athlete.MI;
                    eventModel.State = athlete.State;
                    eventModel.Status = athlete.Status;
                    eventModel.RegistrationNumber = athlete.RegistrationNumber;
                    eventModel.Mode = AthleteMode.Edit;
                    if (athlete.TeamID != null)
                    {
                        eventModel.Team = this.GetTeam(athlete.TeamID);
                    }
                    eventModel.Age = athlete.Age == null ? 0 : athlete.Age.Value;
                    eventModel.Age = athlete.Age == null ? 0 : athlete.Age.Value;
                    athleticsList.Add(eventModel);
                }
            }
            return athleticsList;
        }

        private Team GetTeam(int? teamID)
        {
            if (teamID == null) return null;
            using (AthletesContainer ctx = new AthletesContainer())
            {
                foreach (Team team in ctx.Teams)
                {
                    if (team.TeamID == teamID)
                        return team;
                }
            }
            return null;
        }

        private SchoolDetail GetSchool(int? schoolID)
        {
            if (schoolID == null) return null;
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (SchoolDetail school in ctx.SchoolDetails)
                {
                    if (school.SchoolID == schoolID)
                        return school;
                }
            }
            return null;
        }

        private BaseCounty GetCounty(int? citizenID)
        {
            if (citizenID == null) return null;
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (BaseCounty school in ctx.BaseCounties)
                {
                    if (school.BaseCountyID == citizenID)
                        return school;
                }
            }
            return null;
        }

        public async Task<AuthorizedResponse<AthletiesResult>> UpdateAthleties(ServiceRequest_Athleties AthletiesRequest)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                Athlete athlete = ctx.Athletes.Where(p => p.AthletesID == AthletiesRequest.Athleties.AthletesID).FirstOrDefault();
                if (athlete != null)
                {
                    athlete.Age = AthletiesRequest.Athleties.Age;
                    if (AthletiesRequest.Athleties.SelectedCitizen != null)
                    {
                        athlete.CitizenID = AthletiesRequest.Athleties.SelectedCitizen.BaseCountyID;
                    }
                    athlete.City = AthletiesRequest.Athleties.City;
                    athlete.ClassYR = AthletiesRequest.Athleties.ClassYR;
                    athlete.CompetitorNumber = AthletiesRequest.Athleties.CompetitorNumber;
                    athlete.DateOfBirth = AthletiesRequest.Athleties.DateOfBirth;
                    athlete.FirstName = AthletiesRequest.Athleties.FirstName;
                    athlete.Gender = AthletiesRequest.Athleties.Gender;
                    athlete.LastName = AthletiesRequest.Athleties.LastName;
                    athlete.MI = AthletiesRequest.Athleties.MI;
                    athlete.RegistrationNumber = AthletiesRequest.Athleties.RegistrationNumber;
                    if (AthletiesRequest.Athleties.SelectedSchool != null)
                    {
                        athlete.SchoolID = AthletiesRequest.Athleties.SelectedSchool.SchoolID;
                    }
                    athlete.State = AthletiesRequest.Athleties.State;
                    athlete.Status = AthletiesRequest.Athleties.Status;
                    if (AthletiesRequest.Athleties.Team != null)
                    {
                        athlete.TeamID = AthletiesRequest.Athleties.Team.TeamID;
                    }
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new AthletiesResult();
            Result.Result.AthletiesID = AthletiesRequest.Athleties.AthletesID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public List<BaseCounty> GetAllCountry()
        {
            List<BaseCounty> BaseCountyList = new List<BaseCounty>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (BaseCounty county in ctx.BaseCounties)
                {
                    BaseCountyList.Add(county);
                }
            }
            return BaseCountyList;
        }

        public List<State> GetStateByCountry(int CountryID)
        {
            List<State> StateList = new List<State>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (State state in ctx.States.Where(p => p.CountyID == CountryID))
                {
                    StateList.Add(state);
                }
            }
            return StateList;
        }

        public async Task<List<Team>> GetAllTeams()
        {
            List<Team> TeamList = new List<Team>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                foreach (Team team in ctx.Teams)
                {
                    TeamList.Add(team);
                }
            }
            return TeamList;
        }

        public List<SchoolDetail> GetAllSchoolList()
        {
            List<SchoolDetail> ListofSchool = new List<SchoolDetail>();
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                foreach (SchoolDetail school in ctx.SchoolDetails)
                {
                    ListofSchool.Add(school);
                }
            }
            return ListofSchool;
        }

        public async Task<AuthorizedResponse<RelayResult>> CreateNewRelay(ServiceRequest_Relay RelayRequest)
        {
            try
            {
                int InsertedId = 0;
                if (RelayRequest.Relay != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        Relay relay = new Relay();
                        relay.RelayAbbr = RelayRequest.Relay.RelayAbb;
                        relay.RelayName = RelayRequest.Relay.RelayName;
                        relay.ConvTime = RelayRequest.Relay.ConvTime;
                        relay.Dec = RelayRequest.Relay.Dec;
                        relay.EndAgeRange = RelayRequest.Relay.EndAgeRange;
                        relay.EntryMark = RelayRequest.Relay.EntryMark;
                        relay.EntryNote = RelayRequest.Relay.EntryNote;
                        relay.EventID = RelayRequest.Relay.EventID;
                        relay.EventName = RelayRequest.Relay.EventName;
                        relay.EventNumber = RelayRequest.Relay.EventNumber;
                        relay.Gender = RelayRequest.Relay.Gender;
                        relay.HeatLane = RelayRequest.Relay.HeatLane;
                        relay.IsAdded = true;
                        relay.IsAlt = RelayRequest.Relay.IsAlt;
                        relay.IsExh = RelayRequest.Relay.IsExh;
                        relay.IsSRC = RelayRequest.Relay.IsSRC;
                        relay.RelayAbbr = RelayRequest.Relay.RelayAbb;
                        relay.RelayName = RelayRequest.Relay.RelayName;
                        relay.StartAgeRange = RelayRequest.Relay.StartAgeRange;
                        relay.Status = RelayRequest.Relay.Status;
                        ctx.Relays.Add(relay);
                        ctx.SaveChanges();
                        InsertedId = relay.RelayID;
                    }
                }

                AuthorizedResponse<RelayResult> Result = new AuthorizedResponse<RelayResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new RelayResult();
                Result.Result.RelayID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<RelayResult> Result = new AuthorizedResponse<RelayResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new RelayResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<RelayResult>> UpdateRelay(ServiceRequest_Relay RelayRequest)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                Relay relay = ctx.Relays.Where(p => p.RelayID == RelayRequest.Relay.RelayID).FirstOrDefault();
                if (relay != null)
                {
                    relay.RelayName = RelayRequest.Relay.RelayName;
                    relay.RelayAbbr = RelayRequest.Relay.RelayAbb;
                    relay.ConvTime = RelayRequest.Relay.ConvTime;
                    relay.Dec = RelayRequest.Relay.Dec;
                    relay.EndAgeRange = RelayRequest.Relay.EndAgeRange;
                    relay.EntryMark = RelayRequest.Relay.EntryMark;
                    relay.EntryNote = RelayRequest.Relay.EntryNote;
                    relay.EventID = RelayRequest.Relay.EventID;
                    relay.EventName = RelayRequest.Relay.EventName;
                    relay.EventNumber = RelayRequest.Relay.EventNumber;
                    relay.Gender = RelayRequest.Relay.Gender;
                    relay.HeatLane = RelayRequest.Relay.HeatLane;
                    relay.IsAdded = true;
                    relay.IsAlt = RelayRequest.Relay.IsAlt;
                    relay.IsExh = RelayRequest.Relay.IsExh;
                    relay.IsSRC = RelayRequest.Relay.IsSRC;
                    relay.RelayAbbr = RelayRequest.Relay.RelayAbb;
                    relay.RelayName = RelayRequest.Relay.RelayName;
                    relay.StartAgeRange = RelayRequest.Relay.StartAgeRange;
                    relay.Status = RelayRequest.Relay.Status;
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<RelayResult> Result = new AuthorizedResponse<RelayResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RelayResult();
            Result.Result.RelayID = RelayRequest.Relay.RelayID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<RelayResult>> DeleteRelay(int RelayID)
        {
            AuthorizedResponse<RelayResult> Result = new AuthorizedResponse<RelayResult>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<Relay> eventDetailList = ctx.Relays.Where(p => p.RelayID == RelayID).ToList();
                        if (eventDetailList != null)
                        {
                            ctx.Relays.RemoveRange(eventDetailList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new RelayResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RelayResult();
            Result.Result.RelayID = RelayID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<List<AddRelayViewModel>> GetAllRelay()
        {
            List<AddRelayViewModel> RelayList = new List<AddRelayViewModel>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                foreach (Relay relayObj in ctx.Relays)
                {
                    AddRelayViewModel relay = new AddRelayViewModel();
                    relay.RelayID = relayObj.RelayID;
                    relay.RelayName = relayObj.RelayName;
                    relay.RelayAbb = relayObj.RelayAbbr;
                    relay.ConvTime = relayObj.ConvTime == null ? 0 : relayObj.ConvTime.Value;
                    relay.Dec = relayObj.Dec;
                    relay.EndAgeRange = relayObj.EndAgeRange == null ? 0 : relayObj.EndAgeRange.Value;
                    relay.EntryMark = relayObj.EntryMark;
                    relay.EntryNote = relayObj.EntryNote;
                    relay.EventID = relayObj.EventID == null ? 0 : relayObj.EventID.Value;
                    relay.EventName = relayObj.EventName;
                    relay.EventNumber = relayObj.EventNumber;
                    relay.Gender = relayObj.Gender;
                    relay.HeatLane = relayObj.HeatLane;
                    relay.IsAlt = relayObj.IsAlt == null ? false : relayObj.IsAlt.Value;
                    relay.IsExh = relayObj.IsExh == null ? false : relayObj.IsExh.Value;
                    relay.IsSRC = relayObj.IsSRC == null ? false : relayObj.IsSRC.Value;
                    relay.RelayAbb = relayObj.RelayAbbr;
                    relay.RelayName = relayObj.RelayName;
                    relay.StartAgeRange = relayObj.StartAgeRange == null ? 0 : relayObj.StartAgeRange.Value;
                    relay.Status = relayObj.Status;
                    RelayList.Add(relay);
                }
            }
            return RelayList;
        }

        public async Task<AuthorizedResponse<TeamResult>> CreateNewTeam(ServiceRequest_Team TeamRequest)
        {
            try
            {
                int InsertedId = 0;
                if (TeamRequest.Team != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        Team team = new Team();
                        team.Address1 = TeamRequest.Team.Address1;
                        team.State = TeamRequest.Team.State;
                        team.City = TeamRequest.Team.City;
                        team.Address2 = TeamRequest.Team.Address2;
                        team.Association = TeamRequest.Team.Association;
                        team.Division = TeamRequest.Team.Division;
                        team.FullTeamName = TeamRequest.Team.FullTeamName;
                        team.HeadCoachFirstName = TeamRequest.Team.HeadCoachFirstName;
                        team.HeadCoachLastName = TeamRequest.Team.HeadCoachLastName;
                        team.IsUnAttached = TeamRequest.Team.IsUnAttached;
                        team.Office = TeamRequest.Team.Office;
                        team.Province = TeamRequest.Team.Province;
                        team.ShortTeamName = TeamRequest.Team.ShortTeamName;
                        team.TeamAbbr = TeamRequest.Team.TeamAbbr;
                        team.TeamName = TeamRequest.Team.TeamName;
                        team.Zip = TeamRequest.Team.Zip;
                        team.Region = TeamRequest.Team.Region;
                        team.Status = TeamRequest.Team.Status;
                        team.Home = TeamRequest.Team.Home;
                        team.Email = TeamRequest.Team.Email;
                        team.AltTeamAbb = TeamRequest.Team.AltTeamAbb;
                        team.Country = TeamRequest.Team.CountryString;
                        ctx.Teams.Add(team);
                        ctx.SaveChanges();
                        InsertedId = team.TeamID;
                    }
                }

                AuthorizedResponse<TeamResult> Result = new AuthorizedResponse<TeamResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new TeamResult();
                Result.Result.TeamID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<TeamResult> Result = new AuthorizedResponse<TeamResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new TeamResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<TeamResult>> UpdateTeam(ServiceRequest_Team TeamRequest)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                Team team = ctx.Teams.Where(p => p.TeamID == TeamRequest.Team.TeamID).FirstOrDefault();
                if (team != null)
                {
                    team.Address1 = TeamRequest.Team.Address1;
                    team.State = TeamRequest.Team.State;
                    team.City = TeamRequest.Team.City;
                    team.Address2 = TeamRequest.Team.Address2;
                    team.Association = TeamRequest.Team.Association;
                    team.Division = TeamRequest.Team.Division;
                    team.FullTeamName = TeamRequest.Team.FullTeamName;
                    team.HeadCoachFirstName = TeamRequest.Team.HeadCoachFirstName;
                    team.HeadCoachLastName = TeamRequest.Team.HeadCoachLastName;
                    team.IsUnAttached = TeamRequest.Team.IsUnAttached;
                    team.Office = TeamRequest.Team.Office;
                    team.Province = TeamRequest.Team.Province;
                    team.ShortTeamName = TeamRequest.Team.ShortTeamName;
                    team.TeamAbbr = TeamRequest.Team.TeamAbbr;
                    team.TeamName = TeamRequest.Team.TeamName;
                    team.Zip = TeamRequest.Team.Zip;
                    team.Region = TeamRequest.Team.Region;
                    team.Status = TeamRequest.Team.Status;
                    team.Home = TeamRequest.Team.Home;
                    team.Email = TeamRequest.Team.Email;
                    team.AltTeamAbb = TeamRequest.Team.AltTeamAbb;
                    team.Country = TeamRequest.Team.CountryString;
                    if (TeamRequest.Team.Team != null)
                    {
                        team.TeamID = TeamRequest.Team.Team.TeamID;
                    }
                }
                ctx.SaveChanges();
            }

            AuthorizedResponse<TeamResult> Result = new AuthorizedResponse<TeamResult>();
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new TeamResult();
            Result.Result.TeamID = TeamRequest.Team.TeamID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<TeamResult>> DeleteTeam(int TeamID)
        {
            AuthorizedResponse<TeamResult> Result = new AuthorizedResponse<TeamResult>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<Team> eventDetailList = ctx.Teams.Where(p => p.TeamID == TeamID).ToList();
                        if (eventDetailList != null)
                        {
                            ctx.Teams.RemoveRange(eventDetailList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new TeamResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new TeamResult();
            Result.Result.TeamID = TeamID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<List<Entities.Models.EventDetail>> GetAllEventDetail()
        {
            List<Entities.Models.EventDetail> EventDetailList = new List<Entities.Models.EventDetail>();
            using (EventEntities ctx = new EventEntities())
            {
                foreach (Entities.Models.EventDetail team in ctx.EventDetails)
                {
                    EventDetailList.Add(team);
                }
            }
            return EventDetailList;
        }

        public async Task<List<AthletiesElegible>> GetAllElegibleEvents(int athleteID)
        {
            List<AthletiesElegible> eligibleList = new List<AthletiesElegible>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                foreach (AthletiesElegible eligibleObj in ctx.AthletiesElegibles.Where(p => p.AthletesID == athleteID))
                {
                    AthletiesElegible eligible = new AthletiesElegible();
                    eligible.AthletesID = eligibleObj.AthletesID;
                    eligible.AthletiesElegibleEventID = eligibleObj.AthletiesElegibleEventID;
                    eligible.ConvTime = eligibleObj.ConvTime;
                    eligible.Dec = eligibleObj.Dec;
                    eligible.EntryNote = eligibleObj.EntryNote;
                    eligible.EntryMark = eligibleObj.EntryMark;
                    eligible.EventID = eligibleObj.EventID;
                    eligible.HeatLane = eligibleObj.HeatLane;
                    eligible.IsAdded = eligibleObj.IsAdded;
                    eligible.IsAlt = eligibleObj.IsAlt;
                    eligible.IsExh = eligibleObj.IsExh;
                    eligible.IsSRC = eligibleObj.IsSRC;
                    eligible.Status = eligibleObj.Status;
                    eligible.EventName = eligibleObj.EventName;
                    eligible.EventNumber = eligibleObj.EventNumber;
                    eligibleList.Add(eligible);
                }
            }
            return eligibleList;
        }

        public async Task<AuthorizedResponse<AthletiesResult>> CreateNewElegibleAthleties(ServiceRequest_AthletiesElegible AthletiesRequest)
        {
            try
            {
                int InsertedId = 0;
                if (AthletiesRequest.Elegible != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        AthletiesElegible eligible = new AthletiesElegible();
                        eligible.AthletesID = AthletiesRequest.Elegible.AthletesID;
                        eligible.ConvTime = AthletiesRequest.Elegible.ConvTime;
                        eligible.Dec = AthletiesRequest.Elegible.Dec;
                        eligible.EntryNote = AthletiesRequest.Elegible.EntryNote;
                        eligible.EntryMark = AthletiesRequest.Elegible.EntryMark;
                        eligible.EventID = AthletiesRequest.Elegible.EventID;
                        eligible.HeatLane = AthletiesRequest.Elegible.HeatLane;
                        eligible.IsAdded = true;
                        eligible.IsAlt = AthletiesRequest.Elegible.IsAlt;
                        eligible.IsExh = AthletiesRequest.Elegible.IsExh;
                        eligible.IsSRC = AthletiesRequest.Elegible.IsSRC;
                        eligible.Status = AthletiesRequest.Elegible.Status;
                        eligible.EventName = AthletiesRequest.Elegible.EventName;
                        eligible.EventNumber = AthletiesRequest.Elegible.EventNumber;
                        ctx.AthletiesElegibles.Add(eligible);

                        ctx.SaveChanges();

                        ctx.SaveChanges();
                        InsertedId = eligible.AthletiesElegibleEventID;
                    }
                }

                AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new AthletiesResult();
                Result.Result.AthletiesID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new AthletiesResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<List<AthletiesElegible>> GetAllElegibleEvents()
        {
            List<AthletiesElegible> EventDetailList = new List<AthletiesElegible>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                foreach (AthletiesElegible elegible in ctx.AthletiesElegibles)
                {
                    EventDetailList.Add(elegible);
                }
            }
            return EventDetailList;
        }

        public async Task<AuthorizedResponse<RelayResult>> AddTeamRelay(RelayEntryDetail entryDetail)
        {
            try
            {
                int InsertedId = 0;
                if (entryDetail != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        ctx.RelayEntryDetails.Add(entryDetail);
                        ctx.SaveChanges();
                        InsertedId = entryDetail.RelayEntryID;
                    }
                }

                AuthorizedResponse<RelayResult> Result = new AuthorizedResponse<RelayResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new RelayResult();
                Result.Result.RelayID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<RelayResult> Result = new AuthorizedResponse<RelayResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new RelayResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<List<RelayEntryDetail>> GetAllRelayTeam(int RelayID)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.RelayEntryDetails.Where(p => p.RelayID == RelayID).ToList();

            }
        }

        public async Task<List<RelayPrelimOrder>> GetRelayOrderPreLim(int RelayID)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.RelayPrelimOrders.Where(p => p.RelayID == RelayID).ToList();

            }
        }

        public async Task<List<RelayFinalOrder>> GetRelayOrderPreFinal(int RelayID)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.RelayFinalOrders.Where(p => p.RelayID == RelayID).ToList();

            }
        }

        public async Task<AuthorizedResponse<PrelimFinResult>> CopyPrelimToFinal(ServiceRequest_PrelimFin prelimOrderService)
        {
            try
            {
                int InsertedId = 0;
                if (prelimOrderService != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        List<RelayPrelimOrder> prelimAthleteExisted = ctx.RelayPrelimOrders.Where(p => p.RelayID == prelimOrderService.RelayID).ToList();
                        if (prelimAthleteExisted != null)
                        {
                            ctx.RelayPrelimOrders.RemoveRange(prelimAthleteExisted);
                        }
                        foreach (var prelimOrder in prelimOrderService.PrelimAtheleteList)
                        {
                            RelayFinalOrder finalOrder = ctx.RelayFinalOrders.Where(p => p.AthleteID == prelimOrder.AtheleteID && p.RelayID == prelimOrder.RelayID).FirstOrDefault();
                            if (finalOrder == null)
                            {
                                finalOrder = new RelayFinalOrder();
                                finalOrder.AthleteID = prelimOrder.AtheleteID;
                                finalOrder.Position = prelimOrder.Position;
                                finalOrder.RelayID = prelimOrder.RelayID;
                                ctx.RelayFinalOrders.Add(finalOrder);
                                InsertedId = finalOrder.RelayFinalOrderID;
                            }
                            else
                            {
                                finalOrder.AthleteID = prelimOrder.AtheleteID;
                                finalOrder.Position = prelimOrder.Position;
                                finalOrder.RelayID = prelimOrder.RelayID;
                            }
                        }
                        ctx.SaveChanges();
                    }
                }

                AuthorizedResponse<PrelimFinResult> Result = new AuthorizedResponse<PrelimFinResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new PrelimFinResult();
                Result.Result.RelayID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<PrelimFinResult> Result = new AuthorizedResponse<PrelimFinResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new PrelimFinResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<PrelimFinResult>> CopyFinalToPrelim(ServiceRequest_PrelimFin prelimOrderService)
        {
            try
            {
                int InsertedId = 0;
                if (prelimOrderService != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        List<RelayFinalOrder> prelimAthleteExisted = ctx.RelayFinalOrders.Where(p => p.RelayID == prelimOrderService.RelayID).ToList();
                        if (prelimAthleteExisted != null)
                        {
                            ctx.RelayFinalOrders.RemoveRange(prelimAthleteExisted);
                        }
                        foreach (var prelimOrder in prelimOrderService.PrelimAtheleteList)
                        {
                            RelayPrelimOrder finalOrder = ctx.RelayPrelimOrders.Where(p => p.AthleteID == prelimOrder.AtheleteID && p.RelayID == prelimOrder.RelayID).FirstOrDefault();
                            if (finalOrder == null)
                            {
                                finalOrder = new RelayPrelimOrder();
                                finalOrder.AthleteID = prelimOrder.AtheleteID;
                                finalOrder.Position = prelimOrder.Position;
                                finalOrder.RelayID = prelimOrder.RelayID;
                                ctx.RelayPrelimOrders.Add(finalOrder);
                                InsertedId = finalOrder.RelayPrelimOrderID;
                            }
                            else
                            {
                                finalOrder.AthleteID = prelimOrder.AtheleteID;
                                finalOrder.Position = prelimOrder.Position;
                                finalOrder.RelayID = prelimOrder.RelayID;
                            }
                        }
                        ctx.SaveChanges();
                    }
                }

                AuthorizedResponse<PrelimFinResult> Result = new AuthorizedResponse<PrelimFinResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new PrelimFinResult();
                Result.Result.RelayID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<PrelimFinResult> Result = new AuthorizedResponse<PrelimFinResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new PrelimFinResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<AthletiesResult>> AddAthletesToRelayPrelimOrder(ServiceRequest_Athleties AthletiesRequest, int RelayID)
        {
            try
            {
                int InsertedId = 0;
                if (AthletiesRequest != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        RelayPrelimOrder finalOrder = new RelayPrelimOrder();
                        finalOrder.AthleteID = AthletiesRequest.Athleties.AthletesID;
                        int MaxPosition = ctx.RelayPrelimOrders.Max(p => p.Position).Value;
                        finalOrder.Position = MaxPosition + 1;
                        finalOrder.RelayID = RelayID;
                        ctx.RelayPrelimOrders.Add(finalOrder);
                        ctx.SaveChanges();
                    }
                }

                AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new AthletiesResult();
                Result.Result.AthletiesID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new AthletiesResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<AthletiesResult>> DeleteAthletiesElegible(int AthletiesID)
        {
            AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<AthletiesElegible> eventDetailList = ctx.AthletiesElegibles.Where(p => p.AthletesID == AthletiesID).ToList();
                        if (eventDetailList != null)
                        {
                            ctx.AthletiesElegibles.RemoveRange(eventDetailList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new AthletiesResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new AthletiesResult();
            Result.Result.AthletiesID = AthletiesID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AuthorizedResponse<AthleteContactResult>> AddAthletesContact(ServiceRequest_AthleteContact ContactRequest)
        {
            try
            {
                int InsertedId = 0;
                if (ContactRequest != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        AtheleteContact contact = ctx.AtheleteContacts.Where(p => p.AthleteID == ContactRequest.Contact.AthletesID).FirstOrDefault();
                        if (contact == null)
                        {
                            contact = new AtheleteContact();
                        }
                        contact.AthleteID = ContactRequest.Contact.AthletesID;
                        contact.Address1 = ContactRequest.Contact.Address1;
                        contact.Address2 = ContactRequest.Contact.Address2;
                        contact.Cell = ContactRequest.Contact.Cell.ToString();
                        contact.City = ContactRequest.Contact.City;
                        contact.Country = ContactRequest.Contact.CountryString;
                        contact.Email = ContactRequest.Contact.Email;
                        contact.EmregancyContact = ContactRequest.Contact.EmregancyContact;
                        contact.EmregancyPhone = ContactRequest.Contact.EmregancyPhone.ToString();
                        contact.Fax = ContactRequest.Contact.Fax.ToString();
                        contact.Home = ContactRequest.Contact.Home.ToString();
                        contact.Office = ContactRequest.Contact.Office.ToString();
                        contact.Province = ContactRequest.Contact.Province;
                        contact.State = ContactRequest.Contact.State;
                        contact.Zip = ContactRequest.Contact.Zip.ToString();
                        if (contact.AtheleteContactID <= 0)
                            ctx.AtheleteContacts.Add(contact);
                        ctx.SaveChanges();
                    }
                }

                AuthorizedResponse<AthleteContactResult> Result = new AuthorizedResponse<AthleteContactResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new AthleteContactResult();
                Result.Result.AthleteContactID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<AthleteContactResult> Result = new AuthorizedResponse<AthleteContactResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new AthleteContactResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<AthleteContactResult>> DeleteAthletiesContact(int AthletiesID)
        {
            AuthorizedResponse<AthleteContactResult> Result = new AuthorizedResponse<AthleteContactResult>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<AtheleteContact> eventDetailList = ctx.AtheleteContacts.Where(p => p.AthleteID == AthletiesID).ToList();
                        if (eventDetailList != null)
                        {
                            ctx.AtheleteContacts.RemoveRange(eventDetailList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new AthleteContactResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new AthleteContactResult();
            Result.Result.AthleteContactID = AthletiesID;
            Result.Result.AuthToken = null;
            return Result;
        }

        public async Task<AddContactViewModel> GetContact(int AthletiesID)
        {
            AddContactViewModel contactDetail = new AddContactViewModel();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                AtheleteContact contact = ctx.AtheleteContacts.Where(p => p.AthleteID == AthletiesID).FirstOrDefault();
                if (contact != null)
                {
                    contactDetail.Address1 = contact.Address1;
                    contactDetail.Address2 = contact.Address2;
                    contactDetail.AthletesID = AthletiesID;
                    contactDetail.Cell = Convert.ToInt32(contact.Cell);
                    contactDetail.City = contact.City;
                    contactDetail.Email = contact.Email;
                    contactDetail.EmregancyContact = contact.EmregancyContact;
                    contactDetail.EmregancyPhone = Convert.ToInt32(contact.EmregancyPhone);
                    contactDetail.Fax = Convert.ToInt32(contact.Fax);
                    contactDetail.Home = Convert.ToInt32(contact.Home);
                    contactDetail.Office = Convert.ToInt32(contact.Office);
                    contactDetail.Province = contact.Province;
                    contactDetail.State = contact.State;
                    contactDetail.Zip = Convert.ToInt32(contact.Zip);
                    contactDetail.AthleteContactID = contact.AtheleteContactID;
                    contactDetail.CountryString = contact.Country;
                }


            }
            return contactDetail;
        }

        public async Task<List<EventSession>> GetAllSessions()
        {
            using (EventEntities ctx = new EventEntities())
            {
                return ctx.EventSessions.ToList();
            }
        }

        public async Task<List<EventInSession>> GetAllEventSessions(List<int?> SessionScheduleIDs)
        {
            using (EventEntities ctx = new EventEntities())
            {
                return ctx.EventInSessions.Where(p => SessionScheduleIDs.Contains(p.SessionScheduleID)).ToList();
            }
        }

        public async Task<List<AthleteInLane>> GetAllAthletesInEventList()
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.AthleteInLanes.ToList();
            }
        }

        public async Task<List<EventRecord>> GetAllEventRecordList()
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.EventRecords.ToList();
            }
        }

        public async Task<List<TeamRanking>> GetAllTeamRankingList()
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.TeamRankings.ToList();
            }
        }

        public async Task<List<EventInSession>> GetAllEventSessions()
        {
            using (EventEntities ctx = new EventEntities())
            {
                return ctx.EventInSessions.ToList();
            }
        }

        public async Task<AuthorizedResponse<RunHeatResult>> AddAthletesHeatToEvent(ServiceRequest_RunHeat Heat)
        {
            try
            {
                int InsertedId = 0;
                if (Heat != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        AthleteInLane athLane = ctx.AthleteInLanes.Where(p => p.AthleteInLaneID == Heat.Lane.AthleteInLaneID).FirstOrDefault();

                        if (athLane == null)
                        {
                            athLane = new AthleteInLane();
                        }
                        athLane.AthleteID = Heat.Lane.AthleteID;
                        athLane.AthleteName = Heat.Lane.AthleteName;
                        athLane.ClassYR = Heat.Lane.ClassYR;
                        athLane.CompetitorNumber = Heat.Lane.CompetitorNumber;
                        athLane.EventID = Heat.Lane.EventID;
                        athLane.FnalTime = Heat.Lane.FnalTime;
                        athLane.HPL = Heat.Lane.HPL;
                        athLane.IsDQ = Heat.Lane.IsDQ;
                        athLane.IsExh = Heat.Lane.IsExh;
                        athLane.LaneNumber = Heat.Lane.LaneNumber;
                        athLane.PL = Heat.Lane.PL;
                        athLane.PTS = Heat.Lane.PTS;
                        athLane.SamiTime = Heat.Lane.SamiTime;
                        athLane.SchoolName = Heat.Lane.SchoolName;
                        if (athLane.AthleteInLaneID <= 0)
                            ctx.AthleteInLanes.Add(athLane);


                        ctx.SaveChanges();
                    }
                }



                AuthorizedResponse<RunHeatResult> Result = new AuthorizedResponse<RunHeatResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new RunHeatResult();
                Result.Result.AthleteInLaneID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<RunHeatResult> Result = new AuthorizedResponse<RunHeatResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new RunHeatResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<AuthorizedResponse<RunHeatResult>> DeleteAthletesHeatToEvent(int EventInLaneID)
        {
            AuthorizedResponse<RunHeatResult> Result = new AuthorizedResponse<RunHeatResult>();
            using (AthletesContainer ctx = new AthletesContainer())
            {
                using (var dbContextTransaction = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<AthleteInLane> eventDetailList = ctx.AthleteInLanes.Where(p => p.AthleteInLaneID == EventInLaneID).ToList();
                        if (eventDetailList != null)
                        {
                            ctx.AthleteInLanes.RemoveRange(eventDetailList);
                        }
                        ctx.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        Logger.LoggerManager.LogError(ex.Message);
                        Result.Message = ex.Message;
                        dbContextTransaction.Rollback();
                        Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                        Result.Result = new RunHeatResult();
                        Result.Result.AuthToken = null;
                        return Result;
                    }
                }
            }
            Result.Message = "";
            Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
            Result.Result = new RunHeatResult();
            Result.Result.AthleteInLaneID = EventInLaneID;
            Result.Result.AuthToken = null;
            return Result;
        }
        public async Task<List<GetTeamsInEvent_Result1>> GetTeamsInEvent(int? eventID)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.GetTeamsInEvent(eventID).ToList();
            }
        }

        public async Task<List<Athlete>> GetAthleteInTeam(int? teamID)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.Athletes.Where(p => p.TeamID == teamID).ToList();
            }
        }

        public async Task<AuthorizedResponse<AthletiesResult>> AddAthleteResult(ServiceRequest_AthleteRecord Record)
        {
            try
            {
                int InsertedId = 0;
                if (Record.EventRecordModelList != null)
                {
                    using (AthletesContainer ctx = new AthletesContainer())
                    {
                        foreach (var item in Record.EventRecordModelList)
                        {
                            bool needAdd = false;
                            EventRecord eventRecord = ctx.EventRecords.Where(p => p.EventRecordID == item.EventRecordID).FirstOrDefault();
                            if (eventRecord == null)
                            {
                                eventRecord = new EventRecord();
                                needAdd = true;
                            }
                            eventRecord.Date = item.Date;
                            eventRecord.EventID = item.EventID;
                            eventRecord.Record = item.Record;
                            eventRecord.RecordAbb = item.RecordAbb;
                            eventRecord.RecordHolderName = item.RecordHolderName;
                            eventRecord.Time = item.Time;
                            if (needAdd)
                            {
                                ctx.EventRecords.Add(eventRecord);
                            }
                            InsertedId = eventRecord.EventRecordID;
                        }
                        ctx.SaveChanges();

                    }
                }

                AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
                Result.Message = "";
                Result.Status = eMEET_REQUEST_RESPONSE.SUCCESS;
                Result.Result = new AthletiesResult();
                Result.Result.AthletiesID = InsertedId;
                Result.Result.AuthToken = null;
                return Result;
            }
            catch (Exception ex)
            {

                Logger.LoggerManager.LogError(ex.Message);
                AuthorizedResponse<AthletiesResult> Result = new AuthorizedResponse<AthletiesResult>();
                Result.Message = ex.Message;
                Result.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
                Result.Result = new AthletiesResult();
                Result.Result.AuthToken = null;
                return Result;
            }
        }

        public async Task<List<EventRecord>> GetEventRecordByEventID(int? eventID)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return ctx.EventRecords.Where(p => p.EventID == eventID).ToList();
            }
        }

        public async Task<List<GetTeamPointDetail_Result>> GetTeamPointDetailByEventID(int? eventID)
        {
            using (AthletesContainer ctx = new AthletesContainer())
            {
                return  ctx.GetTeamPointDetail(eventID).ToList();
            }
        }
    }
}
