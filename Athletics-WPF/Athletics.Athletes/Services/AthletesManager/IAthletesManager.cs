﻿using Athletics.Athletes.Services.API.Athleties;
using Athletics.Athletes.ViewModels;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Services.API;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Athletics.Athletes
{
    public enum eMEET_REQUEST_RESPONSE
    {
        DEFAULT_ERROR,
        SUCCESS,
    }

    public class AuthorizedResponse<T> where T : class
    {
        public AuthorizedResponse()
        {
            this.Status = eMEET_REQUEST_RESPONSE.DEFAULT_ERROR;
            Message = "";
            Result = null;
        }

        public eMEET_REQUEST_RESPONSE Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }


    /// <summary>
    /// Anything having to do Meet Setup 
    /// inside this service
    /// </summary>
    public interface IAthletesManager
    {

        Task<AuthorizedResponse<AthletiesResult>> CreateNewAthleties(ServiceRequest_Athleties AthletiesRequest);
        Task<AuthorizedResponse<AthletiesResult>> UpdateAthleties(ServiceRequest_Athleties AthletiesRequest);
        Task<AuthorizedResponse<AthletiesResult>> DeleteAthleties(int AthletiesID);
        Task<List<AddAthletesViewModel>> GetAllAthleties();
        List<BaseCounty> GetAllCountry();
        List<State> GetStateByCountry(int CountryID);
        Task<List<Team>> GetAllTeams();
        List<SchoolDetail> GetAllSchoolList();
        Task<AuthorizedResponse<RelayResult>> CreateNewRelay(ServiceRequest_Relay RelayRequest);
        Task<AuthorizedResponse<RelayResult>> UpdateRelay(ServiceRequest_Relay RelayRequest);
        Task<AuthorizedResponse<RelayResult>> DeleteRelay(int RelayID);
        Task<List<AddRelayViewModel>> GetAllRelay();
        Task<AuthorizedResponse<TeamResult>> CreateNewTeam(ServiceRequest_Team TeamRequest);
        Task<AuthorizedResponse<TeamResult>> UpdateTeam(ServiceRequest_Team TeamRequest);
        Task<AuthorizedResponse<TeamResult>> DeleteTeam(int TeamID);
        Task<List<Entities.Models.EventDetail>> GetAllEventDetail();
        Task<List<AthletiesElegible>> GetAllElegibleEvents(int athleteID);
        Task<AuthorizedResponse<AthletiesResult>> CreateNewElegibleAthleties(ServiceRequest_AthletiesElegible AthletiesRequest);
        Task<List<AthletiesElegible>> GetAllElegibleEvents();
        Task<AuthorizedResponse<RelayResult>> AddTeamRelay(RelayEntryDetail entryDetail);
        Task<List<RelayEntryDetail>> GetAllRelayTeam(int RelayID);
        Task<List<RelayPrelimOrder>> GetRelayOrderPreLim(int RelayID);
        Task<List<RelayFinalOrder>> GetRelayOrderPreFinal(int RelayID);
        Task<AuthorizedResponse<PrelimFinResult>> CopyPrelimToFinal(ServiceRequest_PrelimFin prelimOrder);
        Task<AuthorizedResponse<PrelimFinResult>> CopyFinalToPrelim(ServiceRequest_PrelimFin prelimOrder);
        Task<AuthorizedResponse<AthletiesResult>> AddAthletesToRelayPrelimOrder(ServiceRequest_Athleties AthletiesRequest, int RelayID);
        Task<AuthorizedResponse<AthletiesResult>> DeleteAthletiesElegible(int AthletiesID);
        Task<AuthorizedResponse<AthleteContactResult>> AddAthletesContact(ServiceRequest_AthleteContact ContactRequest);
        Task<AuthorizedResponse<AthleteContactResult>> DeleteAthletiesContact(int AthletiesID);
        Task<AddContactViewModel> GetContact(int AthletiesID);
        Task<List<EventSession>> GetAllSessions();
        Task<List<AthleteInLane>> GetAllAthletesInEventList();
        Task<List<EventRecord>> GetAllEventRecordList();
        Task<List<TeamRanking>> GetAllTeamRankingList();
        Task<List<EventInSession>> GetAllEventSessions();
        Task<AuthorizedResponse<RunHeatResult>> AddAthletesHeatToEvent(ServiceRequest_RunHeat Heat);
        Task<AuthorizedResponse<RunHeatResult>> DeleteAthletesHeatToEvent(int EventInLaneID);
        Task<List<GetTeamsInEvent_Result1>> GetTeamsInEvent(int? eventID);
        Task<List<Athlete>> GetAthleteInTeam(int? teamID);
        Task<AuthorizedResponse<AthletiesResult>> AddAthleteResult(ServiceRequest_AthleteRecord Record);
        Task<List<EventRecord>> GetEventRecordByEventID(int? eventID);
        Task<List<GetTeamPointDetail_Result>> GetTeamPointDetailByEventID(int? eventID);
    }
}
