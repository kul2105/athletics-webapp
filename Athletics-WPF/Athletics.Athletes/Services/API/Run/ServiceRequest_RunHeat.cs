﻿using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_RunHeat
    {
        public int AthleteInLaneID { get; set; }
        public AthleteLane Lane { get; set; }
    }
}
