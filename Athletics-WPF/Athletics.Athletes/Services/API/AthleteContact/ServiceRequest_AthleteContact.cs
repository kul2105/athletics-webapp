﻿using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_AthleteContact
    {
        public int AthleteContactID { get; set; }
        public AddContactViewModel Contact { get; set; }
    }
}
