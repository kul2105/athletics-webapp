﻿using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_Relay
    {
        public int RelayID { get; set; }
        public AddRelayViewModel Relay { get; set; }
    }
}
