﻿using Athletics.Athletes.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_PrelimFin
    {
        public int RelayID { get; set; }
        public ObservableCollection<PrelimAthelete> PrelimAtheleteList { get; set; }
    }
}
