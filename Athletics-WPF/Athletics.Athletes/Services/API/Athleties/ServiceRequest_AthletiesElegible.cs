﻿using Athletics.Athletes.ViewModels;
using Athletics.Entities.Models;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_AthletiesElegible
    {
        public int AthletiesID { get; set; }
        public AthletiesElegible Elegible { get; set; }
    }
}
