﻿using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_Athleties
    {
        public int AthletiesID { get; set; }
        public AddAthletesViewModel Athleties { get; set; }
    }
}
