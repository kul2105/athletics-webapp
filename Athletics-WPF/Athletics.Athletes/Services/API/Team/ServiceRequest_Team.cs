﻿using Athletics.Athletes.ViewModels;

namespace Athletics.MeetSetup.Services.API
{
    public class ServiceRequest_Team
    {
        public int TeamID { get; set; }
        public AddTeamViewModel Team { get; set; }
    }
}
