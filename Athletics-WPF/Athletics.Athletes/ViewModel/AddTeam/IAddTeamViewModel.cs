﻿using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IAddTeamViewModel
    {
        ObservableCollection<AddTeamViewModel> TeamDetailList { get; set; }
        AddTeamViewModel SelectedTeamDetail { get; set; }
    }
}
