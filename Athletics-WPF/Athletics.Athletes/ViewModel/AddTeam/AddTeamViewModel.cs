﻿using Athletics.Athletes;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Entities.Models;

namespace Athletics.Athletes.ViewModels
{

    public class AddTeamViewModel : BindableBase, IAddTeamViewModel, IGenericInteractionView<AddTeamViewModel>
    {
        private IAddTeamView View = null;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public bool IsCanceled;
        public AddTeamViewModel()
        {

        }

        public AddTeamViewModel(IUnityContainer unity, IAddTeamView view, IAthletesManager athletesManager)
        {
            View = view;
            unityContainer = unity;
            AthletesManager = athletesManager;
            View.DataContext = this;
            TeamList = new List<Team>();
            StatusList = new List<string>();
            StatusList.Add("Normal");
            //Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            //{
                List<Team> teams =  AthletesManager.GetAllTeams().Result;
                foreach (Team item in teams)
                {
                    TeamList.Add(item);
                }
            //}));
            CountryList = new List<BaseCounty>();
            //Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            //{
                List<BaseCounty> countryList = AthletesManager.GetAllCountry();
                foreach (BaseCounty item in countryList)
                {
                    CountryList.Add(item);
                }
            //}));

            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "AddTeam")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "AddTeam")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });



        }

        #region Properties

        private ObservableCollection<AddTeamViewModel> _TeamDetailList;
        public ObservableCollection<AddTeamViewModel> TeamDetailList
        {
            get { return _TeamDetailList; }
            set
            {
                SetProperty(ref _TeamDetailList, value);
            }
        }

        private AddTeamViewModel _SelectedTeamDetail;
        public AddTeamViewModel SelectedTeamDetail
        {
            get { return _SelectedTeamDetail; }
            set
            {
                SetProperty(ref _SelectedTeamDetail, value);
            }
        }
        private int _TeamID;
        public int TeamID
        {
            get { return _TeamID; }
            set
            {
                SetProperty(ref _TeamID, value);
            }
        }
        private string _TeamName;
        public string TeamName
        {
            get { return _TeamName; }
            set
            {
                SetProperty(ref _TeamName, value);
            }
        }

        private List<BaseCounty> _CountryList = new List<BaseCounty>();
        public List<BaseCounty> CountryList
        {
            get { return _CountryList; }
            set
            {
                SetProperty(ref _CountryList, value);
            }
        }
        private List<State> _StateList = new List<State>();
        public List<State> StateList
        {
            get { return _StateList; }
            set
            {
                SetProperty(ref _StateList, value);
            }
        }
        private State _SelectedState;
        public State SelectedState
        {
            get { return _SelectedState; }
            set
            {
                SetProperty(ref _SelectedState, value);
                if (value != null)
                {
                    this.State = value.StateName;
                }
            }
        }

        private BaseCounty _SelectedCountry;
        public BaseCounty SelectedCountry
        {
            get { return _SelectedCountry; }
            set
            {
                SetProperty(ref _SelectedCountry, value);
                if (value != null)
                {
                    CountryString = value.BaseCountyName;
                    if (AthletesManager != null)
                    {
                        StateList = new List<State>();
                        //Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        //{
                        List<State> sateList = AthletesManager.GetStateByCountry(value.BaseCountyID);
                        foreach (State item in sateList)
                        {
                            StateList.Add(item);
                        }
                        //}));
                    }
                }
            }
        }

        private List<string> _StatusList;
        public List<string> StatusList
        {
            get { return _StatusList; }
            set
            {
                SetProperty(ref _StatusList, value);
            }
        }

        private string _CountryString;
        public string CountryString
        {
            get { return _CountryString; }
            set
            {
                SetProperty(ref _CountryString, value);
            }
        }
        private string _City;
        public string City
        {
            get { return _City; }
            set
            {
                SetProperty(ref _City, value);
            }
        }
        private string _TeamAbbr;
        public string TeamAbbr
        {
            get { return _TeamAbbr; }
            set
            {
                SetProperty(ref _TeamAbbr, value);
            }
        }
        private string _AltTeamAbb;
        public string AltTeamAbb
        {
            get { return _AltTeamAbb; }
            set
            {
                SetProperty(ref _AltTeamAbb, value);
            }
        }
        private string _Region;
        public string Region
        {
            get { return _Region; }
            set
            {
                SetProperty(ref _Region, value);
            }
        }


        private string _FullTeamName;
        public string FullTeamName
        {
            get { return _FullTeamName; }
            set
            {
                SetProperty(ref _FullTeamName, value);
            }
        }
        private string _Association;
        public string Association
        {
            get { return _Association; }
            set
            {
                SetProperty(ref _Association, value);
            }
        }
        private string _Division;
        public string Division
        {
            get { return _Division; }
            set
            {
                SetProperty(ref _Division, value);
            }
        }
        private string _ShortTeamName;
        public string ShortTeamName
        {
            get { return _ShortTeamName; }
            set
            {
                SetProperty(ref _ShortTeamName, value);
            }
        }
        private bool _IsUnAttached;
        public bool IsUnAttached
        {
            get { return _IsUnAttached; }
            set
            {
                SetProperty(ref _IsUnAttached, value);
            }
        }

        private string _HeadCoachLastName;
        public string HeadCoachLastName
        {
            get { return _HeadCoachLastName; }
            set
            {
                SetProperty(ref _HeadCoachLastName, value);
            }
        }

        private string _HeadCoachFirstName;
        public string HeadCoachFirstName
        {
            get { return _HeadCoachFirstName; }
            set
            {
                SetProperty(ref _HeadCoachFirstName, value);
            }
        }
        private string _Address1;
        public string Address1
        {
            get { return _Address1; }
            set
            {
                SetProperty(ref _Address1, value);
            }
        }

        private string _Address2;
        public string Address2
        {
            get { return _Address2; }
            set
            {
                SetProperty(ref _Address2, value);
            }
        }

        private string _State;
        public string State
        {
            get { return _State; }
            set
            {
                SetProperty(ref _State, value);
            }
        }
        private string _Office;
        public string Office
        {
            get { return _Office; }
            set
            {
                SetProperty(ref _Office, value);
            }
        }
        private string _Province;
        public string Province
        {
            get { return _Province; }
            set
            {
                SetProperty(ref _Province, value);
            }
        }
        private string _Zip;
        public string Zip
        {
            get { return _Zip; }
            set
            {
                SetProperty(ref _Zip, value);
            }
        }


        private List<Team> _TeamList = new List<Team>();
        public List<Team> TeamList
        {
            get { return _TeamList; }
            set
            {
                SetProperty(ref _TeamList, value);
            }
        }
        private string _TeamString;
        public string TeamString
        {
            get { return _TeamString; }
            set
            {
                SetProperty(ref _TeamString, value);
            }
        }

        private Team _Team;
        public Team Team
        {
            get { return _Team; }
            set
            {
                SetProperty(ref _Team, value);
                if (value != null)
                {
                    TeamString = value.TeamName;
                }
            }
        }
        private string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                SetProperty(ref _Status, value);
            }
        }

        private string _Home;
        public string Home
        {
            get { return _Home; }
            set
            {
                SetProperty(ref _Home, value);
            }
        }
        private string _Fax;
        public string Fax
        {
            get { return _Fax; }
            set
            {
                SetProperty(ref _Fax, value);
            }
        }
        private string _Email;
        public string Email
        {
            get { return _Email; }
            set
            {
                SetProperty(ref _Email, value);
            }
        }


        #endregion

        #region Reset UI

        #endregion

        #region Form Properties


        #endregion

        private bool ValidateNextPrevious()
        {
            if (this.TeamID > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(AddTeamViewModel entity)
        {
            if (entity == null) return;
            this.Address1 = entity.Address1;
            this.State = entity.State;
            this.City = entity.City;
            this.Address2 = entity.Address2;
            this.Association = entity.Association;
            this.Division = entity.Division;
            this.FullTeamName = entity.FullTeamName;
            this.HeadCoachFirstName = entity.HeadCoachFirstName;
            this.HeadCoachLastName = entity.HeadCoachLastName;
            this.IsUnAttached = entity.IsUnAttached;
            this.Office = entity.Office;
            this.Province = entity.Province;
            this.ShortTeamName = entity.ShortTeamName;
            this.TeamAbbr = entity.TeamAbbr;
            this.TeamName = entity.TeamName;
            this.Zip = entity.Zip;
            this.Status = entity.Status;
            this.SelectedCountry = this.CountryList.Where(p => p.BaseCountyName == entity.CountryString).FirstOrDefault();
            this.SelectedState = this.StateList.Where(p => p.StateName == entity.State).FirstOrDefault();
            this.AltTeamAbb = entity.AltTeamAbb;
            this.Region = entity.Region;
            this.Home = entity.Home;
            this.Fax = entity.Fax;
            this.Email = entity.Email;
            this.TeamID = entity.TeamID;
        }
        public AddTeamViewModel GetEntity()
        {

            return this;
        }


        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        #endregion
    }

}

