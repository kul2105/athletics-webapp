﻿using Athletics.Athletes;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Entities.Models;

namespace Athletics.Athletes.ViewModels
{

    public class AddRelayViewModel : BindableBase, IAddRelayViewModel, IGenericInteractionView<AddRelayViewModel>
    {
        private IAddRelayView View = null;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public bool IsCanceled;
        public AddRelayViewModel()
        {

        }

        public AddRelayViewModel(IUnityContainer unity, IAddRelayView view, IAthletesManager athletesManager)
        {
            View = view;
            this.SetEntity(SelectedAddRelayViewModel);
            unityContainer = unity;
            AthletesManager = athletesManager;
            View.DataContext = this;
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "AddRelay")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "AddRelay")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

            GenderList = new List<string>();
            GenderList.Add("M");
            GenderList.Add("F");

        }

        #region Properties

        private Entities.Models.EventDetail _SelectedEvent;
        public Entities.Models.EventDetail SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                SetProperty(ref _SelectedEvent, value);
            }
        }

        private AddRelayViewModel _SelectedAddRelayViewModel;
        public AddRelayViewModel SelectedAddRelayViewModel
        {
            get { return _SelectedAddRelayViewModel; }
            set
            {
                SetProperty(ref _SelectedAddRelayViewModel, value);
            }
        }
        private List<string> _GenderList;
        public List<string> GenderList
        {
            get { return _GenderList; }
            set
            {
                SetProperty(ref _GenderList, value);
            }
        }
        private int _RelayID;
        public int RelayID
        {
            get { return _RelayID; }
            set
            {
                SetProperty(ref _RelayID, value);
            }
        }
        private string _RelayName;
        public string RelayName
        {
            get { return _RelayName; }
            set
            {
                SetProperty(ref _RelayName, value);
            }
        }

        private string _RelayAbb;
        public string RelayAbb
        {
            get { return _RelayAbb; }
            set
            {
                SetProperty(ref _RelayAbb, value);
                IsEdit = true;
            }
        }

        private int _StartAgeRange;
        public int StartAgeRange
        {
            get { return _StartAgeRange; }
            set
            {
                SetProperty(ref _StartAgeRange, value);
            }
        }
        private int _EndAgeRange;
        public int EndAgeRange
        {
            get { return _EndAgeRange; }
            set
            {
                SetProperty(ref _EndAgeRange, value);
            }
        }
        private string _Gender;
        public string Gender
        {
            get { return _Gender; }
            set
            {
                SetProperty(ref _Gender, value);
            }
        }

        private string _EntryMark;
        public string EntryMark
        {
            get { return _EntryMark; }
            set
            {
                SetProperty(ref _EntryMark, value);
                IsEdit = true;
            }
        }
        private string _HeatLane;
        public string HeatLane
        {
            get { return _HeatLane; }
            set
            {
                SetProperty(ref _HeatLane, value);
                IsEdit = true;
            }
        }
        private bool _IsSRC;
        public bool IsSRC
        {
            get { return _IsSRC; }
            set
            {
                SetProperty(ref _IsSRC, value);
                IsEdit = true;
            }
        }
        private bool _IsAlt;
        public bool IsAlt
        {
            get { return _IsAlt; }
            set
            {
                SetProperty(ref _IsAlt, value);
                IsEdit = true;
            }
        }
        private bool _IsExh;
        public bool IsExh
        {
            get { return _IsExh; }
            set
            {
                SetProperty(ref _IsExh, value);
                IsEdit = true;
            }
        }
        private string _Dec;
        public string Dec
        {
            get { return _Dec; }
            set
            {
                SetProperty(ref _Dec, value);
                IsEdit = true;
            }
        }
        private double _ConvTime;
        public double ConvTime
        {
            get { return _ConvTime; }
            set
            {
                SetProperty(ref _ConvTime, value);
                IsEdit = true;
            }
        }
        private string _EntryNote;
        public string EntryNote
        {
            get { return _EntryNote; }
            set
            {
                SetProperty(ref _EntryNote, value);
                IsEdit = true;
            }
        }
        private string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                SetProperty(ref _Status, value);
                IsEdit = true;
            }
        }
        private string _EventNumber;
        public string EventNumber
        {
            get { return _EventNumber; }
            set
            {
                SetProperty(ref _EventNumber, value);
            }
        }
        private string _EventName;
        public string EventName
        {
            get { return _EventName; }
            set
            {
                SetProperty(ref _EventName, value);
            }
        }
        private int _EventID;
        public int EventID
        {
            get { return _EventID; }
            set
            {
                SetProperty(ref _EventID, value);
            }
        }
        private bool _IsEdit = false;
        public bool IsEdit
        {
            get { return _IsEdit; }
            set
            {
                SetProperty(ref _IsEdit, value);
            }
        }
        #endregion

        #region Reset UI

        #endregion

        #region Form Properties


        #endregion

        private bool ValidateNextPrevious()
        {
            return true;
        }

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(AddRelayViewModel entity)
        {
            if (entity == null) return;
            this.RelayID = entity.RelayID;
            this.RelayName = entity.RelayName;
            this.RelayAbb = entity.RelayAbb;

        }
        public AddRelayViewModel GetEntity()
        {

            return this;
        }


        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        #endregion
    }

}

