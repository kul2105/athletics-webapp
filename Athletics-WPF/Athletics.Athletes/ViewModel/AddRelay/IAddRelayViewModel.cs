﻿using Athletics.Entities.Models;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IAddRelayViewModel
    {
        AddRelayViewModel SelectedAddRelayViewModel { get; set; }
        Entities.Models.EventDetail SelectedEvent { get; set; }
    }
}
