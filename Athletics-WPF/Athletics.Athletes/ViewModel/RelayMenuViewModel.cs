﻿using Athletics.Athletes;
using Athletics.Athletes.Model;
using Athletics.Athletes.ViewModels;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class RelayMenuViewModel : BindableBase, IRelayMenuViewModel
    {
        private ObservableCollection<RelayMenuItemViewModel> _MenuItems = new ObservableCollection<RelayMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IAthletesManager athleticManager;
        IManageRelayView View;
        IManageRelayViewModel ViewModel;
        IRelayMenuView AthleticMenuView;
        public ObservableCollection<RelayMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public RelayMenuViewModel(IUnityContainer unity, IRelayMenuView _View, IManageRelayView ManageAthleteView, IAthletesManager manager)
        {
            athleticManager = manager;
            View = ManageAthleteView;
            ViewModel = ManageAthleteView.DataContext as IManageRelayViewModel;
            AthleticMenuView = _View;
            AthleticMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<RelayMenuItemViewModel>();
                RelayMenuItemViewModel eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Add";
                _AddCommand = new MeetCommandViewModel(AddCommandHandler);
                eventMenu.Command = AddCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Delete";
                _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
                eventMenu.Command = DeleteCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Age up";
                _ViewCommand = new MeetCommandViewModel(AgeUpCommandHandler);
                eventMenu.Command = AgeUpCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "View";
                _ViewCommand = new MeetCommandViewModel(ViewCommandandler);
                eventMenu.Command = ViewCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Help";
                _HelpCommand = new MeetCommandViewModel(HelpCommandHandler);
                eventMenu.Command = HelpCommand;
                MenuItems.Add(eventMenu);

                #endregion
            });
            unityContainer = unity;
            //View.DataContext = this;

        }


        #region Menu Command
        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }

        private ICommand _AgeUpCommand;
        public ICommand AgeUpCommand
        {
            get
            {
                return _AgeUpCommand;
            }
        }


        private ICommand _ViewCommand;
        public ICommand ViewCommand
        {
            get
            {
                return _ViewCommand;
            }
        }
        private ICommand _HelpCommand;
        public ICommand HelpCommand
        {
            get
            {
                return _HelpCommand;
            }
        }
        #endregion

        #region Command Handler
        private void AgeUpCommandHandler()
        {
            MessageBox.Show("Age up");
        }

        private void HelpCommandHandler()
        {
            MessageBox.Show("Help");
        }


        private void ViewCommandandler()
        {
            MessageBox.Show("View");
        }

        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        if (ViewModel.SelectedRelayDetail == null)
                        {
                            MessageBox.Show("Please Select Relay to Delete", "Relay Delete Error");
                            return;
                        }
                        if (ViewModel.SelectedRelayDetail.RelayID == 0)
                        {
                            if (ViewModel.SelectedRelayDetail == null) return;
                            ServiceRequest_PrelimFin prelimFinal = new ServiceRequest_PrelimFin();
                            prelimFinal.RelayID = ViewModel.SelectedRelayDetail.RelayID;
                            prelimFinal.PrelimAtheleteList = new ObservableCollection<PrelimAthelete>();
                            AuthorizedResponse<PrelimFinResult> Responsefinal = await athleticManager.CopyFinalToPrelim(prelimFinal);
                            if (Responsefinal.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                            {
                                MessageBox.Show("Error while deleteing the Prelim", "Event Delete Error");
                                return;
                            }
                            Responsefinal = await athleticManager.CopyPrelimToFinal(prelimFinal);
                            if (Responsefinal.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                            {
                                MessageBox.Show("Error while deleteing the Final", "Event Delete Error");
                                return;
                            }
                            AuthorizedResponse<AthletiesResult> Response = await athleticManager.DeleteAthleties(ViewModel.SelectedRelayDetail.RelayID);
                            if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                            {
                                MessageBox.Show("Error while deleteing the Athlete", "Event Delete Error");
                                return;
                            }

                            ViewModel.SelectedRelayDetail.RelayID = Response.Result.AthletiesID;
                            ViewModel.RelayDetailList.Remove(ViewModel.SelectedRelayDetail);
                            ViewModel.RelayFinalList.Clear();
                            ViewModel.RelayPrelimList.Clear();

                        }
                    });

                })).Wait();
            });

        }

        private void AddCommandHandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddRelayView();
            window.Name = "AddRelay";
            IAddRelayView relayView = window as IAddRelayView;
            if (ViewModel.SelectedRelayEvent == null)
            {
                MessageBox.Show("Please select the Event", "Event not Selected");
                return;
            }
            if (ViewModel.SelectedTeam == null)
            {
                MessageBox.Show("Please select the Team", "Team not Selected");
                return;
            }
            AddRelayViewModel addEventViewModel = new AddRelayViewModel(this.unityContainer, relayView, this.athleticManager);
            addEventViewModel.SelectedAddRelayViewModel = ViewModel.SelectedRelayDetail;
            addEventViewModel.SelectedEvent = ViewModel.SelectedRelayEvent;
            this.ClearViewModel(addEventViewModel);
            if (addEventViewModel != null)
            {
                IAddAthletesViewModel addEvent = addEventViewModel as IAddAthletesViewModel;
                dialog.BindView(window);
                dialog.BindViewModel(addEventViewModel);
            }

            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            AddRelayViewModel addeventVm = addEventViewModel.GetEntity();
            if (addEventViewModel.IsCanceled == true) return;
            //if (ViewModel.SelectedAthleticsDetail.AthletesID == 0)
            //{

            ServiceRequest_Relay serviceObj = new ServiceRequest_Relay();
            addEventViewModel.RelayName = ViewModel.SelectedTeam.TeamName;
            addEventViewModel.EventID = ViewModel.SelectedRelayEvent.EventID;
            addEventViewModel.EventName = ViewModel.SelectedRelayEvent.Event;
            addEventViewModel.EventNumber = ViewModel.SelectedRelayEvent.EventNumber;
            serviceObj.Relay = addEventViewModel;
            AuthorizedResponse<RelayResult> Response = athleticManager.CreateNewRelay(serviceObj).Result;
            if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
            {
                MessageBox.Show("Error while adding the Relay", "Relay Add Error");
                return;
            }
            ViewModel.RelayPrelimList.Clear();
            ViewModel.AddAthleteToFin(addEventViewModel.RelayID);
            if (ViewModel.RelayPrelimList.Count > 0)
            {
                ServiceRequest_PrelimFin serviceReqPreFinal = new ServiceRequest_PrelimFin();
                serviceReqPreFinal.PrelimAtheleteList = ViewModel.RelayPrelimList;
                serviceReqPreFinal.RelayID = addEventViewModel.RelayID;
                AuthorizedResponse<PrelimFinResult> FinalResponse = athleticManager.CopyFinalToPrelim(serviceReqPreFinal).Result;
                if (FinalResponse.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while adding the Prelim List", "Relay Add Error");
                    return;
                }
            }
            if (ViewModel.RelayFinalList.Count > 0)
            {
                ServiceRequest_PrelimFin serviceReqPreFinal = new ServiceRequest_PrelimFin();
                serviceReqPreFinal.PrelimAtheleteList = ViewModel.RelayFinalList;
                serviceReqPreFinal.RelayID = addEventViewModel.RelayID;
                AuthorizedResponse<PrelimFinResult> FinalResponse = athleticManager.CopyPrelimToFinal(serviceReqPreFinal).Result;
                if (FinalResponse.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while adding the Final List", "Relay Add Error");
                    return;
                }
            }
            //ViewModel.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;

            ViewModel.RelayDetailList.Add(addEventViewModel);

            //}
            //else
            //{
            //}



        }

        private void ClearViewModel(IGenericInteractionView<AddRelayViewModel> addEventViewModel)
        {
            addEventViewModel.SetEntity(new AddRelayViewModel());
        }
        #endregion

    }
}

