﻿using Athletics.Athletes;
using Athletics.Athletes.Model;
using Athletics.Athletes.ViewModels;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class TeamMenuViewModel : BindableBase, ITeamMenuViewModel
    {
        private ObservableCollection<TeamMenuItemViewModel> _MenuItems = new ObservableCollection<TeamMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IAthletesManager athleticManager;
        IManageTeamView View;
        IManageTeamViewModel ViewModel;
        ITeamMenuView AthleticMenuView;
        public ObservableCollection<TeamMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public TeamMenuViewModel(IUnityContainer unity, ITeamMenuView _View, IManageTeamView ManageTeamView, IAthletesManager manager)
        {
            athleticManager = manager;
            View = ManageTeamView;
            ViewModel = ManageTeamView.DataContext as IManageTeamViewModel;
            AthleticMenuView = _View;
            AthleticMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<TeamMenuItemViewModel>();
                TeamMenuItemViewModel eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Layout";
                _LayoutCommand = new MeetCommandViewModel(LayoutCommandHandler);
                eventMenu.Command = LayoutCommand;
                MenuItems.Add(eventMenu);
                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Add";
                _AddCommand = new MeetCommandViewModel(AddCommandHandler);
                eventMenu.Command = AddCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Edit";
                _EditCommand = new MeetCommandViewModel(EditCommandHandler);
                eventMenu.Command = EditCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Delete";
                _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
                eventMenu.Command = DeleteCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Coaches";
                _CoachesCommand = new MeetCommandViewModel(CoachesCommandHandler);
                eventMenu.Command = CoachesCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "View";
                _ViewCommand = new MeetCommandViewModel(ViewCommandandler);
                eventMenu.Command = ViewCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Find";
                _ViewCommand = new MeetCommandViewModel(ViewCommandandler);
                eventMenu.Command = FindCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Print";
                _PrintCommand = new MeetCommandViewModel(PrintCommandandler);
                eventMenu.Command = PrintCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Export To HTML";
                _ExportToHTMLCommand = new MeetCommandViewModel(ExportToHTMLCommandHandler);
                eventMenu.Command = ExportToHTMLCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new TeamMenuItemViewModel();
                eventMenu.Header = "Help";
                _HelpCommand = new MeetCommandViewModel(HelpCommandHandler);
                eventMenu.Command = HelpCommand;
                MenuItems.Add(eventMenu);

                #endregion
            });
            unityContainer = unity;
            //View.DataContext = this;

        }
        #region Menu Command

        private ICommand _LayoutCommand;
        public ICommand LayoutCommand
        {
            get
            {
                return _LayoutCommand;
            }
        }

        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }

        private ICommand _CoachesCommand;
        public ICommand CoachesCommand
        {
            get
            {
                return _CoachesCommand;
            }
        }


        private ICommand _ViewCommand;
        public ICommand ViewCommand
        {
            get
            {
                return _ViewCommand;
            }
        }

        private ICommand _FindCommand;
        public ICommand FindCommand
        {
            get
            {
                return _FindCommand;
            }
        }

        private ICommand _PrintCommand;
        public ICommand PrintCommand
        {
            get
            {
                return _PrintCommand;
            }
        }


        private ICommand _ExportToHTMLCommand;
        public ICommand ExportToHTMLCommand
        {
            get
            {
                return _ExportToHTMLCommand;
            }
        }

        private ICommand _HelpCommand;
        public ICommand HelpCommand
        {
            get
            {
                return _HelpCommand;
            }
        }
        #endregion

        #region Command Handler
        private void HelpCommandHandler()
        {
            MessageBox.Show("Help");
        }

        private void ExportToHTMLCommandHandler()
        {
            View.EventGrid.SelectAllCells();
            View.EventGrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;

            ApplicationCommands.Copy.Execute(null, View.EventGrid);
            String result = (string)Clipboard.GetData(
                         DataFormats.Html);
            View.EventGrid.UnselectAll();
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document1"; // Default file name
            dlg.DefaultExt = ".html"; // Default file extension
            dlg.Filter = "Html documents (.html)|*.html"; // Filter files by extension
            Nullable<bool> resultDialog = dlg.ShowDialog();

            // Process save file dialog box results
            if (resultDialog == true)
            {
                // Save document
                string filename = dlg.FileName;
                System.IO.File.WriteAllText(filename, result);
            }


        }

        private void PrintCommandandler()
        {
            System.Windows.Controls.PrintDialog pdPrintDialog = new System.Windows.Controls.PrintDialog();
            if ((bool)pdPrintDialog.ShowDialog().GetValueOrDefault())
            {
                Size pntPageSize = new Size(pdPrintDialog.PrintableAreaWidth, pdPrintDialog.PrintableAreaHeight);
                View.EventGrid.Measure(pntPageSize);
                View.EventGrid.Arrange(new Rect(5, 5, pntPageSize.Width, pntPageSize.Height));
                pdPrintDialog.PrintVisual(View.EventGrid, "Events");
            }
        }

        private void ViewCommandandler()
        {
            MessageBox.Show("View");
        }

        private void FindCommandandler()
        {
            //ManageTimeMarkStandardView manageRecords = new ManageTimeMarkStandardView();
            //manageRecords.Name = "ManageTimeMarkStandardView";
            //manageRecords.ShowDialog();
            //manageRecords.Close();
        }

        private void CoachesCommandHandler()
        {
            MessageBox.Show("Coaches");
        }

        private void CampCommandHandler()
        {
            MessageBox.Show("CampCommandHandler");
        }
        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        if (ViewModel.SelectedTeamDetail == null)
                        {
                            MessageBox.Show("Please Select Team to Delete", "Team Delete Error");
                            return;
                        }
                        if (ViewModel.SelectedTeamDetail.TeamID > 0)
                        {
                            if (ViewModel.SelectedTeamDetail == null) return;
                            AuthorizedResponse<TeamResult> Response = await athleticManager.DeleteTeam(ViewModel.SelectedTeamDetail.TeamID);
                            if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                            {
                                MessageBox.Show("Error while deleteing the Team", "Event Delete Error");
                                return;
                            }
                            ViewModel.SelectedTeamDetail.TeamID = Response.Result.TeamID;
                            ViewModel.TeamDetailList.Remove(ViewModel.SelectedTeamDetail);

                        }
                    });

                })).Wait();
            });

        }

        private void EditCommandHandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddTeamView();
            window.Name = "AddTeam";
            IAddTeamView eventView = window as IAddTeamView;
            AddTeamViewModel addEventViewModel = new AddTeamViewModel(this.unityContainer, eventView, this.athleticManager);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            addEventViewModel.SetEntity(ViewModel.SelectedTeamDetail);
            if (addEventViewModel != null)
            {
                IAddTeamViewModel addEvent = addEventViewModel as IAddTeamViewModel;
                if (addEvent != null)
                {
                    addEvent.TeamDetailList = ViewModel.TeamDetailList;
                    addEvent.SelectedTeamDetail = ViewModel.SelectedTeamDetail;
                }
                dialog.BindView(window);
                dialog.BindViewModel(addEvent);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            if (ViewModel.SelectedTeamDetail.TeamID > 0)
            {

                ServiceRequest_Team serviceObj = new ServiceRequest_Team();
                serviceObj.Team = ViewModel.SelectedTeamDetail;
                AuthorizedResponse<TeamResult> Response = athleticManager.UpdateTeam(serviceObj).Result;
                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while Updating the Team", "Team Add Error");
                    return;
                }
                //await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                // {
                ViewModel.SelectedTeamDetail.TeamID = Response.Result.TeamID;
                AddTeamViewModel vm = ViewModel.TeamDetailList.Where(p => p.TeamID == ViewModel.SelectedTeamDetail.TeamID).FirstOrDefault();
                if (vm != null)
                {
                    ViewModel.TeamDetailList.Remove(vm);
                    ViewModel.TeamDetailList.Add(addEventViewModel);
                    ViewModel.SelectedTeamDetail = addEventViewModel;
                }
                //}));
            }
            else
            {
            }

            //});

            //})).Wait();
            //});

        }

        private void AddCommandHandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddTeamView();
            window.Name = "AddTeam";
            IAddTeamView eventView = window as IAddTeamView;
            AddTeamViewModel addTeamViewModel = new AddTeamViewModel(this.unityContainer, eventView, this.athleticManager);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            this.ClearViewModel(addTeamViewModel);
            if (addTeamViewModel != null)
            {
                IAddTeamViewModel addEvent = addTeamViewModel as IAddTeamViewModel;
                if (addEvent != null)
                {
                    addEvent.TeamDetailList = ViewModel.TeamDetailList;
                    addEvent.SelectedTeamDetail = ViewModel.SelectedTeamDetail;
                }
                dialog.BindView(window);
                dialog.BindViewModel(addTeamViewModel);
            }
            dialog.ShowDialog();
            dialog.Close();
            AddTeamViewModel addeventVm = addTeamViewModel.GetEntity();
            if (addTeamViewModel.IsCanceled == true) return;
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        if (!string.IsNullOrEmpty(addTeamViewModel.TeamName))
                        {

                            ServiceRequest_Team serviceObj = new ServiceRequest_Team();
                            serviceObj.Team = addTeamViewModel;
                            AuthorizedResponse<TeamResult> Response = await athleticManager.CreateNewTeam(serviceObj);
                            if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                            {
                                MessageBox.Show("Error while adding the Team", "Team Add Error");
                                return;
                            }
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                //ViewModel.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;
                                ViewModel.TeamDetailList.Add(addTeamViewModel);
                            }));

                        }
                        //else
                        //{
                        //}

                    });

                })).Wait();
            });

        }

        private void ClearViewModel(IGenericInteractionView<AddTeamViewModel> addTeamViewModel)
        {
            addTeamViewModel.SetEntity(new AddTeamViewModel());
        }

        private void LayoutCommandHandler()
        {
            MessageBox.Show("Layout");
        }

        #endregion

    }
}

