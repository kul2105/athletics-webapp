﻿using Athletics.Entities.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IManageRelayViewModel
    {
        ObservableCollection<AddRelayViewModel> RelayDetailList { get; set; }
        AddRelayViewModel SelectedRelayDetail { get; set; }
        Entities.Models.EventDetail SelectedRelayEvent { get; set; }
        Team SelectedTeam { get; set; }
        ObservableCollection<PrelimAthelete> RelayPrelimList { get; set; }
        ObservableCollection<PrelimAthelete> RelayFinalList { get; set; }
        void AddAthleteToFin(int RelayID);
    }
}
