﻿using Athletics.Athletes;
using Athletics.Athletes.Model;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using Athletics.MeetSetup.Services.API;
using Athletics.Entities.Models;

namespace Athletics.Athletes.ViewModels
{
    public class PrelimAthelete : BindableBase
    {
        private int? _AtheleteID = 0;
        public int? AtheleteID
        {
            get { return _AtheleteID; }
            set
            {
                SetProperty(ref _AtheleteID, value);
            }
        }
        private string _AtheleteName = string.Empty;
        public string AtheleteName
        {
            get { return _AtheleteName; }
            set
            {
                SetProperty(ref _AtheleteName, value);
            }
        }
        private string _CompID = string.Empty;
        public string CompID
        {
            get { return _CompID; }
            set
            {
                SetProperty(ref _CompID, value);
            }
        }
        private int? _Position = 1;
        public int? Position
        {
            get { return _Position; }
            set
            {
                SetProperty(ref _Position, value);
            }
        }
        private int? _RelayID = 0;
        public int? RelayID
        {
            get { return _RelayID; }
            set
            {
                SetProperty(ref _RelayID, value);
            }
        }

    }

    public class ManageRelayViewModel : BindableBase, IManageRelayViewModel, IGenericInteractionView<ManageRelayViewModel>
    {
        private IManageRelayView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public ManageRelayViewModel(IUnityContainer unity, IManageRelayView view, IAthletesManager athletesManager)
        {
            View = view;
            unityContainer = unity;
            AthletesManager = athletesManager;
            View.DataContext = this;
            List<AddRelayViewModel> AthleticsList = AthletesManager.GetAllRelay().Result.ToList();
            foreach (AddRelayViewModel item in AthleticsList)
            {
                RelayDetailList.Add(item);
            }
            List<Entities.Models.EventDetail> EventDetailList = AthletesManager.GetAllEventDetail().Result.ToList();
            foreach (Entities.Models.EventDetail item in EventDetailList)
            {
                RelayEventAllList.Add(item);
            }
            List<AthletiesElegible> athletiesElegibleList = AthletesManager.GetAllElegibleEvents().Result.ToList();
            foreach (AthletiesElegible item in athletiesElegibleList)
            {
                AllEligibleAthletsList.Add(item);
            }
            List<AddAthletesViewModel> allAthletsList = AthletesManager.GetAllAthleties().Result.ToList();
            foreach (AddAthletesViewModel item in allAthletsList)
            {
                AllAthletsList.Add(item);
            }
            List<Team> teamList = AthletesManager.GetAllTeams().Result.ToList();
            foreach (Team item in teamList)
            {
                TeamList.Add(item);
            }

            List<AddAthletesViewModel> athleteList = AthletesManager.GetAllAthleties().Result.ToList();
            foreach (AddAthletesViewModel item in athleteList)
            {
                AddAthletesViewModelList.Add(item);
            }
            FilterEvent();
            FilterElegibleAtheletes();
            _AddRelayCommand = new MeetCommandViewModel(AddCommandHandler);
            _EditCommand = new MeetCommandViewModel(EditCommandHandler);
            _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
            _CopyFinToPreCommand = new MeetCommandViewModel(CopyFinToPreCommandHandler);
            _CopyPreToFinCommand = new MeetCommandViewModel(CopyPreToFinCommandHandler);
        }

        private void CopyPreToFinCommandHandler()
        {
            if (this.SelectedRelayDetail == null)
            {
                MessageBox.Show("Please select Relay", "No Relay Selected");
                return;
            }
            foreach (var item in this.RelayPrelimList)
            {
                this.RelayFinalList.Add(item);
            }
            this.RelayPrelimList.Clear();
            this.SavePrelimFin();
        }

        private void CopyFinToPreCommandHandler()
        {
            if (this.SelectedRelayDetail == null)
            {
                MessageBox.Show("Please select Relay", "No Relay Selected");
                return;
            }
            this.SelectedRelayDetail.IsEdit = true;
            foreach (var item in RelayFinalList)
            {
                this.RelayPrelimList.Add(item);
            }
            this.RelayFinalList.Clear();
            this.SavePrelimFin();
        }

        private void SavePrelimFin()
        {
            
            if (RelayPrelimList.Count > 0)
            {
                foreach (var item in RelayPrelimList)
                {

                    ServiceRequest_PrelimFin serviceReqPreFinal = new ServiceRequest_PrelimFin();
                    serviceReqPreFinal.RelayID = this.SelectedRelayDetail.RelayID;
                    serviceReqPreFinal.PrelimAtheleteList = RelayPrelimList;
                    AuthorizedResponse<PrelimFinResult> FinalResponse = AthletesManager.CopyFinalToPrelim(serviceReqPreFinal).Result;
                    if (FinalResponse.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show("Error while adding the Prelim List", "Relay Add Error");

                    }
                }
            }
            if (RelayFinalList.Count > 0)
            {
                foreach (var item in RelayFinalList)
                {

                    ServiceRequest_PrelimFin serviceReqPreFinal = new ServiceRequest_PrelimFin();
                    serviceReqPreFinal.PrelimAtheleteList = RelayFinalList;
                    serviceReqPreFinal.RelayID = this.SelectedRelayDetail.RelayID;
                    AuthorizedResponse<PrelimFinResult> FinalResponse = AthletesManager.CopyPrelimToFinal(serviceReqPreFinal).Result;
                    if (FinalResponse.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show("Error while adding the Final List", "Relay Add Error");
                        return;
                    }
                }
            }
        }

        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    //await Task.Run(async () =>
                    //{
                    if (this.SelectedRelayDetail.RelayID > 0)
                    {
                        if (this.SelectedRelayDetail == null) return;
                        ServiceRequest_PrelimFin prelimFinal = new ServiceRequest_PrelimFin();
                        prelimFinal.RelayID = this.SelectedRelayDetail.RelayID;
                        prelimFinal.PrelimAtheleteList = new ObservableCollection<PrelimAthelete>();
                        AuthorizedResponse<PrelimFinResult> Responsefinal = await AthletesManager.CopyFinalToPrelim(prelimFinal);
                        if (Responsefinal.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleteing the Prelim", "Event Delete Error");
                            return;
                        }
                        Responsefinal = await AthletesManager.CopyPrelimToFinal(prelimFinal);
                        if (Responsefinal.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleteing the Final", "Event Delete Error");
                            return;
                        }
                        AuthorizedResponse<RelayResult> Response = await AthletesManager.DeleteRelay(this.SelectedRelayDetail.RelayID);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleteing the Relay", "Relay Delete Error");
                            return;
                        }

                        //this.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;
                        this.RelayDetailList.Remove(this.SelectedRelayDetail);
                        this.RelayFinalList.Clear();
                        this.RelayPrelimList.Clear();
                    }
                    //});

                })).Wait();
            });

        }

        public void EditCommandHandler()
        {

            foreach (var item in this.RelayDetailList.Where(p => p.IsEdit == true))
            {
                ServiceRequest_Relay serviceObj = new ServiceRequest_Relay();
                serviceObj.Relay = item;
                AuthorizedResponse<RelayResult> Response = AthletesManager.UpdateRelay(serviceObj).Result;
                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while Updating the Relay", "Relay Edit Error");
                    return;
                }
                
            }
        }

        private void AddCommandHandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddRelayView();
            window.Name = "AddRelay";
            IAddRelayView eventView = window as IAddRelayView;
            if (this.SelectedRelayEvent == null)
            {
                MessageBox.Show("Please select the Event", "Event not Selected");
                return;
            }
            if (this.SelectedTeam == null)
            {
                MessageBox.Show("Please select the Team", "Team not Selected");
                return;
            }
            AddRelayViewModel addEventViewModel = new AddRelayViewModel(this.unityContainer, eventView, this.AthletesManager);
            addEventViewModel.SelectedAddRelayViewModel = this.SelectedRelayDetail;
            addEventViewModel.SelectedEvent = this.SelectedRelayEvent;
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            this.ClearViewModel(addEventViewModel);
            if (addEventViewModel != null)
            {
                IAddAthletesViewModel addEvent = addEventViewModel as IAddAthletesViewModel;
                dialog.BindView(window);
                dialog.BindViewModel(addEventViewModel);
            }

            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            AddRelayViewModel addeventVm = addEventViewModel.GetEntity();
            if (addEventViewModel.IsCanceled == true) return;
            //if (ViewModel.SelectedAthleticsDetail.AthletesID == 0)
            //{

            ServiceRequest_Relay serviceObj = new ServiceRequest_Relay();
            serviceObj.Relay = addEventViewModel;
            addEventViewModel.RelayName = this.SelectedTeam.TeamName;
            addEventViewModel.EventID = this.SelectedRelayEvent.EventID;
            addEventViewModel.EventName = this.SelectedRelayEvent.Event;
            addEventViewModel.EventNumber = this.SelectedRelayEvent.EventNumber;
            AuthorizedResponse<RelayResult> Response = AthletesManager.CreateNewRelay(serviceObj).Result;

            if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
            {
                MessageBox.Show("Error while adding the Relay", "Relay Add Error");
                return;
            }
            addEventViewModel.RelayID = Response.Result.RelayID;
            this.RelayDetailList.Add(addEventViewModel);
            RelayPrelimList.Clear();
            AddAthleteToFin(addEventViewModel.RelayID);
            if (RelayPrelimList.Count > 0)
            {
                ServiceRequest_PrelimFin serviceReqPreFinal = new ServiceRequest_PrelimFin();
                serviceReqPreFinal.RelayID = addEventViewModel.RelayID;
                serviceReqPreFinal.PrelimAtheleteList = RelayPrelimList;
                AuthorizedResponse<PrelimFinResult> FinalResponse = AthletesManager.CopyFinalToPrelim(serviceReqPreFinal).Result;
                if (FinalResponse.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while adding the Prelim List", "Relay Add Error");
                    return;
                }
            }
            if (RelayFinalList.Count > 0)
            {
                ServiceRequest_PrelimFin serviceReqPreFinal = new ServiceRequest_PrelimFin();
                serviceReqPreFinal.PrelimAtheleteList = RelayFinalList;
                serviceReqPreFinal.RelayID = addEventViewModel.RelayID;
                AuthorizedResponse<PrelimFinResult> FinalResponse = AthletesManager.CopyPrelimToFinal(serviceReqPreFinal).Result;
                if (FinalResponse.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while adding the Final List", "Relay Add Error");
                    return;
                }
            }

        }
        private void ClearViewModel(IGenericInteractionView<AddRelayViewModel> addEventViewModel)
        {
            addEventViewModel.SetEntity(new AddRelayViewModel());
        }


        private List<Entities.Models.EventDetail> _RelayEventAllList = new List<Entities.Models.EventDetail>();
        public List<Entities.Models.EventDetail> RelayEventAllList
        {
            get { return _RelayEventAllList; }
            set
            {
                SetProperty(ref _RelayEventAllList, value);
            }
        }
        private List<Entities.Models.EventDetail> _RelayEventList = new List<Entities.Models.EventDetail>();
        public List<Entities.Models.EventDetail> RelayEventList
        {
            get { return _RelayEventList; }
            set
            {
                SetProperty(ref _RelayEventList, value);
            }
        }
        private Entities.Models.EventDetail _SelectedRelayEvent = null;
        public Entities.Models.EventDetail SelectedRelayEvent
        {
            get { return _SelectedRelayEvent; }
            set
            {
                SetProperty(ref _SelectedRelayEvent, value);
                this.FilterElegibleAtheletes();
            }
        }
        private List<Team> _TeamList = new List<Team>();
        public List<Team> TeamList
        {
            get { return _TeamList; }
            set
            {
                SetProperty(ref _TeamList, value);
            }
        }
        private List<AddAthletesViewModel> _AddAthletesViewModelList = new List<AddAthletesViewModel>();
        public List<AddAthletesViewModel> AddAthletesViewModelList
        {
            get { return _AddAthletesViewModelList; }
            set
            {
                SetProperty(ref _AddAthletesViewModelList, value);
            }
        }
        private Team _SelectedTeam = new Team();
        public Team SelectedTeam
        {
            get { return _SelectedTeam; }
            set
            {
                SetProperty(ref _SelectedTeam, value);
            }
        }
        private ObservableCollection<PrelimAthelete> _RelayPrelimList = new ObservableCollection<PrelimAthelete>();
        public ObservableCollection<PrelimAthelete> RelayPrelimList
        {
            get { return _RelayPrelimList; }
            set
            {
                SetProperty(ref _RelayPrelimList, value);
            }
        }
        private PrelimAthelete _SelectedRelayPrelim = new PrelimAthelete();
        public PrelimAthelete SelectedRelayPrelim
        {
            get { return _SelectedRelayPrelim; }
            set
            {
                SetProperty(ref _SelectedRelayPrelim, value);
            }
        }
        private ObservableCollection<PrelimAthelete> _RelayFinalList = new ObservableCollection<PrelimAthelete>();
        public ObservableCollection<PrelimAthelete> RelayFinalList
        {
            get { return _RelayFinalList; }
            set
            {
                SetProperty(ref _RelayFinalList, value);
            }
        }
        private PrelimAthelete _SelectedFinalPrelim = new PrelimAthelete();
        public PrelimAthelete SelectedFinalPrelim
        {
            get { return _SelectedFinalPrelim; }
            set
            {
                SetProperty(ref _SelectedFinalPrelim, value);
            }
        }


        public void AddAthleteToFin(int relayID)
        {
            int position = 1;
            foreach (var item in this.AddAthletesViewModelList)
            {
                PrelimAthelete relayPrelim = new PrelimAthelete();
                if (item.TeamString == SelectedTeam.TeamName)
                {
                    relayPrelim.AtheleteID = item.AthletesID;
                    relayPrelim.Position = position;
                    relayPrelim.CompID = item.CompetitorNumber;
                    relayPrelim.AtheleteName = item.FirstName;
                    relayPrelim.RelayID = relayID;
                    position++;
                    RelayPrelimList.Add(relayPrelim);
                }
            }


        }

        private void FilterElegibleAtheletes()
        {
            List<AthletiesElegible> ElegibleAhelete = new List<AthletiesElegible>();
            if (IsMixedChecked)
            {
                ElegibleAhelete = (from elegb in this.AllEligibleAthletsList
                                   join ath in this.AllAthletsList on elegb.AthletesID equals ath.AthletesID
                                   select elegb).ToList();

            }
            else if (IsMaleChecked)
            {
                ElegibleAhelete = (from elegb in this.AllEligibleAthletsList
                                   join ath in this.AllAthletsList.Where(p => p.Gender == "M") on elegb.AthletesID equals ath.AthletesID
                                   select elegb).ToList();
            }
            else if (IsFemaleChecked)
            {
                ElegibleAhelete = (from elegb in this.AllEligibleAthletsList
                                   join ath in this.AllAthletsList.Where(p => p.Gender == "F") on elegb.AthletesID equals ath.AthletesID
                                   select elegb).ToList();
            }
            else
            {
                ElegibleAhelete = this.AllEligibleAthletsList;
            }
            if (StartAgeRange > 0 && EndAgeRange > 0)
            {
                ElegibleAhelete = (from elegb in ElegibleAhelete
                                   join ath in this.AllAthletsList.Where(p => p.Age >= StartAgeRange && p.Age <= EndAgeRange) on elegb.AthletesID equals ath.AthletesID
                                   select elegb).ToList();

            }
            else if (StartAgeRange > 0)
            {
                ElegibleAhelete = (from elegb in ElegibleAhelete
                                   join ath in this.AllAthletsList.Where(p => p.Age >= StartAgeRange) on elegb.AthletesID equals ath.AthletesID
                                   select elegb).ToList();
            }
            else if (EndAgeRange > 0)
            {
                ElegibleAhelete = (from elegb in ElegibleAhelete
                                   join ath in this.AllAthletsList.Where(p => p.Age >= EndAgeRange) on elegb.AthletesID equals ath.AthletesID
                                   select elegb).ToList();
            }
            EligibleAthletsList = (from elegb in this.AllAthletsList
                                   join ath in ElegibleAhelete on elegb.AthletesID equals ath.AthletesID
                                   select elegb).Distinct().ToList();
            if (SelectedRelayEvent != null)
            {
                EligibleAthletsList = (from elegb in EligibleAthletsList
                                       join ath in ElegibleAhelete.Where(p => p.EventID == SelectedRelayEvent.EventID) on elegb.AthletesID equals ath.AthletesID
                                       select elegb).Distinct().ToList();
            }
        }

        //private List<Athlete> _EligibleAthletsAllList = new List<Athlete>();
        //public List<Athlete> EligibleAthletsAllList
        //{
        //    get { return _EligibleAthletsAllList; }
        //    set
        //    {
        //        SetProperty(ref _EligibleAthletsAllList, value);
        //    }
        //}

        private List<AddAthletesViewModel> _EligibleAthletsList = new List<AddAthletesViewModel>();
        public List<AddAthletesViewModel> EligibleAthletsList
        {
            get { return _EligibleAthletsList; }
            set
            {
                SetProperty(ref _EligibleAthletsList, value);
            }
        }
        private List<AddAthletesViewModel> _AllAthletsList = new List<AddAthletesViewModel>();
        public List<AddAthletesViewModel> AllAthletsList
        {
            get { return _AllAthletsList; }
            set
            {
                SetProperty(ref _AllAthletsList, value);
            }
        }
        private List<AthletiesElegible> _AllEligibleAthletsList = new List<AthletiesElegible>();
        public List<AthletiesElegible> AllEligibleAthletsList
        {
            get { return _AllEligibleAthletsList; }
            set
            {
                SetProperty(ref _AllEligibleAthletsList, value);
            }
        }
        private AthletiesElegible _SelectedEligibleAthlete;
        public AthletiesElegible SelectedEligibleAthlete
        {
            get { return _SelectedEligibleAthlete; }
            set
            {
                SetProperty(ref _SelectedEligibleAthlete, value);
            }
        }


        private bool _IsAllChecked;
        public bool IsAllChecked
        {
            get { return _IsAllChecked; }
            set
            {
                SetProperty(ref _IsAllChecked, value);
                this.FilterEvent();
            }
        }

        private void FilterEvent()
        {
            //Girls
            //Women
            //Boys
            //Men
            //Mixed

            if (IsMixedChecked)
            {
                this.RelayEventList = this.RelayEventAllList.Where(p => p.Gender == "Mixed").ToList();
            }
            else if (IsMaleChecked)
            {
                this.RelayEventList = this.RelayEventAllList.Where(p => p.Gender == "Men" || p.Gender == "Boys").ToList();
            }
            else if (IsFemaleChecked)
            {
                this.RelayEventList = this.RelayEventAllList.Where(p => p.Gender == "Women" || p.Gender == "Girls").ToList();
            }
            else
            {
                this.RelayEventList = this.RelayEventAllList;
            }
            if (StartAgeRange > 0 && EndAgeRange > 0)
            {
                this.RelayEventList = this.RelayEventList.Where(p => p.StartAgeRange >= StartAgeRange && p.EndAgeRange <= EndAgeRange).ToList();
            }
            else if (StartAgeRange > 0)
            {
                this.RelayEventList = this.RelayEventList.Where(p => p.StartAgeRange >= StartAgeRange).ToList();
            }
            else if (EndAgeRange > 0)
            {
                this.RelayEventList = this.RelayEventList.Where(p => p.EndAgeRange >= EndAgeRange).ToList();
            }
            FilterElegibleAtheletes();
        }

        private bool _IsMixedChecked;
        public bool IsMixedChecked
        {
            get { return _IsMixedChecked; }
            set
            {
                SetProperty(ref _IsMixedChecked, value);
                this.FilterEvent();
            }
        }
        private bool _IsMaleChecked;
        public bool IsMaleChecked
        {
            get { return _IsMaleChecked; }
            set
            {
                SetProperty(ref _IsMaleChecked, value);
                this.FilterEvent();
            }
        }
        private bool _IsFemaleChecked;
        public bool IsFemaleChecked
        {
            get { return _IsFemaleChecked; }
            set
            {
                SetProperty(ref _IsFemaleChecked, value);
                this.FilterEvent();
            }
        }
        private int _StartAgeRange;
        public int StartAgeRange
        {
            get { return _StartAgeRange; }
            set
            {
                SetProperty(ref _StartAgeRange, value);
                this.FilterEvent();
            }
        }
        private int _EndAgeRange;
        public int EndAgeRange
        {
            get { return _EndAgeRange; }
            set
            {
                SetProperty(ref _EndAgeRange, value);
                this.FilterEvent();
            }
        }
        private ObservableCollection<AddRelayViewModel> _RelayDetailList = new ObservableCollection<AddRelayViewModel>();
        public ObservableCollection<AddRelayViewModel> RelayDetailList
        {
            get { return _RelayDetailList; }
            set
            {
                SetProperty(ref _RelayDetailList, value);
            }
        }
        private AddRelayViewModel _SelectedRelayDetail;
        public AddRelayViewModel SelectedRelayDetail
        {
            get { return _SelectedRelayDetail; }
            set
            {
                SetProperty(ref _SelectedRelayDetail, value);
                this.InitilizeFinAndRelay();
            }
        }

        private void InitilizeFinAndRelay()
        {
            RelayFinalList.Clear();
            RelayPrelimList.Clear();
            if (this.SelectedRelayDetail == null) return;
            List<RelayPrelimOrder> prelimOrderList = AthletesManager.GetRelayOrderPreLim(this.SelectedRelayDetail.RelayID).Result.ToList();
            foreach (RelayPrelimOrder item in prelimOrderList)
            {
                PrelimAthelete prelim = new PrelimAthelete();
                prelim.RelayID = item.RelayID;
                prelim.AtheleteID = item.AthleteID;
                prelim.Position = item.Position;
                AddAthletesViewModel athlete = this.AllAthletsList.Where(p => p.AthletesID == item.AthleteID).FirstOrDefault();
                if (athlete != null)
                {
                    prelim.CompID = athlete.CompetitorNumber;
                    prelim.AtheleteName = athlete.FirstName;
                }
                RelayPrelimList.Add(prelim);
            }

            List<RelayFinalOrder> finalOrderList = AthletesManager.GetRelayOrderPreFinal(this.SelectedRelayDetail.RelayID).Result.ToList();
            foreach (RelayFinalOrder item in finalOrderList)
            {
                PrelimAthelete prelim = new PrelimAthelete();
                prelim.RelayID = item.RelayID;
                prelim.AtheleteID = item.AthleteID;
                prelim.Position = item.Position;
                AddAthletesViewModel athlete = this.AllAthletsList.Where(p => p.AthletesID == item.AthleteID).FirstOrDefault();
                if (athlete != null)
                {
                    prelim.CompID = athlete.CompetitorNumber;
                    prelim.AtheleteName = athlete.FirstName;
                }
                RelayFinalList.Add(prelim);
            }
        }

        private bool ValidateInputs()
        {
            return true;

        }

        public void SetEntity(ManageRelayViewModel entity)
        {
        }
        private ObservableCollection<AddRelayViewModel> _RelayListList;
        public ObservableCollection<AddRelayViewModel> RelayListList
        {
            get { return _RelayListList; }
            set
            {
                SetProperty(ref _RelayListList, value);
            }
        }

        private ObservableCollection<RelayMenuItemViewModel> _MenuItems;
        public ObservableCollection<RelayMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }
        public ManageRelayViewModel GetEntity()
        {
            return this;
        }

       
        #region COmmand
        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }
        private ICommand _AddRelayCommand;
        public ICommand AddRelayCommand
        {
            get
            {
                return _AddRelayCommand;
            }
        }
        private ICommand _CopyPreToFinCommand;
        public ICommand CopyPreToFinCommand
        {
            get
            {
                return _CopyPreToFinCommand;
            }
        }
        private ICommand _CopyFinToPreCommand;
        public ICommand CopyFinToPreCommand
        {
            get
            {
                return _CopyFinToPreCommand;
            }
        }


        #endregion
    }

}

