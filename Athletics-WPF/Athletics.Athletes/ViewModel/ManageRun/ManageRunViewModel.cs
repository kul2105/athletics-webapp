﻿using Athletics.Athletes;
using Athletics.Athletes.Model;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using Athletics.MeetSetup.Services.API;
using Athletics.Entities.Models;
using Athletics.Athletes.View.ManageRun;
using GalaSoft.MvvmLight.Command;
using Athletics.Athletes.Behaviors;

namespace Athletics.Athletes.ViewModels
{
    public class AthleteLane : BindableBase
    {
        public delegate void CompetertorNameChange(string competiterName);
        public static event CompetertorNameChange CompetertorNameChanged;
        private int _AthleteInLaneID;
        public int AthleteInLaneID
        {
            get { return _AthleteInLaneID; }
            set
            {
                SetProperty(ref _AthleteInLaneID, value);
            }
        }
        private int _LaneNumber;
        public int LaneNumber
        {
            get { return _LaneNumber; }
            set
            {
                SetProperty(ref _LaneNumber, value);
            }
        }
        private string _CompetitorNumber;
        public string CompetitorNumber
        {
            get { return _CompetitorNumber; }
            set
            {
                SetProperty(ref _CompetitorNumber, value);
                if (CompetertorNameChanged != null)
                {
                    CompetertorNameChanged(value);
                }
            }
        }
        public void SetCompetitorNumber(string competitorNumber)
        {
            _CompetitorNumber = competitorNumber;
            base.OnPropertyChanged("CompetitorNumber");
        }
        private string _AthleteName;
        public string AthleteName
        {
            get { return _AthleteName; }
            set
            {
                SetProperty(ref _AthleteName, value);
            }
        }
        private string _ClassYR;
        public string ClassYR
        {
            get { return _ClassYR; }
            set
            {
                SetProperty(ref _ClassYR, value);
            }
        }
        private string _SchoolName;
        public string SchoolName
        {
            get { return _SchoolName; }
            set
            {
                SetProperty(ref _SchoolName, value);
            }
        }
        private string _SamiTime;
        public string SamiTime
        {
            get { return _SamiTime; }
            set
            {
                SetProperty(ref _SamiTime, value);
            }
        }
        private string _FnalTime;
        public string FnalTime
        {
            get { return _FnalTime; }
            set
            {
                SetProperty(ref _FnalTime, value);
            }
        }
        private bool _IsDQ;
        public bool IsDQ
        {
            get { return _IsDQ; }
            set
            {
                SetProperty(ref _IsDQ, value);
            }
        }
        private bool _IsExh;
        public bool IsExh
        {
            get { return _IsExh; }
            set
            {
                SetProperty(ref _IsExh, value);
            }
        }
        private int _HPL;
        public int HPL
        {
            get { return _HPL; }
            set
            {
                SetProperty(ref _HPL, value);
            }
        }
        private int _PL;
        public int PL
        {
            get { return _PL; }
            set
            {
                SetProperty(ref _PL, value);
            }
        }
        private int _PTS;
        public int PTS
        {
            get { return _PTS; }
            set
            {
                SetProperty(ref _PTS, value);
            }
        }
        private int _EventID;
        public int EventID
        {
            get { return _EventID; }
            set
            {
                SetProperty(ref _EventID, value);
            }
        }
        private int _AthleteID;
        public int AthleteID
        {
            get { return _AthleteID; }
            set
            {
                SetProperty(ref _AthleteID, value);
            }
        }

        private bool _IsDeleted = false;
        public bool IsDeleted
        {
            get { return _IsDeleted; }
            set
            {
                SetProperty(ref _IsDeleted, value);
            }
        }
    }
    public class ManageRunViewModel : BindableBase, IManageRunViewModel, IGenericInteractionView<ManageRunViewModel>
    {
        private IManageRunView View = null;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        List<AddAthletesViewModel> AthleteList = new List<AddAthletesViewModel>();
        public ManageRunViewModel(IUnityContainer unity, IManageRunView view, IAthletesManager athletesManager)
        {
            View = view;
            unityContainer = unity;
            AthletesManager = athletesManager;
            View.DataContext = this;
            List<Entities.Models.EventDetail> EventDetailList = AthletesManager.GetAllEventDetail().Result.ToList();
            foreach (Entities.Models.EventDetail item in EventDetailList)
            {
                AllEventList.Add(item);
            }
            EventList = AllEventList;

            List<AthleteInLane> AthleteInLaneList = AthletesManager.GetAllAthletesInEventList().Result.ToList();
            foreach (AthleteInLane item in AthleteInLaneList)
            {
                AthleteLane laneAth = new AthleteLane();
                laneAth.AthleteID = item.AthleteID == null ? 0 : item.AthleteID.Value;
                laneAth.AthleteInLaneID = item.AthleteInLaneID;
                laneAth.AthleteName = item.AthleteName;
                laneAth.ClassYR = item.ClassYR;
                laneAth.SetCompetitorNumber(item.CompetitorNumber);
                laneAth.EventID = item.EventID == null ? 0 : item.EventID.Value;
                laneAth.FnalTime = item.FnalTime;
                laneAth.HPL = item.HPL == null ? 0 : item.HPL.Value;
                laneAth.IsDQ = item.IsDQ == null ? false : item.IsDQ.Value;
                laneAth.IsExh = item.IsExh == null ? false : item.IsExh.Value;
                laneAth.LaneNumber = item.LaneNumber == null ? 0 : item.LaneNumber.Value;
                laneAth.PL = item.PL == null ? 0 : item.PL.Value;
                laneAth.PTS = item.PTS == null ? 0 : item.PTS.Value;
                laneAth.SamiTime = item.SamiTime;
                laneAth.SchoolName = item.SchoolName;
                AllAddedAthletesList.Add(laneAth);
            }
            List<EventRecord> EventRecordList = AthletesManager.GetAllEventRecordList().Result.ToList();
            foreach (EventRecord item in EventRecordList)
            {
                AllEventRecordList.Add(item);
            }

            AthleteList = AthletesManager.GetAllAthleties().Result.ToList();
            _FilterSessionCommand = new MeetCommandViewModel(FilterSessionCommandHandler);
            _ClearFilterCommand = new MeetCommandViewModel(ClearFilterCommandHandler);
            AthleteLane.CompetertorNameChanged += AthleteLane_CompetertorNameChanged;
            _DeleteHeatCommand = new RelayCommand(DeleteHeatHandler);
            ItemsDragDropCommand = new RelayCommand<DataGridDragDropEventArgs>((args) => DragDropItem(args), (args) => args != null && args.TargetObject != null && args.DroppedObject != null && args.Effects != System.Windows.DragDropEffects.None);
        }

        private void DeleteHeatHandler()
        {
            if (SelectedAthlete != null)
            {

                AthleteLane lane = this.AllAddedAthletesList.Where(p => p.AthleteID == SelectedAthlete.AthleteID).FirstOrDefault();
                if (lane != null)
                {
                    lane.IsDeleted = true;
                }
                AthleteLane lane1 = AllAddedAthletesList.Where(p => p.AthleteID == SelectedAthlete.AthleteID && p.EventID == SelectedAthlete.EventID).FirstOrDefault();
                if (SelectedAthlete.AthleteInLaneID > 0)
                {
                    AthletesManager.DeleteAthletesHeatToEvent(SelectedAthlete.AthleteInLaneID);
                }
                this.AthletesList.Remove(SelectedAthlete);
                this.AllAddedAthletesList.Remove(lane1);


                this.FilterEventRecords(SelectedEvent.EventID);
            }
        }


        private void DragDropItem(DataGridDragDropEventArgs args)
        {
            if (args.DroppedObject.GetType() == typeof(AthleteLane))
            {
                AthleteLane athleteLaneDropped = args.DroppedObject as AthleteLane;
                //if (athleteLaneDropped != null)
                //{
                //    SelectedEventDetail = athleteLaneDropped;
                //    this.AddEventToSchedule();
                //}
            }
        }

        private void AthleteLane_CompetertorNameChanged(string competiterName)
        {
            if (this.SelectedEvent == null)
            {
                MessageBox.Show("Please select Event");
                return;
            }
            AddAthletesViewModel athleteViewModel = AthleteList.Where(p => p.CompetitorNumber == competiterName).FirstOrDefault();
            if (athleteViewModel != null)
            {
                this.SelectedAthlete.AthleteName = athleteViewModel.FirstName + "" + athleteViewModel.LastName;
                this.SelectedAthlete.ClassYR = athleteViewModel.ClassYR;
                this.SelectedAthlete.SchoolName = athleteViewModel.SchoolName;
                this.SelectedAthlete.EventID = this.SelectedEvent.EventID;
                this.SelectedAthlete.AthleteID = athleteViewModel.AthletesID;
                if (this.SelectedAthlete.LaneNumber <= 0)
                {
                    this.SelectedAthlete.LaneNumber = this.AthletesList.Count + 1;
                }
            }
            PreserveAlreadyAddedHeat(this.SelectedEvent.EventID);
        }

        private void ClearFilterCommandHandler()
        {
            EventList = AllEventList;
        }

        private string _EventText = "EVENT LIST All Events(Session not selected)";
        public string EventText
        {
            get { return _EventText; }
            set
            {
                SetProperty(ref _EventText, value);
            }
        }
        private List<Entities.Models.EventDetail> _AllEventList = new List<Entities.Models.EventDetail>();
        public List<Entities.Models.EventDetail> AllEventList
        {
            get { return _AllEventList; }
            set
            {
                SetProperty(ref _AllEventList, value);
            }
        }
        private List<Entities.Models.EventDetail> _EventList = new List<Entities.Models.EventDetail>();
        public List<Entities.Models.EventDetail> EventList
        {
            get { return _EventList; }
            set
            {
                SetProperty(ref _EventList, value);
            }
        }
        private Entities.Models.EventDetail _SelectedEvent = new Entities.Models.EventDetail();
        public Entities.Models.EventDetail SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                PreserveAlreadyAddedHeat(value.EventID);
                this.AthletesList.Clear();
                SetProperty(ref _SelectedEvent, value);
                FilterEventRecords(value.EventID);
            }
        }

        private void PreserveAlreadyAddedHeat(int eventID)
        {
            bool isNew = false;
            foreach (var item in this.AthletesList)
            {
                if (item.AthleteID <= 0) continue;
                AthleteLane laneAth = AllAddedAthletesList.Where(p => p.AthleteID == item.AthleteID).FirstOrDefault();
                isNew = false;
                if (laneAth == null)
                {
                    isNew = true;
                    laneAth = new AthleteLane();
                }
                laneAth.AthleteID = item.AthleteID;
                laneAth.AthleteInLaneID = item.AthleteInLaneID;
                laneAth.AthleteName = item.AthleteName;
                laneAth.ClassYR = item.ClassYR;
                laneAth.SetCompetitorNumber(item.CompetitorNumber);
                laneAth.EventID = item.EventID;
                laneAth.FnalTime = item.FnalTime;
                laneAth.HPL = item.HPL;
                laneAth.IsDQ = item.IsDQ;
                laneAth.IsExh = item.IsExh;
                laneAth.LaneNumber = item.LaneNumber;
                laneAth.PL = item.PL;
                laneAth.PTS = item.PTS;
                laneAth.SamiTime = item.SamiTime;
                laneAth.SchoolName = item.SchoolName;
                if (isNew == true)
                {
                    this.AllAddedAthletesList.Add(laneAth);
                }
            }
        }

        private void FilterEventRecords(int eventID)
        {
            this.AthletesList.Clear();
            List<AthleteLane> LaneInEventList = this.AllAddedAthletesList.Where(p => p.EventID == eventID).ToList();
            int laneNumber = 0;
            if (LaneInEventList.Count > 0)
            {
                laneNumber = LaneInEventList.Max(p => p.LaneNumber);
            }
            if (laneNumber < 10)
            {
                laneNumber = 10;
            }
            for (int i = 1; i <= laneNumber; i++)
            {
                AthleteLane item = LaneInEventList.Where(p => p.LaneNumber == i).FirstOrDefault();
                if (item == null)
                {
                    item = new AthleteLane();
                    item.LaneNumber = i;
                    item.EventID = eventID;
                }
                AthleteLane laneAth = new AthleteLane();
                laneAth.AthleteID = item.AthleteID;
                laneAth.AthleteInLaneID = item.AthleteInLaneID;
                laneAth.AthleteName = item.AthleteName;
                laneAth.ClassYR = item.ClassYR;
                laneAth.SetCompetitorNumber(item.CompetitorNumber);
                laneAth.EventID = item.EventID;
                laneAth.FnalTime = item.FnalTime;
                laneAth.HPL = item.HPL;
                laneAth.IsDQ = item.IsDQ;
                laneAth.IsExh = item.IsExh;
                laneAth.LaneNumber = item.LaneNumber;
                laneAth.PL = item.PL;
                laneAth.PTS = item.PTS;
                laneAth.SamiTime = item.SamiTime;
                laneAth.SchoolName = item.SchoolName;
                this.AthletesList.Add(laneAth);
            }

            this.EventRecordList.Clear();
            foreach (var item in this.AllEventRecordList.Where(p => p.EventID == eventID))
            {
                this.EventRecordList.Add(item);
            }
            this.TeamRankingList.Clear();
            List<GetTeamPointDetail_Result> TeamPointList = AthletesManager.GetTeamPointDetailByEventID(eventID).Result;
            foreach (var item in TeamPointList)
            {
                this.TeamRankingList.Add(item);
            }
        }

        private ObservableCollection<AthleteInLane> _AllAthletesList = new ObservableCollection<AthleteInLane>();
        public ObservableCollection<AthleteInLane> AllAthletesList
        {
            get { return _AllAthletesList; }
            set { SetProperty(ref _AllAthletesList, value); }
        }
        private ObservableCollection<AthleteLane> _AthletesList = new ObservableCollection<AthleteLane>();
        public ObservableCollection<AthleteLane> AthletesList
        {
            get { return _AthletesList; }
            set { SetProperty(ref _AthletesList, value); }
        }
        private ObservableCollection<AthleteLane> _AllAddedAthletesList = new ObservableCollection<AthleteLane>();
        public ObservableCollection<AthleteLane> AllAddedAthletesList
        {
            get { return _AllAddedAthletesList; }
            set { SetProperty(ref _AllAddedAthletesList, value); }
        }
        private AthleteLane _SelectedAthlete = new AthleteLane();
        public AthleteLane SelectedAthlete
        {
            get { return _SelectedAthlete; }
            set { SetProperty(ref _SelectedAthlete, value); }
        }

        private ObservableCollection<EventRecord> _EventRecordList = new ObservableCollection<EventRecord>();
        public ObservableCollection<EventRecord> EventRecordList
        {
            get { return _EventRecordList; }
            set { SetProperty(ref _EventRecordList, value); }
        }
        private ObservableCollection<EventRecord> _AllEventRecordList = new ObservableCollection<EventRecord>();
        public ObservableCollection<EventRecord> AllEventRecordList
        {
            get { return _AllEventRecordList; }
            set { SetProperty(ref _AllEventRecordList, value); }
        }
        private EventRecord _SelectedEventRecord = new EventRecord();
        public EventRecord SelectedEventRecord
        {
            get { return _SelectedEventRecord; }
            set { SetProperty(ref _SelectedEventRecord, value); }
        }
        private ObservableCollection<GetTeamPointDetail_Result> _TeamRankingList = new ObservableCollection<GetTeamPointDetail_Result>();
        public ObservableCollection<GetTeamPointDetail_Result> TeamRankingList
        {
            get { return _TeamRankingList; }
            set { SetProperty(ref _TeamRankingList, value); }
        }
        private GetTeamPointDetail_Result _SelectedTeamRanking = new GetTeamPointDetail_Result();
        public GetTeamPointDetail_Result SelectedTeamRanking
        {
            get { return _SelectedTeamRanking; }
            set { SetProperty(ref _SelectedTeamRanking, value); }
        }

        public RelayCommand<DataGridDragDropEventArgs> ItemsDragDropCommand { get; private set; }

        public void SetEntity(ManageRunViewModel entity)
        {

        }

        public ManageRunViewModel GetEntity()
        {
            return this;
        }

        #region command
        private ICommand _FilterSessionCommand;
        public ICommand FilterSessionCommand
        {
            get
            {
                return _FilterSessionCommand;
            }
        }
        private ICommand _ClearFilterCommand;
        public ICommand ClearFilterCommand
        {
            get
            {
                return _ClearFilterCommand;
            }
        }
        private ICommand _DeleteHeatCommand;
        public ICommand DeleteHeatCommand
        {
            get
            {
                return _DeleteHeatCommand;
            }
        }


        #endregion

        #region Command Handler

        private void FilterSessionCommandHandler()
        {
            FilterSessionView filterSession = new FilterSessionView();
            filterSession.ShowDialog();
            this.FilterEvent(filterSession.EventIds, filterSession.sessionNames);

        }

        private void FilterEvent(List<int> eventIds, string sessionName)
        {
            this.EventList = this.AllEventList.Where(p => eventIds.Contains(p.EventID)).ToList();
            EventText = "EVENT LIST All Events(" + sessionName + ")";
        }

        internal void UpdateHeat()
        {
            foreach (var item in AllAddedAthletesList)
            {
                ServiceRequest_RunHeat heat = new ServiceRequest_RunHeat();
                heat.Lane = item;
                if (heat.Lane.AthleteID > 0)
                    AthletesManager.AddAthletesHeatToEvent(heat);
            }
        }
        #endregion

    }

}

