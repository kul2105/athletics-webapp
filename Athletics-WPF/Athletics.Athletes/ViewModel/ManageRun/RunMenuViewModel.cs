﻿using Athletics.Athletes;
using Athletics.Athletes.Model;
using Athletics.Athletes.ViewModels;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class RunMenuViewModel : BindableBase, IRunMenuViewModel
    {
        private ObservableCollection<RelayMenuItemViewModel> _MenuItems = new ObservableCollection<RelayMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IAthletesManager athleticManager;
        IManageRunView View;
        IManageRunViewModel ViewModel;
        IRunMenuView RunMenuView;
        public ObservableCollection<RelayMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public RunMenuViewModel(IUnityContainer unity, IRunMenuView _View, IManageRunView ManageAthleteView, IAthletesManager manager)
        {
            athleticManager = manager;
            View = ManageAthleteView;
            ViewModel = ManageAthleteView.DataContext as IManageRunViewModel;
            RunMenuView = _View;
            RunMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<RelayMenuItemViewModel>();
                RelayMenuItemViewModel eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Events";
                _EventsCommand = new MeetCommandViewModel(EventsCommandHandler);
                eventMenu.Command = EventsCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Atheletes";
                _AtheletesCommand = new MeetCommandViewModel(AtheletesHandler);
                eventMenu.Command = AtheletesCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Relay";
                _RelayCommand = new MeetCommandViewModel(RelayCommandHandler);
                eventMenu.Command = RelayCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Team";
                _TeamCommand = new MeetCommandViewModel(TeamCommandandler);
                eventMenu.Command = TeamCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Seeding";
                _SeedingCommand = new MeetCommandViewModel(SeedingCommandHandler);
                eventMenu.Command = SeedingCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new RelayMenuItemViewModel();
                eventMenu.Header = "Help";
                _HelpCommand = new MeetCommandViewModel(HelpCommandHandler);
                eventMenu.Command = HelpCommand;
                MenuItems.Add(eventMenu);

                #endregion
            });
            unityContainer = unity;
            //View.DataContext = this;

        }




        #region Menu Command
        private ICommand _EventsCommand;
        public ICommand EventsCommand
        {
            get
            {
                return _EventsCommand;
            }
        }

        private ICommand _AtheletesCommand;
        public ICommand AtheletesCommand
        {
            get
            {
                return _AtheletesCommand;
            }
        }

        private ICommand _RelayCommand;
        public ICommand RelayCommand
        {
            get
            {
                return _RelayCommand;
            }
        }


        private ICommand _TeamCommand;
        public ICommand TeamCommand
        {
            get
            {
                return _TeamCommand;
            }
        }

        private ICommand _SeedingCommand;
        public ICommand SeedingCommand
        {
            get
            {
                return _SeedingCommand;
            }
        }

        private ICommand _HelpCommand;
        public ICommand HelpCommand
        {
            get
            {
                return _HelpCommand;
            }
        }
        #endregion

        #region Command Handler

        private void SeedingCommandHandler()
        {
            Events.RaiseOpenWindow(Windows.Seeding);
        }

        private void RelayCommandHandler()
        {
            Events.RaiseOpenWindow(Windows.Relay);
        }

        private void HelpCommandHandler()
        {
            MessageBox.Show("Help");
        }


        private void TeamCommandandler()
        {
            Events.RaiseOpenWindow(Windows.Team);
        }

        private void AtheletesHandler()
        {
            Events.RaiseOpenWindow(Windows.Athlete);
        }

        private void EventsCommandHandler()
        {

            Events.RaiseOpenWindow(Windows.Event);
        }

        #endregion

    }
}

