﻿using Athletics.Athletes;
using Athletics.Athletes.Model;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using Athletics.MeetSetup.Services.API;
using Athletics.Entities.Models;

namespace Athletics.Athletes.ViewModels
{

    public class ManageAthletesViewModel : BindableBase, IManageAthletesViewModel, IGenericInteractionView<ManageAthletesViewModel>
    {
        private IManageAthletesView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public ManageAthletesViewModel(IUnityContainer unity, IManageAthletesView view, IAthletesManager athletesManager)
        {
            View = view;
            unityContainer = unity;
            AthletesManager = athletesManager;
            View.DataContext = this;


            List<AddAthletesViewModel> AthleticsList = AthletesManager.GetAllAthleties().Result.ToList();
            foreach (AddAthletesViewModel item in AthleticsList)
            {
                AllAthleticsDetailList.Add(item);
            }

            List<Team> teamList = AthletesManager.GetAllTeams().Result.ToList();
            foreach (Team item in teamList)
            {
                SchoolList.Add(item);
            }
            FilterAtheletes();
            _AddCommand = new MeetCommandViewModel(AddCommandHandler);
            _EditCommand = new MeetCommandViewModel(EditCommandHandler);
            _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
        }

        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    //await Task.Run(async () =>
                    //{
                    if (this.SelectedAthleticsDetail.AthletesID > 0)
                    {
                        if (this.SelectedAthleticsDetail == null) return;
                        AuthorizedResponse<AthletiesResult> Response = await AthletesManager.DeleteAthleties(this.SelectedAthleticsDetail.AthletesID);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleteing the Athlete", "Event Delete Error");
                            return;
                        }
                        //this.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;
                        this.AthleticsDetailList.Remove(this.SelectedAthleticsDetail);
                        Response = await AthletesManager.DeleteAthletiesElegible(SelectedAthleticsDetail.AthletesID);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleting the Elegible", "Athlete Add Error");
                            return;
                        }
                    }
                    //});

                })).Wait();
            });

        }

        private void EditCommandHandler()
        {
            if (this.SelectedAthleticsDetail == null) return;
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddAthletesView();
            window.Name = "AddEvents";
            IAddAthletesView eventView = window as IAddAthletesView;
            AddAthletesViewModel addEventViewModel = new AddAthletesViewModel(this.unityContainer, eventView, this.AthletesManager, this.AthleticsDetailList, this.SelectedAthleticsDetail);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            if (addEventViewModel != null)
            {
                IAddAthletesViewModel addEvent = addEventViewModel as IAddAthletesViewModel;
                addEvent.Mode = AthleteMode.Add;
                addEvent.AllAthleticsDetailList = AllAthleticsDetailList;
                dialog.BindView(window);
                dialog.BindViewModel(addEvent);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            addEventViewModel = addEventViewModel.GetEntity();
            //Task.Run(async () =>
            //{
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                //await Task.Run(async () =>
                //{
                if (this.SelectedAthleticsDetail.AthletesID > 0)
                {

                    ServiceRequest_Athleties serviceObj = new ServiceRequest_Athleties();
                    serviceObj.Athleties = addEventViewModel;
                    AuthorizedResponse<AthletiesResult> Response = await AthletesManager.UpdateAthleties(serviceObj);
                    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show("Error while Updating the Athlete", "Athlete Add Error");
                        return;
                    }
                    await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            this.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;
                            this.AthleticsDetailList.Remove(this.SelectedAthleticsDetail);
                            this.AthleticsDetailList.Add(addEventViewModel);
                            this.SelectedAthleticsDetail = addEventViewModel;
                        }));
                    Response = await AthletesManager.DeleteAthletiesElegible(addEventViewModel.AthletesID);
                    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show("Error while deleting the Elegible", "Athlete Add Error");
                        return;
                    }
                    foreach (var item in addEventViewModel.AthleteElegibleList)
                    {
                        item.AthletesID = addEventViewModel.AthletesID;
                        ServiceRequest_AthletiesElegible atheleteEleg = new ServiceRequest_AthletiesElegible();
                        atheleteEleg.AthletiesID = item.AthletesID.Value;
                        atheleteEleg.Elegible = item;
                        Response = await AthletesManager.CreateNewElegibleAthleties(atheleteEleg);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while Adding the Elegible", "Athlete Add Error");
                            return;
                        }
                    }
                }
                else
                {
                }

            }));

            //})).Wait();
            //});

        }

        private void AddCommandHandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddAthletesView();
            window.Name = "AddEvents";
            IAddAthletesView eventView = window as IAddAthletesView;
            AddAthletesViewModel addEventViewModel = new AddAthletesViewModel(this.unityContainer, eventView, this.AthletesManager, this.AthleticsDetailList, this.SelectedAthleticsDetail);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            this.ClearViewModel(addEventViewModel);
            if (addEventViewModel != null)
            {
                IAddAthletesViewModel addEvent = addEventViewModel as IAddAthletesViewModel;
                addEvent.AllAthleticsDetailList = AllAthleticsDetailList;
                addEvent.Mode = AthleteMode.Add;
                dialog.BindView(window);
                dialog.BindViewModel(addEventViewModel);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            AddAthletesViewModel addeventVm = addEventViewModel.GetEntity();
            if (addEventViewModel.IsCanceled == true) return;
            //Task.Run(async () =>
            //{
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                //await Task.Run(async () =>
                //{
                //if (ViewModel.SelectedAthleticsDetail.AthletesID == 0)
                //{
                if (string.IsNullOrEmpty(addEventViewModel.FirstName)) return;
                ServiceRequest_Athleties serviceObj = new ServiceRequest_Athleties();
                serviceObj.Athleties = addEventViewModel;
                AuthorizedResponse<AthletiesResult> Response = await AthletesManager.CreateNewAthleties(serviceObj);
                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while adding the Athlete", "Athlete Add Error");
                    return;
                }
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                    {
                        addEventViewModel.AthletesID = Response.Result.AthletiesID;
                        this.AthleticsDetailList.Add(addEventViewModel);
                    }));
                Response = await AthletesManager.DeleteAthletiesElegible(addEventViewModel.AthletesID);
                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while deleting the Elegible", "Athlete Add Error");
                    return;
                }
                foreach (var item in addEventViewModel.AthleteElegibleList)
                {
                    item.AthletesID = addEventViewModel.AthletesID;
                    ServiceRequest_AthletiesElegible atheleteEleg = new ServiceRequest_AthletiesElegible();
                    atheleteEleg.AthletiesID = item.AthletesID.Value;
                    atheleteEleg.Elegible = item;
                    Response = await AthletesManager.CreateNewElegibleAthleties(atheleteEleg);
                    if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                    {
                        MessageBox.Show("Error while Adding the Elegible", "Athlete Add Error");
                        return;
                    }
                }

            }));

            //})).Wait();
            //});

        }
        private void ClearViewModel(IGenericInteractionView<AddAthletesViewModel> addEventViewModel)
        {
            addEventViewModel.SetEntity(new AddAthletesViewModel());
        }

        private List<AddAthletesViewModel> _AllAthleticsDetailList = new List<AddAthletesViewModel>();
        public List<AddAthletesViewModel> AllAthleticsDetailList
        {
            get { return _AllAthleticsDetailList; }
            set
            {
                SetProperty(ref _AllAthleticsDetailList, value);
            }
        }

        private List<AddAthletesViewModel> _AthleticsDetailList = new List<AddAthletesViewModel>();
        public List<AddAthletesViewModel> AthleticsDetailList
        {
            get { return _AthleticsDetailList; }
            set
            {
                SetProperty(ref _AthleticsDetailList, value);
            }
        }
        private AddAthletesViewModel _SelectedAthleticsDetail;
        public AddAthletesViewModel SelectedAthleticsDetail
        {
            get { return _SelectedAthleticsDetail; }
            set
            {
                SetProperty(ref _SelectedAthleticsDetail, value);
                if (value == null) return;
                if (value.AthletesID > 0)
                {
                    AthleteEntries.Clear();
                    List<AthletiesElegible> elegibliltyList = AthletesManager.GetAllElegibleEvents(value.AthletesID).Result;
                    foreach (var item in elegibliltyList)
                    {
                        AthleteEntries.Add(item);
                    }

                }
            }
        }
        private ObservableCollection<AthletiesElegible> _AthleteEntries = new ObservableCollection<AthletiesElegible>();
        public ObservableCollection<AthletiesElegible> AthleteEntries
        {
            get { return _AthleteEntries; }
            set
            {
                SetProperty(ref _AthleteEntries, value);
            }
        }

        private AthletiesElegible _SelectedEntry = new AthletiesElegible();
        public AthletiesElegible SelectedEntry
        {
            get { return _SelectedEntry; }
            set
            {
                SetProperty(ref _SelectedEntry, value);
            }
        }

        private bool _IsAllChecked;
        public bool IsAllChecked
        {
            get { return _IsAllChecked; }
            set
            {
                SetProperty(ref _IsAllChecked, value);
                this.FilterAtheletes();
            }
        }
        private bool _IsMaleChecked;
        public bool IsMaleChecked
        {
            get { return _IsMaleChecked; }
            set
            {
                SetProperty(ref _IsMaleChecked, value);
                this.FilterAtheletes();
            }
        }
        private bool _IsFemaleChecked;
        public bool IsFemaleChecked
        {
            get { return _IsFemaleChecked; }
            set
            {
                SetProperty(ref _IsFemaleChecked, value);
                this.FilterAtheletes();
            }
        }
        private int _StartAgeRange;
        public int StartAgeRange
        {
            get { return _StartAgeRange; }
            set
            {
                SetProperty(ref _StartAgeRange, value);
                this.FilterAtheletes();
            }
        }
        private int _EndAgeRange;
        public int EndAgeRange
        {
            get { return _EndAgeRange; }
            set
            {
                SetProperty(ref _EndAgeRange, value);
                this.FilterAtheletes();
            }
        }
        private List<Team> _SchoolList = new List<Team>();
        public List<Team> SchoolList
        {
            get { return _SchoolList; }
            set
            {
                SetProperty(ref _SchoolList, value);
            }
        }

        private Team _SelectedSchool;
        public Team SelectedSchool
        {
            get { return _SelectedSchool; }
            set
            {
                SetProperty(ref _SelectedSchool, value);
                if (value != null)
                {
                    this.SchoolName = value.TeamName;
                }
            }
        }
        private string _SchoolName;
        public string SchoolName
        {
            get { return _SchoolName; }
            set
            {
                SetProperty(ref _SchoolName, value);
                this.FilterAtheletes();
            }
        }
        private bool _IsElegibleChecked;
        public bool IsElegibleChecked
        {
            get { return _IsElegibleChecked; }
            set
            {
                SetProperty(ref _IsElegibleChecked, value);
                this.FilterAtheletes();
            }
        }
        private bool _IsEntriesChecked;
        public bool IsEntriesChecked
        {
            get { return _IsEntriesChecked; }
            set
            {
                SetProperty(ref _IsEntriesChecked, value);
                this.FilterAtheletes();
            }
        }
        private bool _IsCombinedSubEventOnlyChecked;
        public bool IsCombinedSubEventOnlyChecked
        {
            get { return _IsCombinedSubEventOnlyChecked; }
            set
            {
                SetProperty(ref _IsCombinedSubEventOnlyChecked, value);
                this.FilterAtheletes();
            }
        }
        private bool _IsAllEventChecked;
        public bool IsAllEventChecked
        {
            get { return _IsAllEventChecked; }
            set
            {
                SetProperty(ref _IsAllEventChecked, value);
                this.FilterAtheletes();
            }
        }
        private void FilterAtheletes()
        {
            if (IsMaleChecked)
            {
                this.AthleticsDetailList = this.AllAthleticsDetailList.Where(p => p.Gender == "M").ToList();
            }
            else if (IsFemaleChecked)
            {
                this.AthleticsDetailList = this.AllAthleticsDetailList.Where(p => p.Gender == "F").ToList();
            }
            else
            {
                this.AthleticsDetailList = this.AllAthleticsDetailList;
            }
            if (StartAgeRange > 0)
            {
                this.AthleticsDetailList = this.AthleticsDetailList.Where(p => p.Age <= this.StartAgeRange).ToList();
            }
            if (EndAgeRange > 0)
            {
                this.AthleticsDetailList = this.AthleticsDetailList.Where(p => p.Age <= this.EndAgeRange).ToList();
            }
            if (!string.IsNullOrEmpty(this.SchoolName))
            {
                this.AthleticsDetailList = this.AthleticsDetailList.Where(p => p.TeamString == SchoolName).ToList();
            }
            if (this.IsElegibleChecked)
            {
                this.AthleticsDetailList = this.AthleticsDetailList.Where(p => p.Age <= this.StartAgeRange && p.Age >= EndAgeRange).ToList();
            }
            else if (this.IsEntriesChecked)
            {
                List<int> AthleteIds = new List<int>();
                foreach (var item in AthleticsDetailList)
                {
                    int count = AthletesManager.GetAllElegibleEvents(item.AthletesID).Result.Count();
                    if (count > 0)
                    {
                        AthleteIds.Add(item.AthletesID);
                    }

                }
                this.AthleticsDetailList = this.AthleticsDetailList.Where(p => AthleteIds.Contains(p.AthletesID)).ToList();
            }
            else if (this.IsCombinedSubEventOnlyChecked)
            {
                // this.AthleticsDetailList = this.AthleticsDetailList.Where(p => p.Age <= this.StartAgeRange && p.Age >= EndAgeRange).ToList();
            }
            else if (this.IsAllEventChecked)
            {
                //this.AthleticsDetailList = this.AthleticsDetailList.Where(p => p.Age <= this.StartAgeRange && p.Age >= EndAgeRange).ToList();
            }

        }

        private bool ValidateInputs()
        {
            return true;

        }

        public void SetEntity(ManageAthletesViewModel entity)
        {
        }
        private ObservableCollection<AthletesMenuItemViewModel> _MenuItems;
        public ObservableCollection<AthletesMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }
        public ManageAthletesViewModel GetEntity()
        {
            return this;
        }
        #region COmmand
        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }
        #endregion
    }

}

