﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public interface IManageAthletesViewModel
    {
        List<AddAthletesViewModel> AthleticsDetailList { get; set; }
        AddAthletesViewModel SelectedAthleticsDetail { get; set; }
    }
}
