﻿using Athletics.Athletes;
using Athletics.Athletes.Model;
using Athletics.Athletes.ViewModels;
using Athletics.Athletes.Views;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup.Services.API;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.ViewModels
{
    public class AthleticsMenuViewModel : BindableBase, IAthleticsMenuViewModel
    {
        private ObservableCollection<AthletesMenuItemViewModel> _MenuItems = new ObservableCollection<AthletesMenuItemViewModel>();
        private readonly IUnityContainer unityContainer;
        IModalDialog dialog = null;
        Window window = null;
        IAthletesManager athleticManager;
        IManageAthletesView View;
        IManageAthletesViewModel ViewModel;
        IAthleticsMenuView AthleticMenuView;
        public ObservableCollection<AthletesMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        public AthleticsMenuViewModel(IUnityContainer unity, IAthleticsMenuView _View, IManageAthletesView ManageAthleteView, IAthletesManager manager)
        {
            athleticManager = manager;
            View = ManageAthleteView;
            ViewModel = ManageAthleteView.DataContext as IManageAthletesViewModel;
            AthleticMenuView = _View;
            AthleticMenuView.DataContext = this;
            Task.Run(async () =>
            {
                #region Initilize Command
                MenuItems = new ObservableCollection<AthletesMenuItemViewModel>();
                AthletesMenuItemViewModel eventMenu = new AthletesMenuItemViewModel();
                //eventMenu.Header = "Layout";
                //_LayoutCommand = new MeetCommandViewModel(LayoutCommandHandler);
                //eventMenu.Command = LayoutCommand;
                //MenuItems.Add(eventMenu);
                eventMenu = new AthletesMenuItemViewModel();
                eventMenu.Header = "Add";
                _AddCommand = new MeetCommandViewModel(AddCommandHandler);
                eventMenu.Command = AddCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new AthletesMenuItemViewModel();
                eventMenu.Header = "Edit";
                _EditCommand = new MeetCommandViewModel(EditCommandHandler);
                eventMenu.Command = EditCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new AthletesMenuItemViewModel();
                eventMenu.Header = "Delete";
                _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
                eventMenu.Command = DeleteCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new AthletesMenuItemViewModel();
                eventMenu.Header = "Contact";
                _ContactCommand = new MeetCommandViewModel(ContactCommandHandler);
                eventMenu.Command = ContactCommand;
                MenuItems.Add(eventMenu);

                //eventMenu = new AthletesMenuItemViewModel();
                //eventMenu.Header = "Camp#";
                //_CampCommand = new MeetCommandViewModel(CampCommandHandler);
                //eventMenu.Command = CampCommand;
                //MenuItems.Add(eventMenu);

                //eventMenu = new AthletesMenuItemViewModel();
                //eventMenu.Header = "ScratchAll";
                //_ScratchAllCommand = new MeetCommandViewModel(ScratchAllCommandHandler);
                //eventMenu.Command = ScratchAllCommand;
                //MenuItems.Add(eventMenu);


                //eventMenu = new AthletesMenuItemViewModel();
                //eventMenu.Header = "View";
                //_ViewCommand = new MeetCommandViewModel(ViewCommandandler);
                //eventMenu.Command = ViewCommand;
                //MenuItems.Add(eventMenu);

                //eventMenu = new AthletesMenuItemViewModel();
                //eventMenu.Header = "Find";
                //_ViewCommand = new MeetCommandViewModel(ViewCommandandler);
                //eventMenu.Command = FindCommand;
                //MenuItems.Add(eventMenu);

                eventMenu = new AthletesMenuItemViewModel();
                eventMenu.Header = "Print";
                _PrintCommand = new MeetCommandViewModel(PrintCommandandler);
                eventMenu.Command = PrintCommand;
                MenuItems.Add(eventMenu);

                eventMenu = new AthletesMenuItemViewModel();
                eventMenu.Header = "Export To HTML";
                _ExportToHTMLCommand = new MeetCommandViewModel(ExportToHTMLCommandHandler);
                eventMenu.Command = ExportToHTMLCommand;
                MenuItems.Add(eventMenu);

                //eventMenu = new AthletesMenuItemViewModel();
                //eventMenu.Header = "Events";
                //_EventsCommand = new MeetCommandViewModel(EventsCommandandler);
                //eventMenu.Command = EventsCommand;
                //MenuItems.Add(eventMenu);

                eventMenu = new AthletesMenuItemViewModel();
                eventMenu.Header = "Relay";
                _RelayCommand = new MeetCommandViewModel(RelayCommandandler);
                eventMenu.Command = RelayCommand;
                MenuItems.Add(eventMenu);

                //eventMenu = new AthletesMenuItemViewModel();
                //eventMenu.Header = "Schools";
                //_SchoolsCommand = new MeetCommandViewModel(SchoolsCommandandler);
                //eventMenu.Command = SchoolsCommand;
                //MenuItems.Add(eventMenu);

                eventMenu = new AthletesMenuItemViewModel();
                eventMenu.Header = "Help";
                _HelpCommand = new MeetCommandViewModel(HelpCommandHandler);
                eventMenu.Command = HelpCommand;
                MenuItems.Add(eventMenu);

                #endregion
            });
            unityContainer = unity;
            //View.DataContext = this;

        }
        #region Menu Command

        private ICommand _LayoutCommand;
        public ICommand LayoutCommand
        {
            get
            {
                return _LayoutCommand;
            }
        }

        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }

        private ICommand _ContactCommand;
        public ICommand ContactCommand
        {
            get
            {
                return _ContactCommand;
            }
        }

        private ICommand _CampCommand;
        public ICommand CampCommand
        {
            get
            {
                return _CampCommand;
            }
        }

        private ICommand _ScratchAllCommand;
        public ICommand ScratchAllCommand
        {
            get
            {
                return _ScratchAllCommand;
            }
        }
        private ICommand _CommentsCommand;
        public ICommand CommentsCommand
        {
            get
            {
                return _CommentsCommand;
            }
        }

        private ICommand _ReNumberCommand;
        public ICommand ReNumberCommand
        {
            get
            {
                return _ReNumberCommand;
            }
        }
        private ICommand _RecordCommand;
        public ICommand RecordCommand
        {
            get
            {
                return _RecordCommand;
            }
        }

        private ICommand _StandardCommand;
        public ICommand StandardCommand
        {
            get
            {
                return _StandardCommand;
            }
        }
        private ICommand _ViewCommand;
        public ICommand ViewCommand
        {
            get
            {
                return _ViewCommand;
            }
        }

        private ICommand _FindCommand;
        public ICommand FindCommand
        {
            get
            {
                return _FindCommand;
            }
        }

        private ICommand _PrintCommand;
        public ICommand PrintCommand
        {
            get
            {
                return _PrintCommand;
            }
        }
        private ICommand _EventsCommand;
        public ICommand EventsCommand
        {
            get
            {
                return _EventsCommand;
            }
        }
        private ICommand _RelayCommand;
        public ICommand RelayCommand
        {
            get
            {
                return _RelayCommand;
            }
        }
        private ICommand _SchoolsCommand;
        public ICommand SchoolsCommand
        {
            get
            {
                return _SchoolsCommand;
            }
        }



        private ICommand _ExportToHTMLCommand;
        public ICommand ExportToHTMLCommand
        {
            get
            {
                return _ExportToHTMLCommand;
            }
        }

        private ICommand _HelpCommand;
        public ICommand HelpCommand
        {
            get
            {
                return _HelpCommand;
            }
        }
        #endregion

        #region Command Handler
        private void SchoolsCommandandler()
        {

        }
        private void RelayCommandandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new ManageRelayView();
            window.Name = "ManageRelay";
            IGenericInteractionView<ManageRelayViewModel> relayVM = unityContainer.Resolve<IManageRelayViewModel>() as IGenericInteractionView<ManageRelayViewModel>;
            if (relayVM != null)
            {
                dialog.BindView(window);
                dialog.BindViewModel(relayVM);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
        }
        private void EventsCommandandler()
        {

        }
        private void HelpCommandHandler()
        {
            MessageBox.Show("Help");
        }

        private void ExportToHTMLCommandHandler()
        {
            View.EventGrid.SelectAllCells();
            View.EventGrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;

            ApplicationCommands.Copy.Execute(null, View.EventGrid);
            String result = (string)Clipboard.GetData(
                         DataFormats.Html);
            View.EventGrid.UnselectAll();
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document1"; // Default file name
            dlg.DefaultExt = ".html"; // Default file extension
            dlg.Filter = "Html documents (.html)|*.html"; // Filter files by extension
            Nullable<bool> resultDialog = dlg.ShowDialog();

            // Process save file dialog box results
            if (resultDialog == true)
            {
                // Save document
                string filename = dlg.FileName;
                System.IO.File.WriteAllText(filename, result);
            }


        }

        private void PrintCommandandler()
        {
            System.Windows.Controls.PrintDialog pdPrintDialog = new System.Windows.Controls.PrintDialog();
            if ((bool)pdPrintDialog.ShowDialog().GetValueOrDefault())
            {
                Size pntPageSize = new Size(pdPrintDialog.PrintableAreaWidth, pdPrintDialog.PrintableAreaHeight);
                View.EventGrid.Measure(pntPageSize);
                View.EventGrid.Arrange(new Rect(5, 5, pntPageSize.Width, pntPageSize.Height));
                pdPrintDialog.PrintVisual(View.EventGrid, "Events");
            }
        }

        private void ViewCommandandler()
        {
            MessageBox.Show("View");
        }

        private void FindCommandandler()
        {
            //ManageTimeMarkStandardView manageRecords = new ManageTimeMarkStandardView();
            //manageRecords.Name = "ManageTimeMarkStandardView";
            //manageRecords.ShowDialog();
            //manageRecords.Close();
        }


        private void ReNumberCommandandler()
        {
            // Re manageRecords = new ManageTimeMarkStandardView();
            // manageRecords.Name = "ManageTimeMarkStandardView";
            // manageRecords.ShowDialog();
            // manageRecords.Close();
        }


        private void ScratchAllCommandHandler()
        {
            ////dialog = ServiceProvider.Instance.Get<IModalDialog>();
            //ManageSessionView window1 = new ManageSessionView();
            //window1.Name = "Sessions";
            ////IManageSessionView sessions = window as IManageSessionView;
            //ManageSessionViewModel addEventViewModel = new ManageSessionViewModel(this.unityContainer, window1, this.AthletesManager);
            //if (addEventViewModel != null)
            //{
            //    // dialog.BindView(window);
            //    window1.DataContext = addEventViewModel;
            //}
            //window1.ShowDialog();
            //window1.Close();
        }

        private void ContactCommandHandler()
        {
            if (ViewModel.SelectedAthleticsDetail == null)
            {
                MessageBox.Show("Please Select the Athlete", "Athelte not Selected");
                return;
            }
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddContactView();
            window.Name = "AddContact";
            AddContactViewModel contactExisted = athleticManager.GetContact(ViewModel.SelectedAthleticsDetail.AthletesID).Result;
            IGenericInteractionView<AddContactViewModel> relayVM = unityContainer.Resolve<IAddContactViewModel>() as IGenericInteractionView<AddContactViewModel>;
            if (relayVM != null)
            {
                if (contactExisted != null)
                {
                    relayVM.SetEntity(contactExisted);
                }
                dialog.BindView(window);
                dialog.BindViewModel(relayVM);
            }
            dialog.ShowDialog();
            AddContactViewModel contact = relayVM.GetEntity();
            contact.AthletesID = ViewModel.SelectedAthleticsDetail.AthletesID;
            ServiceRequest_AthleteContact contactService = new ServiceRequest_AthleteContact();
            contactService.AthleteContactID = contact.AthleteContactID;
            contactService.Contact = contact;
            athleticManager.AddAthletesContact(contactService);
            dialog.Close();
            dialog = null;
        }

        private void CampCommandHandler()
        {
            MessageBox.Show("CampCommandHandler");
        }
        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        if (ViewModel.SelectedAthleticsDetail == null)
                        {
                            MessageBox.Show("Please Select Athlete to Delete", "Event Delete Error");
                            return;
                        }
                        if (ViewModel.SelectedAthleticsDetail.AthletesID == 0)
                        {
                            if (ViewModel.SelectedAthleticsDetail == null) return;
                            AuthorizedResponse<AthletiesResult> Response = await athleticManager.DeleteAthleties(ViewModel.SelectedAthleticsDetail.AthletesID);
                            if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                            {
                                MessageBox.Show("Error while deleteing the Athlete", "Event Delete Error");
                                return;
                            }
                            ViewModel.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;
                            ViewModel.AthleticsDetailList.Remove(ViewModel.SelectedAthleticsDetail);

                        }
                    });

                })).Wait();
            });

        }

        private void EditCommandHandler()
        {
            if (ViewModel.SelectedAthleticsDetail == null) return;
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddAthletesView();
            window.Name = "AddEvents";
            IAddAthletesView eventView = window as IAddAthletesView;
            AddAthletesViewModel addEventViewModel = new AddAthletesViewModel(this.unityContainer, eventView, this.athleticManager, ViewModel.AthleticsDetailList, ViewModel.SelectedAthleticsDetail);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            if (addEventViewModel != null)
            {
                IAddAthletesViewModel addEvent = addEventViewModel as IAddAthletesViewModel;
                dialog.BindView(window);
                dialog.BindViewModel(addEvent);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            addEventViewModel = addEventViewModel.GetEntity();
            Task.Run(async () =>
{
    Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
    {
        await Task.Run(async () =>
        {
            if (ViewModel.SelectedAthleticsDetail.AthletesID > 0)
            {

                ServiceRequest_Athleties serviceObj = new ServiceRequest_Athleties();
                serviceObj.Athleties = addEventViewModel;
                AuthorizedResponse<AthletiesResult> Response = await athleticManager.UpdateAthleties(serviceObj);
                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while Updating the Athlete", "Athlete Add Error");
                    return;
                }
                await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                 {
                     ViewModel.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;
                     ViewModel.AthleticsDetailList.Remove(ViewModel.SelectedAthleticsDetail);
                     ViewModel.AthleticsDetailList.Add(addEventViewModel);
                     ViewModel.SelectedAthleticsDetail = addEventViewModel;
                 }));
            }
            else
            {
            }

        });

    })).Wait();
});

        }

        private void AddCommandHandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddAthletesView();
            window.Name = "AddEvents";
            IAddAthletesView eventView = window as IAddAthletesView;
            AddAthletesViewModel addEventViewModel = new AddAthletesViewModel(this.unityContainer, eventView, this.athleticManager, ViewModel.AthleticsDetailList, ViewModel.SelectedAthleticsDetail);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            this.ClearViewModel(addEventViewModel);
            if (addEventViewModel != null)
            {
                IAddAthletesViewModel addEvent = addEventViewModel as IAddAthletesViewModel;
                dialog.BindView(window);
                dialog.BindViewModel(addEventViewModel);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;
            AddAthletesViewModel addeventVm = addEventViewModel.GetEntity();
            if (addEventViewModel.IsCanceled == true) return;
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        //if (ViewModel.SelectedAthleticsDetail.AthletesID == 0)
                        //{

                        ServiceRequest_Athleties serviceObj = new ServiceRequest_Athleties();
                        serviceObj.Athleties = addEventViewModel;
                        AuthorizedResponse<AthletiesResult> Response = await athleticManager.CreateNewAthleties(serviceObj);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while adding the Athlete", "Athlete Add Error");
                            return;
                        }
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            //ViewModel.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;
                            ViewModel.AthleticsDetailList.Add(addEventViewModel);
                        }));

                        //}
                        //else
                        //{
                        //}

                    });

                })).Wait();
            });

        }

        private void ClearViewModel(IGenericInteractionView<AddAthletesViewModel> addEventViewModel)
        {
            addEventViewModel.SetEntity(new AddAthletesViewModel());
        }

        private void LayoutCommandHandler()
        {
            MessageBox.Show("Layout");
        }

        #endregion

    }
}

