﻿using Athletics.Athletes;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Entities.Models;
using Athletics.ControlLibrary;
using System.Data.SqlClient;
using System.Data;
using Athletics.Report;

namespace Athletics.Athletes.ViewModels
{
    public class ExportDataViewModel : BindableBase, IExportDataViewModel, IGenericInteractionView<ExportDataViewModel>
    {
        private IExportDataView View = null;
        IReportManager reportManager;
        IModalDialog dialog = null;
        string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AthleticsConnection"].ConnectionString;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public bool IsCanceled;
        public ExportDataViewModel()
        {

        }

        public ExportDataViewModel(IUnityContainer unity, IExportDataView view, IReportManager reportMng)
        {
            View = view;
            unityContainer = unity;
            reportManager = reportMng;
            View.DataContext = this;
            _GenerateQueryCommand = new DelegateCommand(GenerateCommandHandler);
            GetAllTableList();
            GetAllTableGrop();
            _ExportDataToExcelCommand = new DelegateCommand(ExportDataToExcelHandler);

        }

        private void GetAllTableGrop()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                TableGroupList = await reportManager.GetAllReportTableGroup();

            })).Wait();
        }

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(ExportDataViewModel entity)
        {

        }
        public ExportDataViewModel GetEntity()
        {

            return this;
        }

        #region Properties
        private ImpObservableCollection<ReportTableResult> _TableList = new ImpObservableCollection<ReportTableResult>();
        public ImpObservableCollection<ReportTableResult> TableList
        {
            get { return _TableList; }
            set
            {
                _TableList = value;
                base.OnPropertyChanged("TableList");
            }
        }
        private ImpObservableCollection<ReportTableGroup> _TableGroupList = new ImpObservableCollection<ReportTableGroup>();
        public ImpObservableCollection<ReportTableGroup> TableGroupList
        {
            get { return _TableGroupList; }
            set
            {
                _TableGroupList = value;
                base.OnPropertyChanged("TableGroupList");
            }
        }
        private ReportTableGroup _SelectedTableGroup = null;
        public ReportTableGroup SelectedTableGroup
        {
            get { return _SelectedTableGroup; }
            set
            {
                _SelectedTableGroup = value;
                base.OnPropertyChanged("SelectedTableGroup");
                TableCollection.Clear();
                this.FilterTableList();
            }
        }

        private void FilterTableList()
        {
            if (SelectedTableGroup == null) return;
            foreach (var item in TableList.Where(p => p.ReportTableGroupID == SelectedTableGroup.ReportTableGroupID))
            {
                //SelectedTable = item;
                AddTableToSurface(item.ReportTableName);
                TableSchema tableSchema = TableCollection.Where(p => p.TableName == item.ReportTableRootTableName).FirstOrDefault();
                if (tableSchema != null)
                {
                    if (!string.IsNullOrEmpty(item.ReportTableRootRelationalTableName))
                    {
                        AddTableToSurface(item.ReportTableRootRelationalTableName);
                    }
                }
            }
        }

        private ReportTableResult _SelectedTable;
        public ReportTableResult SelectedTable
        {
            get { return _SelectedTable; }
            set
            {
                _SelectedTable = value;
                if (_SelectedTable == null) return;
                base.OnPropertyChanged("SelectedTable");
                AddTableToSurface(value.ReportTableName);
                TableSchema tableSchema = TableCollection.Where(p => p.TableName == value.ReportTableRootTableName).FirstOrDefault();
                if (tableSchema != null)
                {
                    if (!string.IsNullOrEmpty(value.ReportTableRootRelationalTableName))
                    {
                        AddTableToSurface(value.ReportTableRootRelationalTableName);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(value.ReportTableRootTableName))
                        GetTableSchema(value.ReportTableRootTableName);


                }

            }
        }
        private ImpObservableCollection<TableSchema> _TableCollection = new ImpObservableCollection<TableSchema>();
        public ImpObservableCollection<TableSchema> TableCollection
        {
            get { return _TableCollection; }
            set
            {
                _TableCollection = value;
                base.OnPropertyChanged("TableCollection");
            }
        }
        private string _QueryText = string.Empty;
        public string QueryText
        {
            get { return _QueryText; }
            set
            {
                _QueryText = value;
                base.OnPropertyChanged("QueryText");
            }
        }
        private DataView _QueryResult;
        public DataView QueryResult
        {
            get { return _QueryResult; }
            set
            {
                _QueryResult = value;
                base.OnPropertyChanged("QueryResult");
            }
        }
        #endregion

        #region Methods
        private void ExportDataToExcelHandler()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "Excel Document (.xlsx)|*.xlsx";
            string filename = string.Empty;
            if (dlg.ShowDialog() == true)
            {
                filename = dlg.FileName;
            }
            ExportToExcel.ToExcel(QueryResult.ToTable(), filename);
        }
        private void GenerateCommandHandler()
        {
            string columnNames = string.Empty;
            string fromtable = string.Empty;
            string joinTable = string.Empty;
            DataTable dt = new DataTable();
            List<string> columnList = new List<string>();
            List<string> TableListAllSelected = new List<string>();

            foreach (var item in this.TableCollection)
            {
                foreach (var columnSchema in item.ColumnCollection.Where(p => p.IsSelected == true))
                {
                    columnList.Add(string.Format("[{0}].[{1}]", item.TableName, columnSchema.ColumnName));
                    if (!TableListAllSelected.Contains(item.TableName))
                    {
                        TableListAllSelected.Add(item.TableName);
                    }
                }

            }
            if (columnList.Count <= 0)
            {
                MessageBox.Show("No Column Selected");
                return;
            }
            List<ReportTableResult> reportTableResultList = this.TableList.Where(p => TableListAllSelected.Contains(p.ReportTableName)).ToList();
            if (reportTableResultList != null)
            {
                ReportTableResult reportTableResult = reportTableResultList.Where(p => p.ReportTableRootTableName == null).FirstOrDefault();
                if (reportTableResult == null)
                {
                    reportTableResult = reportTableResultList.FirstOrDefault();
                }
                fromtable = reportTableResult.ReportTableName;
            }
            columnNames = string.Join(",", columnList);
            joinTable = this.GetTableJoin(reportTableResultList);
            QueryText = string.Format("Select {0} from {1} {2}", columnNames, fromtable, joinTable);

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlDataAdapter sda = new SqlDataAdapter(QueryText, con);
                try
                {
                    con.Open();
                    sda.Fill(dt);
                }
                catch (SqlException se)
                {
                    MessageBox.Show(se.Message);
                }
                finally
                {
                    con.Close();
                }
            }
            QueryResult = dt.DefaultView;

        }
        private void GetAllTableList()
        {
            //if (SelectedTableGroup == null) return;
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                TableList = await reportManager.GetAllReportTable();


            })).Wait();

        }
        private string GetTableJoin(List<ReportTableResult> ReportList)
        {
            List<ReportTableResult> RelationalTable = ReportList.Where(p => p.ReportTableRootRelationalTableName != null).ToList();
            List<ReportTableResult> RelationalTableNotExistINSelect = new List<ReportTableResult>();
            foreach (var item in RelationalTable)
            {
                ReportTableResult existedReportTableResult = ReportList.Where(p => p.ReportTableName == item.ReportTableRootRelationalTableName).FirstOrDefault();
                if (existedReportTableResult == null)
                {
                    existedReportTableResult = this.TableList.Where(p => p.ReportTableName == item.ReportTableRootRelationalTableName).FirstOrDefault();
                    ReportTableResult addedReportTableResult = RelationalTableNotExistINSelect.Where(p => p.ReportTableName == existedReportTableResult.ReportTableName).FirstOrDefault();
                    if (addedReportTableResult == null)
                    {
                        RelationalTableNotExistINSelect.Add(existedReportTableResult);
                    }
                }
            }
            List<ReportTableResult> OtherTableList = ReportList.Where(p => p.ReportTableRootTableName != null && (RelationalTable.Contains(p) == false)).ToList();
            string joinQuery = String.Join(" ", OtherTableList.Select(p => p.ReportTableJoinStatement));
            joinQuery = String.Join(" ", joinQuery + String.Join(" ", RelationalTableNotExistINSelect.Select(p => p.ReportTableJoinStatement)));
            joinQuery = String.Join(" ", joinQuery + String.Join(" ", RelationalTable.Select(p => p.ReportTableJoinStatement)));
            return joinQuery;
        }
        public async void GetTableSchema(string tablename)
        {
            List<GetTableSchema_Result> schematResultList = await reportManager.GetTableSchema(tablename);
            TableSchema tableSchema = new TableSchema();
            tableSchema.ColumnCollection = new ImpObservableCollection<ColumnSchema>();
            tableSchema.TableName = tablename;
            foreach (var item in schematResultList)
            {
                ColumnSchema columnSchema = new ColumnSchema();
                columnSchema.ColumnName = item.COLUMN_NAME;
                columnSchema.IsPrimaryKey = Convert.ToBoolean(item.IsPrimaryKey);
                columnSchema.IsForeignKey = bool.Parse(item.IsForeignKey);
                if (columnSchema.IsForeignKey)
                {
                    List<GetForeignKeyDetail_Result> foreigKeyResultList = await reportManager.GetForeignKeyDetail(tablename, item.COLUMN_NAME);
                    foreach (var foreignKeyDetail in foreigKeyResultList)
                    {
                        columnSchema.ReferencedTableName = foreignKeyDetail.Referenced_Table_Name;
                        columnSchema.ReferencedColumneName = foreignKeyDetail.Referenced_Column_As_FK;
                        columnSchema.ReferencingTableName = foreignKeyDetail.Referencing_Table_Name;
                        columnSchema.ReferencingColumnName = foreignKeyDetail.Referencing_Column_Name;
                    }
                }
                //if (columnSchema.IsForeignKey)
                //{
                //    using (SqlCommand cmdForeignkeyDetail = new SqlCommand("GetForeignKeyDetail", con))
                //    {
                //        cmdForeignkeyDetail.CommandType = CommandType.StoredProcedure;
                //        cmdForeignkeyDetail.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = tablename;
                //        cmdForeignkeyDetail.Parameters.Add("@ColumnName ", SqlDbType.NVarChar).Value = columnSchema.ColumnName;
                //        // con.Open();
                //        SqlDataReader rdr1 = cmdForeignkeyDetail.ExecuteReader();
                //        while (rdr1.Read())
                //        {
                //            columnSchema.ReferencedTableName = rdr1["Referenced_Table_Name"].ToString();
                //            columnSchema.ReferencedColumneName = rdr1["Referenced_Column_As_FK"].ToString();
                //            columnSchema.ReferencingTableName = rdr1["Referencing_Table_Name"].ToString();
                //            columnSchema.ReferencingColumnName = rdr1["Referencing_Column_Name"].ToString();
                //        }
                //    }
                //}
                tableSchema.ColumnCollection.Add(columnSchema);

            }
            var table = TableCollection.Where(p => p.TableName == tableSchema.TableName).FirstOrDefault();
            if (table == null)
                TableCollection.Add(tableSchema);
            //using (SqlConnection con = new SqlConnection(ConnectionString))
            //{
            //    using (SqlCommand cmd = new SqlCommand("GetTableSchema", con))
            //    {
            //        TableSchema tableSchema = new TableSchema();
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = tablename;
            //        con.Open();
            //        SqlDataReader rdr = cmd.ExecuteReader();
            //        tableSchema.ColumnCollection = new ImpObservableCollection<ColumnSchema>();
            //        tableSchema.TableName = tablename;
            //        while (rdr.Read())
            //        {
            //            ColumnSchema columnSchema = new ColumnSchema();
            //            columnSchema.ColumnName = rdr["COLUMN_NAME"].ToString();
            //            columnSchema.IsPrimaryKey = bool.Parse(rdr["IsPrimaryKey"].ToString());
            //            columnSchema.IsForeignKey = bool.Parse(rdr["IsForeignKey"].ToString());
            //            if (columnSchema.IsForeignKey)
            //            {
            //                using (SqlCommand cmdForeignkeyDetail = new SqlCommand("GetForeignKeyDetail", con))
            //                {
            //                    cmdForeignkeyDetail.CommandType = CommandType.StoredProcedure;
            //                    cmdForeignkeyDetail.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = tablename;
            //                    cmdForeignkeyDetail.Parameters.Add("@ColumnName ", SqlDbType.NVarChar).Value = columnSchema.ColumnName;
            //                    // con.Open();
            //                    SqlDataReader rdr1 = cmdForeignkeyDetail.ExecuteReader();
            //                    while (rdr1.Read())
            //                    {
            //                        columnSchema.ReferencedTableName = rdr1["Referenced_Table_Name"].ToString();
            //                        columnSchema.ReferencedColumneName = rdr1["Referenced_Column_As_FK"].ToString();
            //                        columnSchema.ReferencingTableName = rdr1["Referencing_Table_Name"].ToString();
            //                        columnSchema.ReferencingColumnName = rdr1["Referencing_Column_Name"].ToString();
            //                    }
            //                }
            //            }
            //            tableSchema.ColumnCollection.Add(columnSchema);

            //        }
            //TableCollection.Add(tableSchema);
            //base.OnPropertyChanged("TableCollection");
            //}
            //}
        }
        private void AddTableToSurface(string value)
        {
            GetTableSchema(value);
        }

        #endregion

        #region Command
        private ICommand _GenerateQueryCommand;
        public ICommand GenerateQueryCommand
        {
            get
            {
                return _GenerateQueryCommand;
            }
        }

        private ICommand _ExportDataToExcelCommand;
        public ICommand ExportDataToExcelCommand
        {
            get { return _ExportDataToExcelCommand; }
        }

        #endregion
    }

}

