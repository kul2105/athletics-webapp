﻿using Athletics.Athletes;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Entities.Models;

namespace Athletics.Athletes.ViewModels
{

    public class AddContactViewModel : BindableBase, IAddContactViewModel, IGenericInteractionView<AddContactViewModel>
    {
        private IAddContactView View = null;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public bool IsCanceled;
        public AddContactViewModel()
        {

        }

        public AddContactViewModel(IUnityContainer unity, IAddContactView view, IAthletesManager athletesManager)
        {
            View = view;
            unityContainer = unity;
            AthletesManager = athletesManager;
            View.DataContext = this;
            CountryList = new List<BaseCounty>();
            List<BaseCounty> countryList = AthletesManager.GetAllCountry();
            foreach (BaseCounty item in countryList)
            {
                CountryList.Add(item);
            }
            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "AddContact")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "AddContact")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });



        }

        #region Properties
        private List<BaseCounty> _CountryList = new List<BaseCounty>();
        public List<BaseCounty> CountryList
        {
            get { return _CountryList; }
            set
            {
                SetProperty(ref _CountryList, value);
            }
        }
        private List<State> _StateList = new List<State>();
        public List<State> StateList
        {
            get { return _StateList; }
            set
            {
                SetProperty(ref _StateList, value);
            }
        }
        private State _SelectedState;
        public State SelectedState
        {
            get { return _SelectedState; }
            set
            {
                SetProperty(ref _SelectedState, value);
                if (value != null)
                {
                    this.State = value.StateName;
                }
            }
        }

        private BaseCounty _SelectedCountry;
        public BaseCounty SelectedCountry
        {
            get { return _SelectedCountry; }
            set
            {
                SetProperty(ref _SelectedCountry, value);
                if (value != null)
                {
                    CountryString = value.BaseCountyName;
                    if (AthletesManager != null)
                    {
                        StateList = new List<State>();
                        //Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        //{
                        List<State> sateList = AthletesManager.GetStateByCountry(value.BaseCountyID);
                        foreach (State item in sateList)
                        {
                            StateList.Add(item);
                        }
                        //}));
                    }
                }
            }
        }

        private int _AthletesID;
        public int AthletesID
        {
            get { return _AthletesID; }
            set
            {
                SetProperty(ref _AthletesID, value);
            }
        }
        private string _CountryString;
        public string CountryString
        {
            get { return _CountryString; }
            set
            {
                SetProperty(ref _CountryString, value);
            }
        }
        private string _City;
        public string City
        {
            get { return _City; }
            set
            {
                SetProperty(ref _City, value);
            }
        }

        private string _Address1;
        public string Address1
        {
            get { return _Address1; }
            set
            {
                SetProperty(ref _Address1, value);
            }
        }

        private int _Cell;
        public int Cell
        {
            get { return _Cell; }
            set
            {
                SetProperty(ref _Cell, value);
            }
        }
        private string _Address2;
        public string Address2
        {
            get { return _Address2; }
            set
            {
                SetProperty(ref _Address2, value);
            }
        }

        private string _State;
        public string State
        {
            get { return _State; }
            set
            {
                SetProperty(ref _State, value);
            }
        }
        private int _Office;
        public int Office
        {
            get { return _Office; }
            set
            {
                SetProperty(ref _Office, value);
            }
        }
        private string _Province;
        public string Province
        {
            get { return _Province; }
            set
            {
                SetProperty(ref _Province, value);
            }
        }
        private int _Zip;
        public int Zip
        {
            get { return _Zip; }
            set
            {
                SetProperty(ref _Zip, value);
            }
        }

        private int _Home;
        public int Home
        {
            get { return _Home; }
            set
            {
                SetProperty(ref _Home, value);
            }
        }
        private int _Fax;
        public int Fax
        {
            get { return _Fax; }
            set
            {
                SetProperty(ref _Fax, value);
            }
        }
        private string _Email;
        public string Email
        {
            get { return _Email; }
            set
            {
                SetProperty(ref _Email, value);
            }
        }
        private string _EmregancyContact;
        public string EmregancyContact
        {
            get { return _EmregancyContact; }
            set
            {
                SetProperty(ref _EmregancyContact, value);
            }
        }
        private int _EmregancyPhone;
        public int EmregancyPhone
        {
            get { return _EmregancyPhone; }
            set
            {
                SetProperty(ref _EmregancyPhone, value);
            }
        }

        private int _AthleteContactID;
        public int AthleteContactID
        {
            get { return _AthleteContactID; }
            set
            {
                SetProperty(ref _AthleteContactID, value);
            }
        }
        #endregion

        #region Reset UI

        #endregion

        #region Form Properties


        #endregion

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(AddContactViewModel entity)
        {
            if (entity == null) return;
            this.Address1 = entity.Address1;
            this.State = entity.State;
            this.City = entity.City;
            this.Address2 = entity.Address2;
            this.Office = entity.Office;
            this.Province = entity.Province;
            this.Zip = entity.Zip;
            this.SelectedCountry = this.CountryList.Where(p => p.BaseCountyName == entity.CountryString).FirstOrDefault();
            this.SelectedState = this.StateList.Where(p => p.StateName == entity.State).FirstOrDefault();
            this.Home = entity.Home;
            this.Fax = entity.Fax;
            this.Email = entity.Email;
        }
        public AddContactViewModel GetEntity()
        {

            return this;
        }


        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        #endregion
    }

}

