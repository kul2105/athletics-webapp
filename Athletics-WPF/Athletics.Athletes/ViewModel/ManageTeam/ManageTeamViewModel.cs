﻿using Athletics.Athletes;
using Athletics.Athletes.Model;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using Athletics.MeetSetup.Services.API;
using Athletics.Entities.Models;

namespace Athletics.Athletes.ViewModels
{

    public class ManageTeamViewModel : BindableBase, IManageTeamViewModel, IGenericInteractionView<ManageTeamViewModel>
    {
        private IManageTeamView View = null;
        private int AthleteRelayPreferencesId = 0;
        internal bool IsInCloseMode = false;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public ManageTeamViewModel(IUnityContainer unity, IManageTeamView view, IAthletesManager athletesManager)
        {
            View = view;
            unityContainer = unity;
            AthletesManager = athletesManager;
            View.DataContext = this;

            List<Team> AthleticsList = AthletesManager.GetAllTeams().Result.ToList();
            foreach (Team item in AthleticsList)
            {
                IAddTeamView addTeamView = unity.Resolve<IAddTeamView>() as IAddTeamView;
                AddTeamViewModel teamvm = new AddTeamViewModel(unity, addTeamView, athletesManager);
                teamvm.Address1 = item.Address1;
                teamvm.Address2 = item.Address2;
                teamvm.Association = item.Association;
                teamvm.City = item.City;
                teamvm.CountryString = item.Country;
                teamvm.Division = item.Division;
                teamvm.FullTeamName = item.FullTeamName;
                teamvm.HeadCoachFirstName = item.HeadCoachFirstName;
                teamvm.HeadCoachLastName = item.HeadCoachLastName;
                teamvm.IsUnAttached = item.IsUnAttached == null ? false : item.IsUnAttached.Value;
                teamvm.Office = item.Office;
                teamvm.Province = item.Province;
                teamvm.ShortTeamName = item.ShortTeamName;
                teamvm.State = item.State;
                teamvm.TeamAbbr = item.TeamAbbr;
                teamvm.TeamID = item.TeamID;
                teamvm.TeamName = item.TeamName;
                teamvm.TeamString = item.TeamName;
                teamvm.Zip = item.Zip;
                teamvm.Region = item.Region;
                teamvm.Status = item.Status;
                teamvm.Home = item.Home;
                teamvm.Email = item.Email;
                teamvm.AltTeamAbb = item.AltTeamAbb;
                teamvm.CountryString = item.Country;
                teamvm.SelectedCountry = teamvm.CountryList.Where(p => p.BaseCountyName == item.Country).FirstOrDefault();
                teamvm.SelectedState = teamvm.StateList.Where(p => p.StateName == item.State).FirstOrDefault();
                TeamDetailList.Add(teamvm);
            }
            _AddCommand = new MeetCommandViewModel(AddCommandHandler);
            _EditCommand = new MeetCommandViewModel(EditCommandHandler);
            _DeleteCommand = new MeetCommandViewModel(DeleteCommandHandler);
        }

        private void DeleteCommandHandler()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    //await Task.Run(async () =>
                    //{
                    if (this.SelectedTeamDetail.TeamID > 0)
                    {
                        if (this.SelectedTeamDetail == null) return;
                        AuthorizedResponse<TeamResult> Response = await AthletesManager.DeleteTeam(this.SelectedTeamDetail.TeamID);
                        if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                        {
                            MessageBox.Show("Error while deleteing the Team", "Team Delete Error");
                            return;
                        }
                        //this.SelectedAthleticsDetail.AthletesID = Response.Result.AthletiesID;
                        this.TeamDetailList.Remove(this.SelectedTeamDetail);

                    }
                    //});

                })).Wait();
            });

        }

        private void EditCommandHandler()
        {
            if (this.SelectedTeamDetail == null) return;

            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddTeamView();
            window.Name = "AddTeam";
            IAddTeamView eventView = window as IAddTeamView;
            AddTeamViewModel addEventViewModel = new AddTeamViewModel(this.unityContainer, eventView, this.AthletesManager);
            addEventViewModel.SetEntity(this.SelectedTeamDetail);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            if (addEventViewModel != null)
            {
                IAddTeamViewModel addEvent = addEventViewModel as IAddTeamViewModel;
                if (addEvent != null)
                {
                    addEvent.TeamDetailList = this.TeamDetailList;
                    addEvent.SelectedTeamDetail = this.SelectedTeamDetail;
                }
                dialog.BindView(window);
                dialog.BindViewModel(addEvent);
            }
            dialog.ShowDialog();
            dialog.Close();
            dialog = null;

            //dialog = ServiceProvider.Instance.Get<IModalDialog>();
            //window = new AddTeamView();
            //window.Name = "AddTeam";
            //IAddTeamView eventView = window as IAddTeamView;
            ////window = eventView as Window;
            //AddTeamViewModel addEventViewModel = new AddTeamViewModel(this.unityContainer, eventView, this.AthletesManager, this.TeamDetailList, this.SelectedTeamDetail);
            ////IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            ////if (SelectedTeamDetail != null)
            ////{
            ////    //IAddTeamViewModel addEvent = addEventViewModel as IAddTeamViewModel;
            ////    dialog.BindView(window);
            ////    dialog.BindViewModel(SelectedTeamDetail);
            ////}
            ////window.DataContext = SelectedTeamDetail;
            //window.ShowDialog();
            //window.Close();
           // SelectedTeamDetail = addEventViewModel.GetEntity();

            if (this.SelectedTeamDetail.TeamID > 0)
            {

                ServiceRequest_Team serviceObj = new ServiceRequest_Team();
                serviceObj.Team = SelectedTeamDetail;
                AuthorizedResponse<TeamResult> Response = AthletesManager.UpdateTeam(serviceObj).Result;
                if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                {
                    MessageBox.Show("Error while Updating the Team", "Team Add Error");
                    return;
                }
                this.SelectedTeamDetail.TeamID = Response.Result.TeamID;
                AddTeamViewModel vm = this.TeamDetailList.Where(p => p.TeamID == this.SelectedTeamDetail.TeamID).FirstOrDefault();
                if (vm != null)
                {
                    this.TeamDetailList.Remove(vm);
                    this.TeamDetailList.Add(addEventViewModel);
                    this.SelectedTeamDetail = addEventViewModel;
                }
            }
            else
            {
            }



        }

        private void AddCommandHandler()
        {
            dialog = ServiceProvider.Instance.Get<IModalDialog>();
            window = new AddTeamView();
            window.Name = "AddTeam";
            IAddTeamView eventView = window as IAddTeamView;
            AddTeamViewModel addEventViewModel = new AddTeamViewModel(this.unityContainer, eventView, this.AthletesManager);
            //IGenericInteractionView<AddEventViewModel> addEventViewModel = unityContainer.Resolve<IAddEventViewModel>() as IGenericInteractionView<AddEventViewModel>;
            this.ClearViewModel(addEventViewModel);
            if (addEventViewModel != null)
            {
                IAddTeamViewModel addEvent = addEventViewModel as IAddTeamViewModel;
                if (addEvent != null)
                {
                    addEvent.TeamDetailList = this.TeamDetailList;
                    addEvent.SelectedTeamDetail = this.SelectedTeamDetail;
                }
                dialog.BindView(window);
                dialog.BindViewModel(addEventViewModel);
            }
            dialog.ShowDialog();
            dialog.Close();
            AddTeamViewModel addeventVm = addEventViewModel.GetEntity();
            if (addEventViewModel.IsCanceled == true) return;
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                {
                    await Task.Run(async () =>
                    {
                        if (!string.IsNullOrEmpty(addeventVm.TeamName))
                        {

                            ServiceRequest_Team serviceObj = new ServiceRequest_Team();
                            serviceObj.Team = addEventViewModel;
                            AuthorizedResponse<TeamResult> Response = await AthletesManager.CreateNewTeam(serviceObj);
                            if (Response.Status == eMEET_REQUEST_RESPONSE.DEFAULT_ERROR)
                            {
                                MessageBox.Show("Error while adding the Team", "Team Add Error");
                                return;
                            }
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                addEventViewModel.TeamID = Response.Result.TeamID;
                                this.TeamDetailList.Add(addEventViewModel);
                            }));
                        }

                    });

                })).Wait();
            });

        }
        private void ClearViewModel(IGenericInteractionView<AddTeamViewModel> addEventViewModel)
        {
            addEventViewModel.SetEntity(new AddTeamViewModel());
        }

        private ObservableCollection<AddTeamViewModel> _TeamDetailList = new ObservableCollection<AddTeamViewModel>();
        public ObservableCollection<AddTeamViewModel> TeamDetailList
        {
            get { return _TeamDetailList; }
            set
            {
                SetProperty(ref _TeamDetailList, value);
            }
        }
        private AddTeamViewModel _SelectedTeamDetail;
        public AddTeamViewModel SelectedTeamDetail
        {
            get { return _SelectedTeamDetail; }
            set
            {
                SetProperty(ref _SelectedTeamDetail, value);
            }
        }


        private bool ValidateInputs()
        {
            return true;

        }

        public void SetEntity(ManageTeamViewModel entity)
        {
        }
        private ObservableCollection<TeamMenuItemViewModel> _MenuItems;
        public ObservableCollection<TeamMenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                SetProperty(ref _MenuItems, value);
            }
        }
        public ManageTeamViewModel GetEntity()
        {
            return this;
        }
        #region COmmand
        private ICommand _AddCommand;
        public ICommand AddCommand
        {
            get
            {
                return _AddCommand;
            }
        }

        private ICommand _EditCommand;
        public ICommand EditCommand
        {
            get
            {
                return _EditCommand;
            }
        }
        private ICommand _DeleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _DeleteCommand;
            }
        }
        #endregion
    }

}

