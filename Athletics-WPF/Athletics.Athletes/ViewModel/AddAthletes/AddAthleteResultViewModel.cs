﻿using Athletics.Athletes;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Entities.Models;
using Athletics.SetupMeet.Model;
using Athletics.Athletes.SetupMeetManager;
using Athletics.MeetSetup.SetupMeetManager;
using Athletics.Athletes.Services.API.Athleties;

namespace Athletics.Athletes.ViewModels
{
    public partial class EventRecordModel : BindableBase
    {
        private int _EventRecordID;
        public int EventRecordID
        {
            get { return _EventRecordID; }
            set
            {
                SetProperty(ref _EventRecordID, value);
            }
        }
        private string _Record;
        public string Record
        {
            get { return _Record; }
            set
            {
                SetProperty(ref _Record, value);
            }
        }
        private string _RecordAbb;
        public string RecordAbb
        {
            get { return _RecordAbb; }
            set
            {
                SetProperty(ref _RecordAbb, value);
            }
        }
        private string _Time;
        public string Time
        {
            get { return _Time; }
            set
            {
                SetProperty(ref _Time, value);
            }
        }
        private DateTime? _Date;
        public DateTime? Date
        {
            get { return _Date; }
            set
            {
                SetProperty(ref _Date, value);
            }
        }
        private string _RecordHolderName;
        public string RecordHolderName
        {
            get { return _RecordHolderName; }
            set
            {
                SetProperty(ref _RecordHolderName, value);
            }
        }
        private int _EventID;
        public int EventID
        {
            get { return _EventID; }
            set
            {
                SetProperty(ref _EventID, value);
            }
        }
    }
    public class AddAthleteResultViewModel : BindableBase, IAddAthleteResultViewModel, IGenericInteractionView<AddAthleteResultViewModel>
    {
        private IAddAthleteResultView View = null;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public bool IsCanceled;
        private IEventManager EventManager;
        IMeetSetupManage MeetSetupManage;
        List<EventInMeet> EventInMeet = null;
        public AddAthleteResultViewModel()
        {

        }

        public AddAthleteResultViewModel(IUnityContainer unity, IAddAthleteResultView view, IEventManager eventManager, IMeetSetupManage meetSetupManage, IAthletesManager athletesManager)
        {
            View = view;
            unityContainer = unity;
            AthletesManager = athletesManager;
            EventManager = eventManager;
            View.DataContext = this;
            MeetSetupManage = meetSetupManage;

            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      this.IsCanceled = false;
                                      ServiceRequest_AthleteRecord recordService = new ServiceRequest_AthleteRecord();
                                      recordService.EventRecordModelList = this.EventRecordList.ToList();

                                      await athletesManager.AddAthleteResult(recordService);
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "AddAthleteResult")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "AddAthleteResult")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

            ReloadMeet();

        }

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(AddAthleteResultViewModel entity)
        {
            if (entity == null) return;
        }
        public AddAthleteResultViewModel GetEntity()
        {

            return this;
        }
        #region Properties
        private ObservableCollection<MeetSetupModel> _MeetSetupList = new ObservableCollection<MeetSetupModel>();
        public ObservableCollection<MeetSetupModel> MeetSetupList
        {
            get { return _MeetSetupList; }
            set
            {
                SetProperty(ref _MeetSetupList, value);
            }
        }

        private MeetSetupModel _SelectedMeet;
        public MeetSetupModel SelectedMeet
        {
            get { return _SelectedMeet; }
            set
            {
                SetProperty(ref _SelectedMeet, value);
                this.LoadEvents();
            }
        }
        private ObservableCollection<AddEventViewModel> _EventDetailList = new ObservableCollection<AddEventViewModel>();
        public ObservableCollection<AddEventViewModel> EventDetailList
        {
            get { return _EventDetailList; }
            set
            {
                SetProperty(ref _EventDetailList, value);
            }
        }
        private AddEventViewModel _SelectedEventDetail;
        public AddEventViewModel SelectedEventDetail
        {
            get { return _SelectedEventDetail; }
            set
            {
                SetProperty(ref _SelectedEventDetail, value);
                this.FilterTeam();
            }
        }



        private ObservableCollection<AddTeamViewModel> _AllTeamDetailList = new ObservableCollection<AddTeamViewModel>();
        public ObservableCollection<AddTeamViewModel> AllTeamDetailList
        {
            get { return _AllTeamDetailList; }
            set
            {
                SetProperty(ref _AllTeamDetailList, value);
            }
        }
        private ObservableCollection<GetTeamsInEvent_Result1> _TeamDetailList = new ObservableCollection<GetTeamsInEvent_Result1>();
        public ObservableCollection<GetTeamsInEvent_Result1> TeamDetailList
        {
            get { return _TeamDetailList; }
            set
            {
                SetProperty(ref _TeamDetailList, value);
            }
        }
        private GetTeamsInEvent_Result1 _SelectedTeamDetail;
        public GetTeamsInEvent_Result1 SelectedTeamDetail
        {
            get { return _SelectedTeamDetail; }
            set
            {
                SetProperty(ref _SelectedTeamDetail, value);
                FilterTeamAthlete();
            }
        }
        private ObservableCollection<EventRecordModel> _EventRecordList = new ObservableCollection<EventRecordModel>();
        public ObservableCollection<EventRecordModel> EventRecordList
        {
            get { return _EventRecordList; }
            set
            {
                SetProperty(ref _EventRecordList, value);
            }
        }

        #endregion

        #region Methods
        private void ReloadMeet()
        {
            this.MeetSetupList.Clear();
            List<MeetSetupModel> modelM = (from t1 in MeetSetupManage.GetAllMeetSetup().Result
                                           join t2 in MeetSetupManage.GetAllBaseCounty().Result
                                           on t1.BaseCountyID equals t2.BaseCountyID
                                           join t3 in MeetSetupManage.GetAllMeetArena().Result
                                           on t1.MeetArenaID equals t3.MeetArenaID
                                           join t4 in MeetSetupManage.GetAllMeetClass().Result
                                           on t1.MeetClassID equals t4.MeetClassID
                                           join t5 in MeetSetupManage.GetAllMeetKind().Result
                                           on t1.MeetKindID equals t5.MeetKindID
                                           join t6 in MeetSetupManage.GetAllMeetStyle().Result
                                           on t1.MeetStyleID equals t6.MeetStyleID
                                           join t7 in MeetSetupManage.GetAllMeetType().Result
                                          on t1.MeetTypeID equals t7.MeetTypeID
                                           join t8 in MeetSetupManage.GetAllMeetTypeDivision().Result
                                          on t1.MeetTypeDivisionID equals t8.MeetTypeDivisionID
                                           select new MeetSetupModel
                                           {
                                               MeetID = t1.MeetSetupID,
                                               MeetName = t1.MeetSetupName,
                                               MeetName2 = t1.MeetSetupName2,
                                               BaseCounty = t2.BaseCountyName,
                                               MeetArenaOption = t3.MeetArenaName,
                                               MeetClassOption = t4.MeetClassName,
                                               KindOfMeetOption = t5.MeetKindName,
                                               AgeUpDate = t1.MeetSetupAgeUpDate,
                                               EndDate = t1.MeetSetupEndDate,
                                               IsLinkTeamToDivision = t1.LinkTermToDivision,
                                               IsUserDivisionBirthDate = t1.UseDivisionBirthdateRange,
                                               MeetStyleOption = t6.MeetStyleName,
                                               MeetTypeOption = t7.MeetTypeName,
                                               StartDate = t1.MeetSetupStartDate,
                                               MeetTypeDivisionOption = t8.MeetTypeDivisionName,
                                               SeedingPrefrencesGroup = this.GetSeeingPrfrenceGroupForMeet(t1.MeetSetupID),
                                               AthleteRelayPrefrencesGroup = this.GetAthleteRelayPrefrencesGroupForMeet(t1.MeetSetupID)
                                           }).Distinct().ToList();


            foreach (var item in modelM)
            {
                this.MeetSetupList.Add(item);
            }
        }
        private AthleteRelayPrefrencesGroup GetAthleteRelayPrefrencesGroupForMeet(int MeetID)
        {
            AthleteRelayPrefrencesGroup opt = new AthleteRelayPrefrencesGroup();
            opt.AthletePreferencesList = new ObservableCollection<Option>();
            opt.RelayPreferencesList = new ObservableCollection<Option>();
            opt.CompetitorNumbersList = new ObservableCollection<Option>();
            foreach (var AthletePreference in MeetSetupManage.GetRelayPrefrencesMeetData(MeetID).Result.Select(p => p.AthletePreferencesName).Distinct())
            {
                Option optRelayPrefrences = new Option();
                optRelayPrefrences.Display = AthletePreference;
                opt.AthletePreferencesList.Add(optRelayPrefrences);
            }

            foreach (var RelayPreference in MeetSetupManage.GetRelayPrefrencesMeetData(MeetID).Result.Select(p => p.RelayPreferencesName).Distinct())
            {
                Option optRelayPrefrences = new Option();
                optRelayPrefrences.Display = RelayPreference;
                opt.RelayPreferencesList.Add(optRelayPrefrences);
            }

            foreach (var CompetitorNumber in MeetSetupManage.GetRelayPrefrencesMeetData(MeetID).Result.Select(p => p.CompetitorNumbersName).Distinct())
            {
                Option optRelayPrefrences = new Option();
                optRelayPrefrences.Display = CompetitorNumber;
                opt.CompetitorNumbersList.Add(optRelayPrefrences);
            }
            if (opt.AthletePreferencesList.Count <= 0 && opt.RelayPreferencesList.Count <= 0 && opt.CompetitorNumbersList.Count <= 0)
            {
                return null;
            }
            return opt;
        }
        private SeedingPrefrencesGroup GetSeeingPrfrenceGroupForMeet(int MeetID)
        {
            SeedingPrefrencesGroup opt = new SeedingPrefrencesGroup();
            opt.DualMeetList = new ObservableCollection<DualMeetSchoolTransaction>();
            opt.MeetAssignment = null;
            opt.RendomizationRule = null;
            opt.SeedingRules = null;
            opt.StandardAlleyPrefrencesList = new ObservableCollection<Entities.Models.StandardAlleyPrefrence>();
            opt.StandardLanePrefrencesList = new ObservableCollection<Entities.Models.StandardLanePrefrence>();
            opt.WaterfallStartPreferencesList = new ObservableCollection<Entities.Models.WaterfallStartPreference>();

            var rendomizeRules = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                    .Select(x => new { x.RendomizationRuleCloseGap, x.RendomizationRuleRoundFirstMultipleRound, x.RendomizationRuleRoundSecondThirdAndForth, x.RendomizationRuleTimedFinalEvents });

            var rendoizationRule = rendomizeRules.Distinct().FirstOrDefault();
            if (rendoizationRule != null)
            {
                opt.RendomizationRule = new Entities.Models.RendomizationRuleTransaction();
                opt.RendomizationRule.RoundFirstMultipleRound = rendoizationRule.RendomizationRuleRoundFirstMultipleRound;
                opt.RendomizationRule.RoundSecondThirdAndForth = rendoizationRule.RendomizationRuleRoundSecondThirdAndForth;
                opt.RendomizationRule.TimedFinalEvents = rendoizationRule.RendomizationRuleTimedFinalEvents;
                opt.RendomizationRule.CloseGap = rendoizationRule.RendomizationRuleCloseGap;
            }

            var dualMeetAssignList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                    .Select(x => new { x.DualMeetStrictAssignmentAllHeats, x.DualMeetAlternateUserOfUnAssignedLane, x.DualMeetStrictAssignmentFastestHeatOnly, x.DualMeetUseLaneAssignmentsForInLaneRacesOnly, x.DualMeetUserLaneOrPositionAssignmentsAbove });

            var dualMeetList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                     .Select(x => new { x.DualMeetLaneNumber, x.DualMeetSchoolAbbr, x.DualMeetSchoolName });

            foreach (var transaction in dualMeetList.Distinct())
            {
                DualMeetSchoolTransaction trans1 = new DualMeetSchoolTransaction();
                trans1.LaneNumber = transaction.DualMeetLaneNumber;
                trans1.SchoolAbb = transaction.DualMeetSchoolAbbr;
                trans1.SchoolName = transaction.DualMeetSchoolName;
                opt.DualMeetList.Add(trans1);
            }
            var trans = dualMeetAssignList.FirstOrDefault();
            if (trans != null)
            {
                opt.MeetAssignment = new Entities.Models.DualMeetAssignmentTransaction();
                opt.MeetAssignment.AlternamtUserOfUnAssignedLane = trans.DualMeetAlternateUserOfUnAssignedLane;
                opt.MeetAssignment.StrictAssignmentAllHeats = trans.DualMeetStrictAssignmentAllHeats;
                opt.MeetAssignment.StrictAssignmentFastestHeatOnly = trans.DualMeetStrictAssignmentFastestHeatOnly;
                opt.MeetAssignment.UseLaneAssignmentsForInLaneRacesOnly = trans.DualMeetUseLaneAssignmentsForInLaneRacesOnly;
                opt.MeetAssignment.UserLaneOrPositionAssignmentsAbove = trans.DualMeetUserLaneOrPositionAssignmentsAbove;
            }

            var SeedingRuleList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                   .Select(x => new { x.SeedingAllowForeignAthletesInFinal, x.SeedingRulesAllowExhibitionAthletesInFinal, x.SeedingRulesApplyNCAARule, x.SeedingRulesApplyNCAARule1, x.SeedingRulesUseSpecialRandomSelectMethod });

            var seedingRule = SeedingRuleList.Distinct().FirstOrDefault();

            if (seedingRule != null)
            {
                opt.SeedingRules = new Entities.Models.MeetSetupSeedingRule();
                opt.SeedingRules.AllowExhibitionAthletesInFinal = seedingRule.SeedingRulesAllowExhibitionAthletesInFinal;
                opt.SeedingRules.AllowForeignAthletesInFinal = seedingRule.SeedingAllowForeignAthletesInFinal;
                opt.SeedingRules.ApplyNCAARule = seedingRule.SeedingRulesApplyNCAARule;
                opt.SeedingRules.UseSpecialRandomSelectMethod = seedingRule.SeedingRulesUseSpecialRandomSelectMethod;
                opt.SeedingRules.SeedExhibitionAthletesLast = seedingRule.SeedingRulesApplyNCAARule1;
            }


            var StandardAlleyList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                .Select(x => new
                {
                    x.StandardAlleyPrefrencesRowEighthColumnEightthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnFifthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnFirstValue,
                    x.StandardAlleyPrefrencesRowEighthColumnForthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnNinthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnSecondValue,
                    x.StandardAlleyPrefrencesRowEighthColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowEighthColumnSixthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnTenthValue,
                    x.StandardAlleyPrefrencesRowEighthColumnThirdValue,
                    x.StandardAlleyPrefrencesRowFifthColumnEightthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnFifthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnFirstValue,
                    x.StandardAlleyPrefrencesRowFifthColumnForthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnNinthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnSecondValue,
                    x.StandardAlleyPrefrencesRowFifthColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowFifthColumnSixthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnTenthValue,
                    x.StandardAlleyPrefrencesRowFifthColumnThirdValue,
                    x.StandardAlleyPrefrencesRowForthColumnEightthValue,
                    x.StandardAlleyPrefrencesRowForthColumnFifthValue,
                    x.StandardAlleyPrefrencesRowForthColumnFirstValue,
                    x.StandardAlleyPrefrencesRowForthColumnForthValue,
                    x.StandardAlleyPrefrencesRowForthColumnNinthValue,
                    x.StandardAlleyPrefrencesRowForthColumnSecondValue,
                    x.StandardAlleyPrefrencesRowForthColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowForthColumnSixthValue,
                    x.StandardAlleyPrefrencesRowForthColumnTenthValue,
                    x.StandardAlleyPrefrencesRowForthColumnThirdValue,
                    x.StandardAlleyPrefrencesRowNineColumnEightthValue,
                    x.StandardAlleyPrefrencesRowNineColumnFifthValue,
                    x.StandardAlleyPrefrencesRowNineColumnFirstValue,
                    x.StandardAlleyPrefrencesRowNineColumnForthValue,
                    x.StandardAlleyPrefrencesRowNineColumnNinthValue,
                    x.StandardAlleyPrefrencesRowNineColumnSecondValue,
                    x.StandardAlleyPrefrencesRowNineColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowNineColumnSixthValue,
                    x.StandardAlleyPrefrencesRowNineColumnTenthValue,
                    x.StandardAlleyPrefrencesRowNineColumnThirdValue,
                    x.StandardAlleyPrefrencesRowOneColumnEightthValue,
                    x.StandardAlleyPrefrencesRowOneColumnFifthValue,
                    x.StandardAlleyPrefrencesRowOneColumnFirstValue,
                    x.StandardAlleyPrefrencesRowOneColumnForthValue,
                    x.StandardAlleyPrefrencesRowOneColumnNinthValue,
                    x.StandardAlleyPrefrencesRowOneColumnSecondValue,
                    x.StandardAlleyPrefrencesRowOneColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowOneColumnSixthValue,
                    x.StandardAlleyPrefrencesRowOneColumnTenthValue,
                    x.StandardAlleyPrefrencesRowOneColumnThirdValue,
                    x.StandardAlleyPrefrencesRowSecondColumnEightthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnFifthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnFirstValue,
                    x.StandardAlleyPrefrencesRowSecondColumnForthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnNinthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnSecondValue,
                    x.StandardAlleyPrefrencesRowSecondColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowSecondColumnSixthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnTenthValue,
                    x.StandardAlleyPrefrencesRowSecondColumnThirdValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnEightthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnFifthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnFirstValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnForthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnNinthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnSecondValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnSixthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnTenthValue,
                    x.StandardAlleyPrefrencesRowSeventhColumnThirdValue,
                    x.StandardAlleyPrefrencesRowSixthColumnEightthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnFifthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnFirstValue,
                    x.StandardAlleyPrefrencesRowSixthColumnForthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnNinthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnSecondValue,
                    x.StandardAlleyPrefrencesRowSixthColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowSixthColumnSixthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnTenthValue,
                    x.StandardAlleyPrefrencesRowSixthColumnThirdValue,
                    x.StandardAlleyPrefrencesRowThirdColumnEightthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnFifthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnFirstValue,
                    x.StandardAlleyPrefrencesRowThirdColumnForthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnNinthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnSecondValue,
                    x.StandardAlleyPrefrencesRowThirdColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowThirdColumnSixthValue,
                    x.StandardAlleyPrefrencesRowThirdColumnThirdValue,
                    x.StandardAlleyPrefrencesRowThridColumnTenthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnEightthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnFifthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnFirstValue,
                    x.StandardAlleyPrefrencesRowZeroColumnForthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnNinthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnSecondValue,
                    x.StandardAlleyPrefrencesRowZeroColumnSeventhValue,
                    x.StandardAlleyPrefrencesRowZeroColumnSixthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnTenthValue,
                    x.StandardAlleyPrefrencesRowZeroColumnThirdValue,
                });

            Entities.Models.StandardAlleyPrefrence standardAlley = new Entities.Models.StandardAlleyPrefrence();
            foreach (var standredAlley in StandardAlleyList.Distinct())
            {
                Entities.Models.StandardAlleyPrefrence standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "1 Alley";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowZeroColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowZeroColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "2 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowOneColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowOneColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowOneColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowOneColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowOneColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowOneColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "3 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowSecondColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowSecondColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "4 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowThirdColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowThirdColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowThridColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "5 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowForthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowForthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowForthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowForthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowForthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowForthColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "6 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowFifthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowFifthColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "7 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowSixthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowSixthColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "8 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowSeventhColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowSeventhColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "9 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowEighthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowEighthColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardAlleyPrefrence();
                standardAlleyTrans.PreferencesName = "10 Alleys";
                standardAlleyTrans.FirstLane = standredAlley.StandardAlleyPrefrencesRowNineColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardAlleyPrefrencesRowNineColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardAlleyPrefrencesRowNineColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardAlleyPrefrencesRowNineColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardAlleyPrefrencesRowNineColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardAlleyPrefrencesRowNineColumnTenthValue;
                opt.StandardAlleyPrefrencesList.Add(standardAlleyTrans);


            }
            var StandardLaneList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
                                    .Select(x => new
                                    {
                                        x.StandardLanePrefrencesRowEighthColumnEightthValue,
                                        x.StandardLanePrefrencesRowEighthColumnFifthValue,
                                        x.StandardLanePrefrencesRowEighthColumnFirstValue,
                                        x.StandardLanePrefrencesRowEighthColumnForthValue,
                                        x.StandardLanePrefrencesRowEighthColumnNinthValue,
                                        x.StandardLanePrefrencesRowEighthColumnSecondValue,
                                        x.StandardLanePrefrencesRowEighthColumnSeventhValue,
                                        x.StandardLanePrefrencesRowEighthColumnSixthValue,
                                        x.StandardLanePrefrencesRowEighthColumnTenthValue,
                                        x.StandardLanePrefrencesRowEighthColumnThirdValue,
                                        x.StandardLanePrefrencesRowFifthColumnEightthValue,
                                        x.StandardLanePrefrencesRowFifthColumnFifthValue,
                                        x.StandardLanePrefrencesRowFifthColumnFirstValue,
                                        x.StandardLanePrefrencesRowFifthColumnForthValue,
                                        x.StandardLanePrefrencesRowFifthColumnNinthValue,
                                        x.StandardLanePrefrencesRowFifthColumnSecondValue,
                                        x.StandardLanePrefrencesRowFifthColumnSeventhValue,
                                        x.StandardLanePrefrencesRowFifthColumnSixthValue,
                                        x.StandardLanePrefrencesRowFifthColumnTenthValue,
                                        x.StandardLanePrefrencesRowFifthColumnThirdValue,
                                        x.StandardLanePrefrencesRowForthColumnEightthValue,
                                        x.StandardLanePrefrencesRowForthColumnFifthValue,
                                        x.StandardLanePrefrencesRowForthColumnFirstValue,
                                        x.StandardLanePrefrencesRowForthColumnForthValue,
                                        x.StandardLanePrefrencesRowForthColumnNinthValue,
                                        x.StandardLanePrefrencesRowForthColumnSecondValue,
                                        x.StandardLanePrefrencesRowForthColumnSeventhValue,
                                        x.StandardLanePrefrencesRowForthColumnSixthValue,
                                        x.StandardLanePrefrencesRowForthColumnTenthValue,
                                        x.StandardLanePrefrencesRowForthColumnThirdValue,
                                        x.StandardLanePrefrencesRowNineColumnEightthValue,
                                        x.StandardLanePrefrencesRowNineColumnFifthValue,
                                        x.StandardLanePrefrencesRowNineColumnFirstValue,
                                        x.StandardLanePrefrencesRowNineColumnForthValue,
                                        x.StandardLanePrefrencesRowNineColumnNinthValue,
                                        x.StandardLanePrefrencesRowNineColumnSecondValue,
                                        x.StandardLanePrefrencesRowNineColumnSeventhValue,
                                        x.StandardLanePrefrencesRowNineColumnSixthValue,
                                        x.StandardLanePrefrencesRowNineColumnTenthValue,
                                        x.StandardLanePrefrencesRowNineColumnThirdValue,
                                        x.StandardLanePrefrencesRowOneColumnEightthValue,
                                        x.StandardLanePrefrencesRowOneColumnFifthValue,
                                        x.StandardLanePrefrencesRowOneColumnFirstValue,
                                        x.StandardLanePrefrencesRowOneColumnForthValue,
                                        x.StandardLanePrefrencesRowOneColumnNinthValue,
                                        x.StandardLanePrefrencesRowOneColumnSecondValue,
                                        x.StandardLanePrefrencesRowOneColumnSeventhValue,
                                        x.StandardLanePrefrencesRowOneColumnSixthValue,
                                        x.StandardLanePrefrencesRowOneColumnTenthValue,
                                        x.StandardLanePrefrencesRowOneColumnThirdValue,
                                        x.StandardLanePrefrencesRowSecondColumnEightthValue,
                                        x.StandardLanePrefrencesRowSecondColumnFifthValue,
                                        x.StandardLanePrefrencesRowSecondColumnFirstValue,
                                        x.StandardLanePrefrencesRowSecondColumnForthValue,
                                        x.StandardLanePrefrencesRowSecondColumnNinthValue,
                                        x.StandardLanePrefrencesRowSecondColumnSecondValue,
                                        x.StandardLanePrefrencesRowSecondColumnSeventhValue,
                                        x.StandardLanePrefrencesRowSecondColumnSixthValue,
                                        x.StandardLanePrefrencesRowSecondColumnTenthValue,
                                        x.StandardLanePrefrencesRowSecondColumnThirdValue,
                                        x.StandardLanePrefrencesRowSeventhColumnEightthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnFifthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnFirstValue,
                                        x.StandardLanePrefrencesRowSeventhColumnForthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnNinthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnSecondValue,
                                        x.StandardLanePrefrencesRowSeventhColumnSeventhValue,
                                        x.StandardLanePrefrencesRowSeventhColumnSixthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnTenthValue,
                                        x.StandardLanePrefrencesRowSeventhColumnThirdValue,
                                        x.StandardLanePrefrencesRowSixthColumnEightthValue,
                                        x.StandardLanePrefrencesRowSixthColumnFifthValue,
                                        x.StandardLanePrefrencesRowSixthColumnFirstValue,
                                        x.StandardLanePrefrencesRowSixthColumnForthValue,
                                        x.StandardLanePrefrencesRowSixthColumnNinthValue,
                                        x.StandardLanePrefrencesRowSixthColumnSecondValue,
                                        x.StandardLanePrefrencesRowSixthColumnSeventhValue,
                                        x.StandardLanePrefrencesRowSixthColumnSixthValue,
                                        x.StandardLanePrefrencesRowSixthColumnTenthValue,
                                        x.StandardLanePrefrencesRowSixthColumnThirdValue,
                                        x.StandardLanePrefrencesRowThirdColumnEightthValue,
                                        x.StandardLanePrefrencesRowThirdColumnFifthValue,
                                        x.StandardLanePrefrencesRowThirdColumnFirstValue,
                                        x.StandardLanePrefrencesRowThirdColumnForthValue,
                                        x.StandardLanePrefrencesRowThirdColumnNinthValue,
                                        x.StandardLanePrefrencesRowThirdColumnSecondValue,
                                        x.StandardLanePrefrencesRowThirdColumnSeventhValue,
                                        x.StandardLanePrefrencesRowThirdColumnSixthValue,
                                        x.StandardLanePrefrencesRowThirdColumnThirdValue,
                                        x.StandardLanePrefrencesRowThridColumnTenthValue,
                                        x.StandardLanePrefrencesRowZeroColumnEightthValue,
                                        x.StandardLanePrefrencesRowZeroColumnFifthValue,
                                        x.StandardLanePrefrencesRowZeroColumnFirstValue,
                                        x.StandardLanePrefrencesRowZeroColumnForthValue,
                                        x.StandardLanePrefrencesRowZeroColumnNinthValue,
                                        x.StandardLanePrefrencesRowZeroColumnSecondValue,
                                        x.StandardLanePrefrencesRowZeroColumnSeventhValue,
                                        x.StandardLanePrefrencesRowZeroColumnSixthValue,
                                        x.StandardLanePrefrencesRowZeroColumnTenthValue,
                                        x.StandardLanePrefrencesRowZeroColumnThirdValue,
                                    });

            foreach (var standredAlley in StandardLaneList.Distinct())
            {
                Entities.Models.StandardLanePrefrence standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "1 Lane";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowZeroColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowZeroColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowZeroColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowZeroColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowZeroColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowZeroColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowZeroColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowZeroColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowZeroColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowZeroColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "2 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowOneColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowOneColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowOneColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowOneColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowOneColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowOneColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowOneColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowOneColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowOneColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowOneColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "3 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowSecondColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowSecondColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowSecondColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowSecondColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowSecondColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowSecondColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowSecondColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowSecondColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowSecondColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowSecondColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "4 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowThirdColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowThirdColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowThirdColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowThirdColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowThirdColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowThirdColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowThirdColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowThirdColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowThirdColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowThridColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "5 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowForthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowForthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowForthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowForthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowForthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowForthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowForthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowForthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowForthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowForthColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "6 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowFifthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowFifthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowFifthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowFifthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowFifthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowFifthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowFifthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowFifthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowFifthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowFifthColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "7 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowSixthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowSixthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowSixthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowSixthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowSixthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowSixthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowSixthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowSixthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowSixthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowSixthColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "8 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowSeventhColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowSeventhColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowSeventhColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowSeventhColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowSeventhColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowSeventhColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "9 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowEighthColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowEighthColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowEighthColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowEighthColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowEighthColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowEighthColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowEighthColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowEighthColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowEighthColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowEighthColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);


                standardAlleyTrans = new Entities.Models.StandardLanePrefrence();
                standardAlleyTrans.PreferencesName = "10 Lanes";
                standardAlleyTrans.FirstLane = standredAlley.StandardLanePrefrencesRowNineColumnFirstValue;
                standardAlleyTrans.SecondLine = standredAlley.StandardLanePrefrencesRowNineColumnSecondValue;
                standardAlleyTrans.ThirdLine = standredAlley.StandardLanePrefrencesRowNineColumnThirdValue;
                standardAlleyTrans.ForthLine = standredAlley.StandardLanePrefrencesRowNineColumnForthValue;
                standardAlleyTrans.FifthLine = standredAlley.StandardLanePrefrencesRowNineColumnFifthValue;
                standardAlleyTrans.SixthLine = standredAlley.StandardLanePrefrencesRowNineColumnSixthValue;
                standardAlleyTrans.SeventhLine = standredAlley.StandardLanePrefrencesRowNineColumnSeventhValue;
                standardAlleyTrans.EightsLine = standredAlley.StandardLanePrefrencesRowNineColumnEightthValue;
                standardAlleyTrans.NinthLine = standredAlley.StandardLanePrefrencesRowNineColumnNinthValue;
                standardAlleyTrans.TenthLine = standredAlley.StandardLanePrefrencesRowNineColumnTenthValue;
                opt.StandardLanePrefrencesList.Add(standardAlleyTrans);

            }

            var WaterfallList = MeetSetupManage.GetSeedingPreferencesMeetData(MeetID).Result
             .Select(x => new { x.WaterfallStartPosition, x.WaterfallStartRank });

            foreach (var waterfall in WaterfallList.Distinct())
            {
                Entities.Models.WaterfallStartPreference waterfallpref = new Entities.Models.WaterfallStartPreference();
                waterfallpref.Position = waterfall.WaterfallStartPosition;
                waterfallpref.Rank = waterfall.WaterfallStartRank;
                opt.WaterfallStartPreferencesList.Add(waterfallpref);
            }

            foreach (var schoolDetail in MeetSetupManage.GetAllSchoolDetail().Result)
            {
                opt.SchoolDetailList.Add(schoolDetail);
            }
            if (opt.DualMeetList.Count <= 0 && opt.MeetAssignment == null && opt.RendomizationRule == null && opt.SeedingRules == null
                && opt.StandardAlleyPrefrencesList.Count <= 0 && opt.StandardLanePrefrencesList.Count <= 0)
            {
                return null;
            }

            return opt;
        }
        private void FilterTeam()
        {
            TeamDetailList.Clear();
            if (SelectedEventDetail != null)
            {
                List<GetTeamsInEvent_Result1> list = AthletesManager.GetTeamsInEvent(SelectedEventDetail.EventID).Result.ToList();

                foreach (GetTeamsInEvent_Result1 item in list)
                {
                    TeamDetailList.Add(item);
                }
            }
            if (TeamDetailList.Count <= 0)
            {
                SelectedTeamDetail = null;
            }
            else
            {
                SelectedTeamDetail = TeamDetailList[0];
            }
        }
        private void LoadEvents()
        {

            if (SelectedMeet == null) return;
            List<AddEventViewModel> EventList = EventManager.GetAllEvent().Result.ToList();
            EventInMeet = EventManager.GetAllEventInMeet().Result.ToList();
            if (EventInMeet == null) return;
            List<int?> list = EventInMeet.Where(p => p.MeetID == SelectedMeet.MeetID).Select(p => p.EventID).ToList();
            if (list == null) return;
            EventDetailList.Clear();
            foreach (AddEventViewModel item in EventList.Where(p => list.Contains(p.EventID)))
            {
                EventDetailList.Add(item);
            }
        }

        private void FilterTeamAthlete()
        {
            EventRecordList.Clear();
            if (SelectedTeamDetail != null)
            {
                List<Athlete> list = AthletesManager.GetAthleteInTeam(SelectedTeamDetail.TEAMID).Result;
                List<EventRecord> eventRecordList = AthletesManager.GetEventRecordByEventID(SelectedEventDetail.EventID).Result;

                foreach (Athlete item in list)
                {
                    EventRecordModel eventModel = new EventRecordModel();
                    string holderName = string.Format("{0} {1}", item.FirstName, item.LastName);
                    EventRecord existedeventRecord = eventRecordList.Where(p => p.RecordHolderName == holderName).FirstOrDefault();
                    if (existedeventRecord != null)
                    {
                        eventModel.Record = existedeventRecord.Record;
                        eventModel.RecordAbb = existedeventRecord.RecordAbb;
                        eventModel.RecordHolderName = existedeventRecord.RecordHolderName;
                        eventModel.EventID = existedeventRecord.EventID == null ? 0 : existedeventRecord.EventID.Value;
                        eventModel.RecordHolderName = existedeventRecord.RecordHolderName;
                        eventModel.Date = existedeventRecord.Date;
                        eventModel.Time = existedeventRecord.Time;
                    }
                    else
                    {
                        List<BaseCounty> basecountyList = MeetSetupManage.GetAllBaseCounty().Result;
                        if (basecountyList != null)
                        {
                            BaseCounty baseCounty = basecountyList.Where(p => p.BaseCountyID == item.CitizenID).FirstOrDefault();
                            if (baseCounty != null)
                            {
                                eventModel.Record = baseCounty.BaseCountyName;
                            }
                            eventModel.RecordHolderName = string.Format("{0} {1}", item.FirstName, item.LastName);
                            eventModel.Date = DateTime.Now;
                            eventModel.EventID = SelectedEventDetail.EventID;
                        }
                    }

                    EventRecordList.Add(eventModel);
                }
            }
        }
        #endregion

        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        #endregion
    }

}

