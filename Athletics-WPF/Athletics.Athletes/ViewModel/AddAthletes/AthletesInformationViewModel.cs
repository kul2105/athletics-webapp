﻿using Athletics.Athletes;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Entities.Models;

namespace Athletics.Athletes.ViewModels
{
    public class AthleteEvent : BindableBase
    {
        private string _EventNumber;
        public string EventNumber
        {
            get { return _EventNumber; }
            set
            {
                SetProperty(ref _EventNumber, value);
            }
        }
        private string _Event;
        public string Event
        {
            get { return _Event; }
            set
            {
                SetProperty(ref _Event, value);
            }
        }
        private string _EntryMark;
        public string EntryMark
        {
            get { return _EntryMark; }
            set
            {
                SetProperty(ref _EntryMark, value);
            }
        }
        private string _HeatLane;
        public string HeatLane
        {
            get { return _HeatLane; }
            set
            {
                SetProperty(ref _HeatLane, value);
            }
        }
        private bool _IsSRC;
        public bool IsSRC
        {
            get { return _IsSRC; }
            set
            {
                SetProperty(ref _IsSRC, value);
            }
        }
        private bool _IsAlt;
        public bool IsAlt
        {
            get { return _IsAlt; }
            set
            {
                SetProperty(ref _IsAlt, value);
            }
        }
        private bool _IsExh;
        public bool IsExh
        {
            get { return _IsExh; }
            set
            {
                SetProperty(ref _IsExh, value);
            }
        }
        private string _Dec;
        public string Dec
        {
            get { return _Dec; }
            set
            {
                SetProperty(ref _Dec, value);
            }
        }
        private string _ConvTime;
        public string ConvTime
        {
            get { return _ConvTime; }
            set
            {
                SetProperty(ref _ConvTime, value);
            }
        }
        private string _PLFinal;
        public string PLFinal
        {
            get { return _PLFinal; }
            set
            {
                SetProperty(ref _PLFinal, value);
            }
        }
        private string _Final;
        public string Final
        {
            get { return _Final; }
            set
            {
                SetProperty(ref _Final, value);
            }
        }

        private string _PLPrelim;
        public string PLPrelim
        {
            get { return _PLPrelim; }
            set
            {
                SetProperty(ref _PLPrelim, value);
            }
        }
        private string _Prelim;
        public string Prelim
        {
            get { return _Prelim; }
            set
            {
                SetProperty(ref _Prelim, value);
            }
        }
    }

    public class AthletesInformationViewModel : BindableBase
    {

        public AthletesInformationViewModel()
        {

        }

        #region Properties
        private string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                SetProperty(ref _FirstName, value);
            }
        }
        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set
            {
                SetProperty(ref _LastName, value);
            }
        }

        private string _Gender;
        public string Gender
        {
            get { return _Gender; }
            set
            {
                SetProperty(ref _Gender, value);
            }
        }
        private string _Age;
        public string Age
        {
            get { return _Age; }
            set
            {
                SetProperty(ref _Age, value);
            }
        }
        private string _RegistrationNumber;
        public string RegistrationNumber
        {
            get { return _RegistrationNumber; }
            set
            {
                SetProperty(ref _RegistrationNumber, value);
            }
        }
        private string _ClassYR;
        public string ClassYR
        {
            get { return _ClassYR; }
            set
            {
                SetProperty(ref _ClassYR, value);
            }
        }
        private string _CompetitorNumber;
        public string CompetitorNumber
        {
            get { return _CompetitorNumber; }
            set
            {
                SetProperty(ref _CompetitorNumber, value);
            }
        }
        private string _TeamName;
        public string TeamName
        {
            get { return _TeamName; }
            set
            {
                SetProperty(ref _TeamName, value);
            }
        }
        private string _Citizenof;
        public string Citizenof
        {
            get { return _Citizenof; }
            set
            {
                SetProperty(ref _Citizenof, value);
            }
        }
        private string _City;
        public string City
        {
            get { return _City; }
            set
            {
                SetProperty(ref _City, value);
            }
        }
        private string _State;
        public string State
        {
            get { return _State; }
            set
            {
                SetProperty(ref _State, value);
            }
        }
        private string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                SetProperty(ref _Status, value);
            }
        }
        public ObservableCollection<AthleteEvent> _AthleteEventList;
        public ObservableCollection<AthleteEvent> AthleteEventList
        {
            get { return _AthleteEventList; }
            set
            {
                SetProperty(ref _AthleteEventList, value);
            }
        }
        private AthleteEvent _SelectedAthleteEvent;
        public AthleteEvent SelectedAthleteEvent
        {
            get { return _SelectedAthleteEvent; }
            set
            {
                SetProperty(ref _SelectedAthleteEvent, value);
            }
        }
        #endregion

    }

}

