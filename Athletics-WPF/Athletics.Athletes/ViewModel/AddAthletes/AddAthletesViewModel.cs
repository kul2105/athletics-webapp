﻿using Athletics.Athletes;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Athletes.Views;
using Athletics.ModuleCommon;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Athletics.Entities.Models;

namespace Athletics.Athletes.ViewModels
{

    public class AddAthletesViewModel : BindableBase, IAddAthletesViewModel, IGenericInteractionView<AddAthletesViewModel>
    {
        private IAddAthletesView View = null;
        private IAthletesManager AthletesManager;
        IModalDialog dialog = null;
        System.Windows.Window window = null;
        private readonly IUnityContainer unityContainer;
        public bool IsCanceled;
        public AddAthletesViewModel()
        {

        }

        public AddAthletesViewModel(IUnityContainer unity, IAddAthletesView view, IAthletesManager athletesManager, List<AddAthletesViewModel> athleticsDetailList, AddAthletesViewModel selectedAddAthletesViewModel)
        {
            View = view;
            this.SetEntity(selectedAddAthletesViewModel);
            unityContainer = unity;
            AthletesManager = athletesManager;
            View.DataContext = this;
            StatusList = new List<string>();
            StatusList.Add("Normal");
            this.AthleticsDetailList = athleticsDetailList;
            this.SelectedAthleticsDetail = selectedAddAthletesViewModel;
            TeamList = new List<Team>();
            GenderList = new List<string>();
            GenderList.Add("M");
            GenderList.Add("F");
            GenderList.Add("NONE");
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<Team> teams = await AthletesManager.GetAllTeams();
                foreach (Team item in teams)
                {
                    TeamList.Add(item);
                }
            }));
            Citizen = new List<BaseCounty>();
            Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
            {
                List<BaseCounty> countryList = AthletesManager.GetAllCountry();
                foreach (BaseCounty item in countryList)
                {
                    Citizen.Add(item);
                }
            }));

            _OkCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                         {
                             await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                              {
                                  Window window = view as Window;
                                  if (window != null)
                                  {
                                      this.IsCanceled = false;
                                      foreach (Window item in Application.Current.Windows)
                                      {
                                          if (item.Name == "AddEvents")
                                          {
                                              item.Close();
                                              break;
                                          }
                                      }
                                  }
                              }));
                         }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });


            _CancelCommand = new DelegateCommand(async () =>
            {
                if (!ValidateOkCancel())
                {

                }
                else
                {
                    await Task.Run(async () =>
                    {
                        await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            await Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                            {
                                Window window = view as Window;
                                if (window != null)
                                {
                                    foreach (Window item in Application.Current.Windows)
                                    {
                                        if (item.Name == "AddEvents")
                                        {
                                            item.Close();
                                            break;
                                        }
                                    }
                                }
                            }));
                        }));

                    });
                }
            }, () =>
            {
                return ValidateOkCancel();
            });

            _DeleteElegibleCommand = new DelegateCommand(async () =>
            {
                if (!ValidateNextPrevious())
                {

                }
                else
                {
                    if (SelectedAthleteElegible != null)
                        AthleteElegibleList.Remove(SelectedAthleteElegible);
                }
            }, () =>
            {
                return ValidateNextPrevious();
            });

            _AddEventToEntryCommand = new DelegateCommand(async () =>
            {
                if (!ValidateNextPrevious())
                {

                }
                else
                {
                    if (SelectedEvent != null)
                    {
                        AthletiesElegible existedElegible = AthleteElegibleList.Where(p => p.EventID == SelectedEvent.EventID).FirstOrDefault();
                        if (existedElegible == null)
                        {
                            AthletiesElegible elebibleEvent = new AthletiesElegible();
                            elebibleEvent.EventID = SelectedEvent.EventID;
                            elebibleEvent.EventName = SelectedEvent.Event;
                            elebibleEvent.EventNumber = SelectedEvent.EventNumber;
                            AthleteElegibleList.Add(elebibleEvent);
                        }

                    }



                }
            }, () =>
            {
                return ValidateNextPrevious();
            });

            List<Entities.Models.EventDetail> eventDetailList = AthletesManager.GetAllEventDetail().Result;
            foreach (var item in eventDetailList)
            {
                EventList.Add(item);
            }
            FilterElegiblity();
            if (this.AthletesID > 0)
            {
                List<AthletiesElegible> elegibliltyList = AthletesManager.GetAllElegibleEvents(this.AthletesID).Result;
                foreach (var item in elegibliltyList)
                {
                    AthleteElegibleList.Add(item);
                }

            }
        }

        #region Properties
        private List<AddAthletesViewModel> _AllAthleticsDetailList = new List<AddAthletesViewModel>();
        public List<AddAthletesViewModel> AllAthleticsDetailList
        {
            get { return _AllAthleticsDetailList; }
            set
            {
                SetProperty(ref _AllAthleticsDetailList, value);
            }
        }
        private List<string> _GenderList;
        public List<string> GenderList
        {
            get { return _GenderList; }
            set
            {
                SetProperty(ref _GenderList, value);
            }
        }
        private List<AddAthletesViewModel> _AthleticsDetailList;
        public List<AddAthletesViewModel> AthleticsDetailList
        {
            get { return _AthleticsDetailList; }
            set
            {
                SetProperty(ref _AthleticsDetailList, value);
            }
        }

        private AddAthletesViewModel _SelectedAthleticsDetail;
        public AddAthletesViewModel SelectedAthleticsDetail
        {
            get { return _SelectedAthleticsDetail; }
            set
            {
                SetProperty(ref _SelectedAthleticsDetail, value);
            }
        }
        private int _AthletesID;
        public int AthletesID
        {
            get { return _AthletesID; }
            set
            {
                SetProperty(ref _AthletesID, value);
            }
        }
        private int _Age;
        public int Age
        {
            get { return _Age; }
            set
            {
                SetProperty(ref _Age, value);
                FilterElegiblity();
            }
        }

        private void FilterElegiblity()
        {
            if (Gender == "M")
            {
                this.ElegibleEventList = this.EventList.Where(p => p.Gender == "Girls" || p.Gender == "Women" || p.Gender == "Mixed").ToList();
            }
            else if (Gender == "F")
            {
                this.ElegibleEventList = this.EventList.Where(p => p.Gender == "Girls" || p.Gender == "Women" || p.Gender == "Mixed").ToList();
            }
            else
            {
                this.ElegibleEventList = this.EventList;
            }
            if (Age > 0)
            {
                this.ElegibleEventList = this.ElegibleEventList.Where(p => p.StartAgeRange <= this.Age && p.EndAgeRange >= Age).ToList();
            }
        }

        private List<BaseCounty> _Citizen = new List<BaseCounty>();
        public List<BaseCounty> Citizen
        {
            get { return _Citizen; }
            set
            {
                SetProperty(ref _Citizen, value);
            }
        }
        private List<State> _StateList = new List<State>();
        public List<State> StateList
        {
            get { return _StateList; }
            set
            {
                SetProperty(ref _StateList, value);
            }
        }
        private State _SelectedState;
        public State SelectedState
        {
            get { return _SelectedState; }
            set
            {
                SetProperty(ref _SelectedState, value);
            }
        }

        private BaseCounty _SelectedCitizen;
        public BaseCounty SelectedCitizen
        {
            get { return _SelectedCitizen; }
            set
            {
                SetProperty(ref _SelectedCitizen, value);
                if (value != null)
                {
                    CitizenString = value.BaseCountyName;
                    if (AthletesManager != null)
                    {
                        StateList = new List<State>();
                        Application.Current.Dispatcher.BeginInvoke(new Action(async () =>
                        {
                            List<State> sateList = AthletesManager.GetStateByCountry(value.BaseCountyID);
                            foreach (State item in sateList)
                            {
                                StateList.Add(item);
                            }
                        }));
                    }
                }
            }
        }

        private string _CitizenString;
        public string CitizenString
        {
            get { return _CitizenString; }
            set
            {
                SetProperty(ref _CitizenString, value);
            }
        }
        private string _City;
        public string City
        {
            get { return _City; }
            set
            {
                SetProperty(ref _City, value);
            }
        }
        private string _ClassYR;
        public string ClassYR
        {
            get { return _ClassYR; }
            set
            {
                SetProperty(ref _ClassYR, value);
            }
        }
        private string _CompetitorNumber;
        public string CompetitorNumber
        {
            get { return _CompetitorNumber; }
            set
            {
                SetProperty(ref _CompetitorNumber, value);
            }
        }
        private string _DataofBirthString;
        public string DataofBirthString
        {
            get { return _DataofBirthString; }
            set
            {
                SetProperty(ref _DataofBirthString, value);
            }
        }
        private DateTime _DateOfBirth = DateTime.Now;
        public DateTime DateOfBirth
        {
            get { return _DateOfBirth; }
            set
            {
                SetProperty(ref _DateOfBirth, value);
                if (value != null)
                {
                    DataofBirthString = value.ToShortDateString();
                    this.Age = this.CalculateAge(value);
                }
            }
        }

        private int CalculateAge(DateTime value)
        {
            int age = 0;
            age = DateTime.Now.Year - value.Year;
            if (DateTime.Now.DayOfYear < value.DayOfYear)
            {
                age = age - 1;
            }
            return age;
        }

        private string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                SetProperty(ref _FirstName, value);
            }
        }
        private string _Gender;
        public string Gender
        {
            get { return _Gender; }
            set
            {
                SetProperty(ref _Gender, value);
                FilterElegiblity();
            }
        }

        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set
            {
                SetProperty(ref _LastName, value);
            }
        }

        private string _MI;
        public string MI
        {
            get { return _MI; }
            set
            {
                SetProperty(ref _MI, value);
            }
        }
        private string _RegistrationNumber;
        public string RegistrationNumber
        {
            get { return _RegistrationNumber; }
            set
            {
                SetProperty(ref _RegistrationNumber, value);
            }
        }
        private List<SchoolDetail> _SchoolList = new List<SchoolDetail>();
        public List<SchoolDetail> SchoolList
        {
            get { return _SchoolList; }
            set
            {
                SetProperty(ref _SchoolList, value);
            }
        }

        private SchoolDetail _SelectedSchool;
        public SchoolDetail SelectedSchool
        {
            get { return _SelectedSchool; }
            set
            {
                SetProperty(ref _SelectedSchool, value);
                if (value != null)
                {
                    this.SchoolName = value.SchoolName;
                    this.SchoolAbb = value.Abbr;
                }
            }
        }

        private string _SchoolName;
        public string SchoolName
        {
            get { return _SchoolName; }
            set
            {
                SetProperty(ref _SchoolName, value);
            }
        }
        private string _SchoolAbb;
        public string SchoolAbb
        {
            get { return _SchoolAbb; }
            set
            {
                SetProperty(ref _SchoolAbb, value);
            }
        }

        private string _State;
        public string State
        {
            get { return _State; }
            set
            {
                SetProperty(ref _State, value);
            }
        }
        private string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                SetProperty(ref _Status, value);
            }
        }

        private List<Team> _TeamList = new List<Team>();
        public List<Team> TeamList
        {
            get { return _TeamList; }
            set
            {
                SetProperty(ref _TeamList, value);
            }
        }
        private List<string> _StatusList;
        public List<string> StatusList
        {
            get { return _StatusList; }
            set
            {
                SetProperty(ref _StatusList, value);
            }
        }
        private string _TeamString;
        public string TeamString
        {
            get { return _TeamString; }
            set
            {
                SetProperty(ref _TeamString, value);
            }
        }

        private Team _Team;
        public Team Team
        {
            get { return _Team; }
            set
            {
                SetProperty(ref _Team, value);
                if (value != null)
                {
                    TeamString = value.TeamName;
                }
            }
        }
        private List<Entities.Models.EventDetail> _EventList = new List<Entities.Models.EventDetail>();
        public List<Entities.Models.EventDetail> EventList
        {
            get { return _EventList; }
            set
            {
                SetProperty(ref _EventList, value);
            }
        }
        private List<Entities.Models.EventDetail> _ElegibleEventList = new List<Entities.Models.EventDetail>();
        public List<Entities.Models.EventDetail> ElegibleEventList
        {
            get { return _ElegibleEventList; }
            set
            {
                SetProperty(ref _ElegibleEventList, value);
            }
        }
        private Entities.Models.EventDetail _SelectedEvent = new Entities.Models.EventDetail();
        public Entities.Models.EventDetail SelectedEvent
        {
            get { return _SelectedEvent; }
            set
            {
                SetProperty(ref _SelectedEvent, value);
            }
        }
        private ObservableCollection<AthletiesElegible> _AthleteElegibleList = new ObservableCollection<AthletiesElegible>();
        public ObservableCollection<AthletiesElegible> AthleteElegibleList
        {
            get { return _AthleteElegibleList; }
            set
            {
                SetProperty(ref _AthleteElegibleList, value);
            }
        }
        private AthletiesElegible _SelectedAthleteElegible;
        public AthletiesElegible SelectedAthleteElegible
        {
            get { return _SelectedAthleteElegible; }
            set
            {
                SetProperty(ref _SelectedAthleteElegible, value);
            }
        }
        private AthleteMode _Mode;
        public AthleteMode Mode
        {
            get { return _Mode; }
            set
            {
                SetProperty(ref _Mode, value);
                if (value == AthleteMode.Add)
                {
                    this.GenerateRegistrationID();
                    this.GenerateComptiterID();
                }
            }
        }

        private void GenerateComptiterID()
        {
            if (AllAthleticsDetailList.Count > 0)
                CompetitorNumber = AllAthleticsDetailList.Max(p => Convert.ToInt32(p.CompetitorNumber) + 1).ToString();
            else
                CompetitorNumber = "1";
        }

        private void GenerateRegistrationID()
        {
            if (AllAthleticsDetailList.Count > 0)
                RegistrationNumber = AllAthleticsDetailList.Max(p => Convert.ToInt32(p.RegistrationNumber) + 1).ToString();
            else
                RegistrationNumber = "1";
        }
        #endregion

        #region Reset UI

        #endregion

        #region Form Properties


        #endregion

        private bool ValidateNextPrevious()
        {
            return true;
        }

        private bool ValidateOkCancel()
        {
            return true;
        }

        public void SetEntity(AddAthletesViewModel entity)
        {
            if (entity == null) return;
            this.Age = entity.Age;
            this.SelectedCitizen = entity.SelectedCitizen;
            this.Citizen = entity.Citizen;
            this.AthletesID = entity.AthletesID;
            this.City = entity.City;
            this.ClassYR = entity.ClassYR;
            this.CompetitorNumber = entity.CompetitorNumber;
            this.DataofBirthString = entity.DataofBirthString;
            this.DateOfBirth = entity.DateOfBirth;
            this.FirstName = entity.FirstName;
            this.Gender = entity.Gender;
            this.LastName = entity.LastName;
            this.MI = entity.MI;
            this.RegistrationNumber = entity.RegistrationNumber;
            this.SchoolAbb = entity.SchoolAbb;
            this.SchoolName = entity.SchoolName;
            this.SelectedSchool = entity.SelectedSchool;
            this.SelectedState = entity.SelectedState;
            this.State = entity.State;
            this.Status = entity.Status;
            this.Team = entity.Team;

        }
        public AddAthletesViewModel GetEntity()
        {

            return this;
        }


        #region Command
        private ICommand _OkCommand;
        public ICommand OkCommand
        {
            get { return _OkCommand; }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get { return _CancelCommand; }
        }

        private ICommand _DeleteElegibleCommand;
        public ICommand DeleteElegibleCommand
        {
            get { return _DeleteElegibleCommand; }
        }

        private ICommand _AddEventToEntryCommand;
        public ICommand AddEventToEntryCommand
        {
            get { return _AddEventToEntryCommand; }
        }




        #endregion
    }

}

