﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.ViewModels
{
    public enum AthleteMode
    {
        Add,
        Edit
    }
    public interface IAddAthletesViewModel
    {
        List<AddAthletesViewModel> AllAthleticsDetailList { get; set; }
        AthleteMode Mode { get; set; }
    }
}
