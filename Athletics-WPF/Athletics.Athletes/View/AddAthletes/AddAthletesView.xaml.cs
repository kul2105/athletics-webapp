﻿using Athletics.Athletes.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddAthletesView : Window, IAddAthletesView
    {
        public AddAthletesView()
        {
            InitializeComponent();
            //this.DataContext = new CreateReportViewModel(propertyGrid, listBoxMiddle);

        }
        public void ResetFocus()
        {
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddAthletesViewModel vm = this.DataContext as AddAthletesViewModel;
            if (vm != null)
            {
                vm.IsCanceled = true;
            }
            PART_CLOSE_Click(null, e);
        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }

        private void DateTimePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            AddAthletesViewModel vm = this.DataContext as AddAthletesViewModel;
            if (vm != null)
            {
               // vm.DateOfBirth = e.AddedItems[0];
            }
        }

        private void DateTimePicker_LostFocus(object sender, RoutedEventArgs e)
        {
            Microsoft.Windows.Controls.DateTimePicker datetimePicker = sender as Microsoft.Windows.Controls.DateTimePicker;
            if (datetimePicker != null)
            {
                AddAthletesViewModel vm = this.DataContext as AddAthletesViewModel;
                if (vm != null)
                {
                    vm.DateOfBirth = System.Convert.ToDateTime(datetimePicker.Text);
                }
            }
        }
    }
}
