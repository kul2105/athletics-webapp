﻿namespace Athletics.Athletes.Views
{
    public interface IAddAthletesView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
