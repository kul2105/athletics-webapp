﻿namespace Athletics.Athletes.Views
{
    public interface IAddAthleteResultView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
