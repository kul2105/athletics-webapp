﻿using Athletics.Athletes.AthleticsManager;
using Athletics.Athletes.ViewModels;
using Athletics.Entities.Models;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AthletesInformationView : Window
    {
        public AthletesInformationView(int? AthleteID)
        {
            InitializeComponent();
            AthletesManager athleteManager = new AthletesManager();
            List<AddAthletesViewModel> athleteList = athleteManager.GetAllAthleties().Result;
            AddAthletesViewModel athlte = athleteList.Where(p => p.AthletesID == AthleteID).FirstOrDefault();
            if (athlte == null)
            {
                return;
            }
            AthletesInformationViewModel viewModel = new AthletesInformationViewModel();
            viewModel.Age = athlte.Age.ToString();
            viewModel.Citizenof = athlte.CitizenString;
            viewModel.City = athlte.City;
            viewModel.ClassYR = athlte.ClassYR;
            viewModel.CompetitorNumber = athlte.CompetitorNumber;
            viewModel.FirstName = athlte.FirstName;
            viewModel.Gender = athlte.Gender;
            viewModel.LastName = athlte.LastName;
            viewModel.RegistrationNumber = athlte.RegistrationNumber;
            viewModel.State = athlte.State;
            viewModel.Status = athlte.Status;
            viewModel.TeamName = athlte.TeamString;
            List<AthletiesElegible> elegibleAthleteList = athleteManager.GetAllElegibleEvents(athlte.AthletesID).Result;
            foreach (var item in elegibleAthleteList)
            {
                AthleteEvent athleteEvent = new AthleteEvent();
                athleteEvent.ConvTime = item.ConvTime == null ? "0.0" : item.ConvTime.Value.ToString();
                athleteEvent.Dec = item.Dec;
                athleteEvent.EntryMark = item.EntryMark;
                athleteEvent.Event = item.EventName;
                athleteEvent.EventNumber = item.EventNumber;
                //athleteEvent.Final = item.Final;
                athleteEvent.HeatLane = item.HeatLane;
                athleteEvent.IsAlt = item.IsAlt == null ? false : item.IsAlt.Value;
                athleteEvent.IsExh = item.IsExh == null ? false : item.IsExh.Value;
                athleteEvent.IsSRC = item.IsSRC == null ? false : item.IsSRC.Value;
                //athleteEvent.PLFinal = item.PLFinal;
                //athleteEvent.PLPrelim = item.PLPrelim;
                //athleteEvent.Prelim = item.Prelim;
                viewModel.AthleteEventList.Add(athleteEvent);
            }
            this.DataContext = viewModel;

        }
        public void ResetFocus()
        {
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddAthletesViewModel vm = this.DataContext as AddAthletesViewModel;
            if (vm != null)
            {
                vm.IsCanceled = true;
            }
            PART_CLOSE_Click(null, e);
        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }

        private void DateTimePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            AddAthletesViewModel vm = this.DataContext as AddAthletesViewModel;
            if (vm != null)
            {
                // vm.DateOfBirth = e.AddedItems[0];
            }
        }

        private void DateTimePicker_LostFocus(object sender, RoutedEventArgs e)
        {
            Microsoft.Windows.Controls.DateTimePicker datetimePicker = sender as Microsoft.Windows.Controls.DateTimePicker;
            if (datetimePicker != null)
            {
                AddAthletesViewModel vm = this.DataContext as AddAthletesViewModel;
                if (vm != null)
                {
                    vm.DateOfBirth = System.Convert.ToDateTime(datetimePicker.Text);
                }
            }
        }
    }
}
