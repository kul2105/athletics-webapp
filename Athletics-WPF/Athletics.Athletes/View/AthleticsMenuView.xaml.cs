﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class AthleticsMenuView : UserControl, IAthleticsMenuView
    {
        public AthleticsMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
