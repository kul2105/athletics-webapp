﻿using Athletics.Athletes.ViewModels;
using Athletics.ControlLibrary.DataGrid;
using System.Collections.ObjectModel;

namespace Athletics.Athletes.Views
{
    public interface IManageRelayView
    {
        AthleticsGrid EventGrid { get; }
        object DataContext { get; set; }
        void ResetFocus();
    }

}
