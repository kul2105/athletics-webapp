﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Athletics.Athletes.ViewModels;
using Athletics.ControlLibrary.DataGrid;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ManageRelayView : Window, IManageRelayView
    {
        public AthleticsGrid EventGrid { get => this.dataGrid1; }
        public ManageRelayView()
        {
            InitializeComponent();
            this.Loaded += ManageRelayView_Loaded;
            //this.DataContext = new CreateReportViewModel(propertyGrid, listBoxMiddle);

        }

        private void ManageRelayView_Loaded(object sender, RoutedEventArgs e)
        {
            ManageRelayViewModel managerelayVM = this.DataContext as ManageRelayViewModel;
            if (managerelayVM != null)
            {
                if(managerelayVM.RelayDetailList!=null)
                {
                    foreach (var item in managerelayVM.RelayDetailList)
                    {
                        item.IsEdit = false;
                    }
                    
                }
            }

        }

        public void ResetFocus()
        {
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ManageRelayViewModel managerelayVM = this.DataContext as ManageRelayViewModel;
            if (managerelayVM != null)
            {
                managerelayVM.EditCommandHandler();
            }
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);
            
        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }

        private void RelayGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ManageRelayViewModel managerelayVM = this.DataContext as ManageRelayViewModel;
            if (managerelayVM != null)
            {
                managerelayVM.AddRelayCommand.Execute(null);
            }
        }
    }
}
