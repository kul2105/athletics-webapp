﻿namespace Athletics.Athletes.Views
{
    public interface IAddRelayView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
