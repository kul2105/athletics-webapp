﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class RelayMenuView : UserControl, IRelayMenuView
    {
        public RelayMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
