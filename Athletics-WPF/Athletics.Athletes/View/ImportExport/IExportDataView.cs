﻿namespace Athletics.Athletes.Views
{
    public interface IExportDataView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
