﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class TeamMenuView : UserControl, ITeamMenuView
    {
        public TeamMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
