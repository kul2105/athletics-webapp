﻿namespace Athletics.Athletes.Views
{
    public interface IAthleticsMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
