﻿namespace Athletics.Athletes.Views
{
    public interface IRelayMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
