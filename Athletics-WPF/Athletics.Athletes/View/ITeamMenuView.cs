﻿namespace Athletics.Athletes.Views
{
    public interface ITeamMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
