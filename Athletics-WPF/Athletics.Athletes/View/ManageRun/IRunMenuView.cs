﻿namespace Athletics.Athletes.Views
{
    public interface IRunMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
