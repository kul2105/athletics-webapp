﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Athletics.Athletes.Common;
using Athletics.Athletes.ViewModels;
using Athletics.ControlLibrary.DataGrid;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ManageRunView : Window, IManageRunView
    {
        public delegate Point GetPosition(IInputElement element);
        int rowIndex = -1;
        public ManageRunView()
        {
            InitializeComponent();
            this.Loaded += ManageRunView_Loaded;
            TeamEntry.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(TeamEntry_PreviewMouseLeftButtonDown);
            TeamEntry.Drop += new DragEventHandler(TeamEntry_Drop);
            //this.DataContext = new CreateReportViewModel(propertyGrid, listBoxMiddle);

        }

        private void TeamEntry_Drop(object sender, DragEventArgs e)
        {
            ManageRunViewModel runVm = this.DataContext as ManageRunViewModel;
            if (runVm != null)
            {
                if (rowIndex < 0)
                    return;
                int index = this.GetCurrentRowIndex(e.GetPosition);
                if (index < 0)
                    return;
                if (index == rowIndex)
                    return;
                if (index == TeamEntry.Items.Count - 1)
                {
                    MessageBox.Show("This row-index cannot be drop");
                    return;
                }
                if (runVm.SelectedEvent == null) return;
                AthleteLane changedAthlete = runVm.AthletesList[rowIndex];
                AthleteLane droppedOverAthlete = runVm.AthletesList[index];
                if (changedAthlete != null && droppedOverAthlete != null)
                {
                    int athleteId = changedAthlete.AthleteID;
                    string athelteName = changedAthlete.AthleteName;
                    string CompetitorNumber = changedAthlete.CompetitorNumber;
                    string ClassYR = changedAthlete.ClassYR;
                    changedAthlete.AthleteID = droppedOverAthlete.AthleteID;
                    changedAthlete.AthleteName = droppedOverAthlete.AthleteName;
                    changedAthlete.SetCompetitorNumber(droppedOverAthlete.CompetitorNumber);
                    changedAthlete.ClassYR = droppedOverAthlete.ClassYR;
                    droppedOverAthlete.AthleteID = athleteId;
                    droppedOverAthlete.AthleteName = athelteName;
                    droppedOverAthlete.SetCompetitorNumber(CompetitorNumber);
                    droppedOverAthlete.ClassYR = ClassYR;
                    droppedOverAthlete.EventID = runVm.SelectedEvent.EventID;
                    changedAthlete.EventID = runVm.SelectedEvent.EventID;
                }
                AthleteLane laneToDelete = runVm.AllAddedAthletesList.Where(p => p.AthleteID == changedAthlete.AthleteID && p.EventID == changedAthlete.EventID).FirstOrDefault();
                if (laneToDelete != null)
                {
                    runVm.AllAddedAthletesList.Remove(laneToDelete);
                }
                runVm.AllAddedAthletesList.Add(changedAthlete);
                AthleteLane laneToDelete1 = runVm.AllAddedAthletesList.Where(p => p.AthleteID == droppedOverAthlete.AthleteID && p.EventID == changedAthlete.EventID).FirstOrDefault();
                if (laneToDelete1 != null)
                {
                    runVm.AllAddedAthletesList.Remove(laneToDelete1);
                }
                runVm.AllAddedAthletesList.Add(droppedOverAthlete);
            }
        }

        private void TeamEntry_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ManageRunViewModel runVm = this.DataContext as ManageRunViewModel;
            if (runVm != null)
            {
                rowIndex = GetCurrentRowIndex(e.GetPosition);
                if (rowIndex < 0)
                    return;
                TeamEntry.SelectedIndex = rowIndex;
                AthleteLane selectedEmp = runVm.AthletesList[rowIndex] as AthleteLane;
                if (selectedEmp == null)
                    return;
                DragDropEffects dragdropeffects = DragDropEffects.Move;
                if (DragDrop.DoDragDrop(TeamEntry, selectedEmp, dragdropeffects)
                                    != DragDropEffects.None)
                {
                    TeamEntry.SelectedItem = selectedEmp;
                }
            }
        }
        private bool GetMouseTargetRow(Visual theTarget, GetPosition position)
        {
            Rect rect = VisualTreeHelper.GetDescendantBounds(theTarget);
            Point point = position((IInputElement)theTarget);
            return rect.Contains(point);
        }

        private DataGridRow GetRowItem(int index)
        {
            if (TeamEntry.ItemContainerGenerator.Status
                    != GeneratorStatus.ContainersGenerated)
                return null;
            return TeamEntry.ItemContainerGenerator.ContainerFromIndex(index)
                                                            as DataGridRow;
        }

        private int GetCurrentRowIndex(GetPosition pos)
        {
            int curIndex = -1;
            for (int i = 0; i < TeamEntry.Items.Count; i++)
            {
                DataGridRow itm = GetRowItem(i);
                if (GetMouseTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }
            }
            return curIndex;
        }
        private void ManageRunView_Loaded(object sender, RoutedEventArgs e)
        {
            ManageRelayViewModel managerelayVM = this.DataContext as ManageRelayViewModel;
            if (managerelayVM != null)
            {
                if (managerelayVM.RelayDetailList != null)
                {
                    foreach (var item in managerelayVM.RelayDetailList)
                    {
                        item.IsEdit = false;
                    }

                }
            }

        }

        public void ResetFocus()
        {
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ManageRunViewModel managerelayVM = this.DataContext as ManageRunViewModel;
            if (managerelayVM != null)
            {
                managerelayVM.UpdateHeat();
            }
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);

        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }


        private void TeamEntry_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TextBlock textBlock = e.OriginalSource as TextBlock;
            if (textBlock == null) return;
            DataGridCell cell = textBlock.Parent as DataGridCell;
            if (cell == null) return;
            if (cell.Column == null) return;
            if (cell.Column.SortMemberPath == "AthleteName")
            {
                ManageRunViewModel managerrunVM = this.DataContext as ManageRunViewModel;
                if (managerrunVM != null)
                {
                    if (managerrunVM.SelectedAthlete.AthleteID > 0)
                    {
                        AthletesInformationView athelteInfo = new AthletesInformationView(managerrunVM.SelectedAthlete.AthleteID);
                        athelteInfo.ShowDialog();
                    }
                    ;
                }
            }
        }


    }
}
