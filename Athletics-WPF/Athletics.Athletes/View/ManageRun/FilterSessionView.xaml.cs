﻿using Athletics.Entities.Models;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Athletics.Athletes.View.ManageRun
{
    /// <summary>
    /// Interaction logic for FilterSessionView.xaml
    /// </summary>
    public partial class FilterSessionView : Window
    {
        public FilterSessionView()
        {
            InitializeComponent();
            this.InitilzeSession();
        }
        public List<int> EventIds = new List<int>();
        public string sessionNames = string.Empty;
        private void InitilzeSession()
        {
            AthleticsManager.AthletesManager manager = new AthleticsManager.AthletesManager();
            List<EventSession> sessionList = manager.GetAllSessions().Result;
            this.dataGrid2.FilteredItemsSource = sessionList;
        }
        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PART_CLOSE_Click(null, e);

        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            List<EventSession> session = new List<EventSession>();
            foreach (var item in this.dataGrid2.SelectedItems)
            {
                EventSession s = item as EventSession;
                session.Add(s);
            }
            if (session != null)
            {
                AthleticsManager.AthletesManager manager = new AthleticsManager.AthletesManager();
                List<int?> sessionIDs = new List<int?>();
                List<EventSession> eventSessionList = new List<EventSession>();
                foreach (var item in session.Select(p => p.EventSessionID).ToList())
                {
                    sessionIDs.Add(item);
                }
                foreach (var item in session.ToList())
                {
                    eventSessionList.Add(item);
                }
                List<EventInSession> sessionList = manager.GetAllEventSessions(sessionIDs).Result;
                foreach (var item in sessionList)
                {
                    if (item.EventID != null)
                    {
                        this.EventIds.Add(item.EventID.Value);
                    }
                }
                sessionNames = string.Join(",", eventSessionList.Select(p => p.Session));
            }
            this.Visibility = Visibility.Hidden;
        }
    }
}
