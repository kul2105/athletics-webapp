﻿using System;
using System.Windows.Controls;

namespace Athletics.Athletes.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class RunMenuView : UserControl, IRunMenuView
    {
        public RunMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
