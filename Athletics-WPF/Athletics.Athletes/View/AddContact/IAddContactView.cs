﻿namespace Athletics.Athletes.Views
{
    public interface IAddContactView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
