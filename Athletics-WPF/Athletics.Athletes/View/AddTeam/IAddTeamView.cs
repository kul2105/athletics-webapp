﻿namespace Athletics.Athletes.Views
{
    public interface IAddTeamView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }

}
