﻿using Athletics.Athletes.AthleticsManager;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athletics.Athletes.Common
{
    public class GlobalEvent
    {
        public delegate void TargetControlLoadedDelegate(object targetObject);
        public static event TargetControlLoadedDelegate TargetControlLoaded;
        public static void RaiseTargetControlLoaded(object targetObject)
        {
            if (TargetControlLoaded != null)
                TargetControlLoaded(targetObject);
        }

        public static IAthletesManager AthletesManager { get; set; }
    }
}
