﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Athletics.Athletes.Model
{
    public class TeamMenuItemViewModel
    {
        private ICommand _command;
        public TeamMenuItemViewModel()
        {
            MenuItems = new ObservableCollection<TeamMenuItemViewModel>();
        }

        public string Header { get; set; }
        public int MenuID { get; set; }
        public ObservableCollection<TeamMenuItemViewModel> MenuItems { get; set; }

        public ICommand Command
        {
            get
            {
                return _command;
            }
            set
            {
                _command = value;
            }
        }
    }
    
}
