﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Athletics.Athletes.Model
{
    public class RelayMenuItemViewModel
    {
        private ICommand _command;
        public RelayMenuItemViewModel()
        {
            MenuItems = new ObservableCollection<RelayMenuItemViewModel>();
        }

        public string Header { get; set; }
        public int MenuID { get; set; }
        public ObservableCollection<RelayMenuItemViewModel> MenuItems { get; set; }

        public ICommand Command
        {
            get
            {
                return _command;
            }
            set
            {
                _command = value;
            }
        }
    }
    
}
