﻿using Athletics.ModuleCommon;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Athletics.Athletes.TemplateSelector
{
    public class OptionTemplateSelector : DataTemplateSelector
    {
        public override System.Windows.DataTemplate SelectTemplate(object item,
                        System.Windows.DependencyObject container)
        {
            Option athletOption = item as Option;
            if (athletOption != null)
            {
                if (Boolean.Parse(athletOption.Tag) == false)
                {
                    DataTemplate datatemplateCheckBox = Application.Current.FindResource("CheckBoxStyle") as DataTemplate;
                    return datatemplateCheckBox;
                }
                else
                {
                    DataTemplate datatemplateRadiobutton = Application.Current.FindResource("RadioButtonStyle") as DataTemplate;
                    return datatemplateRadiobutton;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
