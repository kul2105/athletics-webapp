﻿using Athletics.Athletes.AthleticsManager;
using Athletics.Athletes.ViewModels;
using Athletics.Athletes.Views;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;

namespace Athletics.Athletes
{
    public class AthletesModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public AthletesModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
            //this.container.TryResolve<IAssigPrefrencesViewModel>();
        }

        public void Initialize()
        {
            this.container.RegisterType<IAthletesManager, AthletesManager>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddAthletesView, AddAthletesView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageAthletesView, ManageAthletesView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAthleticsMenuView, AthleticsMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddAthletesViewModel, AddAthletesViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageAthletesViewModel, ManageAthletesViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAthleticsMenuViewModel, AthleticsMenuViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IRelayMenuView, RelayMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IRelayMenuViewModel, RelayMenuViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageRelayView, ManageRelayView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageRelayViewModel, ManageRelayViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddRelayView, AddRelayView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddRelayViewModel, AddRelayViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<ITeamMenuView, TeamMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<ITeamMenuViewModel, TeamMenuViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageTeamView, ManageTeamView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageTeamViewModel, ManageTeamViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddTeamView, AddTeamView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddTeamViewModel, AddTeamViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddContactViewModel, AddContactViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddContactView, AddContactView>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IManageRunView, ManageRunView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IManageRunViewModel, ManageRunViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IRunMenuView, RunMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IRunMenuViewModel, RunMenuViewModel>(new ContainerControlledLifetimeManager());

            this.container.RegisterType<IExportDataView, ExportDataView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IExportDataViewModel, ExportDataViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddAthleteResultView, AddAthleteResultView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAddAthleteResultViewModel, AddAthleteResultViewModel>(new ContainerControlledLifetimeManager());

            this.regionManager.RegisterViewWithRegion("TeamMenuRegion", typeof(TeamMenuView));
            this.regionManager.RegisterViewWithRegion("AthleticMenuRegion", typeof(AthleticsMenuView));
            this.regionManager.RegisterViewWithRegion("RelayMenuRegion", typeof(RelayMenuView));
            this.regionManager.RegisterViewWithRegion("RunMenuRegion", typeof(RunMenuView));

        }
    }
}
