﻿using System;
using System.Windows.Controls;

namespace AthleticsLauncher.Views
{
    /// <summary>
    /// Interaction logic for UserMenuView.xaml
    /// </summary>
    public partial class UserMenuView : UserControl, IUserMenuView
    {
        public UserMenuView()
        {
            InitializeComponent();
        }

        public void ResetFocus()
        {
        }
    }
}
