﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AthleticsLauncher.Views
{
    public interface IAthleticsLauncherShellView
    {
        object DataContext { get; set; }
    }
}
