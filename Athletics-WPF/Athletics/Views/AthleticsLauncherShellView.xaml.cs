﻿using AthleticsLauncher.ViewModels;
using System.Windows;
using System.Windows.Input;

namespace AthleticsLauncher.Views
{
    /// <summary>
    /// Interaction logic for AthleticsLauncherShellView.xaml
    /// </summary>
    public partial class AthleticsLauncherShellView : Window, IAthleticsLauncherShellView
    {
        public AthleticsLauncherShellView()
        {
            InitializeComponent();
        }
        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
            }
            else
            {
                var window = (Window)((FrameworkElement)sender).TemplatedParent;
                window.DragMove();
            }

        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.Close();
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {

            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            if (window.WindowState == WindowState.Maximized)
                window.WindowState = WindowState.Normal;
            else window.WindowState = WindowState.Maximized;
        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender).TemplatedParent;
            window.WindowState = WindowState.Minimized;
        }

    }
}
