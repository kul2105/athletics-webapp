﻿namespace AthleticsLauncher.Views
{
    public interface IUserMenuView
    {
        object DataContext { get; set; }
        void ResetFocus();
    }
}
