﻿using Athletics.Athletes.Views;
using Athletics.Client.ViewModels;
using Athletics.Client.Views;
using Athletics.MeetSetup.Views;
using Athletics.MeetSetup.Views.SeedPreferences;
using AthleticsLauncher.ViewModels;
using AthleticsLauncher.Views;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;

namespace AthleticsLauncher
{
    public class AthleticsLauncherModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public AthleticsLauncherModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<IUserMenuViewModel, UserMenuViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IUserRoleViewModel, UserRoleViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IUserRoleViewModel, UserRoleViewModel>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IUserMenuView, UserMenuView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IUserRoleView, UserRoleView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAthleticsLauncherShellView, AthleticsLauncherShellView>(new ContainerControlledLifetimeManager());
            this.container.RegisterType<IAthleticsLauncherShellViewModel, AthleticsLauncherShellViewModel>(new ContainerControlledLifetimeManager());

            //This is the main application shell
            this.regionManager.RegisterViewWithRegion("AuthenticatedShellViewRegion", () =>
            {
                return this.container.TryResolve<IAuthenticatedShellView>();
            });

            this.regionManager.RegisterViewWithRegion("MeetSetupShellViewRegion", () =>
             {
                 return this.container.TryResolve<IMeetSetupShellView>();
             });

            this.regionManager.RegisterViewWithRegion("EventRegion", () =>
            {
                return this.container.TryResolve<IManageEventView>();
            });
            //this.regionManager.RegisterViewWithRegion("MyAccountViewRegion", typeof(MyAccountView));
            //this.regionManager.RegisterViewWithRegion("EventRegion", typeof(ManageEventView));
            this.regionManager.RegisterViewWithRegion("MenuRegion", typeof(UserMenuView));
           
        }
    }
}
