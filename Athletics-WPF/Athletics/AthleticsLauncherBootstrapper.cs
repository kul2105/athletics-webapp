﻿using Athletics.Athletes;
using Athletics.Athletes.ViewModels;
using Athletics.Athletes.Views;
using Athletics.Client;
using Athletics.Client.Controllers;
using Athletics.Client.ViewModels;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.MeetSetup;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.Services.API;
using Athletics.MeetSetup.ViewModels;
using Athletics.MeetSetup.ViewModels.SeedPreferences;
using Athletics.MeetSetup.Views;
using Athletics.Report;
using Athletics.Report.ViewModel;
using AthleticsLauncher.ViewModels;
using AthleticsLauncher.Views;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using System;
using System.Windows;

namespace AthleticsLauncher
{
    public class AthleticsLauncherBootstrapper : UnityBootstrapper
    {
        AthleticsLauncherShellView ShellView;
        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();

            ModuleCatalog moduleCatalog = (ModuleCatalog)this.ModuleCatalog;
            moduleCatalog.AddModule(typeof(AthleticsClientModule));
            moduleCatalog.AddModule(typeof(MeetSetupModule));
            moduleCatalog.AddModule(typeof(AthleticsLauncherModule));
            moduleCatalog.AddModule(typeof(ReportModule));
            moduleCatalog.AddModule(typeof(EventModule));
            moduleCatalog.AddModule(typeof(AthletesModule));
        }

        protected override DependencyObject CreateShell()
        {
            this.Container.RegisterType<IAthleticsLauncherShellView, AthleticsLauncherShellView>(new ContainerControlledLifetimeManager());
            ShellView = this.Container.TryResolve<AthleticsLauncherShellView>();
            return ShellView;
        }

        protected override void InitializeModules()
        {
            try
            {
                base.InitializeModules();
                ServiceProvider.RegisterServiceLocator(new Athletics.MeetSetup.Services.UnityServiceLocator());
                ServiceProvider.Instance.Register<IModalDialog, MeetViewDialog>();
                this.Container.TryResolve<IAuthenticatedShellViewModel>();
                this.Container.TryResolve<ISignInDefaultViewModel>();
                this.Container.TryResolve<ISigningInPleaseWaitViewModel>();
                this.Container.TryResolve<ISignUpViewModel>();
                this.Container.TryResolve<IAccountTitleBarViewModel>();
                this.Container.TryResolve<IUserMenuViewModel>();
                this.Container.TryResolve<IMyAccountViewModel>();
                this.Container.TryResolve<IAccountTitleBarViewModel>();
                this.Container.TryResolve<IUserRoleViewModel>();
                this.Container.TryResolve<IUserInRoleViewModel>();
                this.Container.TryResolve<IApplicationUserController>();
                this.Container.TryResolve<IApplicationMenuViewModel>();
                this.Container.TryResolve<IMenuInRoleViewModel>();
                this.Container.TryResolve<IMeetSetupViewModel>();
                this.Container.TryResolve<IMeetSetupShellViewModel>();
                this.Container.TryResolve<IAthleticsLauncherShellViewModel>();
                this.Container.TryResolve<IManageMeetSetupViewModel>();
                this.Container.TryResolve<IAthleteRelayPreferencesViewModel>();
                this.Container.TryResolve<ISeedPreferencesViewModel>();
                this.Container.TryResolve<IStandardLanePrefrencesViewModel>();
                this.Container.TryResolve<IStandardAlleyPrefrencesViewModel>();
                this.Container.TryResolve<IWaterfallStartViewModel>();
                this.Container.TryResolve<IAssigPrefrencesViewModel>();
                this.Container.TryResolve<IServiceRequest_StandardLanePrefrences>();
                this.Container.TryResolve<IServiceRequest_StandardAlleyPrefrences>();
                this.Container.TryResolve<IServiceRequest_WaterfallStartPreference>();
                this.Container.TryResolve<IReportManager>();
                this.Container.TryResolve<ICreateDataSetViewModel>();
                this.Container.TryResolve<IAddAthletesViewModel>();
                this.Container.TryResolve<IManageAthletesViewModel>();
                this.Container.TryResolve<IManageEventViewModel>();
                this.Container.TryResolve<IAthleticsMenuViewModel>();
                this.Container.TryResolve<IEventsMenuViewModel>();
                this.Container.TryResolve<IManageSessionViewModel>();
                //this.Container.TryResolve<IManageTimeMarkStandardViewModel>();
                this.Container.TryResolve<ITimeMarkStandardMenuViewModel>();
                this.Container.TryResolve<ISessionMenuViewModel>();
                this.Container.TryResolve<IRecordMenuViewModel>();
                this.Container.TryResolve<IAddTeamViewModel>();
                this.Container.TryResolve<IManageTeamViewModel>();
                this.Container.TryResolve<ITeamMenuViewModel>();
                this.Container.TryResolve<IAddRelayView>();
                this.Container.TryResolve<IAddRelayViewModel>();
                this.Container.TryResolve<IManageRelayViewModel>();
                this.Container.TryResolve<IRelayMenuViewModel>();
                this.Container.TryResolve<IAddContactViewModel>();
                this.Container.TryResolve<IManageSeedingViewModel>();
                this.Container.TryResolve<ISeedingMenuViewModel>();
                this.Container.TryResolve<IManageRunViewModel>();
                this.Container.TryResolve<IRunMenuViewModel>();
                this.Container.TryResolve<IExportDataViewModel>();
                this.Container.TryResolve<IAddAthleteResultViewModel>();
                Application.Current.MainWindow.Show();
                IApplicationAuthStateController AuthStateController = this.Container.TryResolve<IApplicationAuthStateController>();
                IMeetSetupStateController MeetController = this.Container.TryResolve<IMeetSetupStateController>();
                MeetController.MeetState = e_MEET_STATE.NONE;
                AuthStateController.AuthenticationState = e_AUTH_STATE.NOT_LOGGED_IN;
                AuthStateController.AuthenticationState = e_AUTH_STATE.SIGN_IN_DEFAULT;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void InitializeShell()
        {
            RegisterTypeIfMissing(typeof(IRegionManager), typeof(RegionManager), true);
            base.InitializeShell();

            //App.Current.MainWindow = (Window)this.Shell;
            //App.Current.MainWindow.Show();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Application.Current.Resources.Add("IoC", this.Container);
        }


    }
}
