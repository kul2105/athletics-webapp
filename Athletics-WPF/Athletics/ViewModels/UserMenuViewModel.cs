﻿using Athletics.Athletes.ViewModels;
using Athletics.Athletes.Views;
using Athletics.Client.Controllers;
using Athletics.Client.Services.SignInManager;
using Athletics.Client.ViewModels;
using Athletics.ControlLibrary.DialogWindow;
using Athletics.Entities.Models;
using Athletics.MeetSetup.Controllers;
using Athletics.MeetSetup.ViewModels;
using Athletics.MeetSetup.ViewModels.SeedPreferences;
using Athletics.MeetSetup.Views;
using Athletics.MeetSetup.Views.SeedPreferences;
using Athletics.ModuleCommon;
using Athletics.Report.ViewModel;
using Athletics.Report.Views.GenerateDataSet;
using Athletics.Report.Views.ReportGenerator;
using Athletics.Report.Views.ViewReport;
using Athletics.SetupMeet.View.MeetSetup;
using AthleticsLauncher.Views;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AthleticsLauncher.ViewModels
{
    public class UserMenuViewModel : BindableBase, IUserMenuViewModel
    {
        private IApplicationAuthStateController AuthStateController;
        private IMeetSetupStateController MeetSetupController;
        private ObservableCollection<MenuItemViewModel> _MenuItems = new ObservableCollection<MenuItemViewModel>();
        ISignedInUserViewModel signInvm;
        List<ApplicationMenu> applicationMenuList = new List<ApplicationMenu>();
        private readonly IUnityContainer unityContainer;
        public ObservableCollection<MenuItemViewModel> MenuItems
        {
            get { return _MenuItems; }
            set { SetProperty(ref _MenuItems, value); }
        }

        private void AddMenuItem(MenuItemViewModel item, IApplicationAuthStateController authStateController, ISignedInUserViewModel signedInUserViewModel,
            IMeetSetupStateController meetSetupController)
        {
            List<ApplicationMenu> childMenu = applicationMenuList.Where(p => p.ParentID == item.MenuID).ToList();
            foreach (var itemChild in childMenu)
            {
                MenuItemViewModel child = new MenuItemViewModel(authStateController, signedInUserViewModel, meetSetupController, unityContainer) { Header = itemChild.MenuName, MenuID = itemChild.MenuID };
                item.MenuItems.Add(child);
                AddMenuItem(child, authStateController, signedInUserViewModel, meetSetupController);
            }

        }
        public UserMenuViewModel(IUnityContainer unity, IUserMenuView View, IApplicationAuthStateController authStateController, ISignedInUserViewModel signedInUserViewModel,
            ISignInManager signManager, IMeetSetupStateController meetSetupController)
        {
            Task.Run(async () =>
            {
                applicationMenuList = signManager.GetAllApplicationMenu().Result.ToList();
                PopulateMenu(authStateController, signedInUserViewModel, meetSetupController);

            });
            unityContainer = unity;
            View.DataContext = this;
            signInvm = signedInUserViewModel;
            AuthStateController = authStateController;
            MeetSetupController = meetSetupController;
            AuthStateController.AuthorizationStateChanged -= AuthStateController_AuthorizationStateChanged;
            AuthStateController.AuthorizationStateChanged += AuthStateController_AuthorizationStateChanged;
            MeetSetupController.MeetStateChanged -= MeetSetupController_MeetStateChanged;
            MeetSetupController.MeetStateChanged += MeetSetupController_MeetStateChanged;
            Events.OpenWindow += Events_OpenWindow;
        }
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender);
            if (window != null)
            {
                if (e.RightButton == MouseButtonState.Pressed)
                    window.DragMove();
            }
        }
        private void Events_OpenWindow(Windows window)
        {
            System.Windows.Window windowObj = null;
            IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();
            switch (window)
            {
                case Windows.Event:
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    windowObj = new ManageEventView();
                    windowObj.Name = "Events";
                    IGenericInteractionView<ManageEventViewModel> eventsViewModel = unityContainer.Resolve<IManageEventViewModel>() as IGenericInteractionView<ManageEventViewModel>;
                    if (eventsViewModel != null)
                    {
                        dialog.BindView(windowObj);
                        dialog.BindViewModel(eventsViewModel);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case Windows.Relay:
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    windowObj = new ManageRelayView();
                    windowObj.Name = "Relays";
                    IGenericInteractionView<ManageRelayViewModel> relayVM = unityContainer.Resolve<IManageRelayViewModel>() as IGenericInteractionView<ManageRelayViewModel>;
                    if (relayVM != null)
                    {
                        dialog.BindView(windowObj);
                        dialog.BindViewModel(relayVM);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case Windows.Athlete:
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    windowObj = new ManageAthletesView();
                    windowObj.Name = "Athletes";
                    IGenericInteractionView<ManageAthletesViewModel> athletsVM = unityContainer.Resolve<IManageAthletesViewModel>() as IGenericInteractionView<ManageAthletesViewModel>;
                    if (athletsVM != null)
                    {
                        dialog.BindView(windowObj);
                        dialog.BindViewModel(athletsVM);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case Windows.Team:
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    windowObj = new ManageTeamView();
                    windowObj.Name = "Teams";
                    IGenericInteractionView<ManageTeamViewModel> teamVM = unityContainer.Resolve<IManageTeamViewModel>() as IGenericInteractionView<ManageTeamViewModel>;
                    if (teamVM != null)
                    {
                        dialog.BindView(windowObj);
                        dialog.BindViewModel(teamVM);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case Windows.Seeding:
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    UserControl ucSeed = unityContainer.Resolve<ISeedPreferencesView>() as UserControl;
                    windowObj = new Window();
                    windowObj.Content = ucSeed;
                    windowObj.WindowStyle = WindowStyle.None;
                    windowObj.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    windowObj.Style = ucSeed.FindResource("DialogStyle") as Style;
                    windowObj.ResizeMode = ResizeMode.CanResizeWithGrip;
                    //windowObj.SizeToContent = SizeToContent.WidthAndHeight;
                    windowObj.Title = "Seeding Preferences";
                    windowObj.Name = "SeedPreferences";
                    windowObj.MouseLeftButtonDown += Window_MouseLeftButtonDown;
                    IGenericInteractionView<SeedPreferencesViewModel> seedprefrences = unityContainer.Resolve<ISeedPreferencesViewModel>() as IGenericInteractionView<SeedPreferencesViewModel>;
                    if (seedprefrences != null)
                    {
                        seedprefrences.GetEntity();
                        dialog.BindView(windowObj);
                        dialog.BindViewModel(seedprefrences);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                default:
                    break;
            }
        }

        private void PopulateMenu(IApplicationAuthStateController authStateController, ISignedInUserViewModel signedInUserViewModel, IMeetSetupStateController meetSetupController)
        {
            foreach (var item in applicationMenuList.Where(p => p.ParentID == null))
            {
                MenuItemViewModel menuitemVM = new MenuItemViewModel(authStateController, signedInUserViewModel, meetSetupController, unityContainer) { Header = item.MenuName, MenuID = item.MenuID };
                this.AddMenuItem(menuitemVM, authStateController, signedInUserViewModel, meetSetupController);
                MenuItems.Add(menuitemVM);
            }
        }

        private void MeetSetupController_MeetStateChanged(e_MEET_STATE obj)
        {
            switch (obj)
            {
                case e_MEET_STATE.MEET_SETUP:
                    Visible = true;
                    break;
                case e_MEET_STATE.ATHLETE_RELAY_PREFERENCES:
                    break;
                case e_MEET_STATE.SEEDING_PREFERENCES:
                    break;
                default:
                    break;
            }
        }

        private void AuthStateController_AuthorizationStateChanged(e_AUTH_STATE obj)
        {
            switch (obj)
            {
                case e_AUTH_STATE.SIGN_IN_DEFAULT:
                    this.Visible = false;
                    break;
                case e_AUTH_STATE.SIGN_UP:
                    break;
                case e_AUTH_STATE.FINISH_EXTERNAL_SIGN_UP:
                    this.Visible = false;
                    break;
                case e_AUTH_STATE.JOIN_EXISTING_ORGANIZATION:
                    this.Visible = false;
                    break;
                case e_AUTH_STATE.REGISTRATION_COMPLETE:
                    this.Visible = false;
                    break;
                case e_AUTH_STATE.CREATING_USER:
                    this.Visible = false;
                    break;
                case e_AUTH_STATE.WELCOME_NEW_USER:
                    this.FilterMenuBasedOnUser();
                    this.Visible = true;
                    break;
                case e_AUTH_STATE.UPLOADING_NEW_USER_INFO:
                    this.Visible = false;
                    break;
                case e_AUTH_STATE.LOGGING_IN:
                    this.Visible = true;
                    break;
                case e_AUTH_STATE.LOG_IN_FAILED:
                    this.Visible = false;
                    break;
                case e_AUTH_STATE.LOGGED_IN:
                    this.Visible = true;
                    break;
                case e_AUTH_STATE.NOT_LOGGED_IN:
                    this.Visible = false;
                    break;
                default:
                    break;
            }
        }

        private void FilterMenuBasedOnUser()
        {
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                User currentUser = ctx.Users.Where(p => p.EmailID == AuthStateController.CurrentUserName).FirstOrDefault();
                List<UserInRole> userInRoleList = ctx.UserInRoles.Where(p => p.UserGuid == currentUser.UserGuid).ToList();
                List<ApplicationMenu> MenuList = (from t1 in userInRoleList
                                                  join t2 in ctx.MenuInRoles on t1.RoleGuid equals t2.RoleID
                                                  join t3 in ctx.ApplicationMenus on t2.MenuID.Value equals t3.MenuID
                                                  select t3).ToList();

                List<ApplicationMenu> MissingMenuList = new List<ApplicationMenu>();
                foreach (var item in MenuList)
                {
                    this.GetAllParentMenu(MenuList, item, ref MissingMenuList);
                }
                MenuItems.Clear();
                applicationMenuList = MenuList.Concat(MissingMenuList).ToList();
                this.PopulateMenu(AuthStateController, signInvm, MeetSetupController);

            }
        }

        private void GetAllParentMenu(List<ApplicationMenu> ApplicableMenu, ApplicationMenu menu, ref List<ApplicationMenu> missingMenuList)
        {
            using (AthleticsDBEntities1 ctx = new AthleticsDBEntities1())
            {
                List<ApplicationMenu> applicationMenuList = ctx.ApplicationMenus.Where(p => p.MenuID == menu.ParentID).ToList();
                foreach (var item in applicationMenuList)
                {
                    if ((ApplicableMenu.Where(p => p.MenuID == item.MenuID).Count() <= 0) && (missingMenuList.Where(p => p.MenuID == item.MenuID).Count() <= 0))
                    {
                        missingMenuList.Add(item);
                        GetAllParentMenu(ApplicableMenu, item, ref missingMenuList);
                    }
                }

            }
        }

        private bool _Visible;
        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                SetProperty(ref _Visible, value);
            }
        }
    }

    public class MenuItemViewModel
    {
        private readonly ICommand _command;
        ISignedInUserViewModel signedInVM;
        IApplicationAuthStateController authController;
        private IMeetSetupStateController MeetSetupController;
        private IUnityContainer unityContainer;
        System.Windows.Window window = null;
        public MenuItemViewModel(IApplicationAuthStateController authStateController, ISignedInUserViewModel signedInUserViewModel,
            IMeetSetupStateController meetSetupController, IUnityContainer unity)
        {
            _command = new CommandViewModel(Execute);
            signedInVM = signedInUserViewModel;
            authController = authStateController;
            MeetSetupController = meetSetupController;
            MenuItems = new ObservableCollection<MenuItemViewModel>();
            unityContainer = unity;
        }

        public string Header { get; set; }
        public int MenuID { get; set; }

        public ObservableCollection<MenuItemViewModel> MenuItems { get; set; }

        public ICommand Command
        {
            get
            {
                return _command;
            }
        }

        //public ICommand PART_CLOSE
        //{
        //    get
        //    {
        //        return _PART_CLOSE;
        //    }
        //}

        //private void PART_CLOSE_Click()
        //{

        //    //var window = (Window)((FrameworkElement)sender).TemplatedParent;
        //    window.Visibility = Visibility.Hidden;
        //}


        private void Execute()
        {
            MeetSetupController.MeetState = e_MEET_STATE.NONE;
            authController.AuthenticationState = e_AUTH_STATE.NONE;
            IModalDialog dialog = ServiceProvider.Instance.Get<IModalDialog>();

            switch (Header.Trim())
            {
                case "Create User":
                    signedInVM.ClearCache();
                    authController.AuthenticationState = e_AUTH_STATE.SIGN_UP;
                    break;

                case "Add User Role":
                    signedInVM.ClearCache();
                    authController.AuthenticationState = e_AUTH_STATE.CREATE_ROLE;
                    break;

                case "Assign Role":
                    signedInVM.ClearCache();
                    authController.AuthenticationState = e_AUTH_STATE.ASSIGN_ROLE;
                    break;
                case "Add Module":
                    signedInVM.ClearCache();
                    authController.AuthenticationState = e_AUTH_STATE.CREATE_MENU;
                    break;

                case "Assign Module":
                    signedInVM.ClearCache();
                    authController.AuthenticationState = e_AUTH_STATE.ASSIGN_MENU;
                    break;

                case "Meet Set-up":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    dialog.Close();
                    Window windowmeet = unityContainer.Resolve<IManageMeetSetupView>() as Window;
                    // Window windowmeet = new Window();
                    //windowmeet.Content = uc1;
                    //windowmeet.WindowStyle = WindowStyle.None;
                    //windowmeet.Name = "MeetSetup";
                    //windowmeet.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    //windowmeet.Style = Application.Current.FindResource("DialogStyle") as Style;
                    //windowmeet.ResizeMode = ResizeMode.NoResize;
                    //windowmeet.SizeToContent = SizeToContent.WidthAndHeight;
                    //windowmeet.MouseLeftButtonDown += Window_MouseLeftButtonDown;
                    //windowmeet.Title = "Manage Meet Setup";

                    IGenericInteractionView<ManageMeetSetupViewModel> MeetsetupViewModel = unityContainer.Resolve<IManageMeetSetupViewModel>() as IGenericInteractionView<ManageMeetSetupViewModel>;
                    if (MeetsetupViewModel != null)
                    {
                        // MeetsetupViewModel.GetEntity();
                        dialog.BindView(windowmeet);
                        dialog.BindViewModel(MeetsetupViewModel);
                    }
                    dialog.ShowDialog();
                    break;

                case "Athlete/Relay Preferences":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    dialog.Close();
                    UserControl uc = unityContainer.Resolve<IAthleteRelayPreferencesView>() as UserControl;
                    Window windowPef = new Window();
                    Style stylePRef = uc.FindResource("DialogStyle") as Style;
                    windowPef.Content = uc;
                    windowPef.WindowStyle = WindowStyle.None;
                    windowPef.Name = "AthletsWindow";
                    windowPef.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    windowPef.Style = stylePRef;
                    windowPef.ResizeMode = ResizeMode.NoResize;
                    //window.SizeToContent = SizeToContent.WidthAndHeight;
                    windowPef.MouseLeftButtonDown += Window_MouseLeftButtonDown;
                    windowPef.Title = "Athlete Relay Preferences";

                    IGenericInteractionView<AthleteRelayPreferencesViewModel> athleticrelayPref = unityContainer.Resolve<IAthleteRelayPreferencesViewModel>() as IGenericInteractionView<AthleteRelayPreferencesViewModel>;
                    if (athleticrelayPref != null)
                    {
                        athleticrelayPref.GetEntity();
                        dialog.BindView(windowPef);
                        dialog.BindViewModel(athleticrelayPref);
                    }
                    // MeetsetupViewModel.SetEntity()
                    dialog.ShowDialog();
                    break;

                case "Seeding Preferences":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    UserControl ucSeed = unityContainer.Resolve<ISeedPreferencesView>() as UserControl;
                    window = new Window();
                    Style style = ucSeed.FindResource("DialogStyle") as Style;
                    window.Content = ucSeed;
                    window.WindowStyle = WindowStyle.None;
                    window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    window.Style = style;
                    window.ResizeMode = ResizeMode.NoResize;
                    //window.SizeToContent = SizeToContent.WidthAndHeight;
                    window.Title = "Seeding Preferences";
                    window.Name = "SeedPreferences";
                    window.MouseLeftButtonDown += Window_MouseLeftButtonDown;


                    IGenericInteractionView<SeedPreferencesViewModel> seedprefrences = unityContainer.Resolve<ISeedPreferencesViewModel>() as IGenericInteractionView<SeedPreferencesViewModel>;
                    if (seedprefrences != null)
                    {
                        seedprefrences.GetEntity();
                        dialog.BindView(window);
                        dialog.BindViewModel(seedprefrences);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case "Design  Report Template":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = unityContainer.Resolve<ICreateReportView>() as Window;
                    IGenericInteractionView<CreateReportViewModel> reportviewModel = unityContainer.Resolve<ICreateReportViewModel>() as IGenericInteractionView<CreateReportViewModel>;
                    if (reportviewModel != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(reportviewModel);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                case "Assign DataSet To Report":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ReportGeneratorListView();
                    IGenericInteractionView<ReportGeneratorListViewModel> reportGeneration = unityContainer.Resolve<IReportGeneratorListViewModel>() as IGenericInteractionView<ReportGeneratorListViewModel>;
                    if (reportGeneration != null)
                    {
                        reportGeneration.GetEntity();
                        dialog.BindView(window);
                        dialog.BindViewModel(reportGeneration);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                case "View Report":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ReportViewList();
                    IGenericInteractionView<ReportListViewModel> reportView = unityContainer.Resolve<IReportListViewModel>() as IGenericInteractionView<ReportListViewModel>;
                    if (reportView != null)
                    {
                        reportView.GetEntity();
                        dialog.BindView(window);
                        dialog.BindViewModel(reportView);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                case "Design Report Data Set":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new DataSetView();
                    IGenericInteractionView<DataSetViewModel> designDataSetView = unityContainer.Resolve<IDataSetViewModel>() as IGenericInteractionView<DataSetViewModel>;
                    if (designDataSetView != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(designDataSetView);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case "Entry Scoring Preferences":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new EntryScoringPreferencesListView();
                    IGenericInteractionView<EntryScoringPreferencesListViewModel> genericInteractionViewModel = unityContainer.Resolve<IEntryScoringPreferencesListViewModel>() as IGenericInteractionView<EntryScoringPreferencesListViewModel>;
                    if (genericInteractionViewModel != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(genericInteractionViewModel);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                case "Entry/Scoring Preferences":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ScoringSetupListView();
                    IGenericInteractionView<ScoringSetupListViewModel> scoringSetupViewModel = unityContainer.Resolve<IScoringSetupListViewModel>() as IGenericInteractionView<ScoringSetupListViewModel>;
                    if (scoringSetupViewModel != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(scoringSetupViewModel);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case "Division/Region Names":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new DivisionRegionNamesView();
                    window.Name = "DivisionRegionNamesView";
                    IGenericInteractionView<DivisionRegionNamesViewModel> divisionRegionNameViewModel = unityContainer.Resolve<IDivisionRegionNamesViewModel>() as IGenericInteractionView<DivisionRegionNamesViewModel>;
                    if (divisionRegionNameViewModel != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(divisionRegionNameViewModel);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                case "Events":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ManageEventView();
                    window.Name = "Events";
                    IGenericInteractionView<ManageEventViewModel> eventsViewModel = unityContainer.Resolve<IManageEventViewModel>() as IGenericInteractionView<ManageEventViewModel>;
                    if (eventsViewModel != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(eventsViewModel);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                case "Timer Mark Standard":
                    window = new AddTimeMarkStandardTagView();
                    window.Name = "AddTimeMarkStandardView";
                    window.ShowDialog();
                    window.Close();
                    dialog = null;
                    break;

                case "Athletes":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ManageAthletesView();
                    window.Name = "Athletes";
                    IGenericInteractionView<ManageAthletesViewModel> athletsVM = unityContainer.Resolve<IManageAthletesViewModel>() as IGenericInteractionView<ManageAthletesViewModel>;
                    if (athletsVM != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(athletsVM);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                //Window windowAthletic = unityContainer.Resolve<IManageAthletesView>() as Window;
                //windowAthletic.Name = "Athletes";
                //windowAthletic.Show();
                ////windowAthletic.Close();

                case "Schools":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ManageTeamView();
                    window.Name = "Teams";
                    IGenericInteractionView<ManageTeamViewModel> teamVM = unityContainer.Resolve<IManageTeamViewModel>() as IGenericInteractionView<ManageTeamViewModel>;
                    if (teamVM != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(teamVM);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                case "Relays":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ManageRelayView();
                    window.Name = "Relays";
                    IGenericInteractionView<ManageRelayViewModel> relayVM = unityContainer.Resolve<IManageRelayViewModel>() as IGenericInteractionView<ManageRelayViewModel>;
                    if (relayVM != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(relayVM);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;

                case "Run":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ManageRunView();
                    window.Name = "RunMeet";
                    IGenericInteractionView<ManageRunViewModel> runVM = unityContainer.Resolve<IManageRunViewModel>() as IGenericInteractionView<ManageRunViewModel>;
                    if (runVM != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(runVM);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case "Seeding":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ManageSeedingView();
                    window.Name = "Seeding";
                    IGenericInteractionView<ManageSeedingViewModel> seedingVm = unityContainer.Resolve<IManageSeedingViewModel>() as IGenericInteractionView<ManageSeedingViewModel>;
                    if (seedingVm != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(seedingVm);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case "Export":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new ExportDataView();
                    window.Name = "Export";
                    IGenericInteractionView<ExportDataViewModel> exportVm = unityContainer.Resolve<IExportDataViewModel>() as IGenericInteractionView<ExportDataViewModel>;
                    if (exportVm != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(exportVm);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                case "AddResult":
                    dialog = ServiceProvider.Instance.Get<IModalDialog>();
                    window = new AddAthleteResultView();
                    window.Name = "AddAthleteResult";
                    IGenericInteractionView<AddAthleteResultViewModel> resultVm = unityContainer.Resolve<IAddAthleteResultViewModel>() as IGenericInteractionView<AddAthleteResultViewModel>;
                    if (resultVm != null)
                    {
                        dialog.BindView(window);
                        dialog.BindViewModel(resultVm);
                    }
                    dialog.ShowDialog();
                    dialog.Close();
                    dialog = null;
                    break;
                default:
                    break;
            }
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var window = (Window)((FrameworkElement)sender);
            if (window != null)
            {
                if (e.RightButton == MouseButtonState.Pressed)
                    window.DragMove();
            }
        }
    }

    public class CommandViewModel : ICommand
    {
        private readonly Action _action;

        public CommandViewModel(Action action)
        {
            _action = action;
        }

        public void Execute(object o)
        {
            _action();
        }

        public bool CanExecute(object o)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { }
            remove { }
        }
    }
}

