﻿using Athletics.Client.Controllers;
using Athletics.MeetSetup.Controllers;
using Athletics.Report.Controllers;
using AthleticsLauncher.Views;
using Microsoft.Practices.Prism.Mvvm;

namespace AthleticsLauncher.ViewModels
{
    public class AthleticsLauncherShellViewModel : BindableBase, IAthleticsLauncherShellViewModel
    {
        #region Private Fields
        private IApplicationAuthStateController _AuthStateController;
        private IMeetSetupStateController _MeetStateController;
        private IReportStateController _IReportStateController;
        private IAthleticsLauncherShellView View;
        #endregion

        #region Constructor
        public AthleticsLauncherShellViewModel(IMeetSetupStateController MeetStateController, IApplicationAuthStateController AuthStateController, IAthleticsLauncherShellView view,
            IReportStateController reportController)
        {
            _AuthStateController = AuthStateController;
            _MeetStateController = MeetStateController;
            _IReportStateController = reportController;
            _AuthStateController.AuthorizationStateChanged += _AuthStateController_AuthorizationStateChanged;
            _MeetStateController.MeetStateChanged += _MeetStateController_MeetStateChanged;
            _IReportStateController.Report_STATEChanged += _IReportStateController_Report_STATEChanged;
            View = view;
            View.DataContext = this;
        }

        private void _IReportStateController_Report_STATEChanged(e_Report_STATE obj)
        {
            SetDisableAll();
            if (obj != e_Report_STATE.NONE)
                MeetSetupShellViewVisible = true;
        }

        #endregion

        #region Methods
        private void _MeetStateController_MeetStateChanged(e_MEET_STATE obj)
        {
            SetDisableAll();
            if (obj != e_MEET_STATE.NONE)
                MeetSetupShellViewVisible = true;

        }

        private void _AuthStateController_AuthorizationStateChanged(e_AUTH_STATE obj)
        {
            SetDisableAll();
            AuthenticatedShellViewVisible = true;

        }

        private void SetDisableAll()
        {
            AuthenticatedShellViewVisible = false;
            MeetSetupShellViewVisible = false;
            ReportGeneratorRegionVisible = false;


        }
        #endregion

        #region Properties
        private bool _AuthenticatedShellViewVisible = true;
        public bool AuthenticatedShellViewVisible
        {
            get
            {
                return _AuthenticatedShellViewVisible;
            }
            protected set { SetProperty(ref _AuthenticatedShellViewVisible, value); }
        }

        private bool _MeetSetupShellViewVisible = false;
        public bool MeetSetupShellViewVisible
        {
            get
            {
                return _MeetSetupShellViewVisible;
            }
            protected set { SetProperty(ref _MeetSetupShellViewVisible, value); }
        }

        private bool _ReportGeneratorRegionVisible = false;
        public bool ReportGeneratorRegionVisible
        {
            get
            {
                return _ReportGeneratorRegionVisible;
            }
            protected set { SetProperty(ref _ReportGeneratorRegionVisible, value); }
        }
        #endregion
    }
}
