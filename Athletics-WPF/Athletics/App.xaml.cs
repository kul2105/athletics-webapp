﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace AthleticsLauncher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static AthleticsLauncherBootstrapper bootstrapper;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Application.Current.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(AppDispatcherUnhandledException);
            bootstrapper = new AthleticsLauncherBootstrapper();
            bootstrapper.Run();

            this.ShutdownMode = ShutdownMode.OnMainWindowClose;
        }

        private void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Application.Current.Resources["RepetitiveIndicationColor"] = new SolidColorBrush(Colors.Red);
            Application.Current.Resources["RepetitiveIndicationMessage"] = e.Exception.Message;
            e.Handled = true;
        }

        private void BtnClick_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnClick_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Button btn = sender as Button;
            if(btn!=null)
            {
                btn.IsEnabled = true;
            }
        }
    }
}
