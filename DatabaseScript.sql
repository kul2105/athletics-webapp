USE [AthleticsDB]
GO
/****** Object:  Table [dbo].[ApplicationMenu]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationMenu](
	[MenuID] [int] NOT NULL,
	[MenuName] [nvarchar](50) NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [PK_ApplicationMenus] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AthletePreferences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AthletePreferences](
	[AthletePreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[AthletePreferencesName] [nvarchar](250) NULL,
 CONSTRAINT [PK_AthletePreferences] PRIMARY KEY CLUSTERED 
(
	[AthletePreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AthleteRelayPreferencesTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AthleteRelayPreferencesTransaction](
	[AthleteRelayPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[PreferencesTransactionGroupID] [nvarchar](250) NULL,
	[CompetitorNumberTransactionGroupID] [nvarchar](250) NULL,
	[RelayPreferencesTransactionGroupID] [nvarchar](250) NULL,
 CONSTRAINT [PK_AthleteRelayPreferencesTransaction_1] PRIMARY KEY CLUSTERED 
(
	[AthleteRelayPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AthleticPreferencesTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AthleticPreferencesTransaction](
	[AthleticPreferencesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[AthleticPreferencesGroupID] [nvarchar](250) NOT NULL,
	[AthleticPreferencesID] [int] NULL,
 CONSTRAINT [PK_AthleticPreferencesTransaction_1] PRIMARY KEY CLUSTERED 
(
	[AthleticPreferencesTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BaseCounty]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BaseCounty](
	[BaseCountyID] [int] IDENTITY(1,1) NOT NULL,
	[BaseCountyName] [nvarchar](250) NULL,
 CONSTRAINT [PK_BaseCounties] PRIMARY KEY CLUSTERED 
(
	[BaseCountyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompetitorNumbers]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompetitorNumbers](
	[CompetitorNumberID] [int] IDENTITY(1,1) NOT NULL,
	[CompetitorNumbersName] [nvarchar](250) NULL,
	[IsSingleSelectionGroup] [bit] NOT NULL,
 CONSTRAINT [PK_CompetitorNumbers] PRIMARY KEY CLUSTERED 
(
	[CompetitorNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompetitorNumbersTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompetitorNumbersTransaction](
	[CompetitorNumbersTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[CompetitorNumbersGroupID] [nvarchar](250) NOT NULL,
	[CompetitorNumbersID] [int] NULL,
 CONSTRAINT [PK_CompetitorNumbersTransaction_1] PRIMARY KEY CLUSTERED 
(
	[CompetitorNumbersTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DualMeetAssignmentTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DualMeetAssignmentTransaction](
	[DualMeetAssignmentTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[StrictAssignmentAllHeats] [bit] NULL,
	[StrictAssignmentFastestHeatOnly] [bit] NULL,
	[UseLaneAssignmentsForInLaneRacesOnly] [bit] NULL,
	[UserLaneOrPositionAssignmentsAbove] [bit] NULL,
	[AlternamtUserOfUnAssignedLane] [bit] NULL,
	[DualMeetGroupID] [nvarchar](250) NULL,
 CONSTRAINT [PK_DualMeetAssignmentTransaction] PRIMARY KEY CLUSTERED 
(
	[DualMeetAssignmentTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DualMeetTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DualMeetTransaction](
	[DualMeetTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[DualMeetGroupID] [nvarchar](250) NULL,
	[LaneNumber] [int] NULL,
	[SchoolID] [int] NULL,
 CONSTRAINT [PK_DualMeetTransaction] PRIMARY KEY CLUSTERED 
(
	[DualMeetTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LaneDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LaneDetail](
	[LaneID] [int] IDENTITY(1,1) NOT NULL,
	[LaneNumber] [int] NULL,
	[SchoolID] [int] NULL,
 CONSTRAINT [PK_LaneDetails] PRIMARY KEY CLUSTERED 
(
	[LaneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetArena]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetArena](
	[MeetArenaID] [int] IDENTITY(1,1) NOT NULL,
	[MeetArenaName] [nvarchar](250) NULL,
 CONSTRAINT [PK_MeetArenas] PRIMARY KEY CLUSTERED 
(
	[MeetArenaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetClass]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetClass](
	[MeetClassID] [int] IDENTITY(1,1) NOT NULL,
	[MeetClassName] [nvarchar](250) NULL,
 CONSTRAINT [PK_MeetClasses] PRIMARY KEY CLUSTERED 
(
	[MeetClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetKind]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetKind](
	[MeetKindID] [int] IDENTITY(1,1) NOT NULL,
	[MeetKindName] [nvarchar](50) NULL,
 CONSTRAINT [PK_MeetKinds] PRIMARY KEY CLUSTERED 
(
	[MeetKindID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetup]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetup](
	[MeetSetupID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupName] [nvarchar](250) NULL,
	[MeetSetupName2] [nvarchar](250) NULL,
	[MeetSetupStartDate] [date] NULL,
	[MeetSetupEndDate] [date] NULL,
	[MeetSetupAgeUpDate] [date] NULL,
	[MeetKindID] [int] NULL,
	[MeetClassID] [int] NULL,
	[MeetTypeID] [int] NULL,
	[BaseCountyID] [int] NULL,
	[MeetArenaID] [int] NULL,
	[MeetStyleID] [int] NULL,
	[UseDivisionBirthdateRange] [bit] NULL,
	[LinkTermToDivision] [bit] NULL,
	[MeetTypeDivisionID] [int] NULL,
 CONSTRAINT [PK_MeetSetup] PRIMARY KEY CLUSTERED 
(
	[MeetSetupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupAthletePreferences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupAthletePreferences](
	[MeetSetupAthletePreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[AthletePreferencesID] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupAthletePreferences] PRIMARY KEY CLUSTERED 
(
	[MeetSetupAthletePreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupCompetitorNumber]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupCompetitorNumber](
	[MeetCompetitorNumberID] [int] IDENTITY(1,1) NOT NULL,
	[CompetitorNumberID] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetCompetitorNumber] PRIMARY KEY CLUSTERED 
(
	[MeetCompetitorNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupDualMeet]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupDualMeet](
	[MeetSetupDualMeetID] [int] IDENTITY(1,1) NOT NULL,
	[LaneNumber] [int] NULL,
	[SchoolID] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupDualMeet] PRIMARY KEY CLUSTERED 
(
	[MeetSetupDualMeetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupDualMeetAssignment]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupDualMeetAssignment](
	[MeetSetupDualMeetAssignmentID] [int] IDENTITY(1,1) NOT NULL,
	[StrictAssignmentAllHeats] [bit] NULL,
	[StrictAssignmentFastestHeatOnly] [bit] NULL,
	[UseLaneAssignmentsForInLaneRacesOnly] [bit] NULL,
	[UserLaneOrPositionAssignmentsAbove] [bit] NULL,
	[AlternamtUserOfUnAssignedLane] [bit] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupDualMeetAssignment] PRIMARY KEY CLUSTERED 
(
	[MeetSetupDualMeetAssignmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupRelayPreferences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupRelayPreferences](
	[MeetSetupRelayPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[RelayPreferencesID] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupRelayPreferences] PRIMARY KEY CLUSTERED 
(
	[MeetSetupRelayPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupRendomizationRule]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupRendomizationRule](
	[MeetSetupRendomizationRuleID] [int] IDENTITY(1,1) NOT NULL,
	[TimedFinalEvents] [nvarchar](50) NULL,
	[RoundFirstMultipleRound] [nvarchar](50) NULL,
	[RoundSecondThirdAndForth] [nvarchar](50) NULL,
	[CloseGap] [bit] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupRendomizationRule] PRIMARY KEY CLUSTERED 
(
	[MeetSetupRendomizationRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupSeedingRules]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupSeedingRules](
	[MeetSetupSeedingRulesID] [int] IDENTITY(1,1) NOT NULL,
	[AllowForeignAthletesInFinal] [bit] NULL,
	[AllowExhibitionAthletesInFinal] [bit] NULL,
	[SeedExhibitionAthletesLast] [bit] NULL,
	[ApplyNCAARule] [bit] NULL,
	[UseSpecialRandomSelectMethod] [bit] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupSeedingRules] PRIMARY KEY CLUSTERED 
(
	[MeetSetupSeedingRulesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupStandardLanePrefrences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupStandardLanePrefrences](
	[MeetSetupStandardLanePrefrencesID] [int] IDENTITY(1,1) NOT NULL,
	[RowZeroColumnFirstValue] [smallint] NULL,
	[RowZeroColumnSecondValue] [smallint] NULL,
	[RowZeroColumnThirdValue] [smallint] NULL,
	[RowZeroColumnForthValue] [smallint] NULL,
	[RowZeroColumnFifthValue] [smallint] NULL,
	[RowZeroColumnSixthValue] [smallint] NULL,
	[RowZeroColumnSeventhValue] [smallint] NULL,
	[RowZeroColumnEightthValue] [smallint] NULL,
	[RowZeroColumnNinthValue] [smallint] NULL,
	[RowZeroColumnTenthValue] [smallint] NULL,
	[RowOneColumnFirstValue] [smallint] NULL,
	[RowOneColumnSecondValue] [smallint] NULL,
	[RowOneColumnThirdValue] [smallint] NULL,
	[RowOneColumnForthValue] [smallint] NULL,
	[RowOneColumnFifthValue] [smallint] NULL,
	[RowOneColumnSixthValue] [smallint] NULL,
	[RowOneColumnSeventhValue] [smallint] NULL,
	[RowOneColumnEightthValue] [smallint] NULL,
	[RowOneColumnNinthValue] [smallint] NULL,
	[RowOneColumnTenthValue] [smallint] NULL,
	[RowSecondColumnFirstValue] [smallint] NULL,
	[RowSecondColumnSecondValue] [smallint] NULL,
	[RowSecondColumnThirdValue] [smallint] NULL,
	[RowSecondColumnForthValue] [smallint] NULL,
	[RowSecondColumnFifthValue] [smallint] NULL,
	[RowSecondColumnSixthValue] [smallint] NULL,
	[RowSecondColumnSeventhValue] [smallint] NULL,
	[RowSecondColumnEightthValue] [smallint] NULL,
	[RowSecondColumnNinthValue] [smallint] NULL,
	[RowSecondColumnTenthValue] [smallint] NULL,
	[RowThirdColumnFirstValue] [smallint] NULL,
	[RowThirdColumnSecondValue] [smallint] NULL,
	[RowThirdColumnThirdValue] [smallint] NULL,
	[RowThirdColumnForthValue] [smallint] NULL,
	[RowThirdColumnFifthValue] [smallint] NULL,
	[RowThirdColumnSixthValue] [smallint] NULL,
	[RowThirdColumnSeventhValue] [smallint] NULL,
	[RowThirdColumnEightthValue] [smallint] NULL,
	[RowThirdColumnNinthValue] [smallint] NULL,
	[RowThridColumnTenthValue] [smallint] NULL,
	[RowForthColumnFirstValue] [smallint] NULL,
	[RowForthColumnSecondValue] [smallint] NULL,
	[RowForthColumnThirdValue] [smallint] NULL,
	[RowForthColumnForthValue] [smallint] NULL,
	[RowForthColumnFifthValue] [smallint] NULL,
	[RowForthColumnSixthValue] [smallint] NULL,
	[RowForthColumnSeventhValue] [smallint] NULL,
	[RowForthColumnEightthValue] [smallint] NULL,
	[RowForthColumnNinthValue] [smallint] NULL,
	[RowForthColumnTenthValue] [smallint] NULL,
	[RowFifthColumnFirstValue] [smallint] NULL,
	[RowFifthColumnSecondValue] [smallint] NULL,
	[RowFifthColumnThirdValue] [smallint] NULL,
	[RowFifthColumnForthValue] [smallint] NULL,
	[RowFifthColumnFifthValue] [smallint] NULL,
	[RowFifthColumnSixthValue] [smallint] NULL,
	[RowFifthColumnSeventhValue] [smallint] NULL,
	[RowFifthColumnEightthValue] [smallint] NULL,
	[RowFifthColumnNinthValue] [smallint] NULL,
	[RowFifthColumnTenthValue] [smallint] NULL,
	[RowSixthColumnFirstValue] [smallint] NULL,
	[RowSixthColumnSecondValue] [smallint] NULL,
	[RowSixthColumnThirdValue] [smallint] NULL,
	[RowSixthColumnForthValue] [smallint] NULL,
	[RowSixthColumnFifthValue] [smallint] NULL,
	[RowSixthColumnSixthValue] [smallint] NULL,
	[RowSixthColumnSeventhValue] [smallint] NULL,
	[RowSixthColumnEightthValue] [smallint] NULL,
	[RowSixthColumnNinthValue] [smallint] NULL,
	[RowSixthColumnTenthValue] [smallint] NULL,
	[RowSeventhColumnFirstValue] [smallint] NULL,
	[RowSeventhColumnSecondValue] [smallint] NULL,
	[RowSeventhColumnThirdValue] [smallint] NULL,
	[RowSeventhColumnForthValue] [smallint] NULL,
	[RowSeventhColumnFifthValue] [smallint] NULL,
	[RowSeventhColumnSixthValue] [smallint] NULL,
	[RowSeventhColumnSeventhValue] [smallint] NULL,
	[RowSeventhColumnEightthValue] [smallint] NULL,
	[RowSeventhColumnNinthValue] [smallint] NULL,
	[RowSeventhColumnTenthValue] [smallint] NULL,
	[RowEighthColumnFirstValue] [smallint] NULL,
	[RowEighthColumnSecondValue] [smallint] NULL,
	[RowEighthColumnThirdValue] [smallint] NULL,
	[RowEighthColumnForthValue] [smallint] NULL,
	[RowEighthColumnFifthValue] [smallint] NULL,
	[RowEighthColumnSixthValue] [smallint] NULL,
	[RowEighthColumnSeventhValue] [smallint] NULL,
	[RowEighthColumnEightthValue] [smallint] NULL,
	[RowEighthColumnNinthValue] [smallint] NULL,
	[RowEighthColumnTenthValue] [smallint] NULL,
	[RowNineColumnFirstValue] [smallint] NULL,
	[RowNineColumnSecondValue] [smallint] NULL,
	[RowNineColumnThirdValue] [smallint] NULL,
	[RowNineColumnForthValue] [smallint] NULL,
	[RowNineColumnFifthValue] [smallint] NULL,
	[RowNineColumnSixthValue] [smallint] NULL,
	[RowNineColumnSeventhValue] [smallint] NULL,
	[RowNineColumnEightthValue] [smallint] NULL,
	[RowNineColumnNinthValue] [smallint] NULL,
	[RowNineColumnTenthValue] [smallint] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupStandardLanePrefrences] PRIMARY KEY CLUSTERED 
(
	[MeetSetupStandardLanePrefrencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupWaterfallStart]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupWaterfallStart](
	[MeetSetupWaterfallStartPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[Position] [int] NULL,
	[Rank] [int] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetSetupWaterfallStart] PRIMARY KEY CLUSTERED 
(
	[MeetSetupWaterfallStartPreferenceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetStandardAlleyPrefrences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetStandardAlleyPrefrences](
	[MeetStandardAlleyPrefrencesID] [int] IDENTITY(1,1) NOT NULL,
	[RowZeroColumnFirstValue] [smallint] NULL,
	[RowZeroColumnSecondValue] [smallint] NULL,
	[RowZeroColumnThirdValue] [smallint] NULL,
	[RowZeroColumnForthValue] [smallint] NULL,
	[RowZeroColumnFifthValue] [smallint] NULL,
	[RowZeroColumnSixthValue] [smallint] NULL,
	[RowZeroColumnSeventhValue] [smallint] NULL,
	[RowZeroColumnEightthValue] [smallint] NULL,
	[RowZeroColumnNinthValue] [smallint] NULL,
	[RowZeroColumnTenthValue] [smallint] NULL,
	[RowOneColumnFirstValue] [smallint] NULL,
	[RowOneColumnSecondValue] [smallint] NULL,
	[RowOneColumnThirdValue] [smallint] NULL,
	[RowOneColumnForthValue] [smallint] NULL,
	[RowOneColumnFifthValue] [smallint] NULL,
	[RowOneColumnSixthValue] [smallint] NULL,
	[RowOneColumnSeventhValue] [smallint] NULL,
	[RowOneColumnEightthValue] [smallint] NULL,
	[RowOneColumnNinthValue] [smallint] NULL,
	[RowOneColumnTenthValue] [smallint] NULL,
	[RowSecondColumnFirstValue] [smallint] NULL,
	[RowSecondColumnSecondValue] [smallint] NULL,
	[RowSecondColumnThirdValue] [smallint] NULL,
	[RowSecondColumnForthValue] [smallint] NULL,
	[RowSecondColumnFifthValue] [smallint] NULL,
	[RowSecondColumnSixthValue] [smallint] NULL,
	[RowSecondColumnSeventhValue] [smallint] NULL,
	[RowSecondColumnEightthValue] [smallint] NULL,
	[RowSecondColumnNinthValue] [smallint] NULL,
	[RowSecondColumnTenthValue] [smallint] NULL,
	[RowThirdColumnFirstValue] [smallint] NULL,
	[RowThirdColumnSecondValue] [smallint] NULL,
	[RowThirdColumnThirdValue] [smallint] NULL,
	[RowThirdColumnForthValue] [smallint] NULL,
	[RowThirdColumnFifthValue] [smallint] NULL,
	[RowThirdColumnSixthValue] [smallint] NULL,
	[RowThirdColumnSeventhValue] [smallint] NULL,
	[RowThirdColumnEightthValue] [smallint] NULL,
	[RowThirdColumnNinthValue] [smallint] NULL,
	[RowThridColumnTenthValue] [smallint] NULL,
	[RowForthColumnFirstValue] [smallint] NULL,
	[RowForthColumnSecondValue] [smallint] NULL,
	[RowForthColumnThirdValue] [smallint] NULL,
	[RowForthColumnForthValue] [smallint] NULL,
	[RowForthColumnFifthValue] [smallint] NULL,
	[RowForthColumnSixthValue] [smallint] NULL,
	[RowForthColumnSeventhValue] [smallint] NULL,
	[RowForthColumnEightthValue] [smallint] NULL,
	[RowForthColumnNinthValue] [smallint] NULL,
	[RowForthColumnTenthValue] [smallint] NULL,
	[RowFifthColumnFirstValue] [smallint] NULL,
	[RowFifthColumnSecondValue] [smallint] NULL,
	[RowFifthColumnThirdValue] [smallint] NULL,
	[RowFifthColumnForthValue] [smallint] NULL,
	[RowFifthColumnFifthValue] [smallint] NULL,
	[RowFifthColumnSixthValue] [smallint] NULL,
	[RowFifthColumnSeventhValue] [smallint] NULL,
	[RowFifthColumnEightthValue] [smallint] NULL,
	[RowFifthColumnNinthValue] [smallint] NULL,
	[RowFifthColumnTenthValue] [smallint] NULL,
	[RowSixthColumnFirstValue] [smallint] NULL,
	[RowSixthColumnSecondValue] [smallint] NULL,
	[RowSixthColumnThirdValue] [smallint] NULL,
	[RowSixthColumnForthValue] [smallint] NULL,
	[RowSixthColumnFifthValue] [smallint] NULL,
	[RowSixthColumnSixthValue] [smallint] NULL,
	[RowSixthColumnSeventhValue] [smallint] NULL,
	[RowSixthColumnEightthValue] [smallint] NULL,
	[RowSixthColumnNinthValue] [smallint] NULL,
	[RowSixthColumnTenthValue] [smallint] NULL,
	[RowSeventhColumnFirstValue] [smallint] NULL,
	[RowSeventhColumnSecondValue] [smallint] NULL,
	[RowSeventhColumnThirdValue] [smallint] NULL,
	[RowSeventhColumnForthValue] [smallint] NULL,
	[RowSeventhColumnFifthValue] [smallint] NULL,
	[RowSeventhColumnSixthValue] [smallint] NULL,
	[RowSeventhColumnSeventhValue] [smallint] NULL,
	[RowSeventhColumnEightthValue] [smallint] NULL,
	[RowSeventhColumnNinthValue] [smallint] NULL,
	[RowSeventhColumnTenthValue] [smallint] NULL,
	[RowEighthColumnFirstValue] [smallint] NULL,
	[RowEighthColumnSecondValue] [smallint] NULL,
	[RowEighthColumnThirdValue] [smallint] NULL,
	[RowEighthColumnForthValue] [smallint] NULL,
	[RowEighthColumnFifthValue] [smallint] NULL,
	[RowEighthColumnSixthValue] [smallint] NULL,
	[RowEighthColumnSeventhValue] [smallint] NULL,
	[RowEighthColumnEightthValue] [smallint] NULL,
	[RowEighthColumnNinthValue] [smallint] NULL,
	[RowEighthColumnTenthValue] [smallint] NULL,
	[RowNineColumnFirstValue] [smallint] NULL,
	[RowNineColumnSecondValue] [smallint] NULL,
	[RowNineColumnThirdValue] [smallint] NULL,
	[RowNineColumnForthValue] [smallint] NULL,
	[RowNineColumnFifthValue] [smallint] NULL,
	[RowNineColumnSixthValue] [smallint] NULL,
	[RowNineColumnSeventhValue] [smallint] NULL,
	[RowNineColumnEightthValue] [smallint] NULL,
	[RowNineColumnNinthValue] [smallint] NULL,
	[RowNineColumnTenthValue] [smallint] NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_MeetStandardAlleyPrefrences] PRIMARY KEY CLUSTERED 
(
	[MeetStandardAlleyPrefrencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetStyle]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetStyle](
	[MeetStyleID] [int] IDENTITY(1,1) NOT NULL,
	[MeetStyleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_MeetStyles] PRIMARY KEY CLUSTERED 
(
	[MeetStyleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetType]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetType](
	[MeetTypeID] [int] IDENTITY(1,1) NOT NULL,
	[MeetTypeName] [nvarchar](150) NULL,
 CONSTRAINT [PK_MeetTypes] PRIMARY KEY CLUSTERED 
(
	[MeetTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetTypeDivision]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetTypeDivision](
	[MeetTypeDivisionID] [int] IDENTITY(1,1) NOT NULL,
	[MeetTypeDivisionName] [nvarchar](250) NULL,
	[MeetTypeID] [int] NULL,
 CONSTRAINT [PK_MeetTypeDivisions] PRIMARY KEY CLUSTERED 
(
	[MeetTypeDivisionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MenuInRole]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuInRole](
	[MenuInRoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [nvarchar](50) NULL,
	[MenuID] [int] NULL,
 CONSTRAINT [PK_MenuInRoles] PRIMARY KEY CLUSTERED 
(
	[MenuInRoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RelayPreferences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelayPreferences](
	[RelayPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[RelayPreferencesName] [nvarchar](250) NULL,
 CONSTRAINT [PK_RelayPreferences] PRIMARY KEY CLUSTERED 
(
	[RelayPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RelayPreferencesTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelayPreferencesTransaction](
	[RelayPreferencesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[RelayPreferencesGroupID] [nvarchar](250) NOT NULL,
	[RelayPreferencesID] [int] NULL,
 CONSTRAINT [PK_RelayPreferencesTransaction] PRIMARY KEY CLUSTERED 
(
	[RelayPreferencesTransactionID] ASC,
	[RelayPreferencesGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RendomizationRuleTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RendomizationRuleTransaction](
	[RendomizationRuleTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[RendomizationRuleGroupID] [nvarchar](250) NULL,
	[TimedFinalEvents] [nvarchar](50) NULL,
	[RoundFirstMultipleRound] [nvarchar](50) NULL,
	[RoundSecondThirdAndForth] [nvarchar](50) NULL,
	[CloseGap] [bit] NULL,
 CONSTRAINT [PK_RendomizationRuleTransaction] PRIMARY KEY CLUSTERED 
(
	[RendomizationRuleTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportColumnInDataSetTable]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportColumnInDataSetTable](
	[ReportColumnInDataSetTableID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDataSetTableID] [int] NULL,
	[ReportDataSetColumnID] [int] NULL,
 CONSTRAINT [PK_ReportColumnInDataSetTable] PRIMARY KEY CLUSTERED 
(
	[ReportColumnInDataSetTableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportContentPropertyDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportContentPropertyDetail](
	[ReportContentPropertyID] [int] IDENTITY(1,1) NOT NULL,
	[ReportContentPropertyName] [nvarchar](250) NULL,
	[ReportContentPropertyValue] [nvarchar](max) NULL,
	[ReportGeneratorContentID] [int] NULL,
	[ReportContentPropertyDescription] [nvarchar](350) NULL,
 CONSTRAINT [PK_ReportContentPropertyDetail] PRIMARY KEY CLUSTERED 
(
	[ReportContentPropertyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDataSetInfo]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDataSetInfo](
	[ReportDataSetID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDataSetName] [nvarchar](350) NULL,
	[ReportDataSetDescription] [nvarchar](550) NULL,
	[ReportDataSetQuery] [nvarchar](max) NULL,
 CONSTRAINT [PK_ReportDataSetInfo] PRIMARY KEY CLUSTERED 
(
	[ReportDataSetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDataSetInTable]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDataSetInTable](
	[TableInReportDataSetID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDataSetTableID] [int] NULL,
	[ReportDataSetID] [int] NULL,
 CONSTRAINT [PK_ReportDataSetInTable] PRIMARY KEY CLUSTERED 
(
	[TableInReportDataSetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDataSetMappedDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDataSetMappedDetail](
	[ReportDataSetMappedDetailID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDataSetMasterID] [int] NULL,
	[ReportColumnName] [nvarchar](550) NULL,
	[DataSetColumnName] [nvarchar](550) NULL,
 CONSTRAINT [PK_ReportDataSetMappedDetail] PRIMARY KEY CLUSTERED 
(
	[ReportDataSetMappedDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDataSetMappedMaster]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDataSetMappedMaster](
	[ReportDataSetMappedID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDataSetTableID] [int] NULL,
	[ReportID] [int] NULL,
	[ReportTemplate] [nvarchar](max) NULL,
 CONSTRAINT [PK_ReportDataSetMappedMaster] PRIMARY KEY CLUSTERED 
(
	[ReportDataSetMappedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDataSetTable]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDataSetTable](
	[ReportDataSetTableID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDataSetTableName] [nvarchar](max) NULL,
	[ReportDataSetTableDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK_ReportDataSetTable] PRIMARY KEY CLUSTERED 
(
	[ReportDataSetTableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDataSetTableColumn]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDataSetTableColumn](
	[ReportDataSetTableColumnID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDataSetTableColumnName] [nvarchar](max) NULL,
	[ReportDataSetTableColumnDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK_ReportDataSetTableColumn] PRIMARY KEY CLUSTERED 
(
	[ReportDataSetTableColumnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDetail](
	[ReportID] [int] IDENTITY(1,1) NOT NULL,
	[ReportName] [nvarchar](550) NULL,
	[ReportDescription] [nvarchar](550) NULL,
 CONSTRAINT [PK_ReportDetail] PRIMARY KEY CLUSTERED 
(
	[ReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDetailInfo]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDetailInfo](
	[ReportDetailInfoID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NULL,
	[ReportGroupName] [int] NULL,
 CONSTRAINT [PK_ReportDetailInfo] PRIMARY KEY CLUSTERED 
(
	[ReportDetailInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDetailInfoItem]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDetailInfoItem](
	[ReportDetailInfoItemID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDetailInfoItemType] [nvarchar](550) NULL,
	[ReportDetailInfoID] [int] NULL,
 CONSTRAINT [PK_ReportDetailInfoItem] PRIMARY KEY CLUSTERED 
(
	[ReportDetailInfoItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportDetailInfoItemProperty]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDetailInfoItemProperty](
	[ReportDetailInfoItemPropertyID] [int] IDENTITY(1,1) NOT NULL,
	[ReportDetailInfoItemID] [int] NULL,
	[FontFamilyName] [nvarchar](250) NULL,
	[FontSizeName] [nvarchar](250) NULL,
	[FontStyleName] [nvarchar](250) NULL,
	[FontWeightName] [nvarchar](250) NULL,
	[ItemContent] [nvarchar](max) NULL,
 CONSTRAINT [PK_ReportDetailInfoItemProperty] PRIMARY KEY CLUSTERED 
(
	[ReportDetailInfoItemPropertyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportGenerationContentType]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportGenerationContentType](
	[ReportGenerationContentTypeID] [int] NOT NULL,
	[ReportGenerationContentTypeName] [nvarchar](350) NULL,
	[ReportGenerationContentTypeDescription] [nvarchar](550) NULL,
 CONSTRAINT [PK_ReportGenerationContentType] PRIMARY KEY CLUSTERED 
(
	[ReportGenerationContentTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportGenerationDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportGenerationDetail](
	[ReportGeneratorID] [int] IDENTITY(1,1) NOT NULL,
	[ReportGeneratorIndex] [int] NULL,
	[ReportGeneratorHeadName] [nvarchar](250) NULL,
	[ReportID] [int] NULL,
	[ReportTemplate] [nvarchar](max) NULL,
 CONSTRAINT [PK_ReportGenerationDetail] PRIMARY KEY CLUSTERED 
(
	[ReportGeneratorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportGenerationInfo]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportGenerationInfo](
	[ReportID] [int] IDENTITY(1,1) NOT NULL,
	[ReportName] [nvarchar](350) NULL,
	[ReportDescription] [nvarchar](450) NULL,
 CONSTRAINT [PK_ReportGenerationInfo] PRIMARY KEY CLUSTERED 
(
	[ReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportGeneratorContent]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportGeneratorContent](
	[ReportGeneratorContentID] [int] IDENTITY(1,1) NOT NULL,
	[ReportGeneratorContentIndex] [int] NULL,
	[ReportGeneratorContentType] [nvarchar](350) NULL,
 CONSTRAINT [PK_ReportGeneratorContent] PRIMARY KEY CLUSTERED 
(
	[ReportGeneratorContentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportGroup]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportGroup](
	[ReportGroupID] [int] IDENTITY(1,1) NOT NULL,
	[ReportGroupName] [nvarchar](550) NULL,
 CONSTRAINT [PK_ReportGroup] PRIMARY KEY CLUSTERED 
(
	[ReportGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportTable]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportTable](
	[ReportTableID] [int] IDENTITY(1,1) NOT NULL,
	[ReportTableName] [nvarchar](550) NULL,
	[ReportTableJoinStatement] [nvarchar](550) NULL,
	[ReportTableGroupID] [int] NULL,
	[ReportTableRootTableID] [int] NULL,
	[ReportTableRootRelationalTableID] [int] NULL,
 CONSTRAINT [PK_ReportTable] PRIMARY KEY CLUSTERED 
(
	[ReportTableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportTableGroup]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportTableGroup](
	[ReportTableGroupID] [int] IDENTITY(1,1) NOT NULL,
	[ReportTableGroupName] [nvarchar](550) NULL,
	[ReportTableGroupDescription] [nvarchar](550) NULL,
	[GroupColorCode] [nchar](10) NULL,
 CONSTRAINT [PK_ReportTableGroup] PRIMARY KEY CLUSTERED 
(
	[ReportTableGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleGuid] [nvarchar](50) NOT NULL,
	[RoleID] [nvarchar](50) NULL,
	[RoleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SchoolDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolDetail](
	[SchoolID] [int] IDENTITY(1,1) NOT NULL,
	[Abbr] [nvarchar](50) NULL,
	[SchoolName] [nvarchar](500) NULL,
	[MeetSetupID] [int] NULL,
 CONSTRAINT [PK_SchoolDetails] PRIMARY KEY CLUSTERED 
(
	[SchoolID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SeedingPreferencesTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SeedingPreferencesTransaction](
	[SeedingPrefrencesID] [int] IDENTITY(1,1) NOT NULL,
	[WaterfallStartPreferencesGroupID] [nvarchar](250) NULL,
	[StandardLanePrefrencesGroupID] [nvarchar](250) NULL,
	[StandardAlleyPrefrencesGroupID] [nvarchar](250) NULL,
	[DualMeetGroupID] [nvarchar](250) NULL,
	[SeedingRulesGroupID] [nvarchar](250) NULL,
	[RendomizationRuleGroupID] [nvarchar](250) NULL,
 CONSTRAINT [PK_SeedingPreferences] PRIMARY KEY CLUSTERED 
(
	[SeedingPrefrencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SeedingRulesTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SeedingRulesTransaction](
	[SeedingRulesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[SeedingRulesGroupID] [nvarchar](250) NULL,
	[AllowForeignAthletesInFinal] [bit] NULL,
	[AllowExhibitionAthletesInFinal] [bit] NULL,
	[SeedExhibitionAthletesLast] [bit] NULL,
	[ApplyNCAARule] [bit] NULL,
	[UseSpecialRandomSelectMethod] [bit] NULL,
 CONSTRAINT [PK_SeedingRulesTransaction] PRIMARY KEY CLUSTERED 
(
	[SeedingRulesTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StandardAlleyPrefrences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardAlleyPrefrences](
	[StandardAlleyPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[PreferencesName] [nvarchar](250) NULL,
	[FirstLane] [int] NULL,
	[SecondLine] [int] NULL,
	[ThirdLine] [int] NULL,
	[ForthLine] [int] NULL,
	[FifthLine] [int] NULL,
	[SixthLine] [int] NULL,
	[SeventhLine] [int] NULL,
	[EightsLine] [int] NULL,
	[NinthLine] [int] NULL,
	[TenthLine] [int] NULL,
 CONSTRAINT [PK_StandardAlleyPrefrence] PRIMARY KEY CLUSTERED 
(
	[StandardAlleyPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StandardAlleyPrefrencesTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardAlleyPrefrencesTransaction](
	[StandardAlleyPrefrencesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[StandardAlleyPrefrencesGroupID] [nvarchar](250) NULL,
	[RowZeroColumnFirstValue] [smallint] NULL,
	[RowZeroColumnSecondValue] [smallint] NULL,
	[RowZeroColumnThirdValue] [smallint] NULL,
	[RowZeroColumnForthValue] [smallint] NULL,
	[RowZeroColumnFifthValue] [smallint] NULL,
	[RowZeroColumnSixthValue] [smallint] NULL,
	[RowZeroColumnSeventhValue] [smallint] NULL,
	[RowZeroColumnEightthValue] [smallint] NULL,
	[RowZeroColumnNinthValue] [smallint] NULL,
	[RowZeroColumnTenthValue] [smallint] NULL,
	[RowOneColumnFirstValue] [smallint] NULL,
	[RowOneColumnSecondValue] [smallint] NULL,
	[RowOneColumnThirdValue] [smallint] NULL,
	[RowOneColumnForthValue] [smallint] NULL,
	[RowOneColumnFifthValue] [smallint] NULL,
	[RowOneColumnSixthValue] [smallint] NULL,
	[RowOneColumnSeventhValue] [smallint] NULL,
	[RowOneColumnEightthValue] [smallint] NULL,
	[RowOneColumnNinthValue] [smallint] NULL,
	[RowOneColumnTenthValue] [smallint] NULL,
	[RowSecondColumnFirstValue] [smallint] NULL,
	[RowSecondColumnSecondValue] [smallint] NULL,
	[RowSecondColumnThirdValue] [smallint] NULL,
	[RowSecondColumnForthValue] [smallint] NULL,
	[RowSecondColumnFifthValue] [smallint] NULL,
	[RowSecondColumnSixthValue] [smallint] NULL,
	[RowSecondColumnSeventhValue] [smallint] NULL,
	[RowSecondColumnEightthValue] [smallint] NULL,
	[RowSecondColumnNinthValue] [smallint] NULL,
	[RowSecondColumnTenthValue] [smallint] NULL,
	[RowThirdColumnFirstValue] [smallint] NULL,
	[RowThirdColumnSecondValue] [smallint] NULL,
	[RowThirdColumnThirdValue] [smallint] NULL,
	[RowThirdColumnForthValue] [smallint] NULL,
	[RowThirdColumnFifthValue] [smallint] NULL,
	[RowThirdColumnSixthValue] [smallint] NULL,
	[RowThirdColumnSeventhValue] [smallint] NULL,
	[RowThirdColumnEightthValue] [smallint] NULL,
	[RowThirdColumnNinthValue] [smallint] NULL,
	[RowThridColumnTenthValue] [smallint] NULL,
	[RowForthColumnFirstValue] [smallint] NULL,
	[RowForthColumnSecondValue] [smallint] NULL,
	[RowForthColumnThirdValue] [smallint] NULL,
	[RowForthColumnForthValue] [smallint] NULL,
	[RowForthColumnFifthValue] [smallint] NULL,
	[RowForthColumnSixthValue] [smallint] NULL,
	[RowForthColumnSeventhValue] [smallint] NULL,
	[RowForthColumnEightthValue] [smallint] NULL,
	[RowForthColumnNinthValue] [smallint] NULL,
	[RowForthColumnTenthValue] [smallint] NULL,
	[RowFifthColumnFirstValue] [smallint] NULL,
	[RowFifthColumnSecondValue] [smallint] NULL,
	[RowFifthColumnThirdValue] [smallint] NULL,
	[RowFifthColumnForthValue] [smallint] NULL,
	[RowFifthColumnFifthValue] [smallint] NULL,
	[RowFifthColumnSixthValue] [smallint] NULL,
	[RowFifthColumnSeventhValue] [smallint] NULL,
	[RowFifthColumnEightthValue] [smallint] NULL,
	[RowFifthColumnNinthValue] [smallint] NULL,
	[RowFifthColumnTenthValue] [smallint] NULL,
	[RowSixthColumnFirstValue] [smallint] NULL,
	[RowSixthColumnSecondValue] [smallint] NULL,
	[RowSixthColumnThirdValue] [smallint] NULL,
	[RowSixthColumnForthValue] [smallint] NULL,
	[RowSixthColumnFifthValue] [smallint] NULL,
	[RowSixthColumnSixthValue] [smallint] NULL,
	[RowSixthColumnSeventhValue] [smallint] NULL,
	[RowSixthColumnEightthValue] [smallint] NULL,
	[RowSixthColumnNinthValue] [smallint] NULL,
	[RowSixthColumnTenthValue] [smallint] NULL,
	[RowSeventhColumnFirstValue] [smallint] NULL,
	[RowSeventhColumnSecondValue] [smallint] NULL,
	[RowSeventhColumnThirdValue] [smallint] NULL,
	[RowSeventhColumnForthValue] [smallint] NULL,
	[RowSeventhColumnFifthValue] [smallint] NULL,
	[RowSeventhColumnSixthValue] [smallint] NULL,
	[RowSeventhColumnSeventhValue] [smallint] NULL,
	[RowSeventhColumnEightthValue] [smallint] NULL,
	[RowSeventhColumnNinthValue] [smallint] NULL,
	[RowSeventhColumnTenthValue] [smallint] NULL,
	[RowEighthColumnFirstValue] [smallint] NULL,
	[RowEighthColumnSecondValue] [smallint] NULL,
	[RowEighthColumnThirdValue] [smallint] NULL,
	[RowEighthColumnForthValue] [smallint] NULL,
	[RowEighthColumnFifthValue] [smallint] NULL,
	[RowEighthColumnSixthValue] [smallint] NULL,
	[RowEighthColumnSeventhValue] [smallint] NULL,
	[RowEighthColumnEightthValue] [smallint] NULL,
	[RowEighthColumnNinthValue] [smallint] NULL,
	[RowEighthColumnTenthValue] [smallint] NULL,
	[RowNineColumnFirstValue] [smallint] NULL,
	[RowNineColumnSecondValue] [smallint] NULL,
	[RowNineColumnThirdValue] [smallint] NULL,
	[RowNineColumnForthValue] [smallint] NULL,
	[RowNineColumnFifthValue] [smallint] NULL,
	[RowNineColumnSixthValue] [smallint] NULL,
	[RowNineColumnSeventhValue] [smallint] NULL,
	[RowNineColumnEightthValue] [smallint] NULL,
	[RowNineColumnNinthValue] [smallint] NULL,
	[RowNineColumnTenthValue] [smallint] NULL,
 CONSTRAINT [PK_StandardAlleyPrefrencesTransaction] PRIMARY KEY CLUSTERED 
(
	[StandardAlleyPrefrencesTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StandardLanePrefrences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardLanePrefrences](
	[StandardLanePreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[PreferencesName] [nvarchar](250) NULL,
	[FirstLane] [int] NULL,
	[SecondLine] [int] NULL,
	[ThirdLine] [int] NULL,
	[ForthLine] [int] NULL,
	[FifthLine] [int] NULL,
	[SixthLine] [int] NULL,
	[SeventhLine] [int] NULL,
	[EightsLine] [int] NULL,
	[NinthLine] [int] NULL,
	[TenthLine] [int] NULL,
 CONSTRAINT [PK_StandardLanePrefrence] PRIMARY KEY CLUSTERED 
(
	[StandardLanePreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StandardLanePrefrencesTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StandardLanePrefrencesTransaction](
	[StandardLanePrefrencesTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[StandardLanePrefrencesGroupID] [nvarchar](250) NULL,
	[RowZeroColumnFirstValue] [smallint] NULL,
	[RowZeroColumnSecondValue] [smallint] NULL,
	[RowZeroColumnThirdValue] [smallint] NULL,
	[RowZeroColumnForthValue] [smallint] NULL,
	[RowZeroColumnFifthValue] [smallint] NULL,
	[RowZeroColumnSixthValue] [smallint] NULL,
	[RowZeroColumnSeventhValue] [smallint] NULL,
	[RowZeroColumnEightthValue] [smallint] NULL,
	[RowZeroColumnNinthValue] [smallint] NULL,
	[RowZeroColumnTenthValue] [smallint] NULL,
	[RowOneColumnFirstValue] [smallint] NULL,
	[RowOneColumnSecondValue] [smallint] NULL,
	[RowOneColumnThirdValue] [smallint] NULL,
	[RowOneColumnForthValue] [smallint] NULL,
	[RowOneColumnFifthValue] [smallint] NULL,
	[RowOneColumnSixthValue] [smallint] NULL,
	[RowOneColumnSeventhValue] [smallint] NULL,
	[RowOneColumnEightthValue] [smallint] NULL,
	[RowOneColumnNinthValue] [smallint] NULL,
	[RowOneColumnTenthValue] [smallint] NULL,
	[RowSecondColumnFirstValue] [smallint] NULL,
	[RowSecondColumnSecondValue] [smallint] NULL,
	[RowSecondColumnThirdValue] [smallint] NULL,
	[RowSecondColumnForthValue] [smallint] NULL,
	[RowSecondColumnFifthValue] [smallint] NULL,
	[RowSecondColumnSixthValue] [smallint] NULL,
	[RowSecondColumnSeventhValue] [smallint] NULL,
	[RowSecondColumnEightthValue] [smallint] NULL,
	[RowSecondColumnNinthValue] [smallint] NULL,
	[RowSecondColumnTenthValue] [smallint] NULL,
	[RowThirdColumnFirstValue] [smallint] NULL,
	[RowThirdColumnSecondValue] [smallint] NULL,
	[RowThirdColumnThirdValue] [smallint] NULL,
	[RowThirdColumnForthValue] [smallint] NULL,
	[RowThirdColumnFifthValue] [smallint] NULL,
	[RowThirdColumnSixthValue] [smallint] NULL,
	[RowThirdColumnSeventhValue] [smallint] NULL,
	[RowThirdColumnEightthValue] [smallint] NULL,
	[RowThirdColumnNinthValue] [smallint] NULL,
	[RowThridColumnTenthValue] [smallint] NULL,
	[RowForthColumnFirstValue] [smallint] NULL,
	[RowForthColumnSecondValue] [smallint] NULL,
	[RowForthColumnThirdValue] [smallint] NULL,
	[RowForthColumnForthValue] [smallint] NULL,
	[RowForthColumnFifthValue] [smallint] NULL,
	[RowForthColumnSixthValue] [smallint] NULL,
	[RowForthColumnSeventhValue] [smallint] NULL,
	[RowForthColumnEightthValue] [smallint] NULL,
	[RowForthColumnNinthValue] [smallint] NULL,
	[RowForthColumnTenthValue] [smallint] NULL,
	[RowFifthColumnFirstValue] [smallint] NULL,
	[RowFifthColumnSecondValue] [smallint] NULL,
	[RowFifthColumnThirdValue] [smallint] NULL,
	[RowFifthColumnForthValue] [smallint] NULL,
	[RowFifthColumnFifthValue] [smallint] NULL,
	[RowFifthColumnSixthValue] [smallint] NULL,
	[RowFifthColumnSeventhValue] [smallint] NULL,
	[RowFifthColumnEightthValue] [smallint] NULL,
	[RowFifthColumnNinthValue] [smallint] NULL,
	[RowFifthColumnTenthValue] [smallint] NULL,
	[RowSixthColumnFirstValue] [smallint] NULL,
	[RowSixthColumnSecondValue] [smallint] NULL,
	[RowSixthColumnThirdValue] [smallint] NULL,
	[RowSixthColumnForthValue] [smallint] NULL,
	[RowSixthColumnFifthValue] [smallint] NULL,
	[RowSixthColumnSixthValue] [smallint] NULL,
	[RowSixthColumnSeventhValue] [smallint] NULL,
	[RowSixthColumnEightthValue] [smallint] NULL,
	[RowSixthColumnNinthValue] [smallint] NULL,
	[RowSixthColumnTenthValue] [smallint] NULL,
	[RowSeventhColumnFirstValue] [smallint] NULL,
	[RowSeventhColumnSecondValue] [smallint] NULL,
	[RowSeventhColumnThirdValue] [smallint] NULL,
	[RowSeventhColumnForthValue] [smallint] NULL,
	[RowSeventhColumnFifthValue] [smallint] NULL,
	[RowSeventhColumnSixthValue] [smallint] NULL,
	[RowSeventhColumnSeventhValue] [smallint] NULL,
	[RowSeventhColumnEightthValue] [smallint] NULL,
	[RowSeventhColumnNinthValue] [smallint] NULL,
	[RowSeventhColumnTenthValue] [smallint] NULL,
	[RowEighthColumnFirstValue] [smallint] NULL,
	[RowEighthColumnSecondValue] [smallint] NULL,
	[RowEighthColumnThirdValue] [smallint] NULL,
	[RowEighthColumnForthValue] [smallint] NULL,
	[RowEighthColumnFifthValue] [smallint] NULL,
	[RowEighthColumnSixthValue] [smallint] NULL,
	[RowEighthColumnSeventhValue] [smallint] NULL,
	[RowEighthColumnEightthValue] [smallint] NULL,
	[RowEighthColumnNinthValue] [smallint] NULL,
	[RowEighthColumnTenthValue] [smallint] NULL,
	[RowNineColumnFirstValue] [smallint] NULL,
	[RowNineColumnSecondValue] [smallint] NULL,
	[RowNineColumnThirdValue] [smallint] NULL,
	[RowNineColumnForthValue] [smallint] NULL,
	[RowNineColumnFifthValue] [smallint] NULL,
	[RowNineColumnSixthValue] [smallint] NULL,
	[RowNineColumnSeventhValue] [smallint] NULL,
	[RowNineColumnEightthValue] [smallint] NULL,
	[RowNineColumnNinthValue] [smallint] NULL,
	[RowNineColumnTenthValue] [smallint] NULL,
 CONSTRAINT [PK_StandardLanePrefrencesTransaction] PRIMARY KEY CLUSTERED 
(
	[StandardLanePrefrencesTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserGuid] [nvarchar](50) NOT NULL,
	[EmailID] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[UserPhoto] [varbinary](max) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[UserName] [nvarchar](max) NULL,
	[IsAdmin] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserInRole]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInRole](
	[UserGuid] [nvarchar](50) NULL,
	[RoleGuid] [nvarchar](50) NULL,
	[UserInRoleID] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserInRoles] PRIMARY KEY CLUSTERED 
(
	[UserInRoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WaterfallStartPreferences]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WaterfallStartPreferences](
	[WaterfallStartPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[Position] [int] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_WaterfallStartPreference] PRIMARY KEY CLUSTERED 
(
	[WaterfallStartPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WaterfallStartPreferenceTransaction]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WaterfallStartPreferenceTransaction](
	[WaterfallStartPreferenceTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[WaterfallStartPreferencesGroupID] [nvarchar](250) NULL,
	[Position] [int] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_WaterfallStartPreferenceTransaction] PRIMARY KEY CLUSTERED 
(
	[WaterfallStartPreferenceTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (1, N'File', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (2, N'Set-up', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (3, N'Events', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (4, N'Athletes', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (5, N'Relays', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (6, N'Schools', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (7, N'Seeding', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (8, N'Run', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (9, N'Labels', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (10, N'Check for Updates', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (11, N'Help', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (12, N'Meet Set-up', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (13, N'Athlete/Relay Preferences', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (14, N'Seeding Preferences', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (15, N'Admin', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (16, N'Create User', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (17, N'Add User Role', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (18, N'Assign Role', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (19, N'Add Module', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (20, N'Assign Module', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (21, N'Report', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (22, N'Design  Report Template', 21)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (23, N'Assign DataSet To Report', 21)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (24, N'View Report', 21)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (25, N'Design Report Data Set', 21)
SET IDENTITY_INSERT [dbo].[AthletePreferences] ON 

INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (1, N'Enter ages')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (2, N'Enter birth dates')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (3, N'Enter middle intial')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (4, N'Enter school year')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (5, N'Enter Registration Numbers')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (6, N'Enter Athlete Status')
INSERT [dbo].[AthletePreferences] ([AthletePreferencesID], [AthletePreferencesName]) VALUES (7, N'Enter ''Citizen of''')
SET IDENTITY_INSERT [dbo].[AthletePreferences] OFF
SET IDENTITY_INSERT [dbo].[BaseCounty] ON 

INSERT [dbo].[BaseCounty] ([BaseCountyID], [BaseCountyName]) VALUES (1, N'USA')
INSERT [dbo].[BaseCounty] ([BaseCountyID], [BaseCountyName]) VALUES (2, N'CAN')
INSERT [dbo].[BaseCounty] ([BaseCountyID], [BaseCountyName]) VALUES (3, N'AUS')
INSERT [dbo].[BaseCounty] ([BaseCountyID], [BaseCountyName]) VALUES (4, N'OTH')
SET IDENTITY_INSERT [dbo].[BaseCounty] OFF
SET IDENTITY_INSERT [dbo].[CompetitorNumbers] ON 

INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (1, N'Enter competitor numbers', 0)
INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (2, N'Unique', 1)
INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (3, N'Male/Femail Separately Unique', 1)
INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (4, N'Disable Unqueness', 1)
INSERT [dbo].[CompetitorNumbers] ([CompetitorNumberID], [CompetitorNumbersName], [IsSingleSelectionGroup]) VALUES (7, N'Auto increment competitor numbers', 0)
SET IDENTITY_INSERT [dbo].[CompetitorNumbers] OFF
SET IDENTITY_INSERT [dbo].[LaneDetail] ON 

INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (1, 1, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (2, 2, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (3, 3, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (4, 4, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (5, 5, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (6, 6, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (7, 7, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (8, 8, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (9, 9, NULL)
INSERT [dbo].[LaneDetail] ([LaneID], [LaneNumber], [SchoolID]) VALUES (10, 10, NULL)
SET IDENTITY_INSERT [dbo].[LaneDetail] OFF
SET IDENTITY_INSERT [dbo].[MeetArena] ON 

INSERT [dbo].[MeetArena] ([MeetArenaID], [MeetArenaName]) VALUES (1, N'Outdoor')
INSERT [dbo].[MeetArena] ([MeetArenaID], [MeetArenaName]) VALUES (2, N'Indoor')
SET IDENTITY_INSERT [dbo].[MeetArena] OFF
SET IDENTITY_INSERT [dbo].[MeetClass] ON 

INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (1, N'High School')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (2, N'College')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (3, N'USATF')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (4, N'IAAF')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (5, N'AAU')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (6, N'Master/Veterans')
INSERT [dbo].[MeetClass] ([MeetClassID], [MeetClassName]) VALUES (7, N'Others')
SET IDENTITY_INSERT [dbo].[MeetClass] OFF
SET IDENTITY_INSERT [dbo].[MeetKind] ON 

INSERT [dbo].[MeetKind] ([MeetKindID], [MeetKindName]) VALUES (1, N'Track and Field/CC')
SET IDENTITY_INSERT [dbo].[MeetKind] OFF
SET IDENTITY_INSERT [dbo].[MeetStyle] ON 

INSERT [dbo].[MeetStyle] ([MeetStyleID], [MeetStyleName]) VALUES (1, N'Standard')
INSERT [dbo].[MeetStyle] ([MeetStyleID], [MeetStyleName]) VALUES (2, N'2 Team Dual')
INSERT [dbo].[MeetStyle] ([MeetStyleID], [MeetStyleName]) VALUES (3, N'3 + Team Dbi Dual')
SET IDENTITY_INSERT [dbo].[MeetStyle] OFF
SET IDENTITY_INSERT [dbo].[MeetType] ON 

INSERT [dbo].[MeetType] ([MeetTypeID], [MeetTypeName]) VALUES (1, N'Standard')
SET IDENTITY_INSERT [dbo].[MeetType] OFF
SET IDENTITY_INSERT [dbo].[MeetTypeDivision] ON 

INSERT [dbo].[MeetTypeDivision] ([MeetTypeDivisionID], [MeetTypeDivisionName], [MeetTypeID]) VALUES (1, N'By Event', 1)
INSERT [dbo].[MeetTypeDivision] ([MeetTypeDivisionID], [MeetTypeDivisionName], [MeetTypeID]) VALUES (2, N'By Team', 1)
INSERT [dbo].[MeetTypeDivision] ([MeetTypeDivisionID], [MeetTypeDivisionName], [MeetTypeID]) VALUES (3, N'By Entry', 1)
SET IDENTITY_INSERT [dbo].[MeetTypeDivision] OFF
SET IDENTITY_INSERT [dbo].[MenuInRole] ON 

INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (1, N'1', 15)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (2, N'1', 16)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (3, N'1', 17)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (4, N'1', 18)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (5, N'1', 19)
INSERT [dbo].[MenuInRole] ([MenuInRoleID], [RoleID], [MenuID]) VALUES (6, N'1', 20)
SET IDENTITY_INSERT [dbo].[MenuInRole] OFF
SET IDENTITY_INSERT [dbo].[RelayPreferences] ON 

INSERT [dbo].[RelayPreferences] ([RelayPreferencesID], [RelayPreferencesName]) VALUES (1, N'Allow "A" Relays only')
INSERT [dbo].[RelayPreferences] ([RelayPreferencesID], [RelayPreferencesName]) VALUES (2, N'Allow anyone from any team on a relay')
INSERT [dbo].[RelayPreferences] ([RelayPreferencesID], [RelayPreferencesName]) VALUES (3, N'Allow athlete on 2+ relays in same event')
SET IDENTITY_INSERT [dbo].[RelayPreferences] OFF
SET IDENTITY_INSERT [dbo].[ReportGroup] ON 

INSERT [dbo].[ReportGroup] ([ReportGroupID], [ReportGroupName]) VALUES (1, N'Header')
INSERT [dbo].[ReportGroup] ([ReportGroupID], [ReportGroupName]) VALUES (2, N'Body')
INSERT [dbo].[ReportGroup] ([ReportGroupID], [ReportGroupName]) VALUES (3, N'Footer')
SET IDENTITY_INSERT [dbo].[ReportGroup] OFF
SET IDENTITY_INSERT [dbo].[ReportTable] ON 

INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (1, N'MeetSetup', NULL, 1, NULL, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (2, N'MeetArena', N'INNER JOIN MeetArena ON  MeetSetup.MeetArenaID=MeetArena.MeetArenaID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (3, N'BaseCounty', N'INNER JOIN BaseCounty ON MeetSetup.BaseCountyID=BaseCounty.BaseCountyID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (4, N'MeetClass', N'INNER JOIN MeetClass ON MeetSetup.MeetClassID=MeetClass.MeetClassID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (5, N'MeetKind', N'INNER JOIN  MeetKind ON MeetSetup.MeetKindID=MeetKind.MeetKindID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (6, N'MeetStyle', N'INNER JOIN MeetStyle ON MeetSetup.MeetStyleID=MeetStyle.MeetStyleID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (7, N'MeetType', N'INNER JOIN MeetType ON  MeetSetup.MeetTypeID=MeetType.MeetTypeID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (8, N'MeetSetupRelayPreferences', N'INNER JOIN MeetSetupRelayPreferences ON MeetSetupRelayPreferences on MeetSetup.MeetSetupID =MeetSetupRelayPreferences.MeetSetupID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (9, N'RelayPreferences', N'INNER JOIN MeetSetupRelayPreferences ON RelayPreferences on RelayPreferences.RelayPreferencesID =MeetSetupRelayPreferences.RelayPreferencesID', 1, 1, 8)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (10, N'MeetSetupAthletePreferences', N'INNER JOIN MeetSetupAthletePreferences ON MeetSetupAthletePreferences ON MeetSetup.MeetSetupID=MeetSetupAthletePreferences.MeetSetupID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (11, N'AthletePreferences', N'INNER JOIN MeetSetupAthletePreferences ON AthletePreferences on AthletePreferences.AthletePreferencesID=MeetSetupAthletePreferences.AthletePreferencesID', 1, 1, 10)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (12, N'MeetSetupCompetitorNumber', N'INNER JOIN MeetSetupCompetitorNumber ON MeetSetup.MeetSetupID=MeetSetup.MeetSetupID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (13, N'CompetitorNumbers', N' INNER JOIN CompetitorNumbers on CompetitorNumbers.CompetitorNumberID=MeetSetupCompetitorNumber.CompetitorNumberID', 1, 1, 12)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (14, N'MeetSetupDualMeet', N'INNER JOIN  MeetSetupDualMeet ON MeetSetup.MeetSetupID=MeetSetupDualMeet.MeetSetupID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (15, N'LaneDetail', N'INNER JOIN LaneDetail ON MeetSetupDualMeet.LaneNumber=LaneDetail.LaneNumber', 1, 1, 14)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (16, N'SchoolDetail', N'INNER JOIN SchoolDetail on MeetSetupDualMeet.SchoolID= SchoolDetail.SchoolID ', 1, 1, 14)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (17, N'MeetSetupDualMeetAssignment', N'INNER JOIN  MeetSetupDualMeetAssignment ON MeetSetup.MeetSetupID=MeetSetupDualMeetAssignment.MeetSetupID
', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (18, N'MeetSetupRendomizationRule', N'INNER JOIN  MeetSetupRendomizationRule ON MeetSetup.MeetSetupID=MeetSetupRendomizationRule.MeetSetupID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (19, N'MeetSetupSeedingRules', N'INNER JOIN  MeetSetupSeedingRules ON MeetSetup.MeetSetupID=MeetSetupSeedingRules.MeetSetupID', 1, 1, NULL)
INSERT [dbo].[ReportTable] ([ReportTableID], [ReportTableName], [ReportTableJoinStatement], [ReportTableGroupID], [ReportTableRootTableID], [ReportTableRootRelationalTableID]) VALUES (20, N'MeetSetupWaterfallStart', N'INNER JOIN  MeetSetupWaterfallStart ON MeetSetup.MeetSetupID=MeetSetupWaterfallStart.MeetSetupID', 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[ReportTable] OFF
SET IDENTITY_INSERT [dbo].[ReportTableGroup] ON 

INSERT [dbo].[ReportTableGroup] ([ReportTableGroupID], [ReportTableGroupName], [ReportTableGroupDescription], [GroupColorCode]) VALUES (1, N'MeetSetup', N'Table Related to Meet Setup', N'#ff0000   ')
SET IDENTITY_INSERT [dbo].[ReportTableGroup] OFF
INSERT [dbo].[Role] ([RoleGuid], [RoleID], [RoleName]) VALUES (N'1', N'1', N'Admin')
SET IDENTITY_INSERT [dbo].[SchoolDetail] ON 

INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (1, N'AGOU', N'Agoura HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (2, N'CALB', N'Calabasas HAS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (3, N'MOOR', N'Moorpark HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (4, N'NEPA', N'Newbury Park HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (5, N'ROYL', N'Royal HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (6, N'SLVA', N'Simi valley HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (7, N'THOA', N'Thousand Oaks HS', NULL)
INSERT [dbo].[SchoolDetail] ([SchoolID], [Abbr], [SchoolName], [MeetSetupID]) VALUES (8, N'WELA', N'Westlake HS', NULL)
SET IDENTITY_INSERT [dbo].[SchoolDetail] OFF
SET IDENTITY_INSERT [dbo].[StandardAlleyPrefrences] ON 

INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (1, N'1 Alley', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (2, N'2 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (3, N'3 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (4, N'4 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (5, N'5 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (6, N'6 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (7, N'7 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (8, N'8 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (9, N'9 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardAlleyPrefrences] ([StandardAlleyPreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (10, N'10 Alleys', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[StandardAlleyPrefrences] OFF
SET IDENTITY_INSERT [dbo].[StandardLanePrefrences] ON 

INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (1, N'1 Lane', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (2, N'2 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (3, N'3 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (4, N'4 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (5, N'5 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (6, N'6 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (7, N'7 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (8, N'8 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (9, N'9 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[StandardLanePrefrences] ([StandardLanePreferencesID], [PreferencesName], [FirstLane], [SecondLine], [ThirdLine], [ForthLine], [FifthLine], [SixthLine], [SeventhLine], [EightsLine], [NinthLine], [TenthLine]) VALUES (10, N'10 Lanes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[StandardLanePrefrences] OFF
INSERT [dbo].[User] ([UserGuid], [EmailID], [FirstName], [LastName], [Password], [UserPhoto], [DisplayName], [UserName], [IsAdmin]) VALUES (N'1', N'admin@athleticssoftware.com', N'Admin', N'Admin', N'admin', NULL, N'Administrator', N'admin', 1)
INSERT [dbo].[UserInRole] ([UserGuid], [RoleGuid], [UserInRoleID]) VALUES (N'1', N'1', N'1')
SET IDENTITY_INSERT [dbo].[WaterfallStartPreferences] ON 

INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (1, NULL, 1)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (2, NULL, 2)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (3, NULL, 3)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (4, NULL, 4)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (5, NULL, 5)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (6, NULL, 6)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (7, NULL, 7)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (8, NULL, 8)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (9, NULL, 9)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (10, NULL, 10)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (11, NULL, 11)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (12, NULL, 12)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (13, NULL, 13)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (14, NULL, 14)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (15, NULL, 15)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (16, NULL, 16)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (17, NULL, 17)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (19, NULL, 18)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (20, NULL, 19)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (21, NULL, 20)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (22, NULL, 21)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (23, NULL, 22)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (24, NULL, 23)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (25, NULL, 24)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (26, NULL, 25)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (27, NULL, 26)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (28, NULL, 27)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (29, NULL, 28)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (30, NULL, 29)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (31, NULL, 30)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (32, NULL, 31)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (33, NULL, 32)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (34, NULL, 33)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (35, NULL, 34)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (36, NULL, 35)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (37, NULL, 36)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (38, NULL, 37)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (39, NULL, 38)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (40, NULL, 39)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (41, NULL, 40)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (42, NULL, 41)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (43, NULL, 42)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (44, NULL, 43)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (45, NULL, 44)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (46, NULL, 45)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (47, NULL, 46)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (48, NULL, 47)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (49, NULL, 48)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (50, NULL, 49)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (51, NULL, 50)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (52, NULL, 51)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (53, NULL, 52)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (54, NULL, 53)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (55, NULL, 54)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (56, NULL, 55)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (57, NULL, 56)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (58, NULL, 57)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (59, NULL, 58)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (60, NULL, 59)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (61, NULL, 60)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (62, NULL, 61)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (63, NULL, 62)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (64, NULL, 63)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (65, NULL, 64)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (66, NULL, 65)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (67, NULL, 66)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (68, NULL, 67)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (69, NULL, 68)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (70, NULL, 69)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (71, NULL, 70)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (72, NULL, 71)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (73, NULL, 72)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (74, NULL, 73)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (75, NULL, 74)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (76, NULL, 75)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (77, NULL, 76)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (78, NULL, 77)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (79, NULL, 78)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (80, NULL, 79)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (81, NULL, 80)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (82, NULL, 81)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (83, NULL, 82)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (84, NULL, 83)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (85, NULL, 84)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (86, NULL, 85)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (87, NULL, 86)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (88, NULL, 87)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (89, NULL, 88)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (90, NULL, 89)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (91, NULL, 90)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (92, NULL, 91)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (93, NULL, 92)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (94, NULL, 93)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (95, NULL, 94)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (96, NULL, 95)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (97, NULL, 96)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (98, NULL, 97)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (99, NULL, 98)
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (100, NULL, 99)
GO
INSERT [dbo].[WaterfallStartPreferences] ([WaterfallStartPreferencesID], [Position], [Rank]) VALUES (101, NULL, 100)
SET IDENTITY_INSERT [dbo].[WaterfallStartPreferences] OFF
ALTER TABLE [dbo].[ApplicationMenu]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1] FOREIGN KEY([ParentID])
REFERENCES [dbo].[ApplicationMenu] ([MenuID])
GO
ALTER TABLE [dbo].[ApplicationMenu] CHECK CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1]
GO
ALTER TABLE [dbo].[AthleticPreferencesTransaction]  WITH CHECK ADD  CONSTRAINT [FK_AthleticPreferencesTransaction_AthletePreferences] FOREIGN KEY([AthleticPreferencesID])
REFERENCES [dbo].[AthletePreferences] ([AthletePreferencesID])
GO
ALTER TABLE [dbo].[AthleticPreferencesTransaction] CHECK CONSTRAINT [FK_AthleticPreferencesTransaction_AthletePreferences]
GO
ALTER TABLE [dbo].[CompetitorNumbersTransaction]  WITH CHECK ADD  CONSTRAINT [FK_CompetitorNumbersTransaction_CompetitorNumbers] FOREIGN KEY([CompetitorNumbersID])
REFERENCES [dbo].[CompetitorNumbers] ([CompetitorNumberID])
GO
ALTER TABLE [dbo].[CompetitorNumbersTransaction] CHECK CONSTRAINT [FK_CompetitorNumbersTransaction_CompetitorNumbers]
GO
ALTER TABLE [dbo].[DualMeetTransaction]  WITH CHECK ADD  CONSTRAINT [FK_DualMeetTransaction_SchoolDetail] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[SchoolDetail] ([SchoolID])
GO
ALTER TABLE [dbo].[DualMeetTransaction] CHECK CONSTRAINT [FK_DualMeetTransaction_SchoolDetail]
GO
ALTER TABLE [dbo].[MeetSetup]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_BaseCounty] FOREIGN KEY([BaseCountyID])
REFERENCES [dbo].[BaseCounty] ([BaseCountyID])
GO
ALTER TABLE [dbo].[MeetSetup] CHECK CONSTRAINT [FK_MeetSetup_BaseCounty]
GO
ALTER TABLE [dbo].[MeetSetup]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetArena] FOREIGN KEY([MeetArenaID])
REFERENCES [dbo].[MeetArena] ([MeetArenaID])
GO
ALTER TABLE [dbo].[MeetSetup] CHECK CONSTRAINT [FK_MeetSetup_MeetArena]
GO
ALTER TABLE [dbo].[MeetSetup]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetClass] FOREIGN KEY([MeetClassID])
REFERENCES [dbo].[MeetClass] ([MeetClassID])
GO
ALTER TABLE [dbo].[MeetSetup] CHECK CONSTRAINT [FK_MeetSetup_MeetClass]
GO
ALTER TABLE [dbo].[MeetSetup]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetKind] FOREIGN KEY([MeetKindID])
REFERENCES [dbo].[MeetKind] ([MeetKindID])
GO
ALTER TABLE [dbo].[MeetSetup] CHECK CONSTRAINT [FK_MeetSetup_MeetKind]
GO
ALTER TABLE [dbo].[MeetSetup]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetup] CHECK CONSTRAINT [FK_MeetSetup_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetup]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetStyle] FOREIGN KEY([MeetStyleID])
REFERENCES [dbo].[MeetStyle] ([MeetStyleID])
GO
ALTER TABLE [dbo].[MeetSetup] CHECK CONSTRAINT [FK_MeetSetup_MeetStyle]
GO
ALTER TABLE [dbo].[MeetSetup]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetup_MeetType] FOREIGN KEY([MeetTypeID])
REFERENCES [dbo].[MeetType] ([MeetTypeID])
GO
ALTER TABLE [dbo].[MeetSetup] CHECK CONSTRAINT [FK_MeetSetup_MeetType]
GO
ALTER TABLE [dbo].[MeetSetupAthletePreferences]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupAthletePreferences_AthletePreferences] FOREIGN KEY([AthletePreferencesID])
REFERENCES [dbo].[AthletePreferences] ([AthletePreferencesID])
GO
ALTER TABLE [dbo].[MeetSetupAthletePreferences] CHECK CONSTRAINT [FK_MeetSetupAthletePreferences_AthletePreferences]
GO
ALTER TABLE [dbo].[MeetSetupCompetitorNumber]  WITH CHECK ADD  CONSTRAINT [FK_MeetCompetitorNumber_CompetitorNumbers] FOREIGN KEY([CompetitorNumberID])
REFERENCES [dbo].[CompetitorNumbers] ([CompetitorNumberID])
GO
ALTER TABLE [dbo].[MeetSetupCompetitorNumber] CHECK CONSTRAINT [FK_MeetCompetitorNumber_CompetitorNumbers]
GO
ALTER TABLE [dbo].[MeetSetupDualMeet]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupDualMeet_SchoolDetail] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[SchoolDetail] ([SchoolID])
GO
ALTER TABLE [dbo].[MeetSetupDualMeet] CHECK CONSTRAINT [FK_MeetSetupDualMeet_SchoolDetail]
GO
ALTER TABLE [dbo].[MeetSetupRelayPreferences]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupRelayPreferences_RelayPreferences] FOREIGN KEY([RelayPreferencesID])
REFERENCES [dbo].[RelayPreferences] ([RelayPreferencesID])
GO
ALTER TABLE [dbo].[MeetSetupRelayPreferences] CHECK CONSTRAINT [FK_MeetSetupRelayPreferences_RelayPreferences]
GO
ALTER TABLE [dbo].[MeetTypeDivision]  WITH CHECK ADD  CONSTRAINT [FK_MeetTypeDivision_MeetType] FOREIGN KEY([MeetTypeID])
REFERENCES [dbo].[MeetType] ([MeetTypeID])
GO
ALTER TABLE [dbo].[MeetTypeDivision] CHECK CONSTRAINT [FK_MeetTypeDivision_MeetType]
GO
ALTER TABLE [dbo].[MenuInRole]  WITH CHECK ADD  CONSTRAINT [FK_MenuInRole_ApplicationMenu] FOREIGN KEY([MenuID])
REFERENCES [dbo].[ApplicationMenu] ([MenuID])
GO
ALTER TABLE [dbo].[MenuInRole] CHECK CONSTRAINT [FK_MenuInRole_ApplicationMenu]
GO
ALTER TABLE [dbo].[MenuInRole]  WITH CHECK ADD  CONSTRAINT [FK_MenuInRole_Role] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([RoleGuid])
GO
ALTER TABLE [dbo].[MenuInRole] CHECK CONSTRAINT [FK_MenuInRole_Role]
GO
ALTER TABLE [dbo].[RelayPreferencesTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RelayPreferencesTransaction_RelayPreferences] FOREIGN KEY([RelayPreferencesID])
REFERENCES [dbo].[RelayPreferences] ([RelayPreferencesID])
GO
ALTER TABLE [dbo].[RelayPreferencesTransaction] CHECK CONSTRAINT [FK_RelayPreferencesTransaction_RelayPreferences]
GO
ALTER TABLE [dbo].[ReportColumnInDataSetTable]  WITH CHECK ADD  CONSTRAINT [FK_ReportColumnInDataSetTable_ReportDataSetTable] FOREIGN KEY([ReportDataSetTableID])
REFERENCES [dbo].[ReportDataSetTable] ([ReportDataSetTableID])
GO
ALTER TABLE [dbo].[ReportColumnInDataSetTable] CHECK CONSTRAINT [FK_ReportColumnInDataSetTable_ReportDataSetTable]
GO
ALTER TABLE [dbo].[ReportColumnInDataSetTable]  WITH CHECK ADD  CONSTRAINT [FK_ReportColumnInDataSetTable_ReportDataSetTableColumn] FOREIGN KEY([ReportDataSetColumnID])
REFERENCES [dbo].[ReportDataSetTableColumn] ([ReportDataSetTableColumnID])
GO
ALTER TABLE [dbo].[ReportColumnInDataSetTable] CHECK CONSTRAINT [FK_ReportColumnInDataSetTable_ReportDataSetTableColumn]
GO
ALTER TABLE [dbo].[ReportContentPropertyDetail]  WITH CHECK ADD  CONSTRAINT [FK_ReportContentPropertyDetail_ReportGeneratorContent] FOREIGN KEY([ReportGeneratorContentID])
REFERENCES [dbo].[ReportGeneratorContent] ([ReportGeneratorContentID])
GO
ALTER TABLE [dbo].[ReportContentPropertyDetail] CHECK CONSTRAINT [FK_ReportContentPropertyDetail_ReportGeneratorContent]
GO
ALTER TABLE [dbo].[ReportDataSetInTable]  WITH CHECK ADD  CONSTRAINT [FK_ReportDataSetInTable_ReportDataSetInfo] FOREIGN KEY([ReportDataSetID])
REFERENCES [dbo].[ReportDataSetInfo] ([ReportDataSetID])
GO
ALTER TABLE [dbo].[ReportDataSetInTable] CHECK CONSTRAINT [FK_ReportDataSetInTable_ReportDataSetInfo]
GO
ALTER TABLE [dbo].[ReportDataSetInTable]  WITH CHECK ADD  CONSTRAINT [FK_ReportDataSetInTable_ReportDataSetTable] FOREIGN KEY([ReportDataSetTableID])
REFERENCES [dbo].[ReportDataSetTable] ([ReportDataSetTableID])
GO
ALTER TABLE [dbo].[ReportDataSetInTable] CHECK CONSTRAINT [FK_ReportDataSetInTable_ReportDataSetTable]
GO
ALTER TABLE [dbo].[ReportDataSetMappedDetail]  WITH CHECK ADD  CONSTRAINT [FK_ReportDataSetMappedDetail_ReportDataSetMappedMaster] FOREIGN KEY([ReportDataSetMasterID])
REFERENCES [dbo].[ReportDataSetMappedMaster] ([ReportDataSetMappedID])
GO
ALTER TABLE [dbo].[ReportDataSetMappedDetail] CHECK CONSTRAINT [FK_ReportDataSetMappedDetail_ReportDataSetMappedMaster]
GO
ALTER TABLE [dbo].[ReportDataSetMappedMaster]  WITH CHECK ADD  CONSTRAINT [FK_ReportDataSetMappedMaster_ReportDataSetTable] FOREIGN KEY([ReportDataSetTableID])
REFERENCES [dbo].[ReportDataSetTable] ([ReportDataSetTableID])
GO
ALTER TABLE [dbo].[ReportDataSetMappedMaster] CHECK CONSTRAINT [FK_ReportDataSetMappedMaster_ReportDataSetTable]
GO
ALTER TABLE [dbo].[ReportDataSetMappedMaster]  WITH CHECK ADD  CONSTRAINT [FK_ReportDataSetMappedMaster_ReportDetail] FOREIGN KEY([ReportID])
REFERENCES [dbo].[ReportDetail] ([ReportID])
GO
ALTER TABLE [dbo].[ReportDataSetMappedMaster] CHECK CONSTRAINT [FK_ReportDataSetMappedMaster_ReportDetail]
GO
ALTER TABLE [dbo].[ReportDetailInfo]  WITH CHECK ADD  CONSTRAINT [FK_ReportDetailInfo_ReportDetail] FOREIGN KEY([ReportID])
REFERENCES [dbo].[ReportDetail] ([ReportID])
GO
ALTER TABLE [dbo].[ReportDetailInfo] CHECK CONSTRAINT [FK_ReportDetailInfo_ReportDetail]
GO
ALTER TABLE [dbo].[ReportDetailInfo]  WITH CHECK ADD  CONSTRAINT [FK_ReportDetailInfo_ReportGroup] FOREIGN KEY([ReportGroupName])
REFERENCES [dbo].[ReportGroup] ([ReportGroupID])
GO
ALTER TABLE [dbo].[ReportDetailInfo] CHECK CONSTRAINT [FK_ReportDetailInfo_ReportGroup]
GO
ALTER TABLE [dbo].[ReportDetailInfoItem]  WITH CHECK ADD  CONSTRAINT [FK_ReportDetailInfoItem_ReportDetailInfo] FOREIGN KEY([ReportDetailInfoID])
REFERENCES [dbo].[ReportDetailInfo] ([ReportDetailInfoID])
GO
ALTER TABLE [dbo].[ReportDetailInfoItem] CHECK CONSTRAINT [FK_ReportDetailInfoItem_ReportDetailInfo]
GO
ALTER TABLE [dbo].[ReportDetailInfoItemProperty]  WITH CHECK ADD  CONSTRAINT [FK_ReportDetailInfoItemProperty_ReportDetailInfoItem] FOREIGN KEY([ReportDetailInfoItemID])
REFERENCES [dbo].[ReportDetailInfoItem] ([ReportDetailInfoItemID])
GO
ALTER TABLE [dbo].[ReportDetailInfoItemProperty] CHECK CONSTRAINT [FK_ReportDetailInfoItemProperty_ReportDetailInfoItem]
GO
ALTER TABLE [dbo].[ReportGenerationDetail]  WITH CHECK ADD  CONSTRAINT [FK_ReportGenerationDetail_ReportGenerationInfo] FOREIGN KEY([ReportID])
REFERENCES [dbo].[ReportGenerationInfo] ([ReportID])
GO
ALTER TABLE [dbo].[ReportGenerationDetail] CHECK CONSTRAINT [FK_ReportGenerationDetail_ReportGenerationInfo]
GO
ALTER TABLE [dbo].[ReportTable]  WITH CHECK ADD  CONSTRAINT [FK_ReportTable_ReportTable] FOREIGN KEY([ReportTableRootTableID])
REFERENCES [dbo].[ReportTable] ([ReportTableID])
GO
ALTER TABLE [dbo].[ReportTable] CHECK CONSTRAINT [FK_ReportTable_ReportTable]
GO
ALTER TABLE [dbo].[ReportTable]  WITH CHECK ADD  CONSTRAINT [FK_ReportTable_ReportTable1] FOREIGN KEY([ReportTableRootRelationalTableID])
REFERENCES [dbo].[ReportTable] ([ReportTableID])
GO
ALTER TABLE [dbo].[ReportTable] CHECK CONSTRAINT [FK_ReportTable_ReportTable1]
GO
ALTER TABLE [dbo].[ReportTable]  WITH CHECK ADD  CONSTRAINT [FK_ReportTable_ReportTableGroup] FOREIGN KEY([ReportTableGroupID])
REFERENCES [dbo].[ReportTableGroup] ([ReportTableGroupID])
GO
ALTER TABLE [dbo].[ReportTable] CHECK CONSTRAINT [FK_ReportTable_ReportTableGroup]
GO
ALTER TABLE [dbo].[UserInRole]  WITH CHECK ADD  CONSTRAINT [FK_UserInRole_Role] FOREIGN KEY([RoleGuid])
REFERENCES [dbo].[Role] ([RoleGuid])
GO
ALTER TABLE [dbo].[UserInRole] CHECK CONSTRAINT [FK_UserInRole_Role]
GO
ALTER TABLE [dbo].[UserInRole]  WITH CHECK ADD  CONSTRAINT [FK_UserInRole_User] FOREIGN KEY([UserGuid])
REFERENCES [dbo].[User] ([UserGuid])
GO
ALTER TABLE [dbo].[UserInRole] CHECK CONSTRAINT [FK_UserInRole_User]
GO
/****** Object:  StoredProcedure [dbo].[GetAllReportDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Devender Pal Sharma
-- Create date: 06 April 2019
-- Description:	Get the View detail for Report
-- =============================================
CREATE PROCEDURE [dbo].[GetAllReportDetail]
	
AS
BEGIN
	SET NOCOUNT ON;

    SELECT ReportDetail.ReportID,ReportDetail.ReportName,ReportDetail.ReportDescription,ReportDataSetInfo.ReportDataSetName,
  ReportDataSetInfo.ReportDataSetDescription,
  ReportDataSetInfo.ReportDataSetQuery,
  ReportDataSetMappedMaster.ReportTemplate
   FROM ReportDetail
  INNER JOIN ReportDataSetMappedMaster ON ReportDataSetMappedMaster.ReportID = ReportDetail.ReportID
  INNER JOIN ReportDataSetInfo ON ReportDataSetInfo.ReportDataSetID=ReportDataSetMappedMaster.ReportDataSetTableID
END

GO
/****** Object:  StoredProcedure [dbo].[GetForeignKeyDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetForeignKeyDetail]
	-- Add the parameters for the stored procedure here
	@TableName nvarchar(500),
	@ColumnName nvarchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT o2.name AS Referenced_Table_Name,
       c2.name AS Referenced_Column_As_FK,
       o1.name AS Referencing_Table_Name,
       c1.name AS Referencing_Column_Name,
	s.name AS Constraint_name
	FROM  sysforeignkeys fk
	INNER JOIN sysobjects o1 ON fk.fkeyid = o1.id
	INNER JOIN sysobjects o2 ON fk.rkeyid = o2.id
	INNER JOIN syscolumns c1 ON c1.id = o1.id AND c1.colid = fk.fkey
	INNER JOIN syscolumns c2 ON c2.id = o2.id AND c2.colid = fk.rkey
	INNER JOIN sysobjects s ON fk.constid = s.id
	WHERE o1.name = @TableName
	AND c2.name=@ColumnName
	ORDER BY o2.name
END

GO
/****** Object:  StoredProcedure [dbo].[GetRelayPrefrencesGroupData]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Devender Sharma
-- Create date: 30 December 2018
-- Description:	To get the Relay Preferences to Assign in Meet Setup
-- =============================================
CREATE PROCEDURE [dbo].[GetRelayPrefrencesGroupData] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  declare @InitialGroup nvarchar(30)
  SET @InitialGroup='RelayPrefrences';
	
    -- Insert statements for procedure here
	SELECT distinct @InitialGroup + Cast([AthleteRelayPreferencesID]AS VARCHAR(max)) as RelayGroupID
      ,AthletePreferences.AthletePreferencesName
      ,CompetitorNumbers.CompetitorNumbersName
      ,RelayPreferences.RelayPreferencesName
  FROM [AthleticsDB].[dbo].[AthleteRelayPreferencesTransaction]

  INNER JOIN [AthleticsDB].[dbo].AthleticPreferencesTransaction 
  on AthleticPreferencesTransaction.AthleticPreferencesGroupID = AthleteRelayPreferencesTransaction.PreferencesTransactionGroupID

   INNER JOIN [AthleticsDB].[dbo].AthletePreferences 
  on AthletePreferences.AthletePreferencesID = AthleticPreferencesTransaction.AthleticPreferencesID

  INNER JOIN [AthleticsDB].[dbo].CompetitorNumbersTransaction 
  on [AthleticsDB].[dbo].CompetitorNumbersTransaction.CompetitorNumbersGroupID =AthleteRelayPreferencesTransaction.CompetitorNumberTransactionGroupID

  INNER JOIN [AthleticsDB].[dbo].CompetitorNumbers 
  on CompetitorNumbers.CompetitorNumberID = CompetitorNumbersTransaction.CompetitorNumbersID


   INNER JOIN [AthleticsDB].[dbo].RelayPreferencesTransaction 
  on [AthleticsDB].[dbo].RelayPreferencesTransaction.RelayPreferencesGroupID =AthleteRelayPreferencesTransaction.RelayPreferencesTransactionGroupID
   INNER JOIN [AthleticsDB].[dbo].RelayPreferences 
  on [AthleticsDB].[dbo].RelayPreferences.RelayPreferencesID =RelayPreferencesTransaction.RelayPreferencesID

END



GO
/****** Object:  StoredProcedure [dbo].[GetRelayPrefrencesMeetData]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Devender Sharma
-- Create Jan 06 2019
-- Description:	To Get the Relay Preferences based on meet ID
-- =============================================
CREATE PROCEDURE [dbo].[GetRelayPrefrencesMeetData]
	-- Add the parameters for the stored procedure here
	@MeetSetupID int
	
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT      AthletePreferences.AthletePreferencesName
      ,CompetitorNumbers.CompetitorNumbersName
      ,RelayPreferences.RelayPreferencesName
  FROM  [AthleticsDB].[dbo].MeetSetupAthletePreferences 

   INNER JOIN [AthleticsDB].[dbo].AthletePreferences 
  on AthletePreferences.AthletePreferencesID = MeetSetupAthletePreferences.AthletePreferencesID AND MeetSetupAthletePreferences.MeetSetupID = @MeetSetupID

  INNER JOIN [AthleticsDB].[dbo].MeetSetupCompetitorNumber 
  on [AthleticsDB].[dbo].MeetSetupCompetitorNumber.MeetSetupID = @MeetSetupID

  INNER JOIN [AthleticsDB].[dbo].CompetitorNumbers 
  on CompetitorNumbers.CompetitorNumberID = MeetSetupCompetitorNumber.CompetitorNumberID

   INNER JOIN [AthleticsDB].[dbo].MeetSetupRelayPreferences 
  on [AthleticsDB].[dbo].MeetSetupRelayPreferences.MeetSetupID =@MeetSetupID
   INNER JOIN [AthleticsDB].[dbo].RelayPreferences 
  on [AthleticsDB].[dbo].RelayPreferences.RelayPreferencesID =MeetSetupRelayPreferences.RelayPreferencesID
END

GO
/****** Object:  StoredProcedure [dbo].[GetReportDetailByReportID]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Devender Sharma
-- Create date: 31 March 2019
-- Description:	Get the Report Detail by Report ID
-- =============================================
CREATE PROCEDURE [dbo].[GetReportDetailByReportID]
	-- Add the parameters for the stored procedure here
	@ReportID int 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select Distinct
	   ReportName
	  ,ReportDescription
	  ,ReportGroup.ReportGroupName
	  ,ReportDetailInfoItemType
	  ,FontFamilyName
      ,FontSizeName
      ,FontStyleName
      ,FontWeightName
      ,ItemContent
	from ReportDetail
	INNER JOIN ReportDetailInfo on ReportDetail.ReportID=ReportDetailInfo.ReportID
	INNER JOIN ReportGroup on ReportDetailInfo.ReportGroupName=ReportGroup.ReportGroupID
    INNER JOIN ReportDetailInfoItemProperty on ReportDetailInfo.ReportDetailInfoID=ReportDetailInfoItemProperty.ReportDetailInfoItemID
	--INNER JOIN ReportDetailInfoItem on ReportDetailInfo.ReportDetailInfoID=ReportDetailInfoItemProperty.ReportDetailInfoItemID
	INNER JOIN ReportDetailInfoItem on ReportDetailInfo.ReportDetailInfoID=ReportDetailInfoItem.ReportDetailInfoItemID
	Where ReportDetail.ReportID=@ReportID

  END

GO
/****** Object:  StoredProcedure [dbo].[GetSeedingPreferencesGroupDetail]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSeedingPreferencesGroupDetail]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @InitialGroup nvarchar(30)
	SET @InitialGroup='SeedPrefrences';
	
    -- Insert statements for procedure here
    -- Insert statements for procedure here
	SELECT distinct  @InitialGroup +  Cast(SeedingPrefrencesID AS VARCHAR(max)) as SeedingGroup, SeedingPrefrencesID,Position as WaterfallStartPosition,[Rank] as WaterfallStartRank,
	   [AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnFirstValue] as StandardLanePrefrencesRowZeroColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnSecondValue] as StandardLanePrefrencesRowZeroColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnThirdValue] as StandardLanePrefrencesRowZeroColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnForthValue] as StandardLanePrefrencesRowZeroColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnFifthValue] as StandardLanePrefrencesRowZeroColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnSixthValue] as StandardLanePrefrencesRowZeroColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnSeventhValue] as StandardLanePrefrencesRowZeroColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnEightthValue] as StandardLanePrefrencesRowZeroColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnNinthValue] as StandardLanePrefrencesRowZeroColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowZeroColumnTenthValue] as StandardLanePrefrencesRowZeroColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnFirstValue] as StandardLanePrefrencesRowOneColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnSecondValue] as StandardLanePrefrencesRowOneColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnThirdValue] as StandardLanePrefrencesRowOneColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnForthValue]  as StandardLanePrefrencesRowOneColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnFifthValue] as StandardLanePrefrencesRowOneColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnSixthValue] as StandardLanePrefrencesRowOneColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnSeventhValue] as StandardLanePrefrencesRowOneColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnEightthValue] as StandardLanePrefrencesRowOneColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnNinthValue] as StandardLanePrefrencesRowOneColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowOneColumnTenthValue] as StandardLanePrefrencesRowOneColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnFirstValue] as StandardLanePrefrencesRowSecondColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnSecondValue] as StandardLanePrefrencesRowSecondColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnThirdValue] as StandardLanePrefrencesRowSecondColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnForthValue] as StandardLanePrefrencesRowSecondColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnFifthValue] as StandardLanePrefrencesRowSecondColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnSixthValue] as StandardLanePrefrencesRowSecondColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnSeventhValue] as StandardLanePrefrencesRowSecondColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnEightthValue] as StandardLanePrefrencesRowSecondColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnNinthValue] as StandardLanePrefrencesRowSecondColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSecondColumnTenthValue] as StandardLanePrefrencesRowSecondColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnFirstValue] as StandardLanePrefrencesRowThirdColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnSecondValue] as StandardLanePrefrencesRowThirdColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnThirdValue] as StandardLanePrefrencesRowThirdColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnForthValue] as StandardLanePrefrencesRowThirdColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnFifthValue] as StandardLanePrefrencesRowThirdColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnSixthValue] as StandardLanePrefrencesRowThirdColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnSeventhValue] as StandardLanePrefrencesRowThirdColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnEightthValue] as StandardLanePrefrencesRowThirdColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThirdColumnNinthValue] as StandardLanePrefrencesRowThirdColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowThridColumnTenthValue] as StandardLanePrefrencesRowThridColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnFirstValue] as StandardLanePrefrencesRowForthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnSecondValue] as StandardLanePrefrencesRowForthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnThirdValue] as StandardLanePrefrencesRowForthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnForthValue] as StandardLanePrefrencesRowForthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnFifthValue] as StandardLanePrefrencesRowForthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnSixthValue] as StandardLanePrefrencesRowForthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnSeventhValue] as StandardLanePrefrencesRowForthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnEightthValue] as StandardLanePrefrencesRowForthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnNinthValue] as StandardLanePrefrencesRowForthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowForthColumnTenthValue] as StandardLanePrefrencesRowForthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnFirstValue] as StandardLanePrefrencesRowFifthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnSecondValue] as StandardLanePrefrencesRowFifthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnThirdValue] as StandardLanePrefrencesRowFifthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnForthValue] as StandardLanePrefrencesRowFifthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnFifthValue] as StandardLanePrefrencesRowFifthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnSixthValue] as StandardLanePrefrencesRowFifthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnSeventhValue] as StandardLanePrefrencesRowFifthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnEightthValue] as StandardLanePrefrencesRowFifthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnNinthValue] as StandardLanePrefrencesRowFifthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowFifthColumnTenthValue] as StandardLanePrefrencesRowFifthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnFirstValue] as StandardLanePrefrencesRowSixthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnSecondValue] as StandardLanePrefrencesRowSixthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnThirdValue] as StandardLanePrefrencesRowSixthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnForthValue] as StandardLanePrefrencesRowSixthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnFifthValue] as StandardLanePrefrencesRowSixthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnSixthValue] as StandardLanePrefrencesRowSixthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnSeventhValue] as StandardLanePrefrencesRowSixthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnEightthValue] as StandardLanePrefrencesRowSixthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnNinthValue] as StandardLanePrefrencesRowSixthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSixthColumnTenthValue] as StandardLanePrefrencesRowSixthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnFirstValue] as StandardLanePrefrencesRowSeventhColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnSecondValue] as StandardLanePrefrencesRowSeventhColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnThirdValue] as StandardLanePrefrencesRowSeventhColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnForthValue] as StandardLanePrefrencesRowSeventhColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnFifthValue] as StandardLanePrefrencesRowSeventhColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnSixthValue] as StandardLanePrefrencesRowSeventhColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnSeventhValue] as StandardLanePrefrencesRowSeventhColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnEightthValue] as StandardLanePrefrencesRowSeventhColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnNinthValue] as StandardLanePrefrencesRowSeventhColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowSeventhColumnTenthValue] as StandardLanePrefrencesRowSeventhColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnFirstValue] as StandardLanePrefrencesRowEighthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnSecondValue] as StandardLanePrefrencesRowEighthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnThirdValue] as StandardLanePrefrencesRowEighthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnForthValue] as StandardLanePrefrencesRowEighthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnFifthValue] as StandardLanePrefrencesRowEighthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnSixthValue] as StandardLanePrefrencesRowEighthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnSeventhValue] as StandardLanePrefrencesRowEighthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnEightthValue] as StandardLanePrefrencesRowEighthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnNinthValue] as StandardLanePrefrencesRowEighthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowEighthColumnTenthValue] as StandardLanePrefrencesRowEighthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnFirstValue] as StandardLanePrefrencesRowNineColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnSecondValue] as StandardLanePrefrencesRowNineColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnThirdValue] as StandardLanePrefrencesRowNineColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnForthValue] as StandardLanePrefrencesRowNineColumnForthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnFifthValue] as StandardLanePrefrencesRowNineColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnSixthValue] as StandardLanePrefrencesRowNineColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnSeventhValue] as StandardLanePrefrencesRowNineColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnEightthValue] as StandardLanePrefrencesRowNineColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnNinthValue] as StandardLanePrefrencesRowNineColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardLanePrefrencesTransaction].[RowNineColumnTenthValue]  as StandardLanePrefrencesRowNineColumnTenthValue
	   ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnFirstValue] as StandardAlleyPrefrencesRowZeroColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnSecondValue] as StandardAlleyPrefrencesRowZeroColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnThirdValue] as StandardAlleyPrefrencesRowZeroColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnForthValue] as StandardAlleyPrefrencesRowZeroColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnFifthValue] as StandardAlleyPrefrencesRowZeroColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnSixthValue] as StandardAlleyPrefrencesRowZeroColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnSeventhValue] as StandardAlleyPrefrencesRowZeroColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnEightthValue] as StandardAlleyPrefrencesRowZeroColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnNinthValue] as StandardAlleyPrefrencesRowZeroColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowZeroColumnTenthValue] as StandardAlleyPrefrencesRowZeroColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnFirstValue] as StandardAlleyPrefrencesRowOneColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnSecondValue] as StandardAlleyPrefrencesRowOneColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnThirdValue] as StandardAlleyPrefrencesRowOneColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnForthValue]  as StandardAlleyPrefrencesRowOneColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnFifthValue] as StandardAlleyPrefrencesRowOneColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnSixthValue] as StandardAlleyPrefrencesRowOneColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnSeventhValue] as StandardAlleyPrefrencesRowOneColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnEightthValue] as StandardAlleyPrefrencesRowOneColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnNinthValue] as StandardAlleyPrefrencesRowOneColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowOneColumnTenthValue] as StandardAlleyPrefrencesRowOneColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnFirstValue] as StandardAlleyPrefrencesRowSecondColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnSecondValue] as StandardAlleyPrefrencesRowSecondColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnThirdValue] as StandardAlleyPrefrencesRowSecondColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnForthValue] as StandardAlleyPrefrencesRowSecondColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnFifthValue] as StandardAlleyPrefrencesRowSecondColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnSixthValue] as StandardAlleyPrefrencesRowSecondColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnSeventhValue] as StandardAlleyPrefrencesRowSecondColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnEightthValue] as StandardAlleyPrefrencesRowSecondColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnNinthValue] as StandardAlleyPrefrencesRowSecondColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSecondColumnTenthValue] as StandardAlleyPrefrencesRowSecondColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnFirstValue] as StandardAlleyPrefrencesRowThirdColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnSecondValue] as StandardAlleyPrefrencesRowThirdColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnThirdValue] as StandardAlleyPrefrencesRowThirdColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnForthValue] as StandardAlleyPrefrencesRowThirdColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnFifthValue] as StandardAlleyPrefrencesRowThirdColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnSixthValue] as StandardAlleyPrefrencesRowThirdColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnSeventhValue] as StandardAlleyPrefrencesRowThirdColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnEightthValue] as StandardAlleyPrefrencesRowThirdColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThirdColumnNinthValue] as StandardAlleyPrefrencesRowThirdColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowThridColumnTenthValue] as StandardAlleyPrefrencesRowThridColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnFirstValue] as StandardAlleyPrefrencesRowForthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnSecondValue] as StandardAlleyPrefrencesRowForthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnThirdValue] as StandardAlleyPrefrencesRowForthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnForthValue] as StandardAlleyPrefrencesRowForthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnFifthValue] as StandardAlleyPrefrencesRowForthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnSixthValue] as StandardAlleyPrefrencesRowForthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnSeventhValue] as StandardAlleyPrefrencesRowForthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnEightthValue] as StandardAlleyPrefrencesRowForthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnNinthValue] as StandardAlleyPrefrencesRowForthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowForthColumnTenthValue] as StandardAlleyPrefrencesRowForthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnFirstValue] as StandardAlleyPrefrencesRowFifthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnSecondValue] as StandardAlleyPrefrencesRowFifthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnThirdValue] as StandardAlleyPrefrencesRowFifthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnForthValue] as StandardAlleyPrefrencesRowFifthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnFifthValue] as StandardAlleyPrefrencesRowFifthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnSixthValue] as StandardAlleyPrefrencesRowFifthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnSeventhValue] as StandardAlleyPrefrencesRowFifthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnEightthValue] as StandardAlleyPrefrencesRowFifthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnNinthValue] as StandardAlleyPrefrencesRowFifthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowFifthColumnTenthValue] as StandardAlleyPrefrencesRowFifthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnFirstValue] as StandardAlleyPrefrencesRowSixthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnSecondValue] as StandardAlleyPrefrencesRowSixthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnThirdValue] as StandardAlleyPrefrencesRowSixthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnForthValue] as StandardAlleyPrefrencesRowSixthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnFifthValue] as StandardAlleyPrefrencesRowSixthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnSixthValue] as StandardAlleyPrefrencesRowSixthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnSeventhValue] as StandardAlleyPrefrencesRowSixthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnEightthValue] as StandardAlleyPrefrencesRowSixthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnNinthValue] as StandardAlleyPrefrencesRowSixthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSixthColumnTenthValue] as StandardAlleyPrefrencesRowSixthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnFirstValue] as StandardAlleyPrefrencesRowSeventhColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnSecondValue] as StandardAlleyPrefrencesRowSeventhColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnThirdValue] as StandardAlleyPrefrencesRowSeventhColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnForthValue] as StandardAlleyPrefrencesRowSeventhColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnFifthValue] as StandardAlleyPrefrencesRowSeventhColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnSixthValue] as StandardAlleyPrefrencesRowSeventhColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnSeventhValue] as StandardAlleyPrefrencesRowSeventhColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnEightthValue] as StandardAlleyPrefrencesRowSeventhColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnNinthValue] as StandardAlleyPrefrencesRowSeventhColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowSeventhColumnTenthValue] as StandardAlleyPrefrencesRowSeventhColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnFirstValue] as StandardAlleyPrefrencesRowEighthColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnSecondValue] as StandardAlleyPrefrencesRowEighthColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnThirdValue] as StandardAlleyPrefrencesRowEighthColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnForthValue] as StandardAlleyPrefrencesRowEighthColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnFifthValue] as StandardAlleyPrefrencesRowEighthColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnSixthValue] as StandardAlleyPrefrencesRowEighthColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnSeventhValue] as StandardAlleyPrefrencesRowEighthColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnEightthValue] as StandardAlleyPrefrencesRowEighthColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnNinthValue] as StandardAlleyPrefrencesRowEighthColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowEighthColumnTenthValue] as StandardAlleyPrefrencesRowEighthColumnTenthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnFirstValue] as StandardAlleyPrefrencesRowNineColumnFirstValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnSecondValue] as StandardAlleyPrefrencesRowNineColumnSecondValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnThirdValue] as StandardAlleyPrefrencesRowNineColumnThirdValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnForthValue] as StandardAlleyPrefrencesRowNineColumnForthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnFifthValue] as StandardAlleyPrefrencesRowNineColumnFifthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnSixthValue] as StandardAlleyPrefrencesRowNineColumnSixthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnSeventhValue] as StandardAlleyPrefrencesRowNineColumnSeventhValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnEightthValue] as StandardAlleyPrefrencesRowNineColumnEightthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnNinthValue] as StandardAlleyPrefrencesRowNineColumnNinthValue
      ,[AthleticsDB].[dbo].[StandardAlleyPrefrencesTransaction].[RowNineColumnTenthValue]  as StandardAlleyPrefrencesRowNineColumnTenthValue
	  ,[AthleticsDB].[dbo].DualMeetTransaction.LaneNumber as DualMeetLaneNumber
	   ,[AthleticsDB].[dbo].SchoolDetail.Abbr  as DualMeetSchoolAbbr
      ,[AthleticsDB].[dbo].SchoolDetail.SchoolName  as DualMeetSchoolName
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.AlternamtUserOfUnAssignedLane as DualMeetAlternateUserOfUnAssignedLane
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.StrictAssignmentAllHeats as DualMeetStrictAssignmentAllHeats
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.StrictAssignmentFastestHeatOnly as DualMeetStrictAssignmentFastestHeatOnly
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.UseLaneAssignmentsForInLaneRacesOnly as DualMeetUseLaneAssignmentsForInLaneRacesOnly
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.UserLaneOrPositionAssignmentsAbove as DualMeetUserLaneOrPositionAssignmentsAbove
	  ,[AthleticsDB].[dbo].DualMeetAssignmentTransaction.AlternamtUserOfUnAssignedLane as DualMeetAlternamtUserOfUnAssignedLane

	  ,[AthleticsDB].[dbo].RendomizationRuleTransaction.TimedFinalEvents as RendomizationRuleTimedFinalEvents
	  ,[AthleticsDB].[dbo].RendomizationRuleTransaction.RoundFirstMultipleRound as RendomizationRuleRoundFirstMultipleRound
	  ,[AthleticsDB].[dbo].RendomizationRuleTransaction.RoundSecondThirdAndForth as RendomizationRuleRoundSecondThirdAndForth
	  ,[AthleticsDB].[dbo].RendomizationRuleTransaction.CloseGap as RendomizationRuleCloseGap

	  ,[AthleticsDB].[dbo].SeedingRulesTransaction.AllowForeignAthletesInFinal as SeedingAllowForeignAthletesInFinal
	  ,[AthleticsDB].[dbo].SeedingRulesTransaction.AllowExhibitionAthletesInFinal as SeedingRulesAllowExhibitionAthletesInFinal
	  ,[AthleticsDB].[dbo].SeedingRulesTransaction.SeedExhibitionAthletesLast as SeedingRulesApplyNCAARule1
	  ,[AthleticsDB].[dbo].SeedingRulesTransaction.ApplyNCAARule as SeedingRulesApplyNCAARule
	    ,[AthleticsDB].[dbo].SeedingRulesTransaction.UseSpecialRandomSelectMethod as SeedingRulesUseSpecialRandomSelectMethod

  FROM [AthleticsDB].[dbo].SeedingPreferencesTransaction

  INNER JOIN [AthleticsDB].[dbo].WaterfallStartPreferenceTransaction 
  on WaterfallStartPreferenceTransaction.WaterfallStartPreferencesGroupID = SeedingPreferencesTransaction.WaterfallStartPreferencesGroupID

  INNER JOIN [AthleticsDB].[dbo].StandardLanePrefrencesTransaction 
  on StandardLanePrefrencesTransaction.StandardLanePrefrencesGroupID = SeedingPreferencesTransaction.StandardLanePrefrencesGroupID
   
  
  INNER JOIN [AthleticsDB].[dbo].StandardAlleyPrefrencesTransaction 
  on [AthleticsDB].[dbo].StandardAlleyPrefrencesTransaction.StandardAlleyPrefrencesGroupID =SeedingPreferencesTransaction.StandardAlleyPrefrencesGroupID

  INNER JOIN [AthleticsDB].[dbo].DualMeetTransaction 
  on DualMeetTransaction.DualMeetGroupID = SeedingPreferencesTransaction.DualMeetGroupID

 INNER JOIN [AthleticsDB].[dbo].DualMeetAssignmentTransaction 
  on DualMeetAssignmentTransaction.DualMeetGroupID = SeedingPreferencesTransaction.DualMeetGroupID

   INNER JOIN [AthleticsDB].[dbo].SchoolDetail 
  on SchoolDetail.SchoolID = DualMeetTransaction.SchoolID

  INNER JOIN [AthleticsDB].[dbo].SeedingRulesTransaction 
  on SeedingRulesTransaction.SeedingRulesGroupID = SeedingPreferencesTransaction.SeedingRulesGroupID

INNER JOIN [AthleticsDB].[dbo].RendomizationRuleTransaction 
  on RendomizationRuleTransaction.RendomizationRuleGroupID = SeedingPreferencesTransaction.RendomizationRuleGroupID

    
END

GO
/****** Object:  StoredProcedure [dbo].[GetSeedingPreferencesMeetData]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Devender Sharma
-- Create Jan 06 2019
-- Description:	To Get the Relay Preferences based on meet ID
-- =============================================
CREATE PROCEDURE [dbo].[GetSeedingPreferencesMeetData]
	-- Add the parameters for the stored procedure here
	@MeetSetupID int
	
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT distinct  Position as WaterfallStartPosition,[Rank] as WaterfallStartRank,
	   [AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnFirstValue] as StandardLanePrefrencesRowZeroColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnSecondValue] as StandardLanePrefrencesRowZeroColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnThirdValue] as StandardLanePrefrencesRowZeroColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnForthValue] as StandardLanePrefrencesRowZeroColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnFifthValue] as StandardLanePrefrencesRowZeroColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnSixthValue] as StandardLanePrefrencesRowZeroColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnSeventhValue] as StandardLanePrefrencesRowZeroColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnEightthValue] as StandardLanePrefrencesRowZeroColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnNinthValue] as StandardLanePrefrencesRowZeroColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowZeroColumnTenthValue] as StandardLanePrefrencesRowZeroColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnFirstValue] as StandardLanePrefrencesRowOneColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnSecondValue] as StandardLanePrefrencesRowOneColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnThirdValue] as StandardLanePrefrencesRowOneColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnForthValue]  as StandardLanePrefrencesRowOneColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnFifthValue] as StandardLanePrefrencesRowOneColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnSixthValue] as StandardLanePrefrencesRowOneColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnSeventhValue] as StandardLanePrefrencesRowOneColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnEightthValue] as StandardLanePrefrencesRowOneColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnNinthValue] as StandardLanePrefrencesRowOneColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowOneColumnTenthValue] as StandardLanePrefrencesRowOneColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnFirstValue] as StandardLanePrefrencesRowSecondColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnSecondValue] as StandardLanePrefrencesRowSecondColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnThirdValue] as StandardLanePrefrencesRowSecondColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnForthValue] as StandardLanePrefrencesRowSecondColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnFifthValue] as StandardLanePrefrencesRowSecondColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnSixthValue] as StandardLanePrefrencesRowSecondColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnSeventhValue] as StandardLanePrefrencesRowSecondColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnEightthValue] as StandardLanePrefrencesRowSecondColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnNinthValue] as StandardLanePrefrencesRowSecondColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSecondColumnTenthValue] as StandardLanePrefrencesRowSecondColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnFirstValue] as StandardLanePrefrencesRowThirdColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnSecondValue] as StandardLanePrefrencesRowThirdColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnThirdValue] as StandardLanePrefrencesRowThirdColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnForthValue] as StandardLanePrefrencesRowThirdColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnFifthValue] as StandardLanePrefrencesRowThirdColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnSixthValue] as StandardLanePrefrencesRowThirdColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnSeventhValue] as StandardLanePrefrencesRowThirdColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnEightthValue] as StandardLanePrefrencesRowThirdColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThirdColumnNinthValue] as StandardLanePrefrencesRowThirdColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowThridColumnTenthValue] as StandardLanePrefrencesRowThridColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnFirstValue] as StandardLanePrefrencesRowForthColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnSecondValue] as StandardLanePrefrencesRowForthColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnThirdValue] as StandardLanePrefrencesRowForthColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnForthValue] as StandardLanePrefrencesRowForthColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnFifthValue] as StandardLanePrefrencesRowForthColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnSixthValue] as StandardLanePrefrencesRowForthColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnSeventhValue] as StandardLanePrefrencesRowForthColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnEightthValue] as StandardLanePrefrencesRowForthColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnNinthValue] as StandardLanePrefrencesRowForthColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowForthColumnTenthValue] as StandardLanePrefrencesRowForthColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnFirstValue] as StandardLanePrefrencesRowFifthColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnSecondValue] as StandardLanePrefrencesRowFifthColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnThirdValue] as StandardLanePrefrencesRowFifthColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnForthValue] as StandardLanePrefrencesRowFifthColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnFifthValue] as StandardLanePrefrencesRowFifthColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnSixthValue] as StandardLanePrefrencesRowFifthColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnSeventhValue] as StandardLanePrefrencesRowFifthColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnEightthValue] as StandardLanePrefrencesRowFifthColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnNinthValue] as StandardLanePrefrencesRowFifthColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowFifthColumnTenthValue] as StandardLanePrefrencesRowFifthColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnFirstValue] as StandardLanePrefrencesRowSixthColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnSecondValue] as StandardLanePrefrencesRowSixthColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnThirdValue] as StandardLanePrefrencesRowSixthColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnForthValue] as StandardLanePrefrencesRowSixthColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnFifthValue] as StandardLanePrefrencesRowSixthColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnSixthValue] as StandardLanePrefrencesRowSixthColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnSeventhValue] as StandardLanePrefrencesRowSixthColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnEightthValue] as StandardLanePrefrencesRowSixthColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnNinthValue] as StandardLanePrefrencesRowSixthColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSixthColumnTenthValue] as StandardLanePrefrencesRowSixthColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnFirstValue] as StandardLanePrefrencesRowSeventhColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnSecondValue] as StandardLanePrefrencesRowSeventhColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnThirdValue] as StandardLanePrefrencesRowSeventhColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnForthValue] as StandardLanePrefrencesRowSeventhColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnFifthValue] as StandardLanePrefrencesRowSeventhColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnSixthValue] as StandardLanePrefrencesRowSeventhColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnSeventhValue] as StandardLanePrefrencesRowSeventhColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnEightthValue] as StandardLanePrefrencesRowSeventhColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnNinthValue] as StandardLanePrefrencesRowSeventhColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowSeventhColumnTenthValue] as StandardLanePrefrencesRowSeventhColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnFirstValue] as StandardLanePrefrencesRowEighthColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnSecondValue] as StandardLanePrefrencesRowEighthColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnThirdValue] as StandardLanePrefrencesRowEighthColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnForthValue] as StandardLanePrefrencesRowEighthColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnFifthValue] as StandardLanePrefrencesRowEighthColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnSixthValue] as StandardLanePrefrencesRowEighthColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnSeventhValue] as StandardLanePrefrencesRowEighthColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnEightthValue] as StandardLanePrefrencesRowEighthColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnNinthValue] as StandardLanePrefrencesRowEighthColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowEighthColumnTenthValue] as StandardLanePrefrencesRowEighthColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnFirstValue] as StandardLanePrefrencesRowNineColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnSecondValue] as StandardLanePrefrencesRowNineColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnThirdValue] as StandardLanePrefrencesRowNineColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnForthValue] as StandardLanePrefrencesRowNineColumnForthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnFifthValue] as StandardLanePrefrencesRowNineColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnSixthValue] as StandardLanePrefrencesRowNineColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnSeventhValue] as StandardLanePrefrencesRowNineColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnEightthValue] as StandardLanePrefrencesRowNineColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnNinthValue] as StandardLanePrefrencesRowNineColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetSetupStandardLanePrefrences].[RowNineColumnTenthValue]  as StandardLanePrefrencesRowNineColumnTenthValue
	   ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnFirstValue] as StandardAlleyPrefrencesRowZeroColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnSecondValue] as StandardAlleyPrefrencesRowZeroColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnThirdValue] as StandardAlleyPrefrencesRowZeroColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnForthValue] as StandardAlleyPrefrencesRowZeroColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnFifthValue] as StandardAlleyPrefrencesRowZeroColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnSixthValue] as StandardAlleyPrefrencesRowZeroColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnSeventhValue] as StandardAlleyPrefrencesRowZeroColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnEightthValue] as StandardAlleyPrefrencesRowZeroColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnNinthValue] as StandardAlleyPrefrencesRowZeroColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowZeroColumnTenthValue] as StandardAlleyPrefrencesRowZeroColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnFirstValue] as StandardAlleyPrefrencesRowOneColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnSecondValue] as StandardAlleyPrefrencesRowOneColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnThirdValue] as StandardAlleyPrefrencesRowOneColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnForthValue]  as StandardAlleyPrefrencesRowOneColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnFifthValue] as StandardAlleyPrefrencesRowOneColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnSixthValue] as StandardAlleyPrefrencesRowOneColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnSeventhValue] as StandardAlleyPrefrencesRowOneColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnEightthValue] as StandardAlleyPrefrencesRowOneColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnNinthValue] as StandardAlleyPrefrencesRowOneColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowOneColumnTenthValue] as StandardAlleyPrefrencesRowOneColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnFirstValue] as StandardAlleyPrefrencesRowSecondColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnSecondValue] as StandardAlleyPrefrencesRowSecondColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnThirdValue] as StandardAlleyPrefrencesRowSecondColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnForthValue] as StandardAlleyPrefrencesRowSecondColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnFifthValue] as StandardAlleyPrefrencesRowSecondColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnSixthValue] as StandardAlleyPrefrencesRowSecondColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnSeventhValue] as StandardAlleyPrefrencesRowSecondColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnEightthValue] as StandardAlleyPrefrencesRowSecondColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnNinthValue] as StandardAlleyPrefrencesRowSecondColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSecondColumnTenthValue] as StandardAlleyPrefrencesRowSecondColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnFirstValue] as StandardAlleyPrefrencesRowThirdColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnSecondValue] as StandardAlleyPrefrencesRowThirdColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnThirdValue] as StandardAlleyPrefrencesRowThirdColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnForthValue] as StandardAlleyPrefrencesRowThirdColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnFifthValue] as StandardAlleyPrefrencesRowThirdColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnSixthValue] as StandardAlleyPrefrencesRowThirdColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnSeventhValue] as StandardAlleyPrefrencesRowThirdColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnEightthValue] as StandardAlleyPrefrencesRowThirdColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThirdColumnNinthValue] as StandardAlleyPrefrencesRowThirdColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowThridColumnTenthValue] as StandardAlleyPrefrencesRowThridColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnFirstValue] as StandardAlleyPrefrencesRowForthColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnSecondValue] as StandardAlleyPrefrencesRowForthColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnThirdValue] as StandardAlleyPrefrencesRowForthColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnForthValue] as StandardAlleyPrefrencesRowForthColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnFifthValue] as StandardAlleyPrefrencesRowForthColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnSixthValue] as StandardAlleyPrefrencesRowForthColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnSeventhValue] as StandardAlleyPrefrencesRowForthColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnEightthValue] as StandardAlleyPrefrencesRowForthColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnNinthValue] as StandardAlleyPrefrencesRowForthColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowForthColumnTenthValue] as StandardAlleyPrefrencesRowForthColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnFirstValue] as StandardAlleyPrefrencesRowFifthColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnSecondValue] as StandardAlleyPrefrencesRowFifthColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnThirdValue] as StandardAlleyPrefrencesRowFifthColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnForthValue] as StandardAlleyPrefrencesRowFifthColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnFifthValue] as StandardAlleyPrefrencesRowFifthColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnSixthValue] as StandardAlleyPrefrencesRowFifthColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnSeventhValue] as StandardAlleyPrefrencesRowFifthColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnEightthValue] as StandardAlleyPrefrencesRowFifthColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnNinthValue] as StandardAlleyPrefrencesRowFifthColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowFifthColumnTenthValue] as StandardAlleyPrefrencesRowFifthColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnFirstValue] as StandardAlleyPrefrencesRowSixthColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnSecondValue] as StandardAlleyPrefrencesRowSixthColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnThirdValue] as StandardAlleyPrefrencesRowSixthColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnForthValue] as StandardAlleyPrefrencesRowSixthColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnFifthValue] as StandardAlleyPrefrencesRowSixthColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnSixthValue] as StandardAlleyPrefrencesRowSixthColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnSeventhValue] as StandardAlleyPrefrencesRowSixthColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnEightthValue] as StandardAlleyPrefrencesRowSixthColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnNinthValue] as StandardAlleyPrefrencesRowSixthColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSixthColumnTenthValue] as StandardAlleyPrefrencesRowSixthColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnFirstValue] as StandardAlleyPrefrencesRowSeventhColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnSecondValue] as StandardAlleyPrefrencesRowSeventhColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnThirdValue] as StandardAlleyPrefrencesRowSeventhColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnForthValue] as StandardAlleyPrefrencesRowSeventhColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnFifthValue] as StandardAlleyPrefrencesRowSeventhColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnSixthValue] as StandardAlleyPrefrencesRowSeventhColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnSeventhValue] as StandardAlleyPrefrencesRowSeventhColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnEightthValue] as StandardAlleyPrefrencesRowSeventhColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnNinthValue] as StandardAlleyPrefrencesRowSeventhColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowSeventhColumnTenthValue] as StandardAlleyPrefrencesRowSeventhColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnFirstValue] as StandardAlleyPrefrencesRowEighthColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnSecondValue] as StandardAlleyPrefrencesRowEighthColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnThirdValue] as StandardAlleyPrefrencesRowEighthColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnForthValue] as StandardAlleyPrefrencesRowEighthColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnFifthValue] as StandardAlleyPrefrencesRowEighthColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnSixthValue] as StandardAlleyPrefrencesRowEighthColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnSeventhValue] as StandardAlleyPrefrencesRowEighthColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnEightthValue] as StandardAlleyPrefrencesRowEighthColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnNinthValue] as StandardAlleyPrefrencesRowEighthColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowEighthColumnTenthValue] as StandardAlleyPrefrencesRowEighthColumnTenthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnFirstValue] as StandardAlleyPrefrencesRowNineColumnFirstValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnSecondValue] as StandardAlleyPrefrencesRowNineColumnSecondValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnThirdValue] as StandardAlleyPrefrencesRowNineColumnThirdValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnForthValue] as StandardAlleyPrefrencesRowNineColumnForthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnFifthValue] as StandardAlleyPrefrencesRowNineColumnFifthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnSixthValue] as StandardAlleyPrefrencesRowNineColumnSixthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnSeventhValue] as StandardAlleyPrefrencesRowNineColumnSeventhValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnEightthValue] as StandardAlleyPrefrencesRowNineColumnEightthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnNinthValue] as StandardAlleyPrefrencesRowNineColumnNinthValue
      ,[AthleticsDB].[dbo].[MeetStandardAlleyPrefrences].[RowNineColumnTenthValue]  as StandardAlleyPrefrencesRowNineColumnTenthValue
	  ,[AthleticsDB].[dbo].MeetSetupDualMeet.LaneNumber as DualMeetLaneNumber
	   ,[AthleticsDB].[dbo].SchoolDetail.Abbr  as DualMeetSchoolAbbr
      ,[AthleticsDB].[dbo].SchoolDetail.SchoolName  as DualMeetSchoolName
	  ,[AthleticsDB].[dbo].MeetSetupDualMeetAssignment.AlternamtUserOfUnAssignedLane as DualMeetAlternateUserOfUnAssignedLane
	  ,[AthleticsDB].[dbo].MeetSetupDualMeetAssignment.StrictAssignmentAllHeats as DualMeetStrictAssignmentAllHeats
	  ,[AthleticsDB].[dbo].MeetSetupDualMeetAssignment.StrictAssignmentFastestHeatOnly as DualMeetStrictAssignmentFastestHeatOnly
	  ,[AthleticsDB].[dbo].MeetSetupDualMeetAssignment.UseLaneAssignmentsForInLaneRacesOnly as DualMeetUseLaneAssignmentsForInLaneRacesOnly
	  ,[AthleticsDB].[dbo].MeetSetupDualMeetAssignment.UserLaneOrPositionAssignmentsAbove as DualMeetUserLaneOrPositionAssignmentsAbove
	  ,[AthleticsDB].[dbo].MeetSetupDualMeetAssignment.AlternamtUserOfUnAssignedLane as DualMeetAlternamtUserOfUnAssignedLane

	  ,[AthleticsDB].[dbo].MeetSetupRendomizationRule.TimedFinalEvents as RendomizationRuleTimedFinalEvents
	  ,[AthleticsDB].[dbo].MeetSetupRendomizationRule.RoundFirstMultipleRound as RendomizationRuleRoundFirstMultipleRound
	  ,[AthleticsDB].[dbo].MeetSetupRendomizationRule.RoundSecondThirdAndForth as RendomizationRuleRoundSecondThirdAndForth
	  ,[AthleticsDB].[dbo].MeetSetupRendomizationRule.CloseGap as RendomizationRuleCloseGap

	  ,[AthleticsDB].[dbo].MeetSetupSeedingRules.AllowForeignAthletesInFinal as SeedingAllowForeignAthletesInFinal
	  ,[AthleticsDB].[dbo].MeetSetupSeedingRules.AllowExhibitionAthletesInFinal as SeedingRulesAllowExhibitionAthletesInFinal
	  ,[AthleticsDB].[dbo].MeetSetupSeedingRules.SeedExhibitionAthletesLast as SeedingRulesApplyNCAARule1
	  ,[AthleticsDB].[dbo].MeetSetupSeedingRules.ApplyNCAARule as SeedingRulesApplyNCAARule
	    ,[AthleticsDB].[dbo].MeetSetupSeedingRules.UseSpecialRandomSelectMethod as SeedingRulesUseSpecialRandomSelectMethod

  FROM MeetSetupWaterfallStart  
    INNER JOIN [AthleticsDB].[dbo].MeetSetup
  on MeetSetupWaterfallStart.MeetSetupID = MeetSetup.MeetSetupID AND  MeetSetup.MeetSetupID = @MeetSetupID

  INNER JOIN [AthleticsDB].[dbo].MeetSetupStandardLanePrefrences
  on MeetSetupStandardLanePrefrences.MeetSetupID = @MeetSetupID
   
  
  INNER JOIN [AthleticsDB].[dbo].MeetStandardAlleyPrefrences 
  on [AthleticsDB].[dbo].MeetStandardAlleyPrefrences.MeetSetupID = @MeetSetupID

  INNER JOIN [AthleticsDB].[dbo].MeetSetupDualMeet 
  on MeetSetupDualMeet.MeetSetupID = @MeetSetupID

 INNER JOIN [AthleticsDB].[dbo].MeetSetupDualMeetAssignment 
  on MeetSetupDualMeetAssignment.MeetSetupID = @MeetSetupID

   INNER JOIN [AthleticsDB].[dbo].SchoolDetail 
  on SchoolDetail.SchoolID = MeetSetupDualMeet.SchoolID

  INNER JOIN [AthleticsDB].[dbo].MeetSetupSeedingRules 
  on MeetSetupSeedingRules.MeetSetupID = @MeetSetupID

INNER JOIN [AthleticsDB].[dbo].MeetSetupRendomizationRule 
  on MeetSetupRendomizationRule.MeetSetupID = @MeetSetupID
END


GO
/****** Object:  StoredProcedure [dbo].[GetTableSchema]    Script Date: 4/14/2019 10:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Devender Sharma
-- Create date: March 10
-- Description:	Get Table Schema by Table name
-- =============================================
CREATE PROCEDURE [dbo].[GetTableSchema] 
	-- Add the parameters for the stored procedure here
	@TableName nvarchar(500)
AS
BEGIN

	Select 
  DISTINCT mc.COLUMN_NAME,    
   case        
      when mc.COLUMN_NAME IN(
        select COLUMN_NAME 
      from INFORMATION_SCHEMA.KEY_COLUMN_USAGE cc
	 WHERE cc.TABLE_NAME =@TableName
	 AND OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1) 
	 then 'true' else 'false'
   end as IsPrimaryKey ,
   case        
      when mc.COLUMN_NAME IN(
        select  c1.name
		 FROM  sysforeignkeys fk
		INNER JOIN sysobjects o1 ON fk.fkeyid = o1.id
		INNER JOIN sysobjects o2 ON fk.rkeyid = o2.id
		INNER JOIN syscolumns c1 ON c1.id = o1.id AND c1.colid = fk.fkey
		INNER JOIN syscolumns c2 ON c2.id = o2.id AND c2.colid = fk.rkey
		INNER JOIN sysobjects s ON fk.constid = s.id
WHERE o1.name =@TableName) 
	 then 'true' else 'false'
   end as IsForeignKey
   --o2.name
         
from 
   AthleticsDB.INFORMATION_SCHEMA.COLUMNS mc        
  INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE mr on mc.TABLE_NAME=mr.TABLE_NAME 
 --AND mc.COLUMN_NAME=mr.COLUMN_NAME 
WHERE mc.TABLE_NAME = @TableName


END

GO
