USE [AthleticsDB]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] DROP CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateScoringAward]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] DROP CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateFieldRelay]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] DROP CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateEntryLimit]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] DROP CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateEntriesResult]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] DROP CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateCCRR]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] DROP CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateAgeGrading]
GO
ALTER TABLE [dbo].[ScoringSetupTemplateDetail] DROP CONSTRAINT [FK_ScoringSetupTemplateDetail_ScoringSetupTemplate]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesScoringAward] DROP CONSTRAINT [FK_MeetSetupScroringPreferencesScoringAward_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesEntryLimit] DROP CONSTRAINT [FK_MeetSetupScroringPreferencesTemplateEntryLimit_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesEntriesResult] DROP CONSTRAINT [FK_MeetSetupScroringPreferencesEntriesResult_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesCCRR] DROP CONSTRAINT [FK_MeetSetupScroringPreferencesCCRR_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesAgeGrading] DROP CONSTRAINT [FK_MeetSetupScroringPreferencesAgeGrading_MeetSetup]
GO
ALTER TABLE [dbo].[MeetScroringPreferencesFieldRelay] DROP CONSTRAINT [FK_MeetScroringPreferencesFieldRelay_MeetSetup]
GO
ALTER TABLE [dbo].[MeetScoringSetup] DROP CONSTRAINT [FK_MeetScoringSetup_MeetSetup]
GO
ALTER TABLE [dbo].[EventSessionSchedule] DROP CONSTRAINT [FK_EventSessionSchedule_EventSession]
GO
ALTER TABLE [dbo].[EventInSession] DROP CONSTRAINT [FK_EventInSession_EventSessionSchedule]
GO
ALTER TABLE [dbo].[EventInSession] DROP CONSTRAINT [FK_EventInSession_EventDetail]
GO
ALTER TABLE [dbo].[ApplicationMenu] DROP CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1]
GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateScoringAward]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ScroringPreferencesTemplateScoringAward]
GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateFieldRelay]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ScroringPreferencesTemplateFieldRelay]
GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateEntryLimit]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ScroringPreferencesTemplateEntryLimit]
GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateCCRR]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ScroringPreferencesTemplateCCRR]
GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateAgeGrading]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ScroringPreferencesTemplateAgeGrading]
GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplate]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ScroringPreferencesTemplate]
GO
/****** Object:  Table [dbo].[ScoringSetupTemplateDetail]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ScoringSetupTemplateDetail]
GO
/****** Object:  Table [dbo].[ScoringSetupTemplate]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ScoringSetupTemplate]
GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesScoringAward]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[MeetSetupScroringPreferencesScoringAward]
GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesEntryLimit]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[MeetSetupScroringPreferencesEntryLimit]
GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesEntriesResult]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[MeetSetupScroringPreferencesEntriesResult]
GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesCCRR]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[MeetSetupScroringPreferencesCCRR]
GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesAgeGrading]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[MeetSetupScroringPreferencesAgeGrading]
GO
/****** Object:  Table [dbo].[MeetScroringPreferencesFieldRelay]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[MeetScroringPreferencesFieldRelay]
GO
/****** Object:  Table [dbo].[MeetScoringSetup]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[MeetScoringSetup]
GO
/****** Object:  Table [dbo].[EventSessionSchedule]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[EventSessionSchedule]
GO
/****** Object:  Table [dbo].[EventSession]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[EventSession]
GO
/****** Object:  Table [dbo].[EventInSession]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[EventInSession]
GO
/****** Object:  Table [dbo].[EventDetail]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[EventDetail]
GO
/****** Object:  Table [dbo].[DivisionRegionName]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[DivisionRegionName]
GO
/****** Object:  Table [dbo].[ApplicationMenu]    Script Date: 6/9/2019 6:59:01 PM ******/
DROP TABLE [dbo].[ApplicationMenu]
GO
/****** Object:  Table [dbo].[ApplicationMenu]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationMenu](
	[MenuID] [int] NOT NULL,
	[MenuName] [nvarchar](50) NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [PK_ApplicationMenus] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DivisionRegionName]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DivisionRegionName](
	[DivisionRegionNameID] [int] IDENTITY(1,1) NOT NULL,
	[Division] [nvarchar](550) NULL,
	[Code] [nvarchar](550) NULL,
	[DivisionName] [nvarchar](550) NULL,
	[Range] [nvarchar](550) NULL,
	[Calc] [nvarchar](550) NULL,
	[StartDate] [date] NULL,
	[Operator] [nchar](10) NULL,
	[EndDate] [date] NULL,
	[Entry] [int] NULL,
 CONSTRAINT [PK_DivisionRegionName] PRIMARY KEY CLUSTERED 
(
	[DivisionRegionNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EventDetail]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventDetail](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[EventNumber] [nvarchar](550) NULL,
	[Status] [nvarchar](550) NULL,
	[Gender] [nvarchar](550) NULL,
	[StartAgeRange] [int] NULL,
	[EndAgeRange] [int] NULL,
	[IsMultiAgeGroup] [bit] NULL,
	[Division] [nvarchar](550) NULL,
	[EventNotes] [nvarchar](550) NULL,
	[Entries] [nvarchar](550) NULL,
	[Results] [nvarchar](550) NULL,
	[AgeGroup] [nvarchar](550) NULL,
	[Distance] [int] NULL,
	[Event] [nvarchar](550) NULL,
	[Advance] [nvarchar](550) NULL,
	[HeatOrder] [nvarchar](550) NULL,
	[NumberOfLane] [int] NULL,
	[Heats] [nvarchar](550) NULL,
	[LinePosition] [nvarchar](550) NULL,
	[HeatInSemis] [int] NULL,
	[HeatInQtrs] [int] NULL,
	[IsScoreEvent] [bit] NULL,
	[isJumbOff] [bit] NULL,
	[EntryFee] [float] NULL,
	[Type] [nvarchar](550) NULL,
	[Rnds] [nvarchar](550) NULL,
	[Finals] [nvarchar](550) NULL,
	[FinalNumberOfLanes] [int] NULL,
	[FnalOrder] [nvarchar](550) NULL,
 CONSTRAINT [PK_EventDetail] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EventInSession]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventInSession](
	[EventInSessionID] [int] IDENTITY(1,1) NOT NULL,
	[EventID] [int] NULL,
	[SessionScheduleID] [int] NULL,
 CONSTRAINT [PK_EventInSession] PRIMARY KEY CLUSTERED 
(
	[EventInSessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EventSession]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventSession](
	[EventSessionID] [int] IDENTITY(1,1) NOT NULL,
	[Day] [int] NULL,
	[StartTime] [nvarchar](150) NULL,
	[EntryLimit] [int] NULL,
	[Session] [nvarchar](550) NULL,
	[SessionTitle] [nvarchar](550) NULL,
 CONSTRAINT [PK_EventSession] PRIMARY KEY CLUSTERED 
(
	[EventSessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EventSessionSchedule]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventSessionSchedule](
	[SessionScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[EventSessionID] [int] NOT NULL,
	[EventNumber] [int] NULL,
	[Report] [nvarchar](50) NULL,
	[RND] [nvarchar](550) NULL,
	[EventName] [nvarchar](550) NULL,
	[SessionOrder] [int] NULL,
	[StartTime] [nvarchar](150) NULL,
	[AmPM] [nvarchar](50) NULL,
 CONSTRAINT [PK_EventSessionSchedule] PRIMARY KEY CLUSTERED 
(
	[SessionScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetScoringSetup]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetScoringSetup](
	[MeetScoringSetupID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupID] [int] NULL,
	[Place] [int] NULL,
	[IndividualPints] [int] NULL,
	[RelayPoints] [int] NULL,
	[CombEvtPoints] [int] NULL,
 CONSTRAINT [PK_MeetScoringSetup] PRIMARY KEY CLUSTERED 
(
	[MeetScoringSetupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetScroringPreferencesFieldRelay]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetScroringPreferencesFieldRelay](
	[ScroringPreferencesFieldRelayID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupID] [int] NULL,
	[MarksWillBeAddedForReamRanking] [int] NULL,
	[FieldRelayCanBeScoreWithUnderSizeTeam] [bit] NULL,
	[TimesWillBeAddedForTeamRanking] [int] NULL,
 CONSTRAINT [PK_MeetScroringPreferencesFieldRelay] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesFieldRelayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesAgeGrading]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupScroringPreferencesAgeGrading](
	[ScroringPreferencesAgeGradingID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupID] [int] NULL,
	[UseOneYearAgingTableStandared] [bit] NULL,
	[UseFiveYearAgingTableStandared] [bit] NULL,
	[UseOneYearAgingTableMulti] [bit] NULL,
	[UseFiveYearAgingTableMulti] [bit] NULL,
 CONSTRAINT [PK_MeetSetupScroringPreferencesAgeGrading] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesAgeGradingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesCCRR]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupScroringPreferencesCCRR](
	[ScroringPreferencesCCRRID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupID] [int] NULL,
	[MaximumRunnerToQualifyAsATeam] [int] NULL,
	[NumberOfRunnersScoreFromEachTeam] [int] NULL,
	[MaximumDisplacerPerTeam] [int] NULL,
	[NoneTeamRunnerDisplacedOtherRunner] [bit] NULL,
	[DynamicMultipleTeamScoring] [bit] NULL,
	[PanaltyPointsAddedForShortTeams] [int] NULL,
	[PanaltyPointAddedForShortTeamLast1Last2Etc] [bit] NULL,
	[DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6] [int] NULL,
	[HowManuTopOverAllRunnersToExcludeFromAgeGroupResult] [int] NULL,
 CONSTRAINT [PK_MeetSetupScroringPreferencesCCRR] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesCCRRID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesEntriesResult]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupScroringPreferencesEntriesResult](
	[ScroringPreferencesEntriesResultID] [int] NOT NULL,
	[MeetSetupID] [int] NULL,
	[UseEntryDeclarationMethod] [bit] NULL,
	[EntryNotesWithEachEntry] [bit] NULL,
	[ConvertHandEntryTimesToFAT] [bit] NULL,
	[ShowFinishedPlaceWithEachEntry] [bit] NULL,
	[RecordWindReading] [bit] NULL,
	[RoundUpResultToTenthofReport] [bit] NULL,
	[RankResultByAgeGradedMarks] [bit] NULL,
	[WarnIfTimesMarksAreOutofRange] [bit] NULL,
	[RoundDownTheNearestEventCentimerer] [bit] NULL,
 CONSTRAINT [PK_MeetSetupScroringPreferencesEntriesResult] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesEntriesResultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesEntryLimit]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupScroringPreferencesEntryLimit](
	[ScroringPreferencesEntryLimitID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupID] [int] NULL,
	[MaximumEntriesPerAthleteIncludingRelays] [int] NULL,
	[MaximumTrackEventEntriesPerAthleteIncludingRelay] [int] NULL,
	[MaximumFieldEventEntriesPerAthletes] [int] NULL,
	[WarnIfEntryLimitsExceeds] [bit] NULL,
 CONSTRAINT [PK_MeetSetupScroringPreferencesTemplateEntryLimit] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesEntryLimitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MeetSetupScroringPreferencesScoringAward]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeetSetupScroringPreferencesScoringAward](
	[ScroringPreferencesScoringAwardID] [int] IDENTITY(1,1) NOT NULL,
	[MeetSetupID] [int] NULL,
	[DiffrentPointSystemForMaleAndFemale] [bit] NULL,
	[DiffrentPointSystemForEach] [bit] NULL,
	[ExceedOrEqualQualifingMarks] [bit] NULL,
	[ScoreFastestHeadOnlyRegardLessOverAllPlace] [bit] NULL,
	[ScoreEqualRelayOnly] [bit] NULL,
	[ExhibitionMarksReceiveFinalsRanks] [bit] NULL,
	[TreateForeignAthletesAsExhibition] [bit] NULL,
	[AllowForeignAthletesPointScoreToCountTowordTeamScore] [bit] NULL,
	[MaximumScorePerTeamPerEventIndividual] [int] NULL,
	[MaximumScorePerTeamPerEventRelay] [int] NULL,
	[TopHowManyForAwardLabelIndividual] [int] NULL,
	[TopHowManyForAwardLabelRelay] [int] NULL,
	[MaximumPerTeamFinalFromPreLimsIndividual] [int] NULL,
	[MaximumPerTeamFinalFromPreLimsRelay] [int] NULL,
	[IncludeRelayPointsWithIndivHighPointScore] [bit] NULL,
 CONSTRAINT [PK_MeetSetupScroringPreferencesScoringAward] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesScoringAwardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScoringSetupTemplate]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScoringSetupTemplate](
	[ScoringSetupTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](550) NULL,
	[IsDefault] [bit] NULL,
 CONSTRAINT [PK_ScoringSetupTemplate] PRIMARY KEY CLUSTERED 
(
	[ScoringSetupTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScoringSetupTemplateDetail]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScoringSetupTemplateDetail](
	[ScoringSetupTemplateDetailID] [int] IDENTITY(1,1) NOT NULL,
	[ScoringSetupTemplateID] [int] NOT NULL,
	[Place] [int] NULL,
	[IndividualPints] [int] NULL,
	[RelayPoints] [int] NULL,
	[CombEvtPoints] [int] NULL,
 CONSTRAINT [PK_ScoringSetupTemplateDetail] PRIMARY KEY CLUSTERED 
(
	[ScoringSetupTemplateDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplate]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScroringPreferencesTemplate](
	[ScroringPreferencesTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[ScroringPreferencesTemplateName] [nvarchar](550) NULL,
	[ScroringPreferencesTemplateScoringAwardID] [int] NULL,
	[ScroringPreferencesTemplateAgeGradingID] [int] NULL,
	[ScroringPreferencesTemplateCCRRID] [int] NULL,
	[ScroringPreferencesTemplateEntryLimitID] [int] NULL,
	[ScroringPreferencesTemplateEntriesResultID] [int] NULL,
	[ScroringPreferencesTemplateFieldRelayID] [int] NULL,
	[ScroringPreferencesTemplateEnteryConversionID] [int] NULL,
	[ScroringPreferencesTemplateTwoOrThreeDualID] [int] NULL,
 CONSTRAINT [PK_ScroringPreferencesTemplate] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateAgeGrading]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScroringPreferencesTemplateAgeGrading](
	[ScroringPreferencesTemplateAgeGradingID] [int] IDENTITY(1,1) NOT NULL,
	[UseOneYearAgingTableStandared] [bit] NULL,
	[UseFiveYearAgingTableStandared] [bit] NULL,
	[UseOneYearAgingTableMulti] [bit] NULL,
	[UseFiveYearAgingTableMulti] [bit] NULL,
 CONSTRAINT [PK_ScroringPreferencesTemplateAgeGrading] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesTemplateAgeGradingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateCCRR]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScroringPreferencesTemplateCCRR](
	[ScroringPreferencesTemplateCCRRID] [int] IDENTITY(1,1) NOT NULL,
	[MaximumRunnerToQualifyAsATeam] [int] NULL,
	[NumberOfRunnersScoreFromEachTeam] [int] NULL,
	[MaximumDisplacerPerTeam] [int] NULL,
	[NoneTeamRunnerDisplacedOtherRunner] [bit] NULL,
	[DynamicMultipleTeamScoring] [bit] NULL,
	[PanaltyPointsAddedForShortTeams] [int] NULL,
	[PanaltyPointAddedForShortTeamLast1Last2Etc] [bit] NULL,
	[DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6] [int] NULL,
	[HowManuTopOverAllRunnersToExcludeFromAgeGroupResult] [int] NULL,
 CONSTRAINT [PK_ScroringPreferencesTemplateCCRR] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesTemplateCCRRID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateEntryLimit]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScroringPreferencesTemplateEntryLimit](
	[ScroringPreferencesTemplateEntryLimitID] [int] IDENTITY(1,1) NOT NULL,
	[MaximumEntriesPerAthleteIncludingRelays] [int] NULL,
	[MaximumTrackEventEntriesPerAthleteIncludingRelay] [int] NULL,
	[MaximumFieldEventEntriesPerAthletes] [int] NULL,
	[WarnIfEntryLimitsExceeds] [bit] NULL,
 CONSTRAINT [PK_ScroringPreferencesTemplateEntryLimit] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesTemplateEntryLimitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateFieldRelay]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScroringPreferencesTemplateFieldRelay](
	[ScroringPreferencesTemplateFieldRelayID] [int] IDENTITY(1,1) NOT NULL,
	[MarksWillBeAddedForReamRanking] [int] NULL,
	[FieldRelayCanBeScoreWithUnderSizeTeam] [bit] NULL,
	[TimesWillBeAddedForTeamRanking] [int] NULL,
 CONSTRAINT [PK_ScroringPreferencesTemplateFieldRelay] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesTemplateFieldRelayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScroringPreferencesTemplateScoringAward]    Script Date: 6/9/2019 6:59:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScroringPreferencesTemplateScoringAward](
	[ScroringPreferencesTemplateScoringAwardID] [int] IDENTITY(1,1) NOT NULL,
	[DiffrentPointSystemForMaleAndFemale] [bit] NULL,
	[DiffrentPointSystemForEach] [bit] NULL,
	[ExceedOrEqualQualifingMarks] [bit] NULL,
	[ScoreFastestHeadOnlyRegardLessOverAllPlace] [bit] NULL,
	[ScoreEqualRelayOnly] [bit] NULL,
	[ExhibitionMarksReceiveFinalsRanks] [bit] NULL,
	[TreateForeignAthletesAsExhibition] [bit] NULL,
	[AllowForeignAthletesPointScoreToCountTowordTeamScore] [bit] NULL,
	[MaximumScorePerTeamPerEventIndividual] [int] NULL,
	[MaximumScorePerTeamPerEventRelay] [int] NULL,
	[TopHowManyForAwardLabelIndividual] [int] NULL,
	[TopHowManyForAwardLabelRelay] [int] NULL,
	[MaximumPerTeamFinalFromPreLimsIndividual] [int] NULL,
	[MaximumPerTeamFinalFromPreLimsRelay] [int] NULL,
	[IncludeRelayPointsWithIndivHighPointScore] [bit] NULL,
 CONSTRAINT [PK_ScroringPreferencesTemplateScoringAward] PRIMARY KEY CLUSTERED 
(
	[ScroringPreferencesTemplateScoringAwardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (1, N'File', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (2, N'Set-up', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (3, N'Events', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (4, N'Athletes', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (5, N'Relays', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (6, N'Schools', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (7, N'Seeding', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (8, N'Run', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (9, N'Labels', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (10, N'Check for Updates', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (11, N'Help', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (12, N'Meet Set-up', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (13, N'Athlete/Relay Preferences', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (14, N'Seeding Preferences', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (15, N'Admin', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (16, N'Create User', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (17, N'Add User Role', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (18, N'Assign Role', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (19, N'Add Module', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (20, N'Assign Module', 15)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (21, N'Report', NULL)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (22, N'Design  Report Template', 21)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (23, N'Assign DataSet To Report', 21)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (24, N'View Report', 21)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (25, N'Design Report Data Set', 21)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (26, N'Entry Scoring Preferences', 1)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (27, N'Entry/Scoring Preferences', 2)
INSERT [dbo].[ApplicationMenu] ([MenuID], [MenuName], [ParentID]) VALUES (28, N'Division/Region Names', 2)
SET IDENTITY_INSERT [dbo].[DivisionRegionName] ON 

INSERT [dbo].[DivisionRegionName] ([DivisionRegionNameID], [Division], [Code], [DivisionName], [Range], [Calc], [StartDate], [Operator], [EndDate], [Entry]) VALUES (1, N'1', N'SB', N'Peewee', N'0-8', N'D-Y', CAST(N'2019-04-22' AS Date), N'>         ', CAST(N'2019-04-16' AS Date), 1)
INSERT [dbo].[DivisionRegionName] ([DivisionRegionNameID], [Division], [Code], [DivisionName], [Range], [Calc], [StartDate], [Operator], [EndDate], [Entry]) VALUES (2, N'2', N'BN', N'Bantam', N'9-10', N'Y-Y', CAST(N'2019-04-29' AS Date), N'>         ', CAST(N'2019-04-22' AS Date), 3)
INSERT [dbo].[DivisionRegionName] ([DivisionRegionNameID], [Division], [Code], [DivisionName], [Range], [Calc], [StartDate], [Operator], [EndDate], [Entry]) VALUES (3, N'3', N'MI', N'Midget', N'11-12', N'Y-Y', CAST(N'2019-04-30' AS Date), N'>         ', CAST(N'2019-05-06' AS Date), 4)
INSERT [dbo].[DivisionRegionName] ([DivisionRegionNameID], [Division], [Code], [DivisionName], [Range], [Calc], [StartDate], [Operator], [EndDate], [Entry]) VALUES (4, N'3', N'MI', N'Midget', N'11-12', N'Y-Y', CAST(N'2019-04-30' AS Date), N'>         ', CAST(N'2019-05-06' AS Date), 5)
INSERT [dbo].[DivisionRegionName] ([DivisionRegionNameID], [Division], [Code], [DivisionName], [Range], [Calc], [StartDate], [Operator], [EndDate], [Entry]) VALUES (5, N'4', N'YH', N'Youth', N'13-14', N'Y-y', CAST(N'2019-04-30' AS Date), N'<         ', CAST(N'2019-05-03' AS Date), 5)
SET IDENTITY_INSERT [dbo].[DivisionRegionName] OFF
SET IDENTITY_INSERT [dbo].[MeetScoringSetup] ON 

INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1, 1, 5, 55, 55, 55)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (2, 1, 4, 33, 33, 77)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (3, 1, 2, 0, 9, 99)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (4, 1, 3, 4, 99, 99)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (5, 1, 5, 55, 55, 55)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (6, 1, 6, 7, 7, 7)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (7, 1, 7, 8, 9, 5)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (8, 1, 8, 9, 0, 4)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (9, 1, 9, 8, 4, 44)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (10, 1, 10, 4, 4, 44)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (11, 1, 11, 3, 4, 44)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (12, 1, 12, 22, 44, 44)
INSERT [dbo].[MeetScoringSetup] ([MeetScoringSetupID], [MeetSetupID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (13, 1, 1, 4, 45, 55)
SET IDENTITY_INSERT [dbo].[MeetScoringSetup] OFF
SET IDENTITY_INSERT [dbo].[MeetScroringPreferencesFieldRelay] ON 

INSERT [dbo].[MeetScroringPreferencesFieldRelay] ([ScroringPreferencesFieldRelayID], [MeetSetupID], [MarksWillBeAddedForReamRanking], [FieldRelayCanBeScoreWithUnderSizeTeam], [TimesWillBeAddedForTeamRanking]) VALUES (1, NULL, 5, 0, 5)
INSERT [dbo].[MeetScroringPreferencesFieldRelay] ([ScroringPreferencesFieldRelayID], [MeetSetupID], [MarksWillBeAddedForReamRanking], [FieldRelayCanBeScoreWithUnderSizeTeam], [TimesWillBeAddedForTeamRanking]) VALUES (2, NULL, 5, 0, 5)
SET IDENTITY_INSERT [dbo].[MeetScroringPreferencesFieldRelay] OFF
SET IDENTITY_INSERT [dbo].[MeetSetupScroringPreferencesAgeGrading] ON 

INSERT [dbo].[MeetSetupScroringPreferencesAgeGrading] ([ScroringPreferencesAgeGradingID], [MeetSetupID], [UseOneYearAgingTableStandared], [UseFiveYearAgingTableStandared], [UseOneYearAgingTableMulti], [UseFiveYearAgingTableMulti]) VALUES (2, 1, 0, 1, 0, 1)
SET IDENTITY_INSERT [dbo].[MeetSetupScroringPreferencesAgeGrading] OFF
SET IDENTITY_INSERT [dbo].[MeetSetupScroringPreferencesCCRR] ON 

INSERT [dbo].[MeetSetupScroringPreferencesCCRR] ([ScroringPreferencesCCRRID], [MeetSetupID], [MaximumRunnerToQualifyAsATeam], [NumberOfRunnersScoreFromEachTeam], [MaximumDisplacerPerTeam], [NoneTeamRunnerDisplacedOtherRunner], [DynamicMultipleTeamScoring], [PanaltyPointsAddedForShortTeams], [PanaltyPointAddedForShortTeamLast1Last2Etc], [DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6], [HowManuTopOverAllRunnersToExcludeFromAgeGroupResult]) VALUES (2, 1, 6, 6, 5, 1, 1, 5, 1, 5, 5)
SET IDENTITY_INSERT [dbo].[MeetSetupScroringPreferencesCCRR] OFF
INSERT [dbo].[MeetSetupScroringPreferencesEntriesResult] ([ScroringPreferencesEntriesResultID], [MeetSetupID], [UseEntryDeclarationMethod], [EntryNotesWithEachEntry], [ConvertHandEntryTimesToFAT], [ShowFinishedPlaceWithEachEntry], [RecordWindReading], [RoundUpResultToTenthofReport], [RankResultByAgeGradedMarks], [WarnIfTimesMarksAreOutofRange], [RoundDownTheNearestEventCentimerer]) VALUES (0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[MeetSetupScroringPreferencesEntryLimit] ON 

INSERT [dbo].[MeetSetupScroringPreferencesEntryLimit] ([ScroringPreferencesEntryLimitID], [MeetSetupID], [MaximumEntriesPerAthleteIncludingRelays], [MaximumTrackEventEntriesPerAthleteIncludingRelay], [MaximumFieldEventEntriesPerAthletes], [WarnIfEntryLimitsExceeds]) VALUES (2, 1, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[MeetSetupScroringPreferencesEntryLimit] OFF
SET IDENTITY_INSERT [dbo].[MeetSetupScroringPreferencesScoringAward] ON 

INSERT [dbo].[MeetSetupScroringPreferencesScoringAward] ([ScroringPreferencesScoringAwardID], [MeetSetupID], [DiffrentPointSystemForMaleAndFemale], [DiffrentPointSystemForEach], [ExceedOrEqualQualifingMarks], [ScoreFastestHeadOnlyRegardLessOverAllPlace], [ScoreEqualRelayOnly], [ExhibitionMarksReceiveFinalsRanks], [TreateForeignAthletesAsExhibition], [AllowForeignAthletesPointScoreToCountTowordTeamScore], [MaximumScorePerTeamPerEventIndividual], [MaximumScorePerTeamPerEventRelay], [TopHowManyForAwardLabelIndividual], [TopHowManyForAwardLabelRelay], [MaximumPerTeamFinalFromPreLimsIndividual], [MaximumPerTeamFinalFromPreLimsRelay], [IncludeRelayPointsWithIndivHighPointScore]) VALUES (2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 6, 6, 6, 7, 5, 5, 1)
SET IDENTITY_INSERT [dbo].[MeetSetupScroringPreferencesScoringAward] OFF
SET IDENTITY_INSERT [dbo].[ScoringSetupTemplate] ON 

INSERT [dbo].[ScoringSetupTemplate] ([ScoringSetupTemplateID], [TemplateName], [IsDefault]) VALUES (7, N'Template2', 1)
SET IDENTITY_INSERT [dbo].[ScoringSetupTemplate] OFF
SET IDENTITY_INSERT [dbo].[ScoringSetupTemplateDetail] ON 

INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1116, 7, 5, 55, 55, 55)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1117, 7, 4, 33, 33, 77)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1118, 7, 2, 0, 9, 99)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1119, 7, 3, 4, 99, 99)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1120, 7, 5, 55, 55, 55)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1121, 7, 6, 7, 7, 7)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1122, 7, 7, 8, 9, 5)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1123, 7, 8, 9, 0, 4)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1124, 7, 9, 8, 4, 44)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1125, 7, 10, 4, 4, 44)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1126, 7, 11, 3, 4, 44)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1127, 7, 12, 22, 44, 44)
INSERT [dbo].[ScoringSetupTemplateDetail] ([ScoringSetupTemplateDetailID], [ScoringSetupTemplateID], [Place], [IndividualPints], [RelayPoints], [CombEvtPoints]) VALUES (1128, 7, 1, 4, 45, 55)
SET IDENTITY_INSERT [dbo].[ScoringSetupTemplateDetail] OFF
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplate] ON 

INSERT [dbo].[ScroringPreferencesTemplate] ([ScroringPreferencesTemplateID], [ScroringPreferencesTemplateName], [ScroringPreferencesTemplateScoringAwardID], [ScroringPreferencesTemplateAgeGradingID], [ScroringPreferencesTemplateCCRRID], [ScroringPreferencesTemplateEntryLimitID], [ScroringPreferencesTemplateEntriesResultID], [ScroringPreferencesTemplateFieldRelayID], [ScroringPreferencesTemplateEnteryConversionID], [ScroringPreferencesTemplateTwoOrThreeDualID]) VALUES (3, N'Template1', 3, 3, 3, 3, 3, 3, NULL, NULL)
INSERT [dbo].[ScroringPreferencesTemplate] ([ScroringPreferencesTemplateID], [ScroringPreferencesTemplateName], [ScroringPreferencesTemplateScoringAwardID], [ScroringPreferencesTemplateAgeGradingID], [ScroringPreferencesTemplateCCRRID], [ScroringPreferencesTemplateEntryLimitID], [ScroringPreferencesTemplateEntriesResultID], [ScroringPreferencesTemplateFieldRelayID], [ScroringPreferencesTemplateEnteryConversionID], [ScroringPreferencesTemplateTwoOrThreeDualID]) VALUES (4, N'', 4, 4, 4, 4, 4, 4, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplate] OFF
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateAgeGrading] ON 

INSERT [dbo].[ScroringPreferencesTemplateAgeGrading] ([ScroringPreferencesTemplateAgeGradingID], [UseOneYearAgingTableStandared], [UseFiveYearAgingTableStandared], [UseOneYearAgingTableMulti], [UseFiveYearAgingTableMulti]) VALUES (3, 0, 1, 0, 1)
INSERT [dbo].[ScroringPreferencesTemplateAgeGrading] ([ScroringPreferencesTemplateAgeGradingID], [UseOneYearAgingTableStandared], [UseFiveYearAgingTableStandared], [UseOneYearAgingTableMulti], [UseFiveYearAgingTableMulti]) VALUES (4, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateAgeGrading] OFF
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateCCRR] ON 

INSERT [dbo].[ScroringPreferencesTemplateCCRR] ([ScroringPreferencesTemplateCCRRID], [MaximumRunnerToQualifyAsATeam], [NumberOfRunnersScoreFromEachTeam], [MaximumDisplacerPerTeam], [NoneTeamRunnerDisplacedOtherRunner], [DynamicMultipleTeamScoring], [PanaltyPointsAddedForShortTeams], [PanaltyPointAddedForShortTeamLast1Last2Etc], [DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6], [HowManuTopOverAllRunnersToExcludeFromAgeGroupResult]) VALUES (3, 6, 6, 5, 1, 1, 5, 1, 5, 5)
INSERT [dbo].[ScroringPreferencesTemplateCCRR] ([ScroringPreferencesTemplateCCRRID], [MaximumRunnerToQualifyAsATeam], [NumberOfRunnersScoreFromEachTeam], [MaximumDisplacerPerTeam], [NoneTeamRunnerDisplacedOtherRunner], [DynamicMultipleTeamScoring], [PanaltyPointsAddedForShortTeams], [PanaltyPointAddedForShortTeamLast1Last2Etc], [DefaultNumberOfRunnersTimesToBeAddedInfTeamScoring2To6], [HowManuTopOverAllRunnersToExcludeFromAgeGroupResult]) VALUES (4, 0, 0, 0, 0, 0, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateCCRR] OFF
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateEntryLimit] ON 

INSERT [dbo].[ScroringPreferencesTemplateEntryLimit] ([ScroringPreferencesTemplateEntryLimitID], [MaximumEntriesPerAthleteIncludingRelays], [MaximumTrackEventEntriesPerAthleteIncludingRelay], [MaximumFieldEventEntriesPerAthletes], [WarnIfEntryLimitsExceeds]) VALUES (3, 0, 0, 0, 0)
INSERT [dbo].[ScroringPreferencesTemplateEntryLimit] ([ScroringPreferencesTemplateEntryLimitID], [MaximumEntriesPerAthleteIncludingRelays], [MaximumTrackEventEntriesPerAthleteIncludingRelay], [MaximumFieldEventEntriesPerAthletes], [WarnIfEntryLimitsExceeds]) VALUES (4, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateEntryLimit] OFF
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateFieldRelay] ON 

INSERT [dbo].[ScroringPreferencesTemplateFieldRelay] ([ScroringPreferencesTemplateFieldRelayID], [MarksWillBeAddedForReamRanking], [FieldRelayCanBeScoreWithUnderSizeTeam], [TimesWillBeAddedForTeamRanking]) VALUES (3, 5, 0, 5)
INSERT [dbo].[ScroringPreferencesTemplateFieldRelay] ([ScroringPreferencesTemplateFieldRelayID], [MarksWillBeAddedForReamRanking], [FieldRelayCanBeScoreWithUnderSizeTeam], [TimesWillBeAddedForTeamRanking]) VALUES (4, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateFieldRelay] OFF
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateScoringAward] ON 

INSERT [dbo].[ScroringPreferencesTemplateScoringAward] ([ScroringPreferencesTemplateScoringAwardID], [DiffrentPointSystemForMaleAndFemale], [DiffrentPointSystemForEach], [ExceedOrEqualQualifingMarks], [ScoreFastestHeadOnlyRegardLessOverAllPlace], [ScoreEqualRelayOnly], [ExhibitionMarksReceiveFinalsRanks], [TreateForeignAthletesAsExhibition], [AllowForeignAthletesPointScoreToCountTowordTeamScore], [MaximumScorePerTeamPerEventIndividual], [MaximumScorePerTeamPerEventRelay], [TopHowManyForAwardLabelIndividual], [TopHowManyForAwardLabelRelay], [MaximumPerTeamFinalFromPreLimsIndividual], [MaximumPerTeamFinalFromPreLimsRelay], [IncludeRelayPointsWithIndivHighPointScore]) VALUES (3, 1, 1, 1, 1, 0, 1, 1, 1, 6, 6, 6, 7, 5, 5, 1)
INSERT [dbo].[ScroringPreferencesTemplateScoringAward] ([ScroringPreferencesTemplateScoringAwardID], [DiffrentPointSystemForMaleAndFemale], [DiffrentPointSystemForEach], [ExceedOrEqualQualifingMarks], [ScoreFastestHeadOnlyRegardLessOverAllPlace], [ScoreEqualRelayOnly], [ExhibitionMarksReceiveFinalsRanks], [TreateForeignAthletesAsExhibition], [AllowForeignAthletesPointScoreToCountTowordTeamScore], [MaximumScorePerTeamPerEventIndividual], [MaximumScorePerTeamPerEventRelay], [TopHowManyForAwardLabelIndividual], [TopHowManyForAwardLabelRelay], [MaximumPerTeamFinalFromPreLimsIndividual], [MaximumPerTeamFinalFromPreLimsRelay], [IncludeRelayPointsWithIndivHighPointScore]) VALUES (4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[ScroringPreferencesTemplateScoringAward] OFF
ALTER TABLE [dbo].[ApplicationMenu]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1] FOREIGN KEY([ParentID])
REFERENCES [dbo].[ApplicationMenu] ([MenuID])
GO
ALTER TABLE [dbo].[ApplicationMenu] CHECK CONSTRAINT [FK_ApplicationMenu1_ApplicationMenu1]
GO
ALTER TABLE [dbo].[EventInSession]  WITH CHECK ADD  CONSTRAINT [FK_EventInSession_EventDetail] FOREIGN KEY([EventID])
REFERENCES [dbo].[EventDetail] ([EventID])
GO
ALTER TABLE [dbo].[EventInSession] CHECK CONSTRAINT [FK_EventInSession_EventDetail]
GO
ALTER TABLE [dbo].[EventInSession]  WITH CHECK ADD  CONSTRAINT [FK_EventInSession_EventSessionSchedule] FOREIGN KEY([SessionScheduleID])
REFERENCES [dbo].[EventSessionSchedule] ([SessionScheduleID])
GO
ALTER TABLE [dbo].[EventInSession] CHECK CONSTRAINT [FK_EventInSession_EventSessionSchedule]
GO
ALTER TABLE [dbo].[EventSessionSchedule]  WITH CHECK ADD  CONSTRAINT [FK_EventSessionSchedule_EventSession] FOREIGN KEY([EventSessionID])
REFERENCES [dbo].[EventSession] ([EventSessionID])
GO
ALTER TABLE [dbo].[EventSessionSchedule] CHECK CONSTRAINT [FK_EventSessionSchedule_EventSession]
GO
ALTER TABLE [dbo].[MeetScoringSetup]  WITH CHECK ADD  CONSTRAINT [FK_MeetScoringSetup_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetScoringSetup] CHECK CONSTRAINT [FK_MeetScoringSetup_MeetSetup]
GO
ALTER TABLE [dbo].[MeetScroringPreferencesFieldRelay]  WITH CHECK ADD  CONSTRAINT [FK_MeetScroringPreferencesFieldRelay_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetScroringPreferencesFieldRelay] CHECK CONSTRAINT [FK_MeetScroringPreferencesFieldRelay_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesAgeGrading]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupScroringPreferencesAgeGrading_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesAgeGrading] CHECK CONSTRAINT [FK_MeetSetupScroringPreferencesAgeGrading_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesCCRR]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupScroringPreferencesCCRR_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesCCRR] CHECK CONSTRAINT [FK_MeetSetupScroringPreferencesCCRR_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesEntriesResult]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupScroringPreferencesEntriesResult_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesEntriesResult] CHECK CONSTRAINT [FK_MeetSetupScroringPreferencesEntriesResult_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesEntryLimit]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupScroringPreferencesTemplateEntryLimit_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesEntryLimit] CHECK CONSTRAINT [FK_MeetSetupScroringPreferencesTemplateEntryLimit_MeetSetup]
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesScoringAward]  WITH CHECK ADD  CONSTRAINT [FK_MeetSetupScroringPreferencesScoringAward_MeetSetup] FOREIGN KEY([MeetSetupID])
REFERENCES [dbo].[MeetSetup] ([MeetSetupID])
GO
ALTER TABLE [dbo].[MeetSetupScroringPreferencesScoringAward] CHECK CONSTRAINT [FK_MeetSetupScroringPreferencesScoringAward_MeetSetup]
GO
ALTER TABLE [dbo].[ScoringSetupTemplateDetail]  WITH CHECK ADD  CONSTRAINT [FK_ScoringSetupTemplateDetail_ScoringSetupTemplate] FOREIGN KEY([ScoringSetupTemplateID])
REFERENCES [dbo].[ScoringSetupTemplate] ([ScoringSetupTemplateID])
GO
ALTER TABLE [dbo].[ScoringSetupTemplateDetail] CHECK CONSTRAINT [FK_ScoringSetupTemplateDetail_ScoringSetupTemplate]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate]  WITH CHECK ADD  CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateAgeGrading] FOREIGN KEY([ScroringPreferencesTemplateAgeGradingID])
REFERENCES [dbo].[ScroringPreferencesTemplateAgeGrading] ([ScroringPreferencesTemplateAgeGradingID])
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] CHECK CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateAgeGrading]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate]  WITH CHECK ADD  CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateCCRR] FOREIGN KEY([ScroringPreferencesTemplateCCRRID])
REFERENCES [dbo].[ScroringPreferencesTemplateCCRR] ([ScroringPreferencesTemplateCCRRID])
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] CHECK CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateCCRR]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate]  WITH CHECK ADD  CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateEntriesResult] FOREIGN KEY([ScroringPreferencesTemplateEntriesResultID])
REFERENCES [dbo].[ScroringPreferencesTemplateEntriesResult] ([ScroringPreferencesTemplateEntriesResultID])
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] CHECK CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateEntriesResult]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate]  WITH CHECK ADD  CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateEntryLimit] FOREIGN KEY([ScroringPreferencesTemplateEntryLimitID])
REFERENCES [dbo].[ScroringPreferencesTemplateEntryLimit] ([ScroringPreferencesTemplateEntryLimitID])
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] CHECK CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateEntryLimit]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate]  WITH CHECK ADD  CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateFieldRelay] FOREIGN KEY([ScroringPreferencesTemplateFieldRelayID])
REFERENCES [dbo].[ScroringPreferencesTemplateFieldRelay] ([ScroringPreferencesTemplateFieldRelayID])
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] CHECK CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateFieldRelay]
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate]  WITH CHECK ADD  CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateScoringAward] FOREIGN KEY([ScroringPreferencesTemplateScoringAwardID])
REFERENCES [dbo].[ScroringPreferencesTemplateScoringAward] ([ScroringPreferencesTemplateScoringAwardID])
GO
ALTER TABLE [dbo].[ScroringPreferencesTemplate] CHECK CONSTRAINT [FK_ScroringPreferencesTemplate_ScroringPreferencesTemplateScoringAward]
GO
